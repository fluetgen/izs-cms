<?php

require_once('../assets/classes/global.inc.php');
require_once('../assets/classes/class.mysql.php');

$strMethod = $_SERVER['REQUEST_METHOD'];

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
}

header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');    // cache for 1 day

if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, DELETE, PUT");         

if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

if ($strMethod == 'OPTIONS') {

    exit(0);

}

$strSecret = 'This_Is_A_Pretty_Cool_Secret';

/*

From: https://github.com/Placetel/call-control-notify-api

Our POST request
We will send a POST request with an application/x-www-form-urlencoded payload to your API endpoint for every event. Each event will have an call id to identify the call it belongs to. This call id will be a hex presentation of a SHA256 hash.

In order to verify the authenticity of our request on your side, we're using an HMAC with SHA256. You can configure the shared secret in your external api settings. After that, every request will have the HTTP Header X-PLACETEL-SIGNATURE.
You can calculate the signature and compare it to our signature in X-PLACETEL-SIGNATURE:

require 'openssl'
secret = 'THE_SECRET'
payload = 'POSTED_PAYLOAD'

digest = OpenSSL::Digest.new('sha256')
signature = OpenSSL::HMAC.hexdigest(digest, secret, payload)
For example a secret 12345 with a given payload call_id=4a4cbb39578170aed9a2761a7bec8c7e704a541f52291ef603d6f5f152980c3c&event=CallAccepted&from=0123456789&to=0987654321 will result in:

2.5.1 :005 > digest = OpenSSL::Digest.new('sha256')
 => #<OpenSSL::Digest: e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855>
2.5.1 :006 > signature = OpenSSL::HMAC.hexdigest(digest, secret, payload)
 => "c4f823c5b8806432fe2b83b1fc2ee714422e0cdfb4b5129152a7d0bbcd7792d0"
In order to restrict access to your API endpoint, you may use a simple basic-auth in the URL defined in your external api settings: https://admin:password@your.end.point/callback.
*/


$fp = fopen('php://input', 'r');
$rawData = stream_get_contents($fp);
$arrPost = json_decode($rawData, true);

//mail('system@izs-institut.de', 'Anruf', print_r($_POST, true));

if (isset($_POST['event'])) {

    if ($_POST['event'] === 'AcceptedCall') {
        // do s.th.
    } elseif ($_POST['event'] === 'HungUp') {
        // do s.th.
    } elseif ($_POST['event'] === 'IncomingCall') {
        // do s.th.
    }

    $strCallId =    @$_POST['call_id'];
    $strDirection = @$_POST['direction'];
    $intDuration =  @$_POST['duration'];
    $strEvent =     @$_POST['event'];
    $strFrom =      @$_POST['from'];
    $strPeer =      @$_POST['peer'];
    $strTo =        @$_POST['to'];
    $strType =      @$_POST['type'];

    $strSql = 'INSERT INTO `izs_telefon` (`it_id`, `it_call_id`, `it_direction`, `it_duration`, `it_event`, `it_from`, `it_peer`, `it_to`, `it_type`, `it_time`) ';
    $strSql.= 'VALUES (NULL, "' .$strCallId .'", "' .$strDirection .'", "' .$intDuration .'", "' .$strEvent .'", "' .$strFrom .'", "' .$strPeer .'", "' . $strTo .'", "' .$strType .'", NOW())';
    $arrQql = MySQLStatic::Insert($strSql);

}


exit();

/*

if (isset($arrPost) && (count($arrPost) > 0)) {


    if (isset($arrPost['entity']['contenttype']) && ($arrPost['entity']['contenttype'] == 'mail')) { // content is incomming mail?
        //  && isset($arrPost['entity']['flags']) && (in_array('incoming', $arrPost['entity']['flags']) !== false)

        $strSql = 'SELECT * FROM `izs_cms2sg` WHERE `cs_type` = "Basisdoku"';
        $arrSql = MySQLStatic::Query($strSql);
    
        $arrMonitorTopicList = array();
        if (count($arrSql) > 0) {
            foreach ($arrSql as $intKey => $arrRelation) {
                $arrMonitorTopicList[$arrRelation['cs_sg_index']] = $arrRelation['cs_id'];
            }
        }

        if (isset($arrPost['entity']['target']) && (isset($arrMonitorTopicList[$arrPost['entity']['target']]))) { // target is monitored?

            $strSql = 'UPDATE `izs_cms2sg` SET `cs_sg_update` = NOW() WHERE `cs_id` = "' .$arrMonitorTopicList[$arrPost['entity']['target']] .'"';
            $arrSql = MySQLStatic::Update($strSql);

            $strSql = 'INSERT INTO `izs_sg_hook` (`sh_id`, `sh_type`, `sh_key`, `sh_time`, `sh_entity`) VALUES (NULL, "1", "' .$arrPost['entity']['target'] .'", NOW(), "' .MySQLStatic::esc(serialize($arrPost['entity'])) .'")';
            $intSql = MySQLStatic::Insert($strSql);

        }


    }


}

*/


?>