<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once ('assets/classes/global.inc.php');
require_once ('assets/classes/class.mysql.php');

if (strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) {
  $strServer = 'test';
} else {
  $strServer = 'www';
}

$strHost   = 'https://' .$strServer .'.izs-institut.de/';
$strPortal = 'portal.html';

$strRedirectPage = '';

$strMethod  = $_SERVER['REQUEST_METHOD'];
$arrRequest = explode('/', trim($_REQUEST['q'],'/'));
$arrQuery   = $_REQUEST;

if (isset($arrRequest[0]) && ($arrRequest[0] != '')) {

  if (isset($arrRequest[1]) && ($arrRequest[1] != '')) {

    if ($arrRequest[1] == 'registrierung') {

      if ($arrRequest[0] == 'randstad') {
        $strRedirectPage = 'https://' .$strServer .'.izs-institut.de/randstad/registrierung';
      } elseif ($arrRequest[0] == 'gulp') {
        $strRedirectPage = 'https://' .$strServer .'.izs-institut.de/gulp/registrierung';
      }

    }

  } else {

    $strSql = 'SELECT `Id` FROM `Group__c` WHERE `Direktlink_zum_Konto__c` = "' .MySQLStatic::esc($arrRequest[0]) .'"';
    $arrSql = MySQLStatic::Query($strSql);

    if (count($arrSql) > 0) {
      $strRedirectPage = $strHost .$strPortal .'#!/company/' .$arrSql[0]['Id'];
    }   

  }
  


}

if ($strRedirectPage != '') {

    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' .$strRedirectPage);
    header('Connection: close'); 
    exit;
  
}

?>