<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

ini_set("memory_limit", "-1");
set_time_limit(0);

require_once('../cms/const.inc.php');
require_once('../assets/classes/class.mysql.php');

// regular expression contains a whitelist of characters to allow in filenames
// suitable for NTFS and POSIX systems
// supports UTF8 letters, the use of `\p{L}` matches a single character from any language
// does not support utf8 symbols atm like arrows
function sanitiseFileName ($strName = '', $strReplace = '') {
    return preg_replace("/[^\p{L}0-9 .,;'~`!@#$%^&()\-_+=\[\]\{\}]/u", $strReplace, $strName);
}

if (isset($argv)) {
    parse_str($argv[1], $arrArgv);
    $_REQUEST = $arrArgv;
}

$strRedirectUrl = @$_REQUEST['r'];

if (isset($strRedirectUrl) && ($strRedirectUrl != '')) {

    $arrPart = explode('/', $strRedirectUrl);

    //print_r($arrPart); die();

    // https://www.izs.de/sv-pruefbericht/15105593
    // https://www.izs.de/sv-pruefbericht/a083000000LQSO0AAP /force

    if (count($arrPart) >= 1) {
        
        $strPdf = '';

        $strSql3 = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$arrPart[0] .'" OR `Betriebsnummer__c` =  "' .$arrPart[0] .'"';
        $arrGroup = MySQLStatic::Query($strSql3);

        //GROUP EXISTS
        if (count($arrGroup) > 0) {

            $strSql3 = 'SELECT * FROM `izs_pruefbericht` WHERE `pr_group_id` = "' .$arrGroup[0]['Id'] .'" ORDER BY `pr_year` DESC, `pr_month` DESC, `pr_time` DESC LIMIT 1';
            $arrReport = MySQLStatic::Query($strSql3);

            if ((count($arrReport) == 1) && ((!isset($arrPart[1])) || ($arrPart[1] != 'force'))) {

                header('Content-type: application/pdf');
                header('Content-Disposition: inline; filename="' .$arrReport[0]['pr_file_name'] .'.pdf"');
                readfile(__DIR__ .$arrReport[0]['pr_file']);
                exit;
                
            }

            if (isset($arrPart[1]) && ($arrPart[1] == 'force')) {

                $boolMonthFound = false;

                $strMonth = 'SELECT * FROM `izs_sv_month` ORDER BY `Beitragsjahr__c` DESC, `Beitragsmonat__c` DESC LIMIT 1';
                $arrActualMonth = MySQLStatic::Query($strMonth);

                $intThisMonth  = mktime(0, 0, 0, $arrActualMonth[0]['Beitragsmonat__c'], 1, $arrActualMonth[0]['Beitragsjahr__c']);
                $intLastMonth1 = mktime(0, 0, 0, $arrActualMonth[0]['Beitragsmonat__c'] - 1, 1, $arrActualMonth[0]['Beitragsjahr__c']);
                $intLastMonth2 = mktime(0, 0, 0, $arrActualMonth[0]['Beitragsmonat__c'] - 2, 1, $arrActualMonth[0]['Beitragsjahr__c']);

                // mind. 1 keins enqu

                $strSql2 = 'SELECT `SF42_EventStatus__c` FROM `SF42_IZSEvent__c` ';
                $strSql2.= 'INNER JOIN `Account` AS `Acc` ON `Acc`.`SF42_Comany_ID__c` = `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c` ';
                $strSql2.= 'INNER JOIN `Group__c` ON `Group__c`.`Id` = `Acc`.`SF42_Company_Group__c` ';
                $strSql2.= 'WHERE `SF42_Month__c` = "' .date('n', $intThisMonth) .'" AND `SF42_Year__c` = "' .date('Y', $intThisMonth) .'" ';
                $strSql2.= 'AND `Group__c`.`Id` = "' .$arrGroup[0]['Id'] .'" AND `SF42_EventStatus__c` != "abgelehnt / Dublette" GROUP BY `SF42_EventStatus__c`';
                $arrEventList = MySQLStatic::Query($strSql2);

                if (count($arrEventList) > 0) { // Aktueller Monat mind. begonnen

                    if (in_array('enquired', $arrEventList[0])) {

                        $boolMonthFound = false;

                    } else {

                        $boolMonthFound = true;
                        
                        $arrPart[1] = date('Y', $intThisMonth);
                        $arrPart[2] = date('n', $intThisMonth);

                    }

                }

                if ($boolMonthFound === false) {

                    $strSql2 = 'SELECT `SF42_EventStatus__c` FROM `SF42_IZSEvent__c` ';
                    $strSql2.= 'INNER JOIN `Account` AS `Acc` ON `Acc`.`SF42_Comany_ID__c` = `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c` ';
                    $strSql2.= 'INNER JOIN `Group__c` ON `Group__c`.`Id` = `Acc`.`SF42_Company_Group__c` ';
                    $strSql2.= 'WHERE `SF42_Month__c` = "' .date('n', $intLastMonth1) .'" AND `SF42_Year__c` = "' .date('Y', $intLastMonth1) .'" ';
                    $strSql2.= 'AND `Group__c`.`Id` = "' .$arrGroup[0]['Id'] .'" AND `SF42_EventStatus__c` != "abgelehnt / Dublette" GROUP BY `SF42_EventStatus__c`';
                    $arrEventList = MySQLStatic::Query($strSql2);
    
                    if (count($arrEventList) > 0) { // Aktueller Monat mind. begonnen
    
                        if (in_array('enquired', $arrEventList[0])) {
    
                            $boolMonthFound = false;
    
                        } else {
    
                            $boolMonthFound = true;
                            
                            $arrPart[1] = date('Y', $intLastMonth1);
                            $arrPart[2] = date('n', $intLastMonth1);
    
                        }
    
                    }

                }

                if ($boolMonthFound === false) {

                    $strSql2 = 'SELECT `SF42_EventStatus__c` FROM `SF42_IZSEvent__c` ';
                    $strSql2.= 'INNER JOIN `Account` AS `Acc` ON `Acc`.`SF42_Comany_ID__c` = `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c` ';
                    $strSql2.= 'INNER JOIN `Group__c` ON `Group__c`.`Id` = `Acc`.`SF42_Company_Group__c` ';
                    $strSql2.= 'WHERE `SF42_Month__c` = "' .date('n', $intLastMonth2) .'" AND `SF42_Year__c` = "' .date('Y', $intLastMonth2) .'" ';
                    $strSql2.= 'AND `Group__c`.`Id` = "' .$arrGroup[0]['Id'] .'" AND `SF42_EventStatus__c` != "abgelehnt / Dublette" GROUP BY `SF42_EventStatus__c`';
                    $arrEventList = MySQLStatic::Query($strSql2);
    
                    if (count($arrEventList) > 0) { // Aktueller Monat mind. begonnen
    
                        if (in_array('enquired', $arrEventList[0])) {
    
                            $boolMonthFound = false;
    
                        } else {
    
                            $boolMonthFound = true;
                            
                            $arrPart[1] = date('Y', $intLastMonth2);
                            $arrPart[2] = date('n', $intLastMonth2);
    
                        }
    
                    }

                }

                if ($boolMonthFound === true) {

                    $strDisplayName = '' .sanitiseFileName($arrGroup[0]['Name'] .' - SV Prüfbericht ' .$arrPart[1] .'-' .str_pad($arrPart[2], 2, '0', STR_PAD_LEFT) .'');
                    $strFileName = $strDisplayName;
            
                    require_once('monatsbericht.inc.php');
            
                    header('Content-type: application/pdf');
                    header('Content-Disposition: inline; filename="' .$strFileName .'.pdf"');
                    //readfile(__DIR__ .$strOutputFile);
                    echo $strOutput;


                    $strInsert = 'INSERT INTO `izs_pruefbericht` (`pr_id`, `pr_group_id`, `pr_btnr`, `pr_month`, `pr_year`, `pr_file`, `pr_file_name`, `pr_cl_id`, `pr_time`) VALUES (';	
                    $strInsert.= 'NULL, "' .$arrGroup[0]['Id'] .'",  "' .$arrGroup[0]['Betriebsnummer__c'] .'", "' .$arrPart[2] .'", "' .$arrPart[1] .'", "' .$strOutputFile .'", "' .$strFileName .'", "' .@$_COOKIE['id'] .'", NOW())';
                    $intInsert = MySQLStatic::Insert($strInsert);

                    //file_put_contents($file, $current);

                    exit;
                }

            }

        }

    }

}

http_response_code(404);
//include('my_404.php'); // provide your own HTML for the error page
die();


?>