<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

require_once('../cms/const.inc.php');
require_once('../assets/classes/class.mysql.php');

require_once('../cms/inc/refactor.inc.php');

require_once ('../cms/fpdf182/fpdf.php');
require_once ('../cms/fpdf182/class.pdfhtml.php');
require_once ('../cms/fpdf182/class.merge.php');

$intDurationTime = microtime(true) - $intBeginTime;
if ($_REQUEST['d'] == 't') echo " Include: $intDurationTime Sek." .chr(10);

// INPUT
if (!isset($intEventId) || ($intEventId == '')) {
    //http_response_code(404);
    //include('my_404.php'); // provide your own HTML for the error page
    //die();
} 

if (!isset($strReturnType)) {
    $strReturnType = 'string';
}

$arrMonthList = array(
     1 => 'Januar',
     2 => 'Februar',
     3 => 'März',
     4 => 'April',
     5 => 'Mai',
     6 => 'Juni',
     7 => 'Juli',
     8 => 'August',
     9 => 'September',
    10 => 'Oktober',
    11 => 'November',
    12 => 'Dezember'
);


$arrStatusTanslate = array(
    ''            => 'gray',
    'grün'        => 'green',
    'gelb'        => 'yellow',
    'rot'         => 'red',
    'kein Status' => 'gray'
);


function createAccoutList ($strGroupId = '', $intMonth = 0, $intYear = 0) {

    $strSql = 'SELECT `Id`, `Name`, `SF42_Comany_ID__c`, `BillingCity`, `BillingStreet`, `BillingPostalCode`, `Subline__c`, `SF42_Payment_Status__c` FROM `Account` WHERE `SF42_Company_Group__c` = "' .$strGroupId .'"';
    $arrAccountList = MySQLStatic::Query($strSql);
    
    $arrReturn = array();

    if (count($arrAccountList) > 0) {
    
        foreach ($arrAccountList as $intKey => $arrAccount) {
        
            $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Betriebsnummer_ZA__c` = "' .$arrAccount['SF42_Comany_ID__c'] .'" AND `Beitragsmonat__c` = "' .$intMonth  .'" AND `Beitragsjahr__c` = "' .$intYear  .'" AND `SF42_EventStatus__c` IN ("to enquire", "enquired", "no result", "not OK", "OK") ORDER BY `Betriebsnummer_ZA__c`';
            $arrEventList = MySQLStatic::Query($strSql);
            
            $boolPayed = true;
            
            if (count($arrEventList) > 0) {
                
                $strBetrNr = $arrEventList[0]['Betriebsnummer_ZA__c'];
                
                foreach ($arrEventList as $intEventKey => $arrEvent) {
                    if (!in_array($arrEvent['SF42_EventStatus__c'], array('no result', 'not OK', 'OK')) ) {
                        $boolPayed = false;
                        break;
                    }
                }
                
                if ($boolPayed == true) {
                    $arrEventAccountList[strtolower($arrAccount['Name']) .'_' .$strBetrNr] = $arrAccount;
                }
            
            }
        
        }

        ksort($arrEventAccountList);

        foreach ($arrEventAccountList as $strKey => $arrAccount) {
            $arrReturn[] = $arrAccount;
        }

    }

    return $arrReturn;

}

function createEventList ($strBtnr = '', $intMonth = 0, $intYear = 0) {
    
    $arrReturn = array();
    
    if ($strBtnr != '') {
      
        $strSql = 'SELECT `SF42_IZSEvent__c`.`Id`, `SF42_IZSEvent__c`.`Name` AS `EvName`, `SF42_IZSEvent__c`.`SF42_informationProvider__c`, ';
        $strSql.= '`SF42_informationProvider__r`.`Name`, `SF42_EventStatus__c`, `SF42_EventComment__c`, `SF42_StatusFlag__c`, `Stundung__c`, ';
        $strSql.= '`SF42_PublishingStatus__c`, `SF42_DocumentUrl__c`, `Art_des_Dokuments__c`, `SF42_IZSEvent__c`.`RecordTypeId` ';
        $strSql.= 'AS `RecordType`, `Stundung__c` FROM `SF42_IZSEvent__c` INNER JOIN `Account` AS `SF42_informationProvider__r` ';
        $strSql.= 'ON `SF42_IZSEvent__c`.`SF42_informationProvider__c` = `SF42_informationProvider__r`.`Id` ';
        $strSql.= 'WHERE `SF42_Month__c` = "' .MySQLStatic::esc($intMonth) .'" AND `SF42_Year__c` = "' .MySQLStatic::esc($intYear) .'" ';
        $strSql.= 'AND `SF42_OnlineStatus__c` = "true" AND `Betriebsnummer_ZA__c` = "' .MySQLStatic::esc($strBtnr) .'" ';
        $strSql.= 'AND `SF42_EventStatus__c` != "abgelehnt / Dublette" AND `SF42_IZSEvent__c`.`RecordTypeId` != "01230000001Ao73AAC" ';
        $strSql.= 'ORDER BY `SF42_informationProvider__r`.`Name`';
        $arrResult = MySQLStatic::Query($strSql);
      
        if (count($arrResult) > 0) {
            $arrReturn = $arrResult;
        } 
      
    }
        
    return $arrReturn;
    
}

function addHeader (&$objPdf = null, $strType = '', $str1 = '', $str2 = '') {

    $objPdf->Image( __DIR__ .'/img/header.png', 0, 0, 297, 0, 'png'); 

    if ($strType == 'medium') {

        $floatX1 = 8;
        $objPdf->Image( __DIR__ .'/img/logo_medium.png', $floatX1, 9.7, 59.1, 0);

        $floatY1 = 12;
        $objPdf->SetXY(0, $floatY1);
        $objPdf->SetTextColor(96, 94, 85); //#605e55
        $objPdf->SetFont('Lato', 'B', 10);
        $objPdf->MultiCell(210 - $floatX1, 4, $str1, 0, 'R');

        $objPdf->SetXY(0, $floatY1 + 4);
        $objPdf->SetFont('Lato', '', 10);
        $objPdf->MultiCell(210 - $floatX1, 4, $str2, 0, 'R');


        $objPdf->SetDrawColor(239, 239, 239);
        $objPdf->SetLineWidth(0.4);
        $objPdf->Line(0, 28, 210, 28);
        
    }

}

function addFooter (&$objPdf = null) {

    $objPdf->Image( __DIR__ .'/img/footer.png', 0, 287.2, 297, 0, 'png'); 

    $strHtml = 'IZS Institut für Zahlungssicherheit GmbH  ' .chr(127) .'  Würmtalstraße 20a | 81375 München  ' .chr(127) .'  Telefon: +49 (0) 89 122 237 77 0  ' .chr(127) .'  E-Mail: <a href="mailto:sv-zertifikat@izs-institut.de">sv-zertifikat@izs-institut.de</a>';
    
    $floatX = 13.5;
    $objPdf->SetXY($floatX, 291.5);
    $objPdf->SetMargins($floatX, 10, 10.8);

    $objPdf->SetFont('OfficinaSerif', '', 8);
    $objPdf->SetTextColor(255, 255, 255); //#ffffff 
    $objPdf->setLinkColor(255, 255, 255, ''); //#ffffff 

    $objPdf->WriteHTML(utf8_decode($strHtml));

}

// OUTPUT
$strOutput = '';

$arrPdfLinkList = array();

$intDurationTime = microtime(true) - $intBeginTime;
if ($_REQUEST['d'] == 't') echo " Select: $intDurationTime Sek." .chr(10);

//print_r($arrSql); die();

$arrMergePdfList = array();

if (count($arrGroup) > 0) {

    $floatLeft = 10.8;
    $floatCol1 = 81.5;

    $objPdf = new pdfHtml('P');

    $objPdf->SetTitle($strDisplayName, true);

    $objPdf->AliasNbPages();

    $objPdf->AddFont('Lato', '', 'Lato-Regular.php');
    $objPdf->AddFont('Lato', 'B', 'Lato-Bold.php');

    $objPdf->AddFont('OfficinaSerif', '', 'itcofficinaserifw04medium-webfont.php');
    $objPdf->AddFont('OfficinaSerif', 'B', 'itcofficinaserifw04medium-webfont.php');
    
    $objPdf->AddPage();
    $objPdf->SetAutoPageBreak(false);

    $intDurationTime = microtime(true) - $intBeginTime;
    if ($_REQUEST['d'] == 't') echo " Init PDF: $intDurationTime Sek." .chr(10);
    
    
    addHeader($objPdf);    


    $intDurationTime = microtime(true) - $intBeginTime;
    if ($_REQUEST['d'] == 't') echo " Include BG: $intDurationTime Sek." .chr(10);

    //Page margin
    //$objPdf->SetMargins($floatLeft, 10.8);

    //Logo
    $objPdf->Image( __DIR__ .'/img/logo_large.png', 53.8, 18.2, 102.3, 0);

    $intDurationTime = microtime(true) - $intBeginTime;
    if ($_REQUEST['d'] == 't') echo " Include Logo: $intDurationTime Sek." .chr(10);

    $objPdf->Image( __DIR__ .'/img/line_01.png', 87.3, 55, 35.5, 0);

    $objPdf->SetXY(0, 81.9);
    $objPdf->SetTextColor(96, 94, 85); //#605e55
    $objPdf->SetFont('OfficinaSerif', 'B', 52);
    $objPdf->MultiCell(210, 4, utf8_decode(strtoupper('SV-PrÜfbericht')), 0, 'C');

    $objPdf->SetXY(0, 96);
    $objPdf->SetFont('OfficinaSerif', '', 20);
    $objPdf->MultiCell(210, 4, utf8_decode('der IZS Institut für Zahlungssicherheit GmbH'), 0, 'C');


    $floatY1 = 128.5;
    $objPdf->SetXY(0, $floatY1);
    $objPdf->SetFont('Lato', '', 11);
    $objPdf->MultiCell(210, 4, utf8_decode('UNTERNEHMENSGRUPPE'), 0, 'C');

    $objPdf->SetXY(0, $floatY1 + 7.7); // + 7.7
    $objPdf->SetFont('Lato', 'B', 16);
    $objPdf->MultiCell(210, 4, utf8_decode($arrGroup[0]['Name']), 0, 'C');


    $floatY1 = $floatY1 + 27.9;
    $objPdf->SetXY(0, $floatY1);
    $objPdf->SetFont('Lato', '', 11);
    $objPdf->MultiCell(210, 4, utf8_decode('STATUS & BEITRAGSMONAT'), 0, 'C');


    //ggf. Historische Daten berechnen
    //if (($arrPart[2] == (date('n') - 1)) && ($arrPart[1] == date('Y'))) {

    $stSvStatus = $arrGroup[0]['Status_SV__c'];
    $strHinweis = $arrGroup[0]['Hinweis_sichtbar_auf_Portal__c'];

    $stSvStatus = $arrStatusTanslate[$stSvStatus];


    switch ($stSvStatus) {

        case 'gray':   $intR = 149; $intG  = 149; $intB = 149; break;
        case 'green':  $intR =  71; $intG  = 173; $intB = 147; break;
        case 'yellow': $intR = 236; $intG  = 191; $intB =  89; break;
        case 'red':    $intR = 241; $intG  =  82; $intB =  73; break;

    }


    $floatX1 = 105 - ($objPdf->GetStringWidth('       ' .$arrMonthList[$arrPart[2]] .' ' .$arrPart[1]) / 2) - 3.5;

    $objPdf->SetDrawColor($intR, $intG, $intB); 
    $objPdf->SetFillColor($intR, $intG, $intB); 
    $objPdf->Circle($floatX1, $floatY1 + 9.6, 2.2, 'DF');

    $objPdf->SetXY(0, $floatY1 + 7.7); // + 7.7
    $objPdf->SetFont('Lato', 'B', 16);
    $objPdf->MultiCell(210, 4, utf8_decode('       ' .$arrMonthList[$arrPart[2]] .' ' .$arrPart[1]), 0, 'C');


    /*
    $floatY1 = $floatY1 + 27.9;
    $objPdf->SetXY(0, $floatY1);
    $objPdf->SetFont('Lato', '', 11);
    $objPdf->MultiCell(210, 4, utf8_decode('HINWEIS'), 0, 'C');

    $objPdf->SetXY(0, $floatY1 + 7.7); // + 7.7
    $objPdf->SetFont('Lato', 'B', 16);
    $objPdf->MultiCell(210, 4, utf8_decode('-'), 0, 'C');
    */


    $floatY1 = $floatY1 + 27.9;
    $objPdf->SetXY(0, $floatY1);
    $objPdf->SetFont('Lato', '', 11);
    $objPdf->MultiCell(210, 4, utf8_decode('ERSTELLUNGSZEITPUNKT'), 0, 'C');

    $objPdf->SetXY(0, $floatY1 + 7.7); // + 7.7
    $objPdf->SetFont('Lato', 'B', 16);
    $objPdf->MultiCell(210, 4, utf8_decode(date('d.m.Y | H:i:s') .' CET'), 0, 'C');

    $objPdf->SetXY(16, 252.4);
    $objPdf->SetFont('Lato', '', 13);
    $objPdf->MultiCell(210-32, 6, utf8_decode('Grundlage der Prüfung waren alle ordnungsgemäß und fristgerecht zum Beitragsmonat übermittelten Beitragsnachweisdaten der Unternehmensgruppe.'), 0, 'C');


    addFooter($objPdf);


    // PAGE 2

    $arrAccList = createAccoutList($arrPart[0], $arrPart[2], $arrPart[1]);

    if (count($arrAccList) > 0) {

        $boolFinish = false;

        $intStart = 0;
        $intStop  = count($arrAccList);


        while ($boolFinish == false) {

            $objPdf->AddPage();
            $objPdf->SetAutoPageBreak(false);

            $str1 = utf8_decode('SV-PRÜFBERICHT | ' .$arrMonthList[$arrPart[2]] .' ' .$arrPart[1]);
            $str2 = utf8_decode($arrGroup[0]['Name']);

            addHeader($objPdf, 'medium', $str1, $str2);

            $floatX1 = 8;

            $objPdf->SetXY($floatX1 + 1, 42);
            $objPdf->SetFont('Lato', 'B', 16);
            $objPdf->MultiCell(210, 4, utf8_decode('GEPRÜFTE MELDESTELLEN'), 0, 'L');


            $objPdf->SetDrawColor(239, 239, 239);
            $objPdf->SetLineWidth(0.2);

            $floatY1 = 59.3;
            $objPdf->SetXY($floatX1 + 2, $floatY1);
            $objPdf->SetFont('Lato', 'B', 7);
            $objPdf->MultiCell(74, 4, utf8_decode('Meldestelle'), 0, 'L');

            $objPdf->SetXY(90, $floatY1);
            $objPdf->MultiCell(24.5, 4, utf8_decode('BetriebsNr'), 0, 'C');

            $objPdf->SetXY(114.5 + 5, $floatY1);
            $objPdf->MultiCell(19.5, 4, utf8_decode('Ort'), 0, 'L');

            $objPdf->SetXY(144 + 5, $floatY1);
            $objPdf->MultiCell(48, 4, utf8_decode('Hinweis'), 0, 'L');

            $objPdf->Line($floatX1, 65, 210 - $floatX1, 65);


            $floatX1 = 8;
            $floatY1 = 67;

            
            if (count($arrAccList) > 0) {

                for ($intCount = $intStart; ($intCount < $intStop); $intCount++) {

                    if ($intCount == $intStart) {
                        $floatYOffset = 0;
                    }

                    $objPdf->SetFont('Lato', '', 8);
                    $floatHeight1 = $objPdf->GetMultiCellHeight(68.4, 3.5, utf8_decode($arrAccList[$intCount]['Name']), 0, 'L');
                    $objPdf->SetFont('Lato', '', 7);
                    $floatHeight3 = $objPdf->GetMultiCellHeight(29.5 - 5, 3, utf8_decode($arrAccList[$intCount]['BillingCity']), 0, 'L');
                    $objPdf->SetFont('Lato', '', 7);
                    $floatHeight4 = $objPdf->GetMultiCellHeight(58 - 5 - 5, 3, utf8_decode($arrAccList[$intCount]['Subline__c']), 0, 'L');

                    //echo $floatHeight4 .chr(10);

                    $floatMaxHeight = max($floatHeight1, $floatHeight3, $floatHeight4);

                    //echo $intCount .'/' .count($arrAccList) .chr(10);

                    if (($floatY1 + $floatYOffset + $floatMaxHeight) > 270) {
                        $intStart = $intCount;
                        //echo 'Next start: ' .$intStart .chr(10);
                        //echo 'PageBreak' .chr(10) .chr(10);
                        break;
                    }

                    //echo $floatMaxHeight .chr(10);

                    switch (strtolower($arrAccList[$intCount]['SF42_Payment_Status__c'])) {
                        case '':       $intR = 149; $intG  = 149; $intB = 149; break;
                        case 'gray':   $intR = 149; $intG  = 149; $intB = 149; break;
                        case 'green':  $intR =  71; $intG  = 173; $intB = 147; break;
                        case 'yellow': $intR = 236; $intG  = 191; $intB =  89; break;
                        case 'red':    $intR = 241; $intG  =  82; $intB =  73; break;
                    }

                    $objPdf->SetDrawColor($intR, $intG, $intB); 
                    $objPdf->SetFillColor($intR, $intG, $intB); 
                    $objPdf->Circle($floatX1 + 4, $floatY1 + 3.2 + $floatYOffset, 1.5, 'DF');

                    $objPdf->SetXY($floatX1 + 7.6, $floatY1 + 1.6 + $floatYOffset);
                    $objPdf->SetFont('Lato', '', 8);

                    $intLink = $objPdf->AddLink();
                    $arrPdfLinkList[$arrAccList[$intCount]['Id']] = $intLink;
                    //$objPdf->SetLink($intLink, 0, 1);
                    $objPdf->MultiCell(68.4, 3.5, utf8_decode($arrAccList[$intCount]['Name']), 0, 'L', false, $intLink);

                    $objPdf->SetXY($floatX1 + 82, $floatY1 + 1.6 + $floatYOffset);
                    $objPdf->SetFont('Lato', '', 7);
                    $objPdf->MultiCell(24.5, 3, utf8_decode($arrAccList[$intCount]['SF42_Comany_ID__c']), 0, 'C');

                    $objPdf->SetXY($floatX1 + 106.5 + 5, $floatY1 + 1.6 + $floatYOffset);
                    $objPdf->SetFont('Lato', '', 7);
                    $objPdf->MultiCell(29.5 - 5, 3, utf8_decode($arrAccList[$intCount]['BillingCity']), 0, 'L');

                    //($floatHeight4 == 5) ? $floatYCorrect = - 1.25 : $floatYCorrect = 0;
                    $floatYCorrect = 0;

                    $objPdf->SetXY($floatX1 + 136 + 5, $floatY1 + 1.6 + $floatYOffset + $floatYCorrect);
                    $objPdf->SetFont('Lato', '', 7);
                    $objPdf->MultiCell(58 - 5 - 5, 3, utf8_decode($arrAccList[$intCount]['Subline__c']), 0, 'L');

                    if ($floatMaxHeight <= 3) {
                        $floatYOffset+= 10;
                    } else {
                        $floatYOffset = $floatYOffset + $floatMaxHeight + 7;
                    }
                    

                    $objPdf->SetDrawColor(239, 239, 239);
                    $objPdf->Line($floatX1, $floatY1 + $floatYOffset - 2, 210 - $floatX1, $floatY1 + $floatYOffset - 2);

            
                }

            }


            addFooter($objPdf);


            if ($intCount >= $intStop) {
                $boolFinish = true;
                //echo 'finish' .chr(10);
                break;
            } else {
                //echo $intStop .chr(10);
                //die();
            }

            //die();


        }

    }

    //print_r($arrPdfLinkList); die();

    //Deckblatt und Meldestellen-Liste
    $strTempFileName = 'tmp/' .md5(uniqid(mt_rand(), true)) .'.pdf';
    $objPdf->Output($strTempFileName, 'F');

    $arrMergePdfList[] = $strTempFileName;

    //die();


    // PAGE 3

    if (count($arrAccList) > 0) {

        $boolFinish = false;

        $intStart = 0;

        foreach ($arrAccList as $intKey => $arrAccount) {


            if ($intStart == 0) {
                $arrEventList = createEventList($arrAccount['SF42_Comany_ID__c'], $arrPart[2], $arrPart[1]);

                $intStart = 0;
                $intStop  = count($arrEventList);
            }

            $objPdf = new pdfHtml('P');

            $objPdf->SetTitle($strDisplayName, true);
        
            $objPdf->AliasNbPages();
        
            $objPdf->AddFont('Lato', '', 'Lato-Regular.php');
            $objPdf->AddFont('Lato', 'B', 'Lato-Bold.php');
        
            $objPdf->AddFont('OfficinaSerif', '', 'itcofficinaserifw04medium-webfont.php');
            $objPdf->AddFont('OfficinaSerif', 'B', 'itcofficinaserifw04medium-webfont.php');

            $objPdf->AddPage();
            $objPdf->SetAutoPageBreak(false);

            $objPdf->SetLink($arrPdfLinkList[$arrAccount['Id']], 0, -1);

            $str1 = utf8_decode('SV-PRÜFBERICHT | ' .$arrMonthList[$arrPart[2]] .' ' .$arrPart[1]);
            $str2 = utf8_decode($arrGroup[0]['Name']);

            addHeader($objPdf, 'medium', $str1, $str2);

            $floatX1 = 8;

            $objPdf->SetXY($floatX1 + 1, 42);
            $objPdf->SetFont('Lato', 'B', 16);
            $objPdf->MultiCell(210, 4, utf8_decode($arrAccount['Name']), 0, 'L');

            $objPdf->SetXY($floatX1 + 1, 48);
            $objPdf->SetFont('Lato', '', 10);
            $objPdf->MultiCell(210, 4, utf8_decode(str_replace(chr(10), '', $arrAccount['BillingStreet'] . ' | ' .$arrAccount['BillingPostalCode'] .' ' .$arrAccount['BillingCity'] . ' | Betriebsnummer: ' .$arrAccount['SF42_Comany_ID__c'])), 0, 'L');


            $objPdf->SetDrawColor(239, 239, 239);
            $objPdf->SetLineWidth(0.2);
        
            $floatY1 = 59.3;
            $objPdf->SetXY($floatX1 + 2, $floatY1);
            $objPdf->SetFont('Lato', 'B', 7);
            $objPdf->MultiCell(98, 4, utf8_decode('Krankenkasse'), 0, 'L');
        
            $objPdf->SetXY(116, $floatY1);
            $objPdf->MultiCell(81, 4, utf8_decode('Stundung / Ratenzahlung'), 0, 'L');
        
            $objPdf->Line($floatX1, 65, 210 - $floatX1, 65);
        
        
            $floatX1 = 8;
            $floatY1 = 67;

            if (count($arrEventList) > 0) {

                for ($intCount = $intStart; ($intCount < $intStop); $intCount++) {

                    if ($intCount == $intStart) {
                        $floatYOffset = 0;
                    }

                    $objPdf->SetFont('Lato', '', 8);
                    $floatHeight1 = $objPdf->GetMultiCellHeight(89.4, 3.5, utf8_decode($arrEventList[$intCount]['Name']), 0, 'L');
                    $objPdf->SetFont('Lato', '', 7);
                    $floatHeight2 = $objPdf->GetMultiCellHeight(91 - 5 - 5, 3, utf8_decode($arrEventList[$intCount]['Stundung__c']), 0, 'L');

                    //echo $floatHeight4 .chr(10);

                    $floatMaxHeight = max($floatHeight1, $floatHeight2);

                    //echo $intCount .'/' .count($arrAccList) .chr(10);

                    if (($floatY1 + $floatYOffset + $floatMaxHeight) > 270) {

                        $floatYOffset = 0;

                        addFooter($objPdf);
                        
                        $objPdf->AddPage();
                        $objPdf->SetAutoPageBreak(false);

                        $str1 = utf8_decode('SV-PRÜFBERICHT | ' .$arrMonthList[$arrPart[2]] .' ' .$arrPart[1]);
                        $str2 = utf8_decode($arrGroup[0]['Name']);
            
                        addHeader($objPdf, 'medium', $str1, $str2);

                        $floatX1 = 8;
            
                        $objPdf->SetXY($floatX1 + 1, 42);
                        $objPdf->SetFont('Lato', 'B', 16);
                        $objPdf->MultiCell(210, 4, utf8_decode($arrAccount['Name']), 0, 'L');
            
                        $objPdf->SetXY($floatX1 + 1, 48);
                        $objPdf->SetFont('Lato', '', 10);
                        $objPdf->MultiCell(210, 4, utf8_decode(str_replace(chr(10), '', $arrAccount['BillingStreet'] . ' | ' .$arrAccount['BillingPostalCode'] .' ' .$arrAccount['BillingCity'] . ' | Betriebsnummer: ' .$arrAccount['SF42_Comany_ID__c'])), 0, 'L');
            
            
                        $objPdf->SetDrawColor(239, 239, 239);
                        $objPdf->SetLineWidth(0.2);
                    
                        $floatY1 = 59.3;
                        $objPdf->SetXY($floatX1 + 2, $floatY1);
                        $objPdf->SetFont('Lato', 'B', 7);
                        $objPdf->MultiCell(98, 4, utf8_decode('Krankenkasse'), 0, 'L');
                    
                        $objPdf->SetXY(116, $floatY1);
                        $objPdf->MultiCell(81, 4, utf8_decode('Stundung / Ratenzahlung'), 0, 'L');
                    
                        $objPdf->Line($floatX1, 65, 210 - $floatX1, 65);

                        $floatX1 = 8;
                        $floatY1 = 67;

                    }

                    //echo $floatMaxHeight .chr(10);

                    switch (strtolower($arrEventList[$intCount]['SF42_EventStatus__c'])) {
                        case 'ok':  $intR =  71; $intG  = 173; $intB = 147; break;
                        case 'not ok':    $intR = 241; $intG  =  82; $intB =  73; break;
                    }

                    if (($arrEventList[$intCount]['Stundung__c'] != '') && ($arrEventList[$intCount]['SF42_EventStatus__c'] == 'OK')) {
                        $intR = 236; $intG = 191; $intB = 89;
                    }

                    $objPdf->SetDrawColor($intR, $intG, $intB); 
                    $objPdf->SetFillColor($intR, $intG, $intB); 
                    $objPdf->Circle($floatX1 + 4, $floatY1 + 3.2 + $floatYOffset, 1.5, 'DF');

                    $objPdf->SetXY($floatX1 + 7.6, $floatY1 + 1.6 + $floatYOffset);
                    $objPdf->SetFont('Lato', '', 8);
                    $objPdf->MultiCell(89.4, 3.5, utf8_decode($arrEventList[$intCount]['Name']), 0, 'L');

                    $objPdf->SetXY($floatX1 + 82, $floatY1 + 1.6 + $floatYOffset);
                    $objPdf->SetFont('Lato', '', 7);
                    $objPdf->MultiCell(91 - 5 - 5, 3, utf8_decode($arrEventList[$intCount]['Stundung__c']), 0, 'C');

                    if ($floatMaxHeight <= 3) {
                        $floatYOffset+= 10;
                    } else {
                        $floatYOffset = $floatYOffset + $floatMaxHeight + 7;
                    }
                    

                    $objPdf->SetDrawColor(239, 239, 239);
                    $objPdf->Line($floatX1, $floatY1 + $floatYOffset - 2, 210 - $floatX1, $floatY1 + $floatYOffset - 2);

            
                }

            }


            addFooter($objPdf);

            $strTempFileName = 'tmp/' .md5(uniqid(mt_rand(), true)) .'.pdf';
            $objPdf->Output($strTempFileName, 'F');
        
            $arrMergePdfList[] = $strTempFileName;


            // ZERTIFIKATE

            $strLink = 'https://www.izs.de/monatsreport.php?detailId=' .$arrAccount['Id'] .'&selectedMonth=' .$arrPart[2] .'&selectedYear=' .$arrPart[1] .'';
            $strTempPdfFile = file_get_contents($strLink);
            $strTempFileName = 'tmp/' .md5(uniqid(mt_rand(), true)) .'.pdf';

            file_put_contents($strTempFileName, $strTempPdfFile);

            $arrMergePdfList[] = $strTempFileName;


        }

    }


    $intDurationTime = microtime(true) - $intBeginTime;
    if ($_REQUEST['d'] == 't') echo " Process PDF: $intDurationTime Sek." .chr(10);

    /*
    if ($strReturnType == 'string') {
        $strOutput = $objPdf->Output('S'); //Output($strFileName, 'F')
        //echo $strOutput; die();
    }
    */

    //die();
    //echo __DIR__ .'/' .$arrGroup[0]['Id'] .'.pdf';
    //die();

    if (count($arrMergePdfList) > 0) {

        $objMerge = new FPDF_Merge();
    
        foreach ($arrMergePdfList as $intKey => $strRelativePath) {
    
          $objMerge->add($strRelativePath);
          unlink($strRelativePath);
    
        }
        
        $strOutputFile = '/done/' .$arrGroup[0]['Id'] .'_' .date('dmYHis') .'.pdf';
        $objMerge->output(__DIR__ .$strOutputFile);

        $strShellOutput = shell_exec('exiftool -Title="' .$strDisplayName .'" -overwrite_original ' .__DIR__ .$strOutputFile);
    
      }


}

?>