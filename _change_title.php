<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

ini_set("memory_limit", "-1");
set_time_limit(0);

require_once('assets/classes/class.mysql.php');

$strPdfDir  = __DIR__ .'/cms/server/php/files/';
$strLogFile = __DIR__ .'/cms/server/php/change_list.csv';

$strBackupFolder = __DIR__ .'/cms/server/php/backup/';


if (!file_exists($strBackupFolder)) {
    mkdir($strBackupFolder, 0777);
}

//$arrFileList = glob($strPdfDir .'a*.pdf'); 
$arrFileList = glob($strPdfDir .'*.pdf'); // 948681.pdf

$boolLogExists = false;
if (file_exists($strLogFile)) {
    $boolLogExists = true;
}

$arrLogList = array();
$intRow = 0;

if ($boolLogExists) {
    $resLog = fopen($strLogFile, 'r');
    while (($arrCsvData = fgetcsv($resLog, 1000, ';')) !== false) {
        $intColNum = count($arrCsvData);
        if ($intRow > 0) {
            $arrLogList[$arrCsvData[$intColNum - 1]] = $arrCsvData;
        }
        $intRow++;
    }
    fclose($resLog);
}

$resLog = fopen($strLogFile, 'a');

if ($boolLogExists === false) {
    fputs($resLog, $strBom = (chr(0xEF) .chr(0xBB) .chr(0xBF)));
    fputcsv($resLog, array('Time', 'File', 'Www', 'Title old', 'Title new'), ';');
}

$intFileCount  = 1;
$intFileChange = 1;

foreach ($arrFileList as $intCount => $strfilePath) {

    $arrPart = pathinfo($strfilePath);
    $strEventName = $arrPart['filename']; // 987654, a063000000LpsGtAAJ

    ///*
    $strPdfContent = file_get_contents($strfilePath);

    $strTitle = '';
    $intTitleType = 2;

    preg_match_all('/\/Title \((.{1,})\)/', $strPdfContent, $arrTitle);

    if ((count($arrTitle) == 2) && (isset($arrTitle[1][0]))) {

        $strTitle = $arrTitle[1][0];
        $intTitleType = 1;

    } /* else {

        preg_match_all('/<dc:title>\s*<rdf:Alt>\s*<rdf:li xml:lang="x-default">(.*?)<\/rdf:li>\s*<\/rdf:Alt>\s*<\/dc:title>/i', $strPdfContent, $arrTitle);

        if ((count($arrTitle) == 2) && (isset($arrTitle[1][0]))) {
            $strTitle = $arrTitle[1][0];
            $intTitleType = 2;
        }

    }
    */

    //$strSql = 'SELECT `evid`, `Name`FROM `SF42_IZSEvent__c` WHERE `Id` = "' .$strEventName .'"';
    //$arrSql = MySQLStatic::Query($strSql);

    //if ((count($arrSql) > 0)) { // ($strTitle != '') && Has title and db entry

        // Event ID wo E-
        $strEventId = $strEventName; //substr($arrSql[0]['Name'], 2); // 987654

        // Allready processed?
        if (isset($arrLogList[$strEventId .'.pdf'])) continue; 
            
        // Backup
        $strBackupFile = str_replace('files/', 'backup/', $strfilePath);
        //if (file_exists($strBackupFile)) { unlink($strBackupFile); }
        $strTerminalOutput = shell_exec('cp -p ' .$strfilePath .' ' .$strBackupFile .'');

        //$intTitleType = 2; // a0...pdf

        // make Changes
        if ($intTitleType == 1) {
            
            $strContentUpdated = preg_replace('/\/Title \(.*\)/', '/Title (' .$strEventId .'.pdf' .')', $strPdfContent);
            file_put_contents($strfilePath, $strContentUpdated);

        } elseif ($intTitleType == 2) {
            
            $strShellOutput = shell_exec('exiftool -Title="' .$strEventId .'.pdf' .'" -overwrite_original ' .$strfilePath);
            $intFileChange++;
            //$strContentUpdated = str_replace('>' .$strTitle .'<', '>' .$strEventId .'.pdf<', $strPdfContent);
        
        }
        
        // Log changes
        $strTitle = 'changed';
        fputcsv($resLog, array(date('d.m.Y H:i:s'), $strfilePath, 'https://www.izs.de/cms/server/php/files/' .$strEventName .'.pdf', $strTitle, $strEventId .'.pdf'), ';');
        
    /*
    } else {
        $strTitle = 'no changes';
        fputcsv($resLog, array(date('d.m.Y H:i:s'), $strfilePath, 'https://www.izs.de/cms/server/php/files/' .$strEventName .'.pdf', $strTitle, ''), ';');

    }
    */

    $intFileCount++;

    if ($intFileCount >= 100000) break;

}

fclose($resLog);

echo $intFileCount .' files executed. Changed: ' .$intFileChange .' files.';

?>