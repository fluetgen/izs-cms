<?php

// Konfigurationsdaten für die beiden Server
$oldServerConfig = [
    'host' => 'db-test.cj2k22uuw73e.eu-central-1.rds.amazonaws.com',
    'user' => 'U1246131-2',
    'password' => '442b4cID',
    'database' => 'salesforce_dev_01',
];

$newServerConfig = [
    'host' => 'www.izs.de',
    'user' => 'U1246131-2',
    'password' => '442b4cID',
    'database' => 'salesforce_dev_01',
];

// Verbindung mit MySQL-Servern herstellen
function connectToServer($config) {
    $mysqli = new mysqli($config['host'], $config['user'], $config['password'], $config['database']);
    if ($mysqli->connect_error) {
        die("Verbindung fehlgeschlagen: " . $mysqli->connect_error);
    }
    return $mysqli;
}

// Trigger-Definitionen abrufen
function fetchTriggers($mysqli, $databaseName) {
    $triggers = [];
    $result = $mysqli->query("
        SELECT TRIGGER_NAME 
        FROM INFORMATION_SCHEMA.TRIGGERS 
        WHERE TRIGGER_SCHEMA = '$databaseName'
    ");

    while ($row = $result->fetch_assoc()) {
        $triggerName = $row['TRIGGER_NAME'];
        $definitionResult = $mysqli->query("SHOW CREATE TRIGGER `$triggerName`");
        $definitionRow = $definitionResult->fetch_assoc();
        $triggers[$triggerName] = $definitionRow['SQL Original Statement'];
    }
    return $triggers;
}

// Definitionen vergleichen
function compareTriggers($newTriggers, $oldTriggers) {
    foreach ($newTriggers as $triggerName => $newDefinition) {
        echo "-- Vergleiche Trigger: $triggerName\n";
        if (!isset($oldTriggers[$triggerName])) {
            echo "-- Trigger `$triggerName` fehlt auf dem alten Server.\n";
            echo "DELIMITER $$\n$newDefinition $$\nDELIMITER ;\n";
        } elseif (normalizeDefinition($newDefinition) !== normalizeDefinition($oldTriggers[$triggerName])) {
            echo "-- Unterschied in der Definition von Trigger `$triggerName`:\n";
            echo "-- Neue Definition:\nDELIMITER $$\n$newDefinition $$\nDELIMITER ;\n";
            echo "-- Alte Definition:\nDELIMITER $$\n" . $oldTriggers[$triggerName] . " $$\nDELIMITER ;\n";
        } else {
            echo "-- Trigger `$triggerName` ist identisch.\n";
        }
        echo str_repeat("--", 40) . "\n";
    }

    foreach ($oldTriggers as $triggerName => $oldDefinition) {
        if (!isset($newTriggers[$triggerName])) {
            echo "-- Trigger `$triggerName` fehlt auf dem neuen Server.\n";
        }
    }
}

// Definitionen normalisieren (DEFINER und Backticks entfernen)
function normalizeDefinition($definition) {
    // Entfernt den DEFINER-Abschnitt
    $definition = preg_replace('/DEFINER=`[^`]+`@`[^`]+`\s+/i', '', $definition);
    // Entfernt Backticks um Namen
    $definition = preg_replace('/`/', '', $definition);
    // Entfernt überflüssige Leerzeichen und normalisiert
    return preg_replace('/\s+/', ' ', trim($definition));
}

// Hauptlogik
$newMysqli = connectToServer($newServerConfig);
$oldMysqli = connectToServer($oldServerConfig);

$newTriggers = fetchTriggers($newMysqli, $newServerConfig['database']);
$oldTriggers = fetchTriggers($oldMysqli, $oldServerConfig['database']);

compareTriggers($newTriggers, $oldTriggers);

$newMysqli->close();
$oldMysqli->close();

?>