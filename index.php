<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if ($_SERVER['REMOTE_ADDR'] == '146.52.239.75') {
  //print_r($_REQUEST);
  //die();
}

require_once('assets/classes/class.mysql.php');

if (!isset($_REQUEST['q'])) {

  header('Location: https://www.izs-institut.de/', true, 302); 

} else {
  
  preg_match('/e-(\d*)/', $_REQUEST['q'], $arrId);

  if (count($arrId) == 2) {

    $strSql  = 'SELECT `ev_hash` FROM `izs_event` WHERE `evid` = "' .MySQLStatic::esc($arrId[1]) .'"';
    $arrHash = MySQLStatic::Query($strSql);

    if (count($arrHash) > 0) {

      //echo 'Location: https://' .$_SERVER['SERVER_NAME'] .'/cms/event/' .$arrHash[0]['ev_hash']; die();
      header('Location: https://' .$_SERVER['SERVER_NAME'] .'/cms/event/' .$arrHash[0]['ev_hash']); 
      exit;

    }

  }

  $arrPart = explode('/', $_REQUEST['q']);

  if ((count($arrPart) == 3) && (($arrPart[2] == 'download') || ($arrPart[2] == 'abonnieren'))) {

    header('Location: https://test.izs.de/redirect.php?r=' .$_REQUEST['q']);
    exit;

  }

}

require_once('redirector.inc.php');
require_once('shorturls.inc.php');

?>
