<?php

error_reporting(E_ALL);

require_once('assets/classes/class.mysql.php');

$arrMonth = array(
     1 => 'Januar',
     2 => 'Februar',
     3 => 'März',
     4 => 'April',
     5 => 'Mai',
     6 => 'Juni',
     7 => 'Juli',
     8 => 'August',
     9 => 'September',
    10 => 'Oktober',
    11 => 'November',
    12 => 'Dezember'
);

$strSql = 'SELECT * FROM `Group__c` WHERE `Id` = "' . $_REQUEST['Id'] . '"';
$arrGroup = MySQLStatic::Query($strSql);

if (($arrGroup[0]['Gesamtstatus__c'] == 'rot') || (($_REQUEST['strSelDate'] == '9_2021') && ($_REQUEST['Id'] == 'a084S0000004hpiQAA'))) { //Schwartpaul Sonderlocke!!!
    $strTemplatePath = 'cms/templates/pruefzertifikat_mail_nocert.tpl.html';
} else {
    $strTemplatePath = 'cms/templates/pruefzertifikat_mail.tpl.html';
}

$strTemplate = file_get_contents($strTemplatePath);

$_REQUEST['Id'] = rawurldecode($_REQUEST['Id']);

$boolMail = false;

require_once('cms/parseTemplate.inc.php');

header('Content-Type: text/html; charset=utf-8');

echo $strTemplate;

?>