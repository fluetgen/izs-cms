<?php

// Konfigurationsdaten für die beiden Server
$oldServerConfig = [
    'host' => 'db-test.cj2k22uuw73e.eu-central-1.rds.amazonaws.com',
    'user' => 'U1246131-2',
    'password' => '442b4cID',
    'database' => 'salesforce_dev_01',
];

$newServerConfig = [
    'host' => 'www.izs.de',
    'user' => 'U1246131-2',
    'password' => '442b4cID',
    'database' => 'salesforce_dev_01',
];

// Verbindung mit MySQL-Servern herstellen
function connectToServer($config) {
    $mysqli = new mysqli($config['host'], $config['user'], $config['password'], $config['database']);
    if ($mysqli->connect_error) {
        die("Verbindung fehlgeschlagen: " . $mysqli->connect_error);
    }
    return $mysqli;
}

// View-Definitionen abrufen
function fetchViews($mysqli, $databaseName) {
    $views = [];
    $result = $mysqli->query("
        SELECT TABLE_NAME 
        FROM INFORMATION_SCHEMA.VIEWS 
        WHERE TABLE_SCHEMA = '$databaseName'
    ");

    while ($row = $result->fetch_assoc()) {
        $viewName = $row['TABLE_NAME'];
        $definitionResult = $mysqli->query("SHOW CREATE VIEW `$viewName`");
        $definitionRow = $definitionResult->fetch_assoc();
        $views[$viewName] = $definitionRow['Create View'];
    }
    return $views;
}

// Definitionen vergleichen
function compareViews($newViews, $oldViews) {
    foreach ($newViews as $viewName => $newDefinition) {
        echo "-- Vergleiche View: $viewName\n";
        if (!isset($oldViews[$viewName])) {
            echo "-- View `$viewName` fehlt auf dem alten Server.\n";
            echo "$newDefinition;\n";
        } elseif (normalizeDefinition($newDefinition) !== normalizeDefinition($oldViews[$viewName])) {
            echo "-- Unterschied in der Definition von View `$viewName`:\n";
            echo "-- Neue Definition:\n$newDefinition;\n";
            echo "-- Alte Definition:\n" . $oldViews[$viewName] . ";\n";
        } else {
            echo "-- View `$viewName` ist identisch.\n";
        }
        echo str_repeat("--", 40) . "\n";
    }

    foreach ($oldViews as $viewName => $oldDefinition) {
        if (!isset($newViews[$viewName])) {
            echo "-- View `$viewName` fehlt auf dem neuen Server.\n";
        }
    }
}

// Definitionen normalisieren (DEFINER entfernen)
function normalizeDefinition($definition) {
    // Entfernt den DEFINER-Abschnitt
    $definition = preg_replace('/DEFINER=`[^`]+`@`[^`]+`\s+/i', '', $definition);
    // Entfernt überflüssige Leerzeichen und normalisiert
    return preg_replace('/\s+/', ' ', trim($definition));
}

// Hauptlogik
$newMysqli = connectToServer($newServerConfig);
$oldMysqli = connectToServer($oldServerConfig);

$newViews = fetchViews($newMysqli, $newServerConfig['database']);
$oldViews = fetchViews($oldMysqli, $oldServerConfig['database']);

compareViews($newViews, $oldViews);

$newMysqli->close();
$oldMysqli->close();

?>