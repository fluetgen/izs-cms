<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('fpdi/FPDI_Protection.php');
require_once('../assets/classes/global.inc.php');
require_once('../assets/classes/class.mysql.php');

//print_r($_REQUEST); die();

// https://www.izs.de/protect/2021/3/a084S0000004hpYQAQ_2021_3_vollmacht_sv.pdf
// redirector.php -> https://www.izs.de/index.php?q=protect/2021/3/a084S0000004hpYQAQ_2021_3_vollmacht_sv.pdf
// https://www.izs.de/download/2021/3/a084S0000004hpYQAQ_2021_3_vollmacht_sv.pdf

if (isset($_REQUEST['q']) && ($_REQUEST['q'] != '')) {

    if (!isset($_REQUEST['id'])) {
        $_REQUEST['id'] = '';
    }

    $arrPathInfo = pathinfo($_REQUEST['q']);
    $strFilePath = '../download/' .$arrPathInfo['dirname'] .'/' .$arrPathInfo['basename'];

    $boolExists = file_exists($strFilePath);

    if ($boolExists && ($arrPathInfo['extension'] == 'pdf')) {

    
    if ($_REQUEST['id'] != '') { //Knappschaft

        $strSql = 'SELECT `PDF_Passwortschutz`, `PDF_Passwort` FROM `Account` WHERE `Id` = "' .$_REQUEST['id'] .'"';
        $arrSql = MySQLStatic::Query($strSql);

        if ($arrSql[0]['PDF_Passwort'] != '') {
            $strPassword = $arrSql[0]['PDF_Passwort'];
        }

    } 
    
    if ($strPassword == '') {
        $strPassword = '12345';
    }
    

    $strOrigFile = $strFilePath;
    $strDestFile = $arrPathInfo['basename'];

    $pdf = new FPDI_Protection();
    $pdf->__construct('P', 'in');
    
    $intPageCount = $pdf->setSourceFile($strOrigFile);
    
    for ($intCount = 1; $intCount <= $intPageCount; $intCount++) {
        $resTpl = $pdf->importPage($intCount);
        $pdf->addPage();
        $pdf->useTemplate($resTpl);
    }

    $pdf->SetProtection(array(), $strPassword);
    $pdf->Output($strDestFile, 'D'); // F
    exit;

    }

}

echo 'File not found.';


?>