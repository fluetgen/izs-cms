<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once('../classes/class.mysql.php');
require_once('../classes/class.database.php');

$boolIsIzs = false;
$intUserId = 0;
$boolShow  = true;

if (isset($_COOKIE) && (count($_COOKIE) > 0)) {

    $strToken = 'triple2013';

    if (isset($_COOKIE['id']) && isset($_COOKIE['token']) && ($_COOKIE['token'] == MD5($strToken .'_' .$_COOKIE['id']))) {

        $boolIsIzs = true;
        $intUserId = $_COOKIE['id'];

    }


}

if (!$boolIsIzs) {

    $arrPath = pathinfo($_REQUEST['path']);

    $arrPart = explode('_', $arrPath['filename']);
    //
    $strSql = 'SELECT * FROM `Group__c` WHERE `Id` = "' .MySQLStatic::esc($arrPart[0]) .'"';
    $arrGrp = MySQLStatic::Query($strSql);

    if ((count($arrGrp) > 0) && ($arrGrp[0]['Kooperation_beendet_am__c'] != '0000-00-00')) {
        if (strtotime($arrGrp[0]['Kooperation_beendet_am__c']) < strtotime('- 6 weeks')) {
            $boolShow = false;
        }
    }

}


if (isset($_REQUEST['path']) && ($_REQUEST['path'] != '') && ($boolShow)) {

    $arrPath = pathinfo($_REQUEST['path']);

    if ($arrPath['extension'] == 'pdf') {

        if (strstr(__DIR__, 'test.') !== false) {

            $strPath = '/data/www/sites/test.izs.de/html/assets/pdf-zertifikate/' .$_REQUEST['path'];

        } else {

            $strPath = '/data/www/sites/www.izs.de/html/assets/pdf-zertifikate/' .$_REQUEST['path'];

        }

        if (!file_exists($strPath)) {

            $arrPart = explode('_', $arrPath['filename']);

            $_REQUEST['Id'] = $arrPart[0];
            $_REQUEST['strSelDate'] = $arrPart[2] .'_' .$arrPart[1];
            $_REQUEST['redirect'] = 'no';

            require_once('../../cms/createZertifikat.php');

            //createZertifikat.inc.php?Id=' .$arrPart[0] .'&strSelDate=<MONTH>_<YEAR>  
        }

        if (!file_exists($strPath)) {

            header("HTTP/1.1 404 Not Found");
            exit;

        } else {

            $arrPart = explode('_', $arrPath['filename']);

            if (!$boolIsIzs) {
                //File exits and is allowed to be delivered
                $strSql = 'SELECT `id_id` FROM `izs_download` WHERE `id_type` = "svcert" AND `id_year` = "' .MySQLStatic::esc($arrPart[1]) .'" AND `id_month` = "' .MySQLStatic::esc($arrPart[2]) .'" AND `id_group` = "' .MySQLStatic::esc($arrPart[0]) .'" AND `id_date` = "' .MySQLStatic::esc(date('Y-m-d')) .'"';
                $arrDow = MySQLStatic::Query($strSql);

                if (count($arrDow) > 0) {
                    $strSql = 'UPDATE `izs_download` SET `id_count` = `id_count` + 1 WHERE `id_id` = "' .MySQLStatic::esc($arrDow[0]['id_id']) .'"';
                    $intSql = MySQLStatic::Update($strSql);
                } else {
                    $strSql = 'INSERT INTO `izs_download` (`id_id`, `id_type`, `id_account`, `id_group`, `id_ip`, `id_month`, `id_year`, `id_ev`, `id_date`, `id_time`, `id_last_ip`, `id_count`) ';
                    $strSql.= 'VALUES (NULL, "svcert", "", "' .MySQLStatic::esc($arrPart[0]) .'", ';
                    $strSql.= '"", "' .MySQLStatic::esc($arrPart[2]) .'", "' .MySQLStatic::esc($arrPart[1]) .'", ';
                    $strSql.= '"' .MySQLStatic::esc($arrPath['filename']) .'", NOW(), NOW(), "' .$_SERVER['REMOTE_ADDR'] .'", "1")';
                    $intSql = MySQLStatic::Insert($strSql);
                }
            }

            $strName = $_REQUEST['path'];
            header('Content-Transfer-Encoding: binary');  // For Gecko browsers mainly
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($strPath)) . ' GMT');
            header('Accept-Ranges: bytes');  // For download resume
            header('Content-Length: ' . filesize($strPath));  // File size
            header('Content-Encoding: none');
            header('Content-Type: application/pdf');  // Change this mime type if the file is not PDF
            //header('Content-Disposition: download; filename=' . $strName);  // Make the browser display the Save As dialog
            header('Content-Disposition: inline; filename=' . $strName);  // browse file
            readfile($strPath); 
            exit;
            
        }


    }

}

if (!$boolShow && (isset($arrGrp[0]['Id']))) {

    header('Location: https://www.izs-institut.de/portal.html#!/company/' .$arrGrp[0]['Id']);
    exit;

} else {

    header("HTTP/1.1 404 Not Found");
    exit;

}


/*
$intBorder = 4 * 7 * 84400; // 4 WOCHEN

$arrFile = pathinfo($_REQUEST['path']);

if (isset($_REQUEST['path']) && ($_REQUEST['path'] != '') && (!isset($_REQUEST['d']))) {

    $arrNamePart = explode('_', $arrFile['filename']);

    // PROOVE IF STILL CUSTOMER
    if (is_array($arrNamePart) && (count($arrNamePart) > 0)) {
        $strSql = 'SELECT * FROM `Group__c` WHERE `Id` = "' .MySQLStatic::esc($arrNamePart[0]) .'"';
        $arrRes = MySQLStatic::Query($strSql);

        if (is_array($arrRes) && (count($arrRes) > 0)) { //Kooperation_beendet_am__c
            
            if (($arrRes[0]['Kooperation_beendet_am__c'] != '') && ($arrRes[0]['Kooperation_beendet_am__c'] != '0000-00-00')) {
                $intToday  = date('U');
                $intCancel = strtotime($arrRes[0]['Kooperation_beendet_am__c']);

                if (($intToday - $intCancel) > $intBorder) {

                    // https://www.izs-institut.de/portal.html#!/company/a083A00000nDpJXQA0
                    $strUrl = 'https://www.izs-institut.de/portal.html#!/company/' .$arrNamePart[0];

                    header("HTTP/1.1 301 Moved Permanently");
                    header("Location: ". $strUrl);
                    exit;

                }

            }

            $strUrl = 'https://www.izs-institut.de/events/pdf-zertifikate.php?path=' .$_REQUEST['path'];

            header("HTTP/1.1 301 Moved Permanently");
            header("Location: ". $strUrl);
            exit;            

        }

        //print_r($arrRes); die();
    }

} elseif ((isset($_REQUEST['d'])) && ($_REQUEST['d'] == 1)) {

    if ($arrPath['extension'] == 'pdf') {

        $strPath = '/data/www/sites/www.izs.de/html/assets/pdf-zertifikate/' .$_REQUEST['path'];

        if (!file_exists($strPath)) {

            header("HTTP/1.1 404 Not Found");
            exit;
        
        } else {

            $strName = $_REQUEST['path'];
            header('Content-Transfer-Encoding: binary');  // For Gecko browsers mainly
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($strPath)) . ' GMT');
            header('Accept-Ranges: bytes');  // For download resume
            header('Content-Length: ' . filesize($strPath));  // File size
            header('Content-Encoding: none');
            header('Content-Type: application/pdf');  // Change this mime type if the file is not PDF
            //header('Content-Disposition: download; filename=' . $strName);  // Make the browser display the Save As dialog
            header('Content-Disposition: inline; filename=' . $strName);  // browse file
            readfile($strPath); 
            
            exit;

        }


    }

}

header("HTTP/1.1 404 Not Found");
header("Location: /404.php" );
*/

?>