<?php


require_once('global_test.inc.php');

class MySQLStatic {

    private static $resCon = null;


    public static function connectDb ()  {

        if (is_null(self::$resCon)) {
            
            self::$resCon = @mysqli_connect(
                $GLOBALS['mysql']['config']['server'],
                $GLOBALS['mysql']['config']['user'],
                $GLOBALS['mysql']['config']['pass'], 
                $GLOBALS['mysql']['config']['database']
            );

            if (!self::$resCon) {
                die ("Database connection error");
            }

            mysqli_query(self::$resCon, "SET NAMES 'utf8'");

        }

    }

    public static function FilterToSql ($arrFilter = array()) {
        
        $strCondition = '';
    
        $intCountFilter = count($arrFilter);
        if ($intCountFilter > 0) {
          
          foreach ($arrFilter as $intKey => $arrFilter) {
            if (isset($arrFilter['boolQuote']) && ($arrFilter['boolQuote'] === false)) {
              $strCondition.= '' .$arrFilter['strKey'] .' ' .$arrFilter['strType'] .' ' .$arrFilter['strValue'] .'';
            } else {
              $strCondition.= '`' .$arrFilter['strKey'] .'` ' .$arrFilter['strType'] .' "' .$arrFilter['strValue'] .'"';
            }
            if ($intKey < ($intCountFilter - 1)) {
              $strCondition.= ' AND ';
            }
            $strCondition.= ' ';
          }
          
        }

        return $strCondition;
    }

        
    public static function UpdateFields ($arrFields = array()) {
    
        $strReturn = '';
        
        if (is_array($arrFields) && (count($arrFields) > 0)) {

            $intField = 1;
            
            foreach ($arrFields as $strKey => $strValue) {
                
                $strValue = str_replace('"', '\\"', $strValue);
        
                if (($strValue == 'NULL') || ($strValue == 'NOW()')) {
                    $strValue = $strValue;
                } else {
                    $strValue = '"' .$strValue .'"';
                }
                $strReturn.= '`' .$strKey .'` = ' .$strValue .'';

                if ($intField < count($arrFields)) {
                    $strReturn.= ', ';
                }
                
                $intField++;  
            }
    
        }
        
        return $strReturn;
      
    }

    
    public static function InsertFields ($arrFields = array()) {
    
        $strReturn = '';
        
        if (is_array($arrFields) && (count($arrFields) > 0)) {
        
            $strKeyList = '';
            $strValueList = '';
            
            $intFieldCount = count($arrFields);
            $intField = 1;
            
            foreach ($arrFields as $strKey => $strValue) {
                
                $strKeyList.= '`' .$strKey .'`';
                
                $strValue = str_replace('"', '\\"', $strValue);
        
                if (($strValue == 'NULL') || ($strValue == 'NOW()')) {
                    $strValueList.= $strValue;
                } else {
                    $strValueList.= '"' .$strValue .'"';
                }
                
                if ($intField < $intFieldCount) {
                    $strKeyList.= ', ';
                    $strValueList.= ', ';
                }
                
                $intField++;  
            }
        
            $strReturn = '(' .$strKeyList .') VALUES (' .$strValueList .')';   
    
        }
        
        return $strReturn;
      
    }
    
    public static function ImplodeFields ($arrFields = array()) {
    
        $strReturn = '';
        
        if (is_array($arrFields) && (count($arrFields) > 0)) {

            if (strstr($arrFields[0][0], '`') !== false) {
                $strReturn = '' .implode(', ', $arrFields) .'';
            } else {
                $strReturn = '`' .implode('`, `', $arrFields) .'`';
            }
        
        } else {
            $strReturn = '1'; 
        }
        
        return $strReturn;
    
    }
    
    public static function Insert ($strSql) {
    
        MySQLStatic::connectDb();
    
        $strSql = str_replace('``', '`', $strSql);
        $strSql = str_replace('´´', '´', $strSql);
                        
        $rsResult = mysqli_query(self::$resCon, $strSql);
    
        if (mysqli_error(self::$resCon)) {
            throw new \Exception('Error in Sql Statement (insert): "' .$strSql .'" with error "' .mysqli_error(self::$resCon) .'"', E_WARNING);
        }
        if ($rsResult) {
            //error_log('[' .getmypid() .' - ' .date('Y-m-d H:i:s') .'] - ' .$strSql ."\n", 3, './sql.log');
            return mysqli_insert_id(self::$resCon);
        } else {
            return false;
        }
    }
    
    public static function Query ($strSql, $strSqlType = 'MYSQL_ASSOC') {
    
        MySQLStatic::connectDb();

        $strSql = str_replace('``', '`', $strSql);
        $strSql = str_replace('´´', '´', $strSql);
    
        $arrRowset = array();
        $rsResult = mysqli_query(self::$resCon, $strSql);
    
        if (is_object($rsResult)) {
            if ($strSqlType == 'MYSQL_NUM') {
                $strSqlType = 2;
            } elseif ($strSqlType == 'MYSQL_BOTH') {
                $strSqlType = 3;
            } else {
                $strSqlType = 1;
            }
            while ($row = mysqli_fetch_array($rsResult, $strSqlType)) {
                $arrRowset[] = $row;
            }
            //error_log('[' .getmypid() .' - ' .date('Y-m-d H:i:s') .'] - ' .$strSql ."\n", 3, './sql.log');
            return $arrRowset;
        }
        
        if (mysqli_error(self::$resCon)) {
            throw new \Exception('Error in Sql Statement (query): "' .$strSql .'" with error "' .mysqli_error(self::$resCon) .'"', E_WARNING);
        }
    
        return $arrRowset;
    }
    
    public static function Update ($strSql) {
    
        MySQLStatic::connectDb();
    
        $intAffectedRows = array();
        mysqli_query(self::$resCon, $strSql);
        $intAffectedRows = mysqli_affected_rows(self::$resCon);
    
        if (mysqli_error(self::$resCon)) {
            throw new \Exception('Error in Sql Statement (update): "' .$strSql .'" ' .mysqli_error(self::$resCon), E_WARNING);
        }
        return $intAffectedRows;
    }
    
    public static function close () {
    
        mysqli_close(self::$resCon);
    
    }
    
    public static function esc ($inp) {
    
        if(is_array($inp)) {
            return array_map(__METHOD__, $inp);
        }
        
        if(!empty($inp) && is_string($inp)) {
    
            return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
        
        } elseif (!empty($inp)) {
        
          return $inp;
        
        }
        
    }

    public static function tableExists ($strTable) {

        $boolReturn = false;

        if ($strTable != '') {
            $strSql = 'SELECT * FROM `information_schema`.`tables` WHERE `table_schema` = "' .self::esc($GLOBALS['database']['config']['database']) .'" ';
            $strSql.= 'AND `table_name` = "' .self::esc($strTable) .'" LIMIT 1;';
            $arrSql = self::Query($strSql);

            if (count($arrSql) > 0) $boolReturn = true; 
        }

        return $boolReturn;
    
    }

    public static function removeTable ($strTableName) {

        if (self::tableExists($strTableName)) {
            $strSql = 'DROP TABLE IF EXISTS `' .self::esc($strTableName) .'`;';
            $arrSql = self::Query($strSql);
        }
    
    }

    public static function renameTable ($strTableNameOld = '', $strTableNameNew = '', $boolForce = false) {

        if (self::tableExists($strTableNameOld)) {

            if (self::tableExists($strTableNameNew)) {
                if ($boolForce) {
                    $strSql = 'DROP TABLE IF EXISTS `' .self::esc($strTableNameNew) .'`;';
                    $arrSql = self::Query($strSql);
                } else {
                    return false;
                }
            }

            $strSql = 'RENAME TABLE `' .self::esc($strTableNameOld) .'` TO `' .self::esc($strTableNameNew) .'`;';
            $arrSql = self::Query($strSql);

        } else {
            return false;
        }
    
    }

    public static function copyTable ($strTableNameOld = '', $strTableNameNew = '', $boolForce = false) {

        if (self::tableExists($strTableNameOld)) {

            if (self::tableExists($strTableNameNew)) {
                if ($boolForce) {
                    $strSql = 'DROP TABLE IF EXISTS `' .self::esc($strTableNameNew) .'`;';
                    $arrSql = self::Query($strSql);
                } else {
                    return false;
                }
            }

            $strSql = 'CREATE TABLE `' .self::esc($strTableNameNew) .'` LIKE `' .self::esc($strTableNameOld) .'`;';
            $arrSql = self::Query($strSql);
            $strSql = 'INSERT `' .self::esc($strTableNameNew) .'` SELECT * FROM `' .self::esc($strTableNameOld) .'`;';
            $arrSql = self::Query($strSql);

        } else {
            return false;
        }
    
    }

    public static function getAutoincrement ($strTableName = '') {

        if (self::tableExists($strTableName)) {

            $intReturn = 0;

            $strSql = 'SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = "' .$GLOBALS['database']['config']['database'] .'" AND TABLE_NAME = "' .self::esc($strTableName) . '"';
            $arrResult = self::Query($strSql);
            $intCountResult = count($arrResult);
            
            if ($intCountResult > 0) {
              $intReturn = $arrResult[0]['AUTO_INCREMENT'];
            }
            
            return $intReturn;

        } else {
            return false;
        }
    
    }

    public static function disarm ($str) {
    
        return strip_tags(trim($str));
    
    }
    
}

?>
