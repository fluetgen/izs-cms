<?php

include ('config.core.php');
include (MODX_CORE_PATH .MODX_CONFIG_KEY .'/' .MODX_CONFIG_KEY .'.inc.php');

class ModX {
  
  private static $arrRedirects = array();
  private static $strSearch = '%unternehmensinfo.html?companyid=%';
  private static $strLink = 'http://www.izs.de/unternehmensinfo.html?companyid=';

  function __construct () {
    
  }
  
  public function boolGetRedirects () {
    
    $boolReturn = false;
    
    $strSql = 'SELECT `Id`, `Direktlink_zum_Konto__c` FROM `Group__c` WHERE `Direktlink_zum_Konto__c` != "" ORDER BY `Direktlink_zum_Konto__c`';
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      self::$arrRedirects = $arrResult;
      $boolReturn = true;
    }
    
    return $boolReturn;

  }
  
  public function boolDeleteRedirects () {
    
    global $database_server, $database_user, $database_password, $dbase;
    
    $boolReturn = false;
    
		$mysqlCon = @mysql_connect($database_server, $database_user, $database_password);
		mysql_select_db($dbase, $mysqlCon);
		mysql_query("SET NAMES 'utf8'");
    
    $strSql = 'DELETE FROM `modx_redirects` WHERE `target` LIKE "' .self::$strSearch .'"';
    $arrResult = mysql_query($strSql, $mysqlCon);
    $boolReturn = true;
    
    return $boolReturn;

  }
  
  public function boolWriteRedirects () {
    
    global $database_server, $database_user, $database_password, $dbase;
    
    $boolReturn = false;
    
		$mysqlCon = @mysql_connect($database_server, $database_user, $database_password);
		mysql_select_db($dbase, $mysqlCon);
		mysql_query("SET NAMES 'utf8'");
    
    if (count(self::$arrRedirects) > 0) {
      foreach (self::$arrRedirects as $arrRe) {
        $strSql = 'INSERT INTO `modx_redirects` (`id`, `pattern`, `target`) VALUES (NULL, "' .$arrRe['Direktlink_zum_Konto__c'] .'", "' .self::$strLink .$arrRe['Id'] .'")';
        $arrResult = mysql_query($strSql, $mysqlCon);
      }
      $boolReturn = true;
    }
    
    return $boolReturn;

  }

}

?>