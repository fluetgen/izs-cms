<?php

require_once('global.inc.php');

require_once(SOAP_CLIENT_BASEDIR.'/SforcePartnerClient.php');
require_once(SOAP_CLIENT_BASEDIR.'/SforceHeaderOptions.php');
require_once(SOAP_CLIENT_BASEDIR.'/SforceMetadataClient.php');

class SForceStatic {

	private static $sforceCon = null;
	private static $sforceMetaCon = null;

	public static function connectDb ()  {

		if (is_null(self::$sforceCon)) {

      $mySforceConnection = new SforcePartnerClient();
      $mySforceConnection->createConnection(SOAP_CLIENT_BASEDIR .'/' .SOAP_WSDL);
      $loginResult = $mySforceConnection->login($GLOBALS['sforce']['config']['user'], $GLOBALS['sforce']['config']['pass']);
      
      //print_r($mySforceConnection);
      self::$sforceCon = $mySforceConnection;
      //self::$sforceMetaCon = new SforceMetadataClient(SOAP_CLIENT_BASEDIR .'/' .META_WSDL, $loginResult, self::$sforceCon);

			if (!self::$sforceCon) {
			    die ("Database connection error");
			}

		}

	}

  public static function Insert ($arrData = array(), $strTable = '') {

    $insertResult = array();
  	SForceStatic::connectDb();
  	
  	if (is_array($arrData) && ($strTable != '')) {
      $sObject = new stdclass();
      $sObject->fields = $arrData;
      $sObject->type = $strTable;
      $insertResult = self::$sforceCon->create(array ($sObject));
    }
    
    return $insertResult;

  }

  public static function Query ($strSql) {

  	SForceStatic::connectDb();

    $arrRowset = array();
    
    try {
      
      $queryResult = self::$sforceCon->query($strSql);
      $records = $queryResult->records;
      
      foreach ($records as $record) {
        $sObject = new SObject($record);

        if ($sObject->Id != '') {
          $arrFileds['Id'] = $sObject->Id;
        }        
        
        foreach($sObject->fields as $strKey => $strValue) {
          if (is_object($strValue)) {
            foreach($strValue->fields as $strKey2 => $strValue2) {
              $arrFileds[$strKey2] = $strValue2;
            }
          } else {
            $arrFileds[$strKey] = $strValue;
          }
        }
        $arrRowset[] = $arrFileds;
      }
      
    } catch (Exception $e) {
      
      echo 'Error in Sql Statement (query): "' .$strSql .'"' .'<br />' .chr(10);
      echo $e->faultstring;
        
    }
    
    return $arrRowset;
  }


  public static function Update ($arrData = array(), $strTable = '', $Id = '') {

    $updateResult = array();
  	SForceStatic::connectDb();
  	
  	if (is_array($arrData) && ($strTable != '') && (strlen($Id) > 0)) {
  	  
  	  $arrNull = array();
  	  
  	  foreach ($arrData as $intKey => $strValue) {
  	    if ($strValue == '') {
  	      $arrNull[] = $intKey;
  	      unset($arrData[$intKey]);
  	    }
  	  }
  	  
  	  //print_r($arrNull); die();
  	  
      $sObject = new stdclass();
      $sObject->fields = $arrData;
      $sObject->type = $strTable;
      $sObject->Id = $Id;
      
  	  $sObject->fieldsToNull = $arrNull;
      
      $updateResult = self::$sforceCon->update(array ($sObject));
    }
    
    return $updateResult;

  }

  public static function Delete ($arrData = array()) {

    $deleteResult = array();
  	SForceStatic::connectDb();
    
  	if (is_array($arrData)) {
      $deleteResult = self::$sforceCon->delete($arrData);
    }
    
    return $deleteResult;

  }
  
  public static function DescribeObject ($strTable = '') {
    
    $describeResult = array();
  	SForceStatic::connectDb();
  	
  	if ($strTable != '') {

      $objSf = self::$sforceCon->describeSObject($strTable);
      $intFieldCount = count($objSf->fields);
    
      $arrFields = array();
      $arrFieldsInfo = array();
    
    
      for ($intCount = 0; $intCount < $intFieldCount; $intCount++) {
        if ($objSf->fields[$intCount]->type != 'base64') {
          //Don't transfer binary data. Too large.
          $arrFields[$objSf->fields[$intCount]->name] = $objSf->fields[$intCount]->label;
          $arrFieldsInfo[$objSf->fields[$intCount]->name] = array(
            'type'   => $objSf->fields[$intCount]->type,
            'length' => $objSf->fields[$intCount]->length,
            'ref'    => $objSf->fields[$intCount]->referenceTo,
            'pick'   => $objSf->fields[$intCount]->picklistValues
          );
          
        }
      }
    
      $describeResult = array ('fields' => $arrFields, 'info' => $arrFieldsInfo);
    
    }
    
    return $describeResult;
  }

  public static function SetQueryOptions ($objOptions = null) {

      SForceStatic::connectDb();
      
      if (is_a($objOptions)) {
        $objSf = self::$sforceCon->setQueryOptions($objOptions);
      }

  }

  public static function esc ($str) {

      return mysql_escape_string($str);

  }

  public static function disarm ($str) {

      return strip_tags(trim($str));

  }

}

?>