<?php

class Replication {
  
  private $strSuffix = '_temp';

  function __construct () {
    
  }
  
  public function strGetSuffix () {
  
    return $this->strSuffix;
  
  }
  
  public function strCreate ($strTable = '', $arrFields = array(), $arrInfo = array()) {
    
    $strReturn = '';
    $strReturn.= 'CREATE TABLE `' .$strTable .$this->strSuffix .'` (' .chr(10);
    
    $arrIndex = array();
    
    foreach ($arrFields as $strField => $strFieldDesc) {
      
      $strFieldT = '  `' .$strField .'`';
      $strReturn.= $strFieldT;
      
      $strFieldInfo = $arrInfo[$strField]['type'];
      
      switch ($strFieldInfo) {
        case 'id': $strReturn.= ' VARCHAR(18) NOT NULL, '; break;
        case 'boolean': $strReturn.= ' VARCHAR(5) NOT NULL, '; break;
        case 'reference': $strReturn.= ' VARCHAR(18) NOT NULL, '; $arrIndex[] = $strFieldT; break;
        case 'string': $strReturn.= ' VARCHAR(' .$arrInfo[$strField]['length'] .') NULL, '; break;
        case 'picklist': $strReturn.= ' VARCHAR(255) NOT NULL, '; break;
        case 'textarea': $strReturn.= ' TEXT NULL, '; break;
        case 'phone': $strReturn.= ' VARCHAR(255) NOT NULL, '; break;
        case 'url': $strReturn.= ' VARCHAR(255) NOT NULL, '; break;
        case 'currency': $strReturn.= ' VARCHAR(10) NULL, '; break;
        case 'int': $strReturn.= ' INT(11) NOT NULL, '; break;
        case 'datetime': $strReturn.= ' DATETIME NOT NULL, '; break; //2012-02-01T14:18:24.000Z
        case 'date': $strReturn.= ' DATE NOT NULL, '; break; //???
        case 'email': $strReturn.= ' VARCHAR(255) NOT NULL, '; break;
        case 'double': $strReturn.= ' DOUBLE NOT NULL, '; break;
        case 'multipicklist': $strReturn.= ' TEXT NULL, '; break;
        default: $strReturn.= ' VARCHAR(255) NOT NULL, '; break;
      }
      
      $strReturn.= '' .chr(10);
    }
    
    $strReturn.= 'PRIMARY KEY (`id`)';
    
    if (count($arrIndex) > 0) { 
      $strReturn.= ', ' .chr(10);
      $strReturn.= 'INDEX (' .implode(', ', $arrIndex) .')' .chr(10);
    }
    
    $strReturn.= ') ENGINE = MYISAM ;' .chr(10);
    
    return $strReturn;

  }

}

/*
CREATE TABLE `reporting`.`_test` (
`field1` INT( 11 ) NOT NULL ,
`field2` INT( 11 ) NOT NULL ,
`field3` INT( 11 ) NOT NULL ,
INDEX ( `field1` , `field2` , `field3` )
) ENGINE = MYISAM ;
*/

?>