<?php

require_once('global.inc.php');

class MySQLStatic {

	private static $mysqlCon = null;

	public static function connectDb ()  {

		if (is_null(self::$mysqlCon)) {
		  
			self::$mysqlCon = @mysql_connect($GLOBALS['mysql']['config']['server'],
								 $GLOBALS['mysql']['config']['user'],
								 $GLOBALS['mysql']['config']['pass']);

			if (!self::$mysqlCon) {
			    echo mysql_error();
			    die ("Database connection error");
			}
			mysql_select_db($GLOBALS['mysql']['config']['database'], self::$mysqlCon);
			mysql_query("SET NAMES 'utf8'");

		}

	}

  public static function ImplodeFields ($arrFields = array()) {

    $strReturn = '';
    
    if (is_array($arrFields) && (count($arrFields) > 0)) {
    
      $strReturn = implode(', ', $arrFields);   
    
    } else {
      $strReturn = '1'; 
    }
    
    return $strReturn;

  }

  public static function Insert ($strSql) {

  	MySQLStatic::connectDb();

    $rsResult = mysql_query($strSql, self::$mysqlCon);

    if (mysql_error()) {
      throw new Exception('Error in Sql Statement (insert): "' .$strSql .'" with error "' .mysql_error() .'"', E_WARNING);
    }
    if ($rsResult) {
        //error_log('[' .getmypid() .' - ' .date('Y-m-d H:i:s') .'] - ' .$strSql ."\n", 3, './sql.log');
      return mysql_insert_id(self::$mysqlCon);
    } else {
      return false;
    }
  }

  public static function Query ($strSql, $strSqlType = 'MYSQL_ASSOC') {

  	MySQLStatic::connectDb();

    $arrRowset = array();
    $rsResult = mysql_query($strSql, self::$mysqlCon);

    if (is_resource ($rsResult)) {
      if ($strSqlType == 'MYSQL_NUM') {
        $strSqlType = 2;
      } elseif ($strSqlType == 'MYSQL_BOTH') {
        $strSqlType = 3;
      } else {
        $strSqlType = 1;
      }
      while ($row = mysql_fetch_array($rsResult, $strSqlType)) {
        $arrRowset[] = $row;
      }
      //error_log('[' .getmypid() .' - ' .date('Y-m-d H:i:s') .'] - ' .$strSql ."\n", 3, './sql.log');
      return $arrRowset;
    }
    
    if (mysql_error()) {
      throw new Exception('Error in Sql Statement (query): "' .$strSql .'" with error "' .mysql_error() .'"', E_WARNING);
    }

    return $arrRowset;
  }

  public static function Update ($strSql) {

  	MySQLStatic::connectDb();

    $intAffectedRows = array();
    mysql_query($strSql, self::$mysqlCon);
    $intAffectedRows = mysql_affected_rows(self::$mysqlCon);

    if (mysql_error()) {
        throw new Exception('Error in Sql Statement (update): "' .$strSql .'" ' .mysql_error(), E_WARNING);
    }
    return $intAffectedRows;
  }

  public static function close () {

      mysql_close (self::$mysqlCon);

  }

  public static function esc ($str) {
    
    MySQLStatic::connectDb();
    
    return mysql_real_escape_string($str, self::$mysqlCon);

  }

  public static function disarm ($str) {

      return strip_tags(trim($str));

  }

}

?>