<?php

// Konfigurationsdaten für die beiden Server
$oldServerConfig = [
    'host' => 'db-test.cj2k22uuw73e.eu-central-1.rds.amazonaws.com',
    'user' => 'U1246131-2',
    'password' => '442b4cID',
    'database' => 'salesforce_dev_01',
];

$newServerConfig = [
    'host' => 'www.izs.de',
    'user' => 'U1246131-2',
    'password' => '442b4cID',
    'database' => 'salesforce_dev_01',
];

// Ausschlussliste
$excludeTables = ['ks_cache', 'izs_sg_hook', '_cron_running', '_cron_url', 'ks_cache_triggers'];

// Verbindung mit MySQL-Servern herstellen
function connectToServer($config) {
    $mysqli = new mysqli($config['host'], $config['user'], $config['password'], $config['database']);
    if ($mysqli->connect_error) {
        die("Verbindung fehlgeschlagen: " . $mysqli->connect_error);
    }
    return $mysqli;
}

// Alle SQL-Dateien im Ausgabeordner löschen
function clearOutputDirectory($directory) {
    if (!is_dir($directory)) return;
    $files = glob($directory . '/*.sql'); // Alle .sql-Dateien finden
    foreach ($files as $file) {
        if (is_file($file)) {
            unlink($file); // Datei löschen
        }
    }
}

// Daten der Tabelle abrufen (Primärschlüssel + Index)
function fetchTableData($mysqli, $tableName, $primaryKey) {
    $data = [];
    $result = $mysqli->query("SELECT `$primaryKey` FROM `$tableName`");
    if (!$result) {
        throw new Exception("Fehler beim Abrufen der Daten aus `$tableName`: " . $mysqli->error);
    }
    while ($row = $result->fetch_assoc()) {
        $data[$row[$primaryKey]] = $row[$primaryKey];
    }
    return $data;
}

// Datensätze löschen (Schritt 1)
function findAndDeleteExtraRecords($newData, $oldData, $tableName, $primaryKey) {
    $deleteSQL = [];
    foreach ($oldData as $key => $value) {
        if (!isset($newData[$key])) {
            $deleteSQL[] = "DELETE FROM `$tableName` WHERE `$primaryKey` = '$key';";
        }
    }
    return $deleteSQL;
}

// Datensätze einfügen (Schritt 2)
function findAndInsertMissingRecords($newMysqli, $newData, $oldData, $tableName, $primaryKey) {
    $insertSQL = [];
    foreach ($newData as $key => $value) {
        if (!isset($oldData[$key])) {
            $result = $newMysqli->query("SELECT * FROM `$tableName` WHERE `$primaryKey` = '$value'");
            if (!$result) {
                throw new Exception("Fehler bei der Abfrage in `$tableName` für `$key`: " . $newMysqli->error);
            }
            $row = $result->fetch_assoc();
            if ($row) {
                $columns = implode(", ", array_map(fn($col) => "`$col`", array_keys($row)));
                $values = implode(", ", array_map(fn($val) => is_null($val) ? "NULL" : "'" . addslashes($val) . "'", array_values($row)));
                $insertSQL[] = "INSERT INTO `$tableName` ($columns) VALUES ($values);";
            }
        }
    }
    return $insertSQL;
}

// Tabelleninhalt vergleichen
function compareTableData($newMysqli, $oldMysqli, $tableName, &$allSQLHandler) {
    global $outputDir, $excludeTables;

    // Wenn die Tabelle in der Ausschlussliste ist, überspringen
    if (in_array($tableName, $excludeTables)) {
        echo "-- Tabelle `$tableName` ist in der Ausschlussliste. Überspringe Vergleich.\n";
        return;
    }

    // Finden des Primärschlüssels der Tabelle
    $primaryKey = fetchPrimaryKey($newMysqli, $tableName);
    if (!$primaryKey) {
        echo "-- Keine Primärschlüssel in der Tabelle `$tableName` gefunden. Überspringe Vergleich.\n";
        return;
    }

    // Abrufen der Primärschlüsseldaten
    $newData = fetchTableData($newMysqli, $tableName, $primaryKey);
    $oldData = fetchTableData($oldMysqli, $tableName, $primaryKey);

    // SQL-Befehle für Änderungen
    $deleteSQL = findAndDeleteExtraRecords($newData, $oldData, $tableName, $primaryKey);
    $insertSQL = findAndInsertMissingRecords($newMysqli, $newData, $oldData, $tableName, $primaryKey);

    // Wenn keine Änderungen vorliegen, Ausgabe in der Konsole
    if (empty($deleteSQL) && empty($insertSQL)) {
        echo "-- Keine Änderungen in der Tabelle `$tableName`.\n";
        return;
    }

    // Änderungen in einer Datei speichern
    $filePath = "$outputDir/$tableName.sql";
    $fileHandler = fopen($filePath, 'w');

    if (!empty($deleteSQL)) {
        fwrite($fileHandler, "-- SQL-Befehle zum Löschen überzähliger Datensätze in `$tableName`:\n");
        fwrite($allSQLHandler, "-- SQL-Befehle zum Löschen überzähliger Datensätze in `$tableName`:\n");
        foreach ($deleteSQL as $sql) {
            fwrite($fileHandler, $sql . "\n");
            fwrite($allSQLHandler, $sql . "\n");
        }
    }

    if (!empty($insertSQL)) {
        fwrite($fileHandler, "-- SQL-Befehle zum Einfügen fehlender Datensätze in `$tableName`:\n");
        fwrite($allSQLHandler, "-- SQL-Befehle zum Einfügen fehlender Datensätze in `$tableName`:\n");
        foreach ($insertSQL as $sql) {
            fwrite($fileHandler, $sql . "\n");
            fwrite($allSQLHandler, $sql . "\n");
        }
    }

    fclose($fileHandler);

    echo "-- Änderungen für Tabelle `$tableName` wurden in `$filePath` gespeichert.\n";
}

// Primärschlüssel der Tabelle abrufen
function fetchPrimaryKey($mysqli, $tableName) {
    $result = $mysqli->query("
        SELECT COLUMN_NAME 
        FROM INFORMATION_SCHEMA.COLUMNS 
        WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = '$tableName' AND COLUMN_KEY = 'PRI'
    ");
    if (!$result) {
        throw new Exception("Fehler beim Abrufen des Primärschlüssels für `$tableName`: " . $mysqli->error);
    }
    $row = $result->fetch_assoc();
    return $row ? $row['COLUMN_NAME'] : null;
}

// Hauptlogik
$newMysqli = connectToServer($newServerConfig);
$oldMysqli = connectToServer($oldServerConfig);

$outputDir = __DIR__ . '/sql_output';

// Vorhandene SQL-Dateien löschen
clearOutputDirectory($outputDir);

if (!is_dir($outputDir)) mkdir($outputDir, 0777, true);

// Datei für alle Änderungen
$allSQLFile = "$outputDir/all.sql";
$allSQLHandler = fopen($allSQLFile, 'w');

$tablesResult = $newMysqli->query("
    SELECT TABLE_NAME 
    FROM INFORMATION_SCHEMA.TABLES 
    WHERE TABLE_SCHEMA = '{$newServerConfig['database']}' AND TABLE_TYPE = 'BASE TABLE'
");

if (!$tablesResult) {
    die("-- Fehler beim Abrufen der Tabellen: " . $newMysqli->error);
}

while ($tableRow = $tablesResult->fetch_assoc()) {
    $tableName = $tableRow['TABLE_NAME'];
    compareTableData($newMysqli, $oldMysqli, $tableName, $allSQLHandler);
}

$newMysqli->close();
$oldMysqli->close();
fclose($allSQLHandler);

echo "-- Alle Änderungen wurden in `all.sql` gesammelt.\n";

?>