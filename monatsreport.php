<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

ini_set("memory_limit", "-1");
set_time_limit(0);

//session_start();

define('FPDF_FONTPATH','cms/fpdf17/font/');
define('TEMP_PATH', 'cms/temp/');

$intAccPerPage = 25;
$arrEventAccountList = array();

require_once('assets/classes/class.mysql.php');
require_once('cms/inc/refactor.inc.php');

$arrMonth = array(
     1 => 'Januar',
     2 => 'Februar',
     3 => 'März',
     4 => 'April',
     5 => 'Mai',
     6 => 'Juni',
     7 => 'Juli',
     8 => 'August',
     9 => 'September',
    10 => 'Oktober',
    11 => 'November',
    12 => 'Dezember'
);

$arrStatus = array ( //.status_thick, .status_question, .status_warning, .status_wait
  'OK' => 'status_thick', 
  'gestundet' => 'status_question',
  'not OK' => 'status_warning', 
  'enquired' => 'status_wait', 
  'no Feedback' => 'status_question',
  'no result' => 'status_question'
);

$arrType = array (
  'Kontoauskunft' => 'Form',
  'Kontoauszug' => 'KtoA',
  'Unbedenklichkeitsbescheinigung' => 'UBB',
  'Sonstiges' => 'Sonst.'
);


$intDay = mktime(1, 0, 0, $_REQUEST['selectedMonth'], 1, $_REQUEST['selectedYear']);

$intCutTest = mktime(0, 0, 0,  7, 1, 2022);
$intCutLive = mktime(0, 0, 0, 10, 1, 2022);

if (((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) && ($intDay > $intCutTest)) || ((strstr(@$_SERVER['SERVER_NAME'], 'test') === false) && ($intDay > $intCutLive))) {

  require_once ('cms/fpdf182/fpdf.php');
  require_once ('cms/fpdf182/class.merge.php');

  $strSql = 'SELECT * FROM `Account` WHERE `Id` = "' .$_REQUEST['detailId'] .'" ';
  $arrResult = MySQLStatic::Query($strSql);

  $strSql = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$arrResult[0]['SF42_Company_Group__c'] .'"';
  $arrCompanyGroup = MySQLStatic::Query($strSql);

  // https://test.izs-institut.de/api/company/a083000000LQSO0AAP/account/00130000011RgzmAAC/eventlist?month=7&year=2022

  if (strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) {

    $strUrl = 'https://test.izs-institut.de/api/company/' .$arrCompanyGroup[0]['Id'] .'/account/' .$_REQUEST['detailId'] .'/eventlist?month=' .$_REQUEST['selectedMonth'] .'&year=' .$_REQUEST['selectedYear'] .'';

    $arrAuth = array(
      'http' => array(
          'header' => 'Authorization: Basic ' . base64_encode("izs:test")
      )
    );
  
    $resStream = stream_context_create($arrAuth);
    $resEventList = file_get_contents($strUrl, false, $resStream);
  
    $arrEventList = json_decode($resEventList, true);

  } else {

    $strUrl = 'https://www.izs-institut.de/api/company/' .$arrCompanyGroup[0]['Id'] .'/account/' .$_REQUEST['detailId'] .'/eventlist?month=' .$_REQUEST['selectedMonth'] .'&year=' .$_REQUEST['selectedYear'] .'&rel=1';
    $resEventList = file_get_contents($strUrl);

    $arrEventList = json_decode($resEventList, true);

    //print_r($arrEventList); die();

  }
  
  $arrConcatPdfList = array();
  if (isset($arrEventList['content']['eventlist']) && (count($arrEventList['content']['eventlist']) > 0)) {

    foreach ($arrEventList['content']['eventlist'] as $intKey => $arrEvent) {

      //print_r($arrEvent);

      if ($arrEvent['docurl'] != '') {

        $arrPartList = explode('/', $arrEvent['docurl']);

        //print_r($arrPartList); die();

        $arrPartList[5] = 'pp';
        $arrPartList[6] = 'ip';

        $strUrl = implode('/', $arrPartList);

        //https://test.izs.de/cms/event/ip/pp/2022/7/1042518.pdf
        //echo $arrEvent['docurl']; die();

        $strOutputFile = 'cms/files/temp/' .md5(uniqid(time())) .'.pdf';
        file_put_contents($strOutputFile, file_get_contents($strUrl));
        $arrConcatPdfList[] = $strOutputFile;

      }

    }

    //echo 'true';

  }

  //print_r($arrConcatPdfList); die();

  if (count($arrConcatPdfList) > 0) {

    $objMerge = new FPDF_Merge();

    foreach ($arrConcatPdfList as $intKey => $strRelativePath) {

      $objMerge->add($strRelativePath);
      unlink($strRelativePath);

    }

    $objMerge->output();

  }

  exit;

} elseif ((strstr(@$_SERVER['SERVER_NAME'], 'test') === false) && ($intDay > $intCutLive)) {


}


?>