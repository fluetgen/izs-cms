<?php

date_default_timezone_set('Europe/Berlin');

require_once('assets/classes/class.database.php');
require_once('assets/classes/class.mysql.php');
//require_once('assets/classes/class.modx.php');
require_once('assets/classes/class.replication.php');

$boolReplicate = false;

$intHour   = (int) date('H');
$intMinute = (int) date('i');

$strSql = 'SELECT UNIX_TIMESTAMP(`replication`) AS `replication`, `re_force`, `re_cl_id` FROM `_replicate`';
$arrResult = MySQLStatic::Query($strSql);
$intCountResult = count($arrResult);

if ($arrResult[0]['re_cl_id'] != 0) {
  $strSql2 = 'SELECT `cl_mail` FROM `cms_login` WHERE `cl_id` = "' .$arrResult[0]['re_cl_id'] .'"';
  $arrResult2 = MySQLStatic::Query($strSql2);
  $strSendErrorsTo = $arrResult2[0]['cl_mail'];
}

$boolForced = $arrResult[0]['re_force'];

if ($intCountResult > 0) {

  if ((($intHour == 4) && ($intMinute == 0)) || ($boolForced == 1)) {    
    $boolReplicate = true;
    
    //LAST REPLICATION WITHHIN 10 MINUTES
    if ($arrResult[0]['replication'] > (time() - $intWait)) {
      $boolReplicate = false;
      echo 'Replication allready running. Try again later in ' .($intWait - (time() - $arrResult[0]['replication'])) .' Seconds.';
    }
    
  }
  
}

if ($boolReplicate) {
  
  if ($boolForced) {
    mail($strSendErrorsTo, 'IZS: Replication', 'started.');
  }
  
  //WRITE STATUS
  $strSql = 'INSERT INTO `_log` (`lo_time`, `lo_action`) VALUES (NOW(), "start")';
  $arrResult = MySQLStatic::Insert($strSql);

  $strSql = 'UPDATE `_replicate` SET `replication` = NOW(), `re_force` = "1"';
  $arrResult = MySQLStatic::Update($strSql);

  //DELETE TEMP-TABLES
  foreach ($arrTables as $strTable) {
  
    $objReplication = new Replication;
    
    try {
      $strSql = 'DROP TABLE IF EXISTS `' .$strTable .$objReplication->strGetSuffix() .'`;';
      MySQLStatic::Update($strSql);
    } catch (Exception $e) {
      echo $e->getMessage();
      mail($strSendErrorsTo, 'IZS: Replication Error', $e->getMessage());
      die();
    }

      
  }
  
  //CREATE TABLE-STRUCTURE
  $arrStructure = array();
  foreach ($arrTables as $strTable) {
    $arrResult = SForceStatic::DescribeObject($strTable);
  
    $objReplication = new Replication;
    $strSuffix = $objReplication->strGetSuffix();
      
    $strOutput = $objReplication->strCreate ($strTable, $arrResult['fields'], $arrResult['info']);
    
    $arrStructure[$strTable] = $arrResult['fields'];
    $arrStructureInfo[$strTable] = $arrResult['info'];
    
    try {
      MySQLStatic::Update($strOutput);
    } catch (Exception $e) {
      echo $e->getMessage();
      mail($strSendErrorsTo, 'IZS: Replication Error', $e->getMessage());
      die();
    }

    
  }
  
  $arrCount = array();
  
  //INSERT DATA
  foreach ($arrTables as $strTable) {
    
    $arrCount[$strTable] = 0;
    
    $arrFieldListMysql  = array();
    $arrFieldListSForce = array();
    
    //CREATE FIELD-LISTs
    foreach ($arrStructure[$strTable] as $strFieldName => $strDescription) {
      $arrFieldListMysql[]  = '`' .$strFieldName .'`';
      $arrFieldListSForce[] = $strFieldName;
    }
  
    $strFieldListMysql  = implode(', ', $arrFieldListMysql);
    $strFieldListSForce = implode(', ', $arrFieldListSForce);
  
    $strQuery = 'SELECT ' .$strFieldListSForce .' FROM ' .$strTable .'';
  
    try {
    
      $mySforceConnection = new SforcePartnerClient();
      $mySoapClient = $mySforceConnection->createConnection(SOAP_CLIENT_BASEDIR .'/' .SOAP_WSDL);
      $mylogin = $mySforceConnection->login($GLOBALS['sforce']['config']['user'], $GLOBALS['sforce']['config']['pass']);
      
      $options = new QueryOptions(100);
      $mySforceConnection->setQueryOptions($options);
      
      $response = $mySforceConnection->query($strQuery);
      $queryResult = new QueryResult($response);
      !$done = false;
      
      $intCount = 1;
    
      if ($queryResult->size > 0) {
        while (!$done) {
          foreach ($queryResult->records as $record) {
            $sObject = new SObject($record);
            
            $strSql = 'INSERT INTO `' .$strTable .$strSuffix .'` (' .$strFieldListMysql .') VALUES ("' .$sObject->Id .'", ';
            
            $intCountFields = 1;
            foreach ($sObject->fields as $strFieldname => $strValue) {
              
              if ($arrStructureInfo[$strTable][$strFieldname]['type'] == 'datetime') {
                //2012-02-01T14:18:24.000Z -> 2013-02-01 01:02:03
                $arrTimestamp = explode('T', $strValue);
                $strValue = $arrTimestamp[0] .' ';
                $arrTimestamp = explode('.', $arrTimestamp[1]);
                $strValue.= $arrTimestamp[0];
              }
              
              $strSql.= '"' .str_replace('"', '\\"', $strValue) .'"';
              
              if ($intCountFields < (count($arrFieldListMysql) - 1)) {
                $strSql.= ', ';
              }
              
              $intCountFields++;
            }
            
            $strSql.= ');'.chr(10);
            
            MySQLStatic::Update($strSql);
            $arrCount[$strTable]++;
            
          }
          if ($queryResult->done != true) {
            
            $response = $mySforceConnection->queryMore($queryResult->queryLocator);
            $queryResult = new QueryResult($response);
            
          } else {
            $done = true;
          }
        }
      }
      
    } catch (Exception $e) {
      echo $e->getMessage();
      mail($strSendErrorsTo, 'IZS: Replication Error', $e->getMessage());
      die();
    }

  
  }
  
  //RENAME TABLES
  foreach ($arrTables as $strTable) {
  
    $objReplication = new Replication;
  
    try {
      $strSql = 'DROP TABLE IF EXISTS `' .$strTable .'`;';
      MySQLStatic::Update($strSql);
    } catch (Exception $e) {
      echo $e->getMessage();
      mail($strSendErrorsTo, 'IZS: Replication Error', $e->getMessage());
      die();
    }
  
    try {
      $strSql = 'RENAME TABLE `' .$strTable .$objReplication->strGetSuffix() .'` TO `' .$strTable .'`;';
      MySQLStatic::Update($strSql);
    } catch (Exception $e) {
      echo $e->getMessage();
      mail($strSendErrorsTo, 'IZS: Replication Error', $e->getMessage());
      die();
    }
      
  }
  
  //UPDATE MOD-X
  /*
  $objModX = new ModX;
  $boolGetRedirects    = $objModX->boolGetRedirects();
  $boolDeleteRedirects = $objModX->boolDeleteRedirects();
  $boolWriteRedirects  = $objModX->boolWriteRedirects();
  */

  //SEND MAIL
  $strSql = 'SELECT `re_cl_id` FROM `_replicate`';
  

  //WRITE STATUS
  $strSql = 'UPDATE `_replicate` SET `replication` = "' .$strNullTimestamp .'", `re_force` = "0", `re_cl_id` = "0"';
  $arrResult = MySQLStatic::Update($strSql);

  $strSql = 'INSERT INTO `_log` (`lo_time`, `lo_action`, `lo_message`, `lo_forced`) VALUES (NOW(), "stop", "' .addslashes(serialize($arrCount)) .'", "' .$boolForced .'")';
  $arrResult = MySQLStatic::Insert($strSql);
  
  if ($boolForced) {
    mail($strSendErrorsTo, 'IZS: Replication', 'done.');
  }

}

?>