<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

define('FPDF_FONTPATH','cms/fpdf17/font/');
define('TEMP_PATH', 'cms/temp/');

$intAccPerPage = 25;
$arrEventAccountList = array();

require_once('assets/classes/class.mysql.php');
require_once('cms/inc/refactor.inc.php');

$arrMonth = array(
     1 => 'Januar',
     2 => 'Februar',
     3 => 'März',
     4 => 'April',
     5 => 'Mai',
     6 => 'Juni',
     7 => 'Juli',
     8 => 'August',
     9 => 'September',
    10 => 'Oktober',
    11 => 'November',
    12 => 'Dezember'
);

$arrStatus = array ( //.status_thick, .status_question, .status_warning, .status_wait
  'OK' => 'status_thick', 
  'gestundet' => 'status_question',
  'not OK' => 'status_warning', 
  'enquired' => 'status_wait', 
  'no Feedback' => 'status_question',
  'no result' => 'status_question'
);

$arrType = array (
  'Kontoauskunft' => 'Form',
  'Kontoauszug' => 'KtoA',
  'Unbedenklichkeitsbescheinigung' => 'UBB',
  'Sonstiges' => 'Sonst.'
);


$intDay = mktime(1, 0, 0, $_REQUEST['selectedMonth'], 1, $_REQUEST['selectedYear']);

$intCutTest = mktime(0, 0, 0,  7, 1, 2022);
$intCutLive = mktime(0, 0, 0, 10, 1, 2022);

if (((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) && ($intDay > $intCutTest)) || ((strstr(@$_SERVER['SERVER_NAME'], 'test') === false) && ($intDay > $intCutLive))) {

  require_once ('cms/fpdf182/fpdf.php');
  require_once ('cms/fpdf182/class.merge.php');

  $strSql = 'SELECT * FROM `Account` WHERE `Id` = "' .$_REQUEST['detailId'] .'" ';
  $arrResult = MySQLStatic::Query($strSql);

  $strSql = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$arrResult[0]['SF42_Company_Group__c'] .'"';
  $arrCompanyGroup = MySQLStatic::Query($strSql);

  // https://test.izs-institut.de/api/company/a083000000LQSO0AAP/account/00130000011RgzmAAC/eventlist?month=7&year=2022

  if (strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) {

    $strUrl = 'https://test.izs-institut.de/api/company/' .$arrCompanyGroup[0]['Id'] .'/account/' .$_REQUEST['detailId'] .'/eventlist?month=' .$_REQUEST['selectedMonth'] .'&year=' .$_REQUEST['selectedYear'] .'';

    $arrAuth = array(
      'http' => array(
          'header' => 'Authorization: Basic ' . base64_encode("izs:test")
      )
    );
  
    $resStream = stream_context_create($arrAuth);
    $resEventList = file_get_contents($strUrl, false, $resStream);
  
    $arrEventList = json_decode($resEventList, true);

  } else {

    $strUrl = 'https://www.izs-institut.de/api/company/' .$arrCompanyGroup[0]['Id'] .'/account/' .$_REQUEST['detailId'] .'/eventlist?month=' .$_REQUEST['selectedMonth'] .'&year=' .$_REQUEST['selectedYear'] .'&rel=1';
    $resEventList = file_get_contents($strUrl);

    $arrEventList = json_decode($resEventList, true);

    //print_r($arrEventList); die();

  }
  
  $arrConcatPdfList = array();
  if (isset($arrEventList['content']['eventlist']) && (count($arrEventList['content']['eventlist']) > 0)) {

    foreach ($arrEventList['content']['eventlist'] as $intKey => $arrEvent) {

      //print_r($arrEvent);

      if ($arrEvent['docurl'] != '') {

        $arrPartList = explode('/', $arrEvent['docurl']);

        //print_r($arrPartList); die();

        $arrPartList[5] = 'pp';
        $arrPartList[6] = 'ip';

        $strUrl = implode('/', $arrPartList);

        //https://test.izs.de/cms/event/ip/pp/2022/7/1042518.pdf
        //echo $arrEvent['docurl']; die();

        $strOutputFile = 'cms/files/temp/' .md5(uniqid(time())) .'.pdf';
        file_put_contents($strOutputFile, file_get_contents($strUrl));
        $arrConcatPdfList[] = $strOutputFile;

      }

    }

    //echo 'true';

  }

  //print_r($arrConcatPdfList); die();

  if (count($arrConcatPdfList) > 0) {

    $objMerge = new FPDF_Merge();

    foreach ($arrConcatPdfList as $intKey => $strRelativePath) {

      $objMerge->add($strRelativePath);
      unlink($strRelativePath);

    }

    $objMerge->output();

  }

  exit;

} elseif ((strstr(@$_SERVER['SERVER_NAME'], 'test') === false) && ($intDay > $intCutLive)) {


}

require('cms/fpdf17/fpdf.php');
require('cms/fpdf17/fpdi.php');

#http://izs.force.com/sf42_izs_websitereport?detailId=00130000011RgzmAAC&selectedMonth=1&selectedYear=2014 $strPpName

$boolIsIzs = false;
$intUserId = 0;
$boolShow  = true;

$strSql = 'SELECT * FROM `Account` WHERE `Id` = "' .$_REQUEST['detailId'] .'" ';
$arrResult = MySQLStatic::Query($strSql);

$strSql = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$arrResult[0]['SF42_Company_Group__c'] .'"';
$arrCompanyGroup = MySQLStatic::Query($strSql);


/* ------------------------------------------------------------ */

if ($arrCompanyGroup[0]['Kooperation_beendet_am__c'] != '0000-00-00') {
  if (strtotime($arrCompanyGroup[0]['Kooperation_beendet_am__c']) < strtotime('- 6 weeks')) {
      $boolShow = false;
  }
}

if (isset($_COOKIE) && (count($_COOKIE) > 0)) {

    $strToken = 'triple2013';

    if (isset($_COOKIE['id']) && isset($_COOKIE['token']) && ($_COOKIE['token'] == MD5($strToken .'_' .$_COOKIE['id']))) {

        $boolIsIzs = true;
        $intUserId = $_COOKIE['id'];

    }

} else if (isset($_REQUEST['izs'])) {

    $boolIsIzs = true;
    $intUserId = $_REQUEST['izs'];

}

if (!$boolIsIzs) {

  if (($boolShow) && (count($arrCompanyGroup) > 0)) {

      //File exits and is allowed to be delivered
      $strSql = 'SELECT `id_id` FROM `izs_download` WHERE `id_type` = "svreport" AND `id_group` = "' .MySQLStatic::esc($arrCompanyGroup[0]['Id']) .'" AND `id_date` = "' .MySQLStatic::esc(date('Y-m-d')) .'"';
      $arrDow = MySQLStatic::Query($strSql);

      if (count($arrDow) > 0) {
          $strSql = 'UPDATE `izs_download` SET `id_count` = `id_count` + 1 WHERE `id_id` = "' .MySQLStatic::esc($arrDow[0]['id_id']) .'"';
          $intSql = MySQLStatic::Update($strSql);
      } else {

          if (isset($_REQUEST['ip'])) {
            $strIp = $_REQUEST['ip'];
          } else {
            $strIp = $_SERVER['REMOTE_ADDR'];
          }

          $strSql = 'INSERT INTO `izs_download` (`id_id`, `id_type`, `id_account`, `id_group`, `id_ip`, `id_moth`, `id_year`, `id_ev`, `id_date`, `id_time`, `id_last_ip`, `id_count`) ';
          $strSql.= 'VALUES (NULL, "svreport", "' .MySQLStatic::esc($_REQUEST['detailId']) .'", "' .MySQLStatic::esc($arrCompanyGroup[0]['Id']) .'", ';
          $strSql.= '"", "' .MySQLStatic::esc($_REQUEST['selectedMonth']) .'", "' .MySQLStatic::esc($_REQUEST['selectedYear']) .'", ';
          $strSql.= '"", NOW(), NOW(), "' .$strIp .'", "1")';
          $intSql = MySQLStatic::Insert($strSql);
      }

  } elseif (!$boolShow) {

    header('Location: https://www.izs-institut.de/portal.html#!/company/' .$arrCompanyGroup[0]['Id']);
    exit;

  } else {

    header("HTTP/1.1 404 Not Found");
    exit;

  }

}

/* ------------------------------------------------------------ */


$strPpName = $arrResult[0]['Name'];
$intPpBtnr = $arrResult[0]['SF42_Comany_ID__c'];

$intMonthSelected = $_REQUEST['selectedMonth'];
$intYearSelected = $_REQUEST['selectedYear'];

$strSql5 = '
SELECT SF42_IZSEvent__c.Id, SF42_IZSEvent__c.Name, 
SF42_IZSEvent__c.SF42_informationProvider__c, SF42_informationProvider__r.Name, 
SF42_EventStatus__c, SF42_EventComment__c, SF42_StatusFlag__c, 
SF42_PublishingStatus__c, SF42_DocumentUrl__c, Art_des_Dokuments__c, 
SF42_IZSEvent__c.RecordTypeId AS RecordType, Sperrvermerk__c, 
Status_Klaerung__c, Grund__c 

FROM SF42_IZSEvent__c 

INNER JOIN Account AS SF42_informationProvider__r ON SF42_IZSEvent__c.SF42_informationProvider__c = SF42_informationProvider__r.Id 

WHERE SF42_Month__c = \'' .$intMonthSelected .'\' AND SF42_Year__c = \'' .$intYearSelected .'\' 
AND SF42_OnlineStatus__c = "true" AND Betriebsnummer_ZA__c = \'' .$intPpBtnr .'\' AND SF42_EventStatus__c != "abgelehnt / Dublette" 
AND `SF42_IZSEvent__c`.`RecordTypeId` != \'01230000001Ao73AAC\' 
ORDER BY SF42_informationProvider__r.Name'; //ACHTUNG Name wird durch Name überschrieben!!!


if (@$_SESSION['id'] == 3) {
  //echo $strSql5; die();
}

$arrResult5 = MySQLStatic::Query($strSql5);
$arrEventAccountList = $arrResult5;

$boolSvRaten = false;
$boolSvStundung = false;

foreach ($arrEventAccountList as $intKey => $arrEvent) {
  if (($arrEvent['Status_Klaerung__c'] == 'geschlossen / positiv') && ($arrEvent['Grund__c'] == 'Ratenvereinbarung')) { //„Status Klärung = geschlossen/positiv“ + Grund = „Ratenvereinbarung
    $boolSvRaten = true;
  } 
  if ($arrEvent['SF42_EventStatus__c'] == 'gestundet') {
    $boolSvStundung = true;
  }
}


if (@$_SESSION['id'] == 3) {

  //echo 'boolSvRaten'; var_dump($boolSvRaten);
  //echo 'boolSvStundung'; var_dump($boolSvStundung);

  //echo $strSql5; print_r($arrEventAccountList);
}

//str_pad($value, 8, '0', STR_PAD_LEFT);

/*
$strBgEvent = '
SELECT `be_name` FROM `izs_bg_event` 
WHERE ((`be_befristet` > \'' .$intYearSelected .'-' .str_pad($intMonthSelected, 2, '0', STR_PAD_LEFT) .'-01\') OR (`be_befristet` = "0000-00-00")) 
AND `be_meldestelle` = "' .$_REQUEST['detailId'] .'" AND `be_jahr` <= "' .$intYearSelected .'" AND `be_status_bearbeitung` = "veröffentlicht" 
ORDER BY `be_jahr` DESC, `be_befristet` DESC LIMIT 1
';
$arrBgEvent = MySQLStatic::Query($strBgEvent);

if ($_SESSION['id'] == 3) {
  echo $strBgEvent; print_r($arrBgEvent);
}
*/
$boolBgRaten = false;
/*
if (isset($arrBgEvent[0]['be_name'])) {

  $strBgEvent = 'SELECT * FROM `izs_bgevent_change` WHERE ((`ec_new` = "OK (mit RV)") OR (`ec_old` = "OK (mit RV)")) ';
  $strBgEvent.= 'AND `ev_name` = "' .$arrBgEvent[0]['be_name'] .'" ORDER BY `ec_time` ASC';
  $arrBgEvent = MySQLStatic::Query($strBgEvent);

  if (count($arrBgEvent) > 0) {

    foreach ($arrBgEvent as $intKey => $arrChange) {
      if ($arrChange['ec_new'] == 'OK (mit RV)') {
        $boolBgRaten = true;
      } else {
        $boolBgRaten = false;
      }
    }

  }

  if ($_SESSION['id'] == 3) {
    echo $strBgEvent; var_dump($boolBgRaten); print_r($arrBgEvent); die();
  }

}
*/
if (@$_SESSION['id'] == 3) {
  //echo 'boolBgRaten'; var_dump($boolBgRaten);
  //die();
}

class concat_pdf extends fpdi {
  var $files = array();
  function concat_pdf($orientation='P',$unit='mm',$format='A4') {
    parent::__construct($orientation,$unit,$format);
  }
  function setFiles($files) {
    $this->files = $files;
  }
  function concat() {
    foreach($this->files AS $file) {
      $pagecount = $this->setSourceFile($file);
      for ($i = 1;  $i <= $pagecount;  $i++) {
       $tplidx = $this->ImportPage($i);
       $this->AddPage();
       $this->useTemplate($tplidx);
      }
    }
  }
  // Page footer
  function Footer() {
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('OfficinaSans','',8);
    // Page number
    $this->Cell(0,10,' - Seite '.$this->PageNo().'/{nb}' .' -',0,0,'C');
  }
}

$pdf = new concat_pdf();
$pdf->AliasNbPages();

$pdf->AddFont('OfficinaSans','','itc-officina-sans-lt-book.php');
$pdf->AddFont('OfficinaSans','B','itc-officina-sans-lt-bold.php');
$pdf->AddFont('OfficinaSans','I','itc-officina-sans-lt-book-italic.php');
$pdf->AddFont('OfficinaSans','BI','itc-officina-sans-lt-bold-italic.php');


$pdf->AddPage();
$pdf->SetAutoPageBreak(false);
$pdf->Image('cms/pdf/Monatsreport_Deckblatt.png', 0, 0, 210, 297, 'PNG'); 

$pdf->SetFont('OfficinaSans', '', 19);
$pdf->SetTextColor(160, 133, 20);
$pdf->Text(94, 122, utf8_decode('PRÜFBERICHT'));

$pdf->SetMargins(94, 144, 20);

$pdf->SetY(144);

$pdf->SetFont('OfficinaSans', '', 10);
$pdf->SetTextColor(0, 0, 0);
$pdf->Write(3, utf8_decode($strPpName) .chr(10));
$pdf->SetFont('OfficinaSans', '', 7);
$pdf->SetTextColor(134, 134, 134);
$pdf->Write(5, utf8_decode('Geprüftes Unternehmen') .chr(10));

$pdf->SetFont('OfficinaSans', '', 10);
$pdf->SetTextColor(0, 0, 0);
$pdf->Write(3, chr(10) .utf8_decode($arrResult[0]['SF42_Comany_ID__c']) .chr(10));
$pdf->SetFont('OfficinaSans', '', 7);
$pdf->SetTextColor(134, 134, 134);
$pdf->Write(5, utf8_decode('Betriebsnummer') .chr(10));

$pdf->SetFont('OfficinaSans', '', 10);
$pdf->SetTextColor(0, 0, 0);
$pdf->Write(3, chr(10) .utf8_decode($_REQUEST['selectedMonth'] .' / ' .$_REQUEST['selectedYear']) .chr(10));
$pdf->SetFont('OfficinaSans', '', 7);
$pdf->SetTextColor(134, 134, 134);
$pdf->Write(5, utf8_decode('Prüfperiode') .chr(10));

$pdf->SetFont('OfficinaSans', '', 10);
$pdf->SetTextColor(0, 0, 0);
$pdf->Write(3, chr(10) .utf8_decode(date('d.m.Y')) .chr(10));
$pdf->SetFont('OfficinaSans', '', 7);
$pdf->SetTextColor(134, 134, 134);
$pdf->Write(5, utf8_decode('Datum der Erstellung') .chr(10));

$pdf->SetFont('OfficinaSans', '', 10);
$pdf->SetTextColor(0, 0, 0);
$pdf->Write(5, chr(10) .chr(10) .utf8_decode('Grundlage der Prüfung waren alle ordnungsgemäß und fristgerecht zur Prüfperiode übermittelten Beitragsnachweis-Daten des Unternehmens.') .chr(10));

///*
$arrAmpel = array(
  'rot' => 'rot',
  'grün' => 'gruen',
  'gruen' => 'gruen',
  'gelb' => 'gelb',
  'kein Status' => 'grau',
  'unbekannt' => 'grau'
);

$arrAmpelDb = array(
  'red' => 'rot',
  'green' => 'gruen',
  'yellow' => 'gelb',
  'gray' => 'kein Status'
);

$pdf->Write(5, chr(10) .chr(10));


//Gesamtstatus
$pdf->SetFont('OfficinaSans', '', 10);
$pdf->SetTextColor(0, 0, 0);

if (($_REQUEST['selectedMonth'] == (date('n') - 1)) && ($_REQUEST['selectedYear'] == date('Y'))) {

  $strGesamtstatus = $arrCompanyGroup[0]['Gesamtstatus__c'];
  $strHinweis = $arrCompanyGroup[0]['Hinweis_sichtbar_auf_Portal__c'];

} else {

  if (isset($_REQUEST['detailId'])) {
    $strSql = 'SELECT * FROM `izs_infoservice_status` WHERE `is_group_id` = "' .$arrResult[0]['SF42_Company_Group__c'] .'" AND `is_year` = "' .$_REQUEST['selectedYear'] .'" AND `is_month` = "' .$_REQUEST['selectedMonth']  .'" AND `is_account_id` = "' .$_REQUEST['detailId'] .'"';
    $arrSql = MySQLStatic::Query($strSql);
  }

  if ((count($arrSql) == 0) || (!isset($_REQUEST['detailId']))) {
    $strSql = 'SELECT * FROM `izs_infoservice_status` WHERE `is_group_id` = "' .$arrResult[0]['SF42_Company_Group__c'] .'" AND `is_year` = "' .$_REQUEST['selectedYear'] .'" AND `is_month` = "' .$_REQUEST['selectedMonth']  .'"';
    $arrSql = MySQLStatic::Query($strSql);
  }

  //echo $strSql; die();

  if (is_array($arrSql) && (count($arrSql) > 0)) {

    if (isset($arrAmpelDb[$arrSql[0]['is_status']])) {
      $strGesamtstatus = $arrAmpelDb[$arrSql[0]['is_status']];
    } else {
      $strGesamtstatus = $arrSql[0]['is_status'];
    }

    $strHinweis = $arrSql[0]['is_desc'];

  } else {
    $strGesamtstatus = 'unbekannt';
    $strHinweis = 'Bitte wenden Sie sich an uns, um Auskunft über den exakten Status zu erhalten.';
  }

  if ($strGesamtstatus == '') {
    $strGesamtstatus = 'unbekannt';
  }

}

//echo $arrCompanyGroup[0]['Gesamtstatus__c'];

if (isset($arrAmpel[$strGesamtstatus])) {
  $intY = 217;
  $pdf->Image('cms/img/status_' .$arrAmpel[$strGesamtstatus] .'.png', 95, $intY, 0, 0, 'PNG');
} else {
  $pdf->Text(55, 69, utf8_decode($arrCompanyGroup[0]['Gesamtstatus__c']));
}


$pdf->SetFont('OfficinaSans', '', 7);
$pdf->SetTextColor(134, 134, 134);
$pdf->Write(5, utf8_decode('Gesamtstatus seit Registrierung') .chr(10));


//Hinweis zum Gesamtstatus


$pdf->SetFont('OfficinaSans', '', 10);
$pdf->SetTextColor(0, 0, 0);
$pdf->Write(3, chr(10) .utf8_decode($strHinweis) .chr(10));
$pdf->SetFont('OfficinaSans', '', 7);
$pdf->SetTextColor(134, 134, 134);
$pdf->Write(5, utf8_decode('Hinweis zum Gesamtstatus') .chr(10));
//*/

$pdf->SetMargins(20, 20, 20);


  $strBraunR = 160;
  $strBraunG = 132;
  $strBraunB =   0;

  $pdf->SetTextColor(0, 0, 0);
  
  $intPages = ceil(count($arrEventAccountList) / $intAccPerPage);
  
  for ($intPage = 1; $intPage <= $intPages; $intPage++) {
  

    $pdf->AddPage();
    $pdf->SetAutoPageBreak(false);
    $pdf->Image('cms/pdf/Monatsreport_Folgeblatt.png', 0, 0, 210, 297, 'PNG'); 
    $pdf->SetMargins(20, 20);

    //$pdf->SetY(144);
    
    $pdf->SetFont('OfficinaSans', '', 10);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->Write(3, utf8_decode($strPpName) .chr(10));
    $pdf->SetFont('OfficinaSans', '', 7);
    $pdf->SetTextColor(134, 134, 134);
    $pdf->Write(5, utf8_decode('Geprüftes Unternehmen') .chr(10));

    $intHeight = 35;
    $pdf->SetFont('OfficinaSans', '', 10);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->Text( 21, $intHeight, utf8_decode($arrResult[0]['SF42_Comany_ID__c']));
    $pdf->Text( 50, $intHeight, utf8_decode($_REQUEST['selectedMonth'] .' / ' .$_REQUEST['selectedYear']));
    $pdf->Text( 79, $intHeight, utf8_decode(date('d.m.Y')));
    
    $intHeight = 38;
    $pdf->SetFont('OfficinaSans', '', 7);
    $pdf->SetTextColor(134, 134, 134);
    $pdf->Text( 21, $intHeight, utf8_decode('Betriebsnummer'));
    $pdf->Text( 50, $intHeight, utf8_decode('Prüfperiode'));
    $pdf->Text( 79, $intHeight, utf8_decode('Datum der Erstellung'));

    
    $pdf->SetY(20);

    $pdf->SetDrawColor($strBraunR, $strBraunG, $strBraunB);
    $pdf->SetLineWidth(0.1);
    $intHeight = 45; //140;

    $pdf->Line( 20, $intHeight, 100, $intHeight);
    $pdf->Line(105, $intHeight, 170, $intHeight);
    $pdf->Line(175, $intHeight, 190, $intHeight);
  
    $intHeight = 49.5; //144.5;
    $pdf->SetTextColor($strBraunR, $strBraunG, $strBraunB);
    $pdf->SetFont('OfficinaSans', '', 10);  
    
    $pdf->Text( 20.5, $intHeight, utf8_decode('Krankenkasse'));
    $pdf->Text(105.5, $intHeight, utf8_decode('Bemerkung'));
    $pdf->Text(175.5, $intHeight, utf8_decode('Download'));
    
    $pdf->SetLineWidth(0.5);
    $intHeight = 51.5; //146.5;
    $pdf->Line( 20, $intHeight, 100, $intHeight);
    $pdf->Line(105, $intHeight, 170, $intHeight);
    $pdf->Line(175, $intHeight, 190, $intHeight);

    
    $intStart = ($intPage - 1) * $intAccPerPage;
        
    for ($intCount = ($intStart + 1); $intCount <= count($arrEventAccountList); $intCount++) {
      
      $arrAccount = $arrEventAccountList[$intCount - 1];
      
      $intFactor = $intCount;
      if ($intFactor > $intAccPerPage) {
        $intFactor = $intFactor % $intAccPerPage;
        if ($intFactor == 0) {
          $intFactor = $intAccPerPage;
        }
      }
      
      $strImg = $arrStatus[$arrAccount['SF42_EventStatus__c']];
      if ($strImg == '') {
        $strImg = 'x';
      }

      $intHeight = 44 + ($intFactor * 9); // 149
      $pdf->Image('assets/images/sys/' .$strImg .'.png', 22, $intHeight, 0, 0, 'PNG'); 
      
      $pdf->SetMargins(26, 20);
      $intHeight = 46.5 + ($intFactor * 9);  // 151.5 
      
      $pdf->SetY($intHeight);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->SetFont('OfficinaSans', '', 10);  
      if ($arrAccount['Sperrvermerk__c'] == 'false') {
        $strLink = $arrAccount['SF42_DocumentUrl__c'];
        $strLink = str_replace('www.izs-institut.de', 'www.izs.de', $strLink);
        $pdf->Write(0, utf8_decode($arrAccount['Name']), $strLink);
      } else {
        $pdf->Write(0, utf8_decode($arrAccount['Name']));
      }
      
      $pdf->SetLineWidth(0.1);
      //$pdf->SetDash(0.1, 0.7);
      $intHeight = 51 + ($intFactor * 9); // 156
      $pdf->Line( 20, $intHeight, 100, $intHeight);
      $pdf->Line(105, $intHeight, 170, $intHeight);
      $pdf->Line(175, $intHeight, 190, $intHeight);
        
      $pdf->SetMargins(105.5, 20, 36);
      $pdf->SetXY(106, $intHeight - 7.5);
      $pdf->SetFont('OfficinaSans', '', 5.8);  
      $pdf->MultiCell(63, 3, utf8_decode($arrAccount['SF42_EventComment__c']));

      //$pdf->SetX(110);
      //$pdf->Write(0, utf8_decode($arrAccount['SF42_EventComment__c']));
        
      $pdf->SetMargins(20, 20, 20);
      
      if (($arrAccount['SF42_DocumentUrl__c'] != '') && ($arrAccount['Sperrvermerk__c'] == 'false')) {
        $intHeight = 44 + ($intFactor * 9); // 149
        $pdf->Image('assets/images/sys/icon_pdf.png', 176, $intHeight, 0, 0, 'PNG'); 
        
        $pdf->SetXY(180.6, $intHeight + 2.5);
        $pdf->SetFont('OfficinaSans', '', 8);  
        $pdf->SetTextColor(115, 114, 108);
        $strLink = $arrAccount['SF42_DocumentUrl__c'];
        $strLink = str_replace('www.izs-institut.de', 'www.izs.de', $strLink);
        $pdf->Write(0, utf8_decode($arrType[$arrAccount['Art_des_Dokuments__c']]), $strLink);
      } elseif ($arrAccount['Sperrvermerk__c'] == 'true') {
        $pdf->SetTextColor(115, 114, 108);
        $pdf->SetFont('OfficinaSans', '', 8);  
        $pdf->Text(175.5, $intHeight - 4, utf8_decode('auf Anfrage'));
      }
      
      if ((($intCount % $intAccPerPage) == 0) && ($intCount < count($arrEventAccountList))) {
        $intCount--;
        break;
      }
      
    }
    
  }

$pdf->Output(date('Y.m.d_His - ') .'Monatsreport ' .$_REQUEST['selectedMonth'] .'_' .$_REQUEST['selectedYear'] .' - ' .$strPpName .'.pdf', 'D');

?>