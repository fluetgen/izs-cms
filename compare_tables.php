<?php

// Konfigurationsdaten für die beiden Server
$oldServerConfig = [
    'host' => 'db-test.cj2k22uuw73e.eu-central-1.rds.amazonaws.com',
    'user' => 'U1246131-2',
    'password' => '442b4cID',
    'database' => 'salesforce_dev_01',
];

$newServerConfig = [
    'host' => 'www.izs.de',
    'user' => 'U1246131-2',
    'password' => '442b4cID',
    'database' => 'salesforce_dev_01',
];

// Verbindung mit MySQL-Servern herstellen
function connectToServer($config) {
    $mysqli = new mysqli($config['host'], $config['user'], $config['password'], $config['database']);
    if ($mysqli->connect_error) {
        die("Verbindung fehlgeschlagen: " . $mysqli->connect_error);
    }
    return $mysqli;
}

// Tabellen und deren Definitionen abrufen (Views ignorieren)
function getTablesAndDefinitions($mysqli, $database) {
    $tables = [];
    $result = $mysqli->query("
        SELECT TABLE_NAME 
        FROM INFORMATION_SCHEMA.TABLES 
        WHERE TABLE_SCHEMA = '$database' AND TABLE_TYPE = 'BASE TABLE'
    ");
    while ($row = $result->fetch_assoc()) {
        $tableName = $row['TABLE_NAME'];
        $ddlResult = $mysqli->query("SHOW CREATE TABLE `$tableName`");
        $ddlRow = $ddlResult->fetch_assoc();
        $tables[$tableName] = $ddlRow['Create Table'];
    }
    return $tables;
}

// Unterschiede erkennen und ausgeben (bestimmte Werte ignorieren)
function compareSchemas($newTables, $oldTables) {
    $differences = [];

    foreach ($newTables as $tableName => $newDDL) {
        if (!isset($oldTables[$tableName])) {
            // Tabelle fehlt auf dem alten Server
            $differences[$tableName] = [
                'status' => 'fehlt auf altem Server',
                'newDDL' => $newDDL,
                'oldDDL' => null,
            ];
        } else {
            // Beide Tabellen existieren -> Normalisieren und vergleichen
            $normalizedNewDDL = normalizeDDL($newDDL);
            $normalizedOldDDL = normalizeDDL($oldTables[$tableName]);

            if ($normalizedNewDDL !== $normalizedOldDDL) {
                $differences[$tableName] = [
                    'status' => 'unterschiedlich',
                    'newDDL' => $newDDL,
                    'oldDDL' => $oldTables[$tableName],
                ];
            }
        }
    }

    foreach ($oldTables as $tableName => $oldDDL) {
        if (!isset($newTables[$tableName])) {
            // Tabelle fehlt auf dem neuen Server
            $differences[$tableName] = [
                'status' => 'fehlt auf neuem Server',
                'newDDL' => null,
                'oldDDL' => $oldDDL,
            ];
        }
    }

    return $differences;
}

// Normalisiert ein DDL, um bestimmte Werte zu ignorieren
function normalizeDDL($ddl) {
    // Entfernt AUTO_INCREMENT
    $ddl = preg_replace('/AUTO_INCREMENT=\d+/i', '', $ddl);

    // Ersetzt CHARSET und COLLATE durch Standardwerte
    $ddl = preg_replace('/DEFAULT CHARSET=\w+/i', '', $ddl);
    $ddl = preg_replace('/COLLATE=\w+/i', '', $ddl);

    // Entfernt zusätzliche Leerzeichen und normalisiert
    $ddl = preg_replace('/\s+/', ' ', trim($ddl));

    return $ddl;
}

// Synchronisation starten
$newMysqli = connectToServer($newServerConfig);
$oldMysqli = connectToServer($oldServerConfig);

$newTables = getTablesAndDefinitions($newMysqli, $newServerConfig['database']);
$oldTables = getTablesAndDefinitions($oldMysqli, $oldServerConfig['database']);

$differences = compareSchemas($newTables, $oldTables);

if (!empty($differences)) {
    echo "Unterschiede im Schema:\n\n";
    foreach ($differences as $tableName => $details) {
        echo "Tabelle: $tableName\n";
        echo "Status: " . $details['status'] . "\n";

        if ($details['newDDL'] !== null) {
            echo "Neues Schema:\n" . $details['newDDL'] . "\n";
        }

        if ($details['oldDDL'] !== null) {
            echo "Altes Schema:\n" . $details['oldDDL'] . "\n";
        }

        echo str_repeat("-", 80) . "\n";
    }
} else {
    echo "Die Tabellen-Schemas sind identisch.\n";
}

// Verbindungen schließen
$newMysqli->close();
$oldMysqli->close();

?>