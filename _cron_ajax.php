<?php

date_default_timezone_set('Europe/Berlin');

ini_set("memory_limit", "-1");
set_time_limit(0);

if (isset($_SERVER['SERVER_NAME']) && (strstr($_SERVER['SERVER_NAME'], 'test.') !== false)) {
  $strFileRoot = 'https://test.izs.de/cms/';
} else {
  $strFileRoot = 'https://www.izs.de/cms/';
}

$intLimitPerMinute = 20;
//$intLimitPerMinute = 6;

require_once('assets/classes/class.mysql.php');

$strSql = 'SELECT * FROM `_cron_ajax` WHERE `ca_status` = 1 ORDER BY `ca_id` ASC LIMIT ' .$intLimitPerMinute;
$arrRow = MySQLStatic::Query($strSql);

if (count($arrRow) > 0) {

    foreach ($arrRow as $arrUrl) {
        
        $strSql = 'UPDATE `_cron_ajax` SET `ca_status` = 2, `ca_time` = NOW() WHERE `ca_id` = "' .$arrUrl['ca_id'] .'"';
        $arrRow = MySQLStatic::Update($strSql);

        if ($arrUrl['ca_method'] == 'POST') {

            $strUrl = $strFileRoot .$arrUrl['ca_url'];
            $arrData = unserialize($arrUrl['ca_data']);
            
            // use key 'http' even if you send the request to https://...
            $arrOptions = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($arrData)
                )
            );
            $resContext = stream_context_create($arrOptions);
            $resPost    = file_get_contents($strUrl, false, $resContext);
            if ($result === FALSE) { /* Handle error */ }
            
            var_dump($result);
    
        }
        
        sleep(1);
        
    }

}

?>