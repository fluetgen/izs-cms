<?php

/* 
// Änderung der SV-Daten
*/

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

session_start();

require_once('../assets/classes/class.mysql.php');
require_once('../assets/classes/class.database.php');

$strFrom = 'klaerung@izs-institut.de';

function htmlwrap(&$str, $maxLength, $char='<br />'){
    $count = 0;
    $newStr = '';
    $openTag = false;
    $lenstr = strlen($str);
    for($i=0; $i<$lenstr; $i++){
        $newStr .= $str[$i];
        if($str[$i] == '<'){
            $openTag = true;
            continue;
        }
        if(($openTag) && ($str[$i] == '>')){
            $openTag = false;
            continue;
        }
        if(!$openTag){
            if($str[$i] == ' '){
                if ($count == 0) {
                    $newStr = substr($newStr,0, -1);
                    continue;
                } else {
                    $lastspace = $count + 1;
                }
            }
            $count++;
            if($count==$maxLength){
                if ($str[$i+1] != ' ' && $lastspace && ($lastspace < $count)) {
                    $tmp = ($count - $lastspace)* -1;
                    $newStr = substr($newStr,0, $tmp) . $char . substr($newStr,$tmp);
                    $count = $tmp * -1;
                } else {
                    $newStr .= $char;
                    $count = 0;
                }
                $lastspace = 0;
            }
        }  
    }

    return $newStr;
}

function multi_attach_mail ($to, $cc, $bcc, $files, $subject, $message, $message_html, $strFrom = 'klaerung@izs-institut.de') {

  $to = str_replace(';', ',', $to);
  $cc = str_replace(';', ',', $cc);
  $bcc = str_replace(';', ',', $bcc);
  
  //$to = 'f.luetgen@gmx.de';
    
  $message_m = '';
  
  $from = '=?UTF-8?B?' .base64_encode('IZS Institut für Zahlungssicherheit') .'?= <' .$strFrom .'>';
  $subject = '=?UTF-8?B?' .base64_encode($subject) .'?='; 
  
  $headers  = "";
  $headers .= "From: $from";
  
  if ($cc != '') {
    $headers .= "\n" .'Cc: ' .$cc;
  }
  
  if ($bcc != '') {
    $headers .= "\n" .'Bcc: ' .$bcc; //f.luetgen@gmx.de, 
  }
  
  $strStyleBlack = 'font-size:9pt;color:#2c2d2f;font-family:\'Roboto\',\'Arial\',\'Helvetica\'';
  $strStyleOrange = 'font-size:9pt;color:#f15324;font-family:\'Roboto\',\'Arial\',\'Helvetica\'';
  
  //$signatur = '<p style="font-family:\'Roboto\'">Ihr IZS Service Team</p>' .chr(10);
  $signatur = '';
  $signatur.= '<p><img src="cid:part2.AF836471.09F8779E@izs-institut.de" name="IZS-Logo.png" alt="IZS Logo" style="width: 100px; height: 36px;"></p>' .chr(10);
  //$signatur.= '<p style="font-size: 12pt;color:\'red\';font-family:\'Helvetica\',\'Arial\',\'Calibri\',\'sans-serif\'">+++ Achtung neue Adresse ab 01.02.2022 +++</p>' .chr(10);
  $signatur.= '<p style="' .$strStyleBlack .'"><b>IZS - Institut f&uuml;r Zahlungssicherheit GmbH</b><br>' .chr(10);
  $signatur.= 'W&uuml;rmtalstra&szlig;e 20a &bull; 81375 M&uuml;nchen<br />' .chr(10);
  $signatur.= 'Telefon: +49 (0) 89 122 237 770 &bull; E-Mail: ' .chr(10);
  $signatur.= '<a href="mailto:' .$strFrom .'" style="' .$strStyleOrange .'">' .$strFrom .'</a></p>' .chr(10);

  // boundary
  $strBound = md5(time());
  $strBoundery01 = $strBound .'01';
  $strBoundery02 = $strBound .'02';
  $strBoundery03 = $strBound .'03';

  // headers for attachment
  $headers.= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"" .$strBoundery01 ."\"" ."\n\n";

  $message_m.= "\n" .'This is a multi-part message in MIME format.' ."\n";
  $message_m.= '--' .$strBoundery01 ."\n";
  $message_m.= 'Content-Type: multipart/alternative;' ."\n";
  $message_m.= ' boundary="' .$strBoundery02 .'"' ."\n\n";

  $message_m.= '--' .$strBoundery02 ."\n";
  $message_m.= 'Content-Type: text/plain; charset=UTF-8' ."\n";
  $message_m.= 'Content-Transfer-Encoding: 8bit' ."\n\n";

  $message_m.= wordwrap(strip_tags(str_replace('<br>', "\n", $message)), 70) .wordwrap(strip_tags($signatur), 70) ."\n\n";

  $message_m.= '--' .$strBoundery02 ."\n";
  $message_m.= 'Content-Type: multipart/related;' ."\n";
  $message_m.= ' boundary="' .$strBoundery03 .'"' ."\n\n";

  $message_m.= '--' .$strBoundery03 ."\n";
  $message_m.= 'Content-Type: text/html; charset=UTF-8' ."\n";
  $message_m.= 'Content-Transfer-Encoding: 8bit' ."\n\n";
  
  $message_html = str_replace('<br>', '<br>' ."\n", $message_html);

  $message_html = str_replace('auskunft@izs-institut.de' ,'<a href="mailto:auskunft@izs-institut.de">auskunft@izs-institut.de</a>', $message_html);
  $message_html = str_replace('km@izs-institut.de' ,'<a href="mailto:km@izs-institut.de">km@izs-institut.de</a>', $message_html);

  $message_html = '
  <html>
  <head>
      <meta charset="utf-8">
      <title></title>
      <style>
          * { font-size:11pt; font-family:"Roboto","Helvetica","Arial","Calibri","sans-serif"; color: #2c2d2f; line-height: 1.15em; }
          a { color: #f15324; }
      </style>
  </head>
  <body style="background-color: #ffffff;">
      <div>
      ' .wordwrap($message_html, 70) .'
      </div>
      ' .wordwrap($signatur, 70) .'
  </body>
  </html>';


  $message_m.= $message_html ."\n\n";

  $message_m.= '--' .$strBoundery03 ."\n";

  $message_m.= 'Content-Type: image/png;' ."\n";
  $message_m.= ' name="IZS-Logo.png"' ."\n";
  $message_m.= 'Content-Transfer-Encoding: base64' ."\n";
  $message_m.= 'Content-ID: <part2.AF836471.09F8779E@izs-institut.de>' ."\n";
  $message_m.= 'Content-Disposition: inline;' ."\n";
  $message_m.= ' filename="IZS-Logo.png"' ."\n\n";

$strBase64Logo = 'iVBORw0KGgoAAAANSUhEUgAAAGQAAAAkCAYAAAB/up84AAAX23pUWHRSYXcgcHJvZmlsZSB0eXBl
IGV4aWYAAHjarZpnkiS3doX/YxVaAtyFWQ5shHag5es7qOohh6SeniI0TbapykwA1xwDlDv/9Z/X
/Qf/rFhx2WorvRTPv9xzj4Nfmv/8m+978Pl9f/9K/r4Xfn/dxZ83Ii8lfqbPnz1+r/95Pfx6wOfH
4Df704P6d/Qwf39j/Dy//eVB34GSZqTB9vdB4/ugFD9vhO8MZ/kOkHj1H2ZUequ/LW19h97re2H7
4/9afr/5r3/nSlS3aaTo4kka9KSUvjNL+j+nwc/E95AaF4ZU+N3e95zK92EE6me27s8j/PrXvb9H
Q/59SX/N4s9v7q9ptPXPWczne0X6S/C/y3X88o9vBPvnbL2U/Hng/DOj+Psb8/zk7e/Bv3e3+9bM
IkYuhLp8S/PFKPw8hgup5ZzebYWvyv/G7/V9db4aBbYYavtF3U9+7yGSretCDjuMcMN5P1dYTDHH
Eys/Y1wxvddaqrHHlZS/rK9wY0097dTI8YrHkeKc4q+5hDduf+Ot0Bh5By6NgYcFbvkfv9y/evP/
8uXuVb5DUDBJffgkOKqLmIYyp+9cRQrC/ebNXoB/vr7p938qLHVV5jKFubHA4efnEdPCH7WVXp4T
1xk/PzkOru7vAwgRYxuTCYkM+BKShRJ8jbGGQBwbCRrMnP6JkwwEs7iZZMwplehqpJUYm3tqeNdG
iyXqZUCORKi/KrnpaZCsnI36qblRQ8OSZTPwsVpz1m2UVDJoWUotQstRU83Vaqm1ttrraKnlZq20
2lrrbfTYE2BqvfTaW+99jOgGAw2eNbh+8MqMM808bZZZZ5t9jkX5rLxslVVXW32NHXfa4Mcuu+62
+x4nuAOCnHzslFNPO/2MS63ddPO1W2697fY7fmXtm9W/ff0fsha+WYsvU7qu/soar7pafx4RBCem
nJGxmAMZr8oABR2VM99CzlGZU85gB5rCIpM05cbtoIyRwnxCtBt+5e6PzP1beXPW/q28xf8tc06p
+//InCN1f8/bP2Rti23Wy9inCxVTn+g+3j9tuDgpc+Dp+NnLLrVNq9uASZZOC3ibs8M6I/bOfHYp
dDYDW6yjr5NrsL5tzOBGDaf0AMJOv5k+waOd7n0gmUujdu659ADrjazlnLh3zbf31av5syLXj2PO
JvXXNaOT+s3jgph+ZqDvEphbydOuc3Sv+Ox9C3msYZJF5sfS5iozx6JglxrXquk2I1EAbuWKTMxp
7UCcWl5jND28j7lYaEpbPyJJJLRlpHjyHY4iYWYgclk370xYGnBeStuUoGfdk3SShji5h8DWNCtp
mX2sNggrpVvGWdNRdFZDX3XeYNP8aCeR/US674zL2l6JpF5envX20GyMK0wqlw6YIZ1hdabt7sgb
sGO2fg5wbVqOLN9yPy3tMhdppGgokRzJZt2pUCGx9hE2CxwxEoY1miuhLYgLQiLQdfWipeY41gAw
z8yNedVZ+qA0vB3fz7vATp+dQvdtXQuLFhk702CRQiS5d21aJJ2a91WuLdw0drDdb1tzhVrDKHbm
pmJqIar01ujFrGWXFo/1tMVkZrnRGiHN1ogKNUtTED/bO9BacY60x6gWafo4AIcRG/3Vpn5xY7YN
jY+MALtctcfdBQC/G9EXO9Mp8xJ88yrSfsPs907me6n+eulzosI7LpH6dG89NqlA7tVSit763v29
l78/d1vIVGVNZVIHO5RLauc87tzemvGsvLZImx6ZnjzR6edciJIQbGa9j4Fbq266iw76NZOfibjf
Z/JrFX+dxx+r+N5Jsf52q/sXEUilM/vW1tlAIFGlG9YqUZroWlqIabMX8CZ5TA+2ssY5xzwZLjNl
JDOoQu1bY+p9WaI6bh28CvcASqfxOA0tOU4B8TxHYdBDoLCa4bOYkj9BNaKowKokavILsbMUrUCX
UTqADF/hcrs/w3ELAMdExiqDe/imRrzlCTJWZNBDFrox0GwHlX5DH5QzuFFApZnU2J1eC6fluuKF
ElhGPPfsdAoAUZmYnaXGkxrv6YJHm163ui6dQnSz6nrGdLabtFUUuLbB6OcCNpGgr3xR7lT8qoiR
5ddOxBQCSguopPJpUcAFaG1A8jnZ9bN84a0EioAV54yVjNImtLZz7vQ409evq5O0C/0uZgeXbkbG
FdRknfA4gHoI0tqeobdDCom6KoR2bLskYy4pgx7xxkP7v1rde6Y4+oSWmXNbVCws4pk9kMGAqyoA
RAhE8srP8OVQV0TwlWABaUDpCYQPKAp4odWJJBiIGM3EqsgdLT3wIA3gZvO7wgPXg4QEcRtrApIg
oAjEU24XVoXHyHnpEKlv7hFProB5JU4FRqpkhDcTtX2pMtQPpQIVjX3brIAlsO8XwYGEERTWkc+N
rF2AlvBDeFyPNqLUN9zUSG4sgB+xK/3A4XfTsjHVUDf1eMDyYQT10j6AtSMFQAFKuROxQzKZ2Wlx
QLi9VKQ+FISOuSGyeOCfEre5MhzXRXmxIYVmHdWFNxf4YJvtWmanM66NfgH11WB+CjKQg5PnOAPk
J6Jz70jYM1VXGaaqSR1TDkxmtiBOqlT7IadAd39EcWEXW4uXVoa3cqMtIhD/0k9JalSo+XqH6AhU
JlHxjRaBmOsyJjvXhn+4E98uEint7A5xdjuw9YQ9EOK9RGUZ3qoO14JhJjcshssNxTM6jFdFktwu
vDkn4XmGQBRJ7fsGKw7W46xE80Hk60zoaJ4bIAABKs8JrJrSS4IHerDe3YGJqRsEjCwfkRDeX/X7
F1IHzB7bAyigeK4wVtbSyxZvlUtCG3A2rZwhPvPrGaE2cIOIq01z33WkWigOB3SCGbMg5TZsRpl3
QGFF/r6gZZkI2xkRPzuxhN0RcJOOSKk/3qsT7YVCHG5nxqTJ0CCEif8g8AV60zrnMN6GeUcVd/vT
pRzPVqxCYXET+YWxrcGv4pjgXplwNoAOJOin5yVs64uIBGEuz1QjISRAbv1m+gWZMMYmGRnZPJIb
Jwfa5k6jxCOk0zPQW28MXxL6898gzIN8dAJFvv2TiBdaueCRJ+yMUOgZtBHYIv6MaGHgZ6GMOmo6
kVXPSrHQ9FHP9OyjiXzqI8n2HuTJy2G96yCiEHYsTih0xTVkDLHA7PEc1GrWBgAZRTUPXMDIp/do
g66cqJGNWElUu899ZoRawdISoVMR/rPmgXtBkUzwdO1I0m+XNAcwM9EAi2hmn48D37gDGSonCjAm
a0iSClMi1yEpii1Jcq2KTjMihj6uogOKGvGSnmhZubqKShCrhlFXPUTGFoCXykXlQSC7edZAjS2Q
vJSAsGxZyz7SiIhQCi3BPhQk1gFuzGihLsIAa+2ineE3YIfZEOFLMcWLucoTMQoRr8j/GwqISH9a
i3ZyBcxTJ86hfoZH0t4Zsd8N5HilqHdmJQ9klTTQsKhA2jIyLGB/ZBOzd4KVOZ43gps7lQjzzwBE
FnoIDIbN6kBn83SatRvi3IirBIvYvhAjaAChFQApkBzMixGGxfNPspgK8C63T2zQNZTmYxg7sS+t
kGaBvmIt0BJQ170TAIHkACB+4OAreqYjaV9aY4WFCYGXjyZnITWwgxlvWke2E7zlkVn8gqyh4xHj
EI/fPKuR50ymQTQ4jOCBik17TqHmgNJA3DRyj16sqjUMT6CWIDBmBD/AFlTJmZQbmgv3wvIacgEi
xypDOqdtvquwUcd0AYhEMFB83ngWN1W3oqa4cBSEAToH/jIoPiRR9RtuXPeE2vCsldCKaAueLICx
RyHCxl4/3JFZAZTgYMrRiPEF8MBk1njQ9FgY4ofJQPraMCHajLQedY8XH8jCCXSTtYGoBr2WABxK
G1Qc33xoqIvIwHQCvfUsAUHPYHqhnPrHWtSLxI4NMbMdC8aE4tn3iNS7hB+ln8kJw4EXR8RHNwCp
OCrwFvzWik3btQKqq62OHh34hF6CT2E/8PMCkxVGvX1DGtLV6sAnUk0gRL/LMAMjmMvR6MgMLTGb
p0aQYXRlhakYgFa3jkez8TCJZiCM9/j0QSjKYgtZ8PJUp0cXdEQYLIIigNtb2RRT26yyZBpAmyiI
IxAzEY2ONADFiGSG+7kVtZzAXnADVu0J4EIfSVq86iCHuNAGHapCkO0q+UvVb16HW5CGVZqP27pP
8ouIjTMi3gKGccwqtxB9E+Ha8+BMCTTxYA/wmAoSRfag6wH8kWTsDuZwBdSA/DNBigM1Quiescc4
0JfI2S0+wf+tQN2ACsyBatK26UDbJhQbS0CH0lrohyUhBK9NTCEcSRVhFkfLViA9JAhsi7NlSGod
6idqQ0WVJriNDUBooHrz0D4Mo/biKggWRo94axZJ1NLjRlCT8RNhw4aCrQW4PhWGID7zJ39qT8Rl
JFYwbSWHBhppk8dwK7QrtQdvo7wE9vBqfvsOsXL3RVAQ22jgBEWKeZechn8cjihJwhbJCGKNMpC8
6GmjE4FmVgQ5dnQuhZCRKHWJzKAH9FMBsfHMHWCVg7TesfA4zJFhvkX4k7xDmjDnyiiEZYSX+aFo
F7oTQsgFV8gYmUVtyndOhwwwSIyVg4/wEMkM1CHIMWl1ckXFGihCu44YOu+B3h8fADKQ8AONY5Vd
DmJwRB8mNCF9A0zmA2ijqiPBNCvtRVdgTmrUHpcHRyw/IdqQk22JSZNDp1ZUKVmhe7Sb0wLVhFFo
6EzoA84LnyMCkC/7kfSHaBhYbKAn1AYM1esW7JK3aaMsPTWxpLhQkGhdsF27ejOQIER0zcps3AW3
ZUJy7MlJJmKq0c2AnUCAIn0MciEKlD7cw9I6GKsNxAJtSUQiLen0Xk2AQKNT4WBIwcD50dGQFd7C
DIGmK9PyTzytqA6lIugURCNqASmOr69oyDkEhbhsTI7huAELOtjdsMs8Hb1AhVCSxBEmRdiAVvjM
WlVDtAqmfT9aoFNJ7BckcZ3aIYPwHOJam/71k3aMWaQeN5r5gn1ocLgNoQcHoiA32K0TAPQHihKe
63K26UpI4bJNO0FV3nQlYH1oQbVNxIlZRINPChtTYXCl0eza+iECucHviOZBr/CFX7OeW+bxOi3D
FhfczrIrIMyURY64Z7RVog3yzgOHgJClzaUMQIiK9NZB2nF0S0KlWdIScvUUKmBuQbOCcOPqkhQ4
ZKYQpefDS2Skr/SCTJosTNCp6BgwpP/yKsrt3+EO7NXjIjHRIyL3j0zEXGnox1nYEyTjU9fvO0jM
+nFUZT6WIQQottscWhvnCUvTt7A8HJ2QySjsEmU8VyMKAhBqh9uJYxHDNHw2QvRivpCAeHGTPsJJ
4yi0Rz0WfgiIRm91VBBMgNwCUg8pZbraoztofq452q6Mkf7YKiQP1O4D4KF4WWshn0Chdl9kTLir
y7QwPXCZbxSTUN8jvrSZGNAeMgJ9TKyoLc0I0ytd3cgoOboS+sgIzakCJU0tJMXZoL1dLrzMihIe
3q/tO+ucyRGJzIKxnSAEmLUedtB2L8ZNCI+nhnVjlMDHqhEl1k2DVu1bBwkND68Jj/CMh8LlAonN
UIeOZ8FHRDGS63nui5VryGdIbjOnSq7W0K7KYTE+IGuWNkmPTAWMSl5o4YErQQGipDsSm3oEoyFz
eumUgCrTiSF5pDcJVmu4EkSVA38yLY4AqaHitJ9BPED8hZUgkL0wQ0grWqLpnIulUbwj0YfA9tJ+
HotNzYEWG/n2iuE2xunnjgjT5rWackw9zapzli6mxUslTQYQJZHyTUwcrN6OPqSM4Gw5FBCI9r10
KEOGA+fAlMgQkp7QTmjvCvdw3Q1FtrRQJAQZwEquA9I6nkfvMSmGhJkjCAfk4H9P/nhUmeagwwzK
F/64iNF4jPcQmLdO1VECjOC7/kknLqpARRoI8Us1+wtFDu15SioRYnAXyN0yiBAJYhjigFmbI1IB
2NsGDGw5A9BDkBEAMLxbQUt6HQtYgQUlATA6qww671iHAJv2ZtI5Tvv6J9eEn1z9HWjgeesUnOnj
ClwFJW9odgJGgFTV0VUQWQ5ZGMURy5WX00mGzkslw7K2PVFpCGUYpooIoFOkNZWGFmIFiJ2LrAn1
AbswlWUCw7s5uBrUwrpTSPBCXIAg8QlzkR+yNUVfh/lTsCgabWdMPLsHxOA6qIWXAAjvKM1z6Qaq
6Szt/R2ooXXsqCQ0UAEaYD8RbDiqO5A6F3gHXwB7oCrIZlAPhvRDbq6VbNAapQXkBY8lpUy6A/A9
6OCwp5Te5wgaIiNoa5UUzs9G9FIciosIdXzt251WYeS3aaEpYysuxUbFYvxQ6aUHAAu0wYxVnfgQ
dV44AOax5mhAnnoAXNAnZbQyHYHPTQ2YOZE5bcz/4FFoBB0DsgIZFBmYhpuasIqOHV2Tb8/4U2rk
ihCZAT4goLLBi9ZkLATuFgK0ESGnnnjIFXI9jdil8hPuqG81V1wEE8OLpkqiF2wXLZ8A/0EZ02Lc
lck7PuVIS2iDiuFITUT3IF4cChEnjxpo4DlQjHhFQGS0kyyi2nPL+oMMdWbSA5IXHB7emu6Adluu
EjnNUdAGJ1x9WgDxnwTquOvmVR+FnC5tplDE/vDW8UOntNQiuPMxrRtGjcEctTh1SLWJM9WHnAax
aZIC1h8PNMrzQO8Tgs1DR3RY6IoT6gNIxTS9ze2UXWwSBQTRmxnYmbU9gg+pgi90JoSEDR+Zqs06
psJfTrwVblKyNyCfogBpwbQZlauN5qrzmdYw8XeGXFEvlNShPp+GwwILmEw7PUkAg6A3HbYhUG9b
3hWEEUqLd/dodlkjVXl2M1RR2XNh/uF60OogyBR+aJdF3kGvBqBA2utSJu4zAXy++Gy33aQ7veGt
lwKgUwhPSXuVMfYGfYyw1kY9FgwaSSAunbWL00aoR0tHOjxHkC9Sq/7oLAWvNIGz3TD8A7BHNDWJ
PoN9cWCFh9G5SN0GgjqmHEkBur8gfGnqvvQJjA5Ll/INFOLmE6pvoLD7iVaBPmc3ORqQ3tHPT6oh
Ime4iC67+pCDvdaVIEYFSzu9bSWk5Of1lbmg4mMS+vU1vLtJHxbQ1dpRAgZxL7Q3lEW4NvJTJ3mg
Cc5w5YFDIlMdJYsKAAYyontDXZcYgSD+9VjMo8lLC7yTlHLv36rQydtPXVhBYmHjyq4KSNOJE9ap
Ogs4egTjRJka8Qna9kqSPuNmFSVXwsDwZZfaFQegZXDLuEBtDkgQFSbtgpAMkQ5GL6TwbESpUulv
W+4dYkOSKK+mj0mg2vqI68nfjI1DHbAMWtqyM+AoInG0/4qkkuhv3DfxSBfBxmVwMpVHP2iLHdg4
QXum0iTUECIOgML2ovznPqdnHZoQqVwDt0fqpsOVrKcwNrBTIUdoEH4meJ89GhgM4L5YLsXRqYu6
TjehzjSRIehWmQ8dAW2UIPCZoUadUODiWiWyRymDKlICKSFDbiZG2qNEESIQgnbNS0bm6fBFB33q
ainD8wR+ifroCUDdI9qS6WkPeUf0bqQnHVKBwrz6EEVA5b+cY7KI6oXioVGpCJaLpnqnJDovQy5g
4dot2KOpz0FEHxwFdoSWq1CPDYXWLrDeekSs6wSLMmhPzoGUF4Ssh9LdK2jHNOs4kHpE9UHZOPmI
7sB56QMkOtZZI79TvYAIFFSjkTEWrUnTRKbyjqXiFE0VvAfSuYztcA/AuLRxfRoUPaGdnXH0CZuO
4abvdSoEQC4QlYuFnovV07+5LKgn6zDPLazt+bi1yLPBjRWqBIYUlJCR4hEvehhlq3LbFlBQHTCn
tgqIh0SvQyld7DTiAdyKO7X83IlsZx3yqdoq3ThDYFEfE0Ag0Xpv15uixvofBAfF6YD1tFFFXTtV
yNmj/bmjA26zrL0VjWOGcIFBZCQAPu3NY4rwbWZHe3ZoaTd1MmhURGO+qm7MC45Mx1dkX66DaoTd
9fk9ECPLxSoJaRZ6/7YOWtJz12V4vQnPsOQoAnJNo43aKTN45kDooSZ9wLBpM70iE7SRTyDmYYi7
+sAnQkTOMrB4sKf6cOju8t6BLoMfCBWYuOiTQjXRdrTVnPDc7RD+Ql8dSK7p85Nhe+dpQqIG3W11
gmESuz6MA4C9TQIdaH8/G/Avf7q/v6HqQNyKw7cErlSRTArT3M8dZBAKQMja8AmQwQ25TOlsABw7
0H1/Z27x7WkitloB4ojefh8fkpaZsqLQJCrh6jRVuxcIVAIcADavDdrd3X8DEknf+yB1xRIAAAGE
aUNDUElDQyBwcm9maWxlAAB4nH2RPUjDQBzFX1OlUiod7FDEIUPrZEFUxFGrUIQKoVZo1cHk0i9o
0pKkuDgKrgUHPxarDi7Oujq4CoLgB4izg5Oii5T4v6TQIsaD4368u/e4ewcIrSrTzL5xQNMtI5NK
irn8qhh4hR9hBBFHVGZmfU6S0vAcX/fw8fUuwbO8z/05BtWCyQCfSDzL6oZFvEE8vWnVOe8TR1hZ
VonPiccMuiDxI9cVl984lxwWeGbEyGbmiSPEYqmHlR5mZUMjniKOqZpO+ULOZZXzFmet2mCde/IX
hgr6yjLXaY4ghUUsQYIIBQ1UUIWFBK06KSYytJ/08A87folcCrkqYORYQA0aZMcP/ge/uzWLkxNu
UigJ9L/Y9kccCOwC7aZtfx/bdvsE8D8DV3rXX2sBM5+kN7ta7AgIbwMX111N2QMud4DoU102ZEfy
0xSKReD9jL4pDwzdAsE1t7fOPk4fgCx1lb4BDg6B0RJlr3u8e6C3t3/PdPr7AVKUcpqzJLNyAAAN
HGlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0w
TXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRh
LyIgeDp4bXB0az0iWE1QIENvcmUgNC40LjAtRXhpdjIiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJo
dHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2Ny
aXB0aW9uIHJkZjphYm91dD0iIgogICAgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20v
eGFwLzEuMC9tbS8iCiAgICB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4w
L3NUeXBlL1Jlc291cmNlRXZlbnQjIgogICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9l
bGVtZW50cy8xLjEvIgogICAgeG1sbnM6R0lNUD0iaHR0cDovL3d3dy5naW1wLm9yZy94bXAvIgog
ICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iCiAgICB4bWxuczp4
bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iCiAgIHhtcE1NOkRvY3VtZW50SUQ9Imdp
bXA6ZG9jaWQ6Z2ltcDo0YTE4NDg0Ny0wMDcwLTQ0NzktYmIzOC01ZTMwZjg2M2M2YWIiCiAgIHht
cE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6YTlmMjAzMTctYWM0Yy00MTVlLWE3MjgtZTZmZGEwMjdm
MTUzIgogICB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6OTdmZThiYWQtODNhMy00
YmRkLTg3NjQtMzNiN2NiMTEwOTMxIgogICBkYzpGb3JtYXQ9ImltYWdlL3BuZyIKICAgR0lNUDpB
UEk9IjIuMCIKICAgR0lNUDpQbGF0Zm9ybT0iTWFjIE9TIgogICBHSU1QOlRpbWVTdGFtcD0iMTcx
MDc3NzQwNzE3NDc1OSIKICAgR0lNUDpWZXJzaW9uPSIyLjEwLjMwIgogICB0aWZmOk9yaWVudGF0
aW9uPSIxIgogICB4bXA6Q3JlYXRvclRvb2w9IkdJTVAgMi4xMCI+CiAgIDx4bXBNTTpIaXN0b3J5
PgogICAgPHJkZjpTZXE+CiAgICAgPHJkZjpsaQogICAgICBzdEV2dDphY3Rpb249InNhdmVkIgog
ICAgICBzdEV2dDpjaGFuZ2VkPSIvIgogICAgICBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOmFh
MDg4ZjQ3LTI5YjctNDc3ZC1hMmMzLTM2ODYwZDQ4ODgzNyIKICAgICAgc3RFdnQ6c29mdHdhcmVB
Z2VudD0iR2ltcCAyLjEwIChNYWMgT1MpIgogICAgICBzdEV2dDp3aGVuPSIyMDI0LTAzLTE4VDE2
OjU2OjQ3KzAxOjAwIi8+CiAgICA8L3JkZjpTZXE+CiAgIDwveG1wTU06SGlzdG9yeT4KICA8L3Jk
ZjpEZXNjcmlwdGlvbj4KIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CiAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAKPD94cGFja2V0IGVuZD0idyI/Pnj/
hP0AAAAGYktHRAD/AP8A/6C9p5MAAAAJcEhZcwAALhgAAC4YASqqJyAAAAAHdElNRQfoAxIPOC88
YRAUAAAQX0lEQVRo3u1aeXhURbY/dZe+vaW7k06HkJ1EIKACgoDK4gKD6KgjoEgYWSSyb0FFBERE
eCCLARxFVoFhZBQiPBAZEf1AZPA5CoggWxYIZO0lvXffter9kXSbmG4IYXvf+6i/0nXrnlSd31l+
59xCcA3D98YwljBMBrFX9AFR6AGy0AGwlAqKHEtkPyB1rAyAKt3pyYWreqUeL2XxQR2jPWxiDJ6l
D83GcGdcdaAmAZE/Q09Ki54gHtto4rc/BFjUACFUdKkUnOrbo2huhpRAI9qnobjtBla/tpU26dy8
btOVO2pvJiCBhVMZxVHdB9suzIVATTcgmG6yZLXR9vHzXfft4jxDMBCaQbQ3ltFvTlUnLl7Rc175
HdVfIyC+t0bH4soLc4m7fCwooro5wpWU9qtyn0pMccjeZ+qmiIZSFcWzpimtdCn7F3R//Y63/GHQ
kcEYk44vnf6EuMuHAFHYZgsX5ZaWtNavHdNIf5KIYgAAJBPFzGNhQEAOerrnPnz8+KYjd3JLvdEo
D3jnT2qFL/22g3irHgMg6HqEE96Z0vNwcVa8KnYBBZQcmheJrKuWnMuqhZrJr/wwn7kDQ5SQ5X17
bBwpPb2TeCp6N/l1ipUASBAAJCAKBoqhgQALoGgAKwwyJO/699Bnhq8iZ/a6FX+PBh6EKDFZZZmY
GZO64Z2ur5Fr3fz012dQFRXlBoHnqSbtlqJAp9P5Nm3cKIbmXnxxmI7ng1yzrJmiiVar9WzatPGK
oXf4iJEmURQ6ejyeLm63x6zRaCSO44oMMYbjiiKXPfLoI54J48cTAICwdfqXvcoq544vIJ6KXlff
CRtEnP4McLq9oI39CWljSihTvAOc1QIxJ+qJw2oh7upWIMvdgOHa9tx/gtna37zIh4MFCsHhfKQQ
rKoSHUsoH3USAH68VoVUVlZkVldbd/PBoL5p9oMgOTn5PQBYCQAwbtx4qry8PN/pdD7RHEA4jpM1
GnVOtL1PmDhJXVFeMay4uDjP6/VlY4wpAAC32wMAQBiGUbRazbmC7QX7cnKGbjbHm0+GAcHll54g
7qrcKzIvmuORxrQPGeLfp1p3PKybMl+MsKoGAC4BwFEAKBC+2kojlsVZ+ktfO1zu//iUYAPvE4kc
a5dcy8cfmtXvo94LfdeiEEHg9X6/v7XX62ty2MvKuqt1+H1RQEGeT3e5PanNAcRkMgLHcbGRnr0+
Y4b2xIlfV1ZWVo3EGEfaH5JlmfF4vHd7wHu3x+sdoVKxXRgAAN+8cQZccvIdUARVFNMioI07i8xp
s+C+bntiRrwuA+xumhX1HxpyZ2nItxPXBRT+IQykwQb9Ct/NK/tGLjy26sNZnSc0OXQxDCMwDGNn
GCYccjDGeowxW+sQiNA07QEAXPcb+GDAGs5xf7Q3mlZomo4afjDGtCzLYSKkKIooSVJEIzp+7Hhe
tdU2KuQVDMME9Hrd6djY2Is8z7MBfyAzEAymSJJkAgAkCGKM2+3RMQAAxGkdRPzWeyNjQWMU02IH
yrw7L2bO6nKAgmYnrOyYtL01Tm8Vj8WUBskfCF0jeadag9YtAOBuqrxuXbsVajSnevt9flWdwvGF
Cxc/9AcCjwIAqDkumJTUcrBarS4PAZKenn45mjytVrsjOTlpfeR8Qansdvs8q9XWOSRLo9F8lpCQ
0ChcvfDCkIzikgsTQ2CoVGx5Zmbm2E6dOn315uxZCgDA+g0f07t27epx8WLpXkKILmxk3lnDOFxW
OAoIpiJkQQmZ01czmfe8oZ2xPHC9DMIlBZwxtGY/j8WX/vjMj/kMl+h5GgD+0VR506ZNkwGgsP5c
167dvb+nDIQFQTi/a9d/X2xaCBQuKgrev3NHQQPnyV+ej/bv//avdrsjbLQxMTHnW2W0mr148WKp
Udng9/cSRTEp9NtisbxXsH3blwXbt4XXvJw7SunatVsJIURuSHv5QBsIuro2NgkGI3Orj1Db+6ff
CDAAAFb0mEfUFPcNBUhuRJGBMC7Z+9yEQzNpuEWjcbKMHC2PHf2ljd1uXxQKhSoVG2iRYJm+YcO6
iN6GFeUejHHIKECr1R6K2MxQcwQAKEIIEEIQy7LAkICvHxBZ9Qc2QpAxZTvdrstMXd4i4UYqwcSZ
fi4X7UEgJKaRZSl8D61Ka6wjBjd9sCwLDMNYOY5z1CpI7TSb4xqsGTtunO7s2XPLgkE+JZSX4uLi
1j07YOCenTt3RJSr4lQaQgAQAiCEAMuylkjrMjIyHMmSMsdoMsQH/AGP0Wi8xBDe/yCQhgUg0sUf
oxLTJ+vyFgVutBJSNC1Ki3wX3UEiNgJEJnJMTdDdEQAO3ApA1q5Zo0yZOvXVWJPpXQAgZrO5/IMP
/hZ2kwkTJlHFJcVjnE7Xk6G5WJPpWEZGxvwRw1/E0UOfWIjqadThcIyeOGnKdx9+8H6w/rotW7bw
IQoeziEIK/c2cFRGU0O1bDtFv+Bj281QwhlXocwhtiIIDRN7XcBgfTjQ+lYBAgDw/sqVNgCIeFa7
w36/w1EzJ5ScOY5zmS3xr6xft9ZxJZl6ne4gx3FOQRBiAQBsNvtfZPnUkpycoe/8859bbVdrnaTU
S+IYmdPe17+7+cjNUkAsayQcpYrIpAgQSsRy0v+FFsaw4SNiq6qqVvA8H1vHsrDFEr985+cFh672
7sBBA0/FmkxrQ3QbY0w7HI5JxSUl+/s93n/kkJyh5qiAEN6prdcy/5Vu0/pvwsGc58TvJ3S6GQfl
KBYYoKPWGmrEGFed2IhuJxjDR4ykKyoqptfUOB8IzRmNhoMpySnvNeX9oTk5RK1Rv2OxxK+hKCrM
wgKBYEer1bqxpKTkUP/+T74+esyYtAgegkL1hkQZE95l7i19CgLHNhK+9KGbcdhq0QF+wuuuxH0o
RN1W73C73X1qamqmhoiYSqWqSk1NeWX9+rX+psrY88XuQMdOHfPS0lLHaLWasyE1y7ICgUCwfXlF
+eJTp05//+ennp6R+/JoYxgQxBlq/4na9DP7tNYL/KmVgN16UBxP3IzDtjdmMTJRorYqBJCc4zqM
ILcLjOcHv9DSZrWtlCRZWxeqpARL/Lytn3xy4pppfn6+uOeL3ZvS09MfSWqZ+Kper/uNoigCAEAI
gNfrTbt06fKic2fP7Rg69MXMWg9BqAwQJdN3J+9F5PxKID5TbV/A9qB4MOeuG33gyoA1I4iF2MgJ
DWEG6LLbBcZLo3JZu93+Xx6vNztUQ1gs8V+kZ6RvvB6527d9Vv31vn35bdu0eTgtLXWUyWQ6jhCq
A4Ygl9v9WFlZ2dZhw4abKaCZk0gfZ2M7uHuAYr2r3seMOCLVjLrRh7aLrl6EEG3kpA6iltacux1g
jB03HjkcjsFOp+uvoTmNRn2xRYsWr61ZvfqG1GKbN29y7Pli96a2bdv0SUiwzGJZNtwHc7pc3ex2
x1gKqWOO0PelYCAlfRu+riCQSl8SDuW2vlGHXn58A+WR/c9hIBGrcRXFuJM0llO3AxC/33+X1Wpb
LMu1RTJN03xycvIbWz/5x4VI69/LX84sXrLUEE3ewoWLTNGebVi/zjlm9MtL0tJSp4eSPiEE+f3+
oRTKaLmfTrTGAwiNW8TYnQj+X+YHf5ymuhGHPuctesCH+Z5Rm3sU971b8nhvNRiTp0zRXL58Od/n
8yWHqvH4ePOWDh3u/TzaO0eOHBm0Y8fOnx7r03fZc88P7lz/2ZAhOW3/9dVX3z3yaJ8tzz47sM/U
vFeYxg3IF3CCxbLJYIgpDs1JkpRNqbqiIhQTPBa5j0MAFOtAyvPbq0Lp36+LiuYf+1BVKdhnykTR
R+ZWSNbT2m2rei26pd/YJ0+egi5eKB3vdLrCJEan0/2Wmpo6Z97bb8tRAFRXV1fn+v3+Nlar7dWq
yqqP8vLywmHY7fEMdrncHex2+4tWm+0Du90W0VtkRZF0Ol24UJRkmaJUj23ggdFvAEBRFCGxIBa+
CYVbJkln1jabjx5znRvhUfz9ruAdhVm6lH232juqqqu6VFZVzsa49ooTy7L+5OSW0zZt/Lg6KjGp
rLwn4A+Ev6yqNep9K1asCAAATJo82eR0ugYTUmvgMXr9wecHDYxY2TM0pXe73Rm/5yxNWa0rqcwF
oCRMBaU68jcREtSCULgYlxUkCz9MXcA9uPKavuxNODSjx/lA+QKFYFUU71DiWeNyCcENDVdX485j
x40znjx5aiXPC3F1oQonWCwfmIymg70ffjSi8fXr1xcdOHBwhChJ6rp2itMcF7c99Ly0tPQRnufb
1YFLdDrtzmcHDGi0lZkzZ7NHjx6dEQgEw50Jmqa+Doch4ZsnBoJw+lMgwhWu/TAY2FaHgU2cpUp9
6ghqffV6IefbKffaJefnApGikgMjo/suS5P05Iqe86+pmfnZts+pjz5atdRut3cJU2eKugdjbK77
WyGE/EwI4WuVp5Luv79L7prVqy/NX7AAHT92/O3CouI3ye+3MAlN0zaMcTBq2YoQAoAEjGvvBpjN
cbtnzpw5oP/j/fCXX+6llixdutPhqHkmlItYlq00mYw/qFSqw4qCLyAEAiEkMRgIDvZ6vX2VcEtf
5UlPT3v092SjTtkDim8zSEW50a//yBRIhb1BrvyXWFL9pfBNv1VInfo/qp4bpEir3/pxSdKvnsKN
ApGi1jMsYqwWVVzetYIBAFBWdlkVCPh7AkC3MA/Bv0feujDUvV4Xlqg5LhEALgEhiOO47qThlVik
KErCFb2OkHohh5b1ev3G/o/3wwAAFZWVNE3TAYqiCMYYEUKQKIpJVqttEEJoEEVRMgBgQgiLMUb1
5IgWi+XNrKysX8KAcD3XicL3L88Cv78dyBU9rhwLfDEgnR8CEvsMkarPCvse3gO0/jAYsk9SKqMN
O45gxLWEDwlvS1e3GGBWDJ39SvC+gMJnApBEmci6LAQtsgC3qFTFT9OoE040JyRptToMtR+7SBMi
FCCEiKwoEgAAz/MgiqLY1HcjiePU3Jm727c/8OWeLwAAYPTLudKIkS+NMRhivrdabSP9/sB9iqLQ
AIAIIaAoyh/ZFtHptBdTUlLe0un0ny5bugQ38gThwNOtIFhUANjZuel7owAQwwPQAaBiqwBwMVCa
KkCcG5SgFwFCQCEdBlU8JmIaAeUuwF6DosqaQ4wdVhu7Lmz2ldLHH+/frryiIqFJnWZTrJTdLvvo
urVrBACAnJyc7JOnTrdoHh4A7du3r9r22acRC9kXhuToGIZp63K5HnO73Z0QoEyGYRIQhRhRFGso
iiqM0ev3GwyG3dnZbW1z584lAFGu/AgHn8uAYMkmUCp7X+/txcj4mbzAJOWBoePfuQeWy/D/fMyc
NYsuLyvnCCAGIUCiKMpJSUlC/nvL5MYEJ1qT798TzeA/Mx+kklFAeO7GbA0RoBPPgKbdFDBkH+A6
v3XnXm9TAQEAEH6ezYL3XH/gz78FiqMzgEI1GwikcwKbvBY0Wcu5Xh9b76i+GYCEhvjTHCPxnf8L
8EUvAXZ2r/UYTF0VBEAK0AllQBs+Q1zqeqB1Jaqea+94xfUCEgbmu+EqAJIFQkVfgoMPARHuAVDS
gPgNQIIAlEUCIpQBpSsEoH8CxnQQcan/Qeo4H9t18R0gmjD+F7draGlSEXGRAAAAAElFTkSuQmCC';

  $message_m.= $strBase64Logo ."\n\n";

  $message_m.= '--' .$strBoundery03 .'--' ."\n\n";
  $message_m.= '--' .$strBoundery02 .'--' ."\n\n";
  
  if (is_array($files) && (count($files) > 0)) {
    // preparing attachments
    for($i=0;$i<count($files);$i++){
        if(is_file($files[$i][0])){
            $message_m.= '--' .$strBoundery01 ."\n";
            $fp =   @fopen($files[$i][0],"rb");
            $data = @fread($fp,filesize($files[$i][0]));
            @fclose($fp);
            $data = chunk_split(base64_encode($data));
            
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime = finfo_file($finfo, $files[$i][0]); 

            $arrName = explode('/', $files[$i][0]);
            $strName = $arrName[count($arrName) - 1];
            
            $message_m.= "Content-Type: " .$mime ."; name=\"".$strName."\"\n" .
            "Content-Transfer-Encoding: base64\n" . 
            "Content-Disposition: attachment;\n" . " filename=\"".$strName."\"" .
            "\n\n" . $data . "\n\n";
        }
    }
  }

  $message_m.= '--' .$strBoundery01 .'--';

  $returnpath = "-f" .$strFrom;

  //echo $headers;
  //echo $message_m; die();
  
  $ok = @mail($to, $subject, $message_m, $headers, $returnpath);
  if($ok){ return 1; } else { return 0; }
}

if (isset($_REQUEST['type']) && ($_REQUEST['type'] == 'massnote')) {

  //print_r($_REQUEST); die();

  if ($_REQUEST['id'] != '') {

    $arrEventList = explode('|', $_REQUEST['id']);

    if (count($arrEventList) > 0) {

      foreach ($arrEventList as $intKey => $strNumber) {

        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$strNumber .'", ' .$_SESSION['id'] .', "E-' .$strNumber .'", "", "", "' .str_replace('"', '\\"', $_REQUEST['info']) .'", NOW(), 3)';
        $result = MySQLStatic::Insert($strSql);

      }

    }

  }

  /*
  $strSql = 'INSERT INTO `izs_event_change` ';
  $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
  $strSql.= 'NULL, "' .$_REQUEST['id'] .'", ' .$_SESSION['id'] .', "' .$_REQUEST['name'] .'", "", "", "' .str_replace('"', '\\"', $_REQUEST['message']) .'", NOW(), 3)';
  $result = MySQLStatic::Insert($strSql);
  */

  /*
  if ($_REQUEST['fnames'] != '') {
    
    $arrFnames = explode('|', $_REQUEST['fnames']);
    unset($arrFnames[0]);
    
    foreach ($arrFnames as $intKey => $strFileName) {
      
      $strUploadedFile = 'uploads/' .$strFileName;
      
      $arrFileParts = explode('.', $strFileName);
      $strExtension = strtolower($arrFileParts[count($arrFileParts) - 1]);
      $intSize = filesize($strUploadedFile);
    
      $strSql2 = 'INSERT INTO `izs_event_file` 
       (`ef_id`, `ec_id`, `ef_name`, `ef_type`, `ef_size`, `ef_date`) VALUES (
       NULL, "' .$result .'", "' .$strFileName .'", "' .$strExtension .'", "' .$intSize .'", NOW())';
      $result2 = MySQLStatic::Insert($strSql2);
      
      $strNewName = str_pad($result2, 11, '0', STR_PAD_LEFT);
      
      $strVz = dirname(__FILE__) .'/';
      
      rename($strVz .$strUploadedFile, $strVz .'uploads_events/' .$strNewName .'.' .$strExtension);

    }

  }
  */

  echo 1;

}

if (isset($_REQUEST['type']) && ($_REQUEST['type'] == 'messeditK')) {
  
  $strSql = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
  $strSql.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc(print_r($_REQUEST, true)) .'", "")';
  //$result = MySQLStatic::Insert($strSql);
  
  $arrEventList = explode('|', $_REQUEST['id']);

  foreach ($arrEventList as $intEventId) {
    
    if ($_REQUEST['date'] != '') { //<leer>
      $strSql = 'SELECT `bis_am__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
      
      if ($_REQUEST['date'] == '') {
        $_REQUEST['date'] = '0000-00-00';
        $strNew = $_REQUEST['date'];
      } else {
        $arrDate = explode('.', $_REQUEST['date']);
        $strNew = $arrDate[2] .'-' .$arrDate[1] .'-' .$arrDate[0];
      }
      
      if ($result[0]['field'] == '') {
        $result[0]['field'] = '0000-00-00';
      }
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "bis_am__c", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `bis_am__c` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }
    
    if ($_REQUEST['status'] != '<leer>') {
      $strSql = 'SELECT `Status_Klaerung__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['status'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Status_Klaerung__c", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Status_Klaerung__c` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);

        if (($_REQUEST['status'] != 'in Klärung') && ($_REQUEST['status'] != '')) { // Wennn geschlossen p/n, dann Nächster Schritt löschen.
          $_REQUEST['meile'] = '';
        }

      }

    }

    if ($_REQUEST['grund'] != '<leer>') {
      $strSql = 'SELECT `Grund__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['grund'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Grund__c", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Grund__c` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }
    
    if ($_REQUEST['meile'] != '<leer>') {
      $strSql = 'SELECT `Naechster_Meilenstein__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['meile'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Naechster_Meilenstein__c", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Naechster_Meilenstein__c` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }
    
    if ($_REQUEST['ap'] != '') {
      $strSql = 'SELECT `AP_Klaerung` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['ap'];
      if ($_REQUEST['ap'] == '<delete>') {
        $strNew = '';
      }
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "AP_Klaerung", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `AP_Klaerung` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }

    if ($_REQUEST['tel'] != '') {
      $strSql = 'SELECT `Tel_Klaerung` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['tel'];
      if ($_REQUEST['tel'] == '<delete>') {
        $strNew = '';
      }
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Tel_Klaerung", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Tel_Klaerung` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }

    if ($_REQUEST['info'] != '') { //
      
      $strSql = 'SELECT `Info__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
      
      $strNew = $_REQUEST['info'];
      if ($_REQUEST['info'] == '[LÖSCHEN]') {
        $strNew = '';
      }
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Info__c", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Info__c` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
      }
      
    }

    if (true) {

      $strField = 'SF42_EventStatus__c';

      $strSql = 'SELECT `' .$strField .'` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);

      if (isset($_REQUEST['status']) && ($_REQUEST['status'] == 'geschlossen / positiv')) {
        $strNew = 'OK';
      } else {
        $strNew = 'enquired';
      }
      
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {

        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "' .$strField .'", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `' .$strField .'` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);

      }

    } //

    if (true) {

      $strField = 'RecordTypeId';

      $strSql = 'SELECT `' .$strField .'` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = '01230000001Ao76AAC';
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {

        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "' .$strField .'", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `' .$strField .'` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);

      }

    }   

    if ($_REQUEST['stundung'] != '<leer>') {

      $strField = 'Stundung__c';

      $strSql = 'SELECT `' .$strField .'` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['stundung'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {

        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "' .$strField .'", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `' .$strField .'` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);

      }

    } //

    if ($_REQUEST['rueck'] != '') { //

      $strField = 'Rueckmeldung_am__c';

      $strSql = 'SELECT `' .$strField .'` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      if ($_REQUEST['rueck'] == '') {
        $_REQUEST['rueck'] = '0000-00-00';
        $strNew = $_REQUEST['rueck'];
      } else {
        $arrDate = explode('.', $_REQUEST['rueck']);
        $strNew = $arrDate[2] .'-' .$arrDate[1] .'-' .$arrDate[0];
      }
      
      if ($result[0]['field'] == '') {
        $result[0]['field'] = '0000-00-00';
      }
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {

        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "' .$strField .'", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `' .$strField .'` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);

      }

    } //

    if ($_REQUEST['art'] != '') {
      $strSql = 'SELECT `Art_des_Dokuments__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['art'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Art_des_Dokuments__c", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Art_des_Dokuments__c` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }    
  }

  
  echo 1;
  
}

if (isset($_REQUEST['type']) && ($_REQUEST['type'] == 'messeditKK')) {
  
  $strSql = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
  $strSql.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc(print_r($_REQUEST, true)) .'", "")';
  //$result = MySQLStatic::Insert($strSql);
  
  $arrEventList = explode('|', $_REQUEST['id']);

  foreach ($arrEventList as $intEventId) {

    $_REQUEST['date'] = '';
    $_REQUEST['status'] = 'zurückgestellt durch IZS';
    $_REQUEST['grund'] = 'Kein Konto';
    $_REQUEST['meile'] = '';
    $_REQUEST['ap'] = '';
    $_REQUEST['tel'] = '';

    $_REQUEST['info'] = '';

    if (true) { // Wiedervorlage Datum
      $strSql = 'SELECT `bis_am__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
      
      if ($_REQUEST['date'] == '') {
        $_REQUEST['date'] = '0000-00-00';
        $strNew = $_REQUEST['date'];
      } else {
        $arrDate = explode('.', $_REQUEST['date']);
        $strNew = $arrDate[2] .'-' .$arrDate[1] .'-' .$arrDate[0];
      }
      
      if ($result[0]['field'] == '') {
        $result[0]['field'] = '0000-00-00';
      }
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "bis_am__c", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `bis_am__c` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }

    if ($_REQUEST['grund'] != '<leer>') {
      $strSql = 'SELECT `Grund__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['grund'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Grund__c", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Grund__c` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }

    $_REQUEST['klaerung'] = 'geschlossen / positiv';

    if ($_REQUEST['klaerung'] != '<leer>') {

      $strField = 'Status_Klaerung__c';

      $strSql = 'SELECT `' .$strField .'` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['klaerung'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {

        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "' .$strField .'", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `' .$strField .'` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);

        if (($_REQUEST['klaerung'] != 'in Klärung') && ($_REQUEST['klaerung'] != '')) { // Wennn geschlossen p/n, dann Nächster Schritt löschen.
          $_REQUEST['meile'] = '';
        }        

      }

    } //
    
    if ($_REQUEST['meile'] != '<leer>') {
      $strSql = 'SELECT `Naechster_Meilenstein__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['meile'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Naechster_Meilenstein__c", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Naechster_Meilenstein__c` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }
    
    if ($_REQUEST['ap'] != '') {
      $strSql = 'SELECT `AP_Klaerung` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['ap'];
      if ($_REQUEST['ap'] == '<delete>') {
        $strNew = '';
      }
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "AP_Klaerung", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `AP_Klaerung` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }

    if ($_REQUEST['tel'] != '') {
      $strSql = 'SELECT `Tel_Klaerung` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['tel'];
      if ($_REQUEST['tel'] == '<delete>') {
        $strNew = '';
      }
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Tel_Klaerung", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Tel_Klaerung` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }

    if ($_REQUEST['info'] != '') { //
      
      $strSql = 'SELECT `Info__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
      
      $strNew = $_REQUEST['info'];
      if ($_REQUEST['info'] == '[LÖSCHEN]') {
        $strNew = '';
      }
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Info__c", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Info__c` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
      }
      
    }

    if (true) { // Event Status

      $strField = 'SF42_EventStatus__c';

      $strSql = 'SELECT `' .$strField .'` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);

      $strNew = 'zurückgestellt von IZS';
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {

        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "' .$strField .'", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `' .$strField .'` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);

      }

    } //

    if (true) { //Phase

      $strField = 'RecordTypeId';

      $strSql = 'SELECT `' .$strField .'` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = '01230000001Ao73AAC'; //Delivery
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {

        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "' .$strField .'", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `' .$strField .'` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);

      }

    }   

    /*
Dokument auf Portal sichtbar = nein
    */

    $_REQUEST['online'] = 'false';

    if ($_REQUEST['online'] != '<leer>') {

      $strField = 'SF42_OnlineStatus__c';

      $strSql = 'SELECT `' .$strField .'` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['online'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {

        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "' .$strField .'", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `' .$strField .'` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);

      }

    } //

    $_REQUEST['publishing'] = 'private';

    if ($_REQUEST['publishing'] != '') {
      $strSql = 'SELECT `SF42_PublishingStatus__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['publishing'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "SF42_PublishingStatus__c", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `SF42_PublishingStatus__c` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }

    if ($_REQUEST['rueck'] != '') { //

      $strField = 'Rueckmeldung_am__c';

      $strSql = 'SELECT `' .$strField .'` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      if ($_REQUEST['rueck'] == '') {
        $_REQUEST['rueck'] = '0000-00-00';
        $strNew = $_REQUEST['rueck'];
      } else {
        $arrDate = explode('.', $_REQUEST['rueck']);
        $strNew = $arrDate[2] .'-' .$arrDate[1] .'-' .$arrDate[0];
      }
      
      if ($result[0]['field'] == '') {
        $result[0]['field'] = '0000-00-00';
      }
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {

        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "' .$strField .'", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `' .$strField .'` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);

      }

    } //

  }

  
  echo 1;
  
}

if (isset($_REQUEST['type']) && ($_REQUEST['type'] == 'messeditKG')) {
  
  $strSql = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
  $strSql.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc(print_r($_REQUEST, true)) .'", "")';
  //$result = MySQLStatic::Insert($strSql);
  
  $arrEventList = explode('|', $_REQUEST['id']);

  //print_r($arrEventList); die();

  foreach ($arrEventList as $intEventId) {

    $_REQUEST['date'] = '';
    $_REQUEST['status'] = 'zurückgestellt durch IZS';
    $_REQUEST['grund'] = 'Konto geschlossen';
    $_REQUEST['meile'] = '';
    $_REQUEST['ap'] = '';
    $_REQUEST['tel'] = '';

    $_REQUEST['info'] = '';

    if (true) { // Wiedervorlage Datum
      $strSql = 'SELECT `bis_am__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
      
      $strNew = '';
      if ($_REQUEST['date'] == '') {
        $_REQUEST['date'] = '0000-00-00';
        $strNew = $_REQUEST['date'];
      } else {
        $arrDate = explode('.', $_REQUEST['date']);
        $strNew = $arrDate[2] .'-' .$arrDate[1] .'-' .$arrDate[0];
      }
      
      if ($result[0]['field'] == '') {
        $result[0]['field'] = '0000-00-00';
      }
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "bis_am__c", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `bis_am__c` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }

    if ($_REQUEST['grund'] != '<leer>') {
      $strSql = 'SELECT `Grund__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['grund'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Grund__c", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Grund__c` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }

    $_REQUEST['klaerung'] = 'geschlossen / positiv';

    if ($_REQUEST['klaerung'] != '<leer>') {

      $strField = 'Status_Klaerung__c';

      $strSql = 'SELECT `' .$strField .'` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['klaerung'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {

        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "' .$strField .'", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `' .$strField .'` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);

        if (($_REQUEST['klaerung'] != 'in Klärung') && ($_REQUEST['klaerung'] != '')) { // Wennn geschlossen p/n, dann Nächster Schritt löschen.
          $_REQUEST['meile'] = '';
        }

      }

    } //

    if ($_REQUEST['meile'] != '<leer>') {
      $strSql = 'SELECT `Naechster_Meilenstein__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['meile'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Naechster_Meilenstein__c", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Naechster_Meilenstein__c` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }
    
    if ($_REQUEST['ap'] != '') {
      $strSql = 'SELECT `AP_Klaerung` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['ap'];
      if ($_REQUEST['ap'] == '<delete>') {
        $strNew = '';
      }
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "AP_Klaerung", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `AP_Klaerung` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }

    if ($_REQUEST['tel'] != '') {
      $strSql = 'SELECT `Tel_Klaerung` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['tel'];
      if ($_REQUEST['tel'] == '<delete>') {
        $strNew = '';
      }
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Tel_Klaerung", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Tel_Klaerung` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }

    if ($_REQUEST['info'] != '') { //
      
      $strSql = 'SELECT `Info__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
      
      $strNew = $_REQUEST['info'];
      if ($_REQUEST['info'] == '[LÖSCHEN]') {
        $strNew = '';
      }
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Info__c", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Info__c` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
      }
      
    }

    if (true) { // Event Status

      $strField = 'SF42_EventStatus__c';

      $strSql = 'SELECT `' .$strField .'` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);

      $strNew = 'zurückgestellt von IZS';
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {

        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "' .$strField .'", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `' .$strField .'` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);

      }

    } //

    if (true) { //Phase

      $strField = 'RecordTypeId';

      $strSql = 'SELECT `' .$strField .'` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = '01230000001Ao73AAC'; //Delivery
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {

        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "' .$strField .'", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `' .$strField .'` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);

      }

    }   

    /*
Dokument auf Portal sichtbar = nein
    */

    $_REQUEST['online'] = 'false';

    if ($_REQUEST['online'] != '<leer>') {

      $strField = 'SF42_OnlineStatus__c';

      $strSql = 'SELECT `' .$strField .'` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['online'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {

        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "' .$strField .'", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `' .$strField .'` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);

      }

    } //

    $_REQUEST['publishing'] = 'private';

    if ($_REQUEST['publishing'] != '') {
      $strSql = 'SELECT `SF42_PublishingStatus__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['publishing'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "SF42_PublishingStatus__c", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `SF42_PublishingStatus__c` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditK", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }

    if ($_REQUEST['rueck'] != '') { //

      $strField = 'Rueckmeldung_am__c';
  
      $strSql = 'SELECT `' .$strField .'` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      if ($_REQUEST['rueck'] == '') {
        $_REQUEST['rueck'] = '0000-00-00';
        $strNew = $_REQUEST['rueck'];
      } else {
        $arrDate = explode('.', $_REQUEST['rueck']);
        $strNew = $arrDate[2] .'-' .$arrDate[1] .'-' .$arrDate[0];
      }
      
      if ($result[0]['field'] == '') {
        $result[0]['field'] = '0000-00-00';
      }
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
  
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "' .$strField .'", "' .$strOld .'", "' .$strNew .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `' .$strField .'` = "' .$strNew .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
      }
  
    } //
  
  }

  echo 1;
  
}

if ($_REQUEST['ac'] == 'hsave') {
  $strSql = 'UPDATE `cms_text` SET `ct_text` = "' .$_REQUEST['ct_text'] .'" WHERE `ct_id` = "' .$_REQUEST['id'] .'"';
  echo $result = MySQLStatic::Update($strSql);
}

if (isset($_REQUEST['type']) && ($_REQUEST['type'] == 'chcat')) {
  $strSql = 'UPDATE `cms_text` SET `ct_category` = "' .$_REQUEST['val'] .'" WHERE `ct_id` = "' .$_REQUEST['id'] .'"';
  echo $result = MySQLStatic::Update($strSql);
}

if (isset($_REQUEST['type']) && ($_REQUEST['type'] == 'checkcat')) {
  $strSql = 'SELECT `ct_category` FROM `cms_text` WHERE `ct_id` = "' .$_REQUEST['id'] .'"';
  $result = MySQLStatic::Query($strSql);
  echo $result[0]['ct_category'];
}

if (isset($_REQUEST['type']) && ($_REQUEST['type'] == 'getmail')) {

  $strSql = 'SELECT * FROM `cms_text` WHERE ct_id = "' .$_REQUEST['ctId'] .'"';
  $arrText = MySQLStatic::Query($strSql);
  
  $arrPeriod = explode('_', $_REQUEST['strSelDate']);
  
  $strSalut = '';
  if ($arrAcc[0]['Anrede_Anschreiben__c'] != '') {
    $strSalut.= $arrAcc[0]['Anrede_Anschreiben__c'];
  } else {
    $strSalut.= 'Sehr geehrte Damen und Herren';
  }
  
  if (strstr($strSalut, 'geehrte Frau') || strstr($strSalut, 'geehrter Herr')) {
    $strSalut.= ' ' .$arrAcc[0]['Nachname__c'];
  }
  
  $strText = str_replace("\n", '', $arrText[0]['ct_text']);
  $strText = str_replace('{Salutation}', $strSalut, $strText);
  $strText = str_replace('{Today}', date('d.m.Y'), $strText);
  $strText = str_replace('{Month}', str_pad($arrPeriod[0], 2, "0", STR_PAD_LEFT), $strText);
  $strText = str_replace('{Year}', $arrPeriod[1], $strText);
  
  $strNameFiles = 'id=' .$arrAcc[0]['Id'] .'&y=' .$arrPeriod[1] .'&m=' .$arrPeriod[0];
  
  $message_html = $strText;
  $message_html = str_replace('{DOWNLOAD_VOLLMACHTEN}', '<a href="http://www.izs-institut.de/cms/_download.php?t=a&' .$strNameFiles .'&evid=' .$_REQUEST['evid'] .'">Vollmachten_' .$arrAcc[0]['Name'] .'.pdf</a>', $message_html);
  $message_html = str_replace('{DOWNLOAD_ANFRAGELISTE}', '<a href="http://www.izs-institut.de/cms/_download.php?t=r&' .$strNameFiles .'&evid=' .$_REQUEST['evid'] .'">Anfrageliste_' .$arrAcc[0]['Name'] .'.pdf</a>', $message_html);

  echo $message_html;
}

if (isset($_REQUEST['type']) && ($_REQUEST['type'] == 'klaeedit')) {
  
  $arrEventList = explode('|', $_REQUEST['id']);
  
  foreach ($arrEventList as $intEventId) {
    
    if ($_REQUEST['date'] != '') { //<leer>
      $strSql = 'SELECT `bis_am__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
      
      if ($_REQUEST['date'] == '') {
        $_REQUEST['date'] = '0000-00-00';
        $strNew = $_REQUEST['date'];
      } else {
        $arrDate = explode('.', $_REQUEST['date']);
        $strNew = $arrDate[2] .'-' .$arrDate[1] .'-' .$arrDate[0];
      }
      
      if ($result[0]['field'] == '') {
        $result[0]['field'] = '0000-00-00';
      }
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "bis_am__c", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `bis_am__c` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
      }
    }
    
    if ($_REQUEST['artr'] != '<leer>') {
      $strSql = 'SELECT `Grund__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['artr'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Grund__c", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Grund__c` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
      }
    }
    
    if ($_REQUEST['arts'] != '<leer>') {
      $strSql = 'SELECT `Status_Klaerung__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['arts'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Status_Klaerung__c", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Status_Klaerung__c` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);

        if (($_REQUEST['arts'] != 'in Klärung') && ($_REQUEST['arts'] != '')) { // Wennn geschlossen p/n, dann Nächster Schritt löschen.
          $_REQUEST['artd'] = '';
        }

      }
    }
    
    if ($_REQUEST['artd'] != '<leer>') {
      $strSql = 'SELECT `Naechster_Meilenstein__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['artd'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Naechster_Meilenstein__c", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Naechster_Meilenstein__c` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
      }
    }

    if ($_REQUEST['ap'] != '') { //Tel_Klaerung
      $strSql = 'SELECT `AP_Klaerung` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['ap'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "AP_Klaerung", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `AP_Klaerung` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
      }
    }

    if ($_REQUEST['tel'] != '') { //
      $strSql = 'SELECT `Tel_Klaerung` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['tel'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Tel_Klaerung", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Tel_Klaerung` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
      }
    }

    if ($_REQUEST['info'] != '') { //
      
      /*
      $strSql = 'INSERT INTO `izs_event_change` ';
      $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
      $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "", "", "' .str_replace('"', '\\"', $_REQUEST['note']) .'", NOW(), 3)';
      $result = MySQLStatic::Insert($strSql);
      */
      
      $strSql = 'SELECT `Info__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['info'];
      if ($_REQUEST['info'] == '[LÖSCHEN]') {
        $strNew = '';
      }
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Info__c", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Info__c` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
      }
      
    }


    
  }
  
}

if (isset($_REQUEST['type']) && ($_REQUEST['type'] == 'messedit')) {
  
  $strSql = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
  $strSql.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messedit", "' .MySQLStatic::esc(print_r($_REQUEST, true)) .'", "")';
  //$result = MySQLStatic::Insert($strSql);
  
  $arrEventList = explode('|', $_REQUEST['id']);
  
  foreach ($arrEventList as $intEventId) {
    
    if ($_REQUEST['date'] != '') { //<leer>
      $strSql = 'SELECT `Rueckmeldung_am__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
      
      if ($_REQUEST['date'] == '') {
        $_REQUEST['date'] = '0000-00-00';
        $strNew = $_REQUEST['date'];
      } else {
        $arrDate = explode('.', $_REQUEST['date']);
        $strNew = $arrDate[2] .'-' .$arrDate[1] .'-' .$arrDate[0];
      }
      
      if ($result[0]['field'] == '') {
        $result[0]['field'] = '0000-00-00';
      }
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Rueckmeldung_am__c", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
   
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messedit", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
       
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Rueckmeldung_am__c` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messedit", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }
    
    if ($_REQUEST['artr'] != '<leer>') {
      $strSql = 'SELECT `Art_der_Rueckmeldung__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['artr'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Art_der_Rueckmeldung__c", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messedit", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Art_der_Rueckmeldung__c` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messedit", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }
    
    if ($_REQUEST['artd'] != '<leer>') {
      $strSql = 'SELECT `Art_des_Dokuments__c` AS `field`, `Name`, `SF42_EventStatus__c`, `RecordTypeId` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['artd'];
      $strOld = $result[0]['field'];
      $strOldSatus  = $result[0]['SF42_EventStatus__c'];
      $strOldRecord = $result[0]['RecordTypeId'];
      
      if ($strNew != $strOld) {

        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Art_des_Dokuments__c", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Art_des_Dokuments__c` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
      }
    }
   
  }
  
}

if (isset($_REQUEST['type']) && ($_REQUEST['type'] == 'messeditOk')) {

  //print_r($_REQUEST); die();
  
  $strSql = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
  $strSql.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messeditOk", "' .MySQLStatic::esc(print_r($_REQUEST, true)) .'", "")';
  //$result = MySQLStatic::Insert($strSql);
  
  $arrEventList = explode('|', $_REQUEST['id']);
  
  foreach ($arrEventList as $intEventId) {

    //process only enquired
    $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
    $result = MySQLStatic::Query($strSql);
    if (count($result) > 0) {
      if ($result[0]['SF42_EventStatus__c'] != 'enquired') continue;
    }

    if ($_REQUEST['date'] != '') { //<leer>
      $strSql = 'SELECT `Rueckmeldung_am__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
      
      if ($_REQUEST['date'] == '') {
        $_REQUEST['date'] = '0000-00-00';
        $strNew = $_REQUEST['date'];
      } else {
        $arrDate = explode('.', $_REQUEST['date']);
        $strNew = $arrDate[2] .'-' .$arrDate[1] .'-' .$arrDate[0];
      }
      
      if ($result[0]['field'] == '') {
        $result[0]['field'] = '0000-00-00';
      }
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Rueckmeldung_am__c", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
   
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messedit", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
       
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Rueckmeldung_am__c` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messedit", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }
    
    if ($_REQUEST['artr'] != '<leer>') {
      $strSql = 'SELECT `Art_der_Rueckmeldung__c` AS `field`, `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['artr'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {
        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Art_der_Rueckmeldung__c", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messedit", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Art_der_Rueckmeldung__c` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
        $strSql2 = 'INSERT INTO `cms_log` (`co_id`, `co_cl_id`, `co_time`, `co_item`, `co_parameter`, `co_result`) ';
        $strSql2.= 'VALUES (NULL, "' .$_SESSION['id'] .'", NOW(), "messedit", "' .MySQLStatic::esc($strSql) .'", "' .$result .'")';
        //$result = MySQLStatic::Insert($strSql2);
      }
    }
    
    if ($_REQUEST['artd'] != '<leer>') {
      $strSql = 'SELECT `Art_des_Dokuments__c` AS `field`, `Name`, `SF42_EventStatus__c`, `RecordTypeId` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['artd'];
      $strOld = $result[0]['field'];
      $strOldSatus  = $result[0]['SF42_EventStatus__c'];
      $strOldRecord = $result[0]['RecordTypeId'];
      
      if ($strNew != $strOld) {

        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Art_des_Dokuments__c", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Art_des_Dokuments__c` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
      }
    }

    if ($_REQUEST['ergebnis'] != '<leer>') {
      $strSql = 'SELECT `Art_des_Dokuments__c`, `Name`, `SF42_EventStatus__c` AS `field`, `RecordTypeId` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['ergebnis'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {

        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "SF42_EventStatus__c", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `SF42_EventStatus__c` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
      }
    }

    if ($_REQUEST['stundung'] != '<leer>') {
      $strSql = 'SELECT `Art_des_Dokuments__c`, `Name`, `SF42_EventStatus__c`, `Stundung__c` AS `field`, `RecordTypeId` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$intEventId .'"';
      $result = MySQLStatic::Query($strSql);
  
      $strNew = $_REQUEST['stundung'];
      $strOld = $result[0]['field'];
      
      if ($strNew != $strOld) {

        $strSql = 'INSERT INTO `izs_event_change` ';
        $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
        $strSql.= 'NULL, "' .$intEventId .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "Stundung__c", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
        $result = MySQLStatic::Insert($strSql);
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Stundung__c` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$intEventId .'"';
        $result = MySQLStatic::Update($strSql);
  
      }
    }
   
  }
  
}

if (isset($_REQUEST['type']) && ($_REQUEST['type'] == 'savefield')) {



  $strSql = 'SELECT `' .$_REQUEST['field'] .'` AS `field`, `Name`, `SF42_EventStatus__c`, `RecordTypeId` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$_REQUEST['id'] .'"';
  $result = MySQLStatic::Query($strSql);

  if (($_REQUEST['field'] == 'Rueckmeldung_am__c') || ($_REQUEST['field'] == 'bis_am__c')) {
    if ($_REQUEST['val'] == '') {
      $_REQUEST['val'] = '0000-00-00';
    } else {
      $arrDate = explode('.', $_REQUEST['val']);
      $_REQUEST['val'] = $arrDate[2] .'-' .$arrDate[1] .'-' .$arrDate[0];
    }
  }
  
  $strNew = $_REQUEST['val'];
  $strOld = $result[0]['field'];
  $strOldSatus  = $result[0]['SF42_EventStatus__c'];
  $strOldRecord = $result[0]['RecordTypeId'];

  if ($strNew != $strOld) {

    $strSql = 'INSERT INTO `izs_event_change` ';
    $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
    $strSql.= 'NULL, "' .$_REQUEST['id'] .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "' .$_REQUEST['field'] .'", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
    $result = MySQLStatic::Insert($strSql);
    
    $strSql = 'UPDATE `SF42_IZSEvent__c` SET `' .$_REQUEST['field'] .'` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$_REQUEST['id'] .'"';
    $result = MySQLStatic::Update($strSql);

  }

}


if (isset($_REQUEST['type']) && ($_REQUEST['type'] == 'sfield')) {

  if (strstr($_REQUEST['id'], '-') !== false) {

    $arrId = explode('-', $_REQUEST['id']);

    $strSql = 'SELECT * FROM `cms_back` WHERE `cb_account_id` = "' .$arrId[2] .'" AND `cb_month` = "' .$arrId[1] .'" AND `cb_year` = "' .$arrId[0] .'" AND `cb_evid` = "' .$arrId[3] .'"';
    $arrSql = MySQLStatic::Query($strSql);
    
    if (count($arrSql) > 0) {
      echo $_REQUEST['id'] = $arrSql[0]['cb_id'];
    }

  }  
  
  if (strstr($_REQUEST['id'], '-') === false) {
    if ($_REQUEST['val'] != '') {
      $arrDate = explode('.', $_REQUEST['val']);
      $strDate = $arrDate[2] .'-' .$arrDate[1] .'-' .$arrDate[0];
    } else {
      $strDate = $_REQUEST['val'];
    }
    $strSql = 'UPDATE `cms_back` SET `cb_date` = "' .$strDate .'" WHERE `cb_id` = "' .$_REQUEST['id'] .'"';
    $result = MySQLStatic::Query($strSql);
    
  } else {
    
    $arrId = explode('-', $_REQUEST['id']);

    $arrDate = explode('.', $_REQUEST['val']);
    $strDate = $arrDate[2] .'-' .$arrDate[1] .'-' .$arrDate[0];

    $strSql = 'INSERT INTO `cms_back` (`cb_id`, `cb_account_id`, `cb_month`, `cb_year`, `cb_created`, `cb_date`, `cb_evid`) ';
    $strSql.= 'VALUES (NULL, "' .$arrId[2] .'", "' .$arrId[1] .'", "' .$arrId[0] .'", NOW(), "' .$strDate .'", "' .$arrId[3] .'")';
    $arrSql = MySQLStatic::Insert($strSql);    
    
  }
  
  
  /*
  
  if (($_REQUEST['field'] == 'Rueckmeldung_am__c') || ($_REQUEST['field'] == 'bis_am__c')) {
    if ($_REQUEST['val'] == '') {
      $_REQUEST['val'] = '0000-00-00';
    } else {
      $arrDate = explode('.', $_REQUEST['val']);
      $_REQUEST['val'] = $arrDate[2] .'-' .$arrDate[1] .'-' .$arrDate[0];
    }
  }
  
  $strNew = $_REQUEST['val'];
  $strOld = $result[0]['field'];
  
  if ($strNew != $strOld) {
    $strSql = 'INSERT INTO `izs_event_change` ';
    $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
    $strSql.= 'NULL, "' .$_REQUEST['id'] .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "' .$_REQUEST['field'] .'", "' .str_replace('"', '\\"', $strOld) .'", "' .str_replace('"', '\\"', $strNew) .'", NOW(), 2)';
    $result = MySQLStatic::Insert($strSql);
    
    $strSql = 'UPDATE `SF42_IZSEvent__c` SET `' .$_REQUEST['field'] .'` = "' .str_replace('"', '\\"', $strNew) .'" WHERE `evid` = "' .$_REQUEST['id'] .'"';
    $result = MySQLStatic::Update($strSql);
  }
  
  */

}


if (isset($_REQUEST['type']) && ($_REQUEST['type'] == 'getname')) {
  $strSql = 'SELECT `ef_id` FROM `izs_event_file` WHERE `ef_name` = "' .$_REQUEST['name'] .'" ORDER BY `ef_id` DESC';
  $result = MySQLStatic::Query($strSql);
  echo str_pad($result[0]['ef_id'], 11, '0', STR_PAD_LEFT);
}

if (isset($_REQUEST['type']) && ($_REQUEST['type'] == 'event')) {
  
  $strSql = 'INSERT INTO `izs_event_change` ';
  $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
  $strSql.= 'NULL, "' .$_REQUEST['id'] .'", ' .$_SESSION['id'] .', "' .$_REQUEST['name'] .'", "", "", "' .str_replace('"', '\\"', $_REQUEST['message']) .'", NOW(), 3)';
  $result = MySQLStatic::Insert($strSql);
  
  if ($_REQUEST['fnames'] != '') {
    
    $arrFnames = explode('|', $_REQUEST['fnames']);
    unset($arrFnames[0]);
    
    foreach ($arrFnames as $intKey => $strFileName) {
      
      $strUploadedFile = 'uploads/' .$strFileName;
      
      $arrFileParts = explode('.', $strFileName);
      $strExtension = strtolower($arrFileParts[count($arrFileParts) - 1]);
      $intSize = filesize($strUploadedFile);
    
      $strSql2 = 'INSERT INTO `izs_event_file` 
       (`ef_id`, `ec_id`, `ef_name`, `ef_type`, `ef_size`, `ef_date`) VALUES (
       NULL, "' .$result .'", "' .$strFileName .'", "' .$strExtension .'", "' .$intSize .'", NOW())';
      $result2 = MySQLStatic::Insert($strSql2);
      
      $strNewName = str_pad($result2, 11, '0', STR_PAD_LEFT);
      
      $strVz = dirname(__FILE__) .'/';
      
      echo $strVz .$strUploadedFile .', ' .$strVz .'uploads_events/' .$strNewName .'.' .$strExtension;
      rename($strVz .$strUploadedFile, $strVz .'uploads_events/' .$strNewName .'.' .$strExtension);

    }

  }
  
  //if (mysql_error() == '') {
    echo '1';
  //}
}

if (isset($_REQUEST['type']) && ($_REQUEST['type'] == 'mail')) {

  if (is_array($_REQUEST['to'])) {
    $_REQUEST['to'] = implode(',', $_REQUEST['to']);
  }
  
  if (is_array($_REQUEST['cc'])) {
    $_REQUEST['cc'] = implode(',', $_REQUEST['cc']);
  }

  if ($_REQUEST['cc'] == 'null') {
    $_REQUEST['cc'] = '';
  }

  $arrMessage = array(
    'to' => $_REQUEST['to'], 
    'cc' => $_REQUEST['cc'], 
    'bcc' => $_REQUEST['bcc'],
    'subject' => $_REQUEST['subject'],
    'message' => $_REQUEST['message']
  );

  $strSql = 'INSERT INTO `izs_event_change` ';
  $strSql.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (';
  $strSql.= 'NULL, "' .$_REQUEST['id'] .'", ' .$_SESSION['id'] .', "' .$_REQUEST['name'] .'", "", "", \'' .serialize($arrMessage) .'\', NOW(), 5)';
  $result = MySQLStatic::Insert($strSql);

  if ($_REQUEST['fnames'] != '') {
    
    $arrFnames = explode('|', $_REQUEST['fnames']);
    unset($arrFnames[0]);
    
    $arrAttachments = array();

    foreach ($arrFnames as $intKey => $strFileName) {
      
      $strUploadedFile = 'uploads/' .$strFileName;
      
      $arrFileParts = explode('.', $strFileName);
      $strExtension = strtolower($arrFileParts[count($arrFileParts) - 1]);
      $intSize = filesize($strUploadedFile);
    
      $strSql2 = 'INSERT INTO `izs_event_file` 
       (`ef_id`, `ec_id`, `ef_name`, `ef_type`, `ef_size`, `ef_date`) VALUES (
       NULL, "' .$result .'", "' .$strFileName .'", "' .$strExtension .'", "' .$intSize .'", NOW())';
      $result2 = MySQLStatic::Insert($strSql2);
      
      $strNewName = str_pad($result2, 11, '0', STR_PAD_LEFT);
      $strNewFile = 'uploads_events/' .$strNewName .'.' .$strExtension;
      
      $strVz = dirname(__FILE__) .'/';
      rename($strVz .$strUploadedFile, $strVz .$strNewFile);
      
      $arrAttachments[] = array (
        0 => $strVz .$strNewFile,
        1 => $strNewName .'.' .$strExtension
      );

    }

  }

  /*
  $strVz = dirname(__FILE__) .'/';
  if (count($arrAttachments) > 0) {
    array_unshift($arrAttachments, $strVz .'pdf/Anschreiben_Umstellung_Fax.pdf');
  } else {
    $arrAttachments[] = array (
      0 => $strVz .'pdf/Anschreiben_Umstellung_Fax.pdf'
    );
  }  

  //$files[] = array('pdf/Anschreiben_Umstellung_Fax.pdf', 'Anschreiben_Umstellung_Fax.pdf');
  */
  
  $message_html = $arrMessage['message'];    
  multi_attach_mail($arrMessage['to'], $arrMessage['cc'], $arrMessage['bcc'], $arrAttachments, $arrMessage['subject'], $arrMessage['message'], $message_html);
  
  //if (mysql_error() == '') {
    echo '1';
  //}
}

if (isset($_REQUEST['type']) && ($_REQUEST['type'] == 'text')) {
  $arrId = explode('in_text_', $_REQUEST['id']);
  $strSql = 'UPDATE `cms_text` SET `ct_text` = "' .MySQLStatic::esc($_REQUEST['content']) .'" WHERE `ct_id` = ' .(int) $arrId[1];
  $intResult = MySQLStatic::Update($strSql);
  if ($intResult > 0) {
    echo '1';
  }
}

if (isset($_REQUEST['type']) && ($_REQUEST['type'] == 'head')) {
  $arrId = explode('in_head_', $_REQUEST['id']);
  $strSql = 'UPDATE `cms_text` SET `ct_head` = "' .MySQLStatic::esc($_REQUEST['content']) .'" WHERE `ct_id` = ' .(int) $arrId[1];
  $intResult = MySQLStatic::Update($strSql);
  if ($intResult > 0) {
    echo '1';
  }
}

if (isset($_REQUEST['type']) && ($_REQUEST['type'] == 'name')) {
  $arrId = explode('in_name_', $_REQUEST['id']);
  $strSql = 'UPDATE `cms_text` SET `ct_name` = "' .MySQLStatic::esc($_REQUEST['content']) .'" WHERE `ct_id` = ' .(int) $arrId[1];
  $intResult = MySQLStatic::Update($strSql);
  if ($intResult > 0) {
    echo '1';
  }
}


if (isset($_REQUEST['fdo'])) {

  if ($_REQUEST['fdo'] == 'copy') {
    $strSql_s = 'SELECT * FROM `cms_text` WHERE `ct_id` = "' .(int) $_REQUEST['ctId'] .'"';
    $arrRow_s = MySQLStatic::Query($strSql_s);
    $arrRow_s = $arrRow_s[0];
    
    //if (mysql_error()) echo mysql_error() .' SQL: ' .$resSql_s;
    
    $arrRow_s['ct_id'] = 'NULL';
    $arrRow_s['ct_name'].= ' (Kopie)';
    
    $arrRow_s = str_replace('"', '\\"', $arrRow_s);

    $strSql = 'INSERT INTO `cms_text` (`' .implode('`, `', array_keys($arrRow_s)) .'`) VALUES ("' .implode('", "', array_values($arrRow_s)) .'")';
    $intCt_id = MySQLStatic::Insert($strSql);
    //if (mysql_error()) echo mysql_error() .' SQL: ' .$strSql;
    
    echo $intCt_id;
  }
  
  if ($_REQUEST['fdo'] == 'delete') {
    $strSql = 'DELETE FROM `cms_text` WHERE `ct_id` = "' .(int) $_REQUEST['ctId'] .'"';
    $arrRow = MySQLStatic::Query($strSql);
    echo 1;
  }

  if ($_REQUEST['fdo'] == 'add') {
    if ($_REQUEST['type'] != '') {
      $strSql = 'INSERT INTO `cms_text` (`ct_id`, `ct_name`, `ct_type`) VALUES (NULL, "Neue Vorlage", "' .$_REQUEST['type'] .'")';
    } else {
      $strSql = 'INSERT INTO `cms_text` (`ct_id`, `ct_name`) VALUES (NULL, "Neue Vorlage")';
    }
    $intCt_id = MySQLStatic::Insert($strSql);
    //if (mysql_error()) echo mysql_error() .' SQL: ' .$strSql;
    echo $intCt_id;
  }

}

if (isset($_REQUEST['upload'])) {
  $strSql = 'SELECT `SF42_DocumentUrl__c` FROM `SF42_IZSEvent__c` WHERE `Id` = "' .str_replace('e_', '' ,$_REQUEST['upload']) .'"';
  $resSql = MySQLStatic::Query($strSql);
  
  if ($resSql[0]['SF42_DocumentUrl__c'] != '') {
    echo $resSql[0]['SF42_DocumentUrl__c'];
  } else {
    echo 0;
  }
  
}

if (isset($_REQUEST['geturl']) || isset($_REQUEST['geturls'])) {
  $strSql = 'SELECT `SF42_DocumentUrl__c` FROM `SF42_IZSEvent__c` WHERE `Id` = "' .str_replace('e_', '' ,$_REQUEST['geturl']) .'"';
  $resSql = MySQLStatic::Query($strSql);
  
  if ($resSql[0]['SF42_DocumentUrl__c'] != '') {
    echo $resSql[0]['SF42_DocumentUrl__c'];
  } else {
    echo 'none';
  }
  
}

/*
if (isset($_REQUEST['geturls'])) {
  $strSql = 'SELECT SF42_DocumentUrl__c FROM SF42_IZSEvent__c WHERE Id = \'' .str_replace('e_', '' ,$_REQUEST['geturls']) .'\'';
  $arrResult = SForceStatic::Query($strSql);
  
  if ($arrResult[0]['SF42_DocumentUrl__c'] != '') {
    echo $arrResult[0]['SF42_DocumentUrl__c'];
  } else {
    echo 'none';
  }
  
}
*/


?>