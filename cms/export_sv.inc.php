<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

include('conf/global.inc.php');
require_once('../assets/classes/class.mysql.php');
require_once('../assets/classes/class.database.php');

require_once('functions.inc.php');
require_once('bootstrap.inc.php');

require_once('classes/class.informationprovider.php');

$boolShowIp = false;
$strOutput = '';

$arrRecordType = array (
  'Delivery' => '01230000001Ao73AAC',
  'FollowUp' => '01230000001Ao74AAC',
  'Publication' => '01230000001Ao75AAC',
  'Review' => '01230000001Ao76AAC'
);

$arrRecordTypeFlip = array_flip($arrRecordType);

$strOutput.= '<h1>Export SV-Events</h1>' .chr(10);

if (true) {

// SUCHE BEGIN 

$strSql0 = "SELECT `Month` FROM `izs_month_sv` ORDER BY `SF42_Year__c`  DESC, `SF42_Month__c`  DESC";
$arrResult0 = MySQLStatic::Query($strSql0);

if (count($arrResult0) > 0) {
  
  $strOutput.= '<form method="post" action="">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);
  $strOutput.= '<input type="hidden" name="ac" value="' .$_REQUEST['ac'] .'">' .chr(10);
  
  $strOutput.= '<table>' .chr(10);
  $strOutput.= '<tbody>' .chr(10);

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Zeitraum: </td>' .chr(10);
  $strOutput.= '    <td><table><tr><td><select name="strSelDateFrom" id="strSelDateFrom">' .chr(10);
  
  $intPeriod = 0;

  foreach ($arrResult0 as $arrPeriod0) {
    $strSelected = '';
    $strValue = $arrPeriod0['Month'];

    if (empty($_REQUEST['strSelDateFrom']) && ($intPeriod == 0)) {
      $_REQUEST['strSelDateFrom'] = $strValue;
      //$_REQUEST['send'] = 1;
    }
    
    if ($strValue == $_REQUEST['strSelDateFrom']) {
      $strSelected = ' selected="selected"';
    } 
    $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .$arrPeriod0['Month'] .'</option>' .chr(10);
    $intPeriod++;
  }
  
  $strOutput.= '    </select></td>' .chr(10);

  $strOutput.= '    <td style="padding: 5px 10px;"> bis: </td>' .chr(10);


  $strOutput.= '    <td><select name="strSelDateTo" id="strSelDateTo">' .chr(10);
  
  $intPeriod = 0;

  foreach ($arrResult0 as $arrPeriod0) {
    $strSelected = '';
    $strValue = $arrPeriod0['Month'];

    if (empty($_REQUEST['strSelDateTo']) && ($intPeriod == 0)) {
      $_REQUEST['strSelDateTo'] = $strValue;
      //$_REQUEST['send'] = 1;
    }
    
    if ($strValue == $_REQUEST['strSelDateTo']) {
      $strSelected = ' selected="selected"';
    } 
    $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .$arrPeriod0['Month'] .'</option>' .chr(10);
    $intPeriod++;
  }
  
  $strOutput.= '    </select></td></tr></table></td>' .chr(10);

  $strOutput.= '  </tr>' .chr(10);
  
  $strSql6 = 'SELECT DISTINCT `Account`.`Id`, `Account`.`Name` FROM `SF42_IZSEvent__c` INNER JOIN `Account` ON `SF42_IZSEvent__c`.`SF42_informationProvider__c` = `Account`.`Id` ORDER BY `Account`.`Name`';
  
  $strIpSelected = '';
  
  $arrResult6 = MySQLStatic::Query($strSql6);
  if (count($arrResult6) > 0) {

    if ($_REQUEST['strSelInf'] == 'all') {
      $strSelectedAll = ' selected="selected"';
    } else {
      $strSelectedAll = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Krankenkasse: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelInf" id="strSelInf">' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Krankenkassen -</option>' .chr(10);
    
    foreach ($arrResult6 as $arrProvider) {
      $strSelected = '';
      if ($arrProvider['Id'] == $_REQUEST['strSelInf']) {
        $strSelected = ' selected="selected"';
        $strIpSelected = $arrProvider['Name'];
      } 
      $strOutput.= '    <option' .$strOptClass .' value="' .$arrProvider['Id'] .'"' .$strSelected .'>' .$arrProvider['Name'] .'</option>' .chr(10);
      if ($_REQUEST['conly'] != 1) {
        $strOutput.= $strAddDez;
      }
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
  
  $strSql6 = 'SELECT DISTINCT `Group__c`.`Id`, `Group__c`.`Name` FROM `SF42_IZSEvent__c` INNER JOIN `Group__c` ON `group_id` = `Group__c`.`Id` ';
  $strSql6.= 'ORDER BY `Group__c`.`Name`';
  
  $strIpSelected = '';
  
  $arrResult6 = MySQLStatic::Query($strSql6);
  if (count($arrResult6) > 0) {

    if ($_REQUEST['strSelInf'] == 'all') {
      $strSelectedAll = ' selected="selected"';
    } else {
      $strSelectedAll = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Company Group: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelGrp" id="strSelGrp">' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Company Groups -</option>' .chr(10);
    
    foreach ($arrResult6 as $arrGroup) {
      $strSelected = '';
      if ($arrGroup['Id'] == $_REQUEST['strSelGrp']) {
        $strSelected = ' selected="selected"';
        $strIpSelected = $arrGroup['Name'];
      } 
      $strOutput.= '    <option' .$strOptClass .' value="' .$arrGroup['Id'] .'"' .$strSelected .'>' .$arrGroup['Name'] .'</option>' .chr(10);
      if ($_REQUEST['conly'] != 1) {
        $strOutput.= $strAddDez;
      }
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
  


    $strSql6 = 'SELECT DISTINCT `Account`.`Id`, `Account`.`Name` FROM `SF42_IZSEvent__c` INNER JOIN `Account` ON `Betriebsnummer_ZA__c` = `SF42_Comany_ID__c` ';
    $strSql6.= 'ORDER BY `Account`.`Name`';

    $arrResult6 = MySQLStatic::Query($strSql6);
    if (count($arrResult6) > 0) {
  
      if ($_REQUEST['strSelPay'] == 'all') {
        $strSelectedAll = ' selected="selected"';
      } else {
        $strSelectedAll = '';
      }
  
      $strOutput.= '  <tr>' .chr(10);
      $strOutput.= '    <td>Melsdestelle: </td>' .chr(10);
      $strOutput.= '    <td><select name="strSelPay" id="strSelPay">' .chr(10);
      $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Meldestellen -</option>' .chr(10);
      
      foreach ($arrResult6 as $intKey => $arrProvider) {
        $strSelected = '';
        if ($arrProvider['Id'] == $_REQUEST['strSelPay']) {
          $strSelected = ' selected="selected"';
        } 
        
        if (($strLastName == $arrProvider['Name']) || ($arrResult6[$intKey + 1]['Name'] == $arrProvider['Name'])) {
           $strAdd = ' (' .$arrProvider['Btnr'] .')';
        } else {
          $strAdd = '';
        }
        
        $strOutput.= '    <option value="' .$arrProvider['Id'] .'"' .$strSelected .'>' .$arrProvider['Name'] .'' .$strAdd .'</option>' .chr(10);
        $strLastName = $arrProvider['Name'];
        
      }
    
      $strOutput.= '    </select></td>' .chr(10);
      $strOutput.= '  </tr>' .chr(10);
    }
  

    $strSql7 = 'SELECT COUNT(*) AS `Datensätze`, `SF42_EventStatus__c` FROM `SF42_IZSEvent__c` GROUP BY `SF42_EventStatus__c` ORDER BY `SF42_EventStatus__c` ASC';
    
    $arrResult7 = MySQLStatic::Query($strSql7);
    if (count($arrResult7) > 0) {
  
      if ($_REQUEST['strSelBeSt'] == 'all') {
        $strSelectedAll = ' selected="selected"';
      } else {
        $strSelectedAll = '';
      }
  
      $strOutput.= '  <tr>' .chr(10);
      $strOutput.= '    <td>Ergebnis: </td>' .chr(10);
      $strOutput.= '    <td><select name="strSelBeSt" id="strSelBeSt">' .chr(10);
      $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Status -</option>' .chr(10);
      
      foreach ($arrResult7 as $intKey => $arrStatus) {
        $strSelected = '';
        if ($arrStatus['SF42_EventStatus__c'] == $_REQUEST['strSelBeSt']) {
          $strSelected = ' selected="selected"';
        } 
        
        $strOutput.= '    <option value="' .$arrStatus['SF42_EventStatus__c'] .'"' .$strSelected .'>' .$arrStatus['SF42_EventStatus__c'] .'</option>' .chr(10);
        
      }
    
      $strOutput.= '    </select></td>' .chr(10);
      $strOutput.= '  </tr>' .chr(10);
    }

    $strSql8 = 'SELECT COUNT(*) AS `Datensätze`, IF(`Status_Klaerung__c` = "", "kein Status", `Status_Klaerung__c`) AS `be_klaerung_status` FROM `SF42_IZSEvent__c` GROUP BY `be_klaerung_status` ORDER BY `be_klaerung_status`';
    $arrResult8 = MySQLStatic::Query($strSql8);
    if (count($arrResult8) > 0) {
  
      if ($_REQUEST['strSelKlSt'] == 'all') {
        $strSelectedAll = ' selected="selected"';
      } else {
        $strSelectedAll = '';
      }
  
      $strOutput.= '  <tr>' .chr(10);
      $strOutput.= '    <td>Status Klärung: </td>' .chr(10);
      $strOutput.= '    <td><select name="strSelKlSt" id="strSelKlSt">' .chr(10);
      $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Status -</option>' .chr(10);
      
      foreach ($arrResult8 as $intKey => $arrStatus) {
        $strSelected = '';
        if ($arrStatus['be_klaerung_status'] == $_REQUEST['strSelKlSt']) {
          $strSelected = ' selected="selected"';
        } 
        
        $strOutput.= '    <option value="' .$arrStatus['be_klaerung_status'] .'"' .$strSelected .'>' .$arrStatus['be_klaerung_status'] .'</option>' .chr(10);
        
      }
    
      $strOutput.= '    </select></td>' .chr(10);
      $strOutput.= '  </tr>' .chr(10);
    }  

  $strOutput.= '</tbody>' .chr(10);

  $strOutput.= '  <tfoot>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td align="right" colspan="2"><input type="submit" name="Suche" value="Suche" /></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);
  $strOutput.= '  </tfoot>' .chr(10);

  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);
    
}

if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {

  //date_format(str_to_date('February 2015','%M %Y'),'%Y-%m-01 %H:%i:%s') as date
  //UNIX_TIMESTAMP(STR_TO_DATE('Apr 15 2012 12:00AM', '%M %d %Y %h:%i%p')

  $arrMonthFrom = explode('-', $_REQUEST['strSelDateFrom']);
  $arrMonthTo   = explode('-', $_REQUEST['strSelDateTo']);

  $intMonthFrom = strtotime($arrMonthFrom[1] .'-' .$arrMonthFrom[0] .'-' .'01 00:00');
  $intMonthTo = strtotime($arrMonthTo[1] .'-' .$arrMonthTo[0] .'-' .'01 00:00');
  
  $strSql = 'SELECT `SF42_IZSEvent__c`.*, `AccountKK`.`Name` AS `KK`, `AccountPP`.`Name` AS `PP`, `Group__c`.`Name` AS `Group` FROM `SF42_IZSEvent__c` ';

  $strSql.= 'INNER JOIN `Account` AS `AccountKK` ON `SF42_IZSEvent__c`.`SF42_informationProvider__c` = `AccountKK`.`Id` ';
  $strSql.= 'INNER JOIN `Account` AS `AccountPP` ON `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c` = `AccountPP`.`SF42_Comany_ID__c` ';
  $strSql.= 'INNER JOIN `Group__c` ON `SF42_IZSEvent__c`.`group_id` = `Group__c`.`Id` ';
  
  $strSql.= 'WHERE UNIX_TIMESTAMP(STR_TO_DATE(CONCAT(LPAD(`SF42_Month__c`, 2, "0"), "-01-", `SF42_Year__c`), "%m-%d-%Y")) >= "' .MySQLStatic::esc($intMonthFrom) .'" ';
  $strSql.= 'AND UNIX_TIMESTAMP(STR_TO_DATE(CONCAT(LPAD(`SF42_Month__c`, 2, "0"), "-01-", `SF42_Year__c`), "%m-%d-%Y")) <= "' .MySQLStatic::esc($intMonthTo) .'" ';

  //echo $strSql;

  if ((isset($_REQUEST['strSelInf'])) && ($_REQUEST['strSelInf'] != 'all')) {
    $strSql.= 'AND `SF42_IZSEvent__c`.`SF42_informationProvider__c` = "' .MySQLStatic::esc($_REQUEST['strSelInf']) .'" ';
  }

  if ((isset($_REQUEST['strSelPay'])) && ($_REQUEST['strSelPay'] != 'all')) {
    $strSql.= 'AND `AccountPP`.`Id` = "' .MySQLStatic::esc($_REQUEST['strSelPay']) .'" ';
  }
  
  if ((isset($_REQUEST['strSelGrp'])) && ($_REQUEST['strSelGrp'] != 'all')) {
    $strSql.= 'AND `Group__c`.`Id` = "' .MySQLStatic::esc($_REQUEST['strSelGrp']) .'" ';
  }
  
  if ((isset($_REQUEST['strSelBeSt'])) && ($_REQUEST['strSelBeSt'] != 'all')) {
    $strSql.= 'AND `SF42_EventStatus__c` = "' .MySQLStatic::esc($_REQUEST['strSelBeSt']) .'" ';
  }
  
  if ((isset($_REQUEST['strSelKlSt'])) && ($_REQUEST['strSelKlSt'] != 'all')) {

    $strStatus = '';
    if ($_REQUEST['strSelKlSt'] != 'kein Status') {
      $strStatus = $_REQUEST['strSelKlSt'];
    }

    $strSql.= 'AND `Status_Klaerung__c` = "' .MySQLStatic::esc($strStatus) .'" ';

  }

  $strSql.= 'ORDER BY `SF42_IZSEvent__c`.`ID`';

  $arrInformationProvider = MySQLStatic::Query($strSql);
  
  //echo $strSql;
  //print_r($arrInformationProvider); die();
  //print_r($arrDecentral);
  
  $strExportTable = '';
  
  if (count($arrInformationProvider) > 0) {
    //natcasesort($arrInformationProvider);

    $strOutputTable = '';
    $strOutputTable.= '<table class="" id="5">' .chr(10);
    $strOutputTable.= '  <thead>' .chr(10);
    $strOutputTable.= '    <tr>' .chr(10);
  	//$strOutputTable.= '      <th class="{sorter: false}">Status</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header">ID</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="header">Monat</th>' .chr(10);
    $strOutputTable.= '      <th class="header">Jahr</th>' .chr(10);
  	$strOutputTable.= '      <th class="header">Meldestelle</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="header">Unternehmensgruppe</th>' .chr(10);
    $strOutputTable.= '      <th class="header">Krankenkasse</th>' .chr(10);
  	$strOutputTable.= '      <th class="header">Status Bearbeitung</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header">Ergebnis</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="header">Dokument</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="header">Status Klärung</th>' .chr(10); //class="header"

    $strOutputTable.= '    </tr>' .chr(10);
    $strOutputTable.= '  </thead>' .chr(10);
    $strOutputTable.= '  <tbody>' .chr(10);
    
    $strExportTable.= $strOutputTable;
    
    //print_r($arrInformationProvider);

    foreach ($arrInformationProvider as $strId => $arrEvent) {

      $strOutputTable.= '    <tr>' .chr(10);
      $strExportTable.= '    <tr>' .chr(10);

      $strOutputTable.= '      <td>' .$arrEvent['Name'] .'</a></td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['Name'] .'</td>' .chr(10);

      $strOutputTable.= '      <td>' .$arrEvent['SF42_Month__c'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['SF42_Month__c'] .'</td>' .chr(10);

      $strOutputTable.= '      <td>' .$arrEvent['SF42_Year__c'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['SF42_Year__c'] .'</td>' .chr(10);
      
      $strOutputTable.= '      <td>' .$arrEvent['PP'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['PP'] .'</td>' .chr(10);
      
      $strOutputTable.= '      <td>' .$arrEvent['Group'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['Group'] .'</td>' .chr(10);
      
      $strOutputTable.= '      <td>' .$arrEvent['KK'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['KK'] .'</td>' .chr(10);
      
      $strOutputTable.= '      <td>' .$arrRecordTypeFlip[$arrEvent['RecordTypeId']] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrRecordTypeFlip[$arrEvent['RecordTypeId']] .'</td>' .chr(10);
      
      $strOutputTable.= '      <td>' .$arrEvent['SF42_EventStatus__c'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['SF42_EventStatus__c'] .'</td>' .chr(10);

      $strUrl = '';
      if ($arrEvent['SF42_DocumentUrl__c'] != '') {
        $strUrl = '' .$arrEvent['SF42_DocumentUrl__c'];
        $strOutputTable.= '      <td><a href="' .$strUrl .'" target="_blank">Download</a></td>' .chr(10);
      } else {
        $strOutputTable.= '      <td></td>' .chr(10);
      }
      $strExportTable.= '      <td>' .$strUrl .'</td>' .chr(10);
            
      $strOutputTable.= '      <td>' .$arrEvent['Status_Klaerung__c'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['Status_Klaerung__c'] .'</td>' .chr(10);
      
      $strOutputTable.= '    </tr>' .chr(10);
      $strExportTable.= '    </tr>' .chr(10);
      
    }
      
    $strOutputTable.= '  </tbody>' .chr(10);
    $strOutputTable.= '</table>' .chr(10);

    $strExportTable.= '  </tbody>' .chr(10);
    $strExportTable.= '</table>' .chr(10);

if (@$_SERVER['HTTP_FKLD'] == 'on') {
  //echo $intCatchAllContinue;
}  
    
    $strOutput.= '<div class="clearfix" style="height: 48px; width: 1790px;">' .chr(10);
    $strOutput.= '  <div class="form-left">' .chr(10);
    $strOutput.= '    <h3>' .count($arrInformationProvider) .' Ergebnisse gefunden:</h3>' .chr(10);
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' .chr(10);
    $strOutput.= '    <form method="post" action="_get_csv.php">';
    $strOutput.= '    <input type="hidden" name="strTable" value="' .str_replace('"', "'", $strExportTable) .'">' .chr(10);
    $strOutput.= '    <input type="hidden" name="format" value="xlsx-bg">' .chr(10);
    $strOutput.= '    <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' .chr(10);
    $strOutput.= '    </form>';
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '</div>' .chr(10);
    
    $strOutput.= $strOutputTable;
    
    $strOutput.= "
<script>

$('#5').fixheadertable({ 
   colratio    : [100, 80, 80, 320, 320, 320, 150, 150, 80, 150], 
   height      : 700, 
   width       : 1778, 
   zebra       : true, 
   resizeCol   : true,
   sortable    : true,
   sortType    : ['string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string'],
   sortedColId : '0', 
   dateFormat  : 'Y-m'
});
</script>
";
          
  } else {
    
    $strOutput.= '<p>Keine Events im gewählten Zeitraum vorhanden.</p>' .chr(10);
    
  }

}

$strOutput.= '';

}

?>