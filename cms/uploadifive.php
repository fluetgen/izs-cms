<?php
/*
UploadiFive
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
*/

// Set the uplaod directory
$uploadDir = 'uploads/';

// Set the allowed file extensions
$fileTypes = array('csv', 'pdf', 'doc', 'docx', 'xls', 'txt', 'rtf', 'html', 'zip', 'mp3', 
'wma', 'mpg', 'flv', 'avi', 'jpg', 'jpeg', 'png', 'gif', 'odt', 'txt', 'ppt', 'pptx', 'xlsx'); // Allowed file extensions

$verifyToken = md5('unique_salt' . $_POST['timestamp']);

if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
	$tempFile   = $_FILES['Filedata']['tmp_name'];
	//$uploadDir  = $_SERVER['DOCUMENT_ROOT'] . $uploadDir;
	$targetFile = $uploadDir . $_FILES['Filedata']['name'];

	// Validate the filetype
	$fileParts = pathinfo($_FILES['Filedata']['name']);
	if (in_array(strtolower($fileParts['extension']), $fileTypes)) {

		// Save the file
		move_uploaded_file($tempFile, $targetFile);
		echo "$tempFile, $targetFile";

	} else {

		// The file type wasn't allowed
		echo 'Invalid file type.';

	}
}
?>