<?php

session_start();

require_once('../assets/classes/class.mysql.php');
require_once('../assets/classes/class.database.php');

$strOutput = '';
 
if (isset($_REQUEST['id']) && !isset($_REQUEST['ac'])) {


if (($_REQUEST['ty'] == 'd') || ($_REQUEST['ty'] == 'e')) {
  $strSql = 'SELECT * FROM `Anfragestelle__c` WHERE Id = "' .$_REQUEST['id'] .'"';
  $arrDez = MySQLStatic::Query($strSql);

  $strSql = 'SELECT * FROM `Account` WHERE Id = "' .$arrDez[0]['Information_Provider__c'] .'"';
  $arrAcc_d = MySQLStatic::Query($strSql);
  
  $arrAcc[0]['Name'] = $arrAcc_d[0]['Name'];
  $arrAcc[0]['Abteilung__c'] = $arrDez[0]['Abteilung_Team__c'];
  $arrAcc[0]['Anrede_Briefkopf__c'] = '';
  $arrAcc[0]['Vorname__c'] = $arrDez[0]['Vorname__c'];
  $arrAcc[0]['Nachname__c'] = $arrDez[0]['Name__c'];
  $arrAcc[0]['Anfrage_Stra_e__c'] = $arrDez[0]['Strasse_Hausnummer__c'];
  $arrAcc[0]['Anfrage_PLZ__c'] = $arrDez[0]['PLZ__c'];
  $arrAcc[0]['Anfrage_Ort__c'] = $arrDez[0]['Ort__c'];
  
  $arrAcc[0]['Anrede_Anschreiben__c'] = $arrDez[0]['Anrede__c'];
  $arrAcc[0]['Id'] = $arrDez[0]['Id'];
  $arrAcc[0]['Anfrage_Fax__c'] = $arrDez[0]['Fax__c'];
  $arrAcc[0]['Anfrage_Email_s__c'] = $arrDez[0]['E_Mail_Adressen__c'];

} else {
  $strSql = 'SELECT * FROM `Account` WHERE Id = "' .$_REQUEST['id'] .'"';
  $arrAcc = MySQLStatic::Query($strSql);
}

$strSalut = '';
if ($arrAcc[0]['Anrede_Anschreiben__c'] != '') {
  $strSalut.= $arrAcc[0]['Anrede_Anschreiben__c'];
} else {
  $strSalut.= 'Sehr geehrte Damen und Herren';
}

if (strstr($strSalut, 'geehrte Frau') || strstr($strSalut, 'geehrter Herr')) {
  $strSalut.= ' ' .$arrAcc[0]['Nachname__c'];
}

$arrPeriod = explode('_', $_REQUEST['strSelDate']);


$strSql = 'SELECT * FROM `cms_text` WHERE `ct_id` = "' .$_REQUEST['text'] .'"';
$arrResult = MySQLStatic::Query($strSql);

$strText = str_replace("\n", '', $arrResult[0]['ct_text']);
$strText = str_replace('{Salutation}', $strSalut, $strText);
$strText = str_replace('{Today}', date('d.m.Y'), $strText);
$strText = str_replace('{Month}', str_pad($arrPeriod[0], 2, "0", STR_PAD_LEFT), $strText);
$strText = str_replace('{Year}', $arrPeriod[1], $strText);

$strNameFiles = 'id=' .$arrAcc[0]['Id'] .'&y=' .$arrPeriod[1] .'&m=' .$arrPeriod[0];

$strText = str_replace('{DOWNLOAD_VOLLMACHTEN}', '<a href="http://www.izs-institut.de/cms/_download.php?t=a&' .$strNameFiles .'&evid=' .$_REQUEST['evid'] .'">Vollmachten_' .$arrAcc[0]['Name'] .'.pdf</a>', $strText);
$strText = str_replace('{DOWNLOAD_ANFRAGELISTE}', '<a href="http://www.izs-institut.de/cms/_download.php?t=r&' .$strNameFiles .'&evid=' .$_REQUEST['evid'] .'">Anfrageliste_' .$arrAcc[0]['Name'] .'.pdf</a>', $strText);

$strOutput.= '

<form id="eMail">
  <fieldset class="clearfix">
    <label for="messageM">Nachricht</label><span class="clearfix"></span>
    <textarea name="messageM" id="messageM" class="text ui-widget-content ui-corner-all">' .$strText .'</textarea>
  </fieldset>
</form>

<script type="text/javascript">

var mM = new nicEditor({
  maxHeight : 370
}).panelInstance("messageM");

$(".nicEdit-main").attr("contenteditable","false");
$(".nicEdit-panel").hide();

</script>
';

echo $strOutput;
  
}

?>