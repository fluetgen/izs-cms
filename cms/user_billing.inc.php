<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

require_once('inc/refactor.inc.php');

$intUser = $_SESSION['id'];

$arrHeader = array(
  'user: ' .$intUser,
  'Accept: application/json'
);

if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
  $arrHeader[] = 'test: true';
}

if (isset($_REQUEST['strSelMonth'])) {
    $arrPart  = explode('-', $_REQUEST['strSelMonth']);
    $intMonth = (int) $arrPart[0];
    $intYear  = $arrPart[1];
} else {
    $intMonth = date('n');
    $intYear  = date('Y');
}


function secondsToTime($seconds) {

  $strSign = '';
  if ($seconds < 0) {
    $strSign = '-';
    $seconds = 0 - $seconds;
  }

  $hours = floor($seconds / (60 * 60));

  $divisor_for_minutes = $seconds % (60 * 60);
  $minutes = floor($divisor_for_minutes / 60);

  $divisor_for_seconds = $divisor_for_minutes % 60;
  $seconds = ceil($divisor_for_seconds);

  $obj = array(
      "h" => (int) $hours,
      "m" => (int) $minutes,
      "s" => (int) $seconds,
      "sign" => $strSign
   );

  return $obj;
}

function formatTime ($arrTime = array()) {

  return sprintf('%s%02d:%02d', $arrTime['sign'], $arrTime['h'], $arrTime['m']); //sprintf('%02d:%02d:%02d', $arrTime['h'], $arrTime['m'], $arrTime['s']);

}


$arrHeader[] = 'apikey: 9b0d3b1c-27f2-4e40-8e4d-d735a50dbbee';


$arrRes = curl_get('http://api.izs-institut.de/api/clock/users/', array(), array(), $arrHeader);
$arrUserList = json_decode($arrRes, true);

//print_r($arrUserList); die();

$arrGet = array(
  'month' => $intMonth,
  'year'  => $intYear
);

$arrRes = curl_get('http://api.izs-institut.de/api/clock/tracking/', $arrGet, array(), $arrHeader);
$arrTreckingList = json_decode($arrRes, true);

$arrTracking = array();
if (count($arrTreckingList) > 0) {
  foreach ($arrTreckingList as $intKey => $arrTrackingRaw) {
    $arrTracking[$arrTrackingRaw['id']] = $arrTrackingRaw;
  }
}

$arrGet = array(
  'month' => $intMonth,
  'year'  => $intYear,
  'month_count' => 1,
);

$arrRes = curl_get('http://api.izs-institut.de/api/clock/absence/', $arrGet, array(), $arrHeader);
$arrAbsenceList = json_decode($arrRes, true);

$arrAbsence = array();
if (count($arrAbsenceList) > 0) {

  foreach ($arrAbsenceList as $intKey => $arrAbsenceRaw) {
    $arrAbsence[$arrAbsenceRaw['users_id']][] = $arrAbsenceRaw['date_since'] .' - ' .$arrAbsenceRaw['date_until'];
  }

}


$strOutput = '';
$strOutput.= '<h1>Benutzer - Berichte</h1>' .chr(10);


$strOutput.= '<form method="post" action="" style="padding: 20px 0 20px 0;" id="monthSel">' .chr(10);
$strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);

$strOutput.= '<table>' .chr(10);
$strOutput.= '<tbody>' .chr(10);

$strOutput.= '  <tr>' .chr(10);
$strOutput.= '    <td style="width:110px;">Monat: </td>' .chr(10);
$strOutput.= '    <td><select name="strSelMonth" id="strSelMonth">' .chr(10);

$intShowMonth = 12;

for ($intCount = 0; ($intCount < $intShowMonth); $intCount++) {

    $strMonth = date(('m-Y'), mktime(0, 0, 0, (date('n') - $intCount), 1, date('Y')));

    $strSelected = '';

    if ($strMonth == $_REQUEST['strSelMonth']) {
    $strSelected = ' selected="selected"';
    } 

    $strOutput.= '  <option value="' .$strMonth .'"' .$strSelected .'>' .$strMonth .'</option>' .chr(10);

}

$strOutput.= '    </select></td>' .chr(10);
$strOutput.= '  </tr>' .chr(10);

$strOutput.= '</tbody>' .chr(10);



/*
$strOutput.= '  <tfoot>' .chr(10);
$strOutput.= '  <tr>' .chr(10);
$strOutput.= '    <td align="right" colspan="2"><input type="submit" name="Suche" value="Suche" /></td>' .chr(10);
$strOutput.= '  </tr>' .chr(10);
$strOutput.= '  </tfoot>' .chr(10);
*/
$strOutput.= '</table>' .chr(10);
$strOutput.= '</form>' .chr(10);

$intStundensatz = 13;
$intMaxUmsatz   = 450;

$intMaxSekundenAll = round((($intMaxUmsatz / $intStundensatz) * 60 * 60), 0);
$arrMaxTimePerMonthAll = secondsToTime($intMaxSekundenAll); 

$arrDontShow = array(
  44556,  //Susi
  96218,  //Gordon
  99230,  //Seda
  102440, //Serap
  37460,  //Christian
  96220,  //D.R.
  37471,  //E.E.
  102625, //Frank
  102441, //Kerstin
  44328,  //J. Mutzel
  96219,  //L.R.
  63121,  //P.A.
  37467   //Sonja
);

if (count($arrUserList) > 0) {

    $strOutput.= '<table class="" id="5">' .chr(10);
    $strOutput.= '
    <thead>
      <tr>
        <th>Name</th>
        <th></th>
        <th>Kapazitäten</th>
        <th>Stunden</th>
        <th>maximal</th>
        <th>Telefon</th>
        <th>einsetzbar</th>
        <th>nicht verfügbar</th>
        <th>geplante Abwesenheiten</th>
      </tr>
    </thead>' .chr(10);
    $strOutput.= '<tbody>' .chr(10);




    foreach ($arrUserList as $intKey => $arrUser) {

      //echo $arrUser['name'] .chr(10);

      if (!in_array($arrUser['id'], $arrDontShow)) {

        //echo $arrUser['name'] .chr(10);

        $strSql = 'SELECT * FROM `izs_maginal` WHERE `im_clock_id` = "' .$arrUser['id'] .'"';
        $arrSqlUser = MySQLStatic::Query($strSql);

        if (!isset($arrTracking[$arrUser['id']])) {
          $arrTrackingDetail = array(
            'duration' => 0,
            'name' => $arrUser['name']
          );
        } else {
          $arrTrackingDetail = $arrTracking[$arrUser['id']];
        }

        $strOutput.= '  <tr>' .chr(10);

        $intMaxSekunden = $intMaxSekundenAll;
        $arrMaxTimePerMonth = $arrMaxTimePerMonthAll;

        if (count($arrSqlUser) > 0) {
          $strOutput.= '    <td><span class="hidden">' .$arrSqlUser[0]['im_first'] .' ' .$arrSqlUser[0]['im_last'] .'</span><a href="mailto:' .$arrSqlUser[0]['im_mail'] .'">' .$arrSqlUser[0]['im_first'] .' ' .$arrSqlUser[0]['im_last'] .'</a></td>' .chr(10);
          if ($arrSqlUser[0]['im_max_hours'] > 0) {
            $intMaxSekunden = $arrSqlUser[0]['im_max_hours'] * 60 * 60;
            $arrMaxTimePerMonth = secondsToTime($intMaxSekunden);
          }
        } else {
          $strOutput.= '    <td>' .$arrUser['name'] .'</td>' .chr(10); //' .$arrUser['id'] .'
        }

        $arrDone = secondsToTime($arrTrackingDetail['duration']); 

        $intDiff = $intMaxSekunden - $arrTrackingDetail['duration'];
        $arrDiff = secondsToTime($intDiff); 

        $strOutput.= '    <td><progress max="' .$intMaxSekunden .'" value="' .$arrTrackingDetail['duration'] .'" title="' .round(($arrTrackingDetail['duration'] / $intMaxSekunden) * 100, 2) .'%"></progress></td>' .chr(10);
        $strOutput.= '    <td><span class="hidden">' .$intDiff .'</span>' .formatTime($arrDiff) .'</td>' .chr(10);
        $strOutput.= '    <td><span class="hidden">' .$arrTrackingDetail['duration'] .'</span>' .formatTime($arrDone) .'</td>' .chr(10);
        $strOutput.= '    <td><span class="hidden">' .$intMaxSekunden .'</span>' .formatTime($arrMaxTimePerMonth) .'</td>' .chr(10);

        if (count($arrSqlUser) > 0) {
          $strOutput.= '    <td>' .$arrSqlUser[0]['im_mobile'] .'</td>' .chr(10);
          $strOutput.= '    <td>' .$arrSqlUser[0]['im_application'] .'</td>' .chr(10);
          $strOutput.= '    <td>' .$arrSqlUser[0]['im_not_avail'] .'</td>' .chr(10);
        } else {
          $strOutput.= '    <td>' .'' .'</td>' .chr(10);
          $strOutput.= '    <td>' .'' .'</td>' .chr(10);
          $strOutput.= '    <td>' .'' .'</td>' .chr(10);
        }

        $strAbsence = '';
        if (isset($arrAbsence[$arrUser['id']])) {
          $strAbsence = implode(', ', $arrAbsence[$arrUser['id']]);
        }
        $strOutput.= '    <td>' .$strAbsence .'</td>' .chr(10);

        $strOutput.= '  </tr>' .chr(10);

      }

    }

    $strOutput.= '</table>' .chr(10);

}

//die();

//$strOutput.= '<h3>Geplante Abwesenheiten</h3>' .chr(10);


$strOutput.= "
<script>
$('#5').fixheadertable({ 
 colratio    : [180, 150, 100, 100, 100, 100, 250, 200, 200], 
 height      : 360, 
 width       : 1398, 
 zebra       : true, 
 resizeCol   : true,
 sortable    : true,
 sortType    : ['hidden', '', 'hidden', 'hidden', 'hidden', '', '', ''],
 sortedColId : 0, 
 dateFormat  : 'Y-m'
});

$( document ).ready(function() {

  function formatTable () {
    $('th.ui-widget-content:not(:first)').css('text-align', 'center');
    $('table#5 td:not(:first-child)').css('text-align', 'center');
    $.each($('.hidden'), function (index, value) {
      if (parseInt($(value).text()) < 0 ) $(this).parent().css('color', 'red');
    });
  }

  $('table.head th').click(function(){
    formatTable();
  });

  $('#strSelMonth').change(function(){
    $('#monthSel').submit();
  });

  formatTable();

});

</script>
";





?>