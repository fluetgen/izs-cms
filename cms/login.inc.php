<?php

$strToken = 'triple2013';
$boolLogin = false;
$strReason = '';

require_once('../assets/classes/class.mysql.php');
  
if (isset($_REQUEST['user']) && isset($_REQUEST['pass'])) {

  $strSql = 'SELECT `cl_id`, `cl_ks_id` FROM `cms_login` WHERE `cl_user` = "' .MySQLStatic::esc($_REQUEST['user']) .'" AND `cl_pass` = MD5("' .MySQLStatic::esc($_REQUEST['pass']) .'") AND `cl_deleted` = 0';
  $arrResult = MySQLStatic::Query($strSql);

  if (count($arrResult) > 0) {
    session_regenerate_id(true);
    $_SESSION['id'] = $arrResult[0]['cl_id'];
    $_SESSION['token'] = MD5($strToken .'_' .$_SESSION['id']);

    //izs.de (CMS)
    $intExpire = time() + (365 * 24 * 3600);//(3 * 3600);
    setcookie('id', $arrResult[0]['cl_id'], $intExpire, '/', 'izs.de');
    setcookie('token', MD5($strToken .'_' .$arrResult[0]['cl_id']), $intExpire, '/', 'izs.de');

    //mein-verleiherportal
    if ($arrResult[0]['cl_ks_id'] != '') {
      $strSql2 = 'DELETE FROM `ver_session` WHERE `vs_user_id` = "' .$arrResult[0]['cl_ks_id'] .'"';
      $arrResult2 = MySQLStatic::Query($strSql2);

      $strSql3 = 'INSERT INTO `ver_session` (`vs_id`, `vs_contact_id`, `vs_user_id`, `vs_account_id`, `vs_session`, `vs_time`) VALUES (NULL, "", "' .$arrResult[0]['cl_ks_id'] .'", "", "' .session_id() .'", NOW())';
      $arrResult3 = MySQLStatic::Query($strSql3);
    }

    if (isset($_SERVER['SERVER_NAME']) && (strstr($_SERVER['SERVER_NAME'], 'test.') !== false)) {
      $strOrigin = 'test';
    } else {
      $strOrigin = 'www';
    }

    //izs-institut.de (CRM)
    header('Location: https://sso.izs-institut.de/back.php?id=' .$_SESSION['id'] .'&token=' .$_SESSION['token'] .'&o=' .$strOrigin);
    exit;

    $boolLogin = true;
  } else {
    //mail('f.luetgen@gmx.de', 'Login Error', print_r($_REQUEST, true));
  }

  
} else {

  if (isset($_SESSION['id']) && isset($_SESSION['token'])) {
    if ($_SESSION['token'] != MD5($strToken .'_' .$_SESSION['id'])) {
      @session_destroy();
      $boolLogin = false;
      /*
      $intExpire = time() - 3600;
      setcookie('id', $_SESSION['id'], $intExpire, '/', 'izs.de');
      setcookie('token', MD5($strToken .'_' .$_SESSION['id']), $intExpire, '/', 'izs.de');
      */
      //mail('f.luetgen@gmx.de', 'ID != Token', print_r($_REQUEST, true));
    } else {
      $boolLogin = true;
      /*
      $intExpire = time() + (3 * 3600);
      setcookie('id', $_SESSION['id'], $intExpire, '/', 'izs.de');
      setcookie('token', MD5($strToken .'_' .$_SESSION['id']), $intExpire, '/', 'izs.de');
      */
    }
  } else {
    //mail('f.luetgen@gmx.de', 'No ID/Token', print_r($_REQUEST, true));
    @session_destroy();
    $boolLogin = false;
    /*
    $intExpire = time() - 3600;
    setcookie('id', $_SESSION['id'], $intExpire, '/', 'izs.de');
    setcookie('token', MD5($strToken .'_' .$_SESSION['id']), $intExpire, '/', 'izs.de');
    */
  if ($_REQUEST['ac'] != '') {
      $strReason = 'forced';
    }
  }
  
  if (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'logout')) {
    @session_destroy();
    $boolLogin = false;
    ///*
    $intExpire = time() - 3600;
    setcookie('id', $_SESSION['id'], $intExpire, '/', 'izs.de');
    setcookie('token', MD5($strToken .'_' .$_SESSION['id']), $intExpire, '/', 'izs.de');
    //*/
}

}

if (!isset($_REQUEST['ac'])) {
  $strFirst = '';

  $arrPageList = [];
  $strSql = "SELECT * FROM `cms_page` ORDER BY `cp_parent`, `cp_ord`";
  $arrResult = MySQLStatic::Query($strSql);

  if (count($arrResult) > 0) {
    
    foreach ($arrResult as $intKey => $arrPageRaw) {
      $arrPageList[$arrPageRaw['cp_parent']][] = $arrPageRaw;
    }

  }
  
  if (count($arrPageList[0]) > 0) {

    $arrPageRightList = [];
    $strSqlR = "SELECT * FROM `cms_right` WHERE `cr_cl_id` = '" .$_SESSION['id'] ."' AND `cr_access` = 1";
    $arrResultR = MySQLStatic::Query($strSqlR);

    if (count($arrResultR) > 0) {
      foreach ($arrResultR as $intKey => $arrRightRaw) {
        $arrPageRightList[$arrRightRaw['cr_cp_id']] = $arrRightRaw;
      }
    }
  
    foreach ($arrPageList[0] as $arrPage) {
      
      if (isset($arrPageRightList[$arrPage['cp_id']])) {
      
        if ($arrPage['cp_short'] != '') {
          if ($strFirst == '') {
            $strFirst = $arrPage['cp_short'];
            break;
          }
        }
      
      } else {
        continue;
      }

      $arrResult2 = $arrPageList[$arrPage['cp_id']] ?? [];
      
      if (count($arrResult2) > 0) {
  
        foreach ($arrResult2 as $arrPage2) {
  
          $arrResultR2 = $arrPageRightList[$arrPage2['cp_id']] ?? [];
                
          if ((($arrPage2['cp_admin_only'] && $intAdmin) || (!$arrPage2['cp_admin_only'])) && (count($arrResultR2) > 0)) {
          
            if ($arrPage2['cp_short'] != '') {
              if ($strFirst == '') {
                $strFirst = $arrPage2['cp_short'];
                break;
              }
            }
          
          }
  
        }
  
      }
      
    }
  }
  
  if ($strFirst != '') {
    $_REQUEST['ac'] = $strFirst;
  } else {
    /*
    $intExpire = time() - 3600;
    setcookie('id', $_SESSION['id'], $intExpire, '/', 'izs.de');
    setcookie('token', MD5($strToken .'_' .$_SESSION['id']), $intExpire, '/', 'izs.de');
    */
    //mail('f.luetgen@gmx.de', 'No Pages', print_r($_REQUEST, true));
    header('Location: /cms/index.php?ac=logout');
  }

}

?>