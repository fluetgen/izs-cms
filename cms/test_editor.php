<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

require_once('../assets/classes/class.mysql.php');

$strFolder = 'uploads_events/';

$arrToRename = array();
$arrMissing  = array();

$strSql = 'SELECT * FROM `izs_event_file` ORDER BY `ef_id`';
$arrSql = MySQLStatic::Query($strSql);

foreach ($arrSql as $intKey => $arrDataset) {

  if ($arrDataset['ef_name'] != '') {

    $arrPathInfo = pathinfo($arrDataset['ef_name']); //['extension']
    $strFileName = str_pad($arrDataset['ef_id'], 11, '0', STR_PAD_LEFT) .'.' .$arrPathInfo['extension'];

    if (!file_exists($strFolder .$strFileName)) {

      $strFileNameTemp = $arrDataset['ef_name'];
      if (file_exists($strFolder .$strFileNameTemp)) {
        rename($strFolder .$strFileNameTemp, $strFolder .$strFileName);
        $arrToRename[] = $arrDataset['ef_name'];
      } else {
        $arrMissing[] = $arrDataset['ef_name'];
      }

    }
  
  }
  

}

foreach ($arrMissing as $intKey => $strMissing) {
  echo $strMissing .chr(10);
}

//print_r($_REQUEST);

//00000003748.pdf

?><html>
<head>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jodit/3.1.92/jodit.min.css">
  <script src="//cdnjs.cloudflare.com/ajax/libs/jodit/3.1.92/jodit.min.js"></script>
</head>

<body>

    <!-- <div id="editor"></div> -->


<script>
var editor = new Jodit("#editor", {
  "spellcheck": false,
  "language": "de",
  "toolbarAdaptive": false,
  "showCharsCounter": false,
  "showWordsCounter": false,
  "showXPathInStatusbar": false,

  "askBeforePasteHTML": false,
  "askBeforePasteFromWord": false,
  "defaultActionOnPaste": "insert_only_text",

  "buttons": "source,|,bold,underline,italic,|,,ul,|,,|,|,align,undo,redo,indent,outdent,fullsize",

  "disablePlugins": "bold"
});    
</script>


</body>
</html>