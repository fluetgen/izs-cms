<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

define('FPDF_FONTPATH', 'fpdf17/font/');
define('TEMP_PATH', 'temp/');

require_once('../assets/classes/class.mysql.php');

require('fpdf17/fpdf.php');
require('fpdf17/fpdi.php');

require_once('fpdf17/classes/fpdfmulticell.php');
require_once('fpdf17/mypdf-multicell.php');

$arrMonth = array(
  1 => 'Januar',
  2 => 'Februar',
  3 => 'März',
  4 => 'April',
  5 => 'Mai',
  6 => 'Juni',
  7 => 'Juli',
  8 => 'August',
  9 => 'September',
  10 => 'Oktober',
  11 => 'November',
  12 => 'Dezember'
);

$intAccPerPage = 18;

$strOutput = '';

$_REQUEST['Id'] = rawurldecode($_REQUEST['Id']);

$strSql = 'SELECT * FROM `Group__c` WHERE `Id` = "' . $_REQUEST['Id'] . '"';
$arrCompanyGroup = MySQLStatic::Query($strSql);

$strSql2 = 'SELECT `Vertrag_g_ltig_f_r__c` FROM `Vertrag__c` WHERE `Aktiv__c` = "true" AND `Company_Group__c` = "' . $_REQUEST['Id'] . '"';
$arrVertrag = MySQLStatic::Query($strSql2);

$boolNurEntleiherportal = false;
if ($arrVertrag[0]['Vertrag_g_ltig_f_r__c'] == 'Entleiherportal') {
  $boolNurEntleiherportal = true;
}


class concat_pdf extends fpdi
{
  var $files = array();
  function concat_pdf($orientation = 'P', $unit = 'mm', $format = 'A4')
  {
    parent::__construct($orientation, $unit, $format);
  }
  function setFiles($files)
  {
    $this->files = $files;
  }
  function concat()
  {
    foreach ($this->files as $file) {
      $pagecount = $this->setSourceFile($file);
      for ($i = 1; $i <= $pagecount; $i++) {
        $tplidx = $this->ImportPage($i);
        $this->AddPage();
        $this->useTemplate($tplidx);
      }
    }
  }

  function WriteText($text)
  {

    $intPosIni = 0;
    $intPosFim = 0;
    $intLineHeight = 5;

    if (strpos($text, '<') !== false && strpos($text, '[') !== false) {
      if (strpos($text, '<') < strpos($text, '[')) {
        $this->Write($intLineHeight, substr($text, 0, strpos($text, '<')));
        $intPosIni = strpos($text, '<');
        $intPosFim = strpos($text, '>');
        $this->SetFont('', 'B');
        $this->Write($intLineHeight, substr($text, $intPosIni + 1, $intPosFim - $intPosIni - 1));
        $this->SetFont('', '');
        $this->WriteText(substr($text, $intPosFim + 1, strlen($text)));
      } else {
        $this->Write($intLineHeight, substr($text, 0, strpos($text, '[')));
        $intPosIni = strpos($text, '[');
        $intPosFim = strpos($text, ']');
        $w = $this->GetStringWidth('a') * ($intPosFim - $intPosIni - 1);
        $this->Cell($w, $this->FontSize + 0.75, substr($text, $intPosIni + 1, $intPosFim - $intPosIni - 1), 1, 0, '');
        $this->WriteText(substr($text, $intPosFim + 1, strlen($text)));
      }
    } else {
      if (strpos($text, '<') !== false) {
        $this->Write($intLineHeight, substr($text, 0, strpos($text, '<')));
        $intPosIni = strpos($text, '<');
        $intPosFim = strpos($text, '>');
        $this->SetFont('', 'B');
        $this->WriteText(substr($text, $intPosIni + 1, $intPosFim - $intPosIni - 1));
        $this->SetFont('', '');
        $this->WriteText(substr($text, $intPosFim + 1, strlen($text)));
      } elseif (strpos($text, '[') !== false) {
        $this->Write($intLineHeight, substr($text, 0, strpos($text, '[')));
        $intPosIni = strpos($text, '[');
        $intPosFim = strpos($text, ']');
        $w = $this->GetStringWidth('a') * ($intPosFim - $intPosIni - 1);
        $this->Cell($w, $this->FontSize + 0.75, substr($text, $intPosIni + 1, $intPosFim - $intPosIni - 1), 1, 0, '');
        $this->WriteText(substr($text, $intPosFim + 1, strlen($text)));
      } else {
        $this->Write($intLineHeight, $text);
      }
    }
  }

  function SetDash($black = false, $white = false)
  {
    if ($black and $white) {
      $s = sprintf('[%.3f %.3f] 0 d', $black * $this->k, $white * $this->k);
    } else {
      $s = '[] 0 d';
    }
    $this->_out($s);
  }
}



$arrPeriod = explode('_', $_REQUEST['strSelDate']);
$strCompanyGroup = $arrCompanyGroup[0]['Name'];

$pdf = new myPDF();
$pdf->AliasNbPages();

$pdf->AddFont('OfficinaSans', '', 'itc-officina-sans-lt-book.php');
$pdf->AddFont('OfficinaSans', 'B', 'itc-officina-sans-lt-bold.php');
$pdf->AddFont('OfficinaSans', 'I', 'itc-officina-sans-lt-book-italic.php');
$pdf->AddFont('OfficinaSans', 'BI', 'itc-officina-sans-lt-bold-italic.php');
$pdf->AddFont('symbols', '', 'symbols.php');

$pdf->AddPage();
$pdf->SetAutoPageBreak(false);
$pdf->Image('pdf/zertifikat_vorlage.png', 0, 0, 210, 297, 'PNG');

$strSql = 'SELECT * FROM `Account` WHERE `SF42_Company_Group__c` = "' . $_REQUEST['Id'] . '"';
$arrAccountList = MySQLStatic::Query($strSql);

$strSql = 'SELECT * FROM `Group__c` WHERE `Id` = "' . $_REQUEST['Id'] . '"';
$arrGroup = MySQLStatic::Query($strSql);

$arrEventAccountList = array();

foreach ($arrAccountList as $intKey => $arrAccount) {

  $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Betriebsnummer_ZA__c` = "' . $arrAccount['SF42_Comany_ID__c'] . '" AND `Beitragsmonat__c` = "' . $arrPeriod[0]  . '" AND `Beitragsjahr__c` = "' . $arrPeriod[1]  . '" AND `SF42_EventStatus__c` IN ("to enquire", "enquired", "no result", "not OK", "OK") ORDER BY `Betriebsnummer_ZA__c`';
  $arrEventList = MySQLStatic::Query($strSql);

  $boolPayed = true;

  if (count($arrEventList) > 0) {

    //echo $arrAccount['Name'];

    $strPeriod = $arrEventList[0]['SF42_Payment_Period__c'];
    $strBetrNr = $arrEventList[0]['Betriebsnummer_ZA__c'];

    foreach ($arrEventList as $intEventKey => $arrEvent) {
      if (!in_array($arrEvent['SF42_EventStatus__c'], array('no result', 'not OK', 'OK'))) {
        $boolPayed = false;
        break;
      }
    }

    $arrAccount['periodid'] = $strPeriod;
    $arrAccount['betriebsnr'] = $strBetrNr;
    if ($boolPayed == true) {
      $arrEventAccountList[strtolower($arrAccount['Name']) . '_' . $strBetrNr] = $arrAccount;
    }
  }
}

$arrAmpel = array(
  'rot' => 'rot',
  'grün' => 'gruen',
  'gelb' => 'gelb'
);

$arrAmpelDb = array(
  'rot' => 'red',
  'grün' => 'green',
  'gelb' => 'yellow'
);

if (count($arrEventAccountList) > 0) {
  ksort($arrEventAccountList);

  //print_r($arrEventAccountList); die();

  $strBraunR = 160;
  $strBraunG = 132;
  $strBraunB =   0;

  $pdf->SetMargins(20, 20);

  $oMulticell = new FpdfMulticell($pdf);
  $oMulticell->SetStyle("p", "OfficinaSans", "", 12, "0,0,0");
  $oMulticell->SetStyle("b", "OfficinaSans", "B", 12, "0,0,0");
  $oMulticell->setStyle("i", "OfficinaSans", "I", 12, "0,0,0");

  $oMulticell->SetStyle("h1", "OfficinaSans", "", 16, "0,0,0");
  $oMulticell->SetStyle("h3", "OfficinaSans", "B", 12, "0,0,0");
  $oMulticell->SetStyle("h4", "OfficinaSans", "BI", 12, "0,0,0");
  $oMulticell->SetStyle("hh", "OfficinaSans", "B", 12, "0,0,0");

  $oMulticell->SetStyle("sg", "OfficinaSans", "", 10, "115,114,108");
  $oMulticell->SetStyle("sg", "OfficinaSans", "", 10, "0,0,0");

  $oMulticell->SetStyle("ss", "OfficinaSans", "", 8.8, "115,114,108");
  $oMulticell->SetStyle("font", "symbols", "", 12, "0,0,0");
  $oMulticell->SetStyle("style", "OfficinaSans", "BI", 12, "0,0,0");
  $oMulticell->SetStyle("size", "OfficinaSans", "BI", 12, "0,0,0");
  $oMulticell->SetStyle("color", "OfficinaSans", "BI", 12, "0,0,0");

  $pdf->SetTextColor($strBraunR, $strBraunG, $strBraunB);
  $pdf->SetFont('OfficinaSans', 'B', 18);
  $pdf->Text(20, 30, utf8_decode('PRÜFZERTIFIKAT'));

  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetFont('OfficinaSans', '', 18);
  $pdf->Text(65.5, 30, utf8_decode(str_pad($arrPeriod[0], 2, "0", STR_PAD_LEFT) . '.' . $arrPeriod[1]));


  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetFont('OfficinaSans', '', 9);
  $pdf->Text(20, 35, utf8_decode('Erstellt am ' . date('d.m.Y')));


  $pdf->SetTextColor(134, 134, 134);
  $pdf->SetFont('OfficinaSans', '', 7);
  $pdf->Text(20, 45, utf8_decode('Geprüftes Unternehmen'));
  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetFont('OfficinaSans', '', 10);
  $pdf->Text(20, 49, utf8_decode($strCompanyGroup));


  //Registriert seit  Gesamtstatus seit Registrierung
  $pdf->SetTextColor(134, 134, 134);
  $pdf->SetFont('OfficinaSans', '', 7);
  $pdf->Text(20, 55, utf8_decode('Datum der Erstellung'));
  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetFont('OfficinaSans', '', 10);
  $pdf->Text(20, 59, utf8_decode(date('d.m.Y')));

  if ($boolNurEntleiherportal === false) {
    $pdf->SetTextColor(134, 134, 134);
    $pdf->SetFont('OfficinaSans', '', 7);
    $pdf->Text(55, 55, utf8_decode('Link zur Unternehmensseite bei IZS'));

    $pdf->SetY(55);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFont('OfficinaSans', '', 10);

    $pdf->SetX(55);
    if ($arrCompanyGroup[0]['Direktlink_zum_Konto__c'] != '') {
      $strLink = 'http://www.izs.de/' . $arrCompanyGroup[0]['Direktlink_zum_Konto__c'];
    } else {
      $strLink = 'http://www.izs.de/unternehmensinfo.html?companyid=' . $_REQUEST['Id'];
    }
    $oMulticell->multiCell(145, 5, '<a href="' . $strLink . '">' . str_replace('http://', '', $strLink) . '</a>');
  }
  
  $pdf->SetX(20);


  //Registriert seit  Gesamtstatus seit Registrierung
  $pdf->SetTextColor(134, 134, 134);
  $pdf->SetFont('OfficinaSans', '', 7);
  $pdf->Text(20, 65, utf8_decode('Registriert seit'));
  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetFont('OfficinaSans', '', 10);
  $objDate = new DateTime($arrGroup[0]['Registriert_bei_IZS_seit__c']);
  $pdf->Text(20, 69, utf8_decode($objDate->format('d.m.Y')));

  $pdf->SetTextColor(134, 134, 134);
  $pdf->SetFont('OfficinaSans', '', 7);
  $pdf->Text(55, 65, utf8_decode('Gesamtstatus seit Registrierung'));
  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetFont('OfficinaSans', '', 10);
  //$pdf->Text(45, 54, utf8_decode($arrGroup[0]['Gesamtstatus__c']));

  if (isset($arrAmpel[$arrGroup[0]['Gesamtstatus__c']])) {
    $intY = 66.3;
    $pdf->Image('img/status_' . $arrAmpel[$arrGroup[0]['Gesamtstatus__c']] . '.png', 55.2, $intY, 0, 0, 'PNG');
    $arrStatus['status'] = $arrAmpelDb[$arrGroup[0]['Gesamtstatus__c']];
  } else {
    $pdf->Text(55, 69, utf8_decode($arrGroup[0]['Gesamtstatus__c']));
    $arrStatus['status'] = 'gray';
  }

  //Registriert seit  Gesamtstatus seit Registrierung
  $pdf->SetTextColor(134, 134, 134);
  $pdf->SetFont('OfficinaSans', '', 7);
  $pdf->Text(20, 75, utf8_decode('Link zur PDF-Version'));
  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetFont('OfficinaSans', '', 10);
  $intY = 76.3;
  $pdf->Image('../assets/images/zertifikat/icon_pdf.png', 20.2, $intY, 0, 0, 'PNG');
  $pdf->SetY(76);
  $pdf->SetX(25);//https://www.izs-institut.de/assets/pdf-zertifikate/a083000000VbFpBAAV_2016_3_zertifikat_sv.pdf
  $strLink = 'http://www.izs.de/assets/pdf-zertifikate/' . $_REQUEST['Id'] . '_' . $arrPeriod[1] . '_' . $arrPeriod[0]  . '_zertifikat_sv.pdf';
  $oMulticell->multiCell(22, 5, '<a href="' . $strLink . '">PDF-Dokument</a>');
  $pdf->SetX(20);

  $pdf->SetTextColor(134, 134, 134);
  $pdf->SetFont('OfficinaSans', '', 7);
  $pdf->Text(55, 75, utf8_decode('Hinweis zum Gesamtstatus'));
  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetFont('OfficinaSans', '', 10);

  $arrStatus['desc'] = '';
  if (1) {
    $pdf->SetY(76);
    $pdf->SetX(55);
    $oMulticell->multiCell(135, 5, utf8_decode($arrGroup[0]['Hinweis_sichtbar_auf_Portal__c']), 0);
    $arrStatus['desc'] = $arrGroup[0]['Hinweis_sichtbar_auf_Portal__c'];
  } else {
    $pdf->Text(55, 79.5, utf8_decode($arrGroup[0]['Hinweis_sichtbar_auf_Portal__c']));
    $pdf->SetY(85);
  }

  $pdf->SetFont('OfficinaSans', '', 12);
  $text = 'Wir haben auf Basis der <b>SV-Beitragsnachweisdaten für den Monat ' . str_pad($arrPeriod[0], 2, '0', STR_PAD_LEFT) . '/' . $arrPeriod[1] . '</b>, welche von ';
  $text .= 'den nachstehend genannten Betriebsstätten frist- und formgerecht an IZS übermittelt wurden, ';
  $text .= 'sämtliche Meldungen überprüft.';
  //$pdf->WriteText(utf8_decode($text));

  $pdf->SetY(98);
  $oMulticell->multiCell(170, 5, utf8_decode($text), 0, 'J');

  if ($arrGroup[0]['Gesamtstatus__c'] != 'rot') {

    $pdf->Image('pdf/siegel.png', 20, 116, 170, 20.2, 'PNG');

    $pdf->SetMargins(41.2, 20, 20);
    $pdf->SetY(117.5);
    $pdf->SetFont('OfficinaSans', '', 12);

    if (($_REQUEST['Id'] == 'a083000000iI0fqAAC') && ($_REQUEST['strSelDate'] == '4_2016')) {
      $text = 'Nach unserem Kenntnisstand bestehen aktuell bei <b>keiner der angefragten Krankenkassen ';
      $text .= 'Beitragsrückstände >100 EUR</b>.';
    } else {
      if ($boolNurEntleiherportal === true) { 
        $text = 'Nach unserem Kenntnisstand bestehen aktuell bei <b>keiner der angefragten Krankenkassen ';
        $text .= 'Beitragsrückstände >100 EUR</b>.';
      } else {
        $text = 'Nach unserem Kenntnisstand bestehen aktuell bei <b>keiner der angefragten Krankenkassen ';
        $text .= 'Beitragsrückstände >100 EUR</b>. Alle Betriebsstätten sind weiterhin berechtigt, das ';
        $text .= 'IZS-Vertrauenssiegel zu führen. ';
      }
    }

    $oMulticell->multiCell(146, 5, utf8_decode($text), 0, 'J');
  }

  $pdf->SetMargins(20, 20, 20);

  //print_r($arrEventAccountList); die();

  if (count($arrEventAccountList) <= 4) {

    //print_r($arrEventAccountList); die();

    $pdf->SetDrawColor($strBraunR, $strBraunG, $strBraunB);
    $pdf->SetLineWidth(0.1);
    $intHeight = 140;
    $pdf->Line(20, $intHeight, 130, $intHeight);
    $pdf->Line(135, $intHeight, 160, $intHeight);
    $pdf->Line(165, $intHeight, 190, $intHeight);

    $intHeight = 144.5;
    $pdf->SetTextColor($strBraunR, $strBraunG, $strBraunB);
    $pdf->SetFont('OfficinaSans', '', 10);
    $pdf->Text(20.5, $intHeight, utf8_decode('Betriebsstätte'));
    $pdf->Text(135.5, $intHeight, utf8_decode('Betriebsnummer'));
    $pdf->Text(165.5, $intHeight, utf8_decode('Detailbericht'));

    $pdf->SetLineWidth(0.5);
    $intHeight = 146.5;
    $pdf->Line(20, $intHeight, 130, $intHeight);
    $pdf->Line(135, $intHeight, 160, $intHeight);
    $pdf->Line(165, $intHeight, 190, $intHeight);

    $intCount = 0;

    foreach ($arrEventAccountList as $strKey => $arrAccount) {
      $intHeight = 149 + ($intCount * 9);

      if ($arrAccount['SF42_Payment_Status__c'] == 'Yellow') {
        $pdf->Image('../assets/images/sys/ampel_gelb.png', 21, $intHeight, 3.5, 5, 'PNG');
      } elseif ($arrAccount['SF42_Payment_Status__c'] == 'Red') {
        $pdf->Image('../assets/images/sys/ampel_rot.png', 21, $intHeight, 3.5, 5, 'PNG');
      } elseif ($arrAccount['SF42_Payment_Status__c'] == 'Green') {
        $pdf->Image('../assets/images/sys/ampel_gruen.png', 21, $intHeight, 3.5, 5, 'PNG');
      } else {
        $pdf->Image('../assets/images/sys/ampel_grau.png', 21, $intHeight, 3.5, 5, 'PNG');
      }

      $pdf->SetMargins(26, 20);
      $intHeight = 151.5 + ($intCount * 9);

      $pdf->SetY($intHeight);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->SetFont('OfficinaSans', '', 10);
      $strLink = 'http://www.izs.de/kontoauszug.html?companyid=' . $_REQUEST['Id'] . '&accountid=' . $arrAccount['Id'] . '&selectedMonth=' . $arrPeriod[0] . '&selectedYear=' . $arrPeriod[1] . '';
      $pdf->Write(0, utf8_decode($arrAccount['Name']), $strLink);

      $pdf->SetLineWidth(0.1);
      $pdf->SetDash(0.1, 0.7);
      $intHeight = 156 + ($intCount * 9);
      $pdf->Line(20, $intHeight, 130, $intHeight);
      $pdf->Line(135, $intHeight, 160, $intHeight);
      $pdf->Line(165, $intHeight, 190, $intHeight);

      $pdf->SetMargins(20, 20, 20);

      $pdf->SetX(140);
      $pdf->Write(0, utf8_decode($arrAccount['betriebsnr']));
      $pdf->SetX(164.6);
      $pdf->SetTextColor(115, 114, 108);
      $strLink = 'http://www.izs.de/monatsreport.php?detailId=' . $arrAccount['Id'] . '&selectedMonth=' . $arrPeriod[0] . '&selectedYear=' . $arrPeriod[1] . '';
      $pdf->Write(0, utf8_decode('PDF abrufen'), $strLink);

      //WRITE STATUS
      $strSql = 'DELETE FROM `izs_infoservice_status` WHERE `is_group_id` = "' . $_REQUEST['Id'] . '" AND `is_year` = "' . $arrPeriod[1] . '" AND `is_month` = "' . $arrPeriod[0]  . '" AND `is_account_id` = "' .MySQLStatic::esc($arrAccount['Id']) .'"';
      $arrSql = MySQLStatic::Query($strSql);
      
      $strSql = 'INSERT INTO `izs_infoservice_status` (`is_id`, `is_group_id`, `is_account_id`, `is_year`, `is_month`, `is_status`, `is_desc`) ';
      $strSql.= 'VALUES (NULL, "' .MySQLStatic::esc($_REQUEST['Id']) .'", "' .MySQLStatic::esc($arrAccount['Id']) .'", "' .MySQLStatic::esc($arrPeriod[1]) .'", "' .MySQLStatic::esc($arrPeriod[0]) .'", ';
      $strSql.= '"' .MySQLStatic::esc(strtolower($arrAccount['SF42_Payment_Status__c'])) .'", "' .MySQLStatic::esc($arrAccount['Subline__c']) .'");';
      $intSql = MySQLStatic::Insert($strSql);

      $intCount++;
    }

    $intY = $pdf->GetY() + 10;
    $pdf->SetY($intY);

    $pdf->SetTextColor(115, 114, 108);
    $pdf->SetFont('OfficinaSans', '', 8.8);
    $text = '<ss>Durch Klick auf den Namen des Unternehmens können Sie direkt dessen Online-Konto einsehen. Um den detaillierten Monatsbericht ';
    $text .= 'zu einem Unternehmen abzurufen, klicken Sie einfach auf "PDF abrufen". Je nach Umfang des Berichts kann dessen Erstellung bis zu ';
    $text .= 'einer Minute dauern. In beiden Fällen ist das Bestehen einer Internetverbindung Voraussetzung für die beschriebene Funktionalität.</ss>';
    $oMulticell->multiCell(170, 3.7, utf8_decode($text), 0, 'J');
    //$pdf->Write(3.7, utf8_decode($text));

    $intY = $pdf->GetY() + 12;
    $pdf->SetY($intY);

    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFont('OfficinaSans', '', 12);
    $text = 'Wir garantieren, dass uns sämtliche Auskünfte im Original vorliegen und von den Krankenkassen ';
    $text .= 'direkt an uns gesendet wurden. Für Fragen und weitere Informationen stehen wir Ihnen jederzeit ';
    $text .= 'gerne zur Verfügung.';
    $oMulticell->multiCell(170, 5, utf8_decode($text), 0, 'J');
    //$pdf->Write(5, utf8_decode($text));

    $intY = $pdf->GetY() + 12;
    $pdf->SetY($intY);
    $pdf->SetFont('OfficinaSans', '', 12);
    $oMulticell->multiCell(170, 5, utf8_decode('<b>IZS Institut für Zahlungssicherheit GmbH</b>'));
    $oMulticell->multiCell(170, 5, utf8_decode('Würmtalstraße 20a <font>A</font> 81375 München'));
    $pdf->SetMargins(20, 20, 20);

    $intY = $pdf->GetY() + 7;
    $intHeight = $intY;

    /*
$pdf->SetTextColor(255, 0, 0);
$pdf->Text(120, $intHeight, utf8_decode('Wir sind umgezogen!'));
$pdf->SetTextColor(0, 0, 0);
*/

    $pdf->Text(20, $intHeight, utf8_decode('Telefon:'));
    $pdf->Text(37, $intHeight, utf8_decode('089 122237770'));
    $intHeight = $intHeight + 5;
    $pdf->Text(20, $intHeight, utf8_decode('Fax:'));
    $pdf->Text(37, $intHeight, utf8_decode('089 122237779'));
    $intHeight = $intHeight + 5;
    $pdf->Text(20, $intHeight, utf8_decode('E-Mail:'));
    $pdf->Text(37, $intHeight, utf8_decode('infoservice@izs-institut.de'));

    $pdf->SetMargins(20, 20, 20);

    $intY = $pdf->GetY() + 27;
    $pdf->SetY($intY);
    $pdf->SetTextColor(115, 114, 108);
    $pdf->SetFont('OfficinaSans', '', 10);
    $text = '<sg><a href="http://www.izs.de/agb.html">Es gelten unsere Allgemeinen Geschäftsbedingungen.</a></sg>';
    $oMulticell->multiCell(170, 5, utf8_decode($text), 0, 'L');
    //$pdf->Write(5, utf8_decode($text), 'http://www.izs.de/agb.html');

    $pdf->SetTextColor(0, 0, 0);

  } else {

    $intY = $pdf->GetY() + 17;
    $pdf->SetY($intY);

    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFont('OfficinaSans', '', 12);
    $text = 'Wir garantieren, dass uns sämtliche Auskünfte im Original vorliegen und von den Krankenkassen ';
    $text .= 'direkt an uns gesendet wurden. Für Fragen und weitere Informationen stehen wir Ihnen jederzeit ';
    $text .= 'gerne zur Verfügung.';
    $oMulticell->multiCell(170, 5, utf8_decode($text), 0, 'J');
    //$pdf->Write(5, utf8_decode($text));

    $intY = $pdf->GetY() + 12;
    $pdf->SetY($intY);
    $pdf->SetFont('OfficinaSans', '', 12);
    $oMulticell->multiCell(170, 5, utf8_decode('<b>IZS Institut für Zahlungssicherheit GmbH</b>'));
    $oMulticell->multiCell(170, 5, utf8_decode('Würmtalstraße 20a <font>A</font> 81375 München'));
    $pdf->SetMargins(20, 20, 20);

    $intY = $pdf->GetY() + 7;
    $intHeight = $intY;

    /*
$pdf->SetTextColor(255, 0, 0);
$pdf->Text(120, $intHeight, utf8_decode('Wir sind umgezogen!'));
$pdf->SetTextColor(0, 0, 0);
*/

    $pdf->Text(20, $intHeight, utf8_decode('Telefon:'));
    $pdf->Text(37, $intHeight, utf8_decode('089 122237770'));
    $intHeight = $intHeight + 5;
    $pdf->Text(20, $intHeight, utf8_decode('Fax:'));
    $pdf->Text(37, $intHeight, utf8_decode('089 122237779'));
    $intHeight = $intHeight + 5;
    $pdf->Text(20, $intHeight, utf8_decode('E-Mail:'));
    $pdf->Text(37, $intHeight, utf8_decode('infoservice@izs-institut.de'));

    $pdf->SetMargins(20, 20, 20);

    $intY = $pdf->GetY() + 27;
    $pdf->SetY($intY);
    $pdf->SetTextColor(115, 114, 108);
    $pdf->SetFont('OfficinaSans', '', 10);
    $text = '<sg><a href="http://www.izs.de/agb.html">Es gelten unsere Allgemeinen Geschäftsbedingungen.</a></sg>';
    $oMulticell->multiCell(170, 5, utf8_decode($text), 0, 'L');
    //$pdf->Write(5, utf8_decode($text), 'http://www.izs.de/agb.html');

    $pdf->SetTextColor(0, 0, 0);

    $intPages = ceil(count($arrEventAccountList) / $intAccPerPage);

    for ($intPage = 1; $intPage <= $intPages; $intPage++) {

      $pdf->AddPage();
      $pdf->SetAutoPageBreak(false);
      $pdf->Image('pdf/zertifikat_vorlage.png', 0, 0, 210, 297, 'PNG');
      $pdf->SetMargins(20, 20);

      $pdf->SetTextColor($strBraunR, $strBraunG, $strBraunB);
      $pdf->SetFont('OfficinaSans', 'B', 18);
      $pdf->Text(20, 30, utf8_decode('PRÜFZERTIFIKAT'));

      $pdf->SetTextColor(0, 0, 0);
      $pdf->SetFont('OfficinaSans', '', 18);
      $pdf->Text(65.5, 30, utf8_decode(str_pad($arrPeriod[0], 2, "0", STR_PAD_LEFT) . '.' . $arrPeriod[1]));


      $pdf->SetTextColor(134, 134, 134);
      $pdf->SetFont('OfficinaSans', '', 7);
      $pdf->Text(20, 40, utf8_decode('Geprüftes Unternehmen'));
      $pdf->SetTextColor(0, 0, 0);
      $pdf->SetFont('OfficinaSans', '', 10);
      $pdf->Text(20, 44, utf8_decode($strCompanyGroup));

      $pdf->SetTextColor(134, 134, 134);
      $pdf->SetFont('OfficinaSans', '', 7);
      $pdf->Text(20, 50, utf8_decode('Datum der Erstellung'));
      $pdf->SetTextColor(0, 0, 0);
      $pdf->SetFont('OfficinaSans', '', 10);
      $pdf->Text(20, 54, utf8_decode(date('d.m.Y (H:i \U\h\r)')));

      $pdf->SetTextColor(134, 134, 134);
      $pdf->SetFont('OfficinaSans', '', 7);
      $pdf->Text(20, 60, utf8_decode('Link zur Unternehmensseite bei IZS'));
      $pdf->SetY(63);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->SetFont('OfficinaSans', '', 10);

      if ($arrCompanyGroup[0]['Direktlink_zum_Konto__c'] != '') {
        $strLink = 'http://www.izs.de/' . $arrCompanyGroup[0]['Direktlink_zum_Konto__c'];
      } else {
        $strLink = 'http://www.izs.de/unternehmensinfo.html?companyid=' . $_REQUEST['Id'];
      }
      $oMulticell->multiCell(170, 0, '<a href="' . $strLink . '">' . str_replace('http://', '', $strLink) . '</a>');

      $pdf->SetY(80);

      $pdf->SetTextColor(115, 114, 108);
      $pdf->SetFont('OfficinaSans', '', 8.8);
      $text = '<ss>Durch Klick auf den Namen des Unternehmens können Sie direkt dessen Online-Konto einsehen. Um den detaillierten Monatsbericht ';
      $text .= 'zu einem Unternehmen abzurufen, klicken Sie einfach auf "PDF abrufen". Je nach Umfang des Berichts kann dessen Erstellung bis zu ';
      $text .= 'einer Minute dauern. In beiden Fällen ist das Bestehen einer Internetverbindung Voraussetzung für die beschriebene Funktionalität.</ss>';
      $oMulticell->multiCell(170, 3.7, utf8_decode($text), 0, 'J');
      //$pdf->Write(3.7, utf8_decode($text));

      $pdf->SetDrawColor($strBraunR, $strBraunG, $strBraunB);
      $pdf->SetLineWidth(0.1);
      $intHeight = 105; //140;
      $pdf->Line(20, $intHeight, 130, $intHeight);
      $pdf->Line(135, $intHeight, 160, $intHeight);
      $pdf->Line(165, $intHeight, 190, $intHeight);

      $intHeight = 109.5; //144.5;
      $pdf->SetTextColor($strBraunR, $strBraunG, $strBraunB);
      $pdf->SetFont('OfficinaSans', '', 10);
      $pdf->Text(20.5, $intHeight, utf8_decode('Betriebsstätte'));
      $pdf->Text(135.5, $intHeight, utf8_decode('Betriebsnummer'));
      $pdf->Text(165.5, $intHeight, utf8_decode('Detailbericht'));

      $pdf->SetLineWidth(0.5);
      $intHeight = 111.5; //146.5;
      $pdf->Line(20, $intHeight, 130, $intHeight);
      $pdf->Line(135, $intHeight, 160, $intHeight);
      $pdf->Line(165, $intHeight, 190, $intHeight);

      $intStart = ($intPage - 1) * $intAccPerPage;

      $arrEventAccountList = array_values($arrEventAccountList);

      for ($intCount = ($intStart + 1); $intCount <= count($arrEventAccountList); $intCount++) {

        $arrAccount = $arrEventAccountList[$intCount - 1];

        $intFactor = $intCount;
        if ($intFactor > $intAccPerPage) {
          $intFactor = $intFactor % $intAccPerPage;
          if ($intFactor == 0) {
            $intFactor = $intAccPerPage;
          }
        }

        $intHeight = 104 + ($intFactor * 9); // 149

        if ($arrAccount['SF42_Payment_Status__c'] == 'Yellow') {
          $pdf->Image('../assets/images/sys/ampel_gelb.png', 21, $intHeight, 3.5, 5, 'PNG');
        } elseif ($arrAccount['SF42_Payment_Status__c'] == 'Red') {
          $pdf->Image('../assets/images/sys/ampel_rot.png', 21, $intHeight, 3.5, 5, 'PNG');
        } elseif ($arrAccount['SF42_Payment_Status__c'] == 'Green') {
          $pdf->Image('../assets/images/sys/ampel_gruen.png', 21, $intHeight, 3.5, 5, 'PNG');
        } else {
          $arrAccount['SF42_Payment_Status__c'] = 'gray';
          $pdf->Image('../assets/images/sys/ampel_grau.png', 21, $intHeight, 3.5, 5, 'PNG');
        }

        $pdf->SetMargins(26, 20);
        $intHeight = 106.5 + ($intFactor * 9);  // 151.5 

        $pdf->SetY($intHeight);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('OfficinaSans', '', 10);
        $strLink = 'http://www.izs.de/kontoauszug.html?companyid=' . $_REQUEST['Id'] . '&accountid=' . $arrAccount['Id'] . '&selectedMonth=' . $arrPeriod[0] . '&selectedYear=' . $arrPeriod[1] . '';
        $pdf->Write(0, utf8_decode($arrAccount['Name']), $strLink);


        //WRITE STATUS
        $strSql = 'DELETE FROM `izs_infoservice_status` WHERE `is_group_id` = "' . $_REQUEST['Id'] . '" AND `is_year` = "' . $arrPeriod[1] . '" AND `is_month` = "' . $arrPeriod[0]  . '" AND `is_account_id` = "' .MySQLStatic::esc($arrAccount['Id']) .'"';
        $arrSql = MySQLStatic::Query($strSql);
        
        $strSql = 'INSERT INTO `izs_infoservice_status` (`is_id`, `is_group_id`, `is_account_id`, `is_year`, `is_month`, `is_status`, `is_desc`) ';
        $strSql.= 'VALUES (NULL, "' .MySQLStatic::esc($_REQUEST['Id']) .'", "' .MySQLStatic::esc($arrAccount['Id']) .'", "' .MySQLStatic::esc($arrPeriod[1]) .'", "' .MySQLStatic::esc($arrPeriod[0]) .'", ';
        $strSql.= '"' .MySQLStatic::esc(strtolower($arrAccount['SF42_Payment_Status__c'])) .'", "' .MySQLStatic::esc($arrAccount['Subline__c']) .'");';
        $intSql = MySQLStatic::Insert($strSql);
        

        $pdf->SetLineWidth(0.1);
        $pdf->SetDash(0.1, 0.7);
        $intHeight = 111 + ($intFactor * 9); // 156
        $pdf->Line(20, $intHeight, 130, $intHeight);
        $pdf->Line(135, $intHeight, 160, $intHeight);
        $pdf->Line(165, $intHeight, 190, $intHeight);

        $pdf->SetMargins(20, 20, 20);

        $pdf->SetX(140);
        $pdf->Write(0, utf8_decode($arrAccount['betriebsnr']));
        $pdf->SetX(164.6);
        $pdf->SetTextColor(115, 114, 108);
        $strLink = 'http://www.izs.de/monatsreport.php?detailId=' . $arrAccount['Id'] . '&selectedMonth=' . $arrPeriod[0] . '&selectedYear=' . $arrPeriod[1] . '';
        $pdf->Write(0, utf8_decode('PDF abrufen'), $strLink);

        if ((($intCount % $intAccPerPage) == 0) && ($intCount < count($arrEventAccountList))) {
          $intCount--;
          break;
        }
      }
    }
  }
}


$strFileName = '../assets/pdf-zertifikate/' . $_REQUEST['Id'] . '_' . $arrPeriod[1] . '_' . $arrPeriod[0]  . '_zertifikat_sv.pdf';
if (file_exists($strFileName)) {
  unlink($strFileName);
}
$pdf->Output($strFileName, 'F');

$strSql = 'SELECT `cc_id` FROM `cms_cert` WHERE `cc_group_id` = "' . $_REQUEST['Id'] . '" AND `cc_month` = "' . $arrPeriod[0] . '" AND `cc_year` = "' . $arrPeriod[1] . '"';
$arrCert = MySQLStatic::Query($strSql);

if (count($arrCert) > 0) {
  $strSql = 'UPDATE `cms_cert` SET `cc_created` = NOW() WHERE `cc_id` = "' . $arrCert[0]['cc_id'] . '"';
  MySQLStatic::Update($strSql);
} else {
  $strSql = 'INSERT INTO `cms_cert` (`cc_group_id`, `cc_month`, `cc_year`, `cc_created`) VALUES ("' . $_REQUEST['Id'] . '", "' . $arrPeriod[0] . '", "' . $arrPeriod[1] . '", NOW())';
  MySQLStatic::Insert($strSql);
}

//header('Location: /cms/index.php?ac=cert&send=1&strSelDate=' . $arrPeriod[0] . '_' . $arrPeriod[1]);
