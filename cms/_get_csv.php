<?php

session_start();

require_once('../assets/classes/class.mysql.php');

//GLOBAL SETTINGS
error_reporting(E_ALL);
ini_set("display_errors", 1);

function str_split_unicode($str, $l = 0) {
  if ($l > 0) {
      $ret = array();
      $len = mb_strlen($str, "UTF-8");
      for ($i = 0; $i < $len; $i += $l) {
          $ret[] = mb_substr($str, $i, $l, "UTF-8");
      }
      return $ret;
  }
  return preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
}

function clean_for_word ($strInput = '') {
  
  $strReturn = '';
  
  if ($strInput != '') {
    $arrCharInput = str_split_unicode($strInput);
    foreach ($arrCharInput as $intKey => $strChar) {
      if (ord($strChar) != 194) $strReturn.= $strChar;
    }
  }

  $strReturn = htmlspecialchars($strReturn);

  return $strReturn;

}


if (@$_SERVER['HTTP_FKLD'] == 'on') {
  //echo $_REQUEST['strTable']; die();
}

$_REQUEST['strTable'] = str_replace('&', '&amp;', $_REQUEST['strTable']);

$_REQUEST['strTable'] = preg_replace("/<span class=\"hidden\">(.*?)<\/span>/i", '', $_REQUEST['strTable']);  
$_REQUEST['strTable'] = preg_replace("/<span class=\"hidden\"[^>]+\>(.*?)<\/span>/i", '', $_REQUEST['strTable']);
$_REQUEST['strTable'] = preg_replace("/<span class='hidden'>([^<>]+)<\/span>/i", '', $_REQUEST['strTable']);  

$_REQUEST['strTable'] = preg_replace("/<span class=\"hidden keep\">(.*?)<\/span>/i", '$1', $_REQUEST['strTable']);  

if (@$_SERVER['HTTP_FKLD'] == 'on') {
  //echo $_REQUEST['strTable']; die();
}


$objDom = new DOMDocument();  
$objDom->loadHTML($_REQUEST['strTable']);  
$objDom->preserveWhiteSpace = false; 

$objTables = $objDom->getElementsByTagName('table');
$objRows   = $objTables->item(0)->getElementsByTagName('tr');   
$objCols   = $objRows->item(0)->getElementsByTagName('th');  

//echo '|' .$objDom->saveHTML($objCols); die();

$arrHeader = NULL;

foreach ($objCols as $objNode) {
  if ($objNode->nodeValue == 'ID') {
    $arrHeader[] = 'DB-ID';
  } else {
    if (isset($_REQUEST['encHd']) && ($_REQUEST['encHd'] == 1)) {
      $arrHeader[] = utf8_decode($objNode->nodeValue);
    } else {
      $arrHeader[] = $objNode->nodeValue;
    }
  }
} 

$arrTable = array();

if (count($arrHeader) > 0) {
  $arrTable[0] = $arrHeader;
}

$objRows = $objTables->item(0)->getElementsByTagName('tr');

foreach ($objRows as $objRow) {
  
  $objCols = $objRow->getElementsByTagName('td');
     
  $arrRow = array();
  $intRow = 0;
  
  foreach ($objCols as $objNode) {
    $strCont = $objNode->nodeValue;
    $strHtml = $objDom->saveHTML($objNode);

    //echo $strHtml .chr(10);

    $objDomSub = new DOMDocument();  
    $objDomSub->loadHTML($strHtml);
    $objSelect = $objDomSub->getElementsByTagName('select'); 
    
    if ($objSelect->length > 0) {
      $strCont = preg_replace('/<select\s*.*>(\s*.*)<\/select>/i', '$1', $strHtml);
      $strCont = preg_replace('/<td\s*.*>(\s*.*)<\/td>/i', '$1', $strCont);
    }

    //echo $strCont .chr(10);

    $objInput = $objDomSub->getElementsByTagName('input'); 
    if ($objInput->length > 0) {
      $strCont = '';
      foreach ($objInput as $objInput)  {
        //echo '|' .$objInput->getAttribute('csvchecked') .'|' .chr(10); die();
        if ($objInput->getAttribute('csvchecked') == 'true') {
          $strCont = $objInput->getAttribute('value');
        }  
      }
    }
    
    //if ($strCont != '') { echo $strCont .chr(10); var_dump($objNode); } 

    if (strstr($strCont, 'http') === false) {
    
      $intMatch = preg_match_all('/[a-z][A-Z][a-z]/', $strCont, $arrMatch);
      
      if ($intMatch > 1) {
        $strPattern = '/([a-z])([A-Z])/';
        $strReplace = '${1}|${2}';
        $strCont    = preg_replace($strPattern, $strReplace, $strCont);
        $arrOptions = explode('|', $strCont);

        //echo $arrOptions[0] .chr(10);

        if (preg_match_all('/' .preg_quote($arrOptions[0]) .'/', $strCont, $arrMatch) > 1) {
          $strCont = $arrOptions[0];
        } else {
          $strCont = '';
        }     
        
      }

    }

    $strCont = utf8_decode($strCont);
    $strCont = str_replace('&amp;', '&', $strCont);
    $strCont = str_replace(' EUR', ' €', $strCont);
    
    if ($arrHeader == NULL) {
      $arrRow[] = trim($strCont);
    } else {
      $arrRow[$intRow] = trim($strCont);
    }
    
    $intRow++;
    
  }

  //die();
  
  if (count($arrRow) > 0) {
    $arrTable[] = $arrRow;
  }
}

if (count($arrTable[0]) == 0) {
  $arrTable[0] = $arrHeader;
}


if (count($arrTable) > 0) {

  if (!isset($_REQUEST['format'])) $_REQUEST['format'] = ''; 

  if (($_REQUEST['format'] == 'csv') || ($_REQUEST['format'] == '')) {

    ///*
    header("HTTP/1.1 200 OK");
    header("Pragma: public");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false);
    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    //header('Content-Disposition: attachment; filename=Customers_Export.csv');
  
    //header('Content-type: application/ms-excel');
    header('Content-Disposition: attachment; filename=' .date('Ymd_His') .'_export_tabelle.csv');
    header("Content-Transfer-Encoding: binary");
  
    echo "\xEF\xBB\xBF"; // UTF-8 BOM
    //*/

    $resFile = fopen("php://output", "w");
  
  
    foreach($arrTable as $intRow => $arrRow) {
      fputcsv($resFile, $arrRow, ';', '"');
    }
    
    fclose($resFile);
  
    //print_r($arrTable);

  } elseif ($_REQUEST['format'] == 'xlsx-bg') {

    $arrRange = range('A', 'Z');
  
    $arrUrlStyle = array(
      'font' => array(
        'color' => array(
          'rgb' => '0000FF'
        ),
        'underline' => 'single'
      )
    );

    if ($arrHeader[0] == 'DB-ID') {
      $arrHeader[0] = 'ID';
    }

    require_once dirname(__FILE__) . '/phpexcel/Classes/PHPExcel.php';
    
    $objPHPExcel = new PHPExcel();

    $objPHPExcel->getProperties()->setCreator("Kerstin Mutzel")
      ->setLastModifiedBy("Kerstin Mutzel")
      ->setTitle("PHPExcel Document from IZS")
      ->setSubject("PHPExcel Document")
      ->setDescription("Document for PHPExcel, generated using PHP classes.")
      ->setKeywords("")
      ->setCategory("Database result file");
    
    //Write Header
    $intExcelRow = 1;
    foreach ($arrHeader as $intKey => $strHeadline) {
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue($arrRange[$intKey] .$intExcelRow, utf8_decode($strHeadline));
    }

    $objPHPExcel->getActiveSheet()->getStyle('A1:' .$arrRange[$intKey] .'1')->getFont()->setBold(true);

    foreach ($arrTable as $intLine => $arrExcelLine) {

      if ($intLine == 0) continue;
      
      $intExcelRow = $intLine + 1;

      foreach ($arrExcelLine as $intKey => $strValue) {

        $arrPart = explode('.', $strValue);
        if (((count($arrPart) > 1) && (end($arrPart) == 'pdf')) || (strstr($strValue, 'http://') !== false) || (strstr($strValue, 'https://') !== false)) {

          $strDownloadLink = $strValue;

          $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($intKey, $intExcelRow)->setValueExplicit('Download', \PHPExcel_Cell_DataType::TYPE_STRING2);
          $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($intKey, $intExcelRow)->getNumberFormat()->setFormatCode('@');
          $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($intKey, $intExcelRow)->applyFromArray($arrUrlStyle);
          $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($intKey, $intExcelRow)->getHyperlink()->setUrl($strDownloadLink);        
    
          //$objPHPExcel->setActiveSheetIndex(0)->setCellValue($arrRange[$intKey] .$intExcelRow, $strDownloadLink);
        
        } else {
          
          $objPHPExcel->setActiveSheetIndex(0)->setCellValue($arrRange[$intKey] .$intExcelRow, $strValue);
        
        }
        //->setCellValueExplicit('B' .$intExcelRow, $arrExcelLine['BNR'], PHPExcel_Cell_DataType::TYPE_STRING)
      }

      //AUTO WIDTH
      foreach(range('A', $arrRange[$intKey]) as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
      }


    }


    $objPHPExcel->getActiveSheet()->setTitle('IZS Export');
    $objPHPExcel->setActiveSheetIndex(0);

    //WRITE file
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="izs_export_' .date('dmY_His') .'.xlsx"');

    // Write file to the browser
    $objWriter->save('php://output');
    				
  } elseif ($_REQUEST['format'] == 'xlsx') {
  
    //echo $_REQUEST['format'];
    //print_r($arrTable);

    require_once dirname(__FILE__) . '/phpexcel/Classes/PHPExcel.php';
    
    $objPHPExcel = new PHPExcel();

    $objPHPExcel->getProperties()->setCreator("Kerstin Mutzel")
      ->setLastModifiedBy("Kerstin Mutzel")
      ->setTitle("PHPExcel Document from IZS")
      ->setSubject("PHPExcel Document")
      ->setDescription("Document for PHPExcel, generated using PHP classes.")
      ->setKeywords("")
      ->setCategory("Database result file");
    
    //Write Header
    $intExcelRow = 1;
    $objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue('A' .$intExcelRow, 'Mitgliedsnummer (BG)')
      ->setCellValue('B' .$intExcelRow, 'Meldestelle')
      ->setCellValue('C' .$intExcelRow, 'Straße / Meldestelle')
      ->setCellValue('D' .$intExcelRow, 'PLZ / Meldestelle')
      ->setCellValue('E' .$intExcelRow, 'Ort / Meldestelle')
      ->setCellValue('F' .$intExcelRow, 'Vollmacht');

    $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true);

    /* ---------------------- */

    foreach ($arrTable as $intLine => $arrExcelLine) {

      if ($intLine == 0) continue;
      
      $intExcelRow = $intLine + 1;

      $strSql = 'SELECT * FROM `izs_bg_event` WHERE `be_name` = "' .$arrExcelLine[0] .'"';
      $arrEvent = MySQLStatic::Query($strSql);

      $strSql = 'SELECT * FROM `Account` WHERE `Id` = "' .$arrEvent[0]['be_meldestelle'] .'"';
      $arrAccount = MySQLStatic::Query($strSql);

      $strSql = 'SELECT * FROM `Verbindung_Meldestelle_BG__c` WHERE `Meldestelle__c` = "' .$arrEvent[0]['be_meldestelle'] .'" AND `Berufsgenossenschaft__c` = "' .$arrEvent[0]['be_bg'] .'" AND `Aktiv__c` = "true"';
      $arrVerbindung = MySQLStatic::Query($strSql);


      //die();
      
      $arrGroup = MySQLStatic::Query($strSql);      
      
      $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' .$intExcelRow, $arrVerbindung[0]['Mitgliedsnummer_BG__c'])
        //->setCellValueExplicit('B' .$intExcelRow, $arrExcelLine['BNR'], PHPExcel_Cell_DataType::TYPE_STRING)
        ->setCellValue('B' .$intExcelRow, ($arrAccount[0]['Name']))
        ->setCellValue('C' .$intExcelRow, ($arrAccount[0]['BillingStreet']))
        ->setCellValue('D' .$intExcelRow, ($arrAccount[0]['BillingPostalCode']))
        ->setCellValue('E' .$intExcelRow, ($arrAccount[0]['BillingCity']));

      //AUTO WIDTH
      foreach(range('A','E') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
      }

      //http://www.izs-institut.de/dl.php?f=a083000000RtxPwAAJ_vollmacht_bg.pdf
      $strDownloadLink = 'https://www.izs-institut.de/dl.php?f=' .$arrAccount[0]['SF42_Company_Group__c'] .'_vollmacht_bg.pdf';

      $arrUrlStyle = array(
        'font' => array(
          'color' => array(
            'rgb' => '0000FF'
          ),
          'underline' => 'single'
        )
      );

      $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(5, $intExcelRow)->setValueExplicit('Download', \PHPExcel_Cell_DataType::TYPE_STRING2);
      $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(5, $intExcelRow)->getNumberFormat()->setFormatCode('@');
      $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(5, $intExcelRow)->applyFromArray($arrUrlStyle);
      $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(5, $intExcelRow)->getHyperlink()->setUrl($strDownloadLink);        

    }

    /* ---------------------- */

    $objPHPExcel->getActiveSheet()->setTitle('IZS Export');
    $objPHPExcel->setActiveSheetIndex(0);

    //WRITE file
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="izs_export_' .date('dmY_His') .'.xlsx"');

    // Write file to the browser
    $objWriter->save('php://output');
    				
  } elseif ($_REQUEST['format'] == 'docx') {

    require_once('vendor/phpoffice/phpword/bootstrap.php');

    $strTempPath = 'templates/Anfrage_BG.docx';

    $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($strTempPath);

    $templateProcessor->cloneRow('MNr', count($arrTable) - 1);

    $strBg = '';
    $boolBgOnly = true;

    foreach ($arrTable as $intLine => $arrWordLine) {

      if ($intLine == 0) continue;

      if ($strBg == '') {
        $strBg = $arrWordLine[2];
      } else if ($strBg != $arrWordLine[2]) {
        $boolBgOnly = false;
      }

      $strSql = 'SELECT `Mitgliedsnummer_BG__c` FROM `izs_bg_event` INNER JOIN `Verbindung_Meldestelle_BG__c` ON `be_bg` = `Berufsgenossenschaft__c` AND `be_meldestelle` = `Meldestelle__c` WHERE `be_name` = "' .MySQLStatic::esc($arrWordLine[0]) .'"';
      $resSql = MySQLStatic::Query($strSql);

      $templateProcessor->setValue('MNr#' .($intLine), clean_for_word($resSql[0]['Mitgliedsnummer_BG__c']));
      $templateProcessor->setValue('Bzahler#' .($intLine), clean_for_word($arrWordLine[3]));

    }

    if ($boolBgOnly === false) {
      $strBg = '';
    }

    $templateProcessor->setValue('BGname', clean_for_word($strBg));

    $strNewName = substr(md5(uniqid(mt_rand(), true)), 0, 8);
    
    $templateProcessor->saveAs($strNewName);
    
    $fp = fopen($strNewName, 'rb');
    // send the right headers
    header('Content-type: application/vnd.ms-word');
    header('Content-Disposition: attachment;Filename="BG_Anfrage_' .date('dmY_His') .'.docx"');
    header("Content-Length: " . filesize($strNewName));
    // dump the picture and stop the script
    fpassthru($fp);
    
    unlink($strNewName);
    
    exit;

    				
  }


}

?>