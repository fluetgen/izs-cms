<?php


session_start();
error_reporting(E_ALL);
ini_set("display_errors", 1);

ini_set("memory_limit", "-1");
set_time_limit(0);

if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
  define('APP_PATH', '/data/www/sites/test.izs.de/html/');
} else {
  define('APP_PATH', '/data/www/sites/www.izs.de/html/');
}

//CLASSES
require_once(APP_PATH .'cms/inc/refactor.inc.php');
require_once(APP_PATH .'assets/classes/class.mysql.php');

@$intUser = $_SESSION['id'];

/*
$arrTeam = array(
  //32 => 'Serap',
  '47' => 'Ecem',
  '42' => 'Seda',
  '44' => 'Karo', 
  '36' => 'Gordon'
);

$arrHeader = array(
  'apikey: e9d99940-1aed-414a-b836-b929fe97d093',
  'user: ' .$intUser,
  'Accept: application/json'
);

$arrRes = curl_get('http://api.izs-institut.de/api/sg/category', array(), array(), $arrHeader);
$arrCatList = json_decode($arrRes, true);


print_r($arrCatList); 
*/

$intDate = mktime(0, 0, 0, date('n') - 1, 1, date('Y'));
$intYear = date('Y', $intDate);
$intMonth = date('n', $intDate);

$intMissing  = 0;
$arrMissing  = array();
$arrAllFiles = array();   
$strOutput   = '';
$arrEmpty    = array();

$strSql = 'SELECT `evid`, `Id`, `Name`, `SF42_DocumentUrl__c`, `SF42_Year__c`, `SF42_Month__c`, `SF42_EventStatus__c` FROM `SF42_IZSEvent__c` WHERE 1 ';
$strSql.= 'AND `SF42_Year__c` = "' .$intYear .'" AND  `SF42_Month__c` = "' .$intMonth .'" ';
$strSql.= 'ORDER BY `SF42_Year__c`, `SF42_Month__c`, `evid`';

$arrSql = MySQLStatic::Query($strSql);

foreach ($arrSql as $intKey => $arrDocumentRaw) {

  if ($arrDocumentRaw['SF42_DocumentUrl__c'] != '') {
    
    $strFile = basename($arrDocumentRaw['SF42_DocumentUrl__c']);

    $boolExists = file_exists('server/php/files/' .$strFile);
    
    $arrAllFiles[$strFile] = $arrDocumentRaw;
  
    if ($boolExists === false) {

      if (file_exists('server/php/files/' .$arrDocumentRaw['Id'] .'.pdf')) {
        $strNewPath = 'https://www.izs.de/cms/server/php/files/' .$arrDocumentRaw['Id'] .'.pdf';
        $strSqlUpdate = 'UPDATE `SF42_IZSEvent__c` SET `SF42_DocumentUrl__c` = "' .$strNewPath .'" WHERE `evid` = "' .$arrDocumentRaw['evid'] .'"';
        $arrSqlUpdate = MySQLStatic::Query($strSqlUpdate);
      } else {
        $strLink = 'https://www.izs.de/cms/index.php?ac=sear&det=' .$arrDocumentRaw['evid'];
        $strOutput.= 'Name: ' .$arrDocumentRaw['Name'] .' (' .$arrDocumentRaw['Id'].') -> <ahref="' .$arrDocumentRaw['SF42_DocumentUrl__c'] .'" target="_blank">' .$arrDocumentRaw['SF42_DocumentUrl__c'] .'</a><br />' .chr(10);
        $strOutput.= 'CMS:  <a href="' .$strLink .'" target="_blank">' .$strLink .'</a><br /><br />' .chr(10) .chr(10);
        $arrMissing[] = $arrDocumentRaw;
        $intMissing++;
      }

    }

  } else {

    $arrEmpty[$arrDocumentRaw['Id']] = $arrDocumentRaw;

  }

  // https://www.izs.de/cms/server/php/files/919147.pdf
  // http://www.izs-institut.de/cms/server/php/files/a063000000UKbbrAAD.pdf
  // https://ssl-id1.de/izs-archiv.de/archive/a063000000UKc1xAAD.pdf

}

echo $strOutput;
die();

$arrNoDataset = array();

/*
foreach (glob('server/php/files/*.pdf') as $strFileName) {
  $strFileName = basename($strFileName);
  if (!isset($arrAllFiles[$strFileName])) {

    if (!isset($arrEmpty[pathinfo($strFileName)['filename']])) {
      $arrNoDataset[$strFileName] = 'https://www.izs.de/cms/server/php/files/' .$strFileName;
      //echo 'https://www.izs.de/cms/server/php/files/' .$strFileName;
    }

    rename('server/php/files/' .$strFileName, 'server/php/files/zuklaeren/' .$strFileName);
    
  }
}
*/

print_r($arrNoDataset);

die();

//echo $intMissing .' files are missing in filesystem:' .chr(10);
//echo $strOutput;

require_once dirname(__FILE__) . '/phpexcel/Classes/PHPExcel.php';
    
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Kerstin Mutzel")
  ->setLastModifiedBy("Kerstin Mutzel")
  ->setTitle("PHPExcel Document from IZS")
  ->setSubject("PHPExcel Document")
  ->setDescription("Document for PHPExcel, generated using PHP classes.")
  ->setKeywords("")
  ->setCategory("Database result file");

//Write Header
$intExcelRow = 1;
$objPHPExcel->setActiveSheetIndex(0)
  ->setCellValue('A' .$intExcelRow, 'Jahr')
  ->setCellValue('B' .$intExcelRow, 'Monat')
  ->setCellValue('C' .$intExcelRow, 'Name')
  ->setCellValue('D' .$intExcelRow, 'Url')
  ->setCellValue('E' .$intExcelRow, 'Status')
  ->setCellValue('F' .$intExcelRow, 'Id');

$objPHPExcel->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true);

/* ---------------------- */

foreach ($arrMissing as $intLine => $arrExcelLine) {

  if ($intLine == 0) continue;
  
  $intExcelRow = $intLine + 1;
  
  $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A' .$intExcelRow, $arrExcelLine['SF42_Year__c'])
    //->setCellValueExplicit('B' .$intExcelRow, $arrExcelLine['BNR'], PHPExcel_Cell_DataType::TYPE_STRING)
    ->setCellValue('B' .$intExcelRow, ($arrExcelLine['SF42_Month__c']))
    ->setCellValue('C' .$intExcelRow, ($arrExcelLine['Name']))
    ->setCellValue('D' .$intExcelRow, ($arrExcelLine['SF42_DocumentUrl__c']))
    ->setCellValue('E' .$intExcelRow, ($arrExcelLine['Id']))
    ->setCellValue('F' .$intExcelRow, ($arrExcelLine['SF42_EventStatus__c']));

  //AUTO WIDTH
  foreach(range('A','E') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
  }

  /*
  //http://www.izs-institut.de/dl.php?f=a083000000RtxPwAAJ_vollmacht_bg.pdf
  $strDownloadLink = 'https://www.izs-institut.de/dl.php?f=' .$arrAccount[0]['SF42_Company_Group__c'] .'_vollmacht_bg.pdf';

  $arrUrlStyle = array(
    'font' => array(
      'color' => array(
        'rgb' => '0000FF'
      ),
      'underline' => 'single'
    )
  );

  $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(5, $intExcelRow)->setValueExplicit('Download', \PHPExcel_Cell_DataType::TYPE_STRING2);
  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(5, $intExcelRow)->getNumberFormat()->setFormatCode('@');
  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(5, $intExcelRow)->applyFromArray($arrUrlStyle);
  $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(5, $intExcelRow)->getHyperlink()->setUrl($strDownloadLink);
  */     

}

/* ---------------------- */

$objPHPExcel->getActiveSheet()->setTitle('IZS Export');
$objPHPExcel->setActiveSheetIndex(0);

//WRITE file
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="fehlen_im_filesystem_' .date('dmY_His') .'.xlsx"');

// Write file to the browser
$objWriter->save('php://output');

?>