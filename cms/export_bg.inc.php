<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

include('conf/global.inc.php');
require_once('../assets/classes/class.mysql.php');
require_once('../assets/classes/class.database.php');

require_once('functions.inc.php');
require_once('bootstrap.inc.php');

require_once('classes/class.informationprovider.php');

$boolShowIp = false;
$strOutput = '';

$strOutput.= '<h1>Export BG-Events</h1>' .chr(10);

if (true) {

// SUCHE BEGIN 

$strSql0 = "SELECT DISTINCT `be_jahr` FROM `izs_bg_event` ORDER BY `be_jahr` DESC";
$arrResult0 = MySQLStatic::Query($strSql0);

if (count($arrResult0) > 0) {
  
  $strOutput.= '<form method="post" action="">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);
  $strOutput.= '<input type="hidden" name="ac" value="' .$_REQUEST['ac'] .'">' .chr(10);
  
  $strOutput.= '<table>' .chr(10);
  $strOutput.= '<tbody>' .chr(10);

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Zeitraum: </td>' .chr(10);
  $strOutput.= '    <td><table><tr><td><select name="strSelDateFrom" id="strSelDateFrom">' .chr(10);
  
  $intPeriod = 0;

  foreach ($arrResult0 as $arrPeriod0) {
    $strSelected = '';
    $strValue = $arrPeriod0['be_jahr'];

    if (empty($_REQUEST['strSelDateFrom']) && ($intPeriod == 0)) {
      $_REQUEST['strSelDateFrom'] = $strValue;
      //$_REQUEST['send'] = 1;
    }
    
    if ($strValue == $_REQUEST['strSelDateFrom']) {
      $strSelected = ' selected="selected"';
    } 
    $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .$arrPeriod0['be_jahr'] .'</option>' .chr(10);
    $intPeriod++;
  }
  
  $strOutput.= '    </select></td>' .chr(10);

  $strOutput.= '    <td style="padding: 5px 10px;"> bis: </td>' .chr(10);


  $strOutput.= '    <td><select name="strSelDateTo" id="strSelDateTo">' .chr(10);
  
  $intPeriod = 0;

  foreach ($arrResult0 as $arrPeriod0) {
    $strSelected = '';
    $strValue = $arrPeriod0['be_jahr'];

    if (empty($_REQUEST['strSelDateTo']) && ($intPeriod == 0)) {
      $_REQUEST['strSelDateTo'] = $strValue;
      //$_REQUEST['send'] = 1;
    }
    
    if ($strValue == $_REQUEST['strSelDateTo']) {
      $strSelected = ' selected="selected"';
    } 
    $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .$arrPeriod0['be_jahr'] .'</option>' .chr(10);
    $intPeriod++;
  }
  
  $strOutput.= '    </select></td></tr></table></td>' .chr(10);

  $strOutput.= '  </tr>' .chr(10);
  
  $strSql6 = 'SELECT DISTINCT `Id`, `Name` FROM `izs_bg_event` INNER JOIN `Account` ON `be_bg` = `Id` ORDER BY `Name`';
  
  $strIpSelected = '';
  
  $arrResult6 = MySQLStatic::Query($strSql6);
  if (count($arrResult6) > 0) {

    if ($_REQUEST['strSelInf'] == 'all') {
      $strSelectedAll = ' selected="selected"';
    } else {
      $strSelectedAll = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Berufsgenossenschaft: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelInf" id="strSelInf">' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Berufsgenossenschaften -</option>' .chr(10);
    
    foreach ($arrResult6 as $arrProvider) {
      $strSelected = '';
      if ($arrProvider['Id'] == $_REQUEST['strSelInf']) {
        $strSelected = ' selected="selected"';
        $strIpSelected = $arrProvider['Name'];
      } 
      $strOutput.= '    <option' .$strOptClass .' value="' .$arrProvider['Id'] .'"' .$strSelected .'>' .$arrProvider['Name'] .'</option>' .chr(10);
      if ($_REQUEST['conly'] != 1) {
        $strOutput.= $strAddDez;
      }
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
  
  $strSql6 = 'SELECT DISTINCT `Group__c`.`Id`, `Group__c`.`Name` FROM `izs_bg_event` INNER JOIN `Account` ON `be_meldestelle` = `Account`.`Id` ';
  $strSql6.= 'INNER JOIN `Group__c` ON `Account`.`SF42_Company_Group__c` = `Group__c`.`Id` ORDER BY `Group__c`.`Name`';
  
  $strIpSelected = '';
  
  $arrResult6 = MySQLStatic::Query($strSql6);
  if (count($arrResult6) > 0) {

    if ($_REQUEST['strSelInf'] == 'all') {
      $strSelectedAll = ' selected="selected"';
    } else {
      $strSelectedAll = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Company Group: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelGrp" id="strSelGrp">' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Company Groups -</option>' .chr(10);
    
    foreach ($arrResult6 as $arrGroup) {
      $strSelected = '';
      if ($arrGroup['Id'] == $_REQUEST['strSelGrp']) {
        $strSelected = ' selected="selected"';
        $strIpSelected = $arrGroup['Name'];
      } 
      $strOutput.= '    <option' .$strOptClass .' value="' .$arrGroup['Id'] .'"' .$strSelected .'>' .$arrGroup['Name'] .'</option>' .chr(10);
      if ($_REQUEST['conly'] != 1) {
        $strOutput.= $strAddDez;
      }
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
  


    $strSql6 = 'SELECT DISTINCT `Id`, `Name` FROM `izs_bg_event` INNER JOIN `Account` ON `be_meldestelle` = `Id` ORDER BY `Name` ASC';
    
    $arrResult6 = MySQLStatic::Query($strSql6);
    if (count($arrResult6) > 0) {
  
      if ($_REQUEST['strSelPay'] == 'all') {
        $strSelectedAll = ' selected="selected"';
      } else {
        $strSelectedAll = '';
      }
  
      $strOutput.= '  <tr>' .chr(10);
      $strOutput.= '    <td>Melsdestelle: </td>' .chr(10);
      $strOutput.= '    <td><select name="strSelPay" id="strSelPay">' .chr(10);
      $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Meldestellen -</option>' .chr(10);
      
      foreach ($arrResult6 as $intKey => $arrProvider) {
        $strSelected = '';
        if ($arrProvider['Id'] == $_REQUEST['strSelPay']) {
          $strSelected = ' selected="selected"';
        } 
        
        if (($strLastName == $arrProvider['Name']) || ($arrResult6[$intKey + 1]['Name'] == $arrProvider['Name'])) {
           $strAdd = ' (' .$arrProvider['Btnr'] .')';
        } else {
          $strAdd = '';
        }
        
        $strOutput.= '    <option value="' .$arrProvider['Id'] .'"' .$strSelected .'>' .$arrProvider['Name'] .'' .$strAdd .'</option>' .chr(10);
        $strLastName = $arrProvider['Name'];
        
      }
    
      $strOutput.= '    </select></td>' .chr(10);
      $strOutput.= '  </tr>' .chr(10);
    }
  

    $strSql7 = 'SELECT COUNT(*) AS `Datensätze`, `be_status_bearbeitung` FROM `izs_bg_event` GROUP BY `be_status_bearbeitung` ORDER BY `izs_bg_event`.`be_status_bearbeitung` ASC';
    
    $arrResult7 = MySQLStatic::Query($strSql7);
    if (count($arrResult7) > 0) {
  
      if ($_REQUEST['strSelBeSt'] == 'all') {
        $strSelectedAll = ' selected="selected"';
      } else {
        $strSelectedAll = '';
      }
  
      $strOutput.= '  <tr>' .chr(10);
      $strOutput.= '    <td>Bearbeitungsstatus: </td>' .chr(10);
      $strOutput.= '    <td><select name="strSelBeSt" id="strSelBeSt">' .chr(10);
      $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Status -</option>' .chr(10);
      
      foreach ($arrResult7 as $intKey => $arrStatus) {
        $strSelected = '';
        if ($arrStatus['be_status_bearbeitung'] == $_REQUEST['strSelBeSt']) {
          $strSelected = ' selected="selected"';
        } 
        
        $strOutput.= '    <option value="' .$arrStatus['be_status_bearbeitung'] .'"' .$strSelected .'>' .$arrStatus['be_status_bearbeitung'] .'</option>' .chr(10);
        
      }
    
      $strOutput.= '    </select></td>' .chr(10);
      $strOutput.= '  </tr>' .chr(10);
    }

    $strSql8 = 'SELECT COUNT(*) AS `Datensätze`, IF(`be_klaerung_status` = "", "kein Status", `be_klaerung_status`) AS `be_klaerung_status` FROM `izs_bg_event` GROUP BY `be_klaerung_status` ORDER BY `be_klaerung_status`';
    $arrResult8 = MySQLStatic::Query($strSql8);
    if (count($arrResult8) > 0) {
  
      if ($_REQUEST['strSelKlSt'] == 'all') {
        $strSelectedAll = ' selected="selected"';
      } else {
        $strSelectedAll = '';
      }
  
      $strOutput.= '  <tr>' .chr(10);
      $strOutput.= '    <td>Status Klärung: </td>' .chr(10);
      $strOutput.= '    <td><select name="strSelKlSt" id="strSelKlSt">' .chr(10);
      $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Status -</option>' .chr(10);
      
      foreach ($arrResult8 as $intKey => $arrStatus) {
        $strSelected = '';
        if ($arrStatus['be_klaerung_status'] == $_REQUEST['strSelKlSt']) {
          $strSelected = ' selected="selected"';
        } 
        
        $strOutput.= '    <option value="' .$arrStatus['be_klaerung_status'] .'"' .$strSelected .'>' .$arrStatus['be_klaerung_status'] .'</option>' .chr(10);
        
      }
    
      $strOutput.= '    </select></td>' .chr(10);
      $strOutput.= '  </tr>' .chr(10);
    }  

  $strOutput.= '</tbody>' .chr(10);

  $strOutput.= '  <tfoot>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td align="right" colspan="2"><input type="submit" name="Suche" value="Suche" /></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);
  $strOutput.= '  </tfoot>' .chr(10);

  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);
    
}

if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {
  
  $strSql = 'SELECT `izs_bg_event`.*, `izs_bg`.`Name` AS `BG`, `Account`.`Name` AS `Account`, `Group__c`.`Name` AS `Group` FROM `izs_bg_event` ';

  $strSql.= 'INNER JOIN `izs_bg` ON `be_bg` = `izs_bg`.`Id` ';
  $strSql.= 'INNER JOIN `Account` ON `be_meldestelle` = `Account`.`Id` ';
  $strSql.= 'INNER JOIN `Group__c` ON `Account`.`SF42_Company_Group__c` = `Group__c`.`Id` ';
  
  
  $strSql.= 'WHERE `be_jahr` >= "' .MySQLStatic::esc($_REQUEST['strSelDateFrom']) .'" AND `be_jahr` <= "' .MySQLStatic::esc($_REQUEST['strSelDateTo']) .'" ';

  if ((isset($_REQUEST['strSelInf'])) && ($_REQUEST['strSelInf'] != 'all')) {
    $strSql.= 'AND `be_bg` = "' .MySQLStatic::esc($_REQUEST['strSelInf']) .'" ';
  }

  if ((isset($_REQUEST['strSelPay'])) && ($_REQUEST['strSelPay'] != 'all')) {
    $strSql.= 'AND `be_meldestelle` = "' .MySQLStatic::esc($_REQUEST['strSelPay']) .'" ';
  }
  
  if ((isset($_REQUEST['strSelGrp'])) && ($_REQUEST['strSelGrp'] != 'all')) {
    $strSql.= 'AND `Group__c`.`Id` = "' .MySQLStatic::esc($_REQUEST['strSelGrp']) .'" ';
  }
  
  if ((isset($_REQUEST['strSelBeSt'])) && ($_REQUEST['strSelBeSt'] != 'all')) {
    $strSql.= 'AND `be_status_bearbeitung` = "' .MySQLStatic::esc($_REQUEST['strSelBeSt']) .'" ';
  }
  
  if ((isset($_REQUEST['strSelKlSt'])) && ($_REQUEST['strSelKlSt'] != 'all')) {

    $strStatus = '';
    if ($_REQUEST['strSelKlSt'] != 'kein Status') {
      $strStatus = $_REQUEST['strSelKlSt'];
    }

    $strSql.= 'AND `be_meldestelle` = "' .MySQLStatic::esc($strStatus) .'" ';
  }

  $arrInformationProvider = MySQLStatic::Query($strSql);
  
  //echo $strSql;
  //print_r($arrInformationProvider);
  //print_r($arrDecentral);
  
  $strExportTable = '';
  
  if (count($arrInformationProvider) > 0) {
    //natcasesort($arrInformationProvider);

    $strOutputTable = '';
    $strOutputTable.= '<table class="" id="5">' .chr(10);
    $strOutputTable.= '  <thead>' .chr(10);
    $strOutputTable.= '    <tr>' .chr(10);
  	//$strOutputTable.= '      <th class="{sorter: false}">Status</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header">ID</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="header">Jahr</th>' .chr(10);
  	$strOutputTable.= '      <th class="header">Berufsgenossenschaft</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="header">Unternehmensgruppe</th>' .chr(10);
    $strOutputTable.= '      <th class="header">Meldestelle</th>' .chr(10);
  	$strOutputTable.= '      <th class="header">Status Bearbeitung</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header">Ergebnis</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="header">Dokument</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="header">Status Klärung</th>' .chr(10); //class="header"

    $strOutputTable.= '    </tr>' .chr(10);
    $strOutputTable.= '  </thead>' .chr(10);
    $strOutputTable.= '  <tbody>' .chr(10);
    
    $strExportTable.= $strOutputTable;
    
    //print_r($arrInformationProvider);

    foreach ($arrInformationProvider as $strId => $arrEvent) {

      $strOutputTable.= '    <tr>' .chr(10);
      $strExportTable.= '    <tr>' .chr(10);

      $strOutputTable.= '      <td>' .$arrEvent['be_name'] .'</a></td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['be_name'] .'</td>' .chr(10);

      $strOutputTable.= '      <td>' .$arrEvent['be_jahr'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['be_jahr'] .'</td>' .chr(10);
      
      $strOutputTable.= '      <td>' .$arrEvent['BG'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['BG'] .'</td>' .chr(10);
      
      $strOutputTable.= '      <td>' .$arrEvent['Group'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['Group'] .'</td>' .chr(10);
      
      $strOutputTable.= '      <td>' .$arrEvent['Account'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['Account'] .'</td>' .chr(10);
      
      $strOutputTable.= '      <td>' .$arrEvent['be_status_bearbeitung'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['be_status_bearbeitung'] .'</td>' .chr(10);
      
      $strOutputTable.= '      <td>' .$arrEvent['be_ergebnis'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['be_ergebnis'] .'</td>' .chr(10);

      $strUrl = '';
      if ($arrEvent['be_link'] != '') {
        $strUrl = 'https://www.izs.de/cms/pdf/bg/' .$arrEvent['be_link'];
        $strOutputTable.= '      <td><a href="' .$strUrl .'" target="_blank">Download</a></td>' .chr(10);
      } else {
        $strOutputTable.= '      <td></td>' .chr(10);
      }
      $strExportTable.= '      <td>' .$strUrl .'</td>' .chr(10);
            
      $strOutputTable.= '      <td>' .$arrEvent['be_klaerung_status'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['be_klaerung_status'] .'</td>' .chr(10);
      
      $strOutputTable.= '    </tr>' .chr(10);
      $strExportTable.= '    </tr>' .chr(10);
      
    }
      
    $strOutputTable.= '  </tbody>' .chr(10);
    $strOutputTable.= '</table>' .chr(10);

    $strExportTable.= '  </tbody>' .chr(10);
    $strExportTable.= '</table>' .chr(10);

if (@$_SERVER['HTTP_FKLD'] == 'on') {
  //echo $intCatchAllContinue;
}  
    
    $strOutput.= '<div class="clearfix" style="height: 48px; width: 1690px;">' .chr(10);
    $strOutput.= '  <div class="form-left">' .chr(10);
    $strOutput.= '    <h3>' .count($arrInformationProvider) .' Ergebnisse gefunden:</h3>' .chr(10);
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' .chr(10);
    $strOutput.= '    <form method="post" action="_get_csv.php">';
    $strOutput.= '    <input type="hidden" name="strTable" value="' .str_replace('"', "'", $strExportTable) .'">' .chr(10);
    $strOutput.= '    <input type="hidden" name="format" value="xlsx-bg">' .chr(10);
    $strOutput.= '    <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' .chr(10);
    $strOutput.= '    </form>';
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '</div>' .chr(10);
    
    $strOutput.= $strOutputTable;
    
    $strOutput.= "
<script>

$('#5').fixheadertable({ 
   colratio    : [100, 80, 320, 320, 320, 150, 80, 100, 150], 
   height      : 700, 
   width       : 1668, 
   zebra       : true, 
   resizeCol   : true,
   sortable    : true,
   sortType    : ['string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string'],
   sortedColId : '0', 
   dateFormat  : 'Y-m'
});
</script>
";
          
  } else {
    
    $strOutput.= '<p>Keine Events im gewählten Zeitraum vorhanden.</p>' .chr(10);
    
  }

}

$strOutput.= '';

}

?>