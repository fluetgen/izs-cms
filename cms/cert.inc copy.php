<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

require_once('../assets/classes/class.mysql.php');

$strOutput = '';
$arrCompanyGroupListPayed = array();
$arrCompanyGroupListNotPayed = array();
$arrAccountList = array();

$strOutput.= '<h1>Infoservice</h1>' .chr(10);

$strSql0 = "SELECT DISTINCT `Beitragsmonat__c`, `Beitragsjahr__c` FROM `SF42_IZSEvent__c` ORDER BY `Beitragsjahr__c` DESC, `Beitragsmonat__c` DESC";
$arrResult0 = MySQLStatic::Query($strSql0);
if (count($arrResult0) > 0) {
  
  $strOutput.= '<form method="post" action="">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);

  $strOutput.= '<table>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Zeitraum: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelDate" onchange="this.form.submit()">' .chr(10);
  
  $intPeriod = 0;

  foreach ($arrResult0 as $arrPeriod0) {
    $strSelected = '';
    $strValue = $arrPeriod0['Beitragsmonat__c'] .'_' .$arrPeriod0['Beitragsjahr__c'];
    
    if (empty($_REQUEST['strSelDate']) && ($intPeriod == 0)) {
      $_REQUEST['strSelDate'] = $strValue;
      $_REQUEST['send'] = 1;
      $arrPeriod = $strValue;
    }
    
    if ($strValue == $_REQUEST['strSelDate']) {
      $strSelected = ' selected="selected"';
    } 
    $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .str_pad($arrPeriod0['Beitragsmonat__c'], 2 ,'0', STR_PAD_LEFT) .'-' .$arrPeriod0['Beitragsjahr__c'] .'</option>' .chr(10);
    $intPeriod++;
  }
  
  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);


  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);
    
}

if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {
  
  $arrPeriod = explode('_', $_REQUEST['strSelDate']);
  
  $arrGroups = array();
  $arrInformationProvider = array();
  $arrNotPayedGroup = array();
  $arrNotPayedGroupRed = array();
  $arrNotPayedReason = array();

  $strSql2 = 'SELECT `evid`, `SF42_IZSEvent__c`.`Name` AS `EName`, `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c`, ';
  $strSql2.= '`SF42_IZSEvent__c`.`SF42_informationProvider__c`, `Acc`.`Id`, `Acc`.`Name` AS `PPname`, ';
  $strSql2.= '`SF42_IZSEvent__c`.`Betriebsnummer_ZA__c` AS `PPbtnr`, `Account`.`Dezentrale_Anfrage__c`, ';
  $strSql2.= '`Account`.`Name`, `Group__c`.`Id` AS `IdG`, `Group__c`.`Name` AS `NameG`, ';
  $strSql2.= '`Grund__c`, `Naechster_Meilenstein__c`, `bis_am__c`, `SF42_IZSEvent__c`.`Info__c`, `SF42_EventStatus__c` ';
  $strSql2.= 'FROM `SF42_IZSEvent__c` ';
  $strSql2.= 'INNER JOIN `Account` AS `Acc` ON `Acc`.`SF42_Comany_ID__c` = `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c` ';
  $strSql2.= 'INNER JOIN `Group__c` ON `Group__c`.`Id` = `Acc`.`SF42_Company_Group__c` ';
  $strSql2.= 'INNER JOIN `Account` ON `Account`.`Id` = `SF42_IZSEvent__c`.`SF42_informationProvider__c` ';
  $strSql2.= 'WHERE `SF42_Month__c` = "' .$arrPeriod[0] .'" AND `SF42_Year__c` = "' .$arrPeriod[1] .'" ';
  $strSql2.= 'AND `SF42_EventStatus__c` IN ("to enquire", "enquired", "no result", "not OK", "OK")';
  $arrResult2 = MySQLStatic::Query($strSql2);
  
  
  if (count($arrResult2) > 0) {

    foreach ($arrResult2 as $intEventKey => $arrEvent) {

      if ($arrEvent['IdG'] == 'a084S0000004iHSQAY') continue;
      
      if (!in_array($arrEvent['SF42_EventStatus__c'], array('OK')) ) {
        $arrNotPayedGroup[] = $arrEvent['IdG'];
        if ($arrEvent['SF42_EventStatus__c'] == 'not OK') {
          $arrNotPayedGroupRed[] = $arrEvent['IdG'];
        }
        $arrNotPayedReason[$arrEvent['IdG']][] = array(
          'Name' => $arrEvent['PPname'], 
          'Number' => $arrEvent['Betriebsnummer_ZA__c'], 
          'Reason' => $arrEvent['SF42_EventStatus__c']
        );
      }
      
      $arrCompanyGroupList[strtolower($arrEvent['NameG'])] = array(
        'Id' => $arrEvent['IdG'],
        'Name' => $arrEvent['NameG']
      );

    }
  }
      
  //print_r($arrNotPayedGroup);

  if (count($arrCompanyGroupList) > 0) {
    ksort($arrCompanyGroupList);
    
//    print_r($arrNotPayedGroup);
//    print_r($arrNotPayedReason);
//    print_r($arrCompanyGroupList); die();
    
    $strOutputTable = '';
    $strOutputTable.= '<table class="" id="5">' .chr(10);
    $strOutputTable.= '  <thead>' .chr(10);
    $strOutputTable.= '    <tr>' .chr(10);
  	$strOutputTable.= '      <th class="header">Company Group</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header">Status</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header">Zertifikat</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header" nowrap="nowrap">erzeugt am</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header">Infoservice-Mail</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header" nowrap="nowrap">versandt am</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header">Status</th>' .chr(10); //
    $strOutputTable.= '    </tr>' .chr(10);
    $strOutputTable.= '  </thead>' .chr(10);
    $strOutputTable.= '  <tbody>' .chr(10);

    
    $arrPeriod = explode('_', $_REQUEST['strSelDate']);

    $arrCreateList = array();
    $arrSendList   = array();
    
    foreach ($arrCompanyGroupList as $strKeyName => $arrGroup) {
      
      $strSql = 'SELECT `cc_id`, DATE_FORMAT(`cc_created`, "%d.%m.%Y %H:%i:%s") AS `cc_created`, DATE_FORMAT(`cc_sent`, "%d.%m.%Y %H:%i:%s") AS `cc_sent`, `cc_recipients` FROM `cms_cert` WHERE `cc_group_id` = "' .$arrGroup['Id'] .'" AND `cc_month` = "' .$arrPeriod[0] .'" AND `cc_year` = "' .$arrPeriod[1] .'"';
      $arrCert = MySQLStatic::Query($strSql);
       
      if (count($arrCert) == 0) {
        $arrCert[0]['cc_created'] = '';
        $arrCert[0]['cc_sent'] = '';
      }
      
      $strOutputTable.= '    <tr>' .chr(10);
      $strOutputTable.= '      <td>' .$arrGroup['Name'] .'</td>' .chr(10);
      
      if (!in_array($arrGroup['Id'], $arrNotPayedGroup) == true) {
        $strOutputTable.= '      <td align="left"><img src="/assets/images/sys/ampel_gruen.png" alt="vollständig" /></td>' .chr(10);
        $strOutputTable.= '      <td align="left"><a href="createZertifikat.php?Id=' .$arrGroup['Id'] .'&strSelDate=' .$_REQUEST['strSelDate'] .'">erzeugen</a></td>' .chr(10);
        $arrCreateList[] = 'createZertifikat.php?Id=' .$arrGroup['Id'] .'&strSelDate=' .$_REQUEST['strSelDate'];
      } elseif (in_array($arrGroup['Id'], $arrNotPayedGroupRed)) {
        $strOutputTable.= '      <td align="left"><img src="/assets/images/sys/ampel_rot.png" alt="Beitragsrückstand" title="' .print_r($arrNotPayedReason[$arrGroup['Id']], true) .'" /></td>' .chr(10);
        $strOutputTable.= '      <td></td>' .chr(10);
      } else {
        $strOutputTable.= '      <td align="left"><img src="/assets/images/sys/ampel_gelb.png" alt="in Bearbeitung" title="' .print_r($arrNotPayedReason[$arrGroup['Id']], true) .'" /></td>' .chr(10);
        $strOutputTable.= '      <td></td>' .chr(10);
      }
      
      if ($arrCert[0]['cc_created'] != '') {
        $strFileName = '../assets/pdf-zertifikate/' .$arrGroup['Id'] .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_zertifikat_sv.pdf';
        
        $arrDate = explode(' ', $arrCert[0]['cc_created']);
        $strOutputTable.= '      <td><a href="' .$strFileName .'" target="_blank" alt="' .$arrCert[0]['cc_created'] .'" title="' .$arrCert[0]['cc_created'] .'">' .$arrDate[0] .'</a></td>' .chr(10);
        
        $strSql = 'SELECT `E_Mail__c` FROM `Abonnent__c` WHERE `Verleiher_Gruppe__c` = "' .$arrGroup['Id'] .'" AND `aktiviert__c` = "true"';
        $arrAbo = MySQLStatic::Query($strSql);
        
        if (count($arrAbo) > 0) {        
          $strOutputTable.= '      <td><a href="sendZertifikat.php?Id=' .$arrGroup['Id'] .'&strSelDate=' .$_REQUEST['strSelDate'] .'">versenden (' .count($arrAbo) .')</a></td>' .chr(10);
          $arrSendList[] = 'sendZertifikat.php?Id=' .$arrGroup['Id'] .'&strSelDate=' .$_REQUEST['strSelDate'];
        } else {
          $strOutputTable.= '      <td>Kein Abonent</td>' .chr(10);
        }
      } else {
        $strOutputTable.= '      <td></td>' .chr(10);
        $strOutputTable.= '      <td></td>' .chr(10);
      }
      
      
      if (($arrCert[0]['cc_sent'] != '00.00.0000 00:00:00') && ($arrCert[0]['cc_sent'] != '')) {
        
        $arrDate = explode(' ', $arrCert[0]['cc_sent']);
        
        $strLink = 'https://www.izs.de/pruefzertifikat.php?Id=' .$arrGroup['Id'] .'&strSelDate=' .$_REQUEST['strSelDate'];

        $strOutputTable.= '      <td><a href="' .$strLink .'" target="_blank" alt="' .$arrCert[0]['cc_sent'] .'" title="' .$arrCert[0]['cc_sent'] .'">' .$arrDate[0] .'</td>' .chr(10);
        $strOutputTable.= '      <td><img src="/assets/images/sys/ampel_gruen.png" alt="vollständig (' .$arrCert[0]['cc_recipients'] .')" title="vollständig (' .$arrCert[0]['cc_recipients'] .')" /></td>' .chr(10);
      } elseif ($arrCert[0]['cc_created'] != '') {
        $strOutputTable.= '      <td></td>' .chr(10);
        $strOutputTable.= '      <td><img src="/assets/images/sys/ampel_gelb.png" alt="in Bearbeitung" /></td>' .chr(10);
      } else {
        $strOutputTable.= '      <td></td>' .chr(10);
        $strOutputTable.= '      <td></td>' .chr(10);
      }
      
      $strOutputTable.= '    </tr>' .chr(10);

      
    }
    
    $strOutputTable.= '  </tbody>' .chr(10);
    $strOutputTable.= '</table>' .chr(10);   
    
    $strOutput.= '<div class="clearfix" style="height: 48px; width: 926px;">' .chr(10);
    $strOutput.= '  <div class="form-left">' .chr(10);
    $strOutput.= '    <h3>' .count($arrCompanyGroupList) .' Ergebnisse gefunden:</h3>' .chr(10);
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' .chr(10);
    $strOutput.= '    <form method="post" action="_get_csv.php" style="display: inline; float: right; padding-left: 10px;">';
    $strOutput.= '    <input type="hidden" name="strTable" value="' .str_replace('"', "'", $strOutputTable) .'">' .chr(10);
    $strOutput.= '    <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' .chr(10);
    $strOutput.= '    </form>';
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '</div>' .chr(10);


    $strOutput.= $strOutputTable;
    

    $strOutput.= "
<script>
$('#5').fixheadertable({ 
   colratio    : [350, 80, 80, 80, 120, 80, 80], 
   height      : 500, 
   width       : 910, 
   zebra       : true, 
   sortable    : true,
   sortType    : ['string', 'string', 'string', 'date', 'string', 'date', 'string'],
   sortedColId : 0, 
   dateFormat  : 'd.m.Y'
});
</script>
";

$strOutput.= '<!-- ' .chr(10);
foreach ($arrCreateList as $intKey => $strValue) {
  $strOutput.= '' .$strValue .'' .chr(10);
}

$strOutput.= '' .chr(10);

foreach ($arrSendList as $intKey => $strValue) {
  $strOutput.= '' .$strValue .'' .chr(10);
}
$strOutput.= '-->' .chr(10);

  
  } else {
    
    $strOutput.= '<p>Keine Company Group im gewählten Zeitraum vorhanden.</p>' .chr(10);
    
  }

}

?>