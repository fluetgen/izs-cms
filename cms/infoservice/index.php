<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

require_once('../const.inc.php');
require_once('../../assets/classes/class.mysql.php');

// regular expression contains a whitelist of characters to allow in filenames
// suitable for NTFS and POSIX systems
// supports UTF8 letters, the use of `\p{L}` matches a single character from any language
// does not support utf8 symbols atm like arrows
function sanitiseFileName ($strName = '', $strReplace = '') {
    return preg_replace("/[^\p{L}0-9 .,;'~`!@#$%^&()\-_+=\[\]\{\}]/u", $strReplace, $strName);
}

$strRedirectUrl = @$_REQUEST['r'];

if (isset($strRedirectUrl) && ($strRedirectUrl != '')) {

    $arrPart = explode('/', $strRedirectUrl);

    //print_r($arrPart); die();

    // https://test.izs.de/cms/infoservice/a083000000LQSO0AAP/2022/6/bericht

    if (count($arrPart) == 3) { // 

        $strSql3 = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$arrPart[0] .'"';
        $arrGroup = MySQLStatic::Query($strSql3);

        $_REQUEST['strSelDate'] = $arrPart[2] .'_' .$arrPart[1];
        $_REQUEST['Id'] = $arrPart[0];

        if (($arrGroup[0]['Gesamtstatus__c'] == 'rot') || (($_REQUEST['strSelDate'] == '9_2021') && ($_REQUEST['Id'] == 'a084S0000004hpiQAA'))) { //Schwartpaul Sonderlocke!!!
            $strTemplatePath = '../templates/pruefzertifikat_mail_nocert.tpl.html';
        } else {
            $strTemplatePath = '../templates/pruefzertifikat_mail.tpl.html';
        }
        
        $strTemplate = file_get_contents($strTemplatePath);

        if (strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) {
            $strServer = 'test';
        } else {
            $strServer = 'www';
        }
        
        $boolMail = false;
        
        require_once('../parseTemplate.inc.php');
        
        header('Content-Type: text/html; charset=utf-8');

        if ($strServer == 'test') {
            $strTemplate = str_replace('www.', 'test.', $strTemplate);
        }

        echo $strTemplate;        

        /*
        if (strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) {

            $strUrl = 'https://test.izs-institut.de/api/company/' .$arrCompanyGroup[0]['Id'] .'/account/' .$_REQUEST['detailId'] .'/eventlist?month=' .$_REQUEST['selectedMonth'] .'&year=' .$_REQUEST['selectedYear'] .'';
        
            $arrAuth = array(
              'http' => array(
                  'header' => 'Authorization: Basic ' . base64_encode("izs:test")
              )
            );
          
            $resStream = stream_context_create($arrAuth);
            $resEventList = file_get_contents($strUrl, false, $resStream);
          
            $arrEventList = json_decode($resEventList, true);
        
          }
        */

    } elseif ((count($arrPart) == 4) && ($arrPart[3] == 'bericht')) {
        
        $strPdf = '';

        $strSql3 = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$arrPart[0] .'"';
        $arrGroup = MySQLStatic::Query($strSql3);

        //$strFileName = 'Monatsbericht ' .sanitiseFileName($arrGroup[0]['Name'] .' ' .str_pad($arrPart[2], 2, '0', STR_PAD_LEFT) .'-' .$arrPart[1]);
        $strDisplayName = '' .sanitiseFileName($arrGroup[0]['Name'] .' - SV Prüfbericht ' .$arrPart[1] .'-' .str_pad($arrPart[2], 2, '0', STR_PAD_LEFT) .'');
        $strFileName = $strDisplayName;

        require_once('monatsbericht.inc.php');

        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="' .$strFileName .'.pdf"');
        echo $strOutput;
        
        ;
    }

} else {

    http_response_code(404);
    //include('my_404.php'); // provide your own HTML for the error page
    die();

}

?>