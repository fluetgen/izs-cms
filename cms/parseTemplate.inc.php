<?PHP

$strSql = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$_REQUEST['Id'] .'"';
$arrCompanyGroup = MySQLStatic::Query($strSql);

$arrPeriod = explode('_', $_REQUEST['strSelDate']);
$strCompanyGroup = $arrCompanyGroup[0]['Name'];

$strTemplate = str_replace('{{CompanyGroup}}', $strCompanyGroup, $strTemplate);
$strTemplate = str_replace('{{Year}}', $arrPeriod[1], $strTemplate);
$strTemplate = str_replace('{{Month}}', str_pad($arrPeriod[0], 2, "0", STR_PAD_LEFT), $strTemplate);

$strTemplate = str_replace('{{PersonalAdress}}', '', $strTemplate);

if ($arrCompanyGroup[0]['Direktlink_zum_Konto__c'] != '') {
  $strLink = 'https://www.izs.de/' .$arrCompanyGroup[0]['Direktlink_zum_Konto__c'];
} else {
  $strLink = ' ';
}

$strTemplate = str_replace('{{CompanyUrl}}', $strLink, $strTemplate);
$strTemplate = str_replace('{{CompanyUrlDisplay}}', str_replace('http://', '', $strLink), $strTemplate);

$strLink = 'https://www.izs.de/assets/pdf-zertifikate/' .$_REQUEST['Id'] .'_' .$arrPeriod[1] .'_' .$arrPeriod[0] .'_zertifikat_sv.pdf';
$strTemplate = str_replace('{{PdfUrl}}', $strLink, $strTemplate);

$strLink = 'https://www.izs.de/pruefzertifikat.php?Id=' .$_REQUEST['Id'] .'&strSelDate=' .$_REQUEST['strSelDate'];
if ($boolMail == true) {
  $strText = 'Wird die Mail nicht korrekt angezeigt?<br><a href="' .$strLink .'" target="_blank" style="color: #202020;font-weight: normal;text-decoration: underline;">Klicken Sie hier um die Mail im Browser zu öffnen.</a>';
} else {
  $strTemplate = str_replace('<br /><br /> Falls Sie den Infoservice nicht mehr erhalten möchten,', '', $strTemplate);
  $strTemplate = str_replace('klicken Sie bitte <a href="{{Unsubscribe}}">hier</a>.', '', $strTemplate);
  $strText = '';
}
$strTemplate = str_replace('{{LinkBrowser}}', $strText, $strTemplate);


$strSql = 'SELECT `cc_id`, DATE_FORMAT(`cc_created`, "%d.%m.%Y") AS `cc_created` FROM `cms_cert` WHERE `cc_group_id` = "' .$_REQUEST['Id'] .'" AND `cc_month` = "' .$arrPeriod[0] .'" AND `cc_year` = "' .$arrPeriod[1] .'"';
$arrCert = MySQLStatic::Query($strSql);

$strTemplate = str_replace('{{CreateDate}}', $arrCert[0]['cc_created'], $strTemplate);


$objDate = new DateTime($arrCompanyGroup[0]['Registriert_bei_IZS_seit__c']);
$strTemplate = str_replace('{{RegistedSince}}', $objDate->format('d.m.Y'), $strTemplate);


$arrAmpel = array(
  'rot' => 'rot',
  'grün' => 'gruen',
  'gelb' => 'gelb'
);

if (isset($arrAmpel[$arrCompanyGroup[0]['Gesamtstatus__c']])) {
  $strReplacement = '<img style="padding-top: 3px;" src="https://www.izs.de/cms/img/status_' .$arrAmpel[$arrCompanyGroup[0]['Gesamtstatus__c']] .'.png">';
} else {
  $strReplacement = $arrCompanyGroup[0]['Gesamtstatus__c'];
}

$strTemplate = str_replace('{{EntireStatus}}', $strReplacement, $strTemplate);
$strTemplate = str_replace('{{EntireStatusNote}}', $arrCompanyGroup[0]['Hinweis_sichtbar_auf_Portal__c'], $strTemplate);


$strSql = 'SELECT * FROM `Account` WHERE `SF42_Company_Group__c` = "' .$_REQUEST['Id'] .'"';
$arrAccountList = MySQLStatic::Query($strSql);

$arrEventAccountList = array();

foreach ($arrAccountList as $intKey => $arrAccount) {
  
  $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Betriebsnummer_ZA__c` = "' .$arrAccount['SF42_Comany_ID__c'] .'" AND `Beitragsmonat__c` = "' .$arrPeriod[0]  .'" AND `Beitragsjahr__c` = "' .$arrPeriod[1]  .'" AND `SF42_EventStatus__c` IN ("to enquire", "enquired", "no result", "not OK", "OK") ORDER BY `Betriebsnummer_ZA__c`';
  $arrEventList = MySQLStatic::Query($strSql);
  
  $boolPayed = true;

  if (count($arrEventList) > 0) {
    
    $strPeriod = $arrEventList[0]['SF42_Payment_Period__c'];
    $strBetrNr = $arrEventList[0]['Betriebsnummer_ZA__c'];
    
    foreach ($arrEventList as $intEventKey => $arrEvent) {
      if (!in_array($arrEvent['SF42_EventStatus__c'], array('no result', 'not OK', 'OK')) ) {
        $boolPayed = false;
        break;
      }
    }
    
    $arrAccount['periodid'] = $strPeriod;
    $arrAccount['betriebsnr'] = $strBetrNr;
    if ($boolPayed == true) {
      $arrEventAccountList[strtolower($arrAccount['Name']) .'_' .$strBetrNr] = $arrAccount;
    }

  }
  
}

$strListAccounts = '';

if (count($arrEventAccountList) > 0) {
  ksort($arrEventAccountList);
  
  $strListAccounts.= '
                            					<!-- Tabelle Prüfergebnis START -->
                            						<table id="tableErgebnis" border="0" cellpadding="0" cellspacing="0" style="width:560px">
                            							<thead>
                            								<tr>
                            									<th width="20px" align="left" style="color:#836E2C; font-weight:normal; border-top:1px #836E2C solid; border-bottom:2px #836E2C solid; padding:10px 5px; font-size: 12px;font-family:Verdana;">&nbsp;</th>
                            									<th width="330px" align="left" style="color:#836E2C; font-weight:normal; border-top:1px #836E2C solid; border-bottom:2px #836E2C solid; padding:10px 5px; font-size: 12px;font-family:Verdana;">Betriebstätte</th>
                            									<th width="80px" align="left" style="color:#836E2C; font-weight:normal; border-top:1px #836E2C solid; border-bottom:2px #836E2C solid; padding:10px 5px; font-size: 12px;font-family:Verdana;">BNR</th>
                            									<th width="80px" align="left" style="color:#836E2C; font-weight:normal; border-top:1px #836E2C solid; border-bottom:2px #836E2C solid; padding:10px 5px; font-size: 12px;font-family:Verdana;">Detailbericht</th>
                            								</tr>
                            							</thead>
                            							<tbody>' .chr(10);
  
  
  if (count($arrEventAccountList) > 0) {
    
    
    $intCount = 0;
    
    foreach ($arrEventAccountList as $strKey => $arrAccount) {

      $strLink1 = 'https://www.izs.de/kontoauszug.html?companyid=' .$_REQUEST['Id'] .'&accountid=' .$arrAccount['Id'] .'&selectedMonth=' .$arrPeriod[0] .'&selectedYear=' .$arrPeriod[1] .'';
      $strLink2 = 'https://www.izs.de/monatsreport.php?detailId=' .$arrAccount['Id'] .'&selectedMonth=' .$arrPeriod[0] .'&selectedYear=' .$arrPeriod[1] .'';

      $strAmpelMail = '';
      if ($arrAccount['SF42_Payment_Status__c'] == 'Yellow') {
        $strAmpelMail = 'ampel_gelb.png'; 
      } elseif ($arrAccount['SF42_Payment_Status__c'] == 'Red') {
        $strAmpelMail = 'ampel_rot.png'; 
      } elseif ($arrAccount['SF42_Payment_Status__c'] == 'Green') {
        $strAmpelMail = 'ampel_gruen.png'; 
      } else {
        $strAmpelMail = 'ampel_grau.png'; 
      }
      
      if ($intCount < (count($arrEventAccountList) - 1)) {
        $strListAccounts.= '
                            								<tr>
	                            								<td style="font-size: 12px;font-family:Verdana;"><img src="https://www.izs.de/assets/images/zertifikat/' .$strAmpelMail .'"></td>
	                            								<td style="font-size: 12px;font-family:Verdana;"><a href="' .$strLink1 .'" target="_blank" style="color:#202020; text-decoration:none">' .$arrAccount['Name'] .'</a></td>
	                            								<td style="font-size: 12px;font-family:Verdana;">' .$arrAccount['betriebsnr'] .'</td>
	                            								<td style="font-size: 12px;font-family:Verdana;"><a href="' .$strLink2 .'" style="color:#202020;" target="_blank">PDF abrufen</a></td>
                            								</tr>' .chr(10);
      } else {
        $strListAccounts.= '
                            								<tr>
	                            								<td style="border-bottom-style:solid; font-size: 12px;font-family:Verdana;"><img src="https://www.izs.de/assets/images/zertifikat/' .$strAmpelMail .'"></td>
	                            								<td style="border-bottom-style:solid; font-size: 12px;font-family:Verdana;"><a href="' .$strLink1 .'" target="_blank" style="color:#202020; text-decoration:none">' .$arrAccount['Name'] .'</a></td>
	                            								<td style="border-bottom-style:solid; font-size: 12px;font-family:Verdana;">' .$arrAccount['betriebsnr'] .'</td>
	                            								<td style="border-bottom-style:solid; font-size: 12px;font-family:Verdana;"><a href="' .$strLink2 .'" target="_blank" style="color:#202020;">PDF abrufen</a></td>
                            								</tr>' .chr(10);
      }

      $intCount++;
      
    }
  }
  
  $strListAccounts.= '
                            							</tbody>
                            							<tfoot>
                            								<tr>
                            									<td colspan="4" style="border-bottom:none; color:#999999;font-size:10px;  text-align:justify">Durch Klick auf den Namen des Unternehmens können Sie direkt dessen Online-Konto einsehen. Um den detaillierten Monatsbericht zu einem Unternehmen abzurufen, klicken Sie einfach auf „PDF abrufen“. Je nach Umfang des Berichts kann dessen Erstellung bis zu einer Minute dauern. In beiden Fällen ist das Bestehen einer Internetverbindung Voraussetzung für die beschriebene Funktionalität.</td>
                            								</tr>
                            							</tfoot>
                            						</table>
                            					<!-- Tabelle Prüfergebnis END -->' .chr(10);


}

$strTemplate = str_replace('{{Accounts}}', $strListAccounts, $strTemplate);

?>