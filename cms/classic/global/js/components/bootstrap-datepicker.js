/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */

$.components.register("datepicker", {
    mode: "default",
    language: 'de',
    defaults: {
        autoclose: true
    }
});