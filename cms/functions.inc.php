<?php

function strGetFormElement ($arrValueList = array(), $strSelectedValue = '', $strType = 'select', $strId = '', $boolNull = false, $strOption = '', $boolAssoc = false) {
  
  $strReturn = '';
  
  if ($strType == 'checkbox') {

    foreach ($arrValueList as $intKey => $strValue) {
      $strSelected = '';
      if ($strValue == $strSelectedValue) {
        $strSelected = ' checked="checked"';
      }

      $strReturn.= '  <div class="checkbox-custom checkbox-primary pull-left" >' .chr(10);
      $strReturn.= '    <input type="checkbox" id="' .$strId .'" name="' .$strId .'" value="' .$strValue .'" ' .$strSelected .'/>' .chr(10);
      $strReturn.= '    <label for="' .$strId .'">' .$intKey .'</label>' .chr(10);
      $strReturn.= '  </div>' .chr(10);

      //$strReturn.= '  <input value="' .$strValue .'"' .$strSelected .'>' .$strValue .'</option>' .chr(10);
    }
        
  }
  
  if ($strType == 'textarea') {
    $strReturn.= '  <textarea class="form-control" id="' .$strId .'" name="' .$strId .'" rows="3">' .$strSelectedValue .'</textarea>' .chr(10);
  }
  

  if ($strType == 'text') {
    $strReturn.= '<input type="' .$strType .'" id="' .$strId .'" name="' .$strId .'" value="' .$strSelectedValue .'"' .$strOption .'>' .chr(10);
  }
  
  if ($strType == 'select') {
    
    $strReturn.= '<select id="' .$strId .'" name="' .$strId .'"' .$strOption .'>' .chr(10);

    if ($boolNull) {
      $strSelectedNull = '';
      if ($strSelectedValue == '') {
        $strSelectedNull = ' selected="selected"';
      }
      $strReturn.= '  <option value=""' .$strSelectedNull .'></option>' .chr(10);
    }

    foreach ($arrValueList as $strKey => $strValue) {
      $strSelected = '';

      if ($boolAssoc === true) {
        
        if ($strKey == $strSelectedValue) {
          $strSelected = ' selected="selected"';
        }
        $strReturn.= '  <option value="' .$strKey .'"' .$strSelected .'>' .$strValue .'</option>' .chr(10);

      } else {
        
        if ($strValue == $strSelectedValue) {
          $strSelected = ' selected="selected"';
        }
        $strReturn.= '  <option value="' .$strValue .'"' .$strSelected .'>' .$strValue .'</option>' .chr(10);

      }

    }
    $strReturn.= '</select>' .chr(10);
    
  }
  
  return $strReturn;
}

function strAddWorkingDays ($intStartDate = 0, $intAddDays = 0, $strFormat = 'd.m.Y') {
  
  $strReturn = '';
  
  $intDate = $intStartDate;
  $arrDay  = getdate($intStartDate);
  
  if ($arrDay['weekday'] == 'Saturday') {
    $intDate+= 86400 * 2;
  }
  if ($arrDay['weekday'] == 'Sunday') {
    $intDate+= 86400;
  }
  
  for ($intDay = 0; $intDay < $intAddDays; $intDay++) {
    $intDate+= 86400;
    $arrDay = getdate($intDate);
    if ($arrDay['weekday'] == 'Saturday') {
      $intDate+= 86400 * 2;
    }
  }
  
  $strReturn = date($strFormat, $intDate);
  
  return $strReturn;
}  

function strConvertDate ($strDate = '', $strSFormat = '', $strDFormat = '') {
  $strReturn = '';
  
  $strReturn = date($strDFormat, strtotime($strDate));
  
  return $strReturn;
}

function strArrayImplode ($arrArray = array(), $strGlue = '"', $boolKeys = false) {
  $strReturn = '';  
  
  $arrToImplode = $arrArray;
  
  if ($boolKeys) {
    $arrToImplode = array_keys($arrArray);
  }
  
  $strReturn.= $strGlue .implode($strGlue .', ' .$strGlue, $arrToImplode) .$strGlue;

  return $strReturn;
}

function strFormatDate ($strDate = '', $strExportFormat = '', $strImportFormat = '') {
  $strReturn = '';
  
  if (($strDate != '0000-00-00') && ($strDate != '')) {
    $strReturn = date($strExportFormat, strtotime($strDate));
  }
  
  return $strReturn;
}

function arrExtractArray ($arrToExtract = array(), $strExtract = '') {
  
  $arrReturn = array();
  
  if (is_array($arrToExtract) && (count($arrToExtract) > 0)) {
    foreach ($arrToExtract as $intRow => $arrDataset) {
      $arrReturn[] = $arrDataset[$strExtract];
    }
  }
  
  return $arrReturn;
}

function strConcatName ($strFirst = '', $strLast = '', $strTitle = '') {
  
  $strReturn = ''; 
  
  if ($strFirst != '') {
    $strReturn = $strFirst;
    if ($strLast != '') {
      $strReturn.= ' ' .$strLast;
    }
  } elseif ($strLast != '') {
    $strReturn = $strLast;
  }
  
  if (($strReturn != '') && ($strTitle != '')) {
    $strReturn = $strTitle .' ' .$strReturn;
  }
  
  return $strReturn;
  
}


/* NOT USED */

function file_get_contents_utf8($fn) {
     $content = file_get_contents($fn);
      return mb_convert_encoding($content, 'UTF-8',
          mb_detect_encoding($content, 'UTF-8, ISO-8859-1', true));
} 

function string_to_csv_download ($string, $filename = "export.csv") {
  header('Content-Type: application/csv');
  header('Content-Disposition: attachment; filename="'.$filename.'";');

  $f = fopen('php://output', 'w');
	fwrite($f, $string);

}

function array_to_csv_download ($arrTable, $filename = 'export.csv') {
  header("HTTP/1.1 200 OK");
  header("Pragma: public");
  header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
  header("Cache-Control: private", false);
  header('Content-type: application/ms-excel');
  header('Content-Disposition: attachment; filename="'.$filename.'";');
  header("Content-Transfer-Encoding: binary");
  
  echo "\xEF\xBB\xBF";
  
  $resFile = fopen("php://output", "w");

  foreach($arrTable as $intRow => $arrRow) {
    fputcsv($resFile, $arrRow, ';', '"');
  }
  
  fclose($resFile);
  exit;
}

function enable_implicit_flush() {
	@apache_setenv('no-gzip', 1);
	@ini_set('zlib.output_compression', 0);
	@ini_set('implicit_flush', 1);
	for ($i = 0; $i < ob_get_level(); $i++) { ob_end_flush(); }
	ob_implicit_flush(1);
	echo "<!-- ".str_repeat(' ', 2000)." -->";
}

function store_token($token, $name) {
	if(!file_put_contents("tokens/$name.token", serialize($token)))
		die('<br />Could not store token! <b>Make sure that the directory `tokens` exists and is writable!</b>');
}

function load_token($name) {
	if(!file_exists("tokens/$name.token")) return null;
	return @unserialize(@file_get_contents("tokens/$name.token"));
}

function delete_token($name) {
	@unlink("tokens/$name.token");
}

// function to geocode address, it will return false if unable to geocode address
function geocode($address){
  
    usleep(250000);
 
    // url encode the address
    $address = urlencode(utf8_decode($address));
     
    // google map geocode api url
    $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address={$address}";
	//echo $url .'<br />' .chr(10);
 
    // get the json response
    $resp_json = file_get_contents($url);
     
    // decode the json
    $resp = json_decode($resp_json, true);
 
    // response status will be 'OK', if able to geocode given address 
    if($resp['status']=='OK'){
 
        // get the important data
        $lati = $resp['results'][0]['geometry']['location']['lat'];
        $longi = $resp['results'][0]['geometry']['location']['lng'];
        $formatted_address = $resp['results'][0]['formatted_address'];
        $status = $resp['results'][0]['geometry']['location_type'];
        $arrRegion = $resp['results'][0]['address_components'];
		$arrStreetNo = $resp['results'][0]['address_components'];
        
        $region = '';
		$strStreetNo = '';
		$strStreet = '';
        
        foreach ($arrRegion as $intKey => $arrAddressComponents) {
          if ($arrAddressComponents['types'][0] == 'administrative_area_level_1') {
            $region = $arrAddressComponents['long_name'];
          }
          if ($arrAddressComponents['types'][0] == 'route') {
            $strStreet = $arrAddressComponents['long_name'];
          }
          if ($arrAddressComponents['types'][0] == 'street_number') {
            $strStreetNo = $arrAddressComponents['long_name'];
          }
        }
         
        // verify if data is complete
        if($lati && $longi && $formatted_address){
         
            // put the data in the array
            $data_arr = array();            
             
            array_push(
                $data_arr, 
                    $lati, 
                    $longi, 
                    $formatted_address,
                    $resp,
                    $status,
                    $region, 
					$strStreetNo,
					$strStreet
                );
                
            return $data_arr;
             
        }else{
            return false;
        }
         
    }else{
		print_r($resp);
		echo '<br />' .chr(10);
        return false;
    }
}

function objectToArray ($d) {
    if (is_object($d)) {
        $d = get_object_vars($d);
    }
    if (is_array($d)) {
        return array_map(__FUNCTION__, $d);
    } else {
        return $d;
    }
}

function arrObjToArr ($obj = null) {
	
	$arrReturn = array();
	
	$arrToConvert = objectToArray($obj);
	
	foreach ($arrToConvert as $strServiceName => $arrData) {
		if (isset($arrData[0])) {
			$arrReturn = $arrData;
		} else {
			$arrReturn[] = $arrData;
		}
	}
	
	return $arrReturn;
	
}


function multi_attach_mail($to, $from, $files, $subject, $message) {
  
  $message_m = '';

  var_dump($subject); die(); 
  
  $sendermail = $from;
  $subject = '' .$subject .'';
  
  $headers  = "";
  $headers .= "From: $from";
  
  ///*
  //$headers .= "\n" .'Cc: faxausgang@izs-institut.de';
  $headers .= "\n" .'Bcc: f.luetgen@gmx.de'; //, 
  //*/

  // boundary
  $semi_rand = md5(time());
  $mime_boundary = "==Multipart_Boundary_x" .$semi_rand ."x";

  // headers for attachment
  $headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"" .$mime_boundary ."\"";

  // multipart boundary
  if ($message != '') {
    $message_m.= "--{$mime_boundary}\n" . "Content-Type: text/plain; charset=\"UTF-8\"\n" .
    "Content-Transfer-Encoding: 8bit\n\n" . $message . "\n\n";
  }
  
  //print_r($files); die();
  
  // preparing attachments
  for($i=0;$i<count($files);$i++){
      if(is_file($files[$i])){
          $message_m.= "--{$mime_boundary}\n";
          $fp =   @fopen($files[$i],"rb");
          $data = @fread($fp,filesize($files[$i]));
          @fclose($fp);
          $data = chunk_split(base64_encode($data));
          $message_m.= "Content-Type: text/csv; name=\"".basename($files[$i])."\"\n" .
          "Content-Transfer-Encoding: base64\n" . 
          "Content-Disposition: attachment;\n" . " filename=\"".basename($files[$i])."\"" .
          "\n\n" . $data . "\n\n";
      }
  }
  
  $message_m.= "--{$mime_boundary}--";

  $returnpath = "-f" . $sendermail;
  $ok = @mail($to, $subject, $message_m, $headers, $returnpath);
  if($ok){ return $i; } else { return 0; }
}


?>