<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

require_once('../assets/classes/class.mysql.php');

$strOutput = '';

$strSql = 'SELECT UNIX_TIMESTAMP(`replication`) AS `replication` FROM `_replicate`';
$arrResult = MySQLStatic::Query($strSql);
$intCountResult = count($arrResult);

$strOutput.= '<h1>Synchronisation</h1>' .Chr(10);

if ($intCountResult > 0) {

  if ($arrResult[0]['replication'] < (time() - $intWait)) {
    
    //WRITE STATUS
    $strSql = 'UPDATE `_replicate` SET `re_force` = "1", `re_cl_id` = "' .$_SESSION['id'] .'"';
    $arrResult = MySQLStatic::Update($strSql);

    $strOutput.= '<div class="example-blocks">' .chr(10);
    $strOutput.= 'Synchronisation startet in ...' .chr(10);
    $strOutput.= '<div id="clock">' .chr(10);
    $strOutput.= '</div>' .chr(10) .chr(10);
    
    $intSeconds = (60 - date('s')) + ((10 - ((date('i') % 10) + 1)) * 60);

    $strOutput.= '<script type="text/javascript">' .chr(10);
    $strOutput.= '  var tenMinutes = new Date(new Date().valueOf() + ' .$intSeconds .' * 1000);' .chr(10);
    $strOutput.= '' .chr(10);
    $strOutput.= '  $(\'#clock\').countdown(tenMinutes, {elapse: true}).on(\'update.countdown\', function(event) {' .chr(10);
    $strOutput.= '    var $this = $(this).html(event.strftime(\'\'' .chr(10);
    $strOutput.= '      + \'<span>%M</span> min \'' .chr(10);
    $strOutput.= '      + \'<span>%S</span> sec\'));' .chr(10);

    $strOutput.= '    if (event.elapsed) {' .chr(10);
    $strOutput.= '      $(this).html(\'<span>...gestartet.</span>\');' .chr(10);
    $strOutput.= '    }' .chr(10);

    $strOutput.= '  });' .chr(10);
    $strOutput.= '</script>' .chr(10);

  } else {
    
    $intSeconds = ($intWait - (time() - $arrResult[0]['replication']));
    
    if ($intSeconds > 60) {
      $strTime = floor($intSeconds / 60) .':' .str_pad(($intSeconds % 60), 2, '0', STR_PAD_LEFT) .' Minuten';
    } else {
      $strTime = $intSeconds .' Sekunden';
    }
    

    $strOutput.= 'Synchronisation läuft bereits. Bitte Anfrage in ' .$strTime .' wiederholen.';

  }
   
}

?>