<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

if ($boolStandAlone === true) {

    require_once('../const.inc.php');
    require_once('../../assets/classes/class.mysql.php');

    require_once('../inc/refactor.inc.php');

    require_once ('../fpdf182/fpdf.php');
    require_once ('../fpdf182/class.pdfhtml.php');

}

$arrMonthList = array(
    1 => 'Januar',
    2 => 'Februar',
    3 => 'März',
    4 => 'April',
    5 => 'Mai',
    6 => 'Juni',
    7 => 'Juli',
    8 => 'August',
    9 => 'September',
    10 => 'Oktober',
    11 => 'November',
    12 => 'Dezember'
);

$intDurationTime = microtime(true) - $intBeginTime;
if ($_REQUEST['d'] == 't') echo " Include: $intDurationTime Sek." .chr(10);

// INPUT
if (!isset($strGroupId) || ($strGroupId == '')) {
    //http_response_code(404);
    //include('my_404.php'); // provide your own HTML for the error page
    //die();
} 

function replaceCharacterSp($text, $searchChar, $replaceChar, $occurrenceArray) {
    $count = 0;
    
    $searchLength = strlen($searchChar);
    $replaceLength = strlen($replaceChar);
    
    for ($i = 0; $i < strlen($text); $i++) {
        if (substr($text, $i, $searchLength) == $searchChar) {
            $count++;
            
            if (in_array($count, $occurrenceArray)) {
                $text = substr_replace($text, $replaceChar, $i, $searchLength);
                $i += $replaceLength - 1;
            }
        }
    }
    
    return $text;
}

function calculateLineBreakSp ($strText = '', $floatWidth = 0, $objPdf) {

    $strFinalText = $strText;

    $arrPartList = explode('-', $strText);

    if ((count($arrPartList) > 0) && (!is_null($objPdf))) {

        $floatHeight1 = $objPdf->GetMultiCellHeight($floatWidth, 6, $strText, 0, 'L');
        $floatHeight2 = $objPdf->GetMultiCellHeight($floatWidth, 6, replaceCharacterSp($strText, '-', '-' .chr(10), [1]), 0, 'L');

        if ($floatHeight2 < $floatHeight1) {
            if (count($arrPartList) > 1) {
                $floatHeight3 = $objPdf->GetMultiCellHeight($floatWidth, 6, replaceCharacterSp($strText, '-', '-' .chr(10), [2]), 0, 'L');
                if ($floatHeight3 <= $floatHeight2) {
                    $strFinalText = replaceCharacterSp($strText, '-', '-' .chr(10), [2]);
                } else {
                    $strFinalText = replaceCharacterSp($strText, '-', '-' .chr(10), [1]);
                }
            } else {
                $strFinalText = replaceCharacterSp($strText, '-', '-' .chr(10), [1]);
            }
        }

    }

    return $strFinalText;

}

function boolIsUniqueSp ($strUnique = '') {

    $strSql = 'SELECT `ed_id` FROM `izs_event_download` WHERE `ed_hash` = "' .$strUnique .'"';
    $arrSql = MySQLStatic::Query($strSql);
    
    if (count($arrSql) == 0) {

        return $strUnique;

    } else {

        boolIsUniqueSp(uniqid());

    }

}

function createCertSp ($intEventId = 0, &$objPdf = null, $strReturnType = 'string', $arrGroupSelected = array()) {

    global $intBeginTime, $arrPdfLinkListEvent, $arrSql, $arrMonthList, $boolCreate, $intCreateUser, $strKuendigung;

    if (count($arrGroupSelected) > 0) {
        $arrSql = $arrGroupSelected;
    }

    if (!isset($strReturnType)) {
        $strReturnType = 'string';
    }

    // OUTPUT
    $strOutput = '';

    $intDurationTime = microtime(true) - $intBeginTime;
    if ($_REQUEST['d'] == 't') echo " Select: $intDurationTime Sek." .chr(10);

    //print_r($arrSql); die();

    if (count($arrSql) > 0) {

        $boolStandAlone = false;


        if (!isset($objPdf)) {

            $boolStandAlone = true;
        }

        if ($boolCreate === true) {

            $strUnique = boolIsUniqueSp(uniqid());

            $arrData = array(
                'name'   => $arrSql[0]['Name'],
                'street' => $arrSql[0]['Strasse__c'],
                'zip'    => $arrSql[0]['PLZ__c'],
                'city'   => $arrSql[0]['Ort__c'],
                'izsid'  => $arrSql[0]['Betriebsnummer__c'],
                'member' => date('Y-m-d', strtotime($arrSql[0]['Registriert_bei_IZS_seit__c'])),
                'link'   => '/company_group/' .$arrSql[0]['Id'] .'/stammdaten'
            );

            $strFileName = 'done/'.$arrSql[0]['Id'] .'_' .date('dmYHis') .'.pdf';
            $intUserId   = ($_COOKIE['id']) ?? '';

            $strSql = 'INSERT INTO `izs_subsidiaerprotect` (`pr_id`, `pr_hash`, `pr_group_id`, `pr_file`, `pr_file_name`, `pr_data`, `pr_cl_id`, ';
            $strSql.= '`pr_revoked`, `pr_revoked_by`, `pr_revoked_time`, `pr_time`) VALUES (NULL, "'.$strUnique .'", "' .$arrSql[0]['Id'] .'", "' .$strFileName .'", ';
            $strSql.= '"' .$arrSql[0]['Name'] .' - SubsidiärProtect Zertifikat", "' .(addslashes(serialize($arrData))) .'", "' .$intCreateUser .'", "0", "0", "0000-00-00", NOW())';

            $intSql = MySQLStatic::Insert($strSql);

            $strSql  = 'SELECT `pr_time` FROM `izs_subsidiaerprotect` WHERE `pr_id` = "' .$intSql .'"';
            $arrTime = MySQLStatic::QUERY($strSql);

        } else {

            $arrTime = [];

            $strSql = 'SELECT * FROM `izs_subsidiaerprotect` WHERE `pr_group_id` = "' .$arrSql[0]['Id'] .'" AND `pr_revoked` = 0 ORDER BY `pr_time` DESC LIMIT 1';
            $arrCert = MySQLStatic::Query($strSql);

            $arrData = unserialize($arrCert[0]['pr_data']);
            $strFileName = $arrCert[0]['pr_file_name'];
            $arrTime[0]['pr_time'] = $arrCert[0]['pr_time'];

            $strUnique = $arrCert[0]['pr_hash'];

        }

        $strHost = 'www';
        if (isset($_SERVER['SERVER_NAME']) && (strstr($_SERVER['SERVER_NAME'], 'test.') !== false)) {
            $strHost = 'test';
        }

        $strHash = 'https://' .$strHost .'.izs.de/cms/protect/' .$strUnique;

        $strUid  = md5(uniqid(rand(), true));

        if ($boolStandAlone === true) {
            $strPath = '../files/temp/' .$strUid .'.png';
        } else {
            $strPath =  '/data/www/sites/www.izs.de/html/cms/files/temp/' .$strUid .'.png';
        }
        
        
        QRcode::png($strHash, $strPath);

        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " QR-Code: $intDurationTime Sek." .chr(10);

        $floatLeft = 20;
        $floatCol1 = 81.5;

        if (!isset($objPdf)) {

            $objPdf = new pdfHtml('L');
            $objPdf->AliasNbPages();

            $objPdf->AddFont('Lato', '', 'Lato-Regular.php');
            $objPdf->AddFont('Lato', 'B', 'Lato-Bold.php');

            $objPdf->AddFont('OfficinaSerif', '', 'itcofficinaserifw04medium-webfont.php');
            $objPdf->AddFont('OfficinaSerif', 'B', 'itcofficinaserifw04medium-webfont.php');
            
        } 

        $objPdf->AddPage('L');
        $objPdf->SetAutoPageBreak(false);

        if ($boolStandAlone === false) {
            $objPdf->SetLink($arrPdfLinkListEvent[$arrSql[0]['evid']], 0, -1);
        }

        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " Init PDF: $intDurationTime Sek." .chr(10);
        
        $objPdf->Image( __DIR__ .'/img/bg_pdf5.png', 0, 0, 297, 210, 'PNG'); 


        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " Include BG: $intDurationTime Sek." .chr(10);

        //Page margin
        $objPdf->SetMargins($floatLeft, 10.8);

        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " Include Logo: $intDurationTime Sek." .chr(10);
        
        /*
        $strText = utf8_decode('SubsidiärProtect') .chr(174) .utf8_decode('');

        $objPdf->SetXY(33.5, 21.8);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Helvetica', 'B', 19);
        $objPdf->MultiCell(85, 5, $strText, 0, 'L');
        */

        $objPdf->Image( __DIR__ .'/img/sp-logo3.png', 20, 18, 75); 


        $strText = utf8_decode('SubsidiärProtect     ist Deutschlands einzige Versicherung, mit der sich Entleihfirmen gegen Subsidiärhaftungsschäden von Personaldienstleistern absichern können. Alle IZS-geprüften Personaldienstleister sind dabei automatisch kostenlos mitversichert.');

        $objPdf->SetY(45);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Lato', '', 7);
        $objPdf->MultiCell(85, 5, $strText, 0, 'L');

        $objPdf->SetMargins(38.3, 0);
        $objPdf->SetY(45.1);
        $objPdf->SetFont('Lato', '', 5);
        $objPdf->WriteHTML(chr(174) .utf8_decode(''));


        $objPdf->SetMargins($floatLeft, 10.8);
        $objPdf->SetY(78);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Lato', 'B', 12);
        $objPdf->MultiCell(85, 5, utf8_decode('Starke Vorteile für Entleiher'), 0, 'L');

        $objPdf->SetMargins($floatLeft + 7, 10.8);

        $objPdf->SetY(88.9);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Lato', 'B', 9);
        $objPdf->MultiCell(85, 5, utf8_decode('Finanzielle Absicherung'), 0, 'L');

        $objPdf->SetMargins($floatLeft, 10.8);
        $strText = utf8_decode('Sie sind rundum geschützt - Alle Entleihungen von IZS-geprüften Personaldienstleistern sind automatisch im Rahmen der gewählten Versicherungssumme abgesichert.');

        $objPdf->SetY(96.9);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Lato', '', 7);
        $objPdf->MultiCell(85, 5, $strText, 0, 'L');

        $objPdf->SetMargins($floatLeft + 7, 10.8);
        $objPdf->SetY(114.5);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Lato', 'B', 9);
        $objPdf->MultiCell(85, 5, utf8_decode('Integriertes Nachweismanagement'), 0, 'L');

        $objPdf->SetMargins($floatLeft, 10.8);
        $strText = utf8_decode('IZS besorgt regelmäßig alle wichtigen Auskünfte und Dokumente, die Sie jederzeit im kostenlos nutzbaren Online-Portal für Entleiher einsehen und herunterladen können. Mit anderen Worten: Null Aufwand für Sie.');

        $objPdf->SetY(122.5);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Lato', '', 7);
        $objPdf->MultiCell(85, 5, $strText, 0, 'L');

        $objPdf->SetMargins($floatLeft + 7, 10.8);
        $objPdf->SetY(141.2);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Lato', 'B', 9);
        $objPdf->MultiCell(85, 5, utf8_decode('Intelligentes Frühwarnsystem'), 0, 'L');

        $objPdf->SetMargins($floatLeft, 10.8);
        $strText = utf8_decode('Werden Risiken erkennbar, wie z. B. Beitragsrückstände oder Stundungen, werden Sie automatisch benachrichtigt und können sofort handeln.');

        $objPdf->SetY(149.2);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Lato', '', 7);
        $objPdf->MultiCell(85, 5, $strText, 0, 'L');

        //$objPdf->SetMargins($floatLeft + 7, 10.8);

        $objPdf->setLinkColor(233, 94, 53, ''); //#f06600
        $objPdf->SetMargins(29.6, 0, 0);
        $objPdf->SetY(178.5 - 0.3);
        $objPdf->SetFont('Symbol', 'B', 7 - 2);
        $objPdf->WriteHTML('<a href="https://www.subsidiaerprotect.de/beitragsrechner#rec636410713" target="_blank">' .chr(174) .'</a>');

        $objPdf->SetMargins(61.2, 0, 0);
        $objPdf->SetY(178.5 - 0.3);
        $objPdf->SetFont('Symbol', 'B', 7 - 2);
        $objPdf->WriteHTML('<a href="https://www.subsidiaerprotect.de/antrag" target="_blank">' .chr(174) .'</a>');

        $strHtml = '<br>Detaillierte Informationen zu SubsidiärProtect     für Entleiher, <br />';
        $strHtml.= 'einen <a href="https://www.subsidiaerprotect.de/beitragsrechner#rec636410713" target="_blank" color="#DF5E35">     Beitragsrechner</a> sowie den <a href="https://www.subsidiaerprotect.de/antrag" target="_blank" color="#DF5E35">     Antrag</a> finden Sie online unter: <br /><br />';
        $strHtml1 = '<a href="https://www.subsidiaerprotect.de/" target="_blank" color="#2C2D2E">www.subsidiaerprotect.de</a>';

        $objPdf->SetXY($floatCol1 + 18, 170.5);
        $objPdf->SetMargins(23, 0, 0);

        $objPdf->SetFont('Lato', '', 7);
        $objPdf->SetTextColor(44, 45, 46); //#605e55
        $objPdf->setLinkColor(233, 94, 53, ''); //#f06600
        $objPdf->WriteHTML(utf8_decode($strHtml));
        
        $objPdf->SetFont('Lato', 'B', 11);
        $objPdf->SetTextColor(44, 45, 46); //#605e55
        $objPdf->setLinkColor(0, 0, 0, ''); //#ffffff 
        $objPdf->WriteHTML(utf8_decode($strHtml1));


        $objPdf->SetMargins(73.2, 0);
        $objPdf->SetY(173.9);
        $objPdf->SetFont('Lato', '', 5);
        $objPdf->WriteHTML(chr(174) .utf8_decode(''));
        

        $objPdf->SetMargins($floatLeft + 124, 10.8);
        $objPdf->SetY(44);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Helvetica', '', 72);
        $objPdf->MultiCell(130, 5, utf8_decode('Zertifikat'), 0, 'L');    
        
        $objPdf->SetY(59);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Helvetica', '', 9);
        $objPdf->MultiCell(130, 5, utf8_decode('zum Nachweis der kostenlosen Mitversicherbarkeit über SubsidiärProtect'), 0, 'L');

        $objPdf->SetMargins(247.4, 0);
        $objPdf->SetY(58.5);
        $objPdf->SetFont('Helvetica', '', 7);
        $objPdf->WriteHTML(chr(174) .utf8_decode(''));


        $objPdf->SetFont('Helvetica', 'B', 17);
        $floatWidth1  = $objPdf->GetStringWidth($arrData['name']);

        if ($floatWidth1 > 113) {
            $intY = 77;
        } else {
            $intY = 81;
        }
        
        $objPdf->SetMargins($floatLeft + 136, 10.8);
        $objPdf->SetY($intY);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Helvetica', 'B', 17);
        $objPdf->MultiCell(115, 7, utf8_decode($arrData['name']), 0, 'L');   

        if ($arrData['member'] != '0000-00-00') {
            $intTime = strtotime($arrData['member']);
            $strRegistriert = date('d. ', $intTime) .$arrMonthList[date('n', $intTime)] .date(' Y', $intTime);
        } else {
            $strRegistriert = '';
        }
        
        $objPdf->SetMargins($floatLeft + 125, 10.8);
        $objPdf->SetY(100);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Lato', '', 9);
        $objPdf->MultiCell(130, 5, utf8_decode(str_replace("\n", '', $arrData['street'])), 0, 'L');   
        $objPdf->MultiCell(130, 5, utf8_decode($arrData['zip'] .' ' .str_replace("\n", '', $arrData['city'])), 0, 'L');   
        $objPdf->MultiCell(130, 5, utf8_decode('IZS-ID: ' .$arrData['izsid']), 0, 'L'); 

        if ($strRegistriert != '') {
            $objPdf->MultiCell(130, 5, utf8_decode('Mitglied bei IZS seit: ' .$strRegistriert), 0, 'L');
        }

        $intOffset = 0;
        if (strstr($arrSql[0]['Strasse__c'], chr(10)) !== false) {
            //$intOffset = 5;
        }

        $intKue = 0;
        if ($strKuendigung != '') {

            $objPdf->SetFont('Lato', 'B', 9);

            $floatWidth2  = $objPdf->GetStringWidth('Das Zertifikat läuft am ' .$strKuendigung .' ab.');

            $objPdf->SetLineWidth(0.5);
            $objPdf->SetDrawColor(255, 0, 0);
            $objPdf->Rect($floatLeft + 125, 123, $floatWidth2 + 3, 6);

            $objPdf->SetY(123.5);
            $objPdf->SetTextColor(255, 0, 0); //#000000
            $objPdf->SetFont('Lato', 'B', 9);
            $objPdf->MultiCell(130, 5, utf8_decode('   Das Zertifikat läuft am ' .$strKuendigung .' ab.'), 0, 'L');  

            $objPdf->SetTextColor(44, 45, 46); //#000000

            $intKue = 10;

        }

        $objPdf->Image( __DIR__ .'/img/button3.png', $floatLeft + 125.4, 125 + $intOffset + $intKue, 51.8, 10.2);

        $objPdf->SetMargins($floatLeft + 129, 10.8, 0);
        $objPdf->SetY(128.1 + $intOffset + $intKue);
        $objPdf->SetTextColor(255, 255, 255); //#ffffff

        $objPdf->SetFont('Symbol', '', 10);
        $objPdf->WriteHTML('' .chr(174) .'');

        $objPdf->SetMargins($floatLeft + 134, 10.8, 0);
        $objPdf->SetY(128.1 + $intOffset + $intKue);
        $objPdf->SetFont('Lato', '', 10);
        $objPdf->MultiCell(73, 4, utf8_decode('zum IZS-Mitgliedskonto'), 0, '');

        //$strUrl = 'izs-portal.netlify.app';
        $strUrl = 'portal.izs.de';

        $objPdf->Link($floatLeft + 125, 125 + $intOffset + $intKue, 51.8, 10.2, 'https://' .$strUrl .'/company_group/' .$arrSql[0]['Id'] .'/stammdaten');

        /*
        $objPdf->Image( __DIR__ .'/img/button.png', $floatLeft + 125, 125 + $intOffset + $intKue, 51.8, 10.2);

        $objPdf->SetMargins($floatLeft + 114, 10.8, 0);
        $objPdf->SetY(128 + $intOffset + $intKue);
        $objPdf->SetTextColor(255, 255, 255); //#ffffff
        $objPdf->SetFont('Lato', 'B', 11);
        $objPdf->MultiCell(73, 4, utf8_decode('Zum IZS-Mitgliedskonto'), 0, 'C');

        //$strUrl = 'izs-portal.netlify.app';
        $strUrl = 'portal.izs.de';

        $objPdf->Link($floatLeft + 125, 125 + $intOffset + $intKue, 51.8, 10.2, 'https://' .$strUrl .'/company_group/' .$arrSql[0]['Id'] .'/stammdaten');
        */

        $objPdf->SetMargins($floatLeft + 125, 10.8);

        $objPdf->SetY(155);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Lato', '', 9);
        $objPdf->MultiCell(130, 6, utf8_decode('Dieses Zertifikat ist nur gültig mit QR-Code.'), 0, 'L');   
        $objPdf->MultiCell(130, 6, utf8_decode('Die Gültigkeit kann auch online überprüft werden:'), 0, 'L');   

        $strUrl = $strHost .'.izs.de/cms/protect/' .$strUnique;

        $objPdf->SetMargins($floatLeft + 125, 0, 0);
        $objPdf->SetY(168 - 0.3);
        $objPdf->SetFont('Symbol', 'B', 9);
        $objPdf->setLinkColor(233, 94, 53, ''); //#f06600
        $objPdf->WriteHTML('<a href="' .$strUrl .'" target="_blank">' .chr(174) .'</a>');

        $strHtml = '<a href="https://' .$strUrl .'">' .$strUrl .'</a>';

        $objPdf->SetFont('Lato', '', 9);
        $objPdf->SetMargins($floatLeft + 125 + 4, 10.8, 0);
        $objPdf->SetX(140);
        $objPdf->SetY(168);
        $objPdf->setLinkColor(233, 94, 53, ''); //#f06600
        $objPdf->WriteHTML(utf8_decode($strHtml));


        $objPdf->SetMargins($floatLeft + 125, 10.8, 0);
        $objPdf->SetY(180);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Lato', '', 9);
        $objPdf->MultiCell(130, 4, utf8_decode('Dieses Zertifikat wurde erstellt:'), 0, 'L');

        $intTime = strtotime($arrTime[0]['pr_time']);

        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Lato', '', 9);

        $objPdf->SetXY(150, 186.5);
        $objPdf->MultiCell(130, 4, utf8_decode(date('d.m.Y', $intTime)), 0, 'L');

        $objPdf->SetXY(175, 186.5);
        $objPdf->MultiCell(130, 4, utf8_decode(date('H:i:s', $intTime) .' CET'), 0, 'L');

        //QR-Code
        $objPdf->Image($strPath, 262.5, 172.5, 15, 15);
        unlink($strPath);


        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " Process PDF: $intDurationTime Sek." .chr(10);

        if (($strReturnType == 'string') && ($boolStandAlone === true)) {
            return $objPdf->Output('S'); //Output($strFileName, 'F')
        }


    }

}

if ($boolStandAlone === true) {
    $strOutput = createCertSp($intEventId);
}

?>