<?php

ini_set('display_errors', '1');
ini_set('memory_limit', '-1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

require_once('../const.inc.php');
require_once('../../assets/classes/class.mysql.php');
require_once('phpqrcode/classes/phpqrcode.class.php');

$arrMonthList = array(
    1 => 'Januar',
    2 => 'Februar',
    3 => 'März',
    4 => 'April',
    5 => 'Mai',
    6 => 'Juni',
    7 => 'Juli',
    8 => 'August',
    9 => 'September',
    10 => 'Oktober',
    11 => 'November',
    12 => 'Dezember'
);


function getKuendigung ($strId = '') {

    global $arrMonthList;

    $strKuendigung = '';
    $strSqlV1 = 'SELECT * FROM `Vertrag__c` WHERE `Company_Group__c` = "' .$strId .'" AND `Aktiv__c` = "true" AND `Kuendigung_wirksam_ab__c` != "0000-000-00"';
    $arrSqlV1 = MySQLStatic::Query($strSqlV1);
    if (count($arrSqlV1) > 0) {
        $strSqlV2 = 'SELECT * FROM `Vertrag__c` WHERE `Company_Group__c` = "' .$strId .'" AND `Vertragsbeginn__c` > "' .$arrSqlV1[0]['Vertragsbeginn__c'] .'"';
        $arrSqlV2 = MySQLStatic::Query($strSqlV2);
        if (count($arrSqlV2) == 0) {
            $intKuendigung = strtotime($arrSqlV1[0]['Kuendigung_wirksam_ab__c']);
            $strKuendigung = date('d', $intKuendigung) .'. ' .$arrMonthList[date('n', $intKuendigung)] .' ' .date('Y', $intKuendigung);
        }
    }

    return $strKuendigung;

}


function getKuendigungStatus ($strId = '') {

    $strSqlCompany = 'SELECT * FROM `Vertrag__c` WHERE `Company_Group__c` = "' .$strId .'" AND `Aktiv__c` = "true" AND ((`Kuendigung_wirksam_ab__c` > CURRENT_DATE()) OR (`Kuendigung_wirksam_ab__c` = "0000-000-00"))';
    $arrSqlCompany = MySQLStatic::Query($strSqlCompany);

    if (count($arrSqlCompany) > 0) {
        $boolRevokedCompletely = false;
    } else {
        $boolRevokedCompletely = true;
    }

    return $boolRevokedCompletely;

}

//https://www.izs.de/cms/event/Orizon%20GmbH/TUI%20BKK/2022/7/1042492.pdf

//print_r($_REQUEST); die();

$strRedirectUrl = @$_REQUEST['r'];

if (isset($strRedirectUrl) && ($strRedirectUrl != '')) {

    $arrPart = explode('/', $strRedirectUrl);

    //print_r($arrPart); die();

    if (count($arrPart) == 2) { // VIEW   https://www.izs.de/cms/protect/Randstad%20Deutschland%20GmbH%20-%20Co.%20KG/a083000000IaKp3AAF.pdf
                                // CREATE https://www.izs.de/cms/protect/create/a083000000IaKp3AAF.pdf

        ($arrPart[0] == 'create') ? ($boolCreate = true) : ($boolCreate = false);

        $arrRequest = pathinfo($strRedirectUrl);

        if (isset($arrRequest['filename']) && ($arrRequest['filename'] != '')) {

            $intBeginTime = microtime(true); 

            $strGroupId = $arrRequest['filename'];

            $strSql = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$strGroupId .'"';
            $arrSql = MySQLStatic::Query($strSql);

            $strReturnType  = 'string';
            $boolStandAlone = true;
            $arrPdfLinkListEvent = array(); //NOT USED

            $intCreateUser = $_REQUEST['clid'];

            $strKuendigung = getKuendigung($strGroupId);
            $boolRevokedCompletely = getKuendigungStatus($strGroupId);

            $boolCertMissing = false;
            $strSqlProof2 = 'SELECT * FROM `izs_subsidiaerprotect` WHERE `pr_group_id` = "' .$strGroupId .'" AND `pr_revoked` = 0';
            $arrSqlProof2 = MySQLStatic::Query($strSqlProof2);
            if (count($arrSqlProof2) == 0) {
                $boolCertMissing = true;
            }

            if (($boolCreate === true) || (($boolRevokedCompletely === false) && ($boolCertMissing === false))) {

                include ('create.subsidiaerprotect.cert.inc.php');

                //echo $strOutput; die();

                if (isset($_REQUEST['d']) && ($_REQUEST['d'] == 't')) {
                    
                    //echo $strOutput;
                    $intDurationTime = microtime(true) - $intBeginTime;
                    echo "gesamt: $intDurationTime Sek.";

                } else {

                    if ($boolCreate === true) {

                        $strSql = 'SELECT * FROM `izs_subsidiaerprotect` WHERE `pr_group_id` = "' .$strGroupId .'" ORDER BY `pr_time` DESC';
                        $arrRet = MySQLStatic::Query($strSql);

                        $intTime = strtotime($arrRet[0]['pr_time']);

                        $strName = str_replace('/', '-', $arrSql[0]['Name']);
                        $strName = str_replace('&', '-', $strName);
                        $strUrl = 'https://www.izs.de/cms/protect/' .$strName .'/' .$strGroupId .'.pdf';

                        $strReturn = '<span class="hidden">' .$intTime .'</span>' .date('d.m.Y', $intTime) .' ' .date('H:i:s', $intTime) .'';

                        echo $strReturn;
                        exit;

                    } else {

                        $strFileName = 'Zertifikat SubsidiärProtect - ' .$arrSql[0]['Name'] .'';

                        header ('Content-type:application/pdf');

                        if ((isset($_COOKIE['id'])) && ($_COOKIE['id'] == 3)) {
                            header ('Content-Disposition:inline;filename="' .$strFileName .'.pdf";filename*=UTF-8\'\'' .rawurlencode($strFileName) .'.pdf'); //inline
                        } else {
                            header ('Content-Disposition:attachment;filename="' .$strFileName .'.pdf";filename*=UTF-8\'\'' .rawurlencode($strFileName) .'.pdf'); //inline
                        }

                        //
                        echo $strOutput;

                    }

                }

            } else {

                $strSql = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$strGroupId .'"';
                $arrSql = MySQLStatic::Query($strSql);

                $arrData = array(
                    'name'   => $arrSql[0]['Name'],
                    'street' => $arrSql[0]['Strasse__c'],
                    'zip'    => $arrSql[0]['PLZ__c'],
                    'city'   => $arrSql[0]['Ort__c'],
                    'izsid'  => $arrSql[0]['Betriebsnummer__c'],
                    'member' => date('Y-m-d', strtotime($arrSql[0]['Registriert_bei_IZS_seit__c'])),
                    'link'   => '/company_group/' .$arrSql[0]['Id'] .'/stammdaten'
                );

                $arrSqlProof[0]['pr_data'] = $arrData;

                $strKuendigung = getKuendigung($strGroupId);

                $strSqlCompany = 'SELECT * FROM `Vertrag__c` WHERE `Company_Group__c` = "' .$strGroupId .'" AND `Aktiv__c` = "false" OR ((`Aktiv__c` = "true") AND (`Kuendigung_wirksam_ab__c` <= CURRENT_DATE())) ORDER BY `Vertragsbeginn__c` DESC';
                $arrSqlCompany = MySQLStatic::Query($strSqlCompany);

                $strRevokedCompletely = '';
                $strTime = 0;

                if (count($arrSqlCompany) > 0) {
                    $strRevokedCompletely = $arrSqlCompany[0]['Kooperation_beendet_am__c'];
                    $intTime = strtotime($strRevokedCompletely);
                }

                include ('template.html.php');
                echo $strOutput;

            }

        } 

    } elseif (count($arrPart) == 1) { // PROOF  https://www.izs.de/cms/protect/4c0df93ce6fd34efea1721b2cfb92816

        $strSqlProof = 'SELECT * FROM `izs_subsidiaerprotect` WHERE `pr_hash` = "' .$arrPart[0] .'"';
        $arrSqlProof = MySQLStatic::Query($strSqlProof);

        if (count($arrSqlProof) > 0) {
            $strSqlProof2 = 'SELECT * FROM `izs_subsidiaerprotect` WHERE `pr_group_id` = "' .$arrSqlProof[0]['pr_group_id'] .'" AND `pr_revoked` = 0';
            $arrSqlProof2 = MySQLStatic::Query($strSqlProof2);
            if (count($arrSqlProof2) > 0) {
                $arrSqlProof = $arrSqlProof2;
            } 
        }

        if (count($arrSqlProof) > 0) {

            $arrSqlProof[0]['pr_data'] = unserialize(stripslashes($arrSqlProof[0]['pr_data']));

            $strKuendigung = getKuendigung($arrSqlProof[0]['pr_group_id']);

            $boolRevokedCompletely = getKuendigungStatus($arrSqlProof[0]['pr_group_id']);

            if ($boolRevokedCompletely == true) {

                $strSqlCompany = 'SELECT * FROM `Vertrag__c` WHERE `Company_Group__c` = "' .$arrSqlProof[0]['pr_group_id'] .'" AND `Aktiv__c` = "false" OR ((`Aktiv__c` = "true") AND (`Kuendigung_wirksam_ab__c` <= CURRENT_DATE())) ORDER BY `Vertragsbeginn__c` DESC';
                $arrSqlCompany = MySQLStatic::Query($strSqlCompany);

                $strRevokedCompletely = '';
                if (count($arrSqlCompany) > 0) {
                    $strRevokedCompletely = $arrSqlCompany[0]['Kooperation_beendet_am__c'];
                }
                
            }

            include ('template.html.php');
            echo $strOutput;

        } else {

            http_response_code(404);
            //include('my_404.php'); // provide your own HTML for the error page
            die();

        }


    }


} else {

    http_response_code(404);
    //include('my_404.php'); // provide your own HTML for the error page
    die();

}

?>