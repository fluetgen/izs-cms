<?php

$strOutput = '<!DOCTYPE html>
<html lang="de">

<head>
    <link rel="stylesheet" type="text/css" href="https://www.izs.de/cms/protect/style_mobile.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
</head>

<body>
    <div class="header">
        <div class="logo">
            <a href="https://www.subsidiaerprotect.de/"><img src="https://www.izs.de/cms/protect/img/sp-logo2.png" height="30"></a>
        </div>
    </div>
    <div class="section_1">

    </div>
    <div class="section_2">
        <h2>
            ZERTIFIKAT
        </h2>
        <div>
            zum Nachweis der kostenlosen Mitversicherbarkeit über SubsidiärProtect<sup>&reg;</sup>
        </div>
    </div>
    <div class="section_3">
    ';

    if ($boolRevokedCompletely === true) {

        $strOutput.= '        <div class="section_3_top">Das Zertifikat ist nicht mehr gültig, weil das Unternehmen nicht mehr an die IZS-Plattform angebunden ist.</div>' .chr(10);
        $strOutput.= '        <div class="section_3_red"><img src="https://www.izs.de/cms/protect/img/cancel.svg"></div>' .chr(10);        

    } else if ($boolCertMissing === true) {

        $strOutput.= '        <div class="section_3_top">Das Unternehmen verfügt über keine gültigen Zertifikate.</div>' .chr(10);
        $strOutput.= '        <div class="section_3_red"><img src="https://www.izs.de/cms/protect/img/cancel.svg"></div>' .chr(10);        

    } else if ($arrSqlProof[0]['pr_revoked'] == '1') {
    
        $strOutput.= '        <div class="section_3_top">Das Zertifikat ist nicht gültig</div>' .chr(10);
        $strOutput.= '        <div class="section_3_red"><img src="https://www.izs.de/cms/protect/img/cancel.svg"></div>' .chr(10);

            
    } else {
    
        $strOutput.= '        <div class="section_3_top">Das Zertifikat ist gültig</div>' .chr(10);
        $strOutput.= '        <img src="https://www.izs.de/cms/protect/img/checked.svg">' .chr(10);
    
    }

    //print_r($arrSqlProof[0]);

    $strOutput.= '
        </div>
    ';

    if ($strKuendigung != '') {

        $strOutput.= '
        <div class="section_3_red" style="max-width: 100%;">
            Das Zertifikat läuft am ' .$strKuendigung .' ab.
        </div>
        ';

    }

    $strOutput.= '
        <div class="section_4">
        <div class="section_4_1">
            PERSONALDIENSTLEISTER
        </div>
        <div class="section_4_2">
            ' .$arrSqlProof[0]['pr_data']['name'] .'
        </div>
        <span class="section_4_3">
            ' .$arrSqlProof[0]['pr_data']['street'] .'
        </span>
        <span class="section_4_3">
            ' .$arrSqlProof[0]['pr_data']['zip'] .' ' .$arrSqlProof[0]['pr_data']['city'] .'
        </span>
        <div class="section_4_3">
            IZS-ID: ' .$arrSqlProof[0]['pr_data']['izsid'] .'
        </div>
        <div class="section_4_4">' .chr(10);

        if ($boolRevokedCompletely === true) {
    
            $intTime = strtotime($strRevokedCompletely);
            $strBeendet = date('d. ', $intTime) .$arrMonthList[date('n', $intTime)] .date(' Y', $intTime);
        
            if ($intTime > 0) $strOutput.= '        Kooperation beendet am: ' .$strBeendet .'' .chr(10);
                
        } else {

            $intTime = strtotime($arrSqlProof[0]['pr_data']['member']);
            $strRegistriert = date('d. ', $intTime) .$arrMonthList[date('n', $intTime)] .date(' Y', $intTime);
        
            $strOutput.= '        Mitglied bei IZS seit: ' .$strRegistriert .'' .chr(10);

        }

        $strOutput.= '
        </div>
        
    </div>
    <div class="section_5">
        <div class="section_5_1">Detaillierte Informationen zu SubsidiärProtect<sup>&reg;</sup>:</div>
        <a href="https://www.subsidiaerprotect.de/" class="section_5_3">Zur Webseite</a>
    </div>

</body>

</html>';

?>