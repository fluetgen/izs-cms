<?php

session_start();

date_default_timezone_set('Europe/Berlin');

if (isset($_SERVER['argv'][1]) && ($_SERVER['argv'][1] != '')) {
  for ($intArg = 1; $intArg < count($_SERVER['argv']); $intArg++) {
    $arrPairs = explode('&', $_SERVER['argv'][$intArg]);
    foreach ($arrPairs as $strPair) {
      $arrVar = explode('=', $strPair);
      $_REQUEST[$arrVar[0]] = $arrVar[1];
    }
  }
}

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  //header('Location: /cms/index.php?ac=auth');
}

$start = microtime(true);   //ZEIT

require_once('../assets/classes/class.mysql.php');

$arrPp = array();

$strSql = 'SELECT `Id`, `Name`, `SF42_Comany_ID__c` FROM `Account` ';
$strSql.= 'WHERE `SF42_isPremiumPayer__c` = "true" ORDER BY `Account`.`Name` ASC';
$arrResult = MySQLStatic::Query($strSql);
if (count($arrResult) > 0) {
  foreach ($arrResult as $intKey => $arrProvider) {
    $arrPp[$arrProvider['SF42_Comany_ID__c']] = $arrProvider['Id'];
  }
}

//print_r($arrPp); die();

//$_REQUEST['ec_time'] = '01.04.2020'; // '02.04.2023'

//$_REQUEST['ec_time'] = '02.04.2023';


$strSql = 'SELECT COUNT(*) AS `DS`, `SF42_Premium_Payer__c`, `Betriebsnummer_ZA__c`, 
`SF42_Month__c`, `SF42_Year__c` FROM `SF42_IZSEvent__c` 
INNER JOIN `izs_event_change` ON `ev_id` = `evid` 
WHERE `SF42_OnlineStatus__c` = "true" AND `RecordTypeId` != "01230000001Ao73AAC" ';

if ((@$_REQUEST['month'] != '') && (@$_REQUEST['year'] != '')) {
  $strSql.= 'AND `SF42_Month__c` = "' .$_REQUEST['month'] .'" ';
  $strSql.= 'AND `SF42_Year__c` = "' .$_REQUEST['year'] .'" ';
} elseif (@$_REQUEST['ec_time'] != '') {
  $arrDay = explode('.', $_REQUEST['ec_time']);
  $strStartDay = mktime(0, 0, 0, $arrDay[1], $arrDay[0], $arrDay[2]);
  $strSql.= 'AND `ec_time` > "' .$strStartDay .'" ';
} elseif (date('H') != 23) {
  $strToday = date('Y-m-d') .' 00:00:00';
  $strSql.= 'AND `ec_time` > "' .$strToday .'" ';
} else {
  $strWeek = mktime(0, 0, 0, date('m'), (date('d') - 7), date('Y'));
  $strSql.= 'AND `ec_time` > "' .$strWeek .'" ';
}

if (@$_REQUEST['ppayer'] != '') {
  $strSql.= 'AND `SF42_Premium_Payer__c` LIKE "%' .$_REQUEST['ppayer'] .'%" ';
}

$strSql.= '
GROUP BY `Betriebsnummer_ZA__c`, `SF42_Month__c`, `SF42_Year__c` 
ORDER BY `SF42_Premium_Payer__c`, `Betriebsnummer_ZA__c`, `SF42_Year__c` DESC, `SF42_Month__c` DESC';

$arrResult = MySQLStatic::Query($strSql);

//echo $strSql; die();

$arrAmpel = array();

if (count($arrResult) > 0) {
  
  foreach ($arrResult as $intKey => $arrProvider) {
    
    $startM = microtime(true);   //ZEIT
    
    $strSql1 = 'SELECT `SF42_EventStatus__c` FROM `SF42_IZSEvent__c` WHERE `Betriebsnummer_ZA__c` = "' .$arrProvider['Betriebsnummer_ZA__c'] .'" ';
    $strSql1.= 'AND `SF42_Month__c` = ' .$arrProvider['SF42_Month__c'] .' AND `SF42_Year__c` = ' .$arrProvider['SF42_Year__c'] .' ';
    $strSql1.= 'AND `SF42_OnlineStatus__c` = "true" AND `RecordTypeId` != "01230000001Ao73AAC" ';

    //echo $strSql1; die();

    $arrResult1 = MySQLStatic::Query($strSql1);
    
    if (count($arrResult1) > 0) {
      
      $arrProviderEvent = array();
      
      foreach ($arrResult1 as $intKey1 => $arrEvent) {
        
        if (empty($arrProviderEvent[$arrEvent['SF42_EventStatus__c']])) {
          $arrProviderEvent[$arrEvent['SF42_EventStatus__c']] = 0;
        }
        
        $arrProviderEvent[$arrEvent['SF42_EventStatus__c']]++;
        
      }
      
      $arrProvider['Id'] = $arrPp[$arrProvider['Betriebsnummer_ZA__c']];
      
      if (!isset($arrProviderEvent['enquired'])) {
        $arrProviderEvent['enquired'] = 0;
      }
      if (!isset($arrProviderEvent['OK'])) {
        $arrProviderEvent['OK'] = 0;
      }
      if (!isset($arrProviderEvent['not OK'])) {
        $arrProviderEvent['not OK'] = 0;
      }
      if (!isset($arrProviderEvent['no Feedback'])) {
        $arrProviderEvent['no Feedback'] = 0;
      }
      if (!isset($arrProviderEvent['no result'])) {
        $arrProviderEvent['no result'] = 0;
      }
      if (!isset($arrProviderEvent['gestundet'])) {
        $arrProviderEvent['gestundet'] = 0;
      }
    
      if (($arrProviderEvent['enquired'] > 0) || ($arrProviderEvent['OK'] > 0) || ($arrProviderEvent['not OK'] > 0) || ($arrProviderEvent['no Feedback'] > 0) || ($arrProviderEvent['no result'] > 0) || ($arrProviderEvent['gestundet'] > 0)) {
        $arrAmpel[$arrProvider['Id']][$arrProvider['SF42_Year__c']][$arrProvider['SF42_Month__c']] = 'Yellow';
      
        if ($arrProviderEvent['not OK'] > 0) {
          $arrAmpel[$arrProvider['Id']][$arrProvider['SF42_Year__c']][$arrProvider['SF42_Month__c']] = 'Red';
        } elseif (($arrProviderEvent['enquired'] == 0) && ($arrProviderEvent['not OK'] == 0) && ($arrProviderEvent['no Feedback'] == 0) && ($arrProviderEvent['no result'] == 0) && ($arrProviderEvent['gestundet'] == 0)) {
          $arrAmpel[$arrProvider['Id']][$arrProvider['SF42_Year__c']][$arrProvider['SF42_Month__c']] = 'Green';
        }
      
      }
      
    }
    
  }

}

if (@$_REQUEST['ec_time'] != '') {
  //print_r($arrAmpel); die();
}

$arrUpdates = array();
$arrInserts = array();

if (count($arrAmpel) > 0) {
  foreach($arrAmpel as $strProvider => $arrAmpelList) {
    if (count($arrAmpelList) > 0) {
      foreach($arrAmpelList as $intYear => $arrMonth) {
        if (count($arrMonth) > 0) {
          foreach($arrMonth as $intMonth => $strAmpel) {
            
            $strSql = 'SELECT `pe_id`, `SF42_Period_Status__c` FROM `Period__c` WHERE `Month__c` = "' .$intMonth .'" ';
            $strSql.= 'AND `SF42_Premium_Payer__c` = "' .$strProvider .'" AND `Year__c` = "' .$intYear .'"';
            $arrResult = MySQLStatic::Query($strSql);
            
            if ($arrResult[0]['pe_id'] != '') {
              
              if ($arrResult[0]['SF42_Period_Status__c'] != $strAmpel) {
                $strSqlUpd = 'UPDATE `Period__c` SET `SF42_Period_Status__c` = "' .$strAmpel .'" ';
                $strSqlUpd.= 'WHERE `pe_id` = "' .$arrResult[0]['pe_id'] .'"';
                //echo $strSqlUpd .chr(10);
                $arrResultIns = MySQLStatic::Update($strSqlUpd);
                $arrUpdates[] = $strProvider .' (' .$intMonth .'/' .$intYear .')' .chr(10);
              }              
            
            } else {
              
              $strSqlIns = 'INSERT INTO `Period__c` (`pe_id`, `Id`, `OwnerId`, `IsDeleted`, `Name`, ';
              $strSqlIns.= '`CreatedDate`, `CreatedById`, `LastModifiedDate`, `LastModifiedById`, `SystemModstamp`, ';
              $strSqlIns.= '`LastActivityDate`, `Month__c`, `SF42_Period_Name__c`, `SF42_Period_Status__c`, ';
              $strSqlIns.= '`SF42_Premium_Payer__c`, `Year__c`, `SF42_Number_of_Red_Events__c`, `SF42_Number_of_Yellow_Events__c`, ';
              $strSqlIns.= '`SF42_period_ID_old__c` ';
              $strSqlIns.= ') VALUES(NULL, "", "", "false", NULL, NOW(), "", NOW(), "", NOW(), "0000-00-00", ';
              $strSqlIns.= '' .$intMonth .', NULL, "' .$strAmpel .'", "' .$strProvider .'", ' .$intYear .', 0, 0, NULL)';
              //echo $strSqlIns .chr(10);
              $arrResultIns = MySQLStatic::Insert($strSqlIns);
              $arrInserts[] = $strProvider .' (' .$intMonth .'/' .$intYear .')' .chr(10);
            }

            //echo 'Year: ' .$intYear .', Month: ' .$intMonth .', Ampel: ' .$strAmpel .chr(10);
          }
        }
      }
    }
  }
}


$end = microtime(true);     //ZEIT
$laufzeit = $end - $start;  //ZEIT
echo "Laufzeit: " .round($laufzeit/60, 2) ." Minuten!" .'<br />' .chr(10); //ZEIT

$strMessage = count($arrAmpel) .' changed Events.' .chr(10) .chr(10);
$strMessage.= count($arrUpdates) .' Updates';
if (count($arrUpdates) > 0) {
  $strMessage.= ':' .chr(10);
  $strMessage.= ' - ' .implode(chr(10) .' - ', $arrUpdates) .chr(10) .chr(10);
} else {
  $strMessage.= chr(10) .chr(10);
}

$strMessage.= count($arrInserts) .' Inserts';
if (count($arrInserts) > 0) {
  $strMessage.= ':' .chr(10);
  $strMessage.= ' - ' .implode(chr(10) .' - ', $arrInserts) .chr(10) .chr(10);
} else {
  $strMessage.= chr(10) .chr(10);
}

$strMessage.= 'Laufzeit: ' .sprintf('%02d:%02d', floor($laufzeit/60), $laufzeit % 60) .' Minuten.';

if (strstr(__FILE__, 'web_02') != false) {
  //mail('f.luetgen@gmx.de', 'Ampel Cron', $strMessage);
}

?>