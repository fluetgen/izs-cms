<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

require_once('../assets/classes/class.mysql.php');
require_once('../assets/classes/class.database.php');
require_once('functions.inc.php');

$strOutput = '';


if (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'red')) {
    
  $strSql = 'DELETE FROM `cms_right` WHERE `cr_cl_id` = "' .$_REQUEST['id'] .'"';
  $result = MySQLStatic::Query($strSql);  
  
  if (count($_REQUEST['ri']) > 0) {
  
    foreach ($_REQUEST['ri'] as $intPage => $strValue) {
      
      if ($strValue != 'undefined') {
        
        if ($strValue == 'true') {
          $intAcc = 1;
        } else {
          $intAcc = 0;
        }
      
        $strSqlR = 'INSERT INTO `cms_right` (`cr_cl_id`, `cr_cp_id`, `cr_access`) VALUES (';
        $strSqlR.= '"' .$_REQUEST['id'] .'", "' .$intPage .'", "' .$intAcc .'")';
        $idR = MySQLStatic::Insert($strSqlR);
      
      }
      
    }  
  
  }
  
  echo 1;

}

if (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'udel')) {
  
  $strSql = 'UPDATE `cms_login` SET `cl_deleted` = 1 WHERE `cl_id` = "' .$_REQUEST['id'] .'"';
  $result = MySQLStatic::Query($strSql);  
  
  $strSql = 'DELETE FROM `cms_right` WHERE `cr_cl_id` = "' .$_REQUEST['id'] .'"';
  $result = MySQLStatic::Query($strSql);  
  
  echo 1;

}

if (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'rall')) {

  $arrCsv = array(
    0 => array('')
  );

  //Struktur

  $strSql = "SELECT * FROM `cms_page` WHERE `cp_parent` = 0 ORDER BY `cp_ord`";
  $arrResult = MySQLStatic::Query($strSql);
  
  if (count($arrResult) > 0) {
  
    foreach ($arrResult as $arrPage) {
      
      $arrCsv[] = array($arrPage['cp_name']);
  
      $strSql2 = "SELECT * FROM `cms_page` WHERE `cp_parent` = " .$arrPage['cp_id'] ." ORDER BY `cp_ord`";
      $arrResult2 = MySQLStatic::Query($strSql2);
      
      if (count($arrResult2) > 0) {
  
        foreach ($arrResult2 as $arrPage2) {
              
          $arrCsv[] = array('  ' .$arrPage2['cp_name']);
  
        }

      }
      
    }
    
  }

  $strSqlUser = "SELECT * FROM `cms_login` ORDER BY `cl_id`";
  $arrResultUser = MySQLStatic::Query($strSqlUser);

  foreach ($arrResultUser as $arrUser) {

    $strSql = "SELECT * FROM `cms_page` WHERE `cp_parent` = 0 ORDER BY `cp_ord`";
    $arrResult = MySQLStatic::Query($strSql);
    
    $intLine = 0;
    
    if (count($arrResult) > 0) {
    
      $arrCsv[$intLine][] = $arrUser['cl_first'] .' ' .$arrUser['cl_last'] .'' ;
    
      foreach ($arrResult as $arrPage) {

        $intLine++;
        
        $strSqlR = "SELECT * FROM `cms_right` WHERE `cr_cl_id` = '" .$arrUser['cl_id'] ."' AND `cr_cp_id` = '" .$arrPage['cp_id'] ."' AND `cr_access` = 1";
        $arrResultR = MySQLStatic::Query($strSqlR);
        
        if (count($arrResultR) > 0) {
          $arrCsv[$intLine][] = 'Ja';
        } else {
          $arrCsv[$intLine][] = 'Nein';
        }
    
        $strSql2 = "SELECT * FROM `cms_page` WHERE `cp_parent` = " .$arrPage['cp_id'] ." ORDER BY `cp_ord`";
        $arrResult2 = MySQLStatic::Query($strSql2);
        
        if (count($arrResult2) > 0) {
    
          foreach ($arrResult2 as $arrPage2) {
  
            $intLine++;
          
            $strSqlR2 = "SELECT * FROM `cms_right` WHERE `cr_cl_id` = '" .$arrUser['cl_id'] ."' AND `cr_cp_id` = '" .$arrPage2['cp_id'] ."' AND `cr_access` = 1";
            $arrResultR2 = MySQLStatic::Query($strSqlR2);

            if (count($arrResultR2) > 0) {
              $arrCsv[$intLine][] = 'Ja';
            } else {
              $arrCsv[$intLine][] = 'Nein';
            }
    
          }


        }
        
      }

      
    }
  
  
  }

  array_to_csv_download($arrCsv);

  //echo print_r($arrCsv);


}


if (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'redit')) {

  $strSql = "SELECT * FROM `cms_page` WHERE `cp_parent` = 0 ORDER BY `cp_ord`";
  $arrResult = MySQLStatic::Query($strSql);
  
  $strNav = '';
  
  if (count($arrResult) > 0) {
  
    $strNav.= '          <input type="hidden" name="user" id="user" value="' .$_REQUEST['id'] .'">' .chr(10);
    $strNav.= '          <div id="urights">' .chr(10);
    $strNav.= '            <ul>' .chr(10);
  
    foreach ($arrResult as $arrPage) {
      
      $strSqlR = "SELECT * FROM `cms_right` WHERE `cr_cl_id` = '" .$_REQUEST['id'] ."' AND `cr_cp_id` = '" .$arrPage['cp_id'] ."' AND `cr_access` = 1";
      $arrResultR = MySQLStatic::Query($strSqlR);
      
      if (count($arrResultR) > 0) {
        $strChecked = ' checked="checked"';
      } else {
        $strChecked = '';
      }

      $strNav.= '                  <li><input type="checkbox" class="page" name="page_' .$arrPage['cp_id'] .'" id="page_' .$arrPage['cp_id'] .'" value="1"' .$strChecked .'> ' .$arrPage['cp_name'] .'';
  
      $strSql2 = "SELECT * FROM `cms_page` WHERE `cp_parent` = " .$arrPage['cp_id'] ." ORDER BY `cp_ord`";
      $arrResult2 = MySQLStatic::Query($strSql2);
      
      if (count($arrResult2) > 0) {
  
        $strNav.= chr(10);
        $strNav.= '                    <ul>' .chr(10);
  
        foreach ($arrResult2 as $arrPage2) {
          
          $strSqlR2 = "SELECT * FROM `cms_right` WHERE `cr_cl_id` = '" .$_REQUEST['id'] ."' AND `cr_cp_id` = '" .$arrPage2['cp_id'] ."' AND `cr_access` = 1";
          $arrResultR2 = MySQLStatic::Query($strSqlR2);

          if (count($arrResultR2) > 0) {
            $strChecked2 = ' checked="checked"';
          } else {
            $strChecked2 = '';
          }
                
          $strNav.= '                      <li><input type="checkbox"class="page" name="page_' .$arrPage2['cp_id'] .'" id="page_' .$arrPage2['cp_id'] .'" value="1"' .$strChecked2 .'> ' .$arrPage2['cp_name'] .'</li>' .chr(10);
  
        }
  
        $strNav.= '                    </ul>' .chr(10);
        $strNav.= '                  </li>' .chr(10);
  
      } else {
        $strNav.= '</li>' .chr(10);
      }
      
    }
    
    $strNav.= '            </ul>' .chr(10);
    $strNav.= '         </div>' .chr(10);
  
    echo $strNav;
    
  }

}


if (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'usave')) {

  //print_r($_REQUEST);

  $strSql = 'SELECT * FROM `cms_login` WHERE `cl_id` = "' .$_REQUEST['id'] .'"';
  $result = MySQLStatic::Query($strSql);

  //print_r($result);

  $strSqlClock = 'SELECT * FROM `izs_clock_user` WHERE `cu_id` = "' .$_REQUEST['cu_id'] .'"';
  $resultClock = MySQLStatic::Query($strSqlClock);

  $strSqlClockInfo = 'SELECT * FROM `izs_clock_user_info` WHERE `ci_id` = "' .$_REQUEST['ci_id'] .'";';
  $resultClockInfo = MySQLStatic::Query($strSqlClockInfo);

  $strSqlMaginal = 'SELECT * FROM `izs_maginal` WHERE `im_id` = "' .$_REQUEST['im_id'] .'"';
  $arrResultMaginal = MySQLStatic::Query($strSqlMaginal);

  $arrCrmUser = MySQLStatic::Query('SELECT * FROM `User` WHERE `Id` = "' .$_REQUEST['cl_ks_id'] .'"');

  //print_r($_REQUEST); die();
  
  if ($_REQUEST['ad'] == 'true') {
    $intAdmin = 1;
  } else {
    $intAdmin = 0;
  }  
  
  if (($_REQUEST['id'] != '') && ($_REQUEST['id'] != 0)) {
    $strSql = 'UPDATE `cms_login` SET `cl_first` = "' .$_REQUEST['fi'] .'", ';
    $strSql.= '`cl_last` = "' .$_REQUEST['la'] .'", `cl_mail` = "' .$_REQUEST['ma'] .'", ';
    $strSql.= '`cl_user` = "' .$_REQUEST['un'] .'", `cl_admin` = "' .$intAdmin .'" ';
    if ($_REQUEST['pa'] != '') {
      $strSql.= ', `cl_pass` = MD5("' .$_REQUEST['pa'] .'") ';
    }
    $strSql.= 'WHERE `cl_id` = "' .$_REQUEST['id'] .'"';
    $resultUp = MySQLStatic::Query($strSql);
  } else {
    $strSql = 'INSERT INTO `cms_login` (`cl_first`, `cl_last`, `cl_mail`, ';
    $strSql.= '`cl_user`, `cl_admin`, `cl_pass`';
    $strSql.= ') VALUES ("' .$_REQUEST['fi'] .'", "' .$_REQUEST['la'] .'", "' .$_REQUEST['ma'] .'", ';
    $strSql.= '"' .$_REQUEST['un'] .'", "' .$intAdmin .'", MD5("' .$_REQUEST['pa'] .'"))';
    $$_REQUEST['id'] = MySQLStatic::Insert($strSql);
    
    $strSql = 'SELECT `cp_id` FROM `cms_page`';
    $resultCp = MySQLStatic::Query($strSql);
    
    if (count($resultCp) > 0) {
      foreach ($resultCp as $arrPage) {
        $strSqlR = 'INSERT INTO `cms_right` (`cr_cl_id`, `cr_cp_id`, `cr_access`) VALUES (';
        $strSqlR.= '"' .$id .'", "' .$arrPage['cp_id'] .'", "1")';
        $idR = MySQLStatic::Insert($strSqlR);
      }
    }
    
  }

  if ($_REQUEST['ci_id'] != '') {
    if (count($resultClock) > 0) {
      $strSqlCu = 'UPDATE `izs_clock_user_info` SET `ci_cu_id` = "' .$_REQUEST['cu_id'] .'", `ci_cl_id` = "' .$_REQUEST['id'] .'" WHERE `ci_id` = "' .$_REQUEST['ci_id'] .'"';
      $arrResCu = MySQLStatic::Insert($strSqlCu);
    } else {
      $strSqlCu = 'INSERT INTO `izs_clock_user_info` (`ci_id`, `ci_cu_id`, `ci_cl_id`, `ci_company`) VALUES (NULL, "' .$_REQUEST['cu_id'] .'", "' .$_REQUEST['id'] .'", "IZS")';
      $_REQUEST['ci_id'] = MySQLStatic::Insert($strSqlCu);      
    }
  }

  //var_dump($_REQUEST['cl_crm']); die();

  if ($_REQUEST['cl_four'] == 'true') {
    $strFour = '450er';

    if ($_REQUEST['im_id'] != '') {
      $strSqlCu = 'UPDATE `izs_maginal` SET `im_cl_id` = "' .$_REQUEST['id'] .'", `im_clock_id` = "' .$resultClock[0]['cu_clock_id'] .'", ';
      $strSqlCu.= '`im_last` = "' .$result[0]['cl_last'] .'", `im_first` = "' .$result[0]['cl_first'] .'", `im_company` = "IZS", ';
      $strSqlCu.= '`im_mobile` = "' .$_REQUEST['im_mobile'] .'", `im_max_hours` = "' .$_REQUEST['im_max_hours'] .'", ';
      $strSqlCu.= '`im_application` = "' .$_REQUEST['im_application'] .'", `im_not_avail` = "' .$_REQUEST['im_not_avail'] .'" ';
      $strSqlCu.= 'WHERE `im_id` = "' .$_REQUEST['im_id'] .'"';
      $arrResCu = MySQLStatic::Insert($strSqlCu);
    } else {
      $strSqlCu = 'INSERT INTO `izs_maginal` (`im_id`, `im_cl_id`, `im_clock_id`, `im_last`, `im_first`, `im_company`, `im_mobile`, `im_max_hours`, `im_application`, `im_not_avail`) VALUES ';
      $strSqlCu.= '(NULL, "' .$_REQUEST['id'] .'", "' .$resultClock[0]['cu_clock_id'] .'", "' .$result[0]['cl_last'] .'", "' .$result[0]['cl_first'] .'", "IZS", ';
      $strSqlCu.= '"' .$_REQUEST['im_mobile'] .'", "' .$_REQUEST['im_max_hours'] .'", "' .$_REQUEST['im_application'] .'", "' .$_REQUEST['im_not_avail'] .'")';
      $_REQUEST['im_id'] = MySQLStatic::Insert($strSqlCu);      
    }

  } else {
    $strFour = '';

    if ($_REQUEST['im_id'] != '') {
      $strSqlCu = 'DELETE FROM `izs_maginal` WHERE `im_id` = "' .$_REQUEST['im_id'] .'"';
      $arrResCu = MySQLStatic::Insert($strSqlCu);
    }
  
  }


  $strSqlCu = 'SELECT `ci_id` FROM `izs_clock_user_info` WHERE `ci_cu_id` = "' .$_REQUEST['cu_id'] .'"';
  $arrResCu = MySQLStatic::Query($strSqlCu);
  if (count($arrResCu) > 0) {
    $strSqlCu = 'UPDATE `izs_clock_user_info` SET `ci_type` = "' .$strFour .'" WHERE `ci_id` = "' .$_REQUEST['ci_id'] .'"';
    $arrResCu = MySQLStatic::Update($strSqlCu);
  } else {
    $strSqlCu = 'INSERT INTO `izs_clock_user_info` (`ci_id`, `ci_cu_id`, `ci_cl_id`, `ci_type`, `ci_company`) VALUES (NULL, "' .$_REQUEST['cu_id'] .'", "' .$_REQUEST['id'] .'", "' .$strFour .'", "IZS")';
    $arrResCu = MySQLStatic::Insert($strSqlCu);
  }


  //CRM!!!
  if ($_REQUEST['cl_crm'] == 'true') {
    if ($result[0]['cl_ks_id'] == '') {

    }
  } else {
    $strSqlCrm = 'UPDATE `cms_login` SET `cl_ks_id` = "" WHERE `cl_id` = "' .$_REQUEST['id'] .'"';
  }
  
  echo 1;

}

if (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'usearch')) {
  
  $strSql = 'SELECT * FROM `cms_login` WHERE `cl_user` = "' .$_REQUEST['un'] .'"';
  $result = MySQLStatic::Query($strSql);
  
  if (count($result) > 0) {
    echo 0;
  } else {
    echo 1;
  }

}


if (isset($_REQUEST['ac']) && (($_REQUEST['ac'] == 'uedit') || ($_REQUEST['ac'] == 'ucreate'))) {
  
  $strOutput.= '<p id="validateTips"></p>';

  $strOutput.= '<div class="pbody">';
  
  if ($_REQUEST['ac'] == 'uedit') {
    $strSql = 'SELECT * FROM `cms_login` WHERE `cl_id` = "' .$_REQUEST['id'] .'"';
    $result = MySQLStatic::Query($strSql);

    $strSqlClockInfo = 'SELECT * FROM `izs_clock_user_info` WHERE `ci_cl_id` = "' .$_REQUEST['id'] .'";';
    $resultClockInfo = MySQLStatic::Query($strSqlClockInfo);
    if (count($resultClockInfo) == 0) {
      $resultClockInfo[0]['ci_cu_id'] = 0;
    }

    if ($resultClockInfo[0]['ci_cu_id'] != 0) {
      $strSqlClock = 'SELECT * FROM `izs_clock_user` WHERE `cu_id` = "' .$resultClockInfo[0]['ci_cu_id'] .'"';
      $resultClock = MySQLStatic::Query($strSqlClock);
    } else {
      $strSqlClock = 'SELECT * FROM `izs_clock_user` WHERE`cu_email` = "' .$result[0]['cl_mail'] .'"';
      $resultClock = MySQLStatic::Query($strSqlClock);      
    } 
    
    if (count($resultClock) == 0) {
      $strSqlClock = 'SELECT * FROM `izs_clock_user` WHERE`cu_name` = "' .$result[0]['cl_first'] .' ' .$result[0]['cl_last'] .'"';
      $resultClock = MySQLStatic::Query($strSqlClock);
    }

    if (($resultClockInfo[0]['ci_cu_id'] == 0) && (count($resultClock) > 0)) {
      $strSqlClockInfo = 'SELECT * FROM `izs_clock_user_info` WHERE `ci_cu_id` = "' .$resultClock[0]['cu_id'] .'";';
      $resultClockInfo = MySQLStatic::Query($strSqlClockInfo);
    }

    $strSqlMaginal = 'SELECT * FROM `izs_maginal` WHERE `im_cl_id` = "' .$_REQUEST['id'] .'"';
    $arrResultMaginal = MySQLStatic::Query($strSqlMaginal);

  }

  $arrUserRoleList = MySQLStatic::Query('SELECT `Id`, `Name` FROM `UserRole` ORDER BY `Id`');
  $resultClockList = MySQLStatic::Query('SELECT `cu_id`, `cu_name` FROM `izs_clock_user` ORDER BY `cu_name`');

  $strOutput.= '<form method="post" action="" autocomplete="false">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);
  $strOutput.= '<input type="hidden" name="user" id="user" value="' .$_REQUEST['id'] .'">' .chr(10);
  $strOutput.= '<input type="hidden" name="cu_id" id="cu_id" value="' .@$resultClock[0]['cu_id'] .'">' .chr(10);
  $strOutput.= '<input type="hidden" name="ci_id" id="ci_id" value="' .@$resultClockInfo[0]['ci_id'] .'">' .chr(10);
  $strOutput.= '<input type="hidden" name="im_id" id="im_id" value="' .@$arrResultMaginal[0]['im_id'] .'">' .chr(10);
  $strOutput.= '<input type="hidden" name="cl_ks_id" id="cl_ks_id" value="' .@$result[0]['cl_ks_id'] .'">' .chr(10);
  
  $strOutput.= '<table cellpadding="1">' .chr(10);
  $strOutput.= '<tbody>' .chr(10);
  
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td colspan="2" style="padding: 0 0 10px 0; font-size: 120%"><strong>CMS:</strong></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);  

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td style="width: 130px;">Vorname: </td>' .chr(10);
  $strOutput.= '    <td><input type="text" id="cl_first" style="width: 300px;" value="' .@$result[0]['cl_first'] .'"></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);
  
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Nachname: </td>' .chr(10);
  $strOutput.= '    <td><input type="text" id="cl_last" style="width: 300px;" value="' .@$result[0]['cl_last'] .'"></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);
  
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Benutzername: </td>' .chr(10);
  $strOutput.= '    <td><input type="text" id="cl_user" id="cl_user" style="width: 300px;" value="' .@$result[0]['cl_user'] .'" rel="' .@$result[0]['cl_user'] .'"></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);
  
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>E-Mail: </td>' .chr(10);
  $strOutput.= '    <td><input type="text" id="cl_mail" style="width: 300px;" value="' .@$result[0]['cl_mail'] .'"></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);
  
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Passwort: </td>' .chr(10);
  $strOutput.= '    <td><input type="password" autocomplete="new-password" name="cl_pass" id="cl_pass" style="width: 300px;" value="' .@$_REQUEST['cl_pass'] .'"></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);
  
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Passwort wiederh.: </td>' .chr(10);
  $strOutput.= '    <td><input type="password" autocomplete="new-password" name="cl_pass2" id="cl_pass2" style="width: 300px;" value="' .@$_REQUEST['cl_pass2'] .'"></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);
  
  if (@$result[0]['cl_admin'] == 1) {
    $strCheck = ' checked="checked"';
  } else {
    $strCheck = '';
  }
  
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Admin: </td>' .chr(10);
  $strOutput.= '    <td><input type="checkbox" id="cl_admin" value="1"' .$strCheck .'></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td colspan="2" style="padding: 20px 0 10px 0; font-size: 120%"><strong>Clockodo:</strong></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);  
  
  $strOutput.= '  <tr">' .chr(10);
  $strOutput.= '    <td>Benutzer: </td>' .chr(10);
  $strOutput.= '    <td><select name="ci_cu_id" id="ci_cu_id" style="width: 300px;">' .chr(10);
  $strOutput.= '    <option value=""> - Bitte auswählen - </option>' .chr(10);

  if (count($resultClockList) > 0) {
    foreach ($resultClockList as $intKey => $arrClockUser) {
      $strSelected = '';
      if ($arrClockUser['cu_id'] == @$resultClockInfo[0]['ci_cu_id'] ) {
        $strSelected = 'selected="selected"';
      }
      $strOutput.= '    <option value="' .$arrClockUser['cu_id'] .'"' .$strSelected .'> ' .$arrClockUser['cu_name'] .'</option>' .chr(10);
    }
  }
  
  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);
  
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td colspan="2" style="padding: 20px 0 10px 0; font-size: 120%"><strong>CRM:</strong></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);  

  $strCssAdd = 'none';

  $strCheckKs = '';
  if (@$result[0]['cl_ks_id'] != '') {
    $strCheckKs = ' checked="checked"';
    $strCssAdd  = 'table-row';

    $strSqlRole = 'SELECT `UserRoleId` FROM `User` WHERE `Id` = "' .$result[0]['cl_ks_id'] .'"';
    $arrRole = MySQLStatic::Query($strSqlRole);

  }

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Zugriff erlauben: </td>' .chr(10);
  $strOutput.= '    <td><input type="checkbox" id="cl_crm" value="1"' .$strCheckKs .'></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $strOutput.= '  <tr class="crm" style="display: ' .$strCssAdd .'">' .chr(10);
  $strOutput.= '    <td>Benutzer-Rolle: </td>' .chr(10);
  $strOutput.= '    <td><select name="roleId" id="roleId" style="width: 300px;">' .chr(10);
  $strOutput.= '    <option value=""> - Bitte auswählen - </option>' .chr(10);

  if (count($arrUserRoleList) > 0) {
    foreach ($arrUserRoleList as $intKey => $arrUserRole) {
      $strSelected = '';
      if (@$arrRole[0]['UserRoleId'] == $arrUserRole['Id'] ) {
        $strSelected = 'selected="selected"';
      }
      $strOutput.= '    <option value="' .$arrUserRole['Id'] .'"' .$strSelected .'> ' .$arrUserRole['Name'] .'</option>' .chr(10);
    }
  }
  
  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td colspan="2" style="padding: 20px 0 10px 0; font-size: 120%"><strong>450er:</strong></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);  

  $strCssAdd = 'none';

  $strCheck45 = '';
  if (isset($resultClockInfo[0]['ci_type']) && ($resultClockInfo[0]['ci_type'] == '450er')) {
    $strCheck45 = ' checked="checked"';
    $strCssAdd = 'table-row';
  }

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>450er: </td>' .chr(10);
  $strOutput.= '    <td><input type="checkbox" id="cl_four" value="1"' .$strCheck45 .'></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  if (@$arrResultMaginal[0]['im_max_hours'] == 0) {
    $arrResultMaginal[0]['im_max_hours'] = '';
  }

  //maximal Telefon	einsetzbar	nicht verfügbar
  $strOutput.= '  <tr class="izs450" style="display: ' .$strCssAdd .'">' .chr(10);
  $strOutput.= '    <td>Maximalstunden: </td>' .chr(10);
  $strOutput.= '    <td><input type="text" id="im_max_hours" style="width: 50px;" value="' .@$arrResultMaginal[0]['im_max_hours'] .'"> (Kein Eintrag = Maximale Stundenzahl)</td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $strOutput.= '  <tr class="izs450" style="display: ' .$strCssAdd .'">' .chr(10);
  $strOutput.= '    <td>Telefon: </td>' .chr(10);
  $strOutput.= '    <td><input type="text" id="im_mobile" style="width: 300px;" value="' .@$arrResultMaginal[0]['im_mobile'] .'"></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $strOutput.= '  <tr class="izs450" style="display: ' .$strCssAdd .'">' .chr(10);
  $strOutput.= '    <td>Einsetzbar: </td>' .chr(10);
  $strOutput.= '    <td><textarea name="text" id="im_application" style="width: 300px; height: 80px;">' .@$arrResultMaginal[0]['im_application'] .'</textarea></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $strOutput.= '  <tr class="izs450" style="display: ' .$strCssAdd .'">' .chr(10);
  $strOutput.= '    <td>Nicht verfügbar: </td>' .chr(10);
  $strOutput.= '    <td><textarea name="text" id="im_not_avail" style="width: 300px; height: 80px;">' .@$arrResultMaginal[0]['im_not_avail'] .'</textarea></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);



  $strOutput.= '</tbody>' .chr(10);
  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);

  $strOutput.= '</div>' .chr(10);

  echo $strOutput;  
}

if (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'udelete')) {

  $strSql = 'SELECT * FROM `cms_login` WHERE `cl_id` = "' .$_REQUEST['id'] .'"';
  $result = MySQLStatic::Query($strSql);

  $strOutput.= '<div class="pbody">';
  $strOutput.= '<input type="hidden" name="user" id="user" value="' .$_REQUEST['id'] .'">' .chr(10);
  $strOutput.= '<p>Benutzer "' .$result[0]['cl_user'] .'" wirklich deaktivieren?</p>';  
  $strOutput.= '</div>';

  echo $strOutput;  
}


?>