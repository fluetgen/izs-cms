<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

require_once('../assets/classes/class.mysql.php');

//print_r($_REQUEST); die();

$strMailTemplate = file_get_contents('infoservice/infoservice.html');

if (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'create')) {

    $strSql = 'SELECT * FROM `_cron_url` WHERE `cu_status` = 1 AND `cu_url` = "' .$_REQUEST['url'] .'"';
    $arrSql = MySQLStatic::Query($strSql);

    if (count($arrSql) == 0) {
        $strSql = 'INSERT INTO `_cron_url` (`cu_id`, `cu_hash`, `cu_url`, `cu_time`, `cu_status`) VALUES (NULL, "' .$_REQUEST['hash'] .'", "' .$_REQUEST['url'] .'", NOW(), 1)';
        $intCt_id = MySQLStatic::Insert($strSql);
    }


    // WRITE SUBLINE
    $strSql = 'SELECT `Betriebsnummer_ZA__c` FROM `SF42_IZSEvent__c` WHERE `group_id` = "' . $_REQUEST['id'] . '" AND `Beitragsmonat__c` = "' . $_REQUEST['m']  . '" AND `Beitragsjahr__c` = "' . $_REQUEST['y']  . '" AND `SF42_EventStatus__c` IN ("not OK", "OK") GROUP BY `Betriebsnummer_ZA__c`';
    $arrAccountList = MySQLStatic::Query($strSql);
    
    if (count($arrAccountList) > 0) {

        foreach ($arrAccountList as $intKey => $arrAccount) {
            
            $strSql1 = 'SELECT `Id`, `SF42_Payment_Status__c`, `Subline__c` FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$arrAccount['Betriebsnummer_ZA__c'] .'"';
            $arrSql1 = MySQLStatic::Query($strSql1);

            $strSql = 'DELETE FROM `izs_infoservice_status` WHERE `is_group_id` = "' . $_REQUEST['id'] . '" AND `is_year` = "' . $_REQUEST['y'] . '" AND `is_month` = "' . $_REQUEST['m']  . '" AND `is_account_id` = "' .MySQLStatic::esc($arrSql1[0]['Id']) .'"';
            $arrSql = MySQLStatic::Query($strSql);
            
            $strSql = 'INSERT INTO `izs_infoservice_status` (`is_id`, `is_group_id`, `is_account_id`, `is_year`, `is_month`, `is_status`, `is_desc`) ';
            $strSql.= 'VALUES (NULL, "' .MySQLStatic::esc($_REQUEST['id']) .'", "' .MySQLStatic::esc($arrSql1[0]['Id']) .'", "' .MySQLStatic::esc($_REQUEST['y']) .'", "' .MySQLStatic::esc($_REQUEST['m']) .'", ';
            $strSql.= '"' .MySQLStatic::esc(strtolower($arrSql1[0]['SF42_Payment_Status__c'])) .'", "' .MySQLStatic::esc($arrSql1[0]['Subline__c']) .'");';
            $intSql = MySQLStatic::Insert($strSql);

        }

    }

    echo date('d.m.Y H:i');
} 

if (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'status')) {

    $arrReturn = array();

    $strSql = 'SELECT `izs_pruefbericht`.* FROM `izs_pruefbericht`, ';
    $strSql.= '(SELECT `pr_group_id`, MAX(`pr_time`) AS `pr_time` FROM `izs_pruefbericht` GROUP BY `pr_group_id`) `latest` ';
    $strSql.= 'WHERE `izs_pruefbericht`.`pr_group_id` = `latest`.`pr_group_id` ';
    $strSql.= 'AND `izs_pruefbericht`.`pr_time` = `latest`.`pr_time` AND `pr_month` = "' .$_REQUEST['m'] .'" AND `pr_year` = "' .$_REQUEST['y'] .'"';
    $arrSql = MySQLStatic::Query($strSql);

    if (count($arrSql) > 0) {

        foreach ($arrSql as $intKey => $arrReport) {

            $arrReturn[$arrReport['pr_group_id']] = '<span class="hidden">' .strtotime($arrReport['pr_time']) .'</span><a href="https://www.izs.de/sv-pruefbericht/' .$arrReport['pr_group_id'] .'" target="_blank" id="cre_' .$arrReport['pr_group_id'] .'">' .date('d.m.Y H:i', strtotime($arrReport['pr_time'])) .'<a>';

        }

    }

    //pending?
    $arrPendingList = array();

    $strSql = 'SELECT * FROM `_cron_url` WHERE `cu_status` IN (1, 4) AND `cu_url` LIKE "%sv-pruefbericht%"';
    $arrSql = MySQLStatic::Query($strSql);

    if (count($arrSql) > 0) {

        foreach ($arrSql as $intKey => $arrPending) {

            $arrPart = explode('/', $arrPending['cu_url']);
            $arrReturn[$arrPart[2]] = '<a href="javascript:void(0);" target="" id="cre_' .$arrPart[2] .'">pending...<a>';

        }

    }

    echo json_encode($arrReturn);

    //echo date('d.m.Y H:i:s');
} 

if (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'done')) {

    $intDate = time();

    //TODO created aus letztem File setzen
    $strSql = 'INSERT INTO `cms_cert` (`cc_id`, `cc_group_id`, `cc_month`, `cc_year`, `cc_sent`) VALUES (NULL, "' .$_REQUEST['id'] .'", "' .$_REQUEST['m'] .'", "' .$_REQUEST['y'] .'", "' .date('Y-m-d H:i:s', $intDate) .'")';
    $arrRes = MySQLStatic::Insert($strSql);

    $strSql = 'SELECT `Betriebsnummer_ZA__c` FROM `SF42_IZSEvent__c` WHERE `group_id` = "' . $_REQUEST['id'] . '" AND `Beitragsmonat__c` = "' . $_REQUEST['m']  . '" AND `Beitragsjahr__c` = "' . $_REQUEST['y']  . '" AND `SF42_EventStatus__c` IN ("not OK", "OK") GROUP BY `Betriebsnummer_ZA__c`';
    $arrAccountList = MySQLStatic::Query($strSql);
    
    
    if (count($arrAccountList) > 0) {

        foreach ($arrAccountList as $intKey => $arrAccount) {
            
            $strSql1 = 'SELECT `Id`, `SF42_Payment_Status__c`, `Subline__c` FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$arrAccount['Betriebsnummer_ZA__c'] .'"';
            $arrSql1 = MySQLStatic::Query($strSql1);

            $strSql = 'DELETE FROM `izs_infoservice_status` WHERE `is_group_id` = "' . $_REQUEST['id'] . '" AND `is_year` = "' . $_REQUEST['y'] . '" AND `is_month` = "' . $_REQUEST['m']  . '" AND `is_account_id` = "' .MySQLStatic::esc($arrSql1[0]['Id']) .'"';
            $arrSql = MySQLStatic::Query($strSql);
            
            $strSql = 'INSERT INTO `izs_infoservice_status` (`is_id`, `is_group_id`, `is_account_id`, `is_year`, `is_month`, `is_status`, `is_desc`) ';
            $strSql.= 'VALUES (NULL, "' .MySQLStatic::esc($_REQUEST['id']) .'", "' .MySQLStatic::esc($arrSql1[0]['Id']) .'", "' .MySQLStatic::esc($_REQUEST['y']) .'", "' .MySQLStatic::esc($_REQUEST['m']) .'", ';
            $strSql.= '"' .MySQLStatic::esc(strtolower($arrSql1[0]['SF42_Payment_Status__c'])) .'", "' .MySQLStatic::esc($arrSql1[0]['Subline__c']) .'");';
            $intSql = MySQLStatic::Insert($strSql);

        }

    }

    echo date('d.m.Y H:i', $intDate);

} 

if (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'send')) {

    $boolSingle = false;
    if (isset($_REQUEST['single'])) {
        $boolSingle = true;
    }

    $strSql = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$_REQUEST['id'] .'"';
    $arrGrp = MySQLStatic::Query($strSql);

    if (!$boolSingle) {
        $strSql = 'SELECT `E_Mail__c` FROM `Abonnent__c` WHERE `Verleiher_Gruppe__c` = "' .$_REQUEST['id'] .'" AND `aktiviert__c` = "true"';
        $arrAbo = MySQLStatic::Query($strSql);
    } else {
        $arrAbo = array(
            0 => array(
                'E_Mail__c' => $_REQUEST['single']
            )
        );
    }
    
    if (count($arrAbo) > 0) {

        $strSql = 'SELECT * FROM `izs_pruefbericht_sent` WHERE `ps_group` = "' .$_REQUEST['id'] .'" AND `ps_month` = "' .$_REQUEST['m'] .'" AND `ps_year` = "' .$_REQUEST['y'] .'"';
        $arrSql = MySQLStatic::Query($strSql);

        $arrAlreadySent = array();
        if (count($arrSql) > 0) {
            foreach ($arrSql as $intKey => $arrSent)  {
                $arrAlreadySent[] = $arrSent['ps_reciptient'];
            }
        }

        if ($boolSingle === true) {
            $arrAlreadySent = array();
        }

        $strSubject = 'SV-Prüfbericht ' .$_REQUEST['y'] .'/' .$_REQUEST['m'] .' |  ' .$arrGrp[0]['Name'];

        $strMessage = $strMailTemplate;
        $strMessage = str_replace('{LINK}', 'https://www.izs.de/sv-pruefbericht/' .$arrGrp[0]['Betriebsnummer__c'], $strMessage);
        $strMessage = str_replace('{LINK_CERT}', 'https://www.izs.de/sv-pruefbericht/' .$arrGrp[0]['Betriebsnummer__c'], $strMessage);

        $strMessage = str_replace('{MONTH}', $_REQUEST['m'], $strMessage);
        $strMessage = str_replace('{YEAR}', $_REQUEST['y'], $strMessage);

        $strMessage = str_replace('{NOW}', date('d.m.Y'), $strMessage);

        $strMessage = str_replace('{NAME_GROUP}', $arrGrp[0]['Name'], $strMessage);

        $strMessage = str_replace('{STREET}', $arrGrp[0]['Strasse__c'], $strMessage);
        $strMessage = str_replace('{ZIP}', $arrGrp[0]['PLZ__c'], $strMessage);
        $strMessage = str_replace('{CITY}', $arrGrp[0]['Ort__c'], $strMessage);

        $strMessage = str_replace('{BNR}', $arrGrp[0]['Betriebsnummer__c'], $strMessage);

        $strLink = 'https://www.izs.de/';
        $strText = '';

        if ($arrGrp[0]['Direktlink_zum_Konto__c'] != '') {
            $strLink = $strLink .$arrGrp[0]['Direktlink_zum_Konto__c'];
            $strText = $arrGrp[0]['Direktlink_zum_Konto__c'];
        }

        $strMessage = str_replace('{DIRECT_LINK}', $strLink, $strMessage);
        $strMessage = str_replace('{DIRECT_TEXT}', str_replace('https://', '', $strLink), $strMessage);

        $strReg = date('m/Y', strtotime($arrGrp[0]['Registriert_bei_IZS_seit__c'])); // 

        $strMessage = str_replace('{REG}', $strReg, $strMessage);

        $strNote = '';
        /*
        if ($arrGrp[0]['Hinweis_sichtbar_auf_Portal__c'] != '') {
            $strNote = '
            <div style="line-height:17px">
                <span style="font-size:11px">ALLGEMEINER HINWEIS</span>
            </div>
            ' .$arrGrp[0]['Hinweis_sichtbar_auf_Portal__c'] .'<br /><br />
            ';
        }
        */

        $strMessage = str_replace('{NOTE}', $strNote, $strMessage);

        $arrColor['red']    = 'ff0000'; //RED
        $arrColor['yellow'] = 'f1c40f'; //YELLOW
        $arrColor['green']  = '1abc9c'; //GREEN
        $arrColor['gray']   = 'bdc3c7'; //GRAY

        $strColorAll = $arrColor['gray'];

        if ($arrGrp[0]['Gesamtstatus__c'] == 'gelb') {

            $strColorAll = $arrColor['yellow'];

        } else if ($arrGrp[0]['Gesamtstatus__c'] == 'grün') {

            $strColorAll = $arrColor['green'];

        } else if ($arrGrp[0]['Gesamtstatus__c'] == 'rot') {

            $strColorAll = $arrColor['red'];

        }
        
        $strMessage = str_replace('{COLOR_ALL}', $strColorAll, $strMessage); // '', 'gelb', 'grün', 'kein Status', 'rot'

        $strTextAll = '';
        if ($arrGrp[0]['Textfahne_zu_Gesamtstatus__c'] != '') {
            $strTextAll = trim($arrGrp[0]['Textfahne_zu_Gesamtstatus__c']);
            if (mb_substr($strTextAll, -1) != '.') {
                $strTextAll.= '.';
            }
        }

        $strMessage = str_replace('{TEXT_ALL}', $strTextAll, $strMessage);

        
        $strColorSv = $arrColor['gray'];

        if ($arrGrp[0]['Status_SV__c'] == 'gelb') {

            $strColorSv = $arrColor['yellow'];

        } else if ($arrGrp[0]['Status_SV__c'] == 'grün') {

            $strColorSv = $arrColor['green'];

        } else if ($arrGrp[0]['Status_SV__c'] == 'rot') {

            $strColorSv = $arrColor['red'];

        }
        
        $strMessage = str_replace('{COLOR_SV}', $strColorSv, $strMessage); // '', 'gelb', 'grün', 'kein Status', 'rot'

        $strTextSv = '';
        if ($arrGrp[0]['Status_SV_Hinweis__c'] != '') {
            $strTextSv = trim($arrGrp[0]['Status_SV_Hinweis__c']);
            if (mb_substr($strTextSv, -1) != '.') {
                $strTextSv.= '.';
            }
        }

        $strMessage = str_replace('{TEXT_SV}', $strTextSv, $strMessage);

        /*
        - Beitragsrückstände: <berechnete Anzahl>
(Event: Status = geschlossen/negativ + Grund = Beitragsrückstand)
- Stundungen / Ratenzahlungen: <berechnete Anzahl>
(Event: Phase = Publication + Stundung/Ratenzahlung = ja (egal ob Ergebnis = OK oder NOT OK!))
        */

        $strSql = 'SELECT `Id` FROM `SF42_IZSEvent__c` WHERE `SF42_Month__c` = "' .$_REQUEST['m'] .'" AND `SF42_Year__c` = "' .$_REQUEST['y'] .'" AND `group_id` = "' .$_REQUEST['id'] .'" AND `Status_Klaerung__c` = "geschlossen / negativ" AND `Grund__c` = "Beitragsrückstand" AND `RecordTypeId` = "01230000001Ao75AAC" AND `SF42_EventStatus__c` = "not OK"';
        $resSql = MySQLStatic::Query($strSql);
        $strAnzRueck = count($resSql);

        $strSql = 'SELECT `Id` FROM `SF42_IZSEvent__c` WHERE `SF42_Month__c` = "' .$_REQUEST['m'] .'" AND `SF42_Year__c` = "' .$_REQUEST['y'] .'" AND `group_id` = "' .$_REQUEST['id'] .'" AND `Stundung__c` != "" AND `RecordTypeId` = "01230000001Ao75AAC" AND `SF42_EventStatus__c` IN ("OK", "not OK")';
        $resSql = MySQLStatic::Query($strSql);
        $strAnzStRa = count($resSql);

        $strAddSv = '&nbsp;<br>';
        if (($strAnzStRa > 0) || ($strAnzRueck > 0)) {
            $strAddSv = '            <ul>' .chr(10);
            if ($strAnzRueck > 0) {
                $strAddSv.= '               <li>Beitragsrückstände: ' .$strAnzRueck .'</li>' .chr(10);
            }
            if ($strAnzStRa > 0) {
                $strAddSv.= '               <li>Stundungen / Ratenzahlungen: ' .$strAnzStRa .'</li>' .chr(10);
            }
            $strAddSv.= '            </ul>' .chr(10);
        }

        $strMessage = str_replace('{ADD_SV}', $strAddSv, $strMessage);
        
        $strColorBG = $arrColor['gray'];

        if ($arrGrp[0]['Status_SV__c'] == 'gelb') {

            $strColorBG = $arrColor['yellow'];

        } else if ($arrGrp[0]['Status_SV__c'] == 'grün') {

            $strColorBG = $arrColor['green'];

        } else if ($arrGrp[0]['Status_SV__c'] == 'rot') {

            $strColorBG = $arrColor['red'];

        }
        
        $strMessage = str_replace('{COLOR_BG}', $strColorBG, $strMessage); // '', 'gelb', 'grün', 'kein Status', 'rot'

        $strFrom = 'infoservice@izs-institut.de';
        //$strBcc  = 'system@izs-institut.de';

        $strBoundary = md5(uniqid(time()));

        $strHeaders = "From: =?UTF-8?B?" . base64_encode('IZS Institut für Zahlungssicherheit') . "?= <" . $strFrom . ">\r\n";
        $strHeaders.= "MIME-Version: 1.0\r\n";
        $strHeaders.= "Content-Type: multipart/mixed; boundary=" . $strBoundary . "\r\n";
        //$strHeaders.= "Bcc: " .$strBcc ."\r\n";
        $strHeaders.= "This is a multi-part message in MIME format\r\n";
      
        $strMime = '--' . $strBoundary . chr(10);
        $strMime .= 'Content-Type: text/html; charset=UTF-8' . chr(10);
        $strMime .= 'Content-Transfer-Encoding: 8bit' . chr(10) . chr(10);
      
        $strMessage = htmlspecialchars_decode($strMessage, ENT_QUOTES);
        $strMessage = quoted_printable_decode($strMessage);
      
        //$strBody = $strMime .chunk_split(base64_encode($strTemplate));
        $strBody = $strMime .$strMessage;
        $strBody .= chr(10) .'--' .$strBoundary .'--';

        foreach ($arrAbo as $intKey => $arrReciptient) {

            if (!in_array($arrReciptient['E_Mail__c'], $arrAlreadySent)) {

                //var_dump(!in_array($arrReciptient['E_Mail__c'], $arrAlreadySent));
                //echo 'SEND: ' .$arrReciptient['E_Mail__c'] .'' ,chr(10); continue;

                $strTo = $arrReciptient['E_Mail__c'];
                //$strTo = 'system@izs-institut.de';

                $strEmail = str_replace('{UNSUBSRIBE}', 'https://www.izs-institut.de/api/company/' .$_REQUEST['id'] .'/infoservice/unsubscribe/?email=' . $strTo . '', $strBody);
                $boolMail = mail($strTo, '=?UTF-8?B?' . base64_encode($strSubject) . '?=', $strEmail, $strHeaders);
                //mail($strTo, '=?UTF-8?B?' . base64_encode($strSubject .' (' .$arrReciptient['E_Mail__c'] .')') . '?=', $strEmail, $strHeaders);
                //print_r($boolMail); die();

                $strSql = 'INSERT INTO `izs_pruefbericht_sent` (`ps_id`, `ps_group`, `ps_month`, `ps_year`, `ps_reciptient`, `ps_time`) VALUES (NULL, "' .$_REQUEST['id'] .'", "' .$_REQUEST['m'] .'", "' .$_REQUEST['y'] .'", "' .$arrReciptient['E_Mail__c'] .'", NOW());';
                $resSql = MySQLStatic::Insert($strSql);

            } else {
                //echo 'ALREADY SEND: ' .$arrReciptient['E_Mail__c'] .'' ,chr(10);
            }

            //if ($intKey == 9) break;

        }
        
        if (!$boolSingle) {

            //mail($strFrom, '=?UTF-8?B?' . base64_encode($strSubject) . '?=', $strBody, $strHeaders);

            
            $strSql = 'SELECT `cc_id` FROM `cms_cert` WHERE `cc_group_id` = "' .$_REQUEST['id'] .'" AND `cc_month` = "' .$_REQUEST['m'] .'" AND `cc_year` = "' .$_REQUEST['y'] .'"';
            $arrSql = MySQLStatic::Query($strSql);

            if (count($arrSql) > 0) {
                
                $strSql = 'UPDATE `cms_cert` SET `cc_sent` = NOW(), `cc_recipients` = ' .count($arrAbo) .' WHERE `cc_id` = "' . $arrSql[0]['cc_id'] . '"';
                $arrSql = MySQLStatic::Update($strSql);

            } else {

                $strSql = 'INSERT INTO `cms_cert` (`cc_id`, `cc_group_id`, `cc_month`, `cc_year`, `cc_created`, `cc_sent`, `cc_recipients`) VALUES (';
                $strSql.= 'NULL, "' .$_REQUEST['id'] .'", "' .$_REQUEST['m'] .'", "' .$_REQUEST['y'] .'", "0", NOW(), "' .count($arrAbo) .'");';
                $resSql = MySQLStatic::Insert($strSql);

            }

        }

    }

    echo json_encode(array('update' => date('d.m.Y H:i')));

}

?>