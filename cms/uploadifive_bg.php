<?php

session_start();

require_once('../assets/classes/class.mysql.php');

// Set the uplaod directory
$uploadDir = 'pdf/temp/';

// Set the allowed file extensions
$fileTypes = array('csv', 'pdf', 'doc', 'docx', 'xls', 'txt', 'rtf', 'html', 'zip', 'mp3', 
'wma', 'mpg', 'flv', 'avi', 'jpg', 'jpeg', 'png', 'gif', 'odt', 'txt', 'ppt', 'pptx', 'xlsx'); // Allowed file extensions

$verifyToken = md5('unique_salt' . $_POST['timestamp']);

if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
  
  $tempFile   = $_FILES['Filedata']['tmp_name'];
	$targetFile = $uploadDir .$_FILES['Filedata']['name'];

	// Validate the filetype
	$fileParts = pathinfo($_FILES['Filedata']['name']);
	if (in_array(strtolower($fileParts['extension']), $fileTypes)) {

    if (file_exists($targetFile)) {
      unlink($targetFile);
    }

		// Save the file
		move_uploaded_file($tempFile, $targetFile);
    echo 1;
    
	} else {

		// The file type wasn't allowed
		echo 'Invalid file type.';

	}
}


?>