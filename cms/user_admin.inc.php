<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

$strOutput = '';
$strOutput.= '<h1>Benutzer administrieren</h1>' .chr(10);

$arrAdmin = array (
  0 => 'nein',
  1 => 'ja'
);

$strSql = 'SELECT `cl_id`, `cl_first`, `cl_last`, `cl_user`, `cl_admin` FROM `cms_login` WHERE `cl_deleted` != 1 ORDER BY `cl_last`';
$arrResult = MySQLStatic::Query($strSql);
if (count($arrResult) > 0) {
  
  $strOutputTable = '';
  $strOutputTable.= '<table class="" id="5">' .chr(10);
  $strOutputTable.= '  <thead>' .chr(10);
  $strOutputTable.= '    <tr>' .chr(10);
	$strOutputTable.= '      <th>Vorname</th>' .chr(10); //class="header"
	$strOutputTable.= '      <th>Nachname</th>' .chr(10); //class="header"
	$strOutputTable.= '      <th>Benutzername</th>' .chr(10); //class="header"
  $strOutputTable.= '      <th>Admin</th>' .chr(10);
	$strOutputTable.= '      <th>Rechte</th>' .chr(10); //class="header"
	$strOutputTable.= '      <th>Deaktivieren</th>' .chr(10); //class="header"
  $strOutputTable.= '    </tr>' .chr(10);
  $strOutputTable.= '  </thead>' .chr(10);
  $strOutputTable.= '  <tbody>' .chr(10);
  
  $arrPeriod = explode('_', $_REQUEST['strSelDate']);
  
  $strNext = '';

  foreach ($arrResult as $intNum => $arrUser) {
    
    $strOutputTable.= '    <tr id="ur_' .$arrUser['cl_id'] .'">' .chr(10);
    $strOutputTable.= '      <td>' .$arrUser['cl_first'] .'</td>' .chr(10);
    $strOutputTable.= '      <td>' .$arrUser['cl_last'] .'</td>' .chr(10);
    $strOutputTable.= '      <td><a id="user_' .$arrUser['cl_id'] .'" class="user user-edit">' .$arrUser['cl_user'] .'</a></td>' .chr(10);
    $strOutputTable.= '      <td>' .$arrAdmin[$arrUser['cl_admin']] .'</td>' .chr(10);
    $strOutputTable.= '      <td><a id="user_' .$arrUser['cl_id'] .'" class="user right-edit">ändern</a></td>' .chr(10);
    $strOutputTable.= '      <td><a id="user_' .$arrUser['cl_id'] .'" class="user user-delete"><img src="/cms/img/f_delete.png"></a></td>' .chr(10);
    $strOutputTable.= '    </tr>' .chr(10);
    
  }
    
  $strOutputTable.= '  </tbody>' .chr(10);
  $strOutputTable.= '</table>' .chr(10);


  
  $strOutput.= '<div class="clearfix" style="height: 48px; width: 642px;">' .chr(10);
  $strOutput.= '  <div class="form-left">' .chr(10);
  $strOutput.= '    <h3>' .count($arrResult) .' Benutzer gefunden:</h3>' .chr(10);
  $strOutput.= '  </div>' .chr(10);
  $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' .chr(10);

  $strOutput.= '<button id="user-create" class="ui-button ui-state-default ui-corner-all">Benutzer hinzufügen</button>' .chr(10);

  $strOutput.= '    <form method="post" action="_get_csv.php" style="display: inline; float: right; padding-left: 10px;">';
  $strOutput.= '    <input type="hidden" name="strTable" value="' .str_replace('"', "'", $strOutputTable) .'">' .chr(10);
  $strOutput.= '    <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' .chr(10);
  $strOutput.= '    </form>';
  $strOutput.= '  </div>' .chr(10);
  $strOutput.= '</div>' .chr(10);
  
  $strOutput.= $strOutputTable;

  $strOutput.= '    <form method="post" action="_ajax_user.php?ac=rall" style="padding-top: 20px;">';
  $strOutput.= '    <button id="export-all" class="ui-button ui-state-default ui-corner-all">Alle Berechtigungen exportieren</button>' .chr(10);
  $strOutput.= '    </form>';
  
  $strOutput.= "
<script>
$('#5').fixheadertable({ 
 colratio    : [100, 100, 100, 100, 100, 100], 
 height      : 500, 
 width       : 630, 
 zebra       : true, 
 resizeCol   : true,
 sortable    : true,
 sortType    : ['string', 'string', 'string', 'string', 'string', ''],
 sortedColId : 2, 
 dateFormat  : 'Y-m'
});
</script>
";

  $strOutput.= '
  
<div id="dialog_uedit" title="Benutzer bearbeiten"></div>
<div id="dialog_ucreate" title="Benutzer hinzufügen"></div>
<div id="dialog_redit" title="Rechte bearbeiten"></div>
<div id="dialog_udelete" title="Benutzer deaktivieren"></div>
  
<style>
#validateTips {
  width: 315px;
}
</style>
  
<script>
$(document).ready(function() {

  $("#cl_four").live("click", function() {
    //console.log($("#cl_four").is(":checked"));
    if ($("#cl_four").is(":checked")) {
      $(".izs450").css("display","table-row");
    } else {
      $(".izs450").css("display","none");
    }
  });

  $("#cl_crm").live("click", function() {
    if ($("#cl_crm").is(":checked")) {
      $(".crm").css("display","table-row");
    } else {
      $(".crm").css("display","none");
    }
  });

	var tips = $("#validateTips");

	function updateTips(t) {
		$("#validateTips").text(t).effect("highlight",{},1500);
	}

	$("#dialog_uedit").dialog({
		bgiframe: true,
		autoOpen: false,
		resizable: false,
		height: 800,
		width: 700,
		modal: true,
		buttons: {
			\'OK\': function() {
			
			  bOk = true;
			  
			  if ($("#cl_pass").val() != $("#cl_pass2").val()) {
			    updateTips("Die Passwörter stimmen nicht überein.");
			    bOk = false;
			  } 
			  if ($("#cl_user").val() == "") {
			    updateTips("Der Benutzername darf nicht leer sein.");
			    bOk = false;
			  }

		    if (bOk) {
		    
          var ajax = {};
          ajax.type     = \'POST\';
          ajax.url      = "_ajax_user.php";
          ajax.dataType = "html; charset=utf-8";
      
          d = { 
            id: $("#user").val(), 
            fi: $("#cl_first").val(),
            la: $("#cl_last").val(),
            ma: $("#cl_mail").val(),
            un: $("#cl_user").val(),
            pa: $("#cl_pass").val(),
            ad: $("#cl_admin").attr("checked"), 
            fi: $("#cl_first").val(),

            ci_cu_id: $("#ci_cu_id").val(),

            cl_crm: $("#cl_crm").attr("checked"), 
            ci_cu_id: $("#ci_cu_id").val(),

            cl_four: $("#cl_four").attr("checked"), 
            im_mobile: $("#im_mobile").val(),
            im_max_hours: $("#im_max_hours").val(),
            im_mobile: $("#im_mobile").val(),
            im_application: $("#im_application").val(),
            im_not_avail: $("#im_not_avail").val(),

            cu_id: $("#cu_id").val(),
            ci_id: $("#ci_id").val(),
            im_id: $("#im_id").val(),
            cl_ks_id: $("#cl_ks_id").val(),

            roleId: $("#roleId").val(),
            
            ac: "usave"
          };
      
          ajax.data = d;
          
          $.ajax(ajax);
          
          $("#ur_" + $("#user").val() +" td:nth-child(1)").html($("#cl_first").val());
          $("#ur_" + $("#user").val() +" td:nth-child(2)").html($("#cl_last").val());
          $("#ur_" + $("#user").val() +" td:nth-child(3) a").html($("#cl_user").val());
          
          if ($("#cl_admin").attr("checked")) {
            $("#ur_" + $("#user").val() +" td:nth-child(4)").html("ja");
          } else {
            $("#ur_" + $("#user").val() +" td:nth-child(4)").html("nein");
          }
		    
		      $(this).dialog(\'close\');
		    }

			}, 
			\'Abbrechen\': function() {

		    $(this).dialog(\'close\');

			}
		},
		close: function() {

		}
	});


	$("#dialog_ucreate").dialog({
		bgiframe: true,
		autoOpen: false,
		resizable: false,
		height: 800,
		width: 700,
		modal: true,
		buttons: {
			\'OK\': function() {
			
		    bOk = true;
			  
			  if ($("#cl_pass").val() != $("#cl_pass2").val()) {
			    updateTips("Die Passwörter stimmen nicht überein.");
			    bOk = false;
			  } 
			  if ($("#cl_pass").val() == "") {
			    updateTips("Das Passwort darf nicht leer sein.");
			    bOk = false;
			  }
			  if ($("#cl_user").val() == "") {
			    updateTips("Der Benutzername darf nicht leer sein.");
			    bOk = false;
			  }

		    if (bOk) {
		    
          var ajax = {};
          ajax.type     = \'POST\';
          ajax.url      = "_ajax_user.php";
          ajax.dataType = "html; charset=utf-8";
      
          d = { 
            id: $("#user").val(), 
            fi: $("#cl_first").val(),
            la: $("#cl_last").val(),
            ma: $("#cl_mail").val(),
            un: $("#cl_user").val(),
            pa: $("#cl_pass").val(),
            ad: $("#cl_admin").attr("checked"), 

            ci_cu_id: $("#ci_cu_id").val(),

            cl_crm: $("#cl_crm").attr("checked"), 
            ci_cu_id: $("#ci_cu_id").val(),

            cl_four: $("#cl_four").attr("checked"), 
            im_mobile: $("#im_mobile").val(),
            im_max_hours: $("#im_max_hours").val(),
            im_mobile: $("#im_mobile").val(),
            im_application: $("#im_application").val(),
            im_not_avail: $("#im_not_avail").val(),

            cu_id: $("#cu_id").val(),
            ci_id: $("#ci_id").val(),
            im_id: $("#im_id").val(),
            cl_ks_id: $("#cl_ks_id").val(),

            roleId: $("#roleId").val(),

            ac: "usave"
          };
      
          ajax.data = d;
          
          ajax.success = function(data) {
            location.reload();
          }
      
          $.ajax(ajax);
		    
		      $(this).dialog(\'close\');
		      
		      
		    }

			}, 
			\'Abbrechen\': function() {

		    $(this).dialog(\'close\');

			}
		},
		close: function() {

		}
	});
  

	$("#dialog_udelete").dialog({
		bgiframe: true,
		autoOpen: false,
		resizable: false,
		height: 180,
		width: 350,
		modal: true,
		buttons: {
			\'OK\': function() {

		    bOk = true;

		    if (bOk) {
		    
          var ajax = {};
          ajax.type     = \'POST\';
          ajax.url      = "_ajax_user.php";
          ajax.dataType = "html; charset=utf-8";
      
          d = { 
            id: $("#user").val(), 
            ac: "udel"
          };
      
          ajax.data = d;
      
          ajax.success = function(data) {
            location.reload();
          }

          $.ajax(ajax);
		    
		      $(this).dialog(\'close\');

		    }

			}, 
			\'Abbrechen\': function() {

		    $(this).dialog(\'close\');

			}
		},
		close: function() {

		}
	});


	$("#dialog_redit").dialog({
		bgiframe: true,
		autoOpen: false,
		resizable: false,
		height: 600,
		width: 350,
		modal: true,
		buttons: {
			\'OK\': function() {
			
  	    bOk = true;

		    if (bOk) {
		    
          var ajax = {};
          ajax.type     = \'POST\';
          ajax.url      = "_ajax_user.php";
          ajax.dataType = "html; charset=utf-8";
          
          var arrRi = new Array();
          
          $(".page").each(function() {
            id = $(this).attr("id").split("page_");
            val = $(this).is(":checked");
            arrRi[id[1]] = val;
          });
      
          d = { 
            id: $("#user").val(), 
            ri: arrRi, 
            ac: "red"
          };
      
          ajax.data = d;
          
          ajax.success = function(data) {
            location.reload();
          }
      
          $.ajax(ajax);
		    
		      $(this).dialog(\'close\');
		      
		    }

			}, 
			\'Abbrechen\': function() {

		    $(this).dialog(\'close\');

			}
		},
		close: function() {

		}
	});


});
</script>
' .chr(10);


} else {
  
  $strOutput.= '<p>Keine User vorhanden.</p>' .chr(10);
  
}

?>