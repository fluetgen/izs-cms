<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include ('../../../../assets/classes/class.mysql.php');

//print_r($_COOKIE); die();

$boolIsIzs = false;
$intUserId = 0;
$boolShow  = true;

if (isset($_COOKIE) && (count($_COOKIE) > 0)) {

    $strToken = 'triple2013';

    if (isset($_COOKIE['id']) && isset($_COOKIE['token']) && ($_COOKIE['token'] == MD5($strToken .'_' .$_COOKIE['id']))) {

        $boolIsIzs = true;
        $intUserId = $_COOKIE['id'];

    }


}

if (!$boolIsIzs) {

    $arrPath = pathinfo($_REQUEST['path']);

    $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE Id = "' .MySQLStatic::esc($arrPath['filename']) .'"';
    $arrEve = MySQLStatic::Query($strSql);

    $strSql = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c` = "' .MySQLStatic::esc($arrEve[0]['Betriebsnummer_ZA__c']) .'"';
    $arrAcc = MySQLStatic::Query($strSql);

    //
    $strSql = 'SELECT * FROM `Group__c` WHERE `Id` = "' .MySQLStatic::esc($arrEve[0]['group_id']) .'"';
    $arrGrp = MySQLStatic::Query($strSql);

    if ($arrGrp[0]['Kooperation_beendet_am__c'] != '0000-00-00') {
        if (strtotime($arrGrp[0]['Kooperation_beendet_am__c']) < strtotime('- 6 weeks')) {
            $boolShow = false;
        }
    }

}


/*
if (isset($_SERVER["HTTP_REFERER"]) && (strstr($_SERVER["HTTP_REFERER"], 'https://www.izs.de/cms/') !== false)) {

    header('Content-Type: application/pdf');
    header('Content-Length: ' .filesize($_REQUEST['path']));
    header('Content-Disposition: inline; filename=' .$_REQUEST['path']);
    readfile($_REQUEST['path']);
  
}

if (!isset($_REQUEST['s'])) {

    $strUrl = 'https://www.izs-institut.de/events/' .$_REQUEST['path'];

    header("HTTP/1.1 301 Moved Permanently");
    header("Location: ". $strUrl);
    exit;

}
*/

if (isset($_REQUEST['path']) && ($_REQUEST['path'] != '') && ($boolShow)) {

    $arrPath = pathinfo($_REQUEST['path']);

    if ($arrPath['extension'] == 'pdf') {

        if (strstr(__DIR__, 'test.') !== false) {

            $strPath = '/data/www/sites/test.izs.de/html/cms/server/php/files/' .$_REQUEST['path'];
            if (!file_exists($strPath)) {
                $strPath = '/data/www/sites/test.izs.de/html/cms/server/php/files/' .$_REQUEST['path'];
            }

        } else {

            $strPath = '/data/www/sites/www.izs.de/html/cms/server/php/files/' .$_REQUEST['path'];
            if (!file_exists($strPath)) {
                $strPath = '/data/www/sites/www.izs.de/html/cms/server/php/files/' .$_REQUEST['path'];
            }

        }

        if (!file_exists($strPath)) {

            header("HTTP/1.1 404 Not Found");
            exit;

        } else {

            if (!$boolIsIzs) {
                //File exits and is allowed to be delivered
                $strSql = 'SELECT `id_id` FROM `izs_download` WHERE `id_ev` = "' .MySQLStatic::esc($arrPath['filename']) .'" AND `id_date` = "' .MySQLStatic::esc(date('Y-m-d')) .'"';
                $arrDow = MySQLStatic::Query($strSql);

                if (count($arrDow) > 0) {
                    $strSql = 'UPDATE `izs_download` SET `id_count` = `id_count` + 1 WHERE `id_id` = "' .MySQLStatic::esc($arrDow[0]['id_id']) .'"';
                    $intSql = MySQLStatic::Update($strSql);
                } else {
                    $strSql = 'INSERT INTO `izs_download` (`id_id`, `id_type`, `id_account`, `id_group`, `id_ip`, `id_moth`, `id_year`, `id_ev`, `id_date`, `id_time`, `id_last_ip`, `id_count`) ';
                    $strSql.= 'VALUES (NULL, "svevent", "' .MySQLStatic::esc($arrAcc[0]['Id']) .'", "' .MySQLStatic::esc($arrGrp[0]['Id']) .'", ';
                    $strSql.= '"' .MySQLStatic::esc($arrEve[0]['SF42_informationProvider__c']) .'", "' .MySQLStatic::esc($arrEve[0]['SF42_Month__c']) .'", "' .MySQLStatic::esc($arrEve[0]['SF42_Year__c']) .'", ';
                    $strSql.= '"' .MySQLStatic::esc($arrPath['filename']) .'", NOW(), NOW(), "' .$_SERVER['REMOTE_ADDR'] .'", "1")';
                    $intSql = MySQLStatic::Insert($strSql);
                }
            }

            $strName = $_REQUEST['path'];
            header('Content-Transfer-Encoding: binary');  // For Gecko browsers mainly
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($strPath)) . ' GMT');
            header('Accept-Ranges: bytes');  // For download resume
            header('Content-Length: ' . filesize($strPath));  // File size
            header('Content-Encoding: none');
            header('Content-Type: application/pdf');  // Change this mime type if the file is not PDF
            //header('Content-Disposition: download; filename=' . $strName);  // Make the browser display the Save As dialog
            header('Content-Disposition: inline; filename=' . $strName);  // browse file
            readfile($strPath); 
            exit;
            
        }


    }

}

if (!$boolShow && (isset($arrGrp[0]['Id']))) {

    header('Location: https://www.izs-institut.de/portal.html#!/company/' .$arrGrp[0]['Id']);
    exit;

} else {

    header("HTTP/1.1 404 Not Found");
    exit;

}

?>