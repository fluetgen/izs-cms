<?php
//HTML2PDF by Clément Lavoillotte
//ac.lavoillotte@noos.fr
//webmaster@streetpc.tk
//http://www.streetpc.tk

require_once ('fpdf.php');

//function hex2dec
//returns an associative array (keys: R,G,B) from
//a hex html code (e.g. #3FE5AA)
function hex2dec($couleur = "#000000"){
    $R = substr($couleur, 1, 2);
    $rouge = hexdec($R);
    $V = substr($couleur, 3, 2);
    $vert = hexdec($V);
    $B = substr($couleur, 5, 2);
    $bleu = hexdec($B);
    $tbl_couleur = array();
    $tbl_couleur['R']=$rouge;
    $tbl_couleur['V']=$vert;
    $tbl_couleur['B']=$bleu;
    return $tbl_couleur;
}

//conversion pixel -> millimeter at 72 dpi
function px2mm($px){
    return $px*25.4/72;
}

function txtentities($html){
    $trans = get_html_translation_table(HTML_ENTITIES);
    $trans = array_flip($trans);
    return strtr($html, $trans);
}
////////////////////////////////////

class pdfHtml extends FPDF
{
//variables of html parser
protected $B;
protected $I;
protected $U;
protected $HREF;
protected $fontList;
protected $issetfont;
protected $issetcolor;

protected $intR;
protected $intG;
protected $intB;
protected $strUnderline;

public $arrTheme = array();
public $arrColSet = array();
public $arrHighlight = array();

function __construct($orientation='P', $unit='mm', $format='A4')
{
    //Call parent constructor
    parent::__construct($orientation,$unit,$format);
    //Initialization
    $this->B=0;
    $this->I=0;
    $this->U=0;
    $this->HREF='';
    $this->fontlist=array('arial', 'times', 'courier', 'helvetica', 'symbol');
    $this->issetfont=false;
    $this->issetcolor=false;
    $this->setLinkColor();
}

function WriteHTML($html)
{
    //HTML parser
    $html=strip_tags($html,"<b><u><i><a><img><p><br><strong><em><font><tr><blockquote>"); //supprime tous les tags sauf ceux reconnus
    $html=str_replace("\n",' ',$html); //remplace retour à la ligne par un espace
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE); //éclate la chaîne avec les balises

    //print_r($a);

    foreach($a as $i=>$e)
    {
        if ($e == '') continue;

        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(3.5,stripslashes(txtentities($e)));
        }
        else
        {
            //Tag
            if($e[0]=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag, $attr)
{
    //Opening tag
    switch($tag){
        case 'STRONG':
            $this->SetStyle('B',true);
            break;
        case 'EM':
            $this->SetStyle('I',true);
            break;
        case 'B':
        case 'I':
        case 'U':
            $this->SetStyle($tag,true);
            break;
        case 'A':
            $this->HREF=$attr['HREF'];
            break;
        case 'IMG':
            if(isset($attr['SRC']) && (isset($attr['WIDTH']) || isset($attr['HEIGHT']))) {
                if(!isset($attr['WIDTH']))
                    $attr['WIDTH'] = 0;
                if(!isset($attr['HEIGHT']))
                    $attr['HEIGHT'] = 0;
                $this->Image($attr['SRC'], $this->GetX(), $this->GetY(), px2mm($attr['WIDTH']), px2mm($attr['HEIGHT']));
            }
            break;
        case 'TR':
        case 'BLOCKQUOTE':
        case 'BR':
            $this->Ln(4);
            break;
        case 'P':
            $this->Ln(10);
            break;
        case 'FONT':
            if (isset($attr['COLOR']) && $attr['COLOR']!='') {
                $coul=hex2dec($attr['COLOR']);
                $this->SetTextColor($coul['R'],$coul['V'],$coul['B']);
                $this->issetcolor=true;
            }
            if (isset($attr['FACE']) && in_array(strtolower($attr['FACE']), $this->fontlist)) {
                $this->SetFont(strtolower($attr['FACE']));
                $this->issetfont=true;
            }
            break;
    }
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='STRONG')
        $tag='B';
    if($tag=='EM')
        $tag='I';
    if($tag=='B' || $tag=='I' || $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
    if($tag=='FONT'){
        if ($this->issetcolor==true) {
            $this->SetTextColor(0);
        }
        if ($this->issetfont) {
            $this->SetFont('arial');
            $this->issetfont=false;
        }
    }
}

function SetStyle($tag, $enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
    {
        if($this->$s>0)
            $style.=$s;
    }
    $this->SetFont('',$style);
}

function PutLink($URL, $txt)
{
    //Put a hyperlink
    $this->SetTextColor($this->intR, $this->intG, $this->intB);
    if ($this->strUnderline == 'U') $this->SetStyle('U',true);
    $this->Write(3.5,$txt,$URL);
    if ($this->strUnderline == 'U') $this->SetStyle('U',false);
    $this->SetTextColor(0);
}

function setLinkColor ($intR = 0, $intG = 0, $intB = 255, $strUnderline = 'U') {
    $this->intR = $intR;
    $this->intG = $intG;
    $this->intB = $intB;
    $this->strUnderline = $strUnderline;
}

function Circle($x, $y, $r, $style='D')
{
    $this->Ellipse($x,$y,$r,$r,$style);
}

function Ellipse($x, $y, $rx, $ry, $style='D')
{
    if($style=='F')
        $op='f';
    elseif($style=='FD' || $style=='DF')
        $op='B';
    else
        $op='S';
    $lx=4/3*(M_SQRT2-1)*$rx;
    $ly=4/3*(M_SQRT2-1)*$ry;
    $k=$this->k;
    $h=$this->h;
    $this->_out(sprintf('%.2F %.2F m %.2F %.2F %.2F %.2F %.2F %.2F c',
        ($x+$rx)*$k,($h-$y)*$k,
        ($x+$rx)*$k,($h-($y-$ly))*$k,
        ($x+$lx)*$k,($h-($y-$ry))*$k,
        $x*$k,($h-($y-$ry))*$k));
    $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
        ($x-$lx)*$k,($h-($y-$ry))*$k,
        ($x-$rx)*$k,($h-($y-$ly))*$k,
        ($x-$rx)*$k,($h-$y)*$k));
    $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
        ($x-$rx)*$k,($h-($y+$ly))*$k,
        ($x-$lx)*$k,($h-($y+$ry))*$k,
        $x*$k,($h-($y+$ry))*$k));
    $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c %s',
        ($x+$lx)*$k,($h-($y+$ry))*$k,
        ($x+$rx)*$k,($h-($y+$ly))*$k,
        ($x+$rx)*$k,($h-$y)*$k,
        $op));
}


// TABLE II

    function createTheme ($arrTheme = array()) {
        $this->arrTheme = array_merge($this->arrTheme, $arrTheme);
    }

    function createColSet ($arrColSet = array()) {
        $this->arrColSet = array_merge($this->arrColSet, $arrColSet);
    }

    function createHighlight ($arrHighlight = array()) {
        $this->arrHighlight = array_merge($this->arrHighlight, $arrHighlight);
    }

    function getTheme ($strName = '') {
        return $this->arrTheme[$strName];
    }

    function getHighlight ($strName = '') {
        return $this->arrHighlight[$strName];
    }

    function loadTheme ($arrTheme = array()) {

        $this->SetFont($arrTheme['font_name'], $arrTheme['font_style'], $arrTheme['font_size']);
        $color = explode(",", $arrTheme['fillcolor']);
        $this->SetFillColor($color[0], $color[1], $color[2]);
        $color = explode(",", $arrTheme['textcolor']);
        $this->SetTextColor($color[0], $color[1], $color[2]);            
        $color = explode(",", $arrTheme['drawcolor']);            
        $this->SetDrawColor($color[0], $color[1], $color[2]);
        $this->SetLineWidth($arrTheme['linewidth']);

        return $arrTheme;

    }

   // Create Table
   function WriteTable($tcolums)
   {
      // go through all colums
      for ($i = 0; $i < sizeof($tcolums); $i++)
      {
         $current_col = $tcolums[$i];
         $height = 0;
         
         // get max height of current col
         $nb=0;
         for($b = 0; $b < sizeof($current_col); $b++) {
            // set style
            if (isset($current_col[$b]['theme'])) {
                $arrTheme = $this->getTheme($current_col[$b]['theme']);
                if (isset($arrTheme['colset'])) {
                    $current_col[$b]['width'] = $this->arrColSet[$arrTheme['colset']][$b];
                }
                $current_col[$b] = array_merge($arrTheme, $current_col[$b]);
                $this->loadTheme($current_col[$b]);
            } else {
                $this->SetFont($current_col[$b]['font_name'], $current_col[$b]['font_style'], $current_col[$b]['font_size']);
                $color = explode(",", $current_col[$b]['fillcolor']);
                $this->SetFillColor($color[0], $color[1], $color[2]);
                $color = explode(",", $current_col[$b]['textcolor']);
                $this->SetTextColor($color[0], $color[1], $color[2]);            
                $color = explode(",", $current_col[$b]['drawcolor']);            
                $this->SetDrawColor($color[0], $color[1], $color[2]);
                $this->SetLineWidth($current_col[$b]['linewidth']);
            }
                        
            $nb = max($nb, $this->NbLines($current_col[$b]['width'], $current_col[$b]['text']));            
            $height = $current_col[$b]['height'];

         }  
         $h=$height*$nb;
         
         
         // Issue a page break first if needed
         $this->CheckPageBreak($h);

         $intYOffset = 0;
         
         // Draw the cells of the row
         for($b = 0; $b < sizeof($current_col); $b++)
         {
            $w = $current_col[$b]['width'];
            $a = $current_col[$b]['align'];
            
            // Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            
            // set style
            if (isset($current_col[$b]['theme'])) {
                $arrTheme = $this->getTheme($current_col[$b]['theme']);
                if (isset($arrTheme['colset'])) {
                    $current_col[$b]['width'] = $this->arrColSet[$arrTheme['colset']][$b];
                }
                $current_col[$b] = array_merge($arrTheme, $current_col[$b]);
                $this->loadTheme($current_col[$b]);
            } else {
                $this->SetFont($current_col[$b]['font_name'], $current_col[$b]['font_style'], $current_col[$b]['font_size']);
                $color = explode(",", $current_col[$b]['fillcolor']);
                $this->SetFillColor($color[0], $color[1], $color[2]);
                $color = explode(",", $current_col[$b]['textcolor']);
                $this->SetTextColor($color[0], $color[1], $color[2]);            
                $color = explode(",", $current_col[$b]['drawcolor']);            
                $this->SetDrawColor($color[0], $color[1], $color[2]);
                $this->SetLineWidth($current_col[$b]['linewidth']);
            }
            
            $color = explode(",", $current_col[$b]['fillcolor']);            
            $this->SetDrawColor($color[0], $color[1], $color[2]);
            
            
            // Draw Cell Background
            $this->Rect($x, $y, $w, $h, 'FD');
            
            $color = explode(",", $current_col[$b]['drawcolor']);            
            $this->SetDrawColor($color[0], $color[1], $color[2]);
            
            // Draw Cell Border
            if (substr_count($current_col[$b]['linearea'], "T") > 0)
            {
               $this->Line($x, $y, $x+$w, $y);
            }            
            
            if (substr_count($current_col[$b]['linearea'], "B") > 0)
            {
               $this->Line($x, $y+$h, $x+$w, $y+$h);
               $intYOffset = $current_col[$b]['linewidth'];
            }            
            
            if (substr_count($current_col[$b]['linearea'], "L") > 0)
            {
               $this->Line($x, $y, $x, $y+$h);
            }
                        
            if (substr_count($current_col[$b]['linearea'], "R") > 0)
            {
               $this->Line($x+$w, $y, $x+$w, $y+$h);
            }
            
            // Print the text
            if (isset($current_col[$b]['highlight']) && ($current_col[$b]['highlight'] === true)) {

                //die('true');
                $arrHighlight = $this->arrHighlight[$current_col[$b]['theme']];

                //WHEN RIGHT
                $xh = ($x + $current_col[$b]['width'] - ($this->GetStringWidth($current_col[$b]['text']) + 4) - $this->GetCellMargin());
                $yh = $y + 2.5;

                /*
                $strText = 'Y: ' .$yh;
                $this->SetXY(20, 170);  
                $this->SetTextColor(0, 0, 0);
                $this->MultiCell(100, $current_col[$b]['height'], $strText, 0, $a, 0);
                */


                $color = explode(",", $arrHighlight['textcolor']);
                $this->SetTextColor($color[0], $color[1], $color[2]);            
                $color = explode(",", $arrHighlight['fillcolor']);            
                $this->SetFillColor($color[0], $color[1], $color[2]);

                $this->RoundedRect($xh, $yh, $this->GetStringWidth($current_col[$b]['text']) + 4, $current_col[$b]['height'] - 5, 1, '1234', 'F');

                $this->SetXY($xh, $yh + 0.25);         
                $this->MultiCell($this->GetStringWidth($current_col[$b]['text']) + 4, $current_col[$b]['height'] - 5, $current_col[$b]['text'], 0, 'C', 0);

            } else {
                $this->MultiCell($w, $current_col[$b]['height'], $current_col[$b]['text'], 0, $a, 0);
            }
            
            // Put the position to the right of the cell
            $this->SetXY($x+$w, $y);         
         }

         $this->SetY($y+$intYOffset);
         
         // Go to the next line
         $this->Ln($h);          
      }                  
   }

   
   // If the height h would cause an overflow, add a new page immediately
   function CheckPageBreak($h)
   {
      if($this->GetY()+$h>$this->PageBreakTrigger)
         $this->AddPage($this->CurOrientation);
   }


   // Computes the number of lines a MultiCell of width w will take
   function NbLines($w, $txt)
   {
      $cw=&$this->CurrentFont['cw'];
      if($w==0)
         $w=$this->w-$this->rMargin-$this->x;
      $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
      $s=str_replace("\r", '', $txt);
      $nb=strlen($s);
      if($nb>0 and $s[$nb-1]=="\n")
         $nb--;
      $sep=-1;
      $i=0;
      $j=0;
      $l=0;
      $nl=1;
      while($i<$nb)
      {
         $c=$s[$i];
         if($c=="\n")
         {
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
            continue;
         }
         if($c==' ')
            $sep=$i;
         $l+=$cw[$c];
         if($l>$wmax)
         {
            if($sep==-1)
            {
               if($i==$j)
                  $i++;
            }
            else
               $i=$sep+1;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
         }
         else
            $i++;
      }
      return $nl;
   }

   //Highlight
   function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
   {
       $k = $this->k;
       $hp = $this->h;
       if($style=='F')
           $op='f';
       elseif($style=='FD' || $style=='DF')
           $op='B';
       else
           $op='S';
       $MyArc = 4/3 * (sqrt(2) - 1);
       $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

       $xc = $x+$w-$r;
       $yc = $y+$r;
       $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
       if (strpos($corners, '2')===false)
           $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
       else
           $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

       $xc = $x+$w-$r;
       $yc = $y+$h-$r;
       $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
       if (strpos($corners, '3')===false)
           $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
       else
           $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

       $xc = $x+$r;
       $yc = $y+$h-$r;
       $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
       if (strpos($corners, '4')===false)
           $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
       else
           $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

       $xc = $x+$r ;
       $yc = $y+$r;
       $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
       if (strpos($corners, '1')===false)
       {
           $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
           $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
       }
       else
           $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
       $this->_out($op);
   }

   function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
   {
       $h = $this->h;
       $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
           $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
   }


} //end of class

?>