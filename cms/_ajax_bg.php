<?php

session_start();

require_once('../assets/classes/class.mysql.php');
require_once('../assets/classes/class.database.php');

$uploadDir = 'pdf/bg/';
$tempDir   = 'pdf/temp/';

$strOutput = '';

if (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'save')) {
  
  $_REQUEST = str_replace('"', '\\"', $_REQUEST);
  
  $strSql = 'INSERT INTO `Berufsgenossenschaft__c` (`Id`) VALUES (NULL)';
  $intId  = MySQLStatic::Insert($strSql);
  
  $strNewName = 'BG-' .str_pad($intId, 6,'0', STR_PAD_LEFT); //000001
  
  $arrParts = explode('.', $_REQUEST['fnames']);
  $strExtension = $arrParts[count($arrParts) -1];
  $strNewFileName = $uploadDir .$strNewName .'.' .$strExtension;
 
  $strSql = 'UPDATE `Berufsgenossenschaft__c` SET ';
  $strSql.= '`Dokumenten_Link__c` = "' .$strNewName .'.' .$strExtension .'", ';
  $strSql.= '`Name` = "' .$strNewName .'", ';
  $strSql.= '`Company_Group__c` = "' .$_REQUEST['Company_Group__c'] .'", ';
  $strSql.= '`Beitragsjahr__c` = "' .$_REQUEST['Beitragsjahr__c'] .'", ';
  $strSql.= '`Beitragsschuldner__c` = "' .$_REQUEST['Beitragsschuldner__c'] .'", ';
  $strSql.= '`Mitgliedsnummer__c` = "' .$_REQUEST['Mitgliedsnummer__c'] .'", ';
  $strSql.= '`Berufsgenossenschaft__c` = "' .$_REQUEST['Berufsgenossenschaft__c'] .'", ';
  $strSql.= '`Status__c` = "' .$_REQUEST['Status__c'] .'", ';
  $strSql.= '`Bemerkung__c` = "' .$_REQUEST['Bemerkung__c'] .'" ';
  $strSql.= 'WHERE `Id` = "' .$intId .'"';
  $resSql = MySQLStatic::Query($strSql);
  
  rename($tempDir .$_REQUEST['fnames'], $strNewFileName);
  
  echo '1';

}  
  
if (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'add')) {
  
  $strOutput.= '<p id="validateTips"></p>';

  $strOutput.= '<div class="pbody">';
  
  $strOutput.= '<form id="upl" action="/cms/_save.php" method="POST" enctype="multipart/form-data" class="popup">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);
  $strOutput.= '<input type="hidden" name="cl_id" value="' .$_SESSION['id'] .'">' .chr(10);

  $strOutput.= '<table>' .chr(10);
  $strOutput.= '<tbody>' .chr(10);
  

  $strSql2a = 'SELECT `Id`, `Name` FROM `Group__c` WHERE `SF42_Group_Type__c` = "Lender" ORDER BY `Name`';
  $arrCompanyGroup = MySQLStatic::Query($strSql2a);
  
  if (count($arrCompanyGroup) > 0) {

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Company Group: </td>' .chr(10);
    $strOutput.= '    <td><select name="Company_Group__c" id="Company_Group__c" style="width: 370px;">' .chr(10);
    $strOutput.= '    <option value="">Bitte wählen</option>' .chr(10);
    
    foreach ($arrCompanyGroup as $strKey => $arrValue) {
      $strSelected = '';
      if ($arrValue['Id'] == $arrRow[0]['Company_Group__c']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option value="' .$arrValue['Id'] .'"' .$strSelected .'>' .$arrValue['Name'] .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
  
  $arrYear[0] = date('Y') - 2;
  $arrYear[1] = $arrYear[0] + 1;
  $arrYear[2] = $arrYear[0] + 2;
  $arrYear[3] = $arrYear[0] + 3;
  
  if ($arrRow[0]['Beitragsjahr__c'] == '') {
    $arrRow[0]['Beitragsjahr__c'] = $arrYear[1];
  }
  
  if (count($arrYear) > 0) {

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Beitragsjahr: </td>' .chr(10);
    $strOutput.= '    <td><select name="Beitragsjahr__c" id="Beitragsjahr__c">' .chr(10);
    //$strOutput.= '    <option value="">Bitte wählen</option>' .chr(10);
    
    foreach ($arrYear as $strKey => $strValue) {
      $strSelected = '';
      if ($strValue == $arrRow[0]['Beitragsjahr__c']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option value="' .$strValue .'"' .$strSelected .'>' .$strValue .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
  

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Beitragsschuldner: </td>' .chr(10);
  $strOutput.= '    <td><input type="text" name="Beitragsschuldner__c" id="Beitragsschuldner__c" value="' .$arrRow[0]['Beitragsschuldner__c'] .'" style="width: 300px;"></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Mitgliedsnummer: </td>' .chr(10);
  $strOutput.= '    <td><input type="text" name="Mitgliedsnummer__c" id="Mitgliedsnummer__c" value="' .$arrRow[0]['Mitgliedsnummer__c'] .'" style="width: 300px;"></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

    
  $arrStatus = array (
    'OK' => 'OK',
    'NOT OK' => 'NOT OK',
  );
  
  if (count($arrStatus) > 0) {

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Status: </td>' .chr(10);
    $strOutput.= '    <td><select name="Status__c" id="Status__c">' .chr(10);
    
    foreach ($arrStatus as $strKey => $strValue) {
      $strSelected = '';
      if ($strKey == $arrRow[0]['Status__c']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option value="' .$strKey .'"' .$strSelected .'>' .$strValue .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }

  $strSql2a = 'SELECT `Id`, `Name`  FROM `Account` WHERE `RecordTypeId` = "01230000001DKGtAAO" ORDER BY `Name`';
  $arrBG = MySQLStatic::Query($strSql2a);
  
  if (count($arrBG) > 0) {
    
    if ($arrRow[0]['Berufsgenossenschaft__c'] == '') {
      $arrRow[0]['Berufsgenossenschaft__c'] = '0013000001HH0sBAAT';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Berufsgenossenschaft: </td>' .chr(10);
    $strOutput.= '    <td><select name="Berufsgenossenschaft__c" id="Berufsgenossenschaft__c" style="width: 370px;">' .chr(10);
    //$strOutput.= '    <option value="">Bitte wählen</option>' .chr(10);
    
    foreach ($arrBG as $strKey => $arrValue) {
      $strSelected = '';
      if ($arrValue['Id'] == $arrRow[0]['Berufsgenossenschaft__c']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option value="' .$arrValue['Id'] .'"' .$strSelected .'>' .$arrValue['Name'] .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Bemerkung: </td>' .chr(10);
  $strOutput.= '    <td><textarea name="Bemerkung__c" id="Bemerkung__c" style="width: 300px;">' .$arrRow[0]['Bemerkung__c'] .'</textarea></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td style="width: 100px;">Upload: </td>' .chr(10);

  $strOutput.= '    <td><div id="queue"><span id="emp">Drop Files here...</span></div>
	<input type="hidden" name="fnames" id="fnames" />
	<input type="file" name="file_upload" id="file_upload" />

';

$timestamp = time();

$strOutput.= '
<script type="text/javascript">

$(document).ready(function($) {
      $(\'#file_upload\').uploadifive({
        \'auto\'             : true,
        \'multi\'            : false, 
        \'queueSizeLimit\'   : 1,
        \'uploadLimit\'      : 0, 
				\'formData\'         : { \'id\': \'' .$_REQUEST['id'] .'\', \'timestamp\': \'' .$timestamp .'\', \'token\': \'' .md5('unique_salt' .$timestamp) .'\' },
        \'queueID\'          : \'queue\',
        \'itemTemplate\'     : \'<div class="uploadifive-queue-item"><span class="filename"></span><span class="fileinfo"></span><div class="close"></div></div>\', 
        \'uploadScript\'     : \'uploadifive_bg.php\',
        \'onUploadComplete\' : function(file, data) { if (file.name != "") { $("#fnames").val(file.name);} $(".ui-dialog-buttonpane button:eq(0)").removeClass("ui-state-disabled").attr("disabled", ""); }, 
        \'onAddQueueItem\'   : function(file) { $("#emp").css("display", "none"); $(".ui-dialog-buttonpane button:eq(0)").addClass("ui-state-disabled").attr("disabled", "disabled"); }, 
        \'onCancel\'         : function(file) { var input = $("#fnames"); input.val(""); }  
      });
    });

</script></td>' .chr(10);

  /*
  $strOutput.= '  </tr>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Dokument: </td>' .chr(10);
  $strOutput.= '    <td><iframe id="iframe" src="_files.php?Id=' .$_REQUEST['id'] .'" style="width:372px;height:20px;border:0px;"></iframe></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);
  */
 

  $strOutput.= '</tbody>' .chr(10);

  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);

  $strOutput.= '</div>';



echo $strOutput;
  
}

?>