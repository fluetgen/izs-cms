<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

include('conf/global.inc.php');
require_once('../assets/classes/class.mysql.php');
require_once('../assets/classes/class.database.php');

require_once('classes/class.user.php');

$objUser = new User;
$arrUserListRaw = $objUser->arrGetUserList();

$arrUserList = array();
if (count($arrUserListRaw) > 0) {
  foreach ($arrUserListRaw as $intKey => $arrUser) {
    if (($arrUser['cl_deleted'] != 1) && (strstr($arrUser['cl_user'], '_') === false) && ($arrUser['cl_user'] != 'testuser') && ($arrUser['cl_user'] != 'c.marchsreiter')) {
      $arrUserList[$arrUser['cl_id']] = $arrUser['cl_user'];
    }
  }
}

asort($arrUserList);

$strUrl = 'https://www.izs-institut.de/api/negativ/?active=1'; //to=' .date('Y-m-d') .'
$strJson = file_get_contents($strUrl);
$arrRequest = json_decode($strJson, true);
$arrNvmList = $arrRequest['content']['negativ'];

$arrNvmRequest = array();
$arrNvmRequestOver = array();
if (is_array($arrNvmList) && (count($arrNvmList) > 0)) {
    foreach ($arrNvmList as $intKey => $arrEscRaw) {
        if ($arrEscRaw['iptype'] == 'Krankenkasse') {
          $arrNvmRequest[$arrEscRaw['id']] = $arrEscRaw;
          $strKey = $arrEscRaw['melid'] .'_' .$arrEscRaw['ipid'];
          $arrNvmRequestOver[$strKey][] = $arrEscRaw['id'];
        }
    }
}

//print_r($arrNvmList); die();
//print_r($arrNvmRequestOver); die();

//a083000000iUMkgAAG

function replace_placeholder($strText = '', $intId) {
  global $arrPlaceholder;
  
  $strReturn = $strText;
  
  $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$_REQUEST['det'] .'"';
  $event  = MySQLStatic::Query($strSql);

  $strSql = 'SELECT * FROM `Account` WHERE `Id` = "' .$event[0]['SF42_informationProvider__c'] .'"';
  $Ip  = MySQLStatic::Query($strSql); // == KK

  $strSql = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$event[0]['Betriebsnummer_ZA__c'] .'"';
  $Pp  = MySQLStatic::Query($strSql);

  $strReturn = str_replace('{Month}', $event[0]['SF42_Month__c'], $strReturn);  
  $strReturn = str_replace('{Year}', $event[0]['SF42_Year__c'], $strReturn);  

  $strReturn = str_replace('{Salutation}', $Ip[0]['Anrede_Anschreiben__c'] .' ' .$Ip[0]['Nachname__c'], $strReturn);  
  $strReturn = str_replace('{Beitragszahler}', $Pp[0]['Name'], $strReturn);  
  $strReturn = str_replace('{BNR Beitragszahler}', $Pp[0]['SF42_Comany_ID__c'], $strReturn);  
  $strReturn = str_replace('{Krankenkasse}', $Ip[0]['Name'], $strReturn);  
  $strReturn = str_replace('{Sachbearbeiter KK}', $Ip[0]['Anrede_Briefkopf__c'] .' ' .$Ip[0]['Nachname__c'], $strReturn);  
  $strReturn = str_replace('{Sachbearbeiter KK Telefon}', $Ip[0]['Phone'], $strReturn);  
  $strReturn = str_replace('{Erledigung bis}', $event[0]['bis_am__c'], $strReturn);  
  $strReturn = str_replace('{Event Id}', $event[0]['Name'], $strReturn);  
  
  //$strReturn = str_replace('{}', [0][''], $strReturn);  
  
  return $strReturn;
}

$boolShowIp = false;
$strOutput = '';

$strOutput.= '<h1>Rücklauf</h1>' .chr(10);

$arrSystemStream = array (
  'SF42_OnlineStatus__c' => 'änderte Online Status', 
  'Info__c' => 'änderte globale Status-Info', 
  'bis_am__c' => 'änderte Wiedervorlage-Datum', 
  'RecordTypeId' => 'änderte Phase', 
  'SF42_EventStatus__c' => 'änderte Ergebnis', 
  'SF42_PublishingStatus__c' => 'änderte Sichtbarkeit des Dokuments auf dem Portal', 
  'Art_des_Dokuments__c' => 'änderte Art des Dokuments', 
  'Auskunft_von__c' => 'änderte Auskunftsgeber', 
  'Sperrvermerk__c' => 'änderte Sperrvermerk-Status', 
  'SF42_EventComment__c' => 'änderte den auf dem Portal sichtbaren Kommentar', 
  'SF42_DocumentUrl__c' => 'änderte das Dokument',
  'Rueckmeldung_am__c' => 'änderte Rückmelde-Datum', 
  'Art_der_Rueckmeldung__c' => 'änderte Art der Rückmeldung', 
  'Status_Klaerung__c' => 'änderte den Klärungs-Status', 
  'Grund__c' => 'änderte den Grund für die Klärung', 
  'Beitragsrueckstand__c' => 'änderte die Höhe des Beitragsrückstands', 
  'Naechster_Meilenstein__c' => 'änderte die Information zum nächsten Schritt', 
);

$arrStundung = array(
  'Stundung' => 'Stundung',
	'Ratenzahlung' => 'Ratenzahlung',
	'Stundung oder Ratenzahlung' => 'Stundung oder Ratenzahlung'
);

unset($arrChangePreview['abgelehnt / Dublette']);

$arrRequestWay = array();
$arrDecentral = array();

$arrRecordType = array (
  'Delivery' => '01230000001Ao73AAC',
  'FollowUp' => '01230000001Ao74AAC',
  'Publication' => '01230000001Ao75AAC',
  'Review' => '01230000001Ao76AAC'
);

$arrRecordTypeFlip = array_flip($arrRecordType);

$arrDokArt = array (
  'Dynamische Erzeugung' => 'Digitale Auskunft',
  'Kontoauskunft' => 'Form',
  'Kontoauszug' => 'KtoA',
  'Unbedenklichkeitsbescheinigung' => 'UBB',
  'Sonstiges' => 'Sonst.'
);


$strQueryAdd = '"' .implode('", "', array_keys($arrChangePreview)) .'"';

$arrOnline = array (
  'true' => 'sichtbar',
  'false' => 'nicht sichtbar'
);

$arrSperr = array (
  'true' => 'ja',
  'false' => 'nein'
);

$arrSichtbar = array (
  'online' => 'ja',
  'private' => 'nein',
  'ready' => 'auf Anfrage'  
);

$arrAuskunft = array (
  'Krankenkasse' => 'Krankenkasse',
  'Zeitarbeitsunternehmen' => 'Zeitarbeitsunternehmen',
);

$arrRueck = array (
  'Email' => 'Email',
  'Fax' => 'Fax',
  'Post' => 'Post',
  'Telefon' => 'Telefon'  
);

$arrStatusKlaerung = array (
  'in Klärung' => 'in Klärung',
  'geschlossen / positiv' => 'geschlossen / positiv',
  'geschlossen / negativ' => 'geschlossen / negativ'  
);


if (isset($_REQUEST['det']) && ($_REQUEST['det'] != '')) {
  
  if (isset($_REQUEST['save']) && ($_REQUEST['save'] == 1)) { //"", datum

    
    if ($_REQUEST['bis_am__c'] != '') {
      $arrDate = explode('.', $_REQUEST['bis_am__c']);
      $_REQUEST['bis_am__c'] = $arrDate[2] .'-' .$arrDate[1] .'-' .$arrDate[0];
    } else {
      $_REQUEST['bis_am__c'] = '0000-00-00';
    }
    
    if ($_REQUEST['Rueckmeldung_am__c'] != '') {
      $arrDate = explode('.', $_REQUEST['Rueckmeldung_am__c']);
      $_REQUEST['Rueckmeldung_am__c'] = $arrDate[2] .'-' .$arrDate[1] .'-' .$arrDate[0];
    } else {
      $_REQUEST['Rueckmeldung_am__c'] = '0000-00-00';
    }
    
    $_REQUEST['Naechster_Meilenstein__c'] = str_replace("'", '"', $_REQUEST['Naechster_Meilenstein__c']);
    
    if ($_REQUEST['deletelink'] == 1) {
      $_REQUEST['SF42_DocumentUrl__c'] = '';
    }
    
    $strSql = "    
SELECT `RecordTypeId`, `SF42_EventStatus__c`, `SF42_DocumentUrl__c`, `SF42_PublishingStatus__c`, 
`Art_des_Dokuments__c`, `Auskunft_von__c`, `Sperrvermerk__c`, `SF42_EventComment__c`, 
`Rueckmeldung_am__c`, `Art_der_Rueckmeldung__c`, `Status_Klaerung__c`, `Grund__c`, `Beitragsrueckstand__c`, 
`Naechster_Meilenstein__c`, `Info__c`, `bis_am__c`, `Name`, `SF42_DocumentUrl__c` , SF42_OnlineStatus__c   
FROM `SF42_IZSEvent__c` 
WHERE `evid` = '" .$_REQUEST['det'] ."'";
    $result = MySQLStatic::Query($strSql);
    
    $strEventName = $result[0]['Name'];
    
    foreach ($result[0] as $strKey => $strValue) {
      if (($strKey != 'Name') && ($_REQUEST[$strKey] != $strValue)) {
        
        $strSql2 = 'INSERT INTO `izs_event_change` 
         (`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (
         NULL, "' .$_REQUEST['det'] .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "' .$strKey .'", "' .str_replace('"', '\\"', $strValue) .'", "' .str_replace('"', '\\"', $_REQUEST[$strKey]) .'", NOW(), 2)';
        $result2 = MySQLStatic::Query($strSql2);
        
      }
    }
    
  
    $strSql3 = "
UPDATE `SF42_IZSEvent__c` SET 
`RecordTypeId` = '" .$_REQUEST['RecordTypeId'] ."', `SF42_EventStatus__c` = '" .$_REQUEST['SF42_EventStatus__c'] ."', 
`SF42_DocumentUrl__c` = '" .$_REQUEST['SF42_DocumentUrl__c'] ."', `Info__c` = '" .str_replace("'", "\\'", $_REQUEST['Info__c']) ."', 
`SF42_PublishingStatus__c` = '" .$_REQUEST['SF42_PublishingStatus__c'] ."', `Art_des_Dokuments__c` = '" .$_REQUEST['Art_des_Dokuments__c'] ."', 
`Auskunft_von__c` = '" .$_REQUEST['Auskunft_von__c'] ."', `Sperrvermerk__c` = '" .$_REQUEST['Sperrvermerk__c'] ."', 
`SF42_EventComment__c` = '" .str_replace("'", "\\'", $_REQUEST['SF42_EventComment__c']) ."', `Rueckmeldung_am__c` = '" .$_REQUEST['Rueckmeldung_am__c'] ."', 
`Art_der_Rueckmeldung__c` = '" .$_REQUEST['Art_der_Rueckmeldung__c'] ."', `Status_Klaerung__c` = '" .$_REQUEST['Status_Klaerung__c'] ."', 
`Grund__c` = '" .$_REQUEST['Grund__c'] ."', `Beitragsrueckstand__c` = '" .$_REQUEST['Beitragsrueckstand__c'] ."', `SF42_OnlineStatus__c` = '" .$_REQUEST['SF42_OnlineStatus__c'] ."', 
`Naechster_Meilenstein__c`  = '" .str_replace("'", "\\'", $_REQUEST['Naechster_Meilenstein__c']) ."', `bis_am__c` = '" .$_REQUEST['bis_am__c'] ."' 
WHERE `evid` = '" .$_REQUEST['det'] ."'";

      $result = MySQLStatic::Query($strSql3);
    
  }
  

  $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$_REQUEST['det'] .'"';
  $resSql = MySQLStatic::Query($strSql);
  
  $strEventName = $resSql[0]['Name'];
  
  $strSql2 = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$resSql[0]['Betriebsnummer_ZA__c'] .'"';
  $resSql2 = MySQLStatic::Query($strSql2);
  
  $strSql3 = 'SELECT * FROM `Account` WHERE `Id` = "' .$resSql[0]['SF42_informationProvider__c'] .'"';
  $resSql3 = MySQLStatic::Query($strSql3);
  
  if ($resSql[0]['bis_am__c'] != '0000-00-00') {
    $arrDate = explode('-', $resSql[0]['bis_am__c']);
    $resSql[0]['bis_am__c'] = $arrDate[2] .'.' .$arrDate[1] .'.' .$arrDate[0];
  } else {
    $resSql[0]['bis_am__c'] = '';
  }
  
  if ($resSql[0]['Rueckmeldung_am__c'] != '0000-00-00') {
    $arrDate = explode('-', $resSql[0]['Rueckmeldung_am__c']);
    $resSql[0]['Rueckmeldung_am__c'] = $arrDate[2] .'.' .$arrDate[1] .'.' .$arrDate[0];
  } else {
    $resSql[0]['Rueckmeldung_am__c'] = '';
  }  

/*

<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
	<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#tabs-1">Basis Daten</a></li>
		<li class="ui-state-default ui-corner-top"><a href="#tabs-2">Activity Stream</a></li>
	</ul>
	<div id="tabs-1" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
	
*/

$strClassTabs = 'ui-tabs ui-widget ui-widget-content ui-corner-all';
$strClassTabsUl = 'ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all';
$strUrlT1 = '/cms/index.php?ac=sear&det=' .$_REQUEST['det'] .'&tabs=1';
$strUrlT2 = '/cms/index.php?ac=sear&det=' .$_REQUEST['det'] .'&tabs=2';


if ((empty($_REQUEST['tabs'])) || ($_REQUEST['tabs'] == 1)) {

  $strClassT1 = 'ui-corner-top ui-tabs-selected ui-state-active';
  $strClassT2 = 'ui-corner-top ui-state-default';
  $strClassC1 = 'ui-tabs-panel ui-widget-content ui-corner-bottom';
  $strClassC2 = 'ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide';
} else {
  $strClassT1 = 'ui-corner-top ui-state-default';
  $strClassT2 = 'ui-corner-top ui-tabs-selected ui-state-active';
  $strClassC1 = 'ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide';
  $strClassC2 = 'ui-tabs-panel ui-widget-content ui-corner-bottom';
}


  $strOutput.= '
  
<div id="tabs" class="' .$strClassTabs .'">
	<ul class="' .$strClassTabsUl .'">
		<li class="' .$strClassT1 .'"><a href="' .$strUrlT1 .'">Basis Daten</a></li>
		<li class="' .$strClassT2 .'"><a href="' .$strUrlT2 .'">Activity Stream</a></li>
	</ul>
	<div id="tabs-1" class="' .$strClassC1 .'">
	
';

if ($_REQUEST['edit'] == 1) {

$strOutput.= '
	
			<form action="/cms/index.php?ac=sear&det=' .$_REQUEST['det'] .'" method="post" id="evedit">
			  
			  <input type="hidden" name="save" value="1">
			  
			  <div id="eventhead">
  			  <div id="eventcaption" class="clearfix">
  			    <div class="caption">' .$resSql[0]['Name'] .' - ' .$resSql[0]['SF42_Year__c'] .'/' .$resSql[0]['SF42_Month__c'] .' - PP: ' .$resSql2[0]['Name'] .' (' .$resSql[0]['Betriebsnummer_ZA__c'] .') - IP: ' .$resSql3[0]['Name'] .'</div>
  			    <div style="float: right;"><span id="add-mail" class="button">E-Mail</span> <span id="add-note" class="button">Notiz</span></div>
  			  </div>
  			  <div id="eventinfo" class="clearfix">
  			    <div class="form-left">
  			      <label for="Info__c">Status-Info</label> <textarea rows="4" cols="50" name="Info__c">' .$resSql[0]['Info__c'] .'</textarea>
  			    </div>
  			    <div class="form-right">
  			      <label for="bis_am__c">Wiedervorlage</label> <input type="text" id="datepicker" name="bis_am__c" value="' .$resSql[0]['bis_am__c'] .'">
  			    </div>
  			  </div>
        </div>
        
        <h2>Ergebnis</h2>
			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="SF42_OnlineStatus__c">Online Status</label> <select name="SF42_OnlineStatus__c">' .chr(10);
foreach ($arrOnline as $strKey => $strName) {
  if ($resSql[0]['SF42_OnlineStatus__c'] == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}
$strOutput.= '
			      </select>
			    </div>
			  </div>

			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="RecordTypeId">Phase</label> <select name="RecordTypeId">' .chr(10);
foreach ($arrRecordType as $strName => $strKey) {
  if ($resSql[0]['RecordTypeId'] == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}
$strOutput.= '
			      </select>
			    </div>
			    <div class="form-right">
			      <label for="SF42_EventStatus__c">Ergebnis</label> <select name="SF42_EventStatus__c">' .chr(10);
foreach ($arrChangePreview as $strKey => $strName) {
  if ($resSql[0]['SF42_EventStatus__c'] == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}
$strOutput.= '
			      </select>			    </div>
			  </div>

			  <div class="eventline clearfix">
			    <div class="form-left">
';

if ($resSql[0]['SF42_DocumentUrl__c'] != '') {
  $strLink = '<a href="' .$resSql[0]['SF42_DocumentUrl__c'] .'" target="_blank">Link</a> <input type="checkbox" name="deletelink" value="1" style="margin-left: 20px;"> Löschen';
} else {
  $strLink = '';
}

$strOutput.= '
			      <label for="SF42_DocumentUrl__c">Dokument URL</label> ' .$strLink .'
			      <input type="hidden" name="SF42_DocumentUrl__c" value="' .$resSql[0]['SF42_DocumentUrl__c'] .'">
			    </div>
			    <div class="form-right">
			      <label for="SF42_PublishingStatus__c">Dokument auf Portal sichtbar</label> <select name="SF42_PublishingStatus__c">' .chr(10);
foreach ($arrSichtbar as $strKey => $strName) {
  if ($resSql[0]['SF42_PublishingStatus__c'] == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}
$strOutput.= '
			      </select>			    </div>
			  </div>

			    <div class="form-right">
			      <label for="Auskunft_von__c">Auskunft von</label> <select name="Auskunft_von__c">' .chr(10);
foreach ($arrAuskunft as $strName => $strKey) {   
  if ($resSql[0]['Auskunft_von__c'] == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}
$strOutput.= '
			      </select>			    </div>
			  </div>

			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="Sperrvermerk__c">Sperrvermerk</label> <select name="Sperrvermerk__c">' .chr(10);
foreach ($arrSperr as $strKey => $strName) {
  if ($resSql[0]['Sperrvermerk__c'] == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}
$strOutput.= '
			      </select>
			    </div>
			    <div class="form-right">
			      <label for="SF42_EventComment__c">Kommentar auf Portal</label> <textarea rows="2" cols="45" name="SF42_EventComment__c">' .$resSql[0]['SF42_EventComment__c'] .'</textarea>
			    </div>
			  </div>


			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="Rueckmeldung_am__c">Rückmeldung am</label> <input type="text" id="datepicker2" name="Rueckmeldung_am__c" value="' .$resSql[0]['Rueckmeldung_am__c'] .'">
			    </div>
			  </div>
			  
        <h2>Klärungs-Details</h2>

			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="Status_Klaerung__c">Status</label> <select name="Status_Klaerung__c">' .chr(10);
  $strOutput.= '			        <option value=""></option>';
  
foreach ($arrStatusKlaerung as $strKey => $strName) {
  if ($resSql[0]['Status_Klaerung__c'] == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}
$strOutput.= '
			      </select>
			    </div>
			    <div class="form-right">
			      <label for="Grund__c">Grund</label> <select name="Grund__c">' .chr(10);
  $strOutput.= '			        <option value=""></option>';

foreach ($arrGrund as $strKey => $strName) {
  if ($resSql[0]['Grund__c'] == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}
$strOutput.= '
			      </select>
			    </div>
			  </div>

			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="Beitragsrueckstand__c">Beitragsrückstand in EUR</label> <input type="text" name="Beitragsrueckstand__c" value="' .$resSql[0]['Beitragsrueckstand__c'] .'">
			    </div>
			    <div class="form-right">
			      <label for="Naechster_Meilenstein__c">Nächster Schritt</label> <select name="Naechster_Meilenstein__c">' .chr(10);
  $strOutput.= '			        <option value=""></option>';

foreach ($arrMeilenstein as $strKey => $strName) {
  if (str_replace('"', "'", $resSql[0]['Naechster_Meilenstein__c']) == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}
$strOutput.= '
			      </select>
			    </div>
			  </div>

			</form>

      <p><button id="cancel-edit" class="ui-button ui-state-default ui-corner-all">Abbrechen</button>
      <button id="save-edit" class="ui-button ui-state-default ui-corner-all ui-state-hover">Speichern</button></p>
      
<script>
$("#cancel-edit").bind("click", function() {
  location.href = "/cms/index.php?ac=sear&det=' .$_REQUEST['det'] .'";
})
$("#save-edit").bind("click", function() {
  $("#evedit").submit();
})
</script>

';

} else {

$strOutput.= '
			  <div id="eventhead">
  			  <div id="eventcaption" class="clearfix">
  			    <div class="caption">' .$resSql[0]['Name'] .' - ' .$resSql[0]['SF42_Year__c'] .'/' .$resSql[0]['SF42_Month__c'] .' - PP: ' .$resSql2[0]['Name'] .' (' .$resSql[0]['Betriebsnummer_ZA__c'] .') - IP: ' .$resSql3[0]['Name'] .'</div>
  			    <div style="float: right;"><span id="add-mail" class="button">E-Mail</span> <span id="add-note" class="button">Notiz</span></div>
  			  </div>
  			  <div id="eventinfo" class="clearfix">
  			    <div class="form-left">
  			      <label for="Info__c">Status-Info</label> <span class="field" style="height: 88px;">' .$resSql[0]['Info__c'] .'</span>
  			    </div>
  			    <div class="form-right">
  			      <label for="bis_am__c">Wiedervorlage</label> <span class="field">' .$resSql[0]['bis_am__c'] .'</span>
  			    </div>
  			  </div>
        </div>
        
        <h2>Ergebnis</h2>
			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="SF42_OnlineStatus__c">Online Status</label> <span class="field">' .chr(10);
foreach ($arrOnline as $strKey => $strName) {
  if ($resSql[0]['SF42_OnlineStatus__c'] == $strKey) {
    $strOutput.= '' .$strName .'';
  }
}
$strOutput.= '
			      </span>
			    </div>
			  </div>

			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="RecordTypeId">Phase</label> <span class="field">' .chr(10);
foreach ($arrRecordType as $strName => $strKey) {
  if ($resSql[0]['RecordTypeId'] == $strKey) {
    $strOutput.= '' .$strName .'';
  }
}
$strOutput.= '
			      </span>
			    </div>
			    <div class="form-right">
			      <label for="SF42_EventStatus__c">Ergebnis</label> <span class="field">' .chr(10);
foreach ($arrChangePreview as $strKey => $strName) {
  if ($resSql[0]['SF42_EventStatus__c'] == $strKey) {
    $strOutput.= '' .$strName .'';
  }
}
$strOutput.= '
			      </span>
			    </div>
			  </div>

			  <div class="eventline clearfix">
			    <div class="form-left">
';

if ($resSql[0]['SF42_DocumentUrl__c'] != '') {
  $strLink = '<a href="' .$resSql[0]['SF42_DocumentUrl__c'] .'" target="_blank">Link</a>';
} else {
  $strLink = '';
}

$strOutput.= '
			      <label for="SF42_DocumentUrl__c">Dokument URL</label> ' .$strLink .'
			    </div>
			    <div class="form-right">
			      <label for="SF42_PublishingStatus__c">Dokument auf Portal sichtbar</label> <span class="field">' .chr(10);
foreach ($arrSichtbar as $strKey => $strName) {
  if ($resSql[0]['SF42_PublishingStatus__c'] == $strKey) {
    $strOutput.= '' .$strName .'';
  }
}
$strOutput.= '
			      </span>			    </div>
			  </div>

			    <div class="form-right">
			      <label for="Auskunft_von__c">Auskunft von</label> <span class="field">' .chr(10);
foreach ($arrAuskunft as $strName => $strKey) {   
  if ($resSql[0]['Auskunft_von__c'] == $strKey) {
    $strOutput.= '' .$strName .'';
  }
}
$strOutput.= '
			      </span>			    </div>
			  </div>

			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="Sperrvermerk__c">Sperrvermerk</label> <span class="field">' .chr(10);
foreach ($arrSperr as $strKey => $strName) {
  if ($resSql[0]['Sperrvermerk__c'] == $strKey) {
    $strOutput.= '' .$strName .'';
  }
}
$strOutput.= '
			      </span>
			    </div>
			    <div class="form-right">
			      <label for="SF42_EventComment__c">Kommentar auf Portal</label> <span class="field" style="height: 54px;">' .$resSql[0]['SF42_EventComment__c'] .'</span>
			    </div>
			  </div>


			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="Rueckmeldung_am__c">Rückmeldung am</label> <span class="field">' .$resSql[0]['Rueckmeldung_am__c'] .'</span>
			    </div>
			    <div class="form-right">
 			      <label for="Art_der_Rueckmeldung__c">Art der Rückmeldung</label>  <span class="field">' .chr(10);
foreach ($arrRueck as $strKey => $strName) {
  if ($resSql[0]['Art_der_Rueckmeldung__c'] == $strKey) {
    $strOutput.= '' .$strName .'';
  }
}
$strOutput.= '
			      </span>
			    </div>
			  </div>
			  
        <h2>Klärungs-Details</h2>

			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="Status_Klaerung__c">Status</label> <span class="field">' .chr(10);
foreach ($arrStatusKlaerung as $strKey => $strName) {
  if ($resSql[0]['Status_Klaerung__c'] == $strKey) {
    $strOutput.= '' .$strName .'';
  }
}
$strOutput.= '
			      </span>
			    </div>
			    <div class="form-right">
			      <label for="Grund__c">Grund</label> <span class="field">' .chr(10);
foreach ($arrGrund as $strKey => $strName) {
  if ($resSql[0]['Grund__c'] == $strKey) {
    $strOutput.= '' .$strName .'';
  }
}
$strOutput.= '
			      </span>
			    </div>
			  </div>

			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="Beitragsrueckstand__c">Beitragsrückstand in EUR</label> <span class="field">' .$resSql[0]['Beitragsrueckstand__c'] .'</span>
			    </div>
			    <div class="form-right">
			      <label for="Naechster_Meilenstein__c">Nächster Schritt</label> <span class="field">' .chr(10);

    $strOutput.= '' .$resSql[0]['Naechster_Meilenstein__c'] .'';

$strOutput.= '
			      </span>
			    </div>
			  </div>

      <p><button id="start-editDetail" class="ui-button ui-state-default ui-corner-all ui-state-hover">Bearbeiten</button></p>
      
<script>
$("#start-editDetail").bind("click", function() {
  location.href = "/cms/index.php?ac=sear&det=' .$_REQUEST['det'] .'&edit=1";
})

</script>

';

}

$strOutput.= '			
	</div>
	<div id="tabs-2" class="' .$strClassC2 .'">
	
	  <div id="stream" class="clearfix">
	    <div class="stream-left">

	      <p><a onclick="show(\'all\')" id="sall" class="snav">Alle</a></p>
	      <a onclick="show(\'notiz\')" id="snotiz" class="snav">Notizen</a><br>
	      <a onclick="show(\'file\')" id="sfile" class="snav">Files</a><br>
	      <a onclick="show(\'mail\')" id="smail" class="snav">Emails</a><br>
	      <a onclick="show(\'system\')" id="ssystem" class="snav">System</a><br>

	    </div>
	    <div class="stream-right">
	      
	      <h2>Activities</h2>
	      
	      <div id="stream-list">
';


$arrUser = array();
$strSql = 'SELECT `cl_id`, `cl_first`, `cl_last` FROM `cms_login` ORDER BY `cl_id`';
$resSql = MySQLStatic::Query($strSql);
if (is_array($resSql) && (count($resSql) > 0)) {
  foreach ($resSql as $intLine => $arrUserData) {
    $arrUser[$arrUserData['cl_id']] = $arrUserData['cl_first'] .' ' .$arrUserData['cl_last'];
  }
}


$strSql = 'SELECT * FROM `izs_event_change` WHERE `ev_id` = "' .$_REQUEST['det'] .'" ORDER BY `ec_time` DESC';
$resSql = MySQLStatic::Query($strSql);

$arrStreamType = array (
  0 => 'other', 
  1 => 'create', 
  2 => 'update', 
  3 => 'message',
  4 => 'mail'
);

$arrStreamClass = array (
  1 => 'system',
  2 => 'system', 
  3 => 'notiz',
  4 => 'file',
  5 => 'mail'
);


if (is_array($resSql) && (count($resSql) > 0)) {
  foreach ($resSql as $intLine => $arrStreamLine) {
    $intEcType = $arrStreamLine['ec_type'];
    $strStreamType  = $arrStreamType[$intEcType];
    $strStreamClass = $arrStreamClass[$intEcType];
    
    $strFiles = '';
    if (($arrStreamLine['ec_type'] == 3) || ($arrStreamLine['ec_type'] == 5)) {
      $strSql2 = 'SELECT * FROM `izs_event_file` WHERE `ec_id` = "' .$arrStreamLine['ec_id'] .'" ORDER BY `ef_date` DESC';
      $resSql2 = MySQLStatic::Query($strSql2);
      if (is_array($resSql2) && (count($resSql2) > 0)) {
        if ($arrStreamLine['ec_type'] == 3) {
          $strStreamClass = $arrStreamClass[4];
        } else {
          $strStreamClass = $arrStreamClass[5];
        }
        foreach ($resSql2 as $intLine => $arrFile) {
          $strFiles.= '<br /> - <a href="/cms/uploads_events/' .str_pad($arrFile['ef_id'], 11, '0', STR_PAD_LEFT) .'.' .$arrFile['ef_type'] .'" target="_blank">' .$arrFile['ef_name'] .'</a>' .chr(10);
        }
      }
    }
    
    $strOutput.= '<div class="stream-line ' .$strStreamType .' ' .$strStreamClass .'">' .chr(10);
  
    $strDbDate = strtotime($arrStreamLine['ec_time']);
    $strRefDay = gregoriantojd(date('m', $strDbDate), date('d', $strDbDate), date('Y', $strDbDate));
    $strToday  = unixtojd();
    
    $intInterval = ($strToday - $strRefDay);
    
    if ($intInterval == 0) {
      $strInterval = 'heute (' .date('H:i:s', $strDbDate) .' Uhr)';
    } elseif ($intInterval == 1) {
      $strInterval = 'gestern (' .date('H:i:s', $strDbDate) .' Uhr)';
    } else {
      $strInterval = 'vor ' .$intInterval .' Tagen';;
    }

    $strOutput.= '<span class="stream-head">' .$arrUser[$arrStreamLine['cl_id']] .' - ' .$strInterval .'</span>' .chr(10);
    
    if ($strStreamClass == 'system') {

      if ($arrStreamLine['ec_fild'] == 'RecordTypeId') {
        $strNew = $arrRecordTypeFlip[$arrStreamLine['ec_new']];
        $strOld = $arrRecordTypeFlip[$arrStreamLine['ec_old']];
      } elseif (($arrStreamLine['ec_fild'] == 'bis_am__c') || ($arrStreamLine['ec_fild'] == 'Rueckmeldung_am__c')) {
        
        if (($arrStreamLine['ec_new'] == '0000-00-00') || ($arrStreamLine['ec_new'] == '')) {
          $strNew = '';
        } else {        
          $strNew = date('d.m.Y', strtotime($arrStreamLine['ec_new']));
        }
        if (($arrStreamLine['ec_old'] == '0000-00-00') || ($arrStreamLine['ec_old'] == '')) {
          $strOld = '';
        } else {
          $strOld = date('d.m.Y', strtotime($arrStreamLine['ec_old']));
        }

      } elseif ($arrStreamLine['ec_fild'] == 'SF42_PublishingStatus__c') {
        $strNew = $arrSichtbar[$arrStreamLine['ec_new']];
        $strOld = $arrSichtbar[$arrStreamLine['ec_old']];
      } elseif ($arrStreamLine['ec_fild'] == 'SF42_EventStatus__c') {
        $strNew = $arrChangePreview[$arrStreamLine['ec_new']];
        $strOld = $arrChangePreview[$arrStreamLine['ec_old']];
      } elseif ($arrStreamLine['ec_fild'] == 'Sperrvermerk__c') {
        $strNew = $arrSperr[$arrStreamLine['ec_new']];
        $strOld = $arrSperr[$arrStreamLine['ec_old']];
      } elseif ($arrStreamLine['ec_fild'] == 'SF42_OnlineStatus__c') {
        $strNew = $arrOnline[$arrStreamLine['ec_new']];
        $strOld = $arrOnline[$arrStreamLine['ec_old']];
      } else {
        $strNew = $arrStreamLine['ec_new'];
        $strOld = $arrStreamLine['ec_old'];
      }
      
      if ($arrStreamLine['ec_type'] == 1) {
        $strOutput.= '<span>Event importiert.</span>' .chr(10);
      } else {
        $strOutput.= '<span>' .$arrSystemStream[$arrStreamLine['ec_fild']] .'<br/>neu: "' .$strNew .'"<br/>alt: "' .$strOld .'"</span>' .chr(10);
      }
    
    } elseif (($strStreamClass == 'notiz') || ($strStreamClass == 'file') || ($strStreamClass == 'mail')) {
      
      $strRecipients = '';
      
      if ($strStreamClass != 'mail') {
        $strNew = nl2br($arrStreamLine['ec_new']);
      } else {
        $arrMail = unserialize($arrStreamLine['ec_new']);
        
        $strNew = '';
        //$strNew.= print_r($arrMail, true);
        $strNew.= '<p>sendete folgende Nachricht:<br /><strong>' .$arrMail['subject'] .'</strong>';
        $strNew.= '<p>' .nl2br($arrMail['message']) .'</p>';

        $strRecipients.= "<p><strong>Empfänger:</strong><br />\n";
        $strRecipients.= "An: " .$arrMail['to'] ."<br />\n";
        $strRecipients.= "CC: " .$arrMail['cc'] ."<br />\n";
        $strRecipients.= "BCC: " .$arrMail['bcc'] ."<br />\n";
        $strRecipients.= "</p>\n";
      }

      if (($strStreamClass == 'file') || ($strStreamClass == 'mail')) {
        
        $strOutput.= '<div class="comment">' .chr(10);
        $strOutput.= '  ' .$strNew .'' .chr(10);
        
        if ($strFiles != '') {
          $strOutput.= '<p><strong>Files:</strong>' .chr(10);
          $strOutput.= $strFiles;
          $strOutput.= '</p>' .chr(10);
        }
        if ($strRecipients != '') {
          $strOutput.= $strRecipients;
        }
        
        $strOutput.= '</div>' .chr(10);
        
      } else {
        $strOutput.= '<div class="comment more">' .chr(10);
        $strOutput.= '  ' .$strNew .'' .chr(10);
        if ($strRecipients != '') {
          $strOutput.= $strRecipients;
        }
        $strOutput.= '</div>' .chr(10);
      }
      
    }
    
    $strOutput.= '';
    $strOutput.= '</div>' .chr(10);

  }
}


//$strOutput.= print_r($resSql, true);	      

$strOutput.= '	      
	      </div> <!-- Stream-List -->
	    </div> <!-- Stream-Right -->
	  </div>
	</div>  <!-- Stream -->
    
</div>

</div><!-- End demo -->

 
	<script type="text/javascript">
	$(function() {
		//$("#tabs").tabs();
	});

	$(function() {
		$("#datepicker").datepicker();
	});

	$(function() {
		$("#datepicker2").datepicker();
	});
  	
  function show(type) {
    if (type == "all") {
      $(".stream-line").removeClass(\'hidden\');
    } else {
      $(".stream-line").addClass(\'hidden\');
      $("." + type).removeClass(\'hidden\');
    }
    $(".snav").css("font-weight", "normal");
    $("#s" + type).css("font-weight", "bold");
  }	
  
$(document).ready(function() {
	var showChar = 200;
	var ellipsestext = "...";
	var moretext = "more";
	var lesstext = "less";
	$(\'.more\').each(function() {
		var content = $(this).html();

		if(content.length > showChar) {

			var c = content.substr(0, showChar);
			var h = content.substr(showChar-1, content.length - showChar);

			var html = c + \'<span class="moreelipses">\'+ellipsestext+\'</span>&nbsp;<span class="morecontent"><span>\' + h + \'</span>&nbsp;&nbsp;<a href="" class="morelink">\'+moretext+\'</a></span>\';

			$(this).html(html);
		}

	});

	$(".morelink").click(function(){
		if($(this).hasClass("less")) {
			$(this).removeClass("less");
			$(this).html(moretext);
		} else {
			$(this).addClass("less");
			$(this).html(lesstext);
		}
		$(this).parent().prev().toggle();
		$(this).prev().toggle();
		return false;
	});
});
  </script>
  
	
<style>
#queue, #queueM {
	height: 79px;
	overflow: auto;
	margin-bottom: 10px;
	padding: 0 3px 3px;
	width: 365px;

  background-color: #5BB75B;
  background-repeat: repeat-x;
  border-radius: 4px;
  color: #FFFFFF;
}

#emp, #empM {
  text-align: center;
  width: 365px;
  display: block;
  padding-top: 30px;
}
.uploadifive-queue-item {
    padding: 3px;
}
.uploadifive-queue-item .close {
    background: url("/cms/img/uploadifive-cancel.png") no-repeat scroll 0 2px rgba(0, 0, 0, 0);
}
</style>

<div id="dialog_note" title="Neue Notiz">
	<p id="validateTips"></p>

	<form>
	<fieldset>
		<label for="message"></label>
		<textarea name="message" id="message" class="text ui-widget-content ui-corner-all"></textarea>
	</fieldset>

	<div id="queue"><span id="emp">Drop Files here...</span></div>
	<input type="hidden" name="fnames" id="fnames" />
	<input type="file" name="file_upload" id="file_upload" />

';

$timestamp = time();

$strOutput.= '
<script type="text/javascript">

function dump(obj) {
    var out = "";
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }
    alert(out);
}

$(document).ready(function($) {
      $(\'#file_upload\').uploadifive({
        \'auto\'             : true,
        \'multi\'            : false, 
        \'queueSizeLimit\'   : 999,
        \'uploadLimit\'      : 999, 
				\'formData\'         : { \'source\': \'note\', \'timestamp\': \'' .$timestamp .'\', \'token\': \'' .md5('unique_salt' .$timestamp) .'\' },
        \'queueID\'          : \'queue\',
        \'itemTemplate\'     : \'<div class="uploadifive-queue-item"><span class="filename"></span><span class="fileinfo"></span><div class="close"></div></div>\', 
        \'uploadScript\'     : \'uploadifive.php\',
        \'onUploadComplete\' : function(file, data) { if (file.name != "") { var input = $("#fnames"); input.val(input.val() + "|" + file.name);} }, 
        \'onAddQueueItem\'   : function(file) { $("#emp").css("display", "none"); }, 
        \'onCancel\'         : function(file) { var input = $("#fnames"); if (file.name != "") { input.val(input.val().replace("|" + file.name, "")); } if (input.val() == "") { setTimeout("$(\'#emp\').css(\'display\', \'block\');", 2000); } } 
      });
    });

</script>


	</form>
</div>


<div id="dialog_mail" title="Neue E-Mail" class="clearfix">
<div style="width: 150px; border-right: 1px solid #DDDDDD; float: left; height: 700px;">
';

$strSql = 'SELECT * FROM `cms_text` WHERE `ct_type` = 1 ORDER BY `ct_name`';
$arrResult = MySQLStatic::Query($strSql);

$strJs = '';
$strJs.= 'var subj = new Array(); ' .chr(10);
$strJs.= 'var mess = new Array(); ' .chr(10);
$strJs.= 'subj[0] = "";' .chr(10);
$strJs.= 'mess[0] = "";' .chr(10);

$strOutput.= '<p><a onclick="pattern(\'0\')" id="p_0" class="pnav" style="font-weight: bold;">keine Vorlage</a></p>';

if (count($arrResult) > 0) {
  foreach ($arrResult as $arrText) {
    $strOutput.= '<a onclick="pattern(\'' .$arrText['ct_id'] .'\')" id="p_' .$arrText['ct_id'] .'" class="pnav">' .$arrText['ct_name'] .'</a><br />' .chr(10);
    $strJs.= 'subj[' .$arrText['ct_id'] .'] = "' .str_replace('"', '\"', replace_placeholder($arrText['ct_head'], $_REQUEST['det'])) .'";' .chr(10);
    $strJs.= 'mess[' .$arrText['ct_id'] .'] = "' .str_replace('"', '\"', replace_placeholder($arrText['ct_text'], $_REQUEST['det'])) .'";' .chr(10);
  }
}

$strOutput.= '
</div>

<div style="width: 580px; float: left; padding: 0 0px 10px 20px;">
	<p id="validateTipsM" class="validateTips"></p>

	<form id="eMail">
	  <fieldset class="clearfix">

<label for="to">An:</label>
<input id="to" class="text ui-widget-content ui-corner-all" type="text" name="to">
<label for="cc">CC:</label>
<input id="cc" class="text ui-widget-content ui-corner-all" type="text" name="cc">
<label for="bcc">BCC:</label>
<input id="bcc" class="text ui-widget-content ui-corner-all" type="text" name="bcc">

<label for="subject" class="space">Betreff:</label>
<input id="subject" class="text ui-widget-content ui-corner-all space" type="text" name="subject">

</fieldset>

	<fieldset class="clearfix">
		<label for="messageM">Nachricht</label><span class="clearfix"></span>
		<textarea name="messageM" id="messageM" class="text ui-widget-content ui-corner-all"></textarea>
	</fieldset>

	<div id="queueM" style="margin-top: 10px;"><span id="empM">Drop Files here...</span></div>
	<input type="hidden" name="fnames" id="fnames" />
	<input type="file" name="file_upload" id="file_upload" />

';

$timestamp = time();

$strOutput.= '
<script type="text/javascript">

bkLib.onDomLoaded(function() {
  mM = new nicEditor({
    buttonList : ["bold","italic","underline","left","center","right"], 
    maxHeight : 370
  }).panelInstance("messageM");
});

' .$strJs .'
  function pattern(type) {
    $("#subject").val(subj[type]);
    $("#messageM").val(mess[type]);
    $(".nicEdit-main").focus();
    mM.nicInstances[0].setContent(mess[type]);
    $(".pnav").css("font-weight", "normal");
    $("#p_" + type).css("font-weight", "bold");
  }	

function dump(obj) {
    var out = "";
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }
    alert(out);
}

$(document).ready(function($) {
      $(\'#file_upload\').uploadifive({
        \'auto\'             : true,
        \'multi\'            : false, 
        \'queueSizeLimit\'   : 999,
        \'uploadLimit\'      : 999, 
				\'formData\'         : { \'source\': \'note\', \'timestamp\': \'' .$timestamp .'\', \'token\': \'' .md5('unique_salt' .$timestamp) .'\' },
        \'queueID\'          : \'queueM\',
        \'itemTemplate\'     : \'<div class="uploadifive-queue-item"><span class="filename"></span><span class="fileinfo"></span><div class="close"></div></div>\', 
        \'uploadScript\'     : \'uploadifive.php\',
        \'onUploadComplete\' : function(file, data) { if (file.name != "") { var input = $("#fnames"); input.val(input.val() + "|" + file.name);} }, 
        \'onAddQueueItem\'   : function(file) { $("#empM").css("display", "none"); }, 
        \'onCancel\'         : function(file) { var input = $("#fnames"); if (file.name != "") { input.val(input.val().replace("|" + file.name, "")); } if (input.val() == "") { setTimeout("$(\'#empM\').css(\'display\', \'block\');", 2000); } } 
      });
    });

</script>


	</form>
</div>
</div>


	<script type="text/javascript">

	
  $(document).ready(function() {


		var message = $("#message"), messageM = $("#messageM"), to = $("#to"), cc = $("#cc"), bcc = $("#bcc"), subject = $("#subject"),
			allFields = $([]).add(message).add(to).add(cc).add(bcc).add(subject),
			tips = $("#validateTips"), 
			tipsM = $("#validateTipsM");

		function updateTips(t) {
			tips.text(t).effect("highlight",{},1500);
		}

		function updateTipsM(t) {
		  alert(t);
			tipsM.text(t).effect("highlight",{},1500);
		}

		function checkLength(o,f,min) {
			if ( o.val().length < min ) {
				o.addClass(\'ui-state-error\');
				updateTipsM("Bitte das Feld \'" +f +"\' ausfüllen.");
				return false;
			} else {
				return true;
			}
		}
		
		function checkEnty(o,n,min) {
			if ( o.val().length < min ) {
				o.addClass(\'ui-state-error\');
				updateTips("Es steht nichts im Notizfeld. Bitte nutze das Feld :-).");
				return false;
			} else {
				return true;
			}
		}

		function checkRegexp(o,regexp,n) {
			if ( !( regexp.test( o.val() ) ) ) {
				o.addClass(\'ui-state-error\');
				updateTipsM(n);
				return false;
			} else {
				return true;
			}
		}
    
		$("#dialog_note").dialog({
			bgiframe: true,
			autoOpen: false,
			resizable: false,
			height: 365,
			width: 400,
			modal: true,
			buttons: {
				\'OK\': function() {
					var bValid = true;
					var mes = message.val();
					allFields.removeClass(\'ui-state-error\');
					

					bValid = bValid && checkEnty(message,"message",1);
					
					if (bValid) {
            $.ajax({
              type: "POST",
              dataType: "html; charset=utf-8", 
              url:  "_ajax.php",
              data: { id: ' .$_REQUEST['det'] .', fnames: $("#fnames").val(), message: mes, type: \'event\', name: "' .$strEventName .'" },
            });
            $(this).dialog(\'close\');
					}
				},
				\'Abbrechen\': function() {
					$(this).dialog(\'close\');
				}
			},
			close: function() {
			  tips.text(\' \');
				allFields.val(\'\').removeClass(\'ui-state-error\');
			}
		});
		
		
		$(\'#add-note\').click(function() {
      $(\'#queue\').html(\'<span id="emp">Drop Files here...</span>\');
      $(\'#file_upload\').uploadifive(\'clearQueue\');
			$(\'#dialog_note\').dialog(\'open\');
			
			$("#dialog_note ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');

		})
		.hover(
			function(){ 
				$(this).addClass("ui-state-hover"); 
			},
			function(){ 
				$(this).removeClass("ui-state-hover"); 
			}
		).mousedown(function(){
			$(this).addClass("ui-state-active"); 
		})
		.mouseup(function(){
				$(this).removeClass("ui-state-active");
		});


    
		$("#dialog_mail").dialog({
			bgiframe: true,
			autoOpen: false,
			resizable: false,
			height: 800,
			width: 800,
			modal: true,
			buttons: {
				\'OK\': function() {
					var bValid = true;
					var mesM = $(\'#eMail\').find(\'.nicEdit-main\').html();
					var toM = cc.val();
					var ccM = cc.val();
					var bccM = bcc.val();
					var subjectM = subject.val();
					allFields.removeClass(\'ui-state-error\');
					
					
					bValid = bValid && checkLength(to,"An",1);
					bValid = bValid && checkLength(subject,"Betreff",1);

					bValid = bValid && checkEnty(messageM,"message",1);

          bValid = bValid && checkRegexp(to,/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i,"Keine gültige E-Mail-Adresse.");
          
          if ($("#cc").val() != "") {
            bValid = bValid && checkRegexp(cc,/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i,"Keine gültige E-Mail-Adresse.");
          }
          
          if ($("#bcc").val() != "") {
            bValid = bValid && checkRegexp(bcc,/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i,"Keine gültige E-Mail-Adresse.");
				  }
				  	
					if (bValid) {
            $.ajax({
              type: "POST",
              url:  "_ajax.php",
              dataType: "html; charset=utf-8", 
              data: { id: ' .$_REQUEST['det'] .', to: $("#to").val(), cc: $("#cc").val(), bcc: $("#bcc").val(), subject: $("#subject").val(), fnames: $("#fnames").val(), message: mesM, type: \'mail\', name: "' .$strEventName .'" },
            });
            $(this).dialog(\'close\');

					}
				},
				\'Abbrechen\': function() {
					$(this).dialog(\'close\');
				}
			},
			close: function() {
			  tips.text(\' \');
				allFields.val(\'\').removeClass(\'ui-state-error\');
			}
		});
		
		
		$(\'#add-mail\').click(function() {
		  $(\'#messageM\').val(\'\');
      $(\'#queueM\').html(\'<span id="empM">Drop Files here...</span>\');
      $(\'#file_upload\').uploadifive(\'clearQueue\');
      pattern("0");
			$(\'#dialog_mail\').dialog(\'open\');
			    
      $("#dialog_mail ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');
      
		})
		.hover(
			function(){ 
				$(this).addClass("ui-state-hover"); 
			},
			function(){ 
				$(this).removeClass("ui-state-hover"); 
			}
		).mousedown(function(){
			$(this).addClass("ui-state-active"); 
		})
		.mouseup(function(){
				$(this).removeClass("ui-state-active");
		});

	});
	
	</script>

  ';

} else {

// SUCHE BEGIN 

$strSql0 = "SELECT `Beitragsmonat__c`, `Beitragsjahr__c` FROM `cron_izs_sv_month` ORDER BY `Beitragsjahr__c` DESC, `Beitragsmonat__c` DESC";
$arrResult0 = MySQLStatic::Query($strSql0);
if (count($arrResult0) > 0) {
  
  $strOutput.= '<form method="post" action="">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);

  $strOutput.= '<table autocomplete="off">' .chr(10);
  $strOutput.= '<tbody>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td style="width: 150px;">Zeitraum: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelDate" id="strSelDate">' .chr(10);
  
  $intPeriod = 0;

  foreach ($arrResult0 as $arrPeriod0) {
    $strSelected = '';
    $strValue = $arrPeriod0['Beitragsmonat__c'] .'_' .$arrPeriod0['Beitragsjahr__c'];
    
    if (empty($_REQUEST['strSelDate']) && ($intPeriod == 0)) {
      $_REQUEST['strSelDate'] = $strValue;
      //$_REQUEST['send'] = 1;
    }
    
    if ($strValue == $_REQUEST['strSelDate']) {
      $strSelected = ' selected="selected"';
    } 
    $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .str_pad($arrPeriod0['Beitragsmonat__c'], 2 ,'0', STR_PAD_LEFT) .'-' .$arrPeriod0['Beitragsjahr__c'] .'</option>' .chr(10);
    $intPeriod++;
  }
  
  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Event: </td>' .chr(10);
  $strOutput.= '    <td>E- <input type="text" name="strSearchValue" value="' .$_REQUEST['strSearchValue'] .'"></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);


  $arrStatusCss = array ( 
    'abgelehnt / Dublette' => 'warning', 
    'abgelehnt / Frist' => 'warning', 
    'accepted' => 'warning', 
    'bereit für REVIEW' => 'warning', 
    'enquired' => 'wait', 
    'in progress' => 'warning', 
    'new' => 'warning', 
    'no Feedback' => 'question', 
    'no result' => 'warning', 
    'not assignable' => 'warning', 
    'not OK' => 'warning', 
    'OK' => 'thick', 
    'refused' => 'warning', 
    'to clear' => 'warning', 
    'to enquire' => 'warning', 
    'zugeordnet / abgelegt' => 'warning', 
    'zurückgestellt von IZS' => 'warning',
    'gestundet' => 'question'
  );

  $arrStatus = $arrChangePreview;
    
  if (count($arrStatus) > 0) {
    
    if (($_REQUEST['strSelSta'] == 'all') || ($_REQUEST['strSelSta'] == '')) {
      $strSelectedAll = ' selected="selected"';
      $_REQUEST['strSelSta'] = 'all';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Event-Status: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelSta" id="strSelSta">' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>Alle</option>' .chr(10);
    
    foreach ($arrStatus as $strStatus => $strValue) {
      $strSelected = '';
      if ($strStatus == $_REQUEST['strSelSta']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option value="' .$strStatus .'"' .$strSelected .'>' .$strValue .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
  
  if (count($arrRecordType) > 0) {
    
    if (($_REQUEST['strSelRec'] == 'all') || ($_REQUEST['strSelRec'] == '')) {
      $strSelectedAll = ' selected="selected"';
      $_REQUEST['strSelRec'] = 'all';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Datensatz-Typ: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelRec" id="strSelRec">' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>Alle</option>' .chr(10);
    
    foreach ($arrRecordType as $strValue => $strId) {
      $strSelected = '';
      if ($strId == $_REQUEST['strSelRec']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option value="' .$strId .'"' .$strSelected .'>' .$strValue .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
    
    
  $strSql6 = 'SELECT * FROM `izs_informationprovider` ORDER BY `Name`';
  
  $arrResult6 = MySQLStatic::Query($strSql6);
  if (count($arrResult6) > 0) {

    if ($_REQUEST['strSelInf'] == 'all') {
      $strSelectedAll = ' selected="selected"';
    } else {
      $strSelectedAll = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Information Provider: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelInf" id="strSelInf">' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Information Provider -</option>' .chr(10);
    
    foreach ($arrResult6 as $arrProvider) {
      $strSelected = '';
      if ($arrProvider['Id'] == $_REQUEST['strSelInf']) {
        $strSelected = ' selected="selected"';
      } 
      
      $strAddDez = '';
      
      if ($arrProvider['DezentraleAnfrage'] == 'true') {
        $strOptClass = ' class="optv"';
        
        $strSqlDez = 'SELECT `Id`, `Name`, `CATCH_ALL__c` FROM `Anfragestelle__c` WHERE `Information_Provider__c`="' .$arrProvider['Id'] .'" ORDER BY `Name`';
        $arrSqlDez = MySQLStatic::Query($strSqlDez);

        //$arrProvider['Id'] = 'da_' .$arrProvider['Id'];
        
        if (count($arrSqlDez) > 0) {
          foreach ($arrSqlDez as $arrDez) {
            
            $strSelectedDez = '';
            if (('d_' .$arrDez['Id']) == $_REQUEST['strSelInf']) {
              $strSelectedDez = ' selected="selected"';
            } 
            $strAddDez.= '    <option value="d_' .$arrDez['Id'] .'"' .$strSelectedDez .'>&nbsp;&nbsp;- ' .$arrDez['Name'] .'</option>' .chr(10);

          } 
        }
        
      } else {
        $strOptClass = '';
      }
      
      $strOutput.= '    <option' .$strOptClass .' value="' .$arrProvider['Id'] .'"' .$strSelected .'>' .$arrProvider['Name'] .'</option>' .chr(10);
      $strOutput.= $strAddDez;
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }

    
  $strSql6 = 'SELECT `Name`, `SF42_Comany_ID__c` AS `Btnr`  FROM `Account` WHERE `SF42_isPremiumPayer__c` = "true" AND `SF42_Comany_ID__c` != "" AND `SF42_Company_Group__c` != "a083A00000lqnERQAY" ORDER BY `Name` ';
  
  $arrResult6 = MySQLStatic::Query($strSql6);
  if (count($arrResult6) > 0) {

    if ($_REQUEST['strSelPay'] == 'all') {
      $strSelectedAll = ' selected="selected"';
    } else {
      $strSelectedAll = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Premium Payer: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelPay" id="strSelPay">' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Premium Payer -</option>' .chr(10);
    
    foreach ($arrResult6 as $intKey => $arrProvider) {
      $strSelected = '';
      if ($arrProvider['Btnr'] == $_REQUEST['strSelPay']) {
        $strSelected = ' selected="selected"';
      } 
      
      if (($strLastName == $arrProvider['Name']) || ($arrResult6[$intKey + 1]['Name'] == $arrProvider['Name'])) {
         $strAdd = ' (' .$arrProvider['Btnr'] .')';
      } else {
        $strAdd = '';
      }
      
      $strOutput.= '    <option value="' .$arrProvider['Btnr'] .'"' .$strSelected .'>' .$arrProvider['Name'] .'' .$strAdd .'</option>' .chr(10);
      $strLastName = $arrProvider['Name'];
      
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }

      
  $strSql6 = 'SELECT `Name`, `Id`  FROM `izs_premiumpayer_group` WHERE `Id` != "a083A00000lqnERQAY" ORDER BY `Name`';
    
  $arrResult6 = MySQLStatic::Query($strSql6);
  if (count($arrResult6) > 0) {

    if ($_REQUEST['strSelGru'] == 'all') {
      $strSelectedAll = ' selected="selected"';
    } else {
      $strSelectedAll = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Company Group: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelGru" id="strSelGru">' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Company Groups -</option>' .chr(10);
    
    foreach ($arrResult6 as $intKey => $arrProvider) {
      $strSelected = '';
      if ($arrProvider['Id'] == $_REQUEST['strSelGru']) {
        $strSelected = ' selected="selected"';
      } 
      
      $strOutput.= '    <option value="' .$arrProvider['Id'] .'"' .$strSelected .'>' .$arrProvider['Name'] .'</option>' .chr(10);
      //$strLastName = $arrProvider['Name'];
      
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
  

    if (isset($_REQUEST['outstanding']) && ($_REQUEST['outstanding'] == 1)) {
      $strChecked = ' checked="checked"';
    } else {
      $strChecked = '';
    }
    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Rückmeldung: </td>' .chr(10);
    $strOutput.= '    <td><input type="checkbox" value="1" name="outstanding"' .$strChecked .'> ausstehend</td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Rückmeldung am: </td>' .chr(10);
    $strOutput.= '    <td><input type="text" id="datepicker3" name="Rueckmeldung_am__c" value="' .$_REQUEST['Rueckmeldung_am__c'] .'" autocomplete="off"></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);

    /*
    if (count($arrRueck) > 0) {
  
      if ($_REQUEST['strSelAru'] == 'all') {
        $strSelectedAll = ' selected="selected"';
      } else {
        $strSelectedAll = '';
      }
  
      $strOutput.= '  <tr>' .chr(10);
      $strOutput.= '    <td>Art der Rückmeldung: </td>' .chr(10);
      $strOutput.= '    <td><select name="strSelAru" id="strSelAru">' .chr(10);
      $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Arten -</option>' .chr(10);
      
      foreach ($arrRueck as $strKey => $strArtRueck) {
        $strSelected = '';
        if ($strKey == $_REQUEST['strSelAru']) {
          $strSelected = ' selected="selected"';
        } 
        
        $strOutput.= '    <option value="' .$strKey .'"' .$strSelected .'>' .$strArtRueck .'</option>' .chr(10);
        //$strLastName = $arrProvider['Name'];
        
      }
    
      $strOutput.= '    </select></td>' .chr(10);
      $strOutput.= '  </tr>' .chr(10);

    }
    */

    if (count($arrUserList) > 0) {
  
      if ($_REQUEST['strSelEvo'] == 'all') {
        $strSelectedAll = ' selected="selected"';
      } else {
        $strSelectedAll = '';
      }
  
      $strOutput.= '  <tr>' .chr(10);
      $strOutput.= '    <td>Erfasst von: </td>' .chr(10);
      $strOutput.= '    <td><select name="strSelEvo" id="strSelEvo">' .chr(10);
      $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle MitarbeiterInnen -</option>' .chr(10);
      
      foreach ($arrUserList as $strKey => $strUser) {
        $strSelected = '';
        if ($strKey == $_REQUEST['strSelEvo']) {
          $strSelected = ' selected="selected"';
        } 
        
        $strOutput.= '    <option value="' .$strKey .'"' .$strSelected .'>' .$strUser .'</option>' .chr(10);
        //$strLastName = $arrProvider['Name'];
        
      }
    
      $strOutput.= '    </select></td>' .chr(10);
      $strOutput.= '  </tr>' .chr(10);

    }

    $arrStundung = array(
      'Stundung' => 'Stundung',
      'Ratenzahlung' => 'Ratenzahlung',
      'Stundung oder Ratenzahlung' => 'Stundung oder Ratenzahlung',
    );
      
    if (count($arrStundung) > 0) {
  
      if (($_REQUEST['strSelStu'] == 'all') || ($_REQUEST['strSelStu'] == '')) {
        $strSelectedAll = ' selected="selected"';
      } else {
        $strSelectedAll = '';
      }
  
      if ($_REQUEST['strSelStu'] == 'empty') {
        $strSelectedEmpty = ' selected="selected"';
      } else {
        $strSelectedEmpty = '';
      }

      $strOutput.= '  <tr>' .chr(10);
      $strOutput.= '    <td width="160">Stundung/Ratenzahlung: </td>' .chr(10);
      $strOutput.= '    <td><select name="strSelStu" id="strSelStu">' .chr(10);
      $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle anzeigen -</option>' .chr(10);
      $strOutput.= '    <option value="empty"' .$strSelectedEmpty .'>kein Eintrag</option>' .chr(10);
      
      foreach ($arrStundung as $strKey => $strStundung) {
        $strSelected = '';
        if ($strKey == $_REQUEST['strSelStu']) {
          $strSelected = ' selected="selected"';
        } 
        
        $strOutput.= '    <option value="' .$strKey .'"' .$strSelected .'>' .$strStundung .'</option>' .chr(10);
        //$strLastName = $arrProvider['Name'];
        
      }
    
      $strOutput.= '    </select></td>' .chr(10);
      $strOutput.= '  </tr>' .chr(10);
    }

    $strOutput.= '</tbody>' .chr(10);

    $strOutput.= '  <tfoot>' .chr(10);
    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td align="right" colspan="2"><input type="submit" name="Suche" value="Suche" /></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
    $strOutput.= '  </tfoot>' .chr(10);


  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);
    
}

if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {

  if (in_array($_REQUEST['strSelSta'], $arrChangePreview) == true) { 
    $boolMore = true;
  } else {
    $boolMore = false;
  }
  
  $arrPeriod = explode('_', $_REQUEST['strSelDate']);
  
  if (strstr($_REQUEST['strSelInf'], 'd_') != false) {
    
    $_REQUEST['strSelInf'] = substr($_REQUEST['strSelInf'], 2);
    $strAnfragestelleId = $_REQUEST['strSelInf'];
    
    $strSqlDez = 'SELECT `Id`, `Name`, `CATCH_ALL__c` FROM `Anfragestelle__c` WHERE `Id`="' .$_REQUEST['strSelInf'] .'"';
    $arrSqlDez = MySQLStatic::Query($strSqlDez);
    
    $strSqlPp = 'SELECT `Premium_Payer__c`, `Information_provider__c` FROM `Dezentrale_Anfrage_KK__c` WHERE `Anfragestelle__c` = "' .$_REQUEST['strSelInf'] .'"';
    $arrSqlPp = MySQLStatic::Query($strSqlPp);

    $arrPayer = array();
    $arrPayerAccount = array();
    $strPremiumPayer = '';
    
    if (count($arrSqlPp) > 0) {
      foreach ($arrSqlPp as $arrPremiumPayer) {
        $arrPayer[] = '"' .$arrPremiumPayer['Premium_Payer__c'] .'"';
        $_REQUEST['strSelInf'] = $arrPremiumPayer['Information_provider__c'];
      }
    
      $strPremiumPayer = implode(',', $arrPayer);
      
      $strSqlPpA = 'SELECT `SF42_Comany_ID__c` FROM `Account` WHERE `Id` IN (' .$strPremiumPayer .')';
      $arrSqlPpA = MySQLStatic::Query($strSqlPpA);

      if (count($arrSqlPpA) > 0) {
        foreach ($arrSqlPpA as $arrPpAccount) {
          $arrPayerAccount[] = '"' .$arrPpAccount['SF42_Comany_ID__c'] .'"';
        }
        $strPpAccount = implode(',', $arrPayerAccount);
      }
      
    }
    
    if ($arrSqlDez[0]['CATCH_ALL__c'] == 'true') {
      
      
      $strSqlKK = 'SELECT * FROM `Anfragestelle__c` WHERE `Id`="' .$strAnfragestelleId .'"';
      $arrKK = MySQLStatic::Query($strSqlKK);
      
      $boolIsCatchAll = true;
      $arrCatchAll = $arrKK[0];
      
      $strSqlKK = 'SELECT * FROM `Account` WHERE `Id`="' .$arrKK[0]['Information_Provider__c'] .'"';
      $arrKK = MySQLStatic::Query($strSqlKK);

      $strSql6 = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Beitragsmonat__c` = "' .$arrPeriod[0] .'" AND `Beitragsjahr__c` = "' .$arrPeriod[1] .'" ';
      if (($_REQUEST['strSelSta'] != '') && ($_REQUEST['strSelSta'] != 'all')) {
        $strSql6.= 'AND `SF42_EventStatus__c` = "' .$_REQUEST['strSelSta'] .'" ';
      } else {
        $strSql6.= 'AND `SF42_EventStatus__c` IN (' .$strQueryAdd .') ';
      }
      if (isset($_REQUEST['outstanding']) && ($_REQUEST['outstanding'] == 1)) {
        $strSql6.= 'AND ((`Rueckmeldung_am__c` = "0000-00-00") || (`Rueckmeldung_am__c` = "")) ';
      }
      if (isset($_REQUEST['Rueckmeldung_am__c']) && ($_REQUEST['Rueckmeldung_am__c'] != '')) {
        $strRueckAm = date('Y-m-d', strtotime($_REQUEST['Rueckmeldung_am__c']));
        $strSql6.= 'AND (`Rueckmeldung_am__c` = "' .$strRueckAm .'") ';
      }
      if ($_REQUEST['strSearchValue'] != '') {
        $strSql6.= 'AND `Name` LIKE "E-' .$_REQUEST['strSearchValue'] .'%" ';
      }

      if (($_REQUEST['strSelStu'] != '') && ($_REQUEST['strSelStu'] != 'all')) {
        if ($_REQUEST['strSelStu'] == 'empty') {
          $strSql6.= 'AND (`Stundung__c` = "") ';
        } else {
          $strSql6.= 'AND (`Stundung__c` = "' .$_REQUEST['strSelStu'] .'") ';
        }
      }
      
      $strSql6.= 'AND `SF42_informationProvider__c` = "' .$arrKK[0]['Id'] .'" GROUP BY `Betriebsnummer_ZA__c` ';
      
      if ($_SESSION['id'] == 3) {
        //echo $strSql6;
        //die();
      }
      
      $arrEventList = MySQLStatic::Query($strSql6);

      //print_r($arrEventList); 
      
      foreach ($arrEventList as $intEv => $arrEvent) {
    
        //if ($arrEvent['Name'] == 'E-186305') echo $arrEvent['Name'] .chr(10);

        $strSqlAc = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c`="' .$arrEvent['Betriebsnummer_ZA__c'] .'"';
        //if ($arrEvent['Name'] == 'E-186305') echo $strSqlAc .chr(10);
        $arrAc = MySQLStatic::Query($strSqlAc);

        //PremiumPayer gesuchtem Team zugeordnet
        $strSql7 = 'SELECT * FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrAc[0]['Id'] .'" AND `Information_Provider__c` = "' .$arrKK[0]['Id'] .'" AND `Anfragestelle__c` = "' .$strAnfragestelleId .'"';
        //if ($arrEvent['Name'] == 'E-186305') echo $strSqlAc .chr(10);
        $arrResult7 = MySQLStatic::Query($strSql7);
        
        //if ($arrEvent['Name'] == 'E-186305') echo '(count($arrResult7) == 0): ' .count($arrResult7) .chr(10);

        if (count($arrResult7) == 0) {
          
          $strSql8 = 'SELECT `Id` FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrAc[0]['Id'] .'" AND `Information_Provider__c` = "' .$arrKK[0]['Id'] .'"';
          //if ($arrEvent['Name'] == 'E-186305') echo $strSql8 .chr(10);
          $arrResult8 = MySQLStatic::Query($strSql8);
        
          //if ($arrEvent['Name'] == 'E-186305') echo '(count($arrResult8) == 0): ' .count($arrResult8) .chr(10);
          
          //keinem Team zugeordnet
          if (count($arrResult8) == 0) {
            $arrPayerAccount[] = $arrEvent['Betriebsnummer_ZA__c'];
            //if ($arrEvent['Name'] == 'E-186305') echo $arrEvent['Betriebsnummer_ZA__c'] .chr(10);
          }
          
        }
        
      }

      $strPpAccount = implode(',', $arrPayerAccount);
        
      if ($_SERVER['REMOTE_ADDR'] == '88.217.15.113') {
        // echo '|' .print_r($arrPayerAccount, true) . chr(10) .print_r($arrCatchAllPp, true); die();
      }          



      
    }
  
    $strSql = 'SELECT `evid`, `SF42_IZSEvent__c`.`Id`, `SF42_IZSEvent__c`.`Name`, `SF42_EventStatus__c`, `SF42_Premium_Payer__c`, `Status_Klaerung__c`, ';
    $strSql.= '`Betriebsnummer_IP__c`, `Betriebsnummer_ZA__c`, `SF42_IZSEvent__c`.`SF42_informationProvider__c`, `Account`.`Dezentrale_Anfrage__c` AS `Dezentrale_Anfrage__c`, `Account`.`Name` AS `IpName`, ';
    $strSql.= '`SF42_EventComment__c`, `Art_des_Dokuments__c`, `SF42_PublishingStatus__c`, `Sperrvermerk__c`, `SF42_DocumentUrl__c`, `Acc`.`Id` AS `PpId`, ';
    $strSql.= '`SF42_IZSEvent__c`.`RecordTypeId`, `SF42_EventStatus__c`, `bis_am__c`, `group_id`, `Rueckmeldung_am__c`, `Art_der_Rueckmeldung__c`, `Info__c` ';
    $strSql.= 'FROM `SF42_IZSEvent__c` ';
    $strSql.= 'INNER JOIN `Account` ON `Account`.`Id` = `SF42_IZSEvent__c`.`SF42_informationProvider__c` ';
    $strSql.= 'INNER JOIN `Account` AS `Acc` ON `Acc`.`SF42_Comany_ID__c` = `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c` ';
    $strSql.= 'WHERE 1 ';
    
    if (($_REQUEST['strSelSta'] != '') && ($_REQUEST['strSelSta'] != 'all')) {
      $strSql.= 'AND `SF42_EventStatus__c` = "' .$_REQUEST['strSelSta'] .'" ';
    } else {
      $strSql.= 'AND `SF42_EventStatus__c` IN (' .$strQueryAdd .') ';
    }
    
    if (($_REQUEST['strSelPay'] != 'all') && ($_REQUEST['strSelPay'] != '')) {
      $strSql.= 'AND (`Betriebsnummer_ZA__c` = "' .$_REQUEST['strSelPay'] .'") ';
    }
    
    if (($_REQUEST['strSelInf'] != '') && ($_REQUEST['strSelInf'] != 'all')) {
      $strSql.= 'AND ((`SF42_IZSEvent__c`.`SF42_informationProvider__c` = "' .$arrKK[0]['Id'] .'") OR (`SF42_IZSEvent__c`.`SF42_informationProvider__c` = "' .$_REQUEST['strSelInf'] .'")) ';
    }
    $strSql.= 'AND (`Beitragsmonat__c` = "' .$arrPeriod[0] .'") AND (`Beitragsjahr__c` = "' .$arrPeriod[1] .'") ';
    
    if (isset($_REQUEST['outstanding']) && ($_REQUEST['outstanding'] == 1)) {
      $strSql.= 'AND ((`Rueckmeldung_am__c` = "0000-00-00") || (`Rueckmeldung_am__c` = "")) ';
    }
    if (isset($_REQUEST['Rueckmeldung_am__c']) && ($_REQUEST['Rueckmeldung_am__c'] != '')) {
      $strRueckAm = date('Y-m-d', strtotime($_REQUEST['Rueckmeldung_am__c']));
      $strSql.= 'AND (`Rueckmeldung_am__c` = "' .$strRueckAm .'") ';
    }
    if ($_REQUEST['strSearchValue'] != '') {
      $strSql.= 'AND `Name` LIKE "E-' .$_REQUEST['strSearchValue'] .'%" ';
    }

    $strSql.= 'AND `group_id` != "a083A00000lqnERQAY" ';
    
    /*
    if ($_REQUEST['strSelSta'] == 'enquired') {
      $strSql.= 'AND `RecordTypeId` = "01230000001Ao76AAC" '; //Review -> angefragt
    } elseif ($_REQUEST['strSelSta'] == 'OK') {
      $strSql.= 'AND `RecordTypeId` = "01230000001Ao75AAC" '; //Publication -> OK
    }
    */
    if (($_REQUEST['strSelRec'] != 'all') && ($_REQUEST['strSelRec'] != '')) {
      $strSql.= 'AND `RecordTypeId` = "' .$_REQUEST['strSelRec'] .'" '; 
    }
    
    if (strlen($strPpAccount) > 0) {
      $strSql.= 'AND `Betriebsnummer_ZA__c` IN (' .$strPpAccount .') '; //Betriebsnummer  
    }
    
    if (($_REQUEST['strSelAru'] != 'all') && ($_REQUEST['strSelAru'] != '')) {
      $strSql.= 'AND (`Art_der_Rueckmeldung__c` = "' .$_REQUEST['strSelAru'] .'") ';
    }

    if (($_REQUEST['strSelStu'] != '') && ($_REQUEST['strSelStu'] != 'all')) {
      if ($_REQUEST['strSelStu'] == 'empty') {
        $strSql.= 'AND (`Stundung__c` = "") ';
      } else {
        $strSql.= 'AND (`Stundung__c` = "' .$_REQUEST['strSelStu'] .'") ';
      }
    }
    
    //echo 
    $strSql.= 'ORDER BY `Betriebsnummer_ZA__c` ASC';

    if ($_SERVER['REMOTE_ADDR'] == '2001:a61:3415:cd00:10a5:2721:65dd:b7d1') {
      //echo $strSql;
    }
    
  } else {
    
    if (strstr($_REQUEST['strSelInf'], 'da_') != false) {
      $_REQUEST['strSelInf'] = substr($_REQUEST['strSelInf'], 3);
      //$boolShowIp = true;
    }
  
    $strSql = 'SELECT `evid`, `SF42_IZSEvent__c`.`Id`, `SF42_IZSEvent__c`.`Name`, `SF42_EventStatus__c`, `SF42_Premium_Payer__c`, `Status_Klaerung__c`, ';
    $strSql.= '`Betriebsnummer_IP__c`, `Betriebsnummer_ZA__c`, `SF42_IZSEvent__c`.`SF42_informationProvider__c`, `Account`.`Dezentrale_Anfrage__c` AS `Dezentrale_Anfrage__c`, `Account`.`Name` AS `IpName`, ';
    $strSql.= '`SF42_EventComment__c`, `Art_des_Dokuments__c`, `SF42_PublishingStatus__c`, `Sperrvermerk__c`, `SF42_DocumentUrl__c`, `Acc`.`Id` AS `PpId`, ';
    $strSql.= '`SF42_IZSEvent__c`.`RecordTypeId`, `SF42_EventStatus__c`, `bis_am__c`, `group_id`, `Rueckmeldung_am__c`, `Art_der_Rueckmeldung__c`, `Info__c` ';
    $strSql.= 'FROM `SF42_IZSEvent__c` ';
    $strSql.= 'INNER JOIN `Account` ON `Account`.`Id` = `SF42_IZSEvent__c`.`SF42_informationProvider__c` ';
    $strSql.= 'INNER JOIN `Account` AS `Acc` ON `Acc`.`SF42_Comany_ID__c` = `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c` ';
    $strSql.= 'WHERE 1 ';
    
    if (($_REQUEST['strSelSta'] != '') && ($_REQUEST['strSelSta'] != 'all')) {
      $strSql.= 'AND `SF42_EventStatus__c` = "' .$_REQUEST['strSelSta'] .'" ';
    } else {
      $strSql.= 'AND `SF42_EventStatus__c` IN (' .$strQueryAdd .') ';
    }
    
    if (($_REQUEST['strSelPay'] != 'all') && ($_REQUEST['strSelPay'] != '')) {
      $strSql.= 'AND (`Betriebsnummer_ZA__c` = "' .$_REQUEST['strSelPay'] .'") ';
    }
    
    if (($_REQUEST['strSelInf'] != '') && ($_REQUEST['strSelInf'] != 'all')) {
      $strSql.= 'AND (`SF42_IZSEvent__c`.`SF42_informationProvider__c` = "' .$_REQUEST['strSelInf'] .'") ';
    }
    $strSql.= 'AND (`Beitragsmonat__c` = "' .$arrPeriod[0] .'") AND (`Beitragsjahr__c` = "' .$arrPeriod[1] .'") ';

    $strSql.= 'AND `group_id` != "a083A00000lqnERQAY" ';
    
    /*
    if ($_REQUEST['strSelSta'] == 'enquired') {
      $strSql.= 'AND `RecordTypeId` = "01230000001Ao76AAC" '; //Review -> angefragt
    } elseif ($_REQUEST['strSelSta'] == 'OK') {
      $strSql.= 'AND `RecordTypeId` = "01230000001Ao75AAC" '; //Publication -> OK
    }
    */
    
    if (($_REQUEST['strSelGru'] != 'all') && ($_REQUEST['strSelGru'] != '')) {
      $strSql.= 'AND (`group_id` = "' .$_REQUEST['strSelGru'] .'") ';
    }
    
    if ($_REQUEST['strSearchValue'] != '') {
      $strSql.= 'AND `SF42_IZSEvent__c`.`Name` LIKE "E-' .$_REQUEST['strSearchValue'] .'%" ';
    }
    
    if (($_REQUEST['strSelRec'] != 'all') && ($_REQUEST['strSelRec'] != '')) {
      $strSql.= 'AND `SF42_IZSEvent__c`.`RecordTypeId` = "' .$_REQUEST['strSelRec'] .'" '; 
    }
    
    if (isset($_REQUEST['outstanding']) && ($_REQUEST['outstanding'] == 1)) {
      $strSql.= 'AND ((`Rueckmeldung_am__c` = "0000-00-00") || (`Rueckmeldung_am__c` = "")) ';
    }
    
    if (isset($_REQUEST['Rueckmeldung_am__c']) && ($_REQUEST['Rueckmeldung_am__c'] != '')) {
      $strRueckAm = date('Y-m-d', strtotime($_REQUEST['Rueckmeldung_am__c']));
      $strSql.= 'AND (`Rueckmeldung_am__c` = "' .$strRueckAm .'") ';
    }

    if (($_REQUEST['strSelAru'] != 'all') && ($_REQUEST['strSelAru'] != '')) {
      $strSql.= 'AND (`Art_der_Rueckmeldung__c` = "' .$_REQUEST['strSelAru'] .'") ';
    }

    if (($_REQUEST['strSelStu'] != '') && ($_REQUEST['strSelStu'] != 'all')) {
      if ($_REQUEST['strSelStu'] == 'empty') {
        $strSql.= 'AND (`Stundung__c` = "") ';
      } else {
        $strSql.= 'AND (`Stundung__c` = "' .$_REQUEST['strSelStu'] .'") ';
      }
    }

    //echo 
    $strSql.= 'ORDER BY `Betriebsnummer_ZA__c` ASC';
    
  }

  if ($_COOKIE['id'] == '3') {
    //echo $strSql;
    //die();
  }
  
  $arrResult = MySQLStatic::Query($strSql);

  $arrEscalation = [];
  $strSql = 'SELECT * FROM `Group__c` WHERE `Eskalation__c` = "ja"';
  $arrSql = MySQLStatic::Query($strSql);
  if (count($arrSql) > 0) {
    foreach ($arrSql as $intKey => $arrDataset) {
      $arrEscalation[$arrDataset['Id']] = $arrDataset;
    }
  }

  $start = microtime(true);

  $arrDezAnfList = [];
  $strSql4 = 'SELECT `Anfragestelle__c`, `Premium_Payer__c`, `Information_Provider__c` FROM `Dezentrale_Anfrage_KK__c`'; // WHERE `Premium_Payer__c` = "' .$arrEvent['PpId'] .'" AND `Information_Provider__c` = "' .$arrEvent['SF42_informationProvider__c'] .'"';
  $arrRes4 = MySQLStatic::Query($strSql4);
  if (count($arrRes4) > 0) {
    foreach ($arrRes4 as $intKey => $arrDezAnf) {
      $strKey = $arrDezAnf['Premium_Payer__c'] .'_' .$arrDezAnf['Information_Provider__c'];
      unset($arrDezAnf['Premium_Payer__c']);
      unset($arrDezAnf['Information_Provider__c']);
      $arrDezAnfList[$strKey] = [0 => $arrDezAnf];
    }
  }

  if ($_COOKIE['id'] == '3') {
    //print_r($arrDezAnfList);
    //die();
  }

  $arrAnfragestelleList = [];
  $arrAnfragestelleCatchAllList = [];
  $strSql5 = 'SELECT `Id`, `Name`, `CATCH_ALL__c`, `Information_Provider__c` FROM `Anfragestelle__c`'; // WHERE `Id` = "' .$arrResult4[0]['Anfragestelle__c'] .'"
  $arrResult5 = MySQLStatic::Query($strSql5);
  if (count($arrResult5) > 0) {
    foreach ($arrResult5 as $intKey => $arrAnf) {

      if ($arrAnf['CATCH_ALL__c'] == 'true') {
        unset($arrAnf['CATCH_ALL__c']);
        $strKey = $arrAnf['Information_Provider__c'];
        unset($arrAnf['Information_Provider__c']);
        $arrAnfragestelleCatchAllList[$strKey] = [0 => $arrAnf];
      }

      unset($arrAnf['CATCH_ALL__c']);
      unset($arrAnf['Information_Provider__c']);
      $arrAnfragestelleList[$arrAnf['Id']] = [0 => $arrAnf];
    }
  }

  //$strSql5b = 'SELECT `Id`, `Name` FROM `Anfragestelle__c` WHERE `CATCH_ALL__c` = "true"'; // `Information_Provider__c` = "' .$arrEvent['SF42_informationProvider__c'] .'" AND
  //$arrResult5b = MySQLStatic::Query($strSql5b);



//print_r($arrEscalation); die();

  if (count($arrResult) > 0) {
    
    foreach ($arrResult as $intKey => $arrEvent) {
      
      $boolSecond = false;
      
      if ($arrEvent['Dezentrale_Anfrage__c'] == 'true') { //DEZENTRAL
        
        if (false) {
          $strSql4 = 'SELECT `Anfragestelle__c`  FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrEvent['PpId'] .'" AND `Information_Provider__c` = "' .$arrEvent['SF42_informationProvider__c'] .'"';
          $arrResult4 = MySQLStatic::Query($strSql4);  
        } else {

          //$strSql4 = 'SELECT `Anfragestelle__c`  FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrEvent['PpId'] .'" AND `Information_Provider__c` = "' .$arrEvent['SF42_informationProvider__c'] .'"';
          //$arrResult4 = MySQLStatic::Query($strSql4);  

          $strKey = $arrEvent['PpId'] .'_' .$arrEvent['SF42_informationProvider__c'];
          $arrResult4 = $arrDezAnfList[$strKey] ?? [];

          //if ($arrResult4n != $arrResult4) {
            //print_r($arrResult4);
            //print_r($arrResult4n);
            //die();
          //}

        }

        if ($_SERVER['HTTP_FKLD'] == 'on') {
          //echo $strSql4;
        }
        //echo count($arrResult4);

        if (count($arrResult4) > 0) {
        
          //$strSql5 = 'SELECT `Id`, `Name` FROM `Anfragestelle__c` WHERE `Id` = "' .$arrResult4[0]['Anfragestelle__c'] .'"';
          //$arrResult5 = MySQLStatic::Query($strSql5);

          $arrResult5 = $arrAnfragestelleList[$arrResult4[0]['Anfragestelle__c']] ?? [];

          $arrResult[$intKey]['AnfrageId'] = $arrResult5[0]['Id'];
          $arrResult[$intKey]['AnfrageName'] = $arrResult5[0]['Name'];
          $arrResult[$intKey]['AnfrageType'] = 'd';
        
        } else { //CATCH ALL
        
          //$strSql5b = 'SELECT `Id`, `Name` FROM `Anfragestelle__c` WHERE `Information_Provider__c` = "' .$arrEvent['SF42_informationProvider__c'] .'" AND `CATCH_ALL__c` = "true"';
          //$arrResult5b = MySQLStatic::Query($strSql5b);

          $strKey = $arrEvent['SF42_informationProvider__c'];
          $arrResult5b = $arrAnfragestelleCatchAllList[$strKey];
          
          if ($_SERVER['HTTP_FKLD'] == 'on') {
            //echo $strSql5b;
          }

          $arrResult[$intKey]['AnfrageId'] = $arrResult5b[0]['Id'];
          $arrResult[$intKey]['AnfrageName'] = $arrResult5b[0]['Name'];
          $arrResult[$intKey]['AnfrageType'] = 'c';
          
        }
      
      } else { // ZENTRAL
        
        $arrResult[$intKey]['AnfrageId'] = $arrEvent['SF42_informationProvider__c'];
        $arrResult[$intKey]['AnfrageName'] = $arrEvent['IpName'];
        $arrResult[$intKey]['AnfrageType'] = 'z';

      }
      
      //$arrEventList[] = $arrEvent;
      
    } 
  
  }

  $end = microtime(true);

  $durationInMs = ($end-$start) * 1000;
  echo '<!-- Abchnitt 01: ' .round($durationInMs) .'ms -->' .chr(10);


  $arrInformationProvider = array();
  
  if (count($arrResult) > 0) {
    
    $arrInformationProvider = $arrResult;
  
  }


  //echo count($arrResult);
  if ($_SERVER['HTTP_FKLD'] == 'on') {
    //print_r($arrInformationProvider); die();
  }
  //print_r($arrDecentral);

  if ($_SERVER['HTTP_FKLD'] == 'on') {
    //echo count($arrInformationProvider); 
  }

  $strExportTable = '';
  
  if (count($arrInformationProvider) > 0) {
    //natcasesort($arrInformationProvider);

    if ($_SERVER['HTTP_FKLD'] == 'on') {
      //echo count($arrInformationProvider); 
    }

    $strOutputTable = '';

    if (@$_SERVER['HTTP_FKLD'] == 'on') {
      $strOutputTable.= '<table class="" id="5">' .chr(10);
    } else {
      $strOutputTable.= '<table class="" id="5">' .chr(10);
    }
        
    $strOutputTable.= '  <thead>' .chr(10);
    $strOutputTable.= '    <tr>' .chr(10);
    $strOutputTable.= '      <th class=""><input type="checkbox" value="0" name="check_all_r" id="check_all_r"></th>' .chr(10); //class="header"
    
  	$strOutputTable.= '      <th class="">Status</th>' .chr(10); //class="header"

    $strOutputTable.= '      <th class="">Esc</th>' .chr(10); //class="header"

    $strOutputTable.= '      <th class="">Negativm.</th>' .chr(10); //class="header"
  	
    $strOutputTable.= '      <th class="">Event-ID</th>' .chr(10); //class="header"
    //$strOutputTable.= '      <th class="">Information Provider</th>' .chr(10);
  	$strOutputTable.= '      <th class="">Premium Payer</th>' .chr(10); //class="header"

    $strOutputTable.= '      <th class="">Anfragestelle</th>' .chr(10); //class="header"

  	$strOutputTable.= '      <th class="">BNR PP</th>' .chr(10); //class="header"
  	//$strOutputTable.= '      <th class="">Company Group</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Rückmeldung am</th>' .chr(10); //class="header"
    //$strOutputTable.= '      <th class="">Art d. Rückm.</th>' .chr(10); //class="header"
    //$strOutputTable.= '      <th class="">Art des Dokuments</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="">Erfasst von</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="">Klärung</th>' .chr(10); //class="header"

    $strOutputTable.= '    </tr>' .chr(10);
    $strOutputTable.= '  </thead>' .chr(10);
    $strOutputTable.= '  <tbody>' .chr(10);
    
    $strExportTable.= $strOutputTable;
    
    $arrPeriod = explode('_', $_REQUEST['strSelDate']);
    
    $strNext = '';

    $arrEventStatus = array(
      0 => 0,
      1 => 0
    );

    if ($_SERVER['HTTP_FKLD'] == 'on') {
      //print_r($arrInformationProvider); 
    }

    $arrEventKlaerung = array(
      0 => 0,
      1 => 0,
      2 => 0
    );

    $start = microtime(true);

    $arrEventList = [];
    foreach ($arrInformationProvider as $strId => $arrEvent) {
      $arrEventList[] = $arrEvent['Id'];
    }

    if (true) { // $_SESSION['id'] == 3

      $strEventList = '';
      $arrChangeList = [];
      if (count($arrEventList) > 0) {
        $strEventSql = 'SELECT * FROM `izs_event_change` WHERE `ec_fild` = "Rueckmeldung_am__c" AND `ev_id` IN ("' .implode('", "', $arrEventList) .'")';
        $arrEventSql = MySQLStatic::Query($strEventSql);
        if (count($arrEventSql) > 0) {
          $arrDone = [];
          foreach ($arrEventSql as $intKey => $arrChange) {
            if (($arrChange['ec_new'] != '') && ($arrChange['ec_new'] != '0000-00-00')) {
              if (!in_array($arrChange['ev_id'], $arrDone)) { 
                $arrChangeList[$arrChange['ev_id']] = [0 => $arrChange];
                $arrDone[] = $arrChange['ev_id'];
              }
            } 
          }
        }
      }
      //echo count($arrChangeList); die();
      //print_r($arrChangeList); die();
    }

    $arrGroupList = [];
    $strSqlGr = 'SELECT `Name` FROM `Group__c`'; //WHERE `Id` = "' .$arrEvent['group_id'] .'"
    $arrSqlGr = MySQLStatic::Query($strSqlGr);
    if (count($arrSqlGr) > 0) {
      foreach ($arrSqlGr as $intKey => $arrGroupRaw) {
        $arrGroupList[$arrGroupRaw['Id']] = [0 => $arrGroupRaw];
      }
    }
  
    foreach ($arrInformationProvider as $strId => $arrEvent) {

      //$strSqlE = 'SELECT *  FROM `izs_event_change` WHERE `ev_id` = "' .$arrEvent['Id'] .'" AND `ec_fild` = "Rueckmeldung_am__c" AND ((`ec_new` != "") AND (`ec_new` != "0000-00-00")) ORDER BY `ec_id` DESC LIMIT 1';
      //$arrSqlE = MySQLStatic::Query($strSqlE);

      $arrSqlE = $arrChangeList[$arrEvent['Id']] ?? [];

      if (($_REQUEST['strSelEvo'] != 'all') && ($_REQUEST['strSelEvo'] != '')) {

          if ($arrSqlE[0]['cl_id'] != $_REQUEST['strSelEvo']) continue;

      }
      
      if (@$_SERVER['HTTP_FKLD'] == 'on') {
        //echo $strId .': ' .$arrInformationProvider[10]['Id'] .chr(10);
      }

      if ((($arrEvent['SF42_EventStatus__c'] == 'enquired') && (($arrEvent['Rueckmeldung_am__c'] == '0000-00-00') || ($arrEvent['Rueckmeldung_am__c'] == '')))) {
        $arrEventStatus[0]++;
      } else {
        $arrEventStatus[1]++;
      }

      $strTrClass = '';
      if ($arrEvent['Status_Klaerung__c'] == 'in Klärung') {
        $strTrClass = 'bgred';
        $arrEventKlaerung[0]++;
      } elseif ($arrEvent['Status_Klaerung__c'] == 'geschlossen / positiv') {
        $strTrClass = 'bggreen';
        $arrEventKlaerung[1]++;
      } else {
        $strTrClass = '';
      }

      if ($arrEvent['Status_Klaerung__c'] == 'geschlossen / negativ') {
        $arrEventKlaerung[2]++;
      }
    
      if ($boolMore && ($arrEvent['SF42_PublishingStatus__c'] == 'private')) {
        //$strTrClass = 'bgred';
      }
      
      if ($strNext == 'blubb') {
        $strNext = $arrEvent['Id'];
      }
      
      if (($_REQUEST['done'] != '') && ($_REQUEST['done'] == $arrEvent['Id'])) {
        $strNext = 'blubb';
      }
      
      $strOutputTable.= '    <tr class="' .$strTrClass .'" id="e_' .$arrEvent['Id'] .'">' .chr(10);
      $strExportTable.= '    <tr>' .chr(10);

      $strOutputTable.= '      <td><input type="checkbox" name="rueck[' .$arrEvent['evid'] .']" value="1" class="rueck" rel="' .$arrEvent['evid'] .'" id="check_' .$arrEvent['evid'] .'"></span></td>' .chr(10);
      $strExportTable.= '      <td></td>' .chr(10);

      //print_r($arrEvent); die();
      // Meldestelle: $arrEvent['PpId'] => 0013A00001QTCL3QAP
      // group_id: $arrEvent['group_id']

      /*
      $arrEscRequest[$arrEscRaw['id']]
      $arrNvmRequest
      */

      $strOutputTable.= '      <td align="center"><img src="/assets/images/sys/status_' .$arrStatusCss[$arrEvent['SF42_EventStatus__c']] .'.png" /> ' .$arrEvent['SF42_EventStatus__c'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['SF42_EventStatus__c'] .'</td>' .chr(10);

      $strKey = $arrEvent['PpId'] .'_' .$arrEvent['SF42_informationProvider__c'];

      $strGroupId = $arrEvent['group_id'];

      if (isset($arrEscalation[$strGroupId])) { // 'a083000000D4hUMAAZ' https://izs.smartgroups.io/frontend/main/topic/topic-97f48d16-eb79-48c7-b450-6085ab217700

        preg_match('/topic-[\w-]*/', $arrEscalation[$strGroupId]['Eskalation_Link__c'], $arrLink);
        $strSgLink = $arrLink[0] ?? '';

        $strEsc = '<a href="javascript:void(0)" alt="' .$arrEscalation[$strGroupId]['Eskalation_Grund__c'] .'" title="' .$arrEscalation[$strGroupId]['Eskalation_Grund__c'] .'" onclick="openSG(\'' .$strSgLink .'\')" style="color:red;"><i class="icon fa fa-warning" aria-hidden="true"></i></a>';
        $strOutputTable.= '      <td>' .$strEsc .'</td>' .chr(10);
        $strExportTable.= '      <td></td>' .chr(10);
  
      } else {
        $strOutputTable.= '      <td></td>' .chr(10);
        $strExportTable.= '      <td></td>' .chr(10);
      }


      if (isset($arrNvmRequestOver[$strKey])) {

        //die('YES!');
        
        $strNvmAdd = '';
        $strNvmAddText = '';
        foreach ($arrNvmRequestOver[$strKey] as $strKey => $strNvmId) { // https://crm.izs-institut.de/nem/a0k3000000zh9RoAAI/stammdaten oder $arrNvmRequest[$strNvmId]['chat']
            //echo $strNvmId; 
            $strNvmAdd.= '<a href="https://crm.izs-institut.de/nem/' .$strNvmId .'/stammdaten" alt="' .$arrNvmRequest[$strNvmId]['ntype'] .'" target="_blank"><span class="label bg-color-orange"><i class="fa fa-dot-circle-o"></i></span></a> ';
            $strNvmAddText.= $arrNvmRequest[$strNvmId]['ntype'] .', ';
        }
        $strNvmAddText = substr($strNvmAddText, 0, -2);

        $strOutputTable.= '      <td>' .$strNvmAdd .'</td>' .chr(10);
        $strExportTable.= '      <td>' .$strNvmAddText .'</td>' .chr(10);

      } else {
        
        $strOutputTable.= '      <td></td>' .chr(10);
        $strExportTable.= '      <td></td>' .chr(10);
      
      }


      $strOutputTable.= '      <td><a target="_blank" href="/cms/index.php?ac=sear&det=' .$arrEvent['evid'] .'">' .$arrEvent['Name'] .'</a></td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['Name'] .'</td>' .chr(10);

      
      //$strSql = 'SELECT `Name`, `SF42_Comany_ID__c`, `SF42_Company_Group__c` FROM `Account` WHERE `Id` = "' .$arrEvent['SF42_informationProvider__c'] .'"';
      //$arrSql = MySQLStatic::Query($strSql);

      //$strSqlGr = 'SELECT `Name` FROM `Group__c` WHERE `Id` = "' .$arrEvent['group_id'] .'"';
      //$arrSqlGr = MySQLStatic::Query($strSqlGr);

      $arrSqlGr = $arrGroupList[$arrEvent['group_id']] ?? [];

      //$strOutputTable.= '      <td><span class="hidden">' .$arrSql[0]['Name'] .'</span><a href="index_neu.php?ac=contacts&acid=' .$arrEvent['SF42_informationProvider__c'] .'" title="Zeige Kontakte" target="_blank">' .$arrSql[0]['Name'] .'</a></td>' .chr(10);
      //$strExportTable.= '      <td>' .$arrSql[0]['Name'] .'</td>' .chr(10);
      //$strOutputTable.= '      <td>' .$arrSql[0]['Name'] .'</td>' .chr(10);

      $strOutputTable.= '      <td><span class="hidden">' .$arrEvent['SF42_Premium_Payer__c'] .'</span><a href="https://crm.izs-institut.de/ver/' .$arrEvent['group_id'] .'/smartgroups" target="_blank">' .$arrEvent['SF42_Premium_Payer__c'] .'</a></td>' .chr(10);
      $strOutputTable.= '      <td><span class="hidden">' .$arrEvent['AnfrageName'] .'</span><a href="https://crm.izs-institut.de/kk/' .$arrEvent['SF42_informationProvider__c'] .'/smartgroups" target="_blank">' .$arrEvent['AnfrageName'] .'</a></td>' .chr(10);
      $strOutputTable.= '      <td>' .$arrEvent['Betriebsnummer_ZA__c'] .'</td>' .chr(10);
      //$strOutputTable.= '      <td><span class="hidden">' .$arrSqlGr[0]['Name'] .'</span><a href="index_neu.php?ac=contacts&grid=' .$arrEvent['group_id'] .'" title="Zeige Kontakte" target="_blank">' .$arrSqlGr[0]['Name'] .'</a></td>' .chr(10);
      //$strOutputTable.= '      <td>' .$arrSqlGr[0]['Name'] .'</td>' .chr(10);

      $strExportTable.= '      <td>' .$arrEvent['SF42_Premium_Payer__c'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['AnfrageName'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['Betriebsnummer_ZA__c'] .'</td>' .chr(10);
      //$strExportTable.= '      <td>' .$arrSqlGr[0]['Name'] .'</td>' .chr(10);
      
      if ($arrEvent['Rueckmeldung_am__c'] != '0000-00-00') {
        $arrDate = explode('-', $arrEvent['Rueckmeldung_am__c']);
        $arrEvent['Rueckmeldung_am__c'] = $arrDate[2] .'.' .$arrDate[1] .'.' .$arrDate[0];
      } else {
        $arrEvent['Rueckmeldung_am__c'] = '';
      }

      $strOutputTable.= '      <td><span id="h_date_' .$arrEvent['evid'] .'" class="hidden">' .$arrEvent['Rueckmeldung_am__c'] .'</span><input type="text" id="date_' .$arrEvent['evid'] .'" class="date update nored" name="Rueckmeldung_am__c" value="' .$arrEvent['Rueckmeldung_am__c'] .'" autocomplete="off"></td>' .chr(10);
      
      /*
      $strOutputTable.= '      <td><span id="h_artr_' .$arrEvent['evid'] .'" class="hidden">' .$arrEvent['Art_der_Rueckmeldung__c'] .'</span><select name="Art_der_Rueckmeldung__c" id="artr_' .$arrEvent['evid'] .'" class="update">' .chr(10);
      $strOutputTable.= '			      <option value=""></option>';
      foreach ($arrRueck as $strKey => $strName) {
        if ($arrEvent['Art_der_Rueckmeldung__c'] == $strKey) {
          $strSel = ' selected="selected"';
        } else {
          $strSel = '';
        }
        $strOutputTable.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
      }
      $strOutputTable.= '
			      </select></td>' .chr(10);
      */

      $strExportTable.= '      <td>' .$arrEvent['Rueckmeldung_am__c'] .'</td>' .chr(10);
      //$strExportTable.= '      <td>' .$arrEvent['Art_der_Rueckmeldung__c'] .'</td>' .chr(10);
			
      /*
      $strOutputTable.= '      <td><span id="h_artd_' .$arrEvent['evid'] .'" class="hidden">' .$arrEvent['Art_des_Dokuments__c'] .'</span><select name="Art_des_Dokuments__c" id="artd_' .$arrEvent['evid'] .'" class="update">' .chr(10);
      $strOutputTable.= '			      <option value=""></option>';
      foreach ($arrDokArt as $strKey => $strName) {
        if ($arrEvent['Art_des_Dokuments__c'] == $strKey) {
          $strSel = ' selected="selected"';
        } else {
          $strSel = '';
        }
        $strOutputTable.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strKey .'</option>';
      }
      $strOutputTable.= '
			      </select></td>' .chr(10);

      $strExportTable.= '      <td>' .$arrEvent['Art_des_Dokuments__c'] .'</td>' .chr(10);
      */

      $strErfasst = '';

      if (count($arrSqlE) > 0) {
        $strErfasst = $arrUserList[$arrSqlE[0]['cl_id']];
      }

      $strOutputTable.= '      <td>' .$strErfasst .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$strErfasst .'</td>' .chr(10);

      $strOutputTable.= '      <td>' .$arrEvent['Status_Klaerung__c'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['Status_Klaerung__c'] .'</td>' .chr(10);
      
      $strOutputTable.= '    </tr>' .chr(10);
      $strExportTable.= '    </tr>' .chr(10);
      
    }

    $end = microtime(true);

    $durationInMs = ($end-$start) * 1000;
    echo '<!-- Abchnitt 02: ' .round($durationInMs) .'ms -->' .chr(10);

    //print_r($arrEventStatus);
      
    $strOutputTable.= '  </tbody>' .chr(10);
    $strOutputTable.= '</table>' .chr(10);
    
    $strExportTable.= '  </tbody>' .chr(10);
    $strExportTable.= '</table>' .chr(10);

    $strTitle = '<h3>' .($arrEventStatus[1] + $arrEventStatus[0]) .' Ergebnisse gefunden ';
    $strTitle.= '(erhalten: ' .$arrEventStatus[1] .' / ausstehend: ' .$arrEventStatus[0] .')';

    $intAnzahlKlaerung = ($arrEventKlaerung[0] + $arrEventKlaerung[1] + $arrEventKlaerung[2]);

    $strTitle.= '</h3><h4>' .$intAnzahlKlaerung .' Klärungsfälle ';
    if ($intAnzahlKlaerung > 0) {
      $strTitle.= '(in Klärung: ' .$arrEventKlaerung[0] .' / geschlossen positiv: ' .$arrEventKlaerung[1] .' / geschlossen negativ: ' .$arrEventKlaerung[2] .')';
    }
    $strTitle.= '<h4>';

    $strOutput.= '<div class="clearfix" style="height: 75px; width: 1660px;">' .chr(10);
    $strOutput.= '  <div class="form-left">' .chr(10);
    $strOutput.= '    ' .$strTitle .'' .chr(10);
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' .chr(10);
    $strOutput.= '    <button id="start-editOk" class="ui-button ui-state-default ui-corner-all green">OK erfassen</button>&nbsp;' .chr(10);
    $strOutput.= '    <!-- <button id="start-edit" class="ui-button ui-state-default ui-corner-all">Massenfunktion</button>&nbsp; -->' .chr(10);
    $strOutput.= '    <button id="start-editK" class="ui-button ui-state-default ui-corner-all">Klärungsfall</button>&nbsp;' .chr(10);
    $strOutput.= '    <button id="start-editKK" class="ui-button ui-state-default ui-corner-all gray">Kein Konto</button>&nbsp;' .chr(10);
    $strOutput.= '    <button id="start-editKG" class="ui-button ui-state-default ui-corner-all gray">Konto geschlossen</button>&nbsp;' .chr(10);
    $strOutput.= '    <form method="post" action="_get_csv.php" style="display: inline;">';
    $strOutput.= '    <input type="hidden" name="strTable" value="' .str_replace('"', "'", $strExportTable) .'">' .chr(10);
    $strOutput.= '    <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' .chr(10);
    $strOutput.= '    </form>';
    $strOutput.= '    <button id="start-note" class="ui-button ui-state-default ui-corner-all">Massen-Notizen</button>&nbsp;' .chr(10);
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '</div>' .chr(10);

    $strOutput.= $strOutputTable;

    //$strOutput.= '</form>' .chr(10);


$strOutput.= '
<div id="dialog_edit" title="Daten für Massenfunktion" class="clearfix">
	<p id="validateTipsE" class="validateTips"></p>
	
	<p>Bitte stelle die Daten, die aktualisiert werden sollen:</p>

	<form>
	  <fieldset class="clearfix">

<div class="clearfix">
  <label for="Rueckmeldung_am__c">Rückmeldung am</label>
  <input type="text" id="date_all" class="date" name="Rueckmeldung_am__c" autocomplete="off">
</div>

</fieldset>

	</form>

</div>';

$strOutput.= '
<div id="dialog_editKG" title="Ausgewählte Events auf \'Konto geschlossen\' setzen" class="clearfix">
	
	<p>Alle ausgewählten Datensätzen werden auf "Konto geschlossen" gestellt.<br>
  Bitte bestätige dies abschließend.</p>

  <form>

  <div class="clearfix">
    <label for="Rueckmeldung_am__c">Rückmeldung am</label>
    <input type="text" id="date_allKG" class="date" name="Rueckmeldung_am__c" autocomplete="off">
  </div>

	</form>

</div>';

$strOutput.= '
<div id="dialog_editKK" title="Ausgewählte Events auf \'Kein Konto\' setzen" class="clearfix">
	
	<p>Alle ausgewählten Datensätzen werden auf "Kein Konto" gestellt.<br>
  Bitte bestätige dies abschließend.</p>

  <form>

  <div class="clearfix">
    <label for="Rueckmeldung_am__c">Rückmeldung am</label>
    <input type="text" id="date_allKK" class="date" name="Rueckmeldung_am__c" autocomplete="off">
  </div>

	</form>

</div>';


$strOutput.= '
<div id="dialog_editOk" title="Ausgewählte Events auf OK setzen" class="clearfix">
	<p id="validateTipsOK" class="validateTips red"></p>
	
	<p>Bitte stelle die Daten, die aktualisiert werden sollen:</p>

	<form>
  <input name="Art_des_Dokuments__c" id="artd_all_ok" class="" value="Dynamische Erzeugung" type="hidden" />

	  <fieldset class="clearfix">

<div class="clearfix">
  <label for="Rueckmeldung_am__c" style="width: 155px; display: inline-block; height: 25px;">Rückmeldung am</label>
  <input type="text" id="date_all_ok" class="date" name="Rueckmeldung_am__c" autocomplete="off">
</div>

<div class="clearfix">
<label for="SF42_OnlineStatus__c" style="width: 155px; display: inline-block; height: 25px;">Ergebnis</label><span style="color: #67d933; font-weight: bold;"> OK</span>
<input name="SF42_OnlineStatus__c" id="status_all" class="" value="OK" type="hidden" />
</div>

<div class="clearfix">
  <label for="Stundung__c" style="width: 155px; display: inline-block; height: 25px;">Stundung / Ratenzahlung</label>
  <select name="Stundung__c" id="stundung_all" class="">' .chr(10);
      $strOutput.= '			      <option value="<leer>">Keine Änderung</option>';
      $strOutput.= '			      <option value="">Bestehende Daten löschen</option>';
      foreach ($arrStundung as $strKey => $strName) {
        $strOutput.= '			        <option value="' .$strKey .'">' .$strName .'</option>';
      }
      $strOutput.= '
			      </select>
</div>

</fieldset>

	</form>

</div>';


/*

Ansprechpartner
Telefon Klärung

*/

$strOutput.= '
<div id="dialog_editK" title="Daten für Klärungsfall" class="clearfix">
	<p id="validateTipsK" class="validateTips"></p>
	
	<p>Bitte stelle die Daten, die aktualisiert werden sollen:</p>

	<form>
    
    <input type="hidden" id="art_doc" name="art_doc" value="Dynamische Erzeugung">

	  <fieldset class="clearfix">

<div class="clearfix">
  <label for="Rueckmeldung_am__c" class="fwidth" style="width: 155px; display: inline-block; height: 25px;">Rückmeldung am</label>
  <input type="text" id="date3_all" class="date" name="Rueckmeldung_am__c" autocomplete="off" style="display: inline; margin: 5px 0px;">
  &nbsp;<img src="img/f_delete.png" id="datedel3" />
</div>

<div class="clearfix">
<label for="Status_Klaerung__c" class="fwidth" style="width: 155px; display: inline-block; height: 25px;">Status</label>
<select name="Status_Klaerung__c" id="arts_all" class="">' .chr(10);
      $strOutput.= '			      <option value="<leer>">Keine Änderung</option>';
      $strOutput.= '			      <option value="">Bestehende Daten löschen</option>';
      foreach ($arrStatusKlaerung as $strKey => $strName) {
        $strOutput.= '			        <option value="' .$strKey .'">' .$strKey .'</option>';
      }
      $strOutput.= '
			      </select>
</div>
<div class="clearfix">
  <label for="Grund__c" class="fwidth" style="width: 155px; display: inline-block; height: 25px;">Grund</label>
  <select name="Grund__c" id="artr2_all" class="">' .chr(10);
      $strOutput.= '			      <option value="<leer>">Keine Änderung</option>';
      $strOutput.= '			      <option value="">Bestehende Daten löschen</option>';
      foreach ($arrGrund as $strKey => $strName) {
        $strOutput.= '			        <option value="' .$strKey .'">' .$strName .'</option>';
      }
      $strOutput.= '
			      </select>
</div>
<div class="clearfix">
<label for="Naechster_Meilenstein__c" class="fwidth" style="width: 155px; display: inline-block; height: 25px;">Nächster Schritt</label>
<select name="Naechster_Meilenstein__c" id="artd2_all" class="">' .chr(10);
      $strOutput.= '			      <option value="<leer>">Keine Änderung</option>';
      $strOutput.= '			      <option value="">Bestehende Daten löschen</option>';
      foreach ($arrMeilenstein as $strKey => $strName) {
        $strOutput.= '			        <option value="' .$strKey .'">' .$strKey .'</option>';
      }
      $strOutput.= '
			      </select>
</div>
<div class="clearfix">
  <label for="AP_Klaerung" class="fwidth" style="width: 155px; display: inline-block; height: 25px;">Ansprechpartner</label>
  <input type="text" id="ap_all" name="AP_Klaerung">
  &nbsp;<img src="img/f_delete.png" id="apdel" />
</div>
<div class="clearfix">
  <label for="Tel_Klaerung" class="fwidth" style="width: 155px; display: inline-block; height: 25px;">Telefon</label>
  <input type="text" id="tel_all" name="Tel_Klaerung">
  &nbsp;<img src="img/f_delete.png" id="teldel" />
</div>
<div class="clearfix">
  <label for="bis_am__c" class="fwidth" style="width: 155px; display: inline-block; height: 25px;">Wiedervorlage</label>
  <input type="text" id="date2_all" class="date" name="bis_am__c" autocomplete="off" style="display: inline;">
  &nbsp;<img src="img/f_delete.png" id="datedel" />
</div>

<div class="clearfix">
  <label for="Notizen" class="fwidth" style="width: 155px; display: inline-block; height: 25px;">Status-Info</label>
  <textarea id="in_all" name="Notizen" style="width: 155px; height: 70px;"></textarea>
  &nbsp;<img src="img/f_delete.png" style="margin-bottom: 3px;" id="indel" />
</div>

<div class="clearfix" style=" margin-top: 3px;">
<label for="Stundung__c" class="fwidth" style="width: 155px; display: inline-block; height: 25px;">Stundung / Ratenzahlung</label>
<select name="Stundung__c" id="stundung_all2" class="">' .chr(10);
      $strOutput.= '			      <option value="<leer>">Keine Änderung</option>';
      $strOutput.= '			      <option value="">Bestehende Daten löschen</option>';
      foreach ($arrStundung as $strKey => $strName) {
        $strOutput.= '			        <option value="' .$strKey .'">' .$strKey .'</option>';
      }
      $strOutput.= '
			      </select>
</div>

</fieldset>

	</form>

</div>

<div id="dialog_massnote" title="Daten für Massen-Notizen" class="clearfix">
<p id="validateTipsN" class="validateTips"></p>
	
<p>Bitte gib die Notiz ein:</p>

<form>
  <fieldset class="clearfix">

<div class="clearfix">
<label for="Notizen">Massen-Notiz</label>
<textarea id="in_allmass" name="Notizen" style="width: 222px; height: 200px;"></textarea>
<!-- &nbsp;<img src="img/f_delete.png" style="margin-bottom: 3px;" id="allmassdel" /> -->
</div>

</fieldset>

</form>

</div>


<script>
  $(document).ready(function() {

    function openSG(topicId) {
      if (topicId != "") {
        setTimeout(()=>smartchat.cmd("topic.show", {entity: {id: topicId}}), 150);
      }
    }

		var message = daa = $("#date2_all"), ads = $("#arts_all"), adr = $("#artr2_all"), add = $("#artd2_all"), ada = $("#ap_all"), adt = $("#tel_all"), 
			allFields = $([]).add(daa).add(ads).add(adr).add(add).add(ada).add(adt),
			tipsK = $("#validateTipsK");
      tipsN = $("#validateTipsN");

		function updateTipsK(t) {
			tipsK.text(t).effect("highlight",{},1500);
		}
		
    function handle() {
      var strValues = "";
      $(".rueck").each (function() {
        if($(this).is(":checked")) {
          strValues+= "|" + $(this).attr(\'rel\');
        }
      });
      strValues = strValues.substr(1);
      return strValues;
    }


		$(\'#start-note\').click(function() {
		  var echeck = handle();
		  if (echeck == "") {
			  $(\'#dialog_notice\').dialog(\'open\');
			  
			  $("#dialog_notice ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');
			  
			} else {

			  $("#in_allmass").val("");
			  $(\'#dialog_massnote\').dialog(\'open\');
			  
			  $("#dialog_massnote ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');
			  
			}
		})
		.hover(
			function(){ 
				$(this).addClass("ui-state-hover"); 
			},
			function(){ 
				$(this).removeClass("ui-state-hover"); 
			}
		).mousedown(function(){
			$(this).addClass("ui-state-active"); 
		})
		.mouseup(function(){
				$(this).removeClass("ui-state-active");
		});
    
		$(\'#start-editK\').click(function() {
		  var echeck = handle();
		  if (echeck == "") {
			  $(\'#dialog_notice\').dialog(\'open\');
			    
        $("#dialog_notice ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');

			} else {
			  $("#date2_all").val("");
			  $("#date3_all").val("");
			  $("#arts_all").val("<leer>");
			  $("#artr2_all").val("<leer>");
			  $("#artd2_all").val("<leer>");
			  $("#ap_all").val("");
			  $("#tel_all").val("");
			  $("#in_all").val("");
			  $(\'#dialog_editK\').dialog(\'open\');
			    
        $("#dialog_edit ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');

			}
		})
		.hover(
			function(){ 
				$(this).addClass("ui-state-hover"); 
			},
			function(){ 
				$(this).removeClass("ui-state-hover"); 
			}
		).mousedown(function(){
			$(this).addClass("ui-state-active"); 
		})
		.mouseup(function(){
				$(this).removeClass("ui-state-active");
		});


    
		$("#dialog_editK").dialog({
			bgiframe: true,
			autoOpen: false,
			resizable: false,
			height: 470,
			width: 550,
			modal: true,
			buttons: {
				\'Jetzt ändern\': function() {
					var bValid = true;
					var date = daa.val();
					var status = ads.val();
					var grund = adr.val();
					var meile = add.val();
					var ap = ada.val();
					var tel = adt.val();
          var cids = handle();
          var inx = $("#in_all").val();
					allFields.removeClass(\'ui-state-error\');
					
					if (($("#date2_all").val() == "") && ($("#arts_all").val() == "<leer>") && ($("#artr2_all").val() == "<leer>") && ($("#artd2_all").val() == "<leer>") && ($("#ap_all").val() == "") && ($("#tel_all").val() == "") && ($("#in_all").val() == "")) {
					  updateTipsK("Bitte treffe zumindest eine Auswahl! ;-)");
					} else {
				  	
  					if (bValid) {
              $.ajax({
                type: "POST",
                url:  "_ajax.php",
                dataType: "html; charset=utf-8", 
                data: { id: cids, type: "messeditK", art: $("#art_doc").val(), stundung: $("#stundung_all2").val(), rueck: $("#date3_all").val(), date: $("#date2_all").val(), status: $("#arts_all").val(), grund: $("#artr2_all").val(), meile: $("#artd2_all").val(), ap: $("#ap_all").val(), tel: $("#tel_all").val(), info: $("#in_all").val() },
              });
              $(this).dialog(\'close\');
  					}

  					cids.split("|").forEach(function(entry) {

              // modal: date3_all -> date_1123987
              $("#date_" + entry).val( $("#date3_all").val());

              if ((status != "<leer>") && (status != "")) {

                if (status == "geschlossen / positiv") {

                  $("#e_" + entry + " td:nth-child(2) img").attr("src", "/assets/images/sys/status_thick.png");
                  var part = $("#e_" + entry + " td:nth-child(2)").html().split("enquired");
                  if (part.length == 2) $("#e_" + entry + " td:nth-child(2)").html(part[0] + "OK" + part[1]);

                  $("#e_" + entry).removeClass("bgred");
                  $("#e_" + entry).addClass("bggreen");
  
                } else {

                  $("#e_" + entry + " td:nth-child(2) img").attr("src", "/assets/images/sys/status_wait.png");
                  var part = $("#e_" + entry + " td:nth-child(2)").html().split("OK");
                  if (part.length == 2) $("#e_" + entry + " td:nth-child(2)").html(part[0] + "enquired" + part[1]);

                  if (status == "geschlossen / negativ") {
                    $("#e_" + entry).removeClass("bggreen");
                    $("#e_" + entry).removeClass("bgred");
                  } else { //in Klärung
                    $("#e_" + entry).removeClass("bggreen");
                    $("#e_" + entry).addClass("bgred");
                  }

                }

                $("#e_" + entry + " td:nth-child(10)").html(status);

              }
              
              if (inx != "") {
                if (inx == "[LÖSCHEN]") {
                  $("#in_" + entry).html("");
                } else {
                  $("#in_" + entry).html(\'<img src="img/information.png" title="\' + inx + \'">\');
                }
              }

            });
					
					}
				},
				\'Abbrechen\': function() {
					$(this).dialog(\'close\');
				}
			},
			close: function() {
			  $("#date_all").datepicker("hide");
			  tipsK.text(\' \');
				allFields.val(\'\').removeClass(\'ui-state-error\');
			}
		});

  });

</script>


      
<script>
  $(document).ready(function() {

		var message = daa = $("#date_all"),
			allFields = $([]).add(daa),
			tipsE = $("#validateTipsE");

		function updateTipsE(t) {
			tipsE.text(t).effect("highlight",{},1500);
		}

    function handle() {
      var strValues = "";
      $(".rueck").each (function() {
        if($(this).is(":checked")) {
          strValues+= "|" + $(this).attr(\'rel\');
        }
      });
      strValues = strValues.substr(1);
      return strValues;
    }
    
		$(\'#start-edit\').click(function() {
		  var echeck = handle();
		  if (echeck == "") {
			  $(\'#dialog_notice\').dialog(\'open\');
			    
        $("#dialog_notice ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');

			} else {
			  $("#date_all").val("");
			  $("#artd_all").val("<leer>");
			  $(\'#dialog_edit\').dialog(\'open\');
			  //$("#in_all").val("");
			    
        $("#dialog_edit ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');

			}
		})
		.hover(
			function(){ 
				$(this).addClass("ui-state-hover"); 
			},
			function(){ 
				$(this).removeClass("ui-state-hover"); 
			}
		).mousedown(function(){
			$(this).addClass("ui-state-active"); 
		})
		.mouseup(function(){
				$(this).removeClass("ui-state-active");
		});
    
		$("#dialog_edit").dialog({
			bgiframe: true,
			autoOpen: false,
			resizable: false,
			height: 350,
			width: 400,
			modal: true,
			buttons: {
				\'Jetzt ändern\': function() {
					var bValid = true;
					var date = daa.val();
					var cids = handle();
					//var inx = $("#in_all").val();
					allFields.removeClass(\'ui-state-error\');
					
					
					if (($("#date_all").val() == "") && ($("#artd_all").val() == "<leer>")) { // && ($("#in_all").val() == "")
					  updateTipsE("Bitte treffe zumindest eine Auswahl! ;-)");
					} else {
				  	
  					if (bValid) {
              $.ajax({
                type: "POST",
                url:  "_ajax.php",
                dataType: "html; charset=utf-8", 
                data: { id: cids, type: "messedit", date: $("#date_all").val(), artd: $("#artd_all").val()}, //, info: $("#in_all").val() 
              });
              $("#ui-datepicker-div").hide();
              $(this).dialog(\'close\');
  					}
  					
  					cids.split("|").forEach(function(entry) {
              if (date != "") $("#date_" + entry).val(date);
            });

  					cids.split("|").forEach(function(entry) {
              if (date != "") {
                if (!$("#date_" + entry).addClass("nored")) {
                  $("#date_" + entry).val(date);
                  $("#date_" + entry).parent().find("span").text(date);
                  var arr = date.split(".");
                  var date1 = new Date((new Date).getFullYear(), (new Date).getMonth() + 1, (new Date).getDate()); 
                  var date2 = new Date(arr[2], arr[1], arr[0]);
                  var dif = date2.getTime() - date1.getTime();
                  dif = Math.ceil(dif / 1000 / 60 / 60 / 24); 
                  if (dif == 0) {
                    $("#date_" + entry).addClass("red");
                  } else if (dif < 0) {
                    $("#date_" + entry).addClass("red");
                  } else {
                    $("#date_" + entry).removeClass("red");
                  }
                }
              }

            });

					
					}
				},
				\'Abbrechen\': function() {
					$(this).dialog(\'close\');
				}
			},
			close: function() {
			  $("#date_all").datepicker("hide");
			  tipsE.text(\' \');
				allFields.val(\'\').removeClass(\'ui-state-error\');
			}
		});


    /***************/
    
		$(\'#start-editKK\').click(function() {
      var echeck = handle();
      if (echeck == "") {

        $(\'#dialog_notice\').dialog(\'open\');
        $("#dialog_notice ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');

			} else {

        var strValues = "";
        $(".rueck:checked").each (function() {
            strValues+= "|" + $(this).attr(\'rel\');
        });

        $(\'#dialog_editKK\').dialog(\'open\');
        $("#date_allKK").val("");
          
        $("#dialog_editKK ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');
        $("#dialog_editKK ~ .ui-dialog-buttonpane").children("button:contains(\'Bestätigt\')").addClass(\'green\');

			}
		})
		.hover(
			function(){ 
				$(this).addClass("ui-state-hover"); 
			},
			function(){ 
				$(this).removeClass("ui-state-hover"); 
			}
		).mousedown(function(){
			$(this).addClass("ui-state-active"); 
		})
		.mouseup(function(){
				$(this).removeClass("ui-state-active");
		});

    
		$("#dialog_editKK").dialog({
			bgiframe: true,
			autoOpen: false,
			resizable: false,
			height: 220,
			width: 400,
			modal: true,
			buttons: {
				\'Bestätigt\': function() {
					var bValid = true;
          var cids = handle();
									  	
  					if (bValid) {
              $.ajax({
                type: "POST",
                url:  "_ajax.php",
                dataType: "html; charset=utf-8", 
                data: { id: cids, type: "messeditKK", rueck: $("#date_allKK").val() }
              });
              $(this).dialog(\'close\');
  					}

            cids.split("|").forEach(function(entry) {

              $("#e_" + entry + " td:nth-child(2) img").attr("src", "/assets/images/sys/status_warning.png");
              $("#e_" + entry).addClass("bggreen");

              var part = $("#e_" + entry + " td:nth-child(2)").html().split("enquired");
              if (part.length == 2) $("#e_" + entry + " td:nth-child(2)").html(part[0] + "zurückgestellt von IZS" + part[1]);

              var part = $("#e_" + entry + " td:nth-child(2)").html().split("OK");
              if (part.length == 2) $("#e_" + entry + " td:nth-child(2)").html(part[0] + "zurückgestellt von IZS" + part[1]);

              $("#date_" + entry).val($("#date_allKK").val());

            });


				},
				\'Abbrechen\': function() {
					$(this).dialog(\'close\');
				}
			},
			close: function() {
			}
		});


    /***************/
    
		$(\'#start-editKG\').click(function() {
      var echeck = handle();
      if (echeck == "") {

        $(\'#dialog_notice\').dialog(\'open\');
        $("#dialog_notice ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');

			} else {

        var strValues = "";
        $(".rueck:checked").each (function() {
            strValues+= "|" + $(this).attr(\'rel\');
        });

        $(\'#dialog_editKG\').dialog(\'open\');
        $("#date_allKG").val("");
          
        $("#dialog_editKG ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');
        $("#dialog_editKG ~ .ui-dialog-buttonpane").children("button:contains(\'Bestätigt\')").addClass(\'green\');

			}
		})
		.hover(
			function(){ 
				$(this).addClass("ui-state-hover"); 
			},
			function(){ 
				$(this).removeClass("ui-state-hover"); 
			}
		).mousedown(function(){
			$(this).addClass("ui-state-active"); 
		})
		.mouseup(function(){
				$(this).removeClass("ui-state-active");
		});

    
		$("#dialog_editKG").dialog({
			bgiframe: true,
			autoOpen: false,
			resizable: false,
			height: 220,
			width: 400,
			modal: true,
			buttons: {
				\'Bestätigt\': function() {
					var bValid = true;
          var cids = handle();
									  	
  					if (bValid) {
              $.ajax({
                type: "POST",
                url:  "_ajax.php",
                dataType: "html; charset=utf-8", 
                data: { id: cids, type: "messeditKG", rueck: $("#date_allKG").val() }
              });
              $(this).dialog(\'close\');
  					}

            cids.split("|").forEach(function(entry) {

              $("#e_" + entry + " td:nth-child(2) img").attr("src", "/assets/images/sys/status_warning.png");
              $("#e_" + entry).addClass("bggreen");

              var part = $("#e_" + entry + " td:nth-child(2)").html().split("enquired");
              if (part.length == 2) $("#e_" + entry + " td:nth-child(2)").html(part[0] + "zurückgestellt von IZS" + part[1]);

              var part = $("#e_" + entry + " td:nth-child(2)").html().split("OK");
              if (part.length == 2) $("#e_" + entry + " td:nth-child(2)").html(part[0] + "zurückgestellt von IZS" + part[1]);

              $("#date_" + entry).val($("#date_allKG").val());

            });




				},
				\'Abbrechen\': function() {
					$(this).dialog(\'close\');
				}
			},
			close: function() {
			}
		});

    /***************/
    
		$(\'#start-editOk\').click(function() {
      var echeck = handle();
      if (echeck == "") {

        $(\'#dialog_notice\').dialog(\'open\');
        $("#dialog_notice ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');

			} else {

        var strValues = "";
        $(".rueck:checked").each (function() {
            strValues+= "|" + $(this).attr(\'rel\');
        });

        $.ajax({

          type: "POST",
          url:  "inc/svproof.ajax.php",
          data: { ac: "chk_klaerung", list: strValues },
  
          success: function(data) {

            if (data != 1) {

              alert("Achtung! Du hast folgende Klärungsfälle ausgewählt: " + data);
  
            } else {

              $.ajax({

                type: "POST",
                url:  "inc/svproof.ajax.php",
                data: { ac: "chk_status", list: strValues },
        
                success: function(data) {

                  if (data != 1) {
                    alert(data);
                  }
  
                  $("#date_all_ok").val("");
                  $("#artd_all_ok").val("Dynamische Erzeugung");
          
                  $("#status_all").val("OK");
                  $("#stundung_all").val("<leer>");
          
                  $(\'#dialog_editOk\').dialog(\'open\');
                  //$("#in_all").val("");
                    
                  $("#dialog_editOk ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');
                  $("#dialog_editOk ~ .ui-dialog-buttonpane").children("button:contains(\'OK\')").addClass(\'green\');

                }
  
              });
                
      
            }

          }
  
        });

			}
		})
		.hover(
			function(){ 
				$(this).addClass("ui-state-hover"); 
			},
			function(){ 
				$(this).removeClass("ui-state-hover"); 
			}
		).mousedown(function(){
			$(this).addClass("ui-state-active"); 
		})
		.mouseup(function(){
				$(this).removeClass("ui-state-active");
		});

    
		$("#dialog_editOk").dialog({
			bgiframe: true,
			autoOpen: false,
			resizable: false,
			height: 350,
			width: 400,
			modal: true,
			buttons: {
				\'Auf OK setzen\': function() {
					var bValid = true;
					var date = daa.val();
					var cids = handle();
					//var inx = $("#in_all").val();
					allFields.removeClass(\'ui-state-error\');
					
					
					if (($("#date_all_ok").val() == "")) {
					  
            $("#validateTipsOK").text("\'Rückmeldung am\' muss ausgefüllt sein!").effect("highlight",{},1500);

					} else {
				  	
  					if (bValid) {
              $.ajax({
                type: "POST",
                url:  "_ajax.php",
                dataType: "html; charset=utf-8", 
                data: { id: cids, type: "messeditOk", date: $("#date_all_ok").val(), artd: $("#artd_all_ok").val(), ergebnis: $("#status_all").val(), stundung: $("#stundung_all").val() }, //, info: $("#in_all").val() 
              });
              $(this).dialog(\'close\');
  					}
  					
  					cids.split("|").forEach(function(entry) {
              if ($("#date_all_ok").val() != "") $("#date_" + entry).val($("#date_all_ok").val());
              if ($("#artd_all_ok").val() != "<leer>") $("#artd_" + entry).val($("#artd_all_ok").val());
            });

  					cids.split("|").forEach(function(entry) {
              if ($("#date_all_ok").val() != "") {
                if (!$("#date_" + entry).addClass("nored")) {
                  $("#date_" + entry).val($("#date_all_ok").val());
                  $("#date_" + entry).parent().find("span").text($("#date_all_ok").val());
                  var arr = date.split(".");
                  var date1 = new Date((new Date).getFullYear(), (new Date).getMonth() + 1, (new Date).getDate()); 
                  var date2 = new Date(arr[2], arr[1], arr[0]);
                  var dif = date2.getTime() - date1.getTime();
                  dif = Math.ceil(dif / 1000 / 60 / 60 / 24); 
                  if (dif == 0) {
                    $("#date_" + entry).addClass("red");
                  } else if (dif < 0) {
                    $("#date_" + entry).addClass("red");
                  } else {
                    $("#date_" + entry).removeClass("red");
                  }
                }
              }


              if ($("#artd_all_ok").val() != "<leer>") {
                $("#artd_" + entry).val($("#artd_all_ok").val());
                $("#artd_" + entry).parent().find("span").text($("#artd_all_ok").val());
              }
              if ($("#artd_all_ok").val() == "Dynamische Erzeugung") {
                var part = $("#e_" + entry + " td:nth-child(2)").html().split("enquired");
                if (part.length == 2) {
                  $("#e_" + entry + " td:nth-child(2)").html(part[0] + "OK" + part[1]);
                  $("#e_" + entry + " td:nth-child(2) img").attr("src", "/assets/images/sys/status_thick.png");
                }
              }

            });

					
					}
				},
				\'Abbrechen\': function() {
					$(this).dialog(\'close\');
				}
			},
			close: function() {
			  $("#date_all").datepicker("hide");
			  tipsE.text(\' \');
				allFields.val(\'\').removeClass(\'ui-state-error\');
			}
		});









    $(function() {
      $("#dialog_massnote").dialog({
        bgiframe: true,
        autoOpen: false,
        resizable: false,
        height: 400,
        width: 367,
        modal: true,
        buttons: {
          \'Hinzufügen\': function() {

            var bValid = true;
            var inx = $("#in_allmass").val();
            var cids = handle();
            
            if ($("#in_allmass").val() == "") {
              updateTipsN("Bitte gebe die Daten ein:");
            } else {
              
              if (bValid) {
                $.ajax({
                  type: "POST",
                  url:  "_ajax.php",
                  dataType: "html; charset=utf-8", 
                  data: { id: cids, type: "massnote", info: $("#in_allmass").val() },
                });
                $(this).dialog(\'close\');
              }
              
            }
          },
          \'Abbrechen\': function() {
            $(this).dialog(\'close\');
          }
        },
        close: function() {
          $("#in_allmass").val("");
          tipsN.text(\' \');
          //allFields.val(\'\').removeClass(\'ui-state-error\');
        }
    
      });
    
    });



  });

</script>


<div id="dialog_notice" title="Ups!">
	<p>Es sind noch keine Events ausgewählt.</p>
	<p>Bitte wähle zuerst die Events, für welche die 
    Massenfunktion durchgeführt werden soll.</p>
</div>

<script type="text/javascript">
$(function() {
	$("#dialog_notice").dialog({
		bgiframe: true,
		autoOpen: false,
		resizable: false,
		modal: true,
		buttons: {
			\'Verstanden\': function() {
				$(this).dialog(\'close\');
			}
		}
	});

});
</script>

';

//colratio    : [30, 100, 80, 200, 200, 200, 80, 200, 120, 100, 240, 100, 120], 
     $strOutput.= "
<script>
$('#5').fixheadertable({ 
   colratio    : [30, 100, 50, 100, 80, 400, 400, 80, 120, 100, 120], 
   height      : 500, 
   width       : 1610, 
   zebra       : true, 
   sortable    : true,
   sortType    : ['', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'date', 'string', 'string'],
   sortedColId : 6, 
   dateFormat  : 'd.m.Y'
});

</script>
";

    if (($strNext != '') && ($strNext != 'blubb') && ($_REQUEST['strSelSta'] != 'OK') && ($_SERVER['REQUEST_METHOD'] != 'POST')) {
      $strOutput.= '<script>$(document).ready(function () { openOffersDialog(\'' .$strNext .'\'); });</script>' .chr(10);
    }

          
  } else {
    
    $strOutput.= '<p>Keine Events im gewählten Zeitraum vorhanden.</p>' .chr(10);
    
  }

}

$strOutput.= '';

}

$strOutput.= '
<script>
$(function() {
  $("#datepicker3").datepicker();
});
</script>
';

?>