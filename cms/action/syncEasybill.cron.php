<?php

session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1); 

if (strstr(__DIR__, 'test.') !== false) {
  define('APP_PATH', '/data/www/sites/test.izs.de/html/');
  $strPath = 'test.izs.de';
} else {
  define('APP_PATH', '/data/www/sites/www.izs.de/html/');
  $strPath = 'www.izs.de';
}



//CLASSES
require_once(APP_PATH .'cms/classes/class.user.php');
require_once(APP_PATH .'cms/classes/class.premiumpayer.php');
require_once(APP_PATH .'cms/functions.inc.php');


//FUNCTIONS
/**
* Send a POST requst using cURL 
* @param string $url to request 
* @param array $post values to send 
* @param array $options for cURL 
* @return string 
*/ 
function curl_post($url, array $post = NULL, array $options = array(), $strToken = '') { 

  $defaults = array( 
    CURLOPT_POST => 1, 
    CURLOPT_HEADER => 0, 
    CURLOPT_URL => $url, 
    CURLOPT_FRESH_CONNECT => 1, 
    CURLOPT_RETURNTRANSFER => 1, 
    CURLOPT_FORBID_REUSE => 1, 
    CURLOPT_TIMEOUT => 4,
    CURLOPT_POSTFIELDS => http_build_query($post) 
  ); 

  $ch = curl_init(); 

  if ($strToken != '') {
    curl_setopt ($ch, CURLOPT_HTTPHEADER, array(
      'x-accesstoken => ' .$strToken
    )); 
  }
  
  curl_setopt_array($ch, ($options + $defaults)); 
  if( ! $result = curl_exec($ch)) { 
    trigger_error(curl_error($ch)); 
  } 
  curl_close($ch); 
  return $result; 

} 

/** 
* Send a GET requst using cURL 
* @param string $url to request 
* @param array $get values to send 
* @param array $options for cURL 
* @return string 
*/ 
function curl_get($url, array $get = NULL, array $options = array(), array $header = array()) {

  $defaults = array( 
    CURLOPT_URL => $url. (strpos($url, '?') === FALSE ? '?' : ''). http_build_query($get), 
    CURLOPT_HEADER => 0, 
    CURLOPT_RETURNTRANSFER => TRUE, 
    CURLOPT_TIMEOUT => 4
); 
  
  $ch = curl_init();

  if (count($header) > 0) {
    curl_setopt ($ch, CURLOPT_HTTPHEADER, $header); 
  }
  curl_setopt_array($ch, ($options + $defaults)); 
  if( ! $result = curl_exec($ch)) { 
    trigger_error(curl_error($ch)); 
  } 
  curl_close($ch); 
  return $result; 

} 



//REQUEST

//OBJECTS


//CONST
$strAuthMail = 'monika.albert@izs-institut.de';
$strAuthPass = 'zCZrzzdkqAiKC5LSkxwEmD3Ph3JrlgG5BAcT0CjYjFwHqO29Jt0IMcORqUd6pWxc';

$strAuth = 'Basic ' .base64_encode($strAuthMail .':' .$strAuthPass);

//FUNCTIONS

$headers = array(
  'Accept: application/json',
  'Authorization: ' .$strAuth
);

$arrRes = curl_get('https://api.easybill.de/rest/v1/customers', array(), array(), $headers);
$arrCustomerList = json_decode($arrRes, true);
$intCountResult  = count($arrCustomerList);

$arrEbCostumerList = array();

if (($intCountResult > 0) && (isset($arrCustomerList['total'])) && ($arrCustomerList['total'] > 0)) {
  
  foreach ($arrCustomerList['items'] as $intKey => $arrCustomer) {

    $arrEbCostumerList[$arrCustomer['number']] = array(
      'acquire_options' => '1',
      'cash_allowance_days' => 7,
      'city' => $arrCustomer['city'],
      'company_name' => $arrCustomer['company_name'],
      'country' => 'DE',
      'emails' => $arrCustomer['emails'],
      'first_name' => $arrCustomer['first_name'],
      'id' => $arrCustomer['id'],
      'last_name' => $arrCustomer['last_name'],
      'login_id' => $arrCustomer['login_id'],
      'number' => $arrCustomer['number'],
      'salutation' => $arrCustomer['salutation'],
      'street' => $arrCustomer['street'],
      'zip_code' => $arrCustomer['zip_code']
    );

  }

}

$strSql = 'SELECT `Group__c`.`Name`, `Group__c`.`Strasse__c`, `Group__c`.`PLZ__c`, ';
$strSql.= '`Group__c`.`Ort__c`, `Group__c`.`Betriebsnummer__c` FROM `Vertrag__c` ';
$strSql.= 'INNER JOIN `Group__c` ON `Vertrag__c`.`Company_Group__c` = `Group__c`.`Id` ';
$strSql.= 'WHERE `Company_Group__c` != "" AND `Aktiv__c` = "true"';
$arrResult = MySQLStatic::Query($strSql); 
$intCountResult  = count($arrResult);

$arrSfCostumerList = array();

if ($intCountResult > 0) {
  
  foreach ($arrResult as $intKey => $arrCustomer) {

    $arrSfCostumerList[$arrCustomer['Betriebsnummer__c']] = array(
      'acquire_options' => '1',
      'cash_allowance_days' => 7,
      'city' => $arrCustomer['Ort__c'],
      'company_name' => $arrCustomer['Name'],
      'country' => 'DE',
      'id' => 0,
      'login_id' => 123729,
      'number' => $arrCustomer['Betriebsnummer__c'],
      'street' => $arrCustomer['Strasse__c'],
      'zip_code' => $arrCustomer['PLZ__c']
    );

    $strSqlC = 'SELECT `Contact`.`FirstName`, `Contact`.`LastName`, `Contact`.`Salutation`, `Contact`.`Email` ';
    $strSqlC.= 'FROM `Account` INNER JOIN `Contact` ON `Account`.`Id` = `Contact`.`AccountId` ';
    $strSqlC.= 'WHERE `Contact`.`Aktiv__c` = "true" AND `Ansprechpartner_fuer__c` LIKE "%Vertrag%" ';
    $strSqlC.= 'AND `SF42_Comany_ID__c` = "' .$arrCustomer['Betriebsnummer__c'] .'"';
    $arrResultC = MySQLStatic::Query($strSqlC); 
    $intCountResultC  = count($arrResultC);

    if ($intCountResultC == 1) {

      $arrSfCostumerList[$arrCustomer['Betriebsnummer__c']]['emails'] = array($arrResultC[0]['Email']);
      $arrSfCostumerList[$arrCustomer['Betriebsnummer__c']]['first_name'] = $arrResultC[0]['FirstName'];
      $arrSfCostumerList[$arrCustomer['Betriebsnummer__c']]['last_name'] = $arrResultC[0]['LastName'];
      if ($arrResultC[0]['Salutation'] == 'Herr') {
        $arrSfCostumerList[$arrCustomer['Betriebsnummer__c']]['salutation'] = 0;
      } else {
        $arrSfCostumerList[$arrCustomer['Betriebsnummer__c']]['salutation'] = 2;
      }

    }

  }

}

print_r($arrSfCostumerList);


?>