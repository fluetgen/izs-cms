<?php

session_start();
error_reporting(E_ALL);
ini_set("display_errors", 1);

if (strstr(__DIR__, 'test.') !== false) {
  define('APP_PATH', '/data/www/sites/test.izs.de/html/');
} else {
  define('APP_PATH', '/data/www/sites/www.izs.de/html/');
}
//CLASSES
require_once(APP_PATH .'cms/classes/class.user.php');
require_once(APP_PATH .'assets/classes/class.database.php');

require_once(APP_PATH .'cms/classes/class.stream.php');
require_once(APP_PATH .'cms/classes/class.request.php');
require_once(APP_PATH .'cms/classes/class.sendrequest.php');

require_once(APP_PATH .'cms/functions.inc.php');

require_once(APP_PATH .'cms/classes/class.bgevent.php');
require_once(APP_PATH .'cms/classes/class.bg.php');


//REQUEST
$strAc   = $_REQUEST['ac'];
$intDdId = @$_REQUEST['ddid'];
$intBgId = @$_REQUEST['bgid'];
$intBeId = @$_REQUEST['beid'];
$intCtId = @$_REQUEST['ctid'];
$intYear = @$_REQUEST['year'];
$strVal  = @$_REQUEST['value'];

//OBJECTS
$objStream   = new Stream('basic');
$objUser     = new User;

$objRequest     = new Request;
$objSendRequest = new SendRequest;

$objBgEvent  = new BgEvent;
$objBg       = new Bg;

//CONST

//FUNCTIONS

if ($strAc == 'auskunft') {
  
  $arrEmpty = array();
  
  $arrBgEvent = $objBgEvent->arrGetBgEvent($intBeId);

  $strMitglNummer  = $objBgEvent->strGetMitgliedsnummer($arrBgEvent[0]['be_bg'], $arrBgEvent[0]['be_meldestelle']);
  $strUnternNummer = $objBgEvent->strGetUnternehmensnummer($arrBgEvent[0]['be_bg'], $arrBgEvent[0]['be_meldestelle']);

  $arrBg = $objBg->arrGetBg($arrBgEvent[0]['be_bg']);
  $arrMeldestelle = $objBg->arrGetMeldestellen('', $arrBgEvent[0]['be_meldestelle']);

  $strAuskunftsGeber = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_auskunftsgeber');
  $strAuskunftsGeber.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_auskunftsgeber'], 'select', 'ag_' .$intBeId, true, ' class=""');
  
  $strErgebnis = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_ergebnis');
  $strErgebnis.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_ergebnis'], 'select', 'er_' .$intBeId, true, ' class=""');

  $strDokumentArt = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_dokument_art');
  $strDokumentArt.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_dokument_art'], 'select', 'da_' .$intBeId, true, ' class=""');

  $strSperrvermerk = '';
  $arrSelValues = array('ja' => 1);
  $strSperrvermerk.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_sperrvermerk'], 'checkbox', 'sp_' .$intBeId, true, ' class=""');

  $strBemerkungSichtbar = '';
  $strBemerkungSichtbar.= strGetFormElement($arrEmpty, $arrBgEvent[0]['be_bemerkung'], 'textarea', 'bs_' .$intBeId, true, ' class=""');
  
  $strSichtbarkeit = '';
  if ($arrBgEvent[0]['be_sichtbar'] == '') {
    $arrBgEvent[0]['be_sichtbar'] = 'sichtbar';
  }
  $arrSelValues = $objBgEvent->arrGetSelection('be_sichtbar');
  $strSichtbarkeit.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_sichtbar'], 'select', 'si_' .$intBeId, false, ' class=""');
  
  $strFristDate = '';
  if (($arrBgEvent[0]['be_befristet'] != '') && ($arrBgEvent[0]['be_befristet'] != '0000-00-00')) {
    $strFristDate = strConvertDate($arrBgEvent[0]['be_befristet'], 'Y-m-d', 'd.m.Y');
  }
  $strBefristet = '';
  $strBefristet.= '
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="icon wb-calendar" aria-hidden="true"></i>
                    </span>
                    <input name="be_' .$intBeId .'" id="be_' .$intBeId .'" type="text" class="form-control dp" data-plugin="datepicker" value="' .$strFristDate .'" data-date-format="dd.mm.yyyy" style="width: 120px;">
                  </div>
  ';
    
  $strModal = '
                              <div class="modal-header bg-blue-grey-600">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">x</span>
                                </button>
                                <h4 class="modal-title">Auskunft erfassen</h4>
                              </div>
                              <div class="modal-datail bg-cyan-200">

                                <table>

                                  <col width="150"><!-- 860 -->
                                  <col width="530">
                                  <col width="95">
                                  <col width="85">

                                  <thead>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>Berufsgenossenschaft: </td>
                                      <td>' .$arrBg[0]['ac_name'] .'</td>
                                      <td>Vorgangs-Id: </td>
                                      <td>' .$arrBgEvent[0]['be_name'] .'</td>
                                    </td>
                                    <tr>
                                      <td>Meldestelle: </td>
                                      <td>' .@$arrMeldestelle[0]['ac_name'] .'</td>
                                      <td>Beitragsjahr: </td>
                                      <td>' .$arrBgEvent[0]['be_jahr'] .'</td>
                                    </td>
                                    <tr>
                                      <td>Unternehmensnummer: </td>
                                      <td>' .$strUnternNummer .'</td>
                                      <td></td>
                                      <td></td>
                                    </td>
                                  </tbody>
                                </table>
                                
                              </div>


  
                              <form> 
                              
                              <div class="modal-body">

                                <input type="hidden" name="beid" value="' .$intBeId .'">
                                <input type="hidden" name="be_link" id="be_link" value="">
                                
                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="ag_' .$intBeId .'">Dokument:</label>
                                  </div>
                                  <div class="col-lg-9" id="fileList">
                                    <a href="javascript:void(0)" id="open_btn">Drag \'n drop file here.</a>
                                  </div>
                                </div>

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="ag_' .$intBeId .'">Auskunftsgeber:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strAuskunftsGeber .'
                                  </div>
                                </div>
          
                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="be_' .$intBeId .'">Befristet bis:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strBefristet .'
                                  </div>
                                </div>

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="er_' .$intBeId .'">Ergebnis:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strErgebnis .'
                                  </div>
                                </div>

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="da_' .$intBeId .'">Art des Dokuments:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strDokumentArt .'
                                  </div>
                                </div>
          
                                <div class="row">
                                  <div class="col-lg-3">
                                    <label for="sp_' .$intBeId .'" class="tsp">Sperrvermerk:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strSperrvermerk .'
                                  </div>
                                </div>

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="bs_' .$intBeId .'">Bemerkung (sichtbar):</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strBemerkungSichtbar .'
                                  </div>
                                </div>
          
                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="si_' .$intBeId .'">Sichtbarkeit Dokument:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strSichtbarkeit .'
                                  </div>
                                </div>
          
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                                <button type="button" class="btn btn-primary right" id="svAuskunft">OK</button>
                              </div>

                              </form>

';

  echo $strModal;
  exit;    

}


if ($strAc == 'klaerung') {
  
  $arrBgEvent = $objBgEvent->arrGetBgEvent($intBeId);

  $strMitglNummer = $objBgEvent->strGetMitgliedsnummer($arrBgEvent[0]['be_bg'], $arrBgEvent[0]['be_meldestelle']);

  $arrBg = $objBg->arrGetBg($arrBgEvent[0]['be_bg']);
  $arrMeldestelle = $objBg->arrGetMeldestellen('', $arrBgEvent[0]['be_meldestelle'], false, false);
  
  $arrEmpty = array();


  $strValueRueckmeldung = '';
  if (($arrBgEvent[0]['be_wiedervorlage'] != '') && ($arrBgEvent[0]['be_datum_rueckmeldung'] != '0000-00-00')) {
    $strValueRueckmeldung = strConvertDate($arrBgEvent[0]['be_datum_rueckmeldung'], 'Y-m-d', 'd.m.Y');
  }
  $strRueck = '';
  $strRueck.= '
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="icon wb-calendar" aria-hidden="true"></i>
                    </span>
                    <input name="dr_' .$intBeId .'" id="dr_' .$intBeId .'" type="text" class="form-control dp" data-plugin="datepicker" value="' .$strValueRueckmeldung .'" data-date-format="dd.mm.yyyy" style="width: 120px;">
                  </div>
  ';
  
  $strStatusKlaerung = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_klaerung_status');
  $strStatusKlaerung.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_klaerung_status'], 'select', 'sk_' .$intBeId, true, ' class=""');

  $strStundung = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_stundung');
  $strStundung.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_stundung'], 'select', 'st_' .$intBeId, true, ' class=""');

  $strMeldepflicht = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_meldepflicht');
  $strMeldepflicht.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_meldepflicht'], 'select', 'me_' .$intBeId, false, ' class=""');

  $strBearbeiter = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_bearbeiter');
  $strBearbeiter.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_bearbeiter'], 'select', 'us_' .$intBeId, true, ' class=""', true);
  



  $strRueckstand = '';
  $strRueckstand.= strGetFormElement($arrEmpty, $arrBgEvent[0]['be_rueckstand'], 'text', 'rr_' .$intBeId, true, ' class=""');

  $strGrund = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_klaerung_grund');
  $strGrund.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_klaerung_grund'], 'select', 'gr_' .$intBeId, true, ' class=""');
  
  $strSchritt = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_klaerung_schritt');
  $strSchritt.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_klaerung_schritt'], 'select', 'ns_' .$intBeId, true, ' class=""');

  $strAp = '';
  $strAp.= strGetFormElement($arrEmpty, $arrBgEvent[0]['be_klaerung_ap'], 'text', 'ap_' .$intBeId, true, ' class=""');

  $strTel = '';
  $strTel.= strGetFormElement($arrEmpty, $arrBgEvent[0]['be_klaerung_telefon'], 'text', 'at_' .$intBeId, true, ' class=""');
  
  $strValueWiedervorlage = '';
  if (($arrBgEvent[0]['be_wiedervorlage'] != '') && ($arrBgEvent[0]['be_wiedervorlage'] != '0000-00-00')) {
    $strValueWiedervorlage = strConvertDate($arrBgEvent[0]['be_wiedervorlage'], 'Y-m-d', 'd.m.Y');
  }
  $strWieder = '';
  $strWieder.= '
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="icon wb-calendar" aria-hidden="true"></i>
                    </span>
                    <input name="wi_' .$intBeId .'" id="wi_' .$intBeId .'" type="text" class="form-control dp" data-plugin="datepicker" value="' .$strValueWiedervorlage .'" data-date-format="dd.mm.yyyy" style="width: 120px;">
                  </div>
  ';
  
  $strInfoIntern = '';
  $strInfoIntern.= strGetFormElement($arrEmpty, $arrBgEvent[0]['be_info_intern'], 'textarea', 'ii_' .$intBeId, true, ' class=""');

    
  $strModal = '
                              <div class="modal-header bg-blue-grey-600">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">x</span>
                                </button>
                                <h4 class="modal-title">Klärungsfall erfassen</h4>
                              </div>
                              <div class="modal-datail bg-cyan-200">
                              
                                <table>

                                  <col width="150"><!-- 860 -->
                                  <col width="530">
                                  <col width="95">
                                  <col width="85">

                                  <thead>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>Berufsgenossenschaft: </td>
                                      <td>' .$arrBg[0]['ac_name'] .'</td>
                                      <td>Vorgangs-Id: </td>
                                      <td>' .$arrBgEvent[0]['be_name'] .'</td>
                                    </td>
                                    <tr>
                                      <td>Meldestelle: </td>
                                      <td>' .$arrMeldestelle[0]['ac_name'] .'</td>
                                      <td>Beitragsjahr: </td>
                                      <td>' .$arrBgEvent[0]['be_jahr'] .'</td>
                                    </td>
                                    <tr>
                                      <td>Mitgliedsnummer: </td>
                                      <td>' .$strMitglNummer .'</td>
                                      <td></td>
                                      <td></td>
                                    </td>
                                  </tbody>
                                </table>
                                
                              </div>

                              <form>
                                <input type="hidden" name="beid_k" id="beid_k" value="' .$intBeId .'">

                              <div class="modal-body">

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="dr_' .$intBeId .'">Rückmeldung am:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strRueck .'
                                  </div>
                                </div>
                                
                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="sk_' .$intBeId .'">Status Klärung:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strStatusKlaerung .'
                                  </div>
                                </div>

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="gr_' .$intBeId .'">Grund für Klärung:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strGrund .'
                                  </div>
                                </div>
          
                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="ns_' .$intBeId .'">Nächster Schritt:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strSchritt .'
                                  </div>
                                </div>

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="rr_' .$intBeId .'">Rückstand:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strRueckstand .'
                                  </div>
                                </div>
          
                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="ap_' .$intBeId .'">Ansprechpartner:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strAp .'
                                  </div>
                                </div>
          
                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="at_' .$intBeId .'">Telefon:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strTel .'
                                  </div>
                                </div>

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="ii_' .$intBeId .'">Hinweis:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strInfoIntern .'
                                  </div>
                                </div>

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="wi_' .$intBeId .'">Wiedervorlage:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strWieder .'
                                  </div>
                                </div>


                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="st_' .$intBeId .'">Stundung / Ratenzahlung:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strStundung .'
                                  </div>
                                </div>


                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="me_' .$intBeId .'">Meldepflicht erfüllt:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strMeldepflicht .'
                                  </div>
                                </div>


                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="us_' .$intBeId .'">Bearbeiter:in:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strBearbeiter .'
                                  </div>
                                </div>


                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                                <button type="button" class="btn btn-primary right" id="svKlaerung">Erfassen</button>
                              </div>

                              </form>


';

  echo $strModal;
  exit;    

}

if ($strAc == 'massKlae') {

  $intBeId = 'all';

  $arrEmpty = array();

  $strValueRueckmeldung = '';
  $strRueck = '';
  $strRueck.= '
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="icon wb-calendar" aria-hidden="true"></i>
                    </span>
                    <input name="dr_' .$intBeId .'" id="dr_' .$intBeId .'" type="text" class="form-control dp" data-plugin="datepicker" value="' .$strValueRueckmeldung .'" data-date-format="dd.mm.yyyy" style="width: 120px;"> <i class="fa fa-times red delDate" style="padding-left: 5px; padding-top: 10px;"> </i>
                  </div>
  ';
  
  $strStatusKlaerung = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_klaerung_status');
  $arrSelValues[] = 'Alle Einträge löschen';
  $strStatusKlaerung.= strGetFormElement($arrSelValues, '', 'select', 'sk_' .$intBeId, true, ' class=""');

  $strStundung = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_stundung');
  $arrSelValues[] = 'Alle Einträge löschen';
  $strStundung.= strGetFormElement($arrSelValues, '', 'select', 'st_' .$intBeId, true, ' class=""');

  $strMeldepflicht = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_meldepflicht');
  $strMeldepflicht.= strGetFormElement($arrSelValues, '', 'select', 'me_' .$intBeId, false, ' class=""');

  $strSql = 'SELECT `cl_ks_id` FROM `cms_login` WHERE `cl_id` = "' .$_SESSION['id'] .'"';
  $arrSql = MySQLStatic::Query($strSql);
  
  if (count($arrSql) > 0) {
    $strCrmUser = $arrSql[0]['cl_ks_id'];
  }

  $strBearbeiter = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_bearbeiter');
  $strBearbeiter.= strGetFormElement($arrSelValues, $strCrmUser, 'select', 'us_' .$intBeId, false, ' class=""', true);
  

  $strRueckstand = '';
  $strRueckstand.= strGetFormElement($arrEmpty, '', 'text', 'rr_' .$intBeId, true, ' class=""');

  $strGrund = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_klaerung_grund');
  $arrSelValues[] = 'Alle Einträge löschen';
  $strGrund.= strGetFormElement($arrSelValues, '', 'select', 'gr_' .$intBeId, true, ' class=""');
  
  $strSchritt = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_klaerung_schritt');
  $arrSelValues[] = 'Alle Einträge löschen';
  $strSchritt.= strGetFormElement($arrSelValues, '', 'select', 'ns_' .$intBeId, true, ' class=""');

  $strAp = '';
  $strAp.= strGetFormElement($arrEmpty, '', 'text', 'ap_' .$intBeId, true, ' class=""');

  $strTel = '';
  $strTel.= strGetFormElement($arrEmpty, '', 'text', 'at_' .$intBeId, true, ' class=""');
  
  $strValueWiedervorlage = '';
  $strWieder = '';
  $strWieder.= '
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="icon wb-calendar" aria-hidden="true"></i>
                    </span>
                    <input name="wi_' .$intBeId .'" id="wi_' .$intBeId .'" type="text" class="form-control dp" data-plugin="datepicker" value="' .$strValueWiedervorlage .'" data-date-format="dd.mm.yyyy" style="width: 120px;"> <i class="fa fa-times red delDate" style="padding-left: 5px; padding-top: 10px;"> </i>
                  </div>
  ';
  
  $strInfoIntern = '';
  $strInfoIntern.= strGetFormElement($arrEmpty, '', 'textarea', 'ii_' .$intBeId, true, ' class=""');

    
  $strModal = '

  <script>
    $(".delDate").on("click", function(e) {
      $(this).prev("input").val("00.00.0000");
    }).hover(function() {
      $(this).css("cursor","pointer");
    });
    $(".delText").on("click", function(e) {
      $(this).prev("input").val("[LÖSCHEN]");
    }).hover(function() {
      $(this).css("cursor","pointer");
    });
    $(".delArea").on("click", function(e) {
      $(this).prev("textarea").val("[LÖSCHEN]");
    }).hover(function() {
      $(this).css("cursor","pointer");
    });
  </script>

  <style>textarea.form-control { width: 90% !important; display: inline; }</style>

                              <div class="modal-header bg-blue-grey-600">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">x</span>
                                </button>
                                <h4 class="modal-title">Klärungsfall</h4>
                              </div>

                              <form>
                                <input type="hidden" name="beid_k" id="beid_k" value="' .$intBeId .'">

                              <div class="modal-body">

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="dr_' .$intBeId .'">Rückmeldung am:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strRueck .'
                                  </div>
                                </div>
                                
                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="sk_' .$intBeId .'">Status Klärung:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strStatusKlaerung .'
                                  </div>
                                </div>

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="gr_' .$intBeId .'">Grund für Klärung:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strGrund .'
                                  </div>
                                </div>
          
                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="ns_' .$intBeId .'">Nächster Schritt:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strSchritt .'
                                  </div>
                                </div>

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="rr_' .$intBeId .'">Rückstand:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strRueckstand .' <i class="fa fa-times red delText" style="padding-left: 5px; padding-top: 10px;"> </i>
                                  </div>
                                </div>
          
                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="ap_' .$intBeId .'">Ansprechpartner:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strAp .' <i class="fa fa-times red delText" style="padding-left: 5px; padding-top: 10px;"> </i>
                                  </div>
                                </div>
          
                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="at_' .$intBeId .'">Telefon:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strTel .' <i class="fa fa-times red delText" style="padding-left: 5px; padding-top: 10px;"> </i>
                                  </div>
                                </div>

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="ii_' .$intBeId .'">Hinweis:</label>
                                  </div>
                                  <div class="col-lg-8">
                                    ' .$strInfoIntern .' <i class="fa fa-times red delArea" style="padding-left: 5px; padding-top: 10px;"> </i>
                                  </div>
                                </div>

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="wi_' .$intBeId .'">Wiedervorlage:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strWieder .'
                                  </div>
                                </div>


                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="st_' .$intBeId .'">Stundung / Ratenzahlung:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strStundung .'
                                  </div>
                                </div>


                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="me_' .$intBeId .'">Meldepflicht erfüllt:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strMeldepflicht .'
                                  </div>
                                </div>


                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="us_' .$intBeId .'">Bearbeiter:in:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strBearbeiter .'
                                  </div>
                                </div>


                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                                <button type="button" class="btn btn-primary right" id="svKlaerungMass">Verarbeiten</button>
                              </div>

                              </form>


';

  echo $strModal;
  exit;    

}


if ($strAc == 'massOK') {

  $intBeId = 'all';

  $arrEmpty = array();
  
  $boolUiOld = false;
  $strValueRueckmeldung = '';

  $strRueck = '';
  $strRueck.= '
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="icon wb-calendar" aria-hidden="true"></i>
                    </span>
                    <input name="dr_' .$intBeId .'" id="dr_' .$intBeId .'" type="text" class="form-control dp" data-plugin="datepicker" value="' .$strValueRueckmeldung .'" data-date-format="dd.mm.yyyy" style="width: 120px;">
                  </div>
  ';
  
  $strFristDate = '';
  
  $strBefristet = '';
  $strBefristet.= '
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="icon wb-calendar" aria-hidden="true"></i>
                    </span>
                    <input name="be_' .$intBeId .'" id="be_' .$intBeId .'" type="text" class="form-control dp" data-plugin="datepicker" value="' .$strFristDate .'" data-date-format="dd.mm.yyyy" style="width: 120px;">
                  </div>
  ';
  
  $strErgebnis = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_ergebnis');

  if ($boolUiOld === false) {
    $arrSelValues = array('OK' => 'OK');
  }

  $strErgebnis.= strGetFormElement($arrSelValues, 'OK', 'select', 'er_' .$intBeId, false, ' class=""');

  $strStundung = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_stundung');
  $arrSelValues[] = 'Alle Einträge löschen';
  $strStundung.= strGetFormElement($arrSelValues, '', 'select', 'st_' .$intBeId, true, ' class=""');

    
  $strModal = '
                              <div class="modal-header bg-blue-grey-600">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">x</span>
                                </button>
                                <h4 class="modal-title">OK erfassen</h4>
                              </div>

                              <form>
                                <input type="hidden" name="beid_ok" id="beid_ok" value="' .$intBeId .'">

                              <div class="modal-body">

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="dr_' .$intBeId .'">Rückmeldung am:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strRueck .'
                                  </div>
                                </div>

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="be_' .$intBeId .'">Befristet bis:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strBefristet .'
                                  </div>
                                </div>

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="er_' .$intBeId .'">Ergebnis:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strErgebnis .'
                                  </div>
                                </div>


                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="st_' .$intBeId .'">Stundung / Ratenzahlung:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strStundung .'
                                  </div>
                                </div>
 
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                                <button type="button" class="btn btn-primary right" id="okMass">Erfassen</button>
                              </div>

                              </form>


';

  echo $strModal;
  exit;    

}


if ($strAc == 'ok_erfassen') {
  
  $arrBgEvent = $objBgEvent->arrGetBgEvent($intBeId);

  $strUnterNummer = $objBgEvent->strGetUnternehmensnummer($arrBgEvent[0]['be_bg'], $arrBgEvent[0]['be_meldestelle']);

  $arrBg = $objBg->arrGetBg($arrBgEvent[0]['be_bg']);
  $arrMeldestelle = $objBg->arrGetMeldestellen('', $arrBgEvent[0]['be_meldestelle'], false, false);
  
  $arrEmpty = array();

  $boolUiOld = true;
  if ($arrBgEvent[0]['be_dokument_art'] == 'Dynamische Erzeugung') {
    $boolUiOld = false;
  }

  $strValueRueckmeldung = '';
  if (($arrBgEvent[0]['be_wiedervorlage'] != '') && ($arrBgEvent[0]['be_datum_rueckmeldung'] != '0000-00-00')) {
    $strValueRueckmeldung = strConvertDate($arrBgEvent[0]['be_datum_rueckmeldung'], 'Y-m-d', 'd.m.Y');
  }
  $strRueck = '';
  $strRueck.= '
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="icon wb-calendar" aria-hidden="true"></i>
                    </span>
                    <input name="dr_' .$intBeId .'" id="dr_' .$intBeId .'" type="text" class="form-control dp" data-plugin="datepicker" value="' .$strValueRueckmeldung .'" data-date-format="dd.mm.yyyy" style="width: 120px;">
                  </div>
  ';
  
  $strFristDate = '';
  if (($arrBgEvent[0]['be_befristet'] != '') && ($arrBgEvent[0]['be_befristet'] != '0000-00-00')) {
    $strFristDate = strConvertDate($arrBgEvent[0]['be_befristet'], 'Y-m-d', 'd.m.Y');
  }
  
  $strBefristet = '';
  $strBefristet.= '
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="icon wb-calendar" aria-hidden="true"></i>
                    </span>
                    <input name="be_' .$intBeId .'" id="be_' .$intBeId .'" type="text" class="form-control dp" data-plugin="datepicker" value="' .$strFristDate .'" data-date-format="dd.mm.yyyy" style="width: 120px;">
                  </div>
  ';
  
  $strErgebnis = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_ergebnis');

  if ($boolUiOld === false) {
    $arrSelValues = array('OK' => 'OK');
  }

  $strErgebnis.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_ergebnis'], 'select', 'er_' .$intBeId, false, ' class=""');

  $strStundung = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_stundung');
  $strStundung.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_stundung'], 'select', 'st_' .$intBeId, true, ' class=""');

  $strMeldepflicht = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_meldepflicht');
  $strMeldepflicht.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_meldepflicht'], 'select', 'me_' .$intBeId, false, ' class=""');

    
  $strModal = '
                              <div class="modal-header bg-blue-grey-600">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">x</span>
                                </button>
                                <h4 class="modal-title">OK erfassen</h4>
                              </div>
                              <div class="modal-datail bg-cyan-200">
                              
                                <table>

                                  <col width="150"><!-- 860 -->
                                  <col width="530">
                                  <col width="95">
                                  <col width="85">

                                  <thead>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>Berufsgenossenschaft: </td>
                                      <td>' .$arrBg[0]['ac_name'] .'</td>
                                      <td>Vorgangs-Id: </td>
                                      <td>' .$arrBgEvent[0]['be_name'] .'</td>
                                    </td>
                                    <tr>
                                      <td>Meldestelle: </td>
                                      <td>' .$arrMeldestelle[0]['ac_name'] .'</td>
                                      <td>Beitragsjahr: </td>
                                      <td>' .$arrBgEvent[0]['be_jahr'] .'</td>
                                    </td>
                                    <tr>
                                      <td>Unternehmensnummer: </td>
                                      <td>' .$strUnterNummer .'</td>
                                      <td></td>
                                      <td></td>
                                    </td>
                                  </tbody>
                                </table>
                                
                              </div>

                              <form>
                                <input type="hidden" name="beid_ok" id="beid_ok" value="' .$intBeId .'">

                              <div class="modal-body">

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="dr_' .$intBeId .'">Rückmeldung am:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strRueck .'
                                  </div>
                                </div>

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="be_' .$intBeId .'">Befristet bis:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strBefristet .'
                                  </div>
                                </div>

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="er_' .$intBeId .'">Ergebnis:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strErgebnis .'
                                  </div>
                                </div>


                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="st_' .$intBeId .'">Stundung / Ratenzahlung:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strStundung .'
                                  </div>
                                </div>

                                <div class="row bsp">
                                  <div class="col-lg-3">
                                    <label for="me_' .$intBeId .'">Meldepflicht erfüllt:</label>
                                  </div>
                                  <div class="col-lg-9">
                                    ' .$strMeldepflicht .'
                                  </div>
                                </div>
 
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                                <button type="button" class="btn btn-primary right" id="Erfassen_ok">Erfassen</button>
                              </div>

                              </form>


';

  echo $strModal;
  exit;    

}

?>