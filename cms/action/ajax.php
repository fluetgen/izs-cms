<?php

session_start();
error_reporting(E_ALL & ~E_NOTICE);
ini_set("display_errors", 1); 

if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
  define('APP_PATH', '/data/www/sites/test.izs.de/html/');
} else {
  define('APP_PATH', '/data/www/sites/www.izs.de/html/');
}

//CLASSES
require_once(APP_PATH .'cms/classes/class.user.php');

require_once(APP_PATH .'cms/classes/class.premiumpayer.php');
require_once(APP_PATH .'cms/classes/class.contact.php');
require_once(APP_PATH .'cms/classes/class.document.php');
require_once(APP_PATH .'cms/classes/class.group.php');
require_once(APP_PATH .'cms/classes/class.account.php');

require_once(APP_PATH .'assets/classes/class.database.php');

require_once(APP_PATH .'cms/classes/class.stream.php');
require_once(APP_PATH .'cms/classes/class.multimail.php');

require_once(APP_PATH .'cms/classes/class.request.php');
require_once(APP_PATH .'cms/classes/class.sendrequest.php');

require_once(APP_PATH .'cms/functions.inc.php');

require_once(APP_PATH .'cms/classes/class.bgevent.php');

require_once(APP_PATH .'cms/classes/class.bg.php');
require_once(APP_PATH .'cms/classes/class.informationprovider.php');

require_once(APP_PATH .'cms/inc/refactor.inc.php');

require_once(APP_PATH .'cms/classes/class.template.php');

//FUNCTIONS

function multi_attach_mail_bg ($to, $files, $subject, $message, $message_html, $cc = '', $bcc = '', $strFrom) {
  global $boolDebug;

  $to = str_replace(';', ',', $to);

  $cc = str_replace(';', ',', $cc);
  $bcc = str_replace(';', ',', $bcc);

  if (strstr($_SERVER['SERVER_NAME'], 'test.') !== false) {
    $subject.= ': ' .$to;
    //$to = 'system@izs-institut.de, km@izs-institut.de';
    //$to = 'f.luetgen@gmx.de';
  }
  
  //$to = 'system@izs-institut.de';
    
  $message_m = '';
  
  $from = '=?UTF-8?B?' .base64_encode('IZS Institut für Zahlungssicherheit') .'?= <' .$strFrom .'>';
  $subject = '=?UTF-8?B?' .base64_encode($subject) .'?='; 

  $headers  = "";
  $headers .= "From: $from";

  if ($cc != '') {
    $headers .= "\n" .'Cc: ' .$cc;
  }
  
  if ($bcc != '') {
    $headers .= "\n" .'Bcc: ' .$bcc; //f.luetgen@gmx.de, 
  }
  
  $strStyleBlack = 'font-size:9pt;color:#2c2d2f;font-family:\'Roboto\',\'Arial\',\'Helvetica\'';
  $strStyleOrange = 'font-size:9pt;color:#f15324;font-family:\'Roboto\',\'Arial\',\'Helvetica\'';
  
  //$signatur = '<p style="font-family:\'Roboto\'">Ihr IZS Service Team</p>' .chr(10);
  $signatur = '';
  $signatur.= '<p><img src="cid:part2.AF836471.09F8779E@izs-institut.de" name="IZS-Logo.png" alt="IZS Logo" style="width: 100px; height: 36px;"></p>' .chr(10);
  //$signatur.= '<p style="font-size: 12pt;color:\'red\';font-family:\'Helvetica\',\'Arial\',\'Calibri\',\'sans-serif\'">+++ Achtung neue Adresse ab 01.02.2022 +++</p>' .chr(10);
  $signatur.= '<p style="' .$strStyleBlack .'"><b>IZS - Institut f&uuml;r Zahlungssicherheit GmbH</b><br>' .chr(10);
  $signatur.= 'W&uuml;rmtalstra&szlig;e 20a &bull; 81375 M&uuml;nchen<br />' .chr(10);
  $signatur.= 'Telefon: +49 (0) 89 122 237 770 &bull; E-Mail: ' .chr(10);
  $signatur.= '<a href="mailto:' .$strFrom .'" style="' .$strStyleOrange .'">' .$strFrom .'</a></p>' .chr(10);


  // boundary
  $strBound = md5(time());
  $strBoundery01 = $strBound .'01';
  $strBoundery02 = $strBound .'02';
  $strBoundery03 = $strBound .'03';

  // headers for attachment
  $headers.= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"" .$strBoundery01 ."\"" ."\n\n";

  $message_m.= "\n" .'This is a multi-part message in MIME format.' ."\n";
  $message_m.= '--' .$strBoundery01 ."\n";
  $message_m.= 'Content-Type: multipart/alternative;' ."\n";
  $message_m.= ' boundary="' .$strBoundery02 .'"' ."\n\n";

  $message_m.= '--' .$strBoundery02 ."\n";
  $message_m.= 'Content-Type: text/plain; charset=UTF-8' ."\n";
  $message_m.= 'Content-Transfer-Encoding: 8bit' ."\n\n";

  $message_m.= $message .strip_tags($signatur) ."\n\n";

  $message_m.= '--' .$strBoundery02 ."\n";
  $message_m.= 'Content-Type: multipart/related;' ."\n";
  $message_m.= ' boundary="' .$strBoundery03 .'"' ."\n\n";

  $message_m.= '--' .$strBoundery03 ."\n";
  $message_m.= 'Content-Type: text/html; charset=UTF-8' ."\n";
  $message_m.= 'Content-Transfer-Encoding: 8bit' ."\n\n";

  $message_html = str_replace('auskunft@izs-institut.de' ,'<a href="mailto:auskunft@izs-institut.de">auskunft@izs-institut.de</a>', $message_html);
  
  $message_html = '
  <html>
  <head>
      <meta charset="utf-8">
      <title></title>
      <style>
          * { font-size:11pt; font-family:"Roboto","Helvetica","Arial","Calibri","sans-serif"; color: #2c2d2f; line-height: 1.15em; }
          a { color: #f15324; }
      </style>
  </head>
  <body style="background-color: #ffffff;">
      <div>
      ' .wordwrap($message_html, 70) .'
      </div>
      ' .wordwrap($signatur, 70) .'
  </body>
  </html>';

  if ($_SERVER['REMOTE_ADDR'] == '46.244.179.250') {
    //echo $message_html; die();
  } 

  $message_m.= $message_html ."\n\n";

  $message_m.= '--' .$strBoundery03 ."\n";

  $message_m.= 'Content-Type: image/png;' ."\n";
  $message_m.= ' name="IZS-Logo.png"' ."\n";
  $message_m.= 'Content-Transfer-Encoding: base64' ."\n";
  $message_m.= 'Content-ID: <part2.AF836471.09F8779E@izs-institut.de>' ."\n";
  $message_m.= 'Content-Disposition: inline;' ."\n";
  $message_m.= ' filename="IZS-Logo.png"' ."\n\n";

$strBase64Logo = 'iVBORw0KGgoAAAANSUhEUgAAAGQAAAAkCAYAAAB/up84AAAX23pUWHRSYXcgcHJvZmlsZSB0eXBl
IGV4aWYAAHjarZpnkiS3doX/YxVaAtyFWQ5shHag5es7qOohh6SeniI0TbapykwA1xwDlDv/9Z/X
/Qf/rFhx2WorvRTPv9xzj4Nfmv/8m+978Pl9f/9K/r4Xfn/dxZ83Ii8lfqbPnz1+r/95Pfx6wOfH
4Df704P6d/Qwf39j/Dy//eVB34GSZqTB9vdB4/ugFD9vhO8MZ/kOkHj1H2ZUequ/LW19h97re2H7
4/9afr/5r3/nSlS3aaTo4kka9KSUvjNL+j+nwc/E95AaF4ZU+N3e95zK92EE6me27s8j/PrXvb9H
Q/59SX/N4s9v7q9ptPXPWczne0X6S/C/y3X88o9vBPvnbL2U/Hng/DOj+Psb8/zk7e/Bv3e3+9bM
IkYuhLp8S/PFKPw8hgup5ZzebYWvyv/G7/V9db4aBbYYavtF3U9+7yGSretCDjuMcMN5P1dYTDHH
Eys/Y1wxvddaqrHHlZS/rK9wY0097dTI8YrHkeKc4q+5hDduf+Ot0Bh5By6NgYcFbvkfv9y/evP/
8uXuVb5DUDBJffgkOKqLmIYyp+9cRQrC/ebNXoB/vr7p938qLHVV5jKFubHA4efnEdPCH7WVXp4T
1xk/PzkOru7vAwgRYxuTCYkM+BKShRJ8jbGGQBwbCRrMnP6JkwwEs7iZZMwplehqpJUYm3tqeNdG
iyXqZUCORKi/KrnpaZCsnI36qblRQ8OSZTPwsVpz1m2UVDJoWUotQstRU83Vaqm1ttrraKnlZq20
2lrrbfTYE2BqvfTaW+99jOgGAw2eNbh+8MqMM808bZZZZ5t9jkX5rLxslVVXW32NHXfa4Mcuu+62
+x4nuAOCnHzslFNPO/2MS63ddPO1W2697fY7fmXtm9W/ff0fsha+WYsvU7qu/soar7pafx4RBCem
nJGxmAMZr8oABR2VM99CzlGZU85gB5rCIpM05cbtoIyRwnxCtBt+5e6PzP1beXPW/q28xf8tc06p
+//InCN1f8/bP2Rti23Wy9inCxVTn+g+3j9tuDgpc+Dp+NnLLrVNq9uASZZOC3ibs8M6I/bOfHYp
dDYDW6yjr5NrsL5tzOBGDaf0AMJOv5k+waOd7n0gmUujdu659ADrjazlnLh3zbf31av5syLXj2PO
JvXXNaOT+s3jgph+ZqDvEphbydOuc3Sv+Ox9C3msYZJF5sfS5iozx6JglxrXquk2I1EAbuWKTMxp
7UCcWl5jND28j7lYaEpbPyJJJLRlpHjyHY4iYWYgclk370xYGnBeStuUoGfdk3SShji5h8DWNCtp
mX2sNggrpVvGWdNRdFZDX3XeYNP8aCeR/US674zL2l6JpF5envX20GyMK0wqlw6YIZ1hdabt7sgb
sGO2fg5wbVqOLN9yPy3tMhdppGgokRzJZt2pUCGx9hE2CxwxEoY1miuhLYgLQiLQdfWipeY41gAw
z8yNedVZ+qA0vB3fz7vATp+dQvdtXQuLFhk702CRQiS5d21aJJ2a91WuLdw0drDdb1tzhVrDKHbm
pmJqIar01ujFrGWXFo/1tMVkZrnRGiHN1ogKNUtTED/bO9BacY60x6gWafo4AIcRG/3Vpn5xY7YN
jY+MALtctcfdBQC/G9EXO9Mp8xJ88yrSfsPs907me6n+eulzosI7LpH6dG89NqlA7tVSit763v29
l78/d1vIVGVNZVIHO5RLauc87tzemvGsvLZImx6ZnjzR6edciJIQbGa9j4Fbq266iw76NZOfibjf
Z/JrFX+dxx+r+N5Jsf52q/sXEUilM/vW1tlAIFGlG9YqUZroWlqIabMX8CZ5TA+2ssY5xzwZLjNl
JDOoQu1bY+p9WaI6bh28CvcASqfxOA0tOU4B8TxHYdBDoLCa4bOYkj9BNaKowKokavILsbMUrUCX
UTqADF/hcrs/w3ELAMdExiqDe/imRrzlCTJWZNBDFrox0GwHlX5DH5QzuFFApZnU2J1eC6fluuKF
ElhGPPfsdAoAUZmYnaXGkxrv6YJHm163ui6dQnSz6nrGdLabtFUUuLbB6OcCNpGgr3xR7lT8qoiR
5ddOxBQCSguopPJpUcAFaG1A8jnZ9bN84a0EioAV54yVjNImtLZz7vQ409evq5O0C/0uZgeXbkbG
FdRknfA4gHoI0tqeobdDCom6KoR2bLskYy4pgx7xxkP7v1rde6Y4+oSWmXNbVCws4pk9kMGAqyoA
RAhE8srP8OVQV0TwlWABaUDpCYQPKAp4odWJJBiIGM3EqsgdLT3wIA3gZvO7wgPXg4QEcRtrApIg
oAjEU24XVoXHyHnpEKlv7hFProB5JU4FRqpkhDcTtX2pMtQPpQIVjX3brIAlsO8XwYGEERTWkc+N
rF2AlvBDeFyPNqLUN9zUSG4sgB+xK/3A4XfTsjHVUDf1eMDyYQT10j6AtSMFQAFKuROxQzKZ2Wlx
QLi9VKQ+FISOuSGyeOCfEre5MhzXRXmxIYVmHdWFNxf4YJvtWmanM66NfgH11WB+CjKQg5PnOAPk
J6Jz70jYM1VXGaaqSR1TDkxmtiBOqlT7IadAd39EcWEXW4uXVoa3cqMtIhD/0k9JalSo+XqH6AhU
JlHxjRaBmOsyJjvXhn+4E98uEint7A5xdjuw9YQ9EOK9RGUZ3qoO14JhJjcshssNxTM6jFdFktwu
vDkn4XmGQBRJ7fsGKw7W46xE80Hk60zoaJ4bIAABKs8JrJrSS4IHerDe3YGJqRsEjCwfkRDeX/X7
F1IHzB7bAyigeK4wVtbSyxZvlUtCG3A2rZwhPvPrGaE2cIOIq01z33WkWigOB3SCGbMg5TZsRpl3
QGFF/r6gZZkI2xkRPzuxhN0RcJOOSKk/3qsT7YVCHG5nxqTJ0CCEif8g8AV60zrnMN6GeUcVd/vT
pRzPVqxCYXET+YWxrcGv4pjgXplwNoAOJOin5yVs64uIBGEuz1QjISRAbv1m+gWZMMYmGRnZPJIb
Jwfa5k6jxCOk0zPQW28MXxL6898gzIN8dAJFvv2TiBdaueCRJ+yMUOgZtBHYIv6MaGHgZ6GMOmo6
kVXPSrHQ9FHP9OyjiXzqI8n2HuTJy2G96yCiEHYsTih0xTVkDLHA7PEc1GrWBgAZRTUPXMDIp/do
g66cqJGNWElUu899ZoRawdISoVMR/rPmgXtBkUzwdO1I0m+XNAcwM9EAi2hmn48D37gDGSonCjAm
a0iSClMi1yEpii1Jcq2KTjMihj6uogOKGvGSnmhZubqKShCrhlFXPUTGFoCXykXlQSC7edZAjS2Q
vJSAsGxZyz7SiIhQCi3BPhQk1gFuzGihLsIAa+2ineE3YIfZEOFLMcWLucoTMQoRr8j/GwqISH9a
i3ZyBcxTJ86hfoZH0t4Zsd8N5HilqHdmJQ9klTTQsKhA2jIyLGB/ZBOzd4KVOZ43gps7lQjzzwBE
FnoIDIbN6kBn83SatRvi3IirBIvYvhAjaAChFQApkBzMixGGxfNPspgK8C63T2zQNZTmYxg7sS+t
kGaBvmIt0BJQ170TAIHkACB+4OAreqYjaV9aY4WFCYGXjyZnITWwgxlvWke2E7zlkVn8gqyh4xHj
EI/fPKuR50ymQTQ4jOCBik17TqHmgNJA3DRyj16sqjUMT6CWIDBmBD/AFlTJmZQbmgv3wvIacgEi
xypDOqdtvquwUcd0AYhEMFB83ngWN1W3oqa4cBSEAToH/jIoPiRR9RtuXPeE2vCsldCKaAueLICx
RyHCxl4/3JFZAZTgYMrRiPEF8MBk1njQ9FgY4ofJQPraMCHajLQedY8XH8jCCXSTtYGoBr2WABxK
G1Qc33xoqIvIwHQCvfUsAUHPYHqhnPrHWtSLxI4NMbMdC8aE4tn3iNS7hB+ln8kJw4EXR8RHNwCp
OCrwFvzWik3btQKqq62OHh34hF6CT2E/8PMCkxVGvX1DGtLV6sAnUk0gRL/LMAMjmMvR6MgMLTGb
p0aQYXRlhakYgFa3jkez8TCJZiCM9/j0QSjKYgtZ8PJUp0cXdEQYLIIigNtb2RRT26yyZBpAmyiI
IxAzEY2ONADFiGSG+7kVtZzAXnADVu0J4EIfSVq86iCHuNAGHapCkO0q+UvVb16HW5CGVZqP27pP
8ouIjTMi3gKGccwqtxB9E+Ha8+BMCTTxYA/wmAoSRfag6wH8kWTsDuZwBdSA/DNBigM1Quiescc4
0JfI2S0+wf+tQN2ACsyBatK26UDbJhQbS0CH0lrohyUhBK9NTCEcSRVhFkfLViA9JAhsi7NlSGod
6idqQ0WVJriNDUBooHrz0D4Mo/biKggWRo94axZJ1NLjRlCT8RNhw4aCrQW4PhWGID7zJ39qT8Rl
JFYwbSWHBhppk8dwK7QrtQdvo7wE9vBqfvsOsXL3RVAQ22jgBEWKeZechn8cjihJwhbJCGKNMpC8
6GmjE4FmVgQ5dnQuhZCRKHWJzKAH9FMBsfHMHWCVg7TesfA4zJFhvkX4k7xDmjDnyiiEZYSX+aFo
F7oTQsgFV8gYmUVtyndOhwwwSIyVg4/wEMkM1CHIMWl1ckXFGihCu44YOu+B3h8fADKQ8AONY5Vd
DmJwRB8mNCF9A0zmA2ijqiPBNCvtRVdgTmrUHpcHRyw/IdqQk22JSZNDp1ZUKVmhe7Sb0wLVhFFo
6EzoA84LnyMCkC/7kfSHaBhYbKAn1AYM1esW7JK3aaMsPTWxpLhQkGhdsF27ejOQIER0zcps3AW3
ZUJy7MlJJmKq0c2AnUCAIn0MciEKlD7cw9I6GKsNxAJtSUQiLen0Xk2AQKNT4WBIwcD50dGQFd7C
DIGmK9PyTzytqA6lIugURCNqASmOr69oyDkEhbhsTI7huAELOtjdsMs8Hb1AhVCSxBEmRdiAVvjM
WlVDtAqmfT9aoFNJ7BckcZ3aIYPwHOJam/71k3aMWaQeN5r5gn1ocLgNoQcHoiA32K0TAPQHihKe
63K26UpI4bJNO0FV3nQlYH1oQbVNxIlZRINPChtTYXCl0eza+iECucHviOZBr/CFX7OeW+bxOi3D
FhfczrIrIMyURY64Z7RVog3yzgOHgJClzaUMQIiK9NZB2nF0S0KlWdIScvUUKmBuQbOCcOPqkhQ4
ZKYQpefDS2Skr/SCTJosTNCp6BgwpP/yKsrt3+EO7NXjIjHRIyL3j0zEXGnox1nYEyTjU9fvO0jM
+nFUZT6WIQQottscWhvnCUvTt7A8HJ2QySjsEmU8VyMKAhBqh9uJYxHDNHw2QvRivpCAeHGTPsJJ
4yi0Rz0WfgiIRm91VBBMgNwCUg8pZbraoztofq452q6Mkf7YKiQP1O4D4KF4WWshn0Chdl9kTLir
y7QwPXCZbxSTUN8jvrSZGNAeMgJ9TKyoLc0I0ytd3cgoOboS+sgIzakCJU0tJMXZoL1dLrzMihIe
3q/tO+ucyRGJzIKxnSAEmLUedtB2L8ZNCI+nhnVjlMDHqhEl1k2DVu1bBwkND68Jj/CMh8LlAonN
UIeOZ8FHRDGS63nui5VryGdIbjOnSq7W0K7KYTE+IGuWNkmPTAWMSl5o4YErQQGipDsSm3oEoyFz
eumUgCrTiSF5pDcJVmu4EkSVA38yLY4AqaHitJ9BPED8hZUgkL0wQ0grWqLpnIulUbwj0YfA9tJ+
HotNzYEWG/n2iuE2xunnjgjT5rWackw9zapzli6mxUslTQYQJZHyTUwcrN6OPqSM4Gw5FBCI9r10
KEOGA+fAlMgQkp7QTmjvCvdw3Q1FtrRQJAQZwEquA9I6nkfvMSmGhJkjCAfk4H9P/nhUmeagwwzK
F/64iNF4jPcQmLdO1VECjOC7/kknLqpARRoI8Us1+wtFDu15SioRYnAXyN0yiBAJYhjigFmbI1IB
2NsGDGw5A9BDkBEAMLxbQUt6HQtYgQUlATA6qww671iHAJv2ZtI5Tvv6J9eEn1z9HWjgeesUnOnj
ClwFJW9odgJGgFTV0VUQWQ5ZGMURy5WX00mGzkslw7K2PVFpCGUYpooIoFOkNZWGFmIFiJ2LrAn1
AbswlWUCw7s5uBrUwrpTSPBCXIAg8QlzkR+yNUVfh/lTsCgabWdMPLsHxOA6qIWXAAjvKM1z6Qaq
6Szt/R2ooXXsqCQ0UAEaYD8RbDiqO5A6F3gHXwB7oCrIZlAPhvRDbq6VbNAapQXkBY8lpUy6A/A9
6OCwp5Te5wgaIiNoa5UUzs9G9FIciosIdXzt251WYeS3aaEpYysuxUbFYvxQ6aUHAAu0wYxVnfgQ
dV44AOax5mhAnnoAXNAnZbQyHYHPTQ2YOZE5bcz/4FFoBB0DsgIZFBmYhpuasIqOHV2Tb8/4U2rk
ihCZAT4goLLBi9ZkLATuFgK0ESGnnnjIFXI9jdil8hPuqG81V1wEE8OLpkqiF2wXLZ8A/0EZ02Lc
lck7PuVIS2iDiuFITUT3IF4cChEnjxpo4DlQjHhFQGS0kyyi2nPL+oMMdWbSA5IXHB7emu6Adluu
EjnNUdAGJ1x9WgDxnwTquOvmVR+FnC5tplDE/vDW8UOntNQiuPMxrRtGjcEctTh1SLWJM9WHnAax
aZIC1h8PNMrzQO8Tgs1DR3RY6IoT6gNIxTS9ze2UXWwSBQTRmxnYmbU9gg+pgi90JoSEDR+Zqs06
psJfTrwVblKyNyCfogBpwbQZlauN5qrzmdYw8XeGXFEvlNShPp+GwwILmEw7PUkAg6A3HbYhUG9b
3hWEEUqLd/dodlkjVXl2M1RR2XNh/uF60OogyBR+aJdF3kGvBqBA2utSJu4zAXy++Gy33aQ7veGt
lwKgUwhPSXuVMfYGfYyw1kY9FgwaSSAunbWL00aoR0tHOjxHkC9Sq/7oLAWvNIGz3TD8A7BHNDWJ
PoN9cWCFh9G5SN0GgjqmHEkBur8gfGnqvvQJjA5Ll/INFOLmE6pvoLD7iVaBPmc3ORqQ3tHPT6oh
Ime4iC67+pCDvdaVIEYFSzu9bSWk5Of1lbmg4mMS+vU1vLtJHxbQ1dpRAgZxL7Q3lEW4NvJTJ3mg
Cc5w5YFDIlMdJYsKAAYyontDXZcYgSD+9VjMo8lLC7yTlHLv36rQydtPXVhBYmHjyq4KSNOJE9ap
Ogs4egTjRJka8Qna9kqSPuNmFSVXwsDwZZfaFQegZXDLuEBtDkgQFSbtgpAMkQ5GL6TwbESpUulv
W+4dYkOSKK+mj0mg2vqI68nfjI1DHbAMWtqyM+AoInG0/4qkkuhv3DfxSBfBxmVwMpVHP2iLHdg4
QXum0iTUECIOgML2ovznPqdnHZoQqVwDt0fqpsOVrKcwNrBTIUdoEH4meJ89GhgM4L5YLsXRqYu6
TjehzjSRIehWmQ8dAW2UIPCZoUadUODiWiWyRymDKlICKSFDbiZG2qNEESIQgnbNS0bm6fBFB33q
ainD8wR+ifroCUDdI9qS6WkPeUf0bqQnHVKBwrz6EEVA5b+cY7KI6oXioVGpCJaLpnqnJDovQy5g
4dot2KOpz0FEHxwFdoSWq1CPDYXWLrDeekSs6wSLMmhPzoGUF4Ssh9LdK2jHNOs4kHpE9UHZOPmI
7sB56QMkOtZZI79TvYAIFFSjkTEWrUnTRKbyjqXiFE0VvAfSuYztcA/AuLRxfRoUPaGdnXH0CZuO
4abvdSoEQC4QlYuFnovV07+5LKgn6zDPLazt+bi1yLPBjRWqBIYUlJCR4hEvehhlq3LbFlBQHTCn
tgqIh0SvQyld7DTiAdyKO7X83IlsZx3yqdoq3ThDYFEfE0Ag0Xpv15uixvofBAfF6YD1tFFFXTtV
yNmj/bmjA26zrL0VjWOGcIFBZCQAPu3NY4rwbWZHe3ZoaTd1MmhURGO+qm7MC45Mx1dkX66DaoTd
9fk9ECPLxSoJaRZ6/7YOWtJz12V4vQnPsOQoAnJNo43aKTN45kDooSZ9wLBpM70iE7SRTyDmYYi7
+sAnQkTOMrB4sKf6cOju8t6BLoMfCBWYuOiTQjXRdrTVnPDc7RD+Ql8dSK7p85Nhe+dpQqIG3W11
gmESuz6MA4C9TQIdaH8/G/Avf7q/v6HqQNyKw7cErlSRTArT3M8dZBAKQMja8AmQwQ25TOlsABw7
0H1/Z27x7WkitloB4ojefh8fkpaZsqLQJCrh6jRVuxcIVAIcADavDdrd3X8DEknf+yB1xRIAAAGE
aUNDUElDQyBwcm9maWxlAAB4nH2RPUjDQBzFX1OlUiod7FDEIUPrZEFUxFGrUIQKoVZo1cHk0i9o
0pKkuDgKrgUHPxarDi7Oujq4CoLgB4izg5Oii5T4v6TQIsaD4368u/e4ewcIrSrTzL5xQNMtI5NK
irn8qhh4hR9hBBFHVGZmfU6S0vAcX/fw8fUuwbO8z/05BtWCyQCfSDzL6oZFvEE8vWnVOe8TR1hZ
VonPiccMuiDxI9cVl984lxwWeGbEyGbmiSPEYqmHlR5mZUMjniKOqZpO+ULOZZXzFmet2mCde/IX
hgr6yjLXaY4ghUUsQYIIBQ1UUIWFBK06KSYytJ/08A87folcCrkqYORYQA0aZMcP/ge/uzWLkxNu
UigJ9L/Y9kccCOwC7aZtfx/bdvsE8D8DV3rXX2sBM5+kN7ta7AgIbwMX111N2QMud4DoU102ZEfy
0xSKReD9jL4pDwzdAsE1t7fOPk4fgCx1lb4BDg6B0RJlr3u8e6C3t3/PdPr7AVKUcpqzJLNyAAAN
HGlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0w
TXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRh
LyIgeDp4bXB0az0iWE1QIENvcmUgNC40LjAtRXhpdjIiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJo
dHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2Ny
aXB0aW9uIHJkZjphYm91dD0iIgogICAgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20v
eGFwLzEuMC9tbS8iCiAgICB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4w
L3NUeXBlL1Jlc291cmNlRXZlbnQjIgogICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9l
bGVtZW50cy8xLjEvIgogICAgeG1sbnM6R0lNUD0iaHR0cDovL3d3dy5naW1wLm9yZy94bXAvIgog
ICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iCiAgICB4bWxuczp4
bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iCiAgIHhtcE1NOkRvY3VtZW50SUQ9Imdp
bXA6ZG9jaWQ6Z2ltcDo0YTE4NDg0Ny0wMDcwLTQ0NzktYmIzOC01ZTMwZjg2M2M2YWIiCiAgIHht
cE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6YTlmMjAzMTctYWM0Yy00MTVlLWE3MjgtZTZmZGEwMjdm
MTUzIgogICB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6OTdmZThiYWQtODNhMy00
YmRkLTg3NjQtMzNiN2NiMTEwOTMxIgogICBkYzpGb3JtYXQ9ImltYWdlL3BuZyIKICAgR0lNUDpB
UEk9IjIuMCIKICAgR0lNUDpQbGF0Zm9ybT0iTWFjIE9TIgogICBHSU1QOlRpbWVTdGFtcD0iMTcx
MDc3NzQwNzE3NDc1OSIKICAgR0lNUDpWZXJzaW9uPSIyLjEwLjMwIgogICB0aWZmOk9yaWVudGF0
aW9uPSIxIgogICB4bXA6Q3JlYXRvclRvb2w9IkdJTVAgMi4xMCI+CiAgIDx4bXBNTTpIaXN0b3J5
PgogICAgPHJkZjpTZXE+CiAgICAgPHJkZjpsaQogICAgICBzdEV2dDphY3Rpb249InNhdmVkIgog
ICAgICBzdEV2dDpjaGFuZ2VkPSIvIgogICAgICBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOmFh
MDg4ZjQ3LTI5YjctNDc3ZC1hMmMzLTM2ODYwZDQ4ODgzNyIKICAgICAgc3RFdnQ6c29mdHdhcmVB
Z2VudD0iR2ltcCAyLjEwIChNYWMgT1MpIgogICAgICBzdEV2dDp3aGVuPSIyMDI0LTAzLTE4VDE2
OjU2OjQ3KzAxOjAwIi8+CiAgICA8L3JkZjpTZXE+CiAgIDwveG1wTU06SGlzdG9yeT4KICA8L3Jk
ZjpEZXNjcmlwdGlvbj4KIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CiAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAKPD94cGFja2V0IGVuZD0idyI/Pnj/
hP0AAAAGYktHRAD/AP8A/6C9p5MAAAAJcEhZcwAALhgAAC4YASqqJyAAAAAHdElNRQfoAxIPOC88
YRAUAAAQX0lEQVRo3u1aeXhURbY/dZe+vaW7k06HkJ1EIKACgoDK4gKD6KgjoEgYWSSyb0FFBERE
eCCLARxFVoFhZBQiPBAZEf1AZPA5CoggWxYIZO0lvXffter9kXSbmG4IYXvf+6i/0nXrnlSd31l+
59xCcA3D98YwljBMBrFX9AFR6AGy0AGwlAqKHEtkPyB1rAyAKt3pyYWreqUeL2XxQR2jPWxiDJ6l
D83GcGdcdaAmAZE/Q09Ki54gHtto4rc/BFjUACFUdKkUnOrbo2huhpRAI9qnobjtBla/tpU26dy8
btOVO2pvJiCBhVMZxVHdB9suzIVATTcgmG6yZLXR9vHzXfft4jxDMBCaQbQ3ltFvTlUnLl7Rc175
HdVfIyC+t0bH4soLc4m7fCwooro5wpWU9qtyn0pMccjeZ+qmiIZSFcWzpimtdCn7F3R//Y63/GHQ
kcEYk44vnf6EuMuHAFHYZgsX5ZaWtNavHdNIf5KIYgAAJBPFzGNhQEAOerrnPnz8+KYjd3JLvdEo
D3jnT2qFL/22g3irHgMg6HqEE96Z0vNwcVa8KnYBBZQcmheJrKuWnMuqhZrJr/wwn7kDQ5SQ5X17
bBwpPb2TeCp6N/l1ipUASBAAJCAKBoqhgQALoGgAKwwyJO/699Bnhq8iZ/a6FX+PBh6EKDFZZZmY
GZO64Z2ur5Fr3fz012dQFRXlBoHnqSbtlqJAp9P5Nm3cKIbmXnxxmI7ng1yzrJmiiVar9WzatPGK
oXf4iJEmURQ6ejyeLm63x6zRaCSO44oMMYbjiiKXPfLoI54J48cTAICwdfqXvcoq544vIJ6KXlff
CRtEnP4McLq9oI39CWljSihTvAOc1QIxJ+qJw2oh7upWIMvdgOHa9tx/gtna37zIh4MFCsHhfKQQ
rKoSHUsoH3USAH68VoVUVlZkVldbd/PBoL5p9oMgOTn5PQBYCQAwbtx4qry8PN/pdD7RHEA4jpM1
GnVOtL1PmDhJXVFeMay4uDjP6/VlY4wpAAC32wMAQBiGUbRazbmC7QX7cnKGbjbHm0+GAcHll54g
7qrcKzIvmuORxrQPGeLfp1p3PKybMl+MsKoGAC4BwFEAKBC+2kojlsVZ+ktfO1zu//iUYAPvE4kc
a5dcy8cfmtXvo94LfdeiEEHg9X6/v7XX62ty2MvKuqt1+H1RQEGeT3e5PanNAcRkMgLHcbGRnr0+
Y4b2xIlfV1ZWVo3EGEfaH5JlmfF4vHd7wHu3x+sdoVKxXRgAAN+8cQZccvIdUARVFNMioI07i8xp
s+C+bntiRrwuA+xumhX1HxpyZ2nItxPXBRT+IQykwQb9Ct/NK/tGLjy26sNZnSc0OXQxDCMwDGNn
GCYccjDGeowxW+sQiNA07QEAXPcb+GDAGs5xf7Q3mlZomo4afjDGtCzLYSKkKIooSVJEIzp+7Hhe
tdU2KuQVDMME9Hrd6djY2Is8z7MBfyAzEAymSJJkAgAkCGKM2+3RMQAAxGkdRPzWeyNjQWMU02IH
yrw7L2bO6nKAgmYnrOyYtL01Tm8Vj8WUBskfCF0jeadag9YtAOBuqrxuXbsVajSnevt9flWdwvGF
Cxc/9AcCjwIAqDkumJTUcrBarS4PAZKenn45mjytVrsjOTlpfeR8Qansdvs8q9XWOSRLo9F8lpCQ
0ChcvfDCkIzikgsTQ2CoVGx5Zmbm2E6dOn315uxZCgDA+g0f07t27epx8WLpXkKILmxk3lnDOFxW
OAoIpiJkQQmZ01czmfe8oZ2xPHC9DMIlBZwxtGY/j8WX/vjMj/kMl+h5GgD+0VR506ZNkwGgsP5c
167dvb+nDIQFQTi/a9d/X2xaCBQuKgrev3NHQQPnyV+ej/bv//avdrsjbLQxMTHnW2W0mr148WKp
Udng9/cSRTEp9NtisbxXsH3blwXbt4XXvJw7SunatVsJIURuSHv5QBsIuro2NgkGI3Orj1Db+6ff
CDAAAFb0mEfUFPcNBUhuRJGBMC7Z+9yEQzNpuEWjcbKMHC2PHf2ljd1uXxQKhSoVG2iRYJm+YcO6
iN6GFeUejHHIKECr1R6K2MxQcwQAKEIIEEIQy7LAkICvHxBZ9Qc2QpAxZTvdrstMXd4i4UYqwcSZ
fi4X7UEgJKaRZSl8D61Ka6wjBjd9sCwLDMNYOY5z1CpI7TSb4xqsGTtunO7s2XPLgkE+JZSX4uLi
1j07YOCenTt3RJSr4lQaQgAQAiCEAMuylkjrMjIyHMmSMsdoMsQH/AGP0Wi8xBDe/yCQhgUg0sUf
oxLTJ+vyFgVutBJSNC1Ki3wX3UEiNgJEJnJMTdDdEQAO3ApA1q5Zo0yZOvXVWJPpXQAgZrO5/IMP
/hZ2kwkTJlHFJcVjnE7Xk6G5WJPpWEZGxvwRw1/E0UOfWIjqadThcIyeOGnKdx9+8H6w/rotW7bw
IQoeziEIK/c2cFRGU0O1bDtFv+Bj281QwhlXocwhtiIIDRN7XcBgfTjQ+lYBAgDw/sqVNgCIeFa7
w36/w1EzJ5ScOY5zmS3xr6xft9ZxJZl6ne4gx3FOQRBiAQBsNvtfZPnUkpycoe/8859bbVdrnaTU
S+IYmdPe17+7+cjNUkAsayQcpYrIpAgQSsRy0v+FFsaw4SNiq6qqVvA8H1vHsrDFEr985+cFh672
7sBBA0/FmkxrQ3QbY0w7HI5JxSUl+/s93n/kkJyh5qiAEN6prdcy/5Vu0/pvwsGc58TvJ3S6GQfl
KBYYoKPWGmrEGFed2IhuJxjDR4ykKyoqptfUOB8IzRmNhoMpySnvNeX9oTk5RK1Rv2OxxK+hKCrM
wgKBYEer1bqxpKTkUP/+T74+esyYtAgegkL1hkQZE95l7i19CgLHNhK+9KGbcdhq0QF+wuuuxH0o
RN1W73C73X1qamqmhoiYSqWqSk1NeWX9+rX+psrY88XuQMdOHfPS0lLHaLWasyE1y7ICgUCwfXlF
+eJTp05//+ennp6R+/JoYxgQxBlq/4na9DP7tNYL/KmVgN16UBxP3IzDtjdmMTJRorYqBJCc4zqM
ILcLjOcHv9DSZrWtlCRZWxeqpARL/Lytn3xy4pppfn6+uOeL3ZvS09MfSWqZ+Kper/uNoigCAEAI
gNfrTbt06fKic2fP7Rg69MXMWg9BqAwQJdN3J+9F5PxKID5TbV/A9qB4MOeuG33gyoA1I4iF2MgJ
DWEG6LLbBcZLo3JZu93+Xx6vNztUQ1gs8V+kZ6RvvB6527d9Vv31vn35bdu0eTgtLXWUyWQ6jhCq
A4Ygl9v9WFlZ2dZhw4abKaCZk0gfZ2M7uHuAYr2r3seMOCLVjLrRh7aLrl6EEG3kpA6iltacux1g
jB03HjkcjsFOp+uvoTmNRn2xRYsWr61ZvfqG1GKbN29y7Pli96a2bdv0SUiwzGJZNtwHc7pc3ex2
x1gKqWOO0PelYCAlfRu+riCQSl8SDuW2vlGHXn58A+WR/c9hIBGrcRXFuJM0llO3AxC/33+X1Wpb
LMu1RTJN03xycvIbWz/5x4VI69/LX84sXrLUEE3ewoWLTNGebVi/zjlm9MtL0tJSp4eSPiEE+f3+
oRTKaLmfTrTGAwiNW8TYnQj+X+YHf5ymuhGHPuctesCH+Z5Rm3sU971b8nhvNRiTp0zRXL58Od/n
8yWHqvH4ePOWDh3u/TzaO0eOHBm0Y8fOnx7r03fZc88P7lz/2ZAhOW3/9dVX3z3yaJ8tzz47sM/U
vFeYxg3IF3CCxbLJYIgpDs1JkpRNqbqiIhQTPBa5j0MAFOtAyvPbq0Lp36+LiuYf+1BVKdhnykTR
R+ZWSNbT2m2rei26pd/YJ0+egi5eKB3vdLrCJEan0/2Wmpo6Z97bb8tRAFRXV1fn+v3+Nlar7dWq
yqqP8vLywmHY7fEMdrncHex2+4tWm+0Du90W0VtkRZF0Ol24UJRkmaJUj23ggdFvAEBRFCGxIBa+
CYVbJkln1jabjx5znRvhUfz9ruAdhVm6lH232juqqqu6VFZVzsa49ooTy7L+5OSW0zZt/Lg6KjGp
rLwn4A+Ev6yqNep9K1asCAAATJo82eR0ugYTUmvgMXr9wecHDYxY2TM0pXe73Rm/5yxNWa0rqcwF
oCRMBaU68jcREtSCULgYlxUkCz9MXcA9uPKavuxNODSjx/lA+QKFYFUU71DiWeNyCcENDVdX485j
x40znjx5aiXPC3F1oQonWCwfmIymg70ffjSi8fXr1xcdOHBwhChJ6rp2itMcF7c99Ly0tPQRnufb
1YFLdDrtzmcHDGi0lZkzZ7NHjx6dEQgEw50Jmqa+Doch4ZsnBoJw+lMgwhWu/TAY2FaHgU2cpUp9
6ghqffV6IefbKffaJefnApGikgMjo/suS5P05Iqe86+pmfnZts+pjz5atdRut3cJU2eKugdjbK77
WyGE/EwI4WuVp5Luv79L7prVqy/NX7AAHT92/O3CouI3ye+3MAlN0zaMcTBq2YoQAoAEjGvvBpjN
cbtnzpw5oP/j/fCXX+6llixdutPhqHkmlItYlq00mYw/qFSqw4qCLyAEAiEkMRgIDvZ6vX2VcEtf
5UlPT3v092SjTtkDim8zSEW50a//yBRIhb1BrvyXWFL9pfBNv1VInfo/qp4bpEir3/pxSdKvnsKN
ApGi1jMsYqwWVVzetYIBAFBWdlkVCPh7AkC3MA/Bv0feujDUvV4Xlqg5LhEALgEhiOO47qThlVik
KErCFb2OkHohh5b1ev3G/o/3wwAAFZWVNE3TAYqiCMYYEUKQKIpJVqttEEJoEEVRMgBgQgiLMUb1
5IgWi+XNrKysX8KAcD3XicL3L88Cv78dyBU9rhwLfDEgnR8CEvsMkarPCvse3gO0/jAYsk9SKqMN
O45gxLWEDwlvS1e3GGBWDJ39SvC+gMJnApBEmci6LAQtsgC3qFTFT9OoE040JyRptToMtR+7SBMi
FCCEiKwoEgAAz/MgiqLY1HcjiePU3Jm727c/8OWeLwAAYPTLudKIkS+NMRhivrdabSP9/sB9iqLQ
AIAIIaAoyh/ZFtHptBdTUlLe0un0ny5bugQ38gThwNOtIFhUANjZuel7owAQwwPQAaBiqwBwMVCa
KkCcG5SgFwFCQCEdBlU8JmIaAeUuwF6DosqaQ4wdVhu7Lmz2ldLHH+/frryiIqFJnWZTrJTdLvvo
urVrBACAnJyc7JOnTrdoHh4A7du3r9r22acRC9kXhuToGIZp63K5HnO73Z0QoEyGYRIQhRhRFGso
iiqM0ev3GwyG3dnZbW1z584lAFGu/AgHn8uAYMkmUCp7X+/txcj4mbzAJOWBoePfuQeWy/D/fMyc
NYsuLyvnCCAGIUCiKMpJSUlC/nvL5MYEJ1qT798TzeA/Mx+kklFAeO7GbA0RoBPPgKbdFDBkH+A6
v3XnXm9TAQEAEH6ezYL3XH/gz78FiqMzgEI1GwikcwKbvBY0Wcu5Xh9b76i+GYCEhvjTHCPxnf8L
8EUvAXZ2r/UYTF0VBEAK0AllQBs+Q1zqeqB1Jaqea+94xfUCEgbmu+EqAJIFQkVfgoMPARHuAVDS
gPgNQIIAlEUCIpQBpSsEoH8CxnQQcan/Qeo4H9t18R0gmjD+F7draGlSEXGRAAAAAElFTkSuQmCC';

  $message_m.= $strBase64Logo ."\n\n";

  $message_m.= '--' .$strBoundery03 .'--' ."\n\n";
  $message_m.= '--' .$strBoundery02 .'--' ."\n\n";
  
  // preparing attachments
  for($i=0;$i<count($files);$i++){
      if(is_file($files[$i][0])){
          $message_m.= '--' .$strBoundery01 ."\n";
          $fp =   @fopen($files[$i][0],"rb");
          $data = @fread($fp,filesize($files[$i][0]));
          @fclose($fp);
          $data = chunk_split(base64_encode($data));
          
          $finfo = finfo_open(FILEINFO_MIME_TYPE);
          $mime = finfo_file($finfo, $files[$i][0]); 
          
          $message_m.= "Content-Type: " .$mime ."; name=\"".basename($files[$i][1])."\"\n" .
          "Content-Transfer-Encoding: base64\n" . 
          "Content-Disposition: attachment;\n" . " filename=\"=?UTF-8?B?" .base64_encode($files[$i][1]) ."?=\"" .
          "\n\n" . $data . "\n\n";
      } else {
        echo 'NOT A FILE: ' .$files[$i][0]; die();
      }
  }
  

  $message_m.= '--' .$strBoundery01 .'--';

  $returnpath = "-f" . $strFrom;

  //$to = 'system@izs-institut.de';
  //$message_m = 'test message';
  //$headers = "From: $from";
  //$ok = @mail($to, $subject, $message_m, $headers, $returnpath);

  /*
  echo $headers .chr(10);
  echo $to .chr(10);
  echo $subject .chr(10);
  echo $message_m .chr(10) .chr(10);
  */

  $ok = @mail($to, $subject, $message_m, $headers, $returnpath);
  if($ok){ return $i; } else { return 0; }

}


//REQUEST
$strAc   = $_REQUEST['ac'];
$intDdId = @$_REQUEST['ddid'];
$intBgId = @$_REQUEST['bgid'];
$intBeId = @$_REQUEST['beid'];
$intCtId = @$_REQUEST['ctid'];
$intYear = @$_REQUEST['year'];
$strVal  = @$_REQUEST['value'];

$arrFields = @$_REQUEST['values'];

//OBJECTS
$objDocument = new Document;
$objStream   = new Stream('basic');
$objUser     = new User;

$objGroup    = new Group;

$objRequest     = new Request;
$objSendRequest = new SendRequest;

$objBgEvent  = new BgEvent;

$objIp      = new InformationProvider;
$objBg      = new Bg;

$objTemplate = new Template;

//CONST
$arrUpdateStatus = $objDocument->arrGetUpdateStatus();
$arrNextStatus   = $objDocument->arrGetNextStatus();
$intMaxUpdStatus = $objDocument->intGetMaxUpdateStatus();

//FUNCTIONS

if ($strAc == 'renderMassage') {

  echo json_encode($objTemplate->renderTemplate($_REQUEST['ct_id_select'], $_REQUEST['beid'], 'BG'));
  exit;

}

if ($strAc == 'sendMassage') {

  //find Chat
  $arrHeader = array(
    'apikey: e9d99940-1aed-414a-b836-b929fe97d093',
    'user: ' .$_SESSION['id'],
    'Accept: application/json'
  );

  $arrGet = array(
    'from' => 0,
    'q' => 'BG-' .str_pad($_REQUEST['beid'], 6, '0', STR_PAD_LEFT),
    'state' => 'open',
    'type' => 'topic'
  );

  $arrRes = curl_get('http://api.izs-institut.de/api/sg/browse/topic', $arrGet, array(), $arrHeader);
  $arrToppicList = json_decode($arrRes, true);

  $strTopicId   = '';
  $strTopicMail = '';

  if (isset($arrToppicList['total']) && ($arrToppicList['total'] > 0)) {

    $strTopicId   = $arrToppicList['items'][0]['id'];
    $strTopicMail = @$arrToppicList['items'][0]['metadata']['email'];

  }

  //create Token
  $strMessageAdd = '';
  if ($strTopicId != '') {
    $strMessageAdd.= '<hr>' .chr(10);
    $strMessageAdd.= '<p>Bitte diesen Abschnitt nicht verändern und beim Antworten in der E-Mail belassen:<br />' .chr(10);
    $strMessageAdd.= '<a href="https://izs.smartgroups.io/topic/' .$strTopicId .'">' .'__' .$strTopicId .'</a></p>' .chr(10);
    $strMessageAdd.= '<hr>' .chr(10);
  } 

  //send Mail with Token
  if (false) {
    $_REQUEST['to']  = array('system@izs-institut.de');
    $_REQUEST['cc']  = array(); //array('f.luetgen@gmx.de');
    $_REQUEST['bcc'] = ''; //'frank@luetgen.net';
  }

  (is_array($_REQUEST['to'])) ? $to = implode(', ', $_REQUEST['to']) : $to = '';
  (is_array($_REQUEST['cc'])) ? $cc = implode(', ', $_REQUEST['cc']) : $cc = '';
  $bcc = $_REQUEST['bcc'];
  $files = array();
  $subject = $_REQUEST['ct_head'];

  $message = strip_tags($strMessageAdd .$_REQUEST['ct_text']);
  //$message = strip_tags($_REQUEST['ct_text']);
  $message_html = $strMessageAdd .$_REQUEST['ct_text'];

  $strFrom = 'klaerung@izs-institut.de';

  //$to = 'f.luetgen@gmx.de';
  //$strTopicMail = 'system@izs-institut.de';

  $resMail = multi_attach_mail_bg($to, $files, $subject, $message, $message_html, $cc, $bcc, $strFrom);

  if ($strTopicMail != '') {

    $strAddHeader = '';
    //$strAddHeader.= '<hr />' .chr(10);
    $strAddHeader.= '<strong>To: </strong>' .$to .'<br>' .chr(10);
    $strAddHeader.= '<strong>Cc: </strong>' .$cc .'<br>' .chr(10);
    $strAddHeader.= '<strong>Bcc: </strong>' .$bcc .'<br>' .chr(10);
    $strAddHeader.= '<hr />' .chr(10);

    $resMailChat = multi_attach_mail_bg($strTopicMail, $files, $subject, strip_tags($_REQUEST['ct_text']), $strAddHeader .$_REQUEST['ct_text'], '', '', $strFrom);
  }

  //write History
  $arrNew = array (
    'to' => $to,
    'cc' => $cc,
    'bcc' => $bcc,
    'subject' => $subject,
    'message' => $_REQUEST['ct_text'],
  );

  $strSql = 'INSERT INTO `izs_bgevent_change` (`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) ';
  $strSql.= 'VALUES (NULL, "' .$_REQUEST['beid'] .'", "' .$_SESSION['id'] .'", "BG-' .str_pad($_REQUEST['beid'], 6, '0', STR_PAD_LEFT) .'", "", "", \'' .serialize($arrNew) .'\', NOW(), "5")';
  MySQLStatic::Query($strSql);

  //return value
  echo 1;
  exit;

}


if ($strAc == 'bgDelDoc') {

  if ($intBeId != '') {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'OK',
      'beid' => $intBeId
    );
    
    $arrFields['be_link']  = '';
    $boolChange = $objBgEvent->boolChangeValueList($arrFields, $intBeId);    
    
  } else {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'FAIL',
      'beid' => $intBeId,
      'Reason' => 'Ein allgemeiner Fehler ist aufgetreten.'
    );    
    
  }
  
  echo json_encode($arrJson);
  exit;    
  

}


if ($strAc == 'bgRelease') {

  if ($intBeId != '') {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'OK',
      'beid' => $intBeId
    );
    
    $arrFields['be_status_bearbeitung'] = 'veröffentlicht';
    $arrFields['be_sichtbar']           = 'sichtbar';
    $boolChange = $objBgEvent->boolChangeValueList($arrFields, $intBeId);    
    
  } else {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'FAIL',
      'beid' => $intBeId,
      'Reason' => 'Ein allgemeiner Fehler ist aufgetreten.'
    );    
    
  }
  
  echo json_encode($arrJson);
  exit;    
  

}


if ($strAc == 'okClick') {

  //print_r($_REQUEST);

  if ($intBeId != '') {

    $intUserId = $_COOKIES['Id'];

    $strSql = 'SELECT `cl_ks_id` FROM `cms_login` WHERE `cl_id` = "' .MySQLStatic::esc($intUserId) .'"';
    $arrSql = MySQLStatic::Query($strSql);

    $strKSId = '';
    if (count($arrSql) > 0) {
      $strKSId = $arrSql[0]['cl_ks_id'];
    }

    
    //antwort schicken
    $arrJson = array(
      'Status' => 'OK',
      'beid' => $intBeId,
      'fields' => implode(',', $arrFields)
    );

    $strSql = 'SELECT * FROM `izs_bg_event` WHERE `beid` = "' .$intBeId .'"';
    $arrSql = MySQLStatic::Query($strSql);

    if (in_array($arrSql[0]['be_klaerung_status'], array('in Klärung')) !== false) {
      $arrFields['be_klaerung_status']  = 'geschlossen - positiv';
      $arrFields['be_klaerung_schritt'] = '';
      $arrFields['be_wiedervorlage']    = '0000-00-00';
    }
    
    //DO CHANGE + WRITE LOG
    if ($arrFields['be_datum_rueckmeldung'] != '') {
      $arrFields['be_datum_rueckmeldung']  = strConvertDate($arrFields['be_datum_rueckmeldung'], 'd.m.Y', 'Y-m-d'); 
    } else {
      $arrFields['be_datum_rueckmeldung']  = '0000-00-00';
    }

    if ($arrFields['be_befristet'] != '') {
      $arrFields['be_befristet']  = strConvertDate($arrFields['be_befristet'], 'd.m.Y', 'Y-m-d'); 
    } else {
      $arrFields['be_befristet']  = '0000-00-00';
    }

    if ($strKSId != '') {
      $arrFields['be_bearbeiter'] = $strKSId;
    }

    if ($arrFields['be_meldepflicht'] == '') {
      unset($arrFields['be_meldepflicht']);
    }

    //print_r($arrFields); exit;

    $boolChange = $objBgEvent->boolChangeValueList($arrFields, $intBeId);    
    
  } else {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'FAIL',
      'beid' => $intBeId,
      'fields' => implode(',', $arrFields), 
      'Reason' => 'Ein allgemeiner Fehler ist aufgetreten.'
    );    
    
  }
  
  echo json_encode($arrJson);
  exit;    
  

}

if ($strAc == 'svKlaerungClick') {

  if ($intBeId != '') {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'OK',
      'beid' => $intBeId,
      'fields' => implode(',', $arrFields)
    );
    
    //DO CHANGE + WRITE LOG
    if ($arrFields['be_wiedervorlage'] != '') {
      $arrFields['be_wiedervorlage']  = strConvertDate($arrFields['be_wiedervorlage'], 'd.m.Y', 'Y-m-d'); 
    } else {
      $arrFields['be_wiedervorlage']  = '0000-00-00';
    }

    if ($arrFields['be_datum_rueckmeldung'] != '') {
      $arrFields['be_datum_rueckmeldung']  = strConvertDate($arrFields['be_datum_rueckmeldung'], 'd.m.Y', 'Y-m-d'); 
    } else {
      $arrFields['be_datum_rueckmeldung']  = '0000-00-00';
    }

    if ($arrFields['be_befristet'] != '') {
      $arrFields['be_befristet']  = strConvertDate($arrFields['be_befristet'], 'd.m.Y', 'Y-m-d'); 
    } else {
      $arrFields['be_befristet']  = '0000-00-00';
    }

    if ($arrFields['be_klaerung_status'] == 'geschlossen - positiv') {
      $arrFields['be_klaerung_schritt'] = '';
      $arrFields['be_wiedervorlage']    = '0000-00-00';
    }

    $arrFields['be_ergebnis'] = '';

    //print_r($arrFields); exit;

    $boolChange = $objBgEvent->boolChangeValueList($arrFields, $intBeId);    
    
  } else {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'FAIL',
      'beid' => $intBeId,
      'fields' => implode(',', $arrFields), 
      'Reason' => 'Ein allgemeiner Fehler ist aufgetreten.'
    );    
    
  }
  
  echo json_encode($arrJson);
  exit;    
  

}


if ($strAc == 'okClickMass') {

  //print_r($_REQUEST); die();

  if (isset($_REQUEST['beidMass']) && (count($_REQUEST['beidMass']) > 0)) {
    
    $strTemp = $arrFields['be_bearbeiter'];

    if ($arrFields['be_bearbeiter'] == 0) {
      $arrFields['be_bearbeiter'] = '';
    }

    //antwort schicken
    $arrJson = array(
      'Status' => 'OK',
      'beidMass' => $_REQUEST['beidMass'],
      'fields' => $arrFields
    );

    //print_r($arrFields);

    if ($arrFields['be_datum_rueckmeldung'] == '00.00.0000') {
      $arrFields['be_datum_rueckmeldung']  = '0000-00-00';
    } elseif ($arrFields['be_datum_rueckmeldung'] != '') {
      $arrFields['be_datum_rueckmeldung']  = strConvertDate($arrFields['be_datum_rueckmeldung'], 'd.m.Y', 'Y-m-d'); 
    } else {
      unset($arrFields['be_datum_rueckmeldung']);
    }

    if ($arrFields['be_befristet'] == '00.00.0000') {
      $arrFields['be_befristet']  = '0000-00-00';
    } elseif ($arrFields['be_befristet'] != '') {
      $arrFields['be_befristet']  = strConvertDate($arrFields['be_befristet'], 'd.m.Y', 'Y-m-d'); 
    } else {
      unset($arrFields['be_befristet']);
    }

    if ($arrFields['be_stundung'] == 'Alle Einträge löschen') {
      $arrFields['be_stundung'] = '';
    } elseif ($arrFields['be_stundung'] == '') {
      unset($arrFields['be_stundung']);
    }

    if ($arrFields['be_bearbeiter'] == '0') {
      $arrFields['be_bearbeiter'] = '';
    } elseif ($arrFields['be_bearbeiter'] == '') {
      unset($arrFields['be_bearbeiter']);
    }

    // be_meldepflicht kann nicht gelöscht werden

    //print_r($_REQUEST); die();

    foreach ($_REQUEST['beidMass'] as $intKey => $intBeId) {

      $strSql = 'SELECT * FROM `izs_bg_event` WHERE `beid` = "' .$intBeId .'"';
      $arrSql = MySQLStatic::Query($strSql);
  
      if (in_array($arrSql[0]['be_klaerung_status'], array('in Klärung')) !== false) {
        $arrFields['be_klaerung_status']  = 'geschlossen - positiv';
        $arrFields['be_klaerung_schritt'] = '';
        $arrFields['be_wiedervorlage']    = '0000-00-00';
      }

      //echo $intBeId .chr(10);
      //print_r($arrFields);

      $boolChange = $objBgEvent->boolChangeValueList($arrFields, $intBeId, false);   

    }
    
  } else {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'FAIL',
      'beidMass' => $_REQUEST['beidMass'],
      'fields' => $arrFields, 
      'Reason' => 'Ein allgemeiner Fehler ist aufgetreten.'
    );    
    
  }
  
  echo json_encode($arrJson);
  exit;    
  

}


if ($strAc == 'svKlaerungClickMass') {

  //print_r($_REQUEST); die();

  if (isset($_REQUEST['beidMass']) && (count($_REQUEST['beidMass']) > 0)) {
    
    $strTemp = $arrFields['be_bearbeiter'];

    if ($arrFields['be_bearbeiter'] == 0) {
      $arrFields['be_bearbeiter'] = '';
    }

    //antwort schicken
    $arrJson = array(
      'Status' => 'OK',
      'beidMass' => $_REQUEST['beidMass'],
      'fields' => $arrFields
    );

    //echo json_encode($arrJson);
    //exit;

    //print_r($arrFields);

    $arrFields['be_ergebnis'] = '';

    if ($arrFields['be_datum_rueckmeldung'] == '00.00.0000') {
      $arrFields['be_datum_rueckmeldung']  = '0000-00-00';
    } elseif ($arrFields['be_datum_rueckmeldung'] != '') {
      $arrFields['be_datum_rueckmeldung']  = strConvertDate($arrFields['be_datum_rueckmeldung'], 'd.m.Y', 'Y-m-d'); 
    } else {
      unset($arrFields['be_datum_rueckmeldung']);
    }

    if ($arrFields['be_klaerung_status'] == 'Alle Einträge löschen') {
      $arrFields['be_klaerung_status'] = '';
    } elseif ($arrFields['be_klaerung_status'] == '') {
      unset($arrFields['be_klaerung_status']);
    }

    if ($arrFields['be_klaerung_grund'] == 'Alle Einträge löschen') {
      $arrFields['be_klaerung_grund'] = '';
    } elseif ($arrFields['be_klaerung_grund'] == '') {
      unset($arrFields['be_klaerung_grund']);
    }

    if ($arrFields['be_klaerung_schritt'] == 'Alle Einträge löschen') {
      $arrFields['be_klaerung_schritt'] = '';
    } elseif ($arrFields['be_klaerung_schritt'] == '') {
      unset($arrFields['be_klaerung_schritt']);
    }

    if ($arrFields['be_klaerung_status'] == 'geschlossen - positiv') {
      $arrFields['be_klaerung_schritt'] = '';
      $arrFields['be_wiedervorlage'] = '00.00.0000';
    }

    if ($arrFields['be_rueckstand'] == '[LÖSCHEN]') {
      $arrFields['be_rueckstand'] = '';
    } elseif ($arrFields['be_rueckstand'] == '') {
      unset($arrFields['be_rueckstand']);
    }

    if ($arrFields['be_klaerung_ap'] == '[LÖSCHEN]') {
      $arrFields['be_klaerung_ap'] = '';
    } elseif ($arrFields['be_klaerung_ap'] == '') {
      unset($arrFields['be_klaerung_ap']);
    }

    if ($arrFields['be_klaerung_telefon'] == '[LÖSCHEN]') {
      $arrFields['be_klaerung_telefon'] = '';
    } elseif ($arrFields['be_klaerung_telefon'] == '') {
      unset($arrFields['be_klaerung_telefon']);
    }

    if ($arrFields['be_info_intern'] == '[LÖSCHEN]') {
      $arrFields['be_info_intern'] = '';
    } elseif ($arrFields['be_info_intern'] == '') {
      unset($arrFields['be_info_intern']);
    }

    //var_dump($arrFields['be_wiedervorlage']);

    if ($arrFields['be_wiedervorlage'] == '00.00.0000') {
      $arrFields['be_wiedervorlage'] = '0000-00-00';
    } elseif ($arrFields['be_wiedervorlage'] != '') {
      $arrFields['be_wiedervorlage']  = strConvertDate($arrFields['be_wiedervorlage'], 'd.m.Y', 'Y-m-d'); 
    } else {
      unset($arrFields['be_wiedervorlage']);
    }

    if ($arrFields['be_stundung'] == 'Alle Einträge löschen') {
      $arrFields['be_stundung'] = '';
    } elseif ($arrFields['be_stundung'] == '') {
      unset($arrFields['be_stundung']);
    }

    if ($arrFields['be_bearbeiter'] == '0') {
      $arrFields['be_bearbeiter'] = '';
    } elseif ($arrFields['be_bearbeiter'] == '') {
      unset($arrFields['be_bearbeiter']);
    }

    // be_meldepflicht kann nicht gelöscht werden

    //print_r($arrFields); die();

    foreach ($_REQUEST['beidMass'] as $intKey => $intBeId) {

      //print_r($arrFields);
      //echo $intBeId;
      $boolChange = $objBgEvent->boolChangeValueList($arrFields, $intBeId, false);   
    
    }
    
  } else {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'FAIL',
      'beidMass' => $_REQUEST['beidMass'],
      'fields' => $arrFields, 
      'Reason' => 'Ein allgemeiner Fehler ist aufgetreten.'
    );    
    
  }
  
  echo json_encode($arrJson);
  exit;    
  

}

if ($strAc == 'svAuskunftClick') {

  if ($intBeId != '') {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'OK',
      'beid' => $intBeId,
      'fields' => implode(',', $arrFields)
    );
    
    //DO CHANGE + WRITE LOG
    if ($arrFields['be_befristet'] != '') {
      $arrFields['be_befristet']  = strConvertDate($arrFields['be_befristet'], 'd.m.Y', 'Y-m-d'); 
    } else {
      $arrFields['be_befristet']  = '0000-00-00';
    }
    
    $arrFields['be_status_bearbeitung']  = 'Endkontrolle';
    
    $boolChange = $objBgEvent->boolChangeValueList($arrFields, $intBeId);    
    
  } else {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'FAIL',
      'beid' => $intBeId,
      'fields' => implode(',', $arrFields), 
      'Reason' => 'Ein allgemeiner Fehler ist aufgetreten.'
    );    
    
  }
  
  echo json_encode($arrJson);
  exit;    
  

}


if ($strAc == 'bgErChange') {
    
  if ($intBeId != '') {
    
    $arrBgEvent = $objBgEvent->arrGetBgEvent($intBeId);
    if ($arrBgEvent[0]['be_dokument_art'] == '') {
      $arrBgEvent[0]['be_dokument_art'] = 'Unbedenklichkeitsbescheinigung (UBB)';
    }
    
    $strAction = '';
    $strModalName = '';
    $strKlaerungStatus = '';
    
    if ($strVal == 'NOT OK') {
      $strAction = 'modal';
      $strModalName = 'Klaerung';
      $strKlaerungStatus = 'in Klärung';
      $strStatusBearbeitung = 'erhalten - Klärungsfall';
    } elseif ($strVal == 'OK') {

      //$strAction = 'modal';
      //$strModalName = 'Auskunft';

      $strStatusBearbeitung = 'erhalten - verarbeitbar';
    } elseif ($strVal == '') {
      $strStatusBearbeitung = 'angefragt';
    } else {
      $strStatusBearbeitung = 'angefragt';
    }
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'OK',
      'beid' => $intBeId,
      'value' => $strVal,
      'Action' => $strAction,
      'Name' => $strModalName
    );
    
    
    //DO CHANGE + WRITE LOG
    $boolChange = $objBgEvent->boolChangeValue('be_ergebnis', $strVal, $intBeId);
    $boolChange = $objBgEvent->boolChangeValue('be_status_bearbeitung', $strStatusBearbeitung, $intBeId);
    $boolChange = $objBgEvent->boolChangeValue('be_dokument_art', $arrBgEvent[0]['be_dokument_art'], $intBeId);
    $boolChange = $objBgEvent->boolChangeValue('be_klaerung_status', $strKlaerungStatus, $intBeId);
        
    
  } else {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'FAIL',
      'beid' => $intBeId,
      'value' => $strVal,
      'Reason' => 'Ein allgemeiner Fehler ist aufgetreten.'
    );    
    
  }
  
  echo json_encode($arrJson);
  exit;    

}

if ($strAc == 'bgRuChange') {
    
  if ($intBeId != '') {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'OK',
      'beid' => $intBeId,
      'value' => $strVal
    );
    
    //DO CHANGE + WRITE LOG
    if ($strVal == '') {
      $strDbDate  = '0000-00-00';
    } else {
      $strDbDate  = strConvertDate($strVal, 'd.m.Y', 'Y-m-d');
    }
    $boolChange = $objBgEvent->boolChangeValue('be_datum_rueckmeldung', $strDbDate, $intBeId);
    
    if ($strVal == '') {
      $boolChange = $objBgEvent->boolChangeValue('be_ergebnis', '', $intBeId);
      $boolChange = $objBgEvent->boolChangeValue('be_status_bearbeitung', 'angefragt', $intBeId);
    }
    
    
  } else {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'FAIL',
      'beid' => $intBeId,
      'value' => $strVal,
      'Reason' => 'Ein allgemeiner Fehler ist aufgetreten.'
    );    
    
  }
  
  echo json_encode($arrJson);
  exit;    

}


if ($strAc == 'bgFrChange') {
    
  if ($intBeId != '') {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'OK',
      'beid' => $intBeId,
      'value' => $strVal
    );
    
    //DO CHANGE + WRITE LOG
    if ($strVal == '') {
      $strDbDate  = '0000-00-00';
    } else {
      $strDbDate  = strConvertDate($strVal, 'd.m.Y', 'Y-m-d');
    }
    $boolChange = $objBgEvent->boolChangeValue('be_befristet', $strDbDate, $intBeId);
    
  } else {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'FAIL',
      'beid' => $intBeId,
      'value' => $strVal,
      'Reason' => 'Ein allgemeiner Fehler ist aufgetreten.'
    );    
    
  }
  
  echo json_encode($arrJson);
  exit;    

}


if ($strAc == 'bgDaChange') {
    
  if ($intBeId != '') {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'OK',
      'beid' => $intBeId,
      'value' => $strVal
    );
    
    //DO CHANGE + WRITE LOG
    $boolChange = $objBgEvent->boolChangeValue('be_dokument_art', $strVal, $intBeId);
    
  } else {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'FAIL',
      'beid' => $intBeId,
      'value' => $strVal,
      'Reason' => 'Ein allgemeiner Fehler ist aufgetreten.'
    );    
    
  }
  
  echo json_encode($arrJson);
  exit;    

}

if ($strAc == 'ddArchivClick') {
  
  $strArchivAction = $_REQUEST['aaction'];
  
  if ($strArchivAction == $arrUpdateStatus[5]) { //Keine Antwort

    $arrDocument = $objDocument->arrGetDocument($intDdId);
    $intTime = time();

    //sf aktualisieren
    //$boolSfStatus = $objDocument->boolUpdateUpdateStatusSf($intDdId, $arrUpdateStatus[5], $intTime);
    
    //db aktualisieren
    $boolDbStatus = $objDocument->boolUpdateUpdateStatusDb($intDdId, $arrUpdateStatus[5], $intTime);
  
    //Stream schreiben
    $intStream = $objStream->intInsertStream($intDdId, $arrDocument[0]['dd_grid'], 'Update_Status__c', $arrDocument[0]['dd_ustatus'], $arrUpdateStatus[5]);

    //antwort schicken
    $arrJson = array(
      'Status' => 'OK',
      'ddid' => $intDdId,
      'Action' => 'restart',
      'UTime' => date('d.m.Y', $intTime),
      'UTimeH' => date('Ymd', $intTime),
      'UStatus' => $arrUpdateStatus[5],
      'UStatusNew' => '',
      'SfResult' => @$boolSfStatus,
      'DbResult' => @$boolDbStatus,
      'StResult' => @$intStream
    );
    
  } elseif ($strArchivAction == $arrUpdateStatus[6]) { //Archiv

    $arrDocument = $objDocument->arrGetDocument($intDdId);
    $intTime = time();

    //sf aktualisieren
    //$boolSfStatus = $objDocument->boolArchiveDocumentSf($intDdId);
    
    //db aktualisieren
    $boolDbStatus = $objDocument->boolArchiveDocumentDb($intDdId);
  
    //Stream schreiben
    $intStream = $objStream->intInsertStream($intDdId, $arrDocument[0]['dd_grid'], 'Status__c', 'aktiv', 'archiviert');

    //antwort schicken
    $arrJson = array(
      'Status' => 'OK',
      'ddid' => $intDdId,
      'Action' => 'archive',
      'SfResult' => @$boolSfStatus,
      'DbResult' => @$boolDbStatus,
      'StResult' => @$intStream
    );
        
  }

    echo json_encode($arrJson);
    exit;  
}


if ($strAc == 'ddStatusClickMass') {

  $arrSerialize = array(
    'ac' => 'ddStatusClick',
    'data' => $_REQUEST['data']
  );

  $arrSerialize['data']['_SESSION']['id'] = $_SESSION['id'];

  //INSERT INTO `_cron_ajax` (`ca_id`, `ca_hash`, `ca_url`, `ca_time`, `ca_data`, `ca_method`, `ca_status`) VALUES (NULL, '', '', '', '', '', '');

  $strSql = 'INSERT INTO `_cron_ajax` (`ca_id`, `ca_url`, `ca_time`, `ca_foreign_id`, `ca_data`, `ca_method`, `ca_status`) ';
  $strSql.= 'VALUES (NULL, "' .$_REQUEST['url'] .'", NOW(), "' .$_REQUEST['data']['ddid'] .'", "' .str_replace('"', '\"', serialize($arrSerialize)) .'", "' .$_SERVER['REQUEST_METHOD']  .'", 1)';
  MySQLStatic::Query($strSql);

  //mail('system@izs-institut.de', 'test', 'Data: ' .print_r($_REQUEST['data'], true) .chr(10) .'Method: ' .$_SERVER['REQUEST_METHOD'] .chr(10) .'Url:' .$_REQUEST['url']);  

}


if ($strAc == 'ddStatusClick') {

  if (isset($_REQUEST['data'])) {
    $intDdId = $_REQUEST['data']['ddid'];
    $_SESSION['id'] = $_REQUEST['data']['_SESSION']['id'];
  }
  
  $arrDocument = $objDocument->arrGetDocument($intDdId);
  $intTime = time();
  $strUStatusNew = $objDocument->strGetNewUpdateStatus($arrDocument[0]['dd_ustatus']);

  $objGroup = new Group;
  $arrGroup = $objGroup->arrGetGroup($arrDocument[0]['dd_grid']);

  if ($arrDocument[0]['dd_ustatus'] == '3. Erinnerung') {

    //antwort schicken
    $arrJson = array(
      'Status' => 'OK',
      'ddid' => $intDdId,
      'Action' => 'modal',
      'Name' => 'Archive'
    );
    
    echo json_encode($arrJson);
    exit;

  }
  
  //Empfänfer ermitteln
  $arrRecipients = $objDocument->arrGetRecipients($intDdId);

  //print_r($arrRecipients); die();

  if (count($arrRecipients) > 0) {

    //sf aktualisieren
    if (strstr($_SERVER['SERVER_NAME'], 'test.') === false) {
      //$boolSfStatus = $objDocument->boolUpdateUpdateStatusSf($intDdId, $strUStatusNew, $intTime);
    } 
    
    ///*
    //db aktualisieren
    $boolDbStatus = $objDocument->boolUpdateUpdateStatusDb($intDdId, $strUStatusNew, $intTime);
  
    //Stream schreiben
    $intStream = $objStream->intInsertStream($intDdId, $arrDocument[0]['dd_grid'], 'Update_Status__c', $arrDocument[0]['dd_ustatus'], $strUStatusNew);
    
    if ($arrDocument[0]['dd_ustatus'] == '') {
      $strAdd = 'Neues Dokument angefordert.';
      if ($arrDocument[0]['dd_ivisible'] != '') {
        $strAdd.= ' ' .$arrDocument[0]['dd_ivisible'];
      }
      $intStream = $objStream->intInsertStream($intDdId, $arrDocument[0]['dd_grid'], 'Info_sichtbar__c', $strAdd);
      $objDocument->boolUpdateField($intDdId, 'Info_sichtbar__c', $strAdd);
    }
    //*/
    
    //mail verschicken
    $strTo    = implode(', ', $arrRecipients);
    $arrFiles = array();
    $strFrom  = 'basisdokumentation@izs-institut.de';
    $strBcc   = $strFrom;

    if (strstr($_SERVER['SERVER_NAME'], 'test.') !== false) {
      $strTo  = 'system@izs-institut.de';
      $strBcc = '';
    }

    $strSubject = $objDocument->strGetSubject($arrDocument[0]['dd_name'], $strUStatusNew);
    $strMessageHtml = $objDocument->strGetMessage($arrDocument[0]['ddid'], $strUStatusNew);
    $strMessage = $strMessageHtml;

    $strUStatusNewForStream = $strUStatusNew;

    //MAIL SENDING CUTTED HERE AND PASETED AT THE AND 

    //geänderte Daten abrufen
    $arrDocument = $objDocument->arrGetDocument($intDdId);
    $strUStatusNew = $objDocument->strGetNewUpdateAction($arrDocument[0]['dd_ustatus']);

    $arrDocStatus = $objDocument->arrGetDocumentStatus($arrDocument[0]['ddid']);
    
    $strAm = '';
    $strEscDate  = '';
    $strEscDateH = '';
    $strDeadlineClass = '';
    if (count($arrDocStatus) > 0) {
      $strAm = strConvertDate($arrDocStatus[0]['bc_time'], 'Y-m-d H:i:s', 'd.m.Y');
      $strEscDate = strAddWorkingDays((strConvertDate($strAm, 'd.m.Y', 'U')), $objDocument->intGetEscalation($arrDocument[0]['dd_ustatus']));
      $strEscDateH = strConvertDate($strEscDate, 'd.m.Y', 'Ymd');
      if (strConvertDate($strEscDate, 'd.m.Y', 'U') <= strConvertDate(date('d.m.Y'), 'd.m.Y', 'U')) {
        $strDeadlineClass = 'red-600';
      }
    }

    
    //antwort schicken
    $arrJson = array(
      'Status' => 'OK',
      'ddid' => $intDdId,
      'UTime' => date('d.m.Y', $intTime),
      'UTimeH' => date('Ymd', $intTime),
      'Deadline' => $strEscDate,
      'DeadlineH' => $strEscDateH,
      'UStatus' => $arrDocument[0]['dd_ustatus'],
      'UStatusNew' => $strUStatusNew,
      'Recipients' => '"' .implode('", "', $arrRecipients) .'"',
      'SfResult' => @$boolSfStatus,
      'DbResult' => @$boolDbStatus,
      'StResult' => @$intStream,
      'MaResult' => @$intMail,
      'DeadlineClass' => $strDeadlineClass,
      'InfoExtern' => $arrDocument[0]['dd_ivisible']
    );

    //CREATE CHAT ON "ANFRAGE SENDEN"
    if (true) {

      $intUser = $_SESSION['id'];

      $arrTeam = array(
        //32 => 'Serap',
        '47' => 'Ecem',
        '42' => 'Seda',
        '44' => 'Karo', 
        '36' => 'Gordon',
        '50' => 'Ceren'
      );

      $arrHeader = array(
        'apikey: e9d99940-1aed-414a-b836-b929fe97d093',
        'user: ' .$intUser,
        'Accept: application/json'
      );

      if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
        $arrHeader[] = 'test: true';
      }

      //Create User List
      $arrRes = curl_get('http://api.izs-institut.de/api/sg/user', array(), array(), $arrHeader);
      $arrUserList = json_decode($arrRes, true);

      //Create Context (name, uri)
      $arrContext = array(
        'name' => 'IZS/Basisdokumentation/' .$arrGroup[0]['gr_name'] .'/' .$arrDocument[0]['dd_dtype'],
        'uri' => 'http://www.izs.de/cms/index_neu.php?ac=bd_aktualisieren#' .$intDdId
      );

      if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
        $arrContext['uri'] = 'http://test.izs.de/cms/index_neu.php?ac=bd_aktualisieren#' .$intDdId;
      }

      $arrRes = curl_post('http://api.izs-institut.de/api/sg/context', $arrContext, array(), $arrHeader);
      $arrContext = json_decode($arrRes, true);

      if ($strUStatusNew == '1. Erinnerung senden') {
  
        $arrRes = curl_get('http://api.izs-institut.de/api/sg/category', array(), array(), $arrHeader);
        $arrCatList = json_decode($arrRes, true);

        $arrMember = array($arrUserList[$intUser]);
  
        $arrFollower = array();
        foreach ($arrTeam as $intTeamId => $strName) {
          if ($intTeamId != $intUser) {
            $arrFollower[] = $arrUserList[$intTeamId];
          }
        }

        $arrDocument = $objDocument->arrGetDocument($intDdId);
  
        $intEscDate = (strtotime($strEscDate) + 43200) * 1000; // 12:00 Uhr
  
        $arrTopic = array(
          'title' => 'Warten auf Eingang: ' .$arrGroup[0]['gr_name'] .' - ' .$arrDocument[0]['dd_dtype'] .' - ' .$arrDocument[0]['dd_name'],
          'category' => array(array_search('Basisdokumentation', $arrCatList)),
          'context' => $arrContext['id'],
          'member' => $arrMember,
          'follow' => $arrFollower,
          // 'followup' => array($arrUserList[$_SESSION['id']] => (int) $intEscDate),
          //'text' => '', //'E-Mail Benachrichtigung gesendet am ' .date('d.m.Y') .' um ' .date('H:i:s') .' an folgende Empfänger:<br /><br />' .$strTo,
          'due' => (int) $intEscDate,
          'cmsid' => $intDdId,
          'type' => 'Basisdoku'
        );
  
        $arrRes = curl_post('http://api.izs-institut.de/api/sg/topic', $arrTopic, array(), $arrHeader);
        $arrTop = json_decode($arrRes, true);
  
      } else {

        $arrRes = curl_get('http://api.izs-institut.de/api/sg/topic/' .$arrContext['id'], array(), array(), $arrHeader);
        $arrTopicList = json_decode($arrRes, true);

        if (count($arrTopicList) > 0) {

          foreach ($arrTopicList as $intSort => $arrTopic) {

            $strTopicId = key($arrTopic);
            $strTopicTitle = current($arrTopic);

            if ((strstr($strTopicTitle, 'Warten auf Eingang') !== false) && (strstr($strTopicTitle, $arrDocument[0]['dd_name']) !== false)) {
              break;
            }

          }

          $intEscDate = (strtotime($strEscDate) + 43200) * 1000; // 12:00 Uhr

          $arrTopic = array(
            'due' => (int) $intEscDate
          );
    
          $arrRes = curl_post('http://api.izs-institut.de/api/sg/topic/' .$strTopicId, $arrTopic, array(), $arrHeader);
          $arrTop = json_decode($arrRes, true);

          $arrTop['id'] = $strTopicId;

        }

      }

      //Write chat content
      if (!isset($arrTop['id'])) {
        
        $arrRes = curl_get('http://api.izs-institut.de/api/sg/topic/' .$arrContext['id'], array(), array(), $arrHeader);
        $arrTopicList = json_decode($arrRes, true);

        foreach ($arrTopicList as $intSort => $arrTopic) {

          foreach ($arrTopic as $strId => $strTitle) {
            if (strstr($strTitle, 'Warten auf Eingang: ') !== false) {
              $arrTop['id'] = $strId;
              break;
            }
          }

        }

      }


      if (isset($arrTop['id'])) {

        $arrPost = array(
          'author' => $arrUserList[$intUser],
          'context' => $arrContext['id'],
          'text' => '<p>E-Mail ("' .$arrDocument[0]['dd_ustatus'] .'") gesendet am ' .date('d.m.Y') .' um ' .date('H:i:s') .' an folgende Empfänger:<br />' .$strTo .'</p>',
          'target' => $arrTop['id'],
          'topic' => $arrTop['id'],
          'type' => 'content',
          'contenttype' => 'message'
        );

        $arrRes = curl_post('http://api.izs-institut.de/api/sg/content', $arrPost, array(), $arrHeader);

        //update due
        if ($strUStatusNew != '1. Erinnerung senden') {
          ;
        }

      }

    }

    $strMessageAdd = '';
    if (isset($arrTop['id']) && ($arrTop['id'] != '')) {
      $strMessageAdd.= '<hr>' .chr(10);
      $strMessageAdd.= '<p>Bitte diesen Abschnitt nicht verändern und beim Antworten in der E-Mail belassen:<br />' .chr(10);
      $strMessageAdd.= '__' .$arrTop['id'] .'</p>' .chr(10);
      $strMessageAdd.= '<hr>' .chr(10);
    }  

    /*
    $strTo  = 'km@izs-institut.de';
    //$strTo  = 'system@izs-institut.de';
    $strBcc = 'system@izs-institut.de';
    */

    //$strTo  = 'system@izs-institut.de';
    //$strBcc = '';

    $objEmail = new MultiMail;
    $arrMail = $objEmail->intSendMultiMail($strTo, $arrFiles, $strSubject, $strFrom, $strMessageAdd .$strMessage, $strMessageAdd .$strMessageHtml, $strBcc);
    $arrMail['UStatus'] = $strUStatusNewForStream;

    //Stream schreiben
    $intStream = $objStream->intInsertStream($intDdId, $arrDocument[0]['dd_grid'], '', '', serialize($arrMail), 5);
  
  } else {

    //antwort schicken
    $arrJson = array(
      'Status' => 'FAIL',
      'ddid' => $intDdId,
      'Reason' => 'Keine Empfänger mit Merkmal "Basisdokumentation" vorhanden.'
    );

  }
  
  echo json_encode($arrJson);
  
}

if ($strAc == 'ddInfoClick') {

  if (isset($_REQUEST['data'])) {
    $intDdId = $_REQUEST['data']['ddid'];
    $_SESSION['id'] = $_REQUEST['data']['_SESSION']['id'];
    $strEnt = $_REQUEST['data']['ent'];
  }

  $intUser = $_SESSION['id'];

  $strEnt = $_REQUEST['ent'];

  $objAccount = new Account();
  $arrEntleiher = $objAccount->arrGetAccount($strEnt);

  //echo $arrEntleiher[0]['ac_name']; die();

  $arrDocument  = $objDocument->arrGetDocument($intDdId);

  $objGroup = new Group;
  $arrGroup = $objGroup->arrGetGroup($arrDocument[0]['dd_grid']);
  
  //Empfänfer ermitteln
  $objContact = new Contact;
  $arrContactList = $objContact->arrGetContactListFromIp($strEnt, '', true, true);

  if (count($arrContactList) > 0) {

    $arrRecipientList = array();
    foreach ($arrContactList as $intKey => $arrContact) {
      $arrRecipientList[] = $arrContact['co_mail'];
    }


    //mail verschicken
    $arrFiles = array();
    $strFrom  = 'basisdokumentation@izs-institut.de';
    $strBcc   = ''; //$strFrom;

    if (strstr($_SERVER['SERVER_NAME'], 'test.') !== false) {
      $strTo  = 'system@izs-institut.de';
      $strBcc = '';
    }

    $strSubject = $objDocument->strGetSubject($arrDocument[0]['dd_name'], 'Info an Entleiher senden');
    $strMessageHtml = $objDocument->strGetMessage($arrDocument[0]['ddid'], 'Info an Entleiher senden');
    $strMessage = $strMessageHtml;

    $strMessageAdd = '';

    $objEmail = new MultiMail;

    foreach ($arrRecipientList as $intKey => $strTo) {

      //$strTo = 'system@izs-institut.de';
      //$strBcc = '';

      $arrMail = $objEmail->intSendMultiMail($strTo, $arrFiles, $strSubject, $strFrom, $strMessageAdd .$strMessage, $strMessageAdd .$strMessageHtml, $strBcc);

    }
    
    //Stream schreiben
    $arrMail['UStatus']     = 'Info an Entleiher';
    $arrMail['Entleiher']   = $strEnt;
    $arrMail['Reciptiants'] = implode(', ', $arrRecipientList);


    // Send Summary
    $strTo = $strFrom;
    $strSummary = '<p>Sehr geehrte Frau Mutzel,</p>' .chr(10) .chr(10);
    $strSummary.= '<p>Versendet am:  ' .date('d.m.Y H:i:s') .'<br />' .chr(10);
    $strSummary.= 'Empfänger: ' .$arrMail['Reciptiants'] .'</p>' .chr(10) .chr(10);

    $strMessageHtml = str_replace('<p>Sehr geehrte Damen und Herren,</p>', $strSummary, $strMessageHtml);
    $strMessage     = str_replace('Sehr geehrte Damen und Herren,', strip_tags($strSummary), $strMessage);
    
    $objEmail->intSendMultiMail($strTo, $arrFiles, $strSubject, $strFrom, $strMessageAdd .$strMessage, $strMessageAdd .$strMessageHtml, $strBcc);


    $intStream = $objStream->intInsertStream($intDdId, $arrDocument[0]['dd_grid'], '', '', serialize($arrMail), 5);

    $arrSent = array(
      'Reciptiants' => $arrMail['Reciptiants'],
      'Entleiher'   => $strEnt
    );

    $intStream = $objStream->intInsertStream($intDdId, $arrDocument[0]['dd_grid'], '', '', serialize($arrSent), 7);    

    $arrHeader = array(
      'apikey: e9d99940-1aed-414a-b836-b929fe97d093',
      'user: ' .$intUser,
      'Accept: application/json'
    );

    if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
      $arrHeader[] = 'test: true';
    }

    //Create User List
    $arrRes = curl_get('http://api.izs-institut.de/api/sg/user', array(), array(), $arrHeader);
    $arrUserList = json_decode($arrRes, true);

    //Create Context (name, uri)
    $arrContext = array(
      'name' => 'IZS/Basisdokumentation/' .$arrGroup[0]['gr_name'] .'/' .$arrDocument[0]['dd_dtype'],
      'uri' => 'http://www.izs.de/cms/index_neu.php?ac=bd_aktualisieren#' .$intDdId
    );

    if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
      $arrContext['uri'] = 'http://test.izs.de/cms/index_neu.php?ac=bd_aktualisieren#' .$intDdId;
    }

    $arrRes = curl_post('http://api.izs-institut.de/api/sg/context', $arrContext, array(), $arrHeader);
    $arrContext = json_decode($arrRes, true);

    //Write chat content
    if (!isset($arrTop['id'])) {
      
      $arrRes = curl_get('http://api.izs-institut.de/api/sg/topic/' .$arrContext['id'], array(), array(), $arrHeader);
      $arrTopicList = json_decode($arrRes, true);

      foreach ($arrTopicList as $intSort => $arrTopic) {

        foreach ($arrTopic as $strId => $strTitle) {
          if (strstr($strTitle, $arrDocument[0]['dd_name']) !== false) {
            $arrTop['id'] = $strId;
            break;
          }
        }

      }

    }

    //echo $arrTop['id'];

    if (isset($arrTop['id'])) {

      $arrPost = array(
        'author' => $arrUserList[$intUser],
        'context' => $arrContext['id'],
        'text' => '<p>E-Mail ("Info an Entleiher") gesendet an "' .$arrEntleiher[0]['ac_name'] .'" am ' .date('d.m.Y') .' um ' .date('H:i:s') .' an folgende Empfänger:<br />' .implode(', ', $arrRecipientList) .'</p>',
        'target' => $arrTop['id'],
        'topic' => $arrTop['id'],
        'type' => 'content',
        'contenttype' => 'message'
      );

      $arrRes = curl_post('http://api.izs-institut.de/api/sg/content', $arrPost, array(), $arrHeader);

    }

    $arrJson = array(
      'Status' => 'OK',
      'ddid' => $intDdId,
      'ent'  => $strEnt
    );

  } else {

    //antwort schicken
    $arrJson = array(
      'Status' => 'FAIL',
      'ddid' => $intDdId,
      'Reason' => 'Keine Empfänger mit Merkmal "Basisdokumentation" vorhanden.'
    );

  }

  
  echo json_encode($arrJson);
  
}

if ($strAc == 'bd_detail') {
  
  $arrDocument = $objDocument->arrGetDocument($intDdId);
  $arrDsType   = $objDocument->arrGetDTypeUpdate();
  
  $arrGroup = $objGroup->arrGetGroup($arrDocument[0]['dd_grid']);
  
/*
[dd_grid] => a083000000UW4WuAAL [dd_link] => https://izs-institut.box.com/s/069lkkgs6uz6hwsjuaoo34mzl76eyy8a [dd_status] => aktuell [dd_dtype] => ISO 9001:2008
*/  
  $strDataName = $strIChatPath .'/Basisdokumentation/' .$arrGroup[0]['gr_name'] .'/' .$arrDocument[0]['dd_dtype']; 
  
  $arrStream = $objStream->arrGetStreamList($intDdId);

  if ($_SESSION['id'] == 3) {
    //print_r($arrStream); die();
  }

  $strStream = '';
  if (count($arrStream) > 0) {
    
    for ($intCount = 0; $intCount < count($arrStream); $intCount++) {
      
      $arrStreamEntry = $arrStream[$intCount];
      
      //print_r($arrStreamEntry);
      
      if ($arrStreamEntry['bc_type'] == 2) {
        
        if (($arrStreamEntry['bc_fild'] == 'Update_Status__c') && ($arrStreamEntry['bc_new'] == $arrUpdateStatus[0])) {
          
          $strStream.= '<div class="streamentry">' .chr(10);
          $strStream.= '<p><span class="streamuser">' .$objUser->strGetUserName($arrStreamEntry['bc_clid']) .'' .chr(10);
          $strStream.= ' - am ' .strConvertDate($arrStreamEntry['bc_time'], 'Y-m-d H:i:s', 'd.m.Y / H:i:s') .'</span></p>' .chr(10);
          $strStream.= '<h3>Anfrage neu gestartet</h3>' .chr(10);
          $strStream.= '</div>' .chr(10);
          
        }
        
        if (($arrStreamEntry['bc_fild'] == 'Aktiv__c') && ($arrStreamEntry['bc_new'] == 'false')) {
          
          $strStream.= '<div class="streamentry">' .chr(10);
          $strStream.= '<p><span class="streamuser">' .$objUser->strGetUserName($arrStreamEntry['bc_clid']) .'' .chr(10);
          $strStream.= ' - am ' .strConvertDate($arrStreamEntry['bc_time'], 'Y-m-d H:i:s', 'd.m.Y / H:i:s') .'</span></p>' .chr(10);
          $strStream.= '<h3>Dokument archiviert</h3>' .chr(10);
          $strStream.= '</div>' .chr(10);
          
        }
        
      }
      if ($arrStreamEntry['bc_type'] == 5) { 
        
        $arrMail = unserialize($arrStreamEntry['bc_new']);
        
        $strHeadline = @$arrMail['UStatus'];
        if (($strHeadline[1] == '.') || ($strHeadline == 'Info an Entleiher')) {
          $strHeadline.= ' versandt';
        } 
        
        $strStream.= '<div class="streamentry">' .chr(10);
        $strStream.= '<p><span class="streamuser">' .$objUser->strGetUserName($arrStreamEntry['bc_clid']) .'' .chr(10);
        $strStream.= ' - am ' .strConvertDate($arrStreamEntry['bc_time'], 'Y-m-d H:i:s', 'd.m.Y / H:i:s') .'</span></p>' .chr(10);
        $strStream.= '<h3>' .$strHeadline .'</h3>' .chr(10);
        $strStream.= '<p><span class="streamhead">An: ' .$arrMail['Reciptiants'] .'</span><p>' .chr(10);
        $strStream.= '<p><span class="streamhead">Betreff: ' .$arrMail['Subject'] .'</span></p>' .chr(10);
        $strStream.= '<p>' .str_replace(' href=', ' target="_blank" href=', $arrMail['Message']) .'</p>' .chr(10);
        $strStream.= '</div>' .chr(10);

      }
      
    }
      
  }

  $strReturn = '';
  
  $strReturn.= '' .chr(10);
  $strReturn.= '                              <div class="modal-header">' .chr(10);
  $strReturn.= '                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">' .chr(10);
  $strReturn.= '                                  <span aria-hidden="true">x</span>' .chr(10);
  $strReturn.= '                                </button>' .chr(10);
  $strReturn.= '                                <h4 class="modal-title">Dokument-ID: ' .$arrDocument[0]['dd_name'] .'</h4>' .chr(10);
  //$strReturn.= '                                <h6>Dokument-ID: ' .$arrDocument[0]['dd_name'] .'</h6>' .chr(10);
  $strReturn.= '                              </div>' .chr(10);
  $strReturn.= '                              <div class="modal-body">' .chr(10);
  $strReturn.= '' .chr(10);

  $strCrmLink = $strCrmUrl .'doc/' .$intDdId .'/stammdaten';

  $strReturn.= '
  
                <!-- Example Tabs -->
              <div class="example-wrap">
                <div class="nav-tabs-horizontal">
                  <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                    <li class="active" role="presentation"><a data-toggle="tab" href="#exampleTabsOne" aria-controls="exampleTabsOne"
                      role="tab">Stammdaten</a></li>
                    <li role="presentation"><a data-toggle="tab" href="#exampleTabsTwo" aria-controls="exampleTabsTwo"
                      role="tab"><span>Chats</span><span class="badge up badge-danger" kng smartchat data-type="unread" data-uri="' .$strIChatUri .'cms/index_neu.php?ac=bg_detail&beid=' .$intBeId .'"></span></a></li>
                  </ul>
                  <div class="tab-content padding-top-20">
                    <div class="tab-pane active" id="exampleTabsOne" role="tabpanel" style="min-height: 450px;">
                    
                      
                  <ul class="blocks-2">
                    <li>
                      <div class="example-col">
                      
                         <table class="table table-striped">
                           <colgroup>
                             <col width="40%">
                             <col width="60%">
                           </colgroup>
                          <thead>
                          </thead>
                          <tbody>
                            <tr>
                              <th class="text-nowrap"><strong>Update-Status</strong></th>
                              <td><strong>' .$arrDocument[0]['dd_ustatus'] .'</strong></td>
                            </tr>
                            <tr>
                              <th class="text-nowrap">Datensatztyp</th>
                              <td>' .$arrDsType[$arrDocument[0]['dd_rtype']] .'</td>
                            </tr>
                            <tr>
                              <th class="text-nowrap">Dokumententyp</th>
                              <td>' .$arrDocument[0]['dd_dtype'] .'</td>
                            </tr>
                            <tr>
                              <th class="text-nowrap">Ablauf</th>
                              <td>' .strConvertDate($arrDocument[0]['dd_end'], 'Y-m-d', 'd.m.Y') .'</td>
                            </tr>
                            <tr>
                              <th class="text-nowrap">CRM</th>
                              <td><a href="' .$strCrmLink .'" target="_blank">Stammdaten</a></td>
                            </tr>
                          </tbody>
                        </table>
                      
                      </div>
                    </li>
                    <li>
                      <div class="example-col">
                      
                      <strong>Aktivitäten</strong>
                      
                      <div class="stream">
                        ' .$strStream .'
                      </div>
                      </div>
                    </li>
                  </ul>
                  

                    </div>
                    <div class="tab-pane" id="exampleTabsTwo" role="tabpanel" style="min-height: 450px;">
                      <div kng smartchat data-type="topics" data-uri="' .$strIChatUri .'cms/index_neu.php?ac=bd_aktualisieren#' .$intDdId .'" data-name="' .$strDataName .'"></div>
                    </div>
                  </div>
                </div>
              </div>
              
              <script>
                $("[kng]").each(function() { (window.KNG || window.smartchat).addComponent($(this)); });
              </script>
              
              <!-- End Example Tabs -->

  ' .chr(10);
  
  $strReturn.= '                              </div>' .chr(10);
  $strReturn.= '                              <div class="modal-footer">' .chr(10);
  $strReturn.= '                              </div>' .chr(10);
  
  echo $strReturn;
  
}

if ($strAc == 'bgCreateVollmachtClick') {
  
  $intTime = time();
  
  $strProtovol = 'http://';
  if (@$_SERVER['HTTPS'] != '') {
    $strProtovol = 'https://';
  }

  $strSql = 'SELECT `BeitragsjahrBg__c` FROM `izs_bg` WHERE `Id` = "' .$intBgId .'"';
  $arrBg = MySQLStatic::Query($strSql);

  if (count($arrBg) > 0) {
    $intYear = date('Y') - $arrBg[0]['BeitragsjahrBg__c'];
  } else {
    $strSql = 'SELECT `Information_Provider__c`  FROM `Anfragestelle__c` WHERE `Id` = "' .$intBgId .'"';
    $arrAs = MySQLStatic::Query($strSql);

    $strSql = 'SELECT `BeitragsjahrBg__c` FROM `izs_bg` WHERE `Id` = "' .$arrAs[0]['Information_Provider__c'] .'"';
    $arrBg = MySQLStatic::Query($strSql);    
  }

  $intYear = date('Y') - $arrBg[0]['BeitragsjahrBg__c'];
  
  $strHost = $strProtovol .$_SERVER['HTTP_HOST'];
  $strOutputRequest = file_get_contents($strHost .'/cms/action/createVollmachtBg.php?acid=' .$intBgId .'&year=' .$intYear .'&bnr=' .$_REQUEST['bnr']);
  $strHost .'/cms/action/createVollmachtBg.php?acid=' .$intBgId .'&year=' .$intYear .'&bnr=' .$_REQUEST['bnr'];

  $strAdd = '';
  if ($_REQUEST['bnr'] != '') {
    $strAdd = '_' .$_REQUEST['bnr'];
  }
  
  //antwort schicken
  $arrJson = array(
    'Status' => 'OK',
    'bgid' => $intBgId,
    'year' => $intYear,
    'CTime' => date('d.m.Y', $intTime),
    'CTimeH' => date('YmdHis', $intTime),
    'CTimeHf' => date('d.m.Y H:i:s', $intTime),
    'CLink' => '/cms/pdf/vollmacht/bg/' .$intBgId .$strAdd .'_vollmacht.pdf',
    'CRequest' => $strOutputRequest,
    'URL' => $strHost .'/cms/action/createVollmachtBg.php?acid=' .$intBgId .'&year=' .$intYear .'&bnr=' .$_REQUEST['bnr']
  );

  if ($_REQUEST['bnr'] != '') {
    $arrJson['bnr'] = $_REQUEST['bnr'];
  }

  echo json_encode($arrJson);
  exit;
  
}


if ($strAc == 'bgCreateAnfrageClick') {

  $strOnline = -1;
  if (isset($_REQUEST['online']) && ($_REQUEST['online'] == 'on_yes')) {
    $strOnline = 1;
  } elseif (isset($_REQUEST['online']) && ($_REQUEST['online'] == 'on_no')) {
    $strOnline = 0;
  }

  $intTime = time();
  
  $strProtovol = 'http://';
  if (@$_SERVER['HTTPS'] != '') {
    $strProtovol = 'https://';
  }

  $strSql = 'SELECT `BeitragsjahrBg__c` FROM `izs_bg` WHERE `Id` = "' .$intBgId .'"';
  $arrBg = MySQLStatic::Query($strSql);

  if (count($arrBg) > 0) {
    $intYear = date('Y') - $arrBg[0]['BeitragsjahrBg__c'];
  } else {
    $strSql = 'SELECT `Information_Provider__c`  FROM `Anfragestelle__c` WHERE `Id` = "' .$intBgId .'"';
    $arrAs = MySQLStatic::Query($strSql);

    $strSql = 'SELECT `BeitragsjahrBg__c` FROM `izs_bg` WHERE `Id` = "' .$arrAs[0]['Information_Provider__c'] .'"';
    $arrBg = MySQLStatic::Query($strSql);    
  }

  $intYear = date('Y') - $arrBg[0]['BeitragsjahrBg__c'];
  
  $strHost = $strProtovol .$_SERVER['HTTP_HOST'];
  $strOutputRequest = file_get_contents($strHost .'/cms/action/createAnfragelisteBg.php?acid=' .$intBgId .'&year=' .$intYear  .'&bnr=' .$_REQUEST['bnr'] .'&online=' .$strOnline);

  $strAdd = '';
  if ($_REQUEST['bnr'] != '') {
    $strAdd = '_' .$_REQUEST['bnr'];
  }
    
  //antwort schicken
  $arrJson = array(
    'Status' => 'OK',
    'bgid' => $intBgId,
    'year' => $intYear,
    'ATime' => date('d.m.Y', $intTime),
    'ATimeH' => date('YmdHis', $intTime),
    'ATimeHf' => date('d.m.Y H:i:s', $intTime),
    'XLink' => '/cms/pdf/anfrage/bg/' .$intBgId .$strAdd .'_anfrage.xlsx?' .substr(md5(uniqid(mt_rand(), true)), 0, 4) .'',
    'ALink' => '/cms/pdf/anfrage/bg/' .$intBgId .$strAdd .'_anfrage.pdf?' .substr(md5(uniqid(mt_rand(), true)), 0, 4) .'',
    'ARequest' => $strOutputRequest,
    'URL' => $strHost .'/cms/action/createAnfragelisteBg.php?acid=' .$intBgId .'&year=' .$intYear  .'&bnr=' .$_REQUEST['bnr'] .'&online=' .$strOnline
  );

  if ($_REQUEST['bnr'] != '') {
    $arrJson['bnr'] = $_REQUEST['bnr'];
  }

  echo json_encode($arrJson);
  exit;
  
}


if ($strAc == 'bgSendAnfrageClick') {

  $boolDebug = false;

  $boolDez = false;

  $strSql = 'SELECT `BeitragsjahrBg__c` FROM `izs_bg` WHERE `Id` = "' .$intBgId .'"';
  $arrBg = MySQLStatic::Query($strSql);

  if (count($arrBg) > 0) {
    $intYear = date('Y') - $arrBg[0]['BeitragsjahrBg__c'];
  } else {
    $strSql = 'SELECT `Information_Provider__c`  FROM `Anfragestelle__c` WHERE `Id` = "' .$intBgId .'"';
    $arrAs = MySQLStatic::Query($strSql);

    $strSql = 'SELECT `BeitragsjahrBg__c` FROM `izs_bg` WHERE `Id` = "' .$arrAs[0]['Information_Provider__c'] .'"';
    $arrBg = MySQLStatic::Query($strSql);    
  }

  $intYear = date('Y') - $arrBg[0]['BeitragsjahrBg__c'];  

  $strDezId = '';
  $arrDez = $objIp->arrDezentraleAnfrage($intBgId);

  if (count($arrDez) > 0) {
    $strDezId = $intBgId;
    $intBgId = $arrDez[0]['Information_Provider__c'];
    $boolDez = true;
  
    $arrAnfragestelle = $objBg->arrGetAnfragestelle($strDezId);
    $arrBg[0]['Anrede_Anschreiben__c'] = $arrAnfragestelle[0]['Anrede__c'];
    $arrBg[0]['Nachname__c']  = $arrAnfragestelle[0]['Name__c'];
    $arrBg[0]['Anfrage_Email_s__c'] = $arrAnfragestelle[0]['E_Mail_Adressen__c'];

    if ($arrBg[0]['Betreff_Zusatzinfo__c'] == '') {
      $arrBg[0]['Betreff_Zusatzinfo__c'] = $arrAnfragestelle[0]['Betreff_Zusatzinfo__c'];
    }

    $strSql = 'SELECT * FROM `Account` WHERE `Id` = "' .$intBgId .'"';
    $arrBg0 = MySQLStatic::Query($strSql);

    $arrBg[0]['Name'] = $arrBg0[0]['Name'];
    $arrBg[0]['Anfrageliste_XLS__c'] = $arrBg0[0]['Anfrageliste_XLS__c'];

  } else {
    $strBgId = $intBgId; 
  }

  $intTime = time();

  $strSql = 'SELECT * FROM `cms_text` WHERE ct_id = "' .$intCtId .'"';
  $arrText = MySQLStatic::Query($strSql);

  if ($boolDez == false) {
    $strSql = 'SELECT * FROM `Account` WHERE `Id` = "' .$intBgId .'"';
    $arrBg = MySQLStatic::Query($strSql);
  }

  //print_r($arrBg); die();

  $strSalut = '';
  if ($arrBg[0]['Anrede_Anschreiben__c'] != '') {
    $strSalut.= $arrBg[0]['Anrede_Anschreiben__c'];
  } else {
    $strSalut.= 'Sehr geehrte Damen und Herren';
  }
  
  if ((strstr($strSalut, 'geehrte Frau') || strstr($strSalut, 'geehrter Herr')) && ($arrBg[0]['Nachname__c'] != '')) {
    $strSalut.= ' ' .$arrBg[0]['Nachname__c'];
  } else {
    $strSalut = 'Sehr geehrte Damen und Herren';
  }
  
  
  $arrText = $objBgEvent->arrGetAnschreiben($intCtId);

  $arrPlaceholder= [];
  if ($_REQUEST['bnr'] != '') {

    // SELECT * FROM `izs_bg_event` WHERE `be_meldestelle` LIKE '0013000000FjoUXAAZ' AND `be_bg` LIKE '0013000000d8o7gAAA' AND `be_status_bearbeitung` = 'angefragt'
    $strSqlBnr = 'SELECT * FROM `izs_bg_event` WHERE `be_meldestelle` = "' .$_REQUEST['bnr'] .'" AND `be_bg` = "' .$intBgId .'" AND `be_status_bearbeitung` NOT IN ("veröffentlicht", "zurückgestellt von IZS")';
    $arrSqlBnr = MySQLStatic::Query($strSqlBnr);

    if (isset($arrSqlBnr[0])) {
      $arrPlaceholder = $objTemplate->getBgPlaceholder($arrSqlBnr[0]['beid']);
    }

     //$_REQUEST['bnr']
  }
  
  //echo $_REQUEST['bnr']; print_r($arrPlaceholder); die();

  $strText = $arrText[0]['ct_text']; //ct_name

  $strText = str_replace('{Salutation}', $strSalut, $strText);
  $strText = str_replace('{Salutation}', $strSalut, $strText);

  //en, {Beitragszahler}, {Unternehmensnummer}, bitten wir um Auskunft zur Beitragsfälligkeit {Faelligkeit}.

  //print_r($arrAcc[0]); die();

  if ($arrBg[0]['Betreff_Zusatzinfo__c'] != '') {
    $strSubjectAdd = $arrBg[0]['Betreff_Zusatzinfo__c'] .' - ';
  } else {
    $strSubjectAdd = '';
  }

  $strId = ($boolDez) ? $strDezId : $strBgId;

  $strFrom = 'auskunft-bg@izs-institut.de';
  $to = $arrBg[0]['Anfrage_Email_s__c'];

  if ($boolDebug) $to = 'system@izs-institut.de';
  
  $strAdd = '';
  if ($_REQUEST['bnr'] != '') {
    $strAdd = '_' .$_REQUEST['bnr'];
  }

  $files = array();
  $strVollmachtName = 'Vollmacht_' .str_replace('/', '_', $arrBg[0]['Name']) .'.pdf';
  $strVollmacht = '../pdf/vollmacht/bg/' .$strId .$strAdd .'_vollmacht.pdf';

  $strAnfrageName = 'Anfrageliste_' .str_replace('/', '_', $arrBg[0]['Name']) .'.pdf';
  $strAnfrage = '../pdf/anfrage/bg/' .$strId .$strAdd .'_anfrage.pdf';

  $strExcel = '../pdf/anfrage/bg/' .$strId .$strAdd .'_anfrage.xlsx';
  $strExcelName = 'Anfrageliste_' .str_replace('/', '_', $arrBg[0]['Name']) .'.xlsx';

  if (($arrBg[0]['Anfrageliste_XLS__c'] == 'XLS (mit Adresse & Vollmacht)') || ($intBgId == '0013000001HH0sBAAT')) {

    $files[] = array($strExcel, $strExcelName);
  
  } elseif (trim($arrBg[0]['Anfrageliste_XLS__c']) == '')  {
  
    if (!isset($_REQUEST['bnr']) || ($_REQUEST['bnr'] == '')) {
      $files[] = array($strAnfrage, $strAnfrageName); 
    }
    $files[] = array($strVollmacht, $strVollmachtName);
  
  }

  $subject = $strSubjectAdd .$arrText[0]['ct_head'];

  if ($_REQUEST['bnr'] != '') {

    $strSql = 'SELECT `Mitgliedsnummer_BG__c`, `Unternehmensnummer_BG__c` FROM `Verbindung_Meldestelle_BG__c` ';
    $strSql.= 'WHERE `Meldestelle__c` = "' .$_REQUEST['bnr'] .'" AND `Berufsgenossenschaft__c` = "' .$intBgId .'" AND ';
    $strSql.= '`Aktiv__c` = "true" AND ((`Befristet_bis__c` > "' .date('Y-m-d') .'") OR (`Befristet_bis__c` = "0000-00-00"))';
    $arrSql = MySQLStatic::Query($strSql);

    if (count($arrSql) > 0) {
      $subject = $arrSql[0]['Unternehmensnummer_BG__c'] .': ' .$subject;
    }

  }

  $message = strip_tags($strText);

  if ($boolDebug) { $subject.= ' {' .$arrBg[0]['Anfrage_Email_s__c'] .'}'; }

  if (count($arrPlaceholder) > 0) {
    foreach ($arrPlaceholder as $strFind => $strReplace) {

      //echo "$strFind => $strReplace" .chr(10);

      $message = str_replace($strFind, $strReplace, $message); 
      $strText = str_replace($strFind, $strReplace, $strText);  
      $subject = str_replace($strFind, $strReplace, $subject);
    }
  }
  //var_dump((count($arrPlaceholder) > 0));
  //echo $subject .chr(10);
  //echo $message .chr(10); 
  //echo $strText .chr(10);
  //die();
  //$to = 'fl@izs-institut.de';

  //{}: Auskunft zur Beitragsfälligkeit {Beitragsfälligkeit}

  //echo $subject .chr(10); echo $strText; die();

  multi_attach_mail_bg($to, $files, $subject, $message, $strText, '', '', 'auskunft-bg@izs-institut.de');
  //echo $message; print_r($files);die();
  //multi_attach_mail_bg('system@izs-institut.de', $files, $to .': ' .$subject, $message, $strText);
  //multi_attach_mail_bg('km@izs-institut.de', $files, $to .': ' .$subject, $message, $strText);
  //multi_attach_mail_bg('system@izs-institut.de', $files, $to .': ' .$subject, $message, $strText);

  if ($boolDebug) {
    //die();
  }

  //Send Mail to toppic (chat)
  $intUser = $_SESSION['id'];

  $arrHeader = array(
    'apikey: e9d99940-1aed-414a-b836-b929fe97d093',
    'user: ' .$intUser,
    'Accept: application/json'
  );

  //Create User List
  $arrRes = curl_get('http://api.izs-institut.de/api/sg/user', array(), array(), $arrHeader);
  $arrUserList = json_decode($arrRes, true);

  //print_r($arrUserList); die();

  if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
    $arrHeader[] = 'test: true';
  }

  if ($_REQUEST['bnr'] != '') {
    $arrOutputRequest = $objRequest->intSetRequest($strId, $intYear, $_REQUEST['bnr']);
  } else {
    $arrOutputRequest = $objRequest->intSetRequest($strId, $intYear);
  }
  
  $boolOutputLog    = $objSendRequest->boolWriteLog($strId, $intYear, $_REQUEST['bnr']);
  $boolOutputLog = 1;

  $strSRequest = '';

  //echo print_r($arrOutputRequest); die();

  if (is_array($arrOutputRequest) && (count($arrOutputRequest) > 0)) {

    $strSRequest = '';


    $intEsc = strtotime($strEscDate); //$strEscDate UNSET???
    if ($intEsc <= 0) {
      $intEsc = time();
    }
    
    $intEscDate = ($intEsc + 43200 + (86400 * 7)) * 1000; // 12:00 Uhr (+ 7 Tage)

    foreach ($arrOutputRequest as $intKey => $strEventName) {

      //echo $strEventName['be_name']; die();
      //$strEventName['be_name'] = 'BG-002775';
      
      $arrGet = array(
        'from' => 0,
        'q' => $strEventName['be_name'],
        'state' => 'open',
        'type' => 'topic'
      );
    
      //Create User List
      $arrRes = curl_get('http://api.izs-institut.de/api/sg/browse/topic', $arrGet, array(), $arrHeader);
      $arrToppicList = json_decode($arrRes, true);

      //print_r($arrToppicList); die();

      if (isset($arrToppicList['items'][0]['metadata']['email']) && ($arrToppicList['items'][0]['metadata']['email'] != '')) {

        $strTopicId = $arrToppicList['items'][0]['id'];
        //$strTopicId = 'topic-348c56d0-7fbf-450a-9700-d2fbe6da56f4'; //
        //$arrToppicList['items'][0]['metadata']['email'] = '44ztwsh41cmpx3ixgws37de6w@izs-institut.net';

        //echo $strTopicId; die();

        //change Name & Due (+7 Tage)
        $arrPart = explode(':', $arrToppicList['items'][0]['title']);
        if (true) { //($arrPart[0] == 'anfordern') {
          
          if (isset($arrToppicList['items'][0]['target']) && ($arrToppicList['items'][0]['target'] != '')) {

            $arrContext['id'] = $arrToppicList['items'][0]['target'];
            //$arrContext['id'] = 'context-46a6cc25-d809-43a0-a8ed-c6fcdf0910aa';

            $arrPost = array(
              'author' => $arrUserList[$intUser],
              'context' => $arrContext['id'],
              'text' => '<p>E-Mail ("' .$arrText[0]['ct_name'] .'") gesendet am ' .date('d.m.Y') .' um ' .date('H:i:s') .' an folgende Empfänger:<br />' .$to .'</p>',
              'target' => $strTopicId,
              'topic' => $strTopicId,
              'type' => 'content',
              'contenttype' => 'message'
            );
    
            $arrRes = curl_post('http://api.izs-institut.de/api/sg/content', $arrPost, array(), $arrHeader);
            //echo $arrRes; die();

          }

          if ($arrPart[0] == 'anfordern') {
            $arrTopic = array(
              'title' => str_replace('anfordern:', 'Warten auf Eingang:', $arrToppicList['items'][0]['title']),
              'due' => (int) $intEscDate
            );
          }
    
          $arrRes = curl_post('http://api.izs-institut.de/api/sg/topic/' .$strTopicId, $arrTopic, array(), $arrHeader);
          $arrTop = json_decode($arrRes, true);

        }

        multi_attach_mail_bg($arrToppicList['items'][0]['metadata']['email'], $files, $subject, $message, $strText, '', '','auskunft-bg@izs-institut.de');
        //multi_attach_mail_bg('system@izs-institut.de', $files, $arrToppicList['items'][0]['metadata']['email'] .': ' .$subject, $message, $strText);
        //multi_attach_mail_bg('system@izs-institut.de', $files, $arrToppicList['items'][0]['metadata']['email'] .': ' .$subject, $message, $strText);
      }

    }

  }
  
  //antwort schicken
  $arrJson = array(
    'Status' => 'OK',
    'bgid' => $strId,
    'ctid' => $intCtId,
    'year' => $intYear,
    'STime' => date('d.m.Y', $intTime),
    'STimeH' => date('YmdHis', $intTime),
    'SRequest' => $strSRequest,
    'SLog' => $boolOutputLog,
    'Topic' => $strTopicId
  );

  if ($_REQUEST['bnr'] != '') {
    $arrJson['bnr'] = $_REQUEST['bnr'];
  }
  
  echo json_encode($arrJson);
  exit;
  
}

?>
