<?php

session_start();
error_reporting(E_ALL);
ini_set("display_errors", 1);

if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
  define('APP_PATH', '/data/www/sites/test.izs.de/html/');
} else {
  define('APP_PATH', '/data/www/sites/www.izs.de/html/');
}

//CLASSES
require_once(APP_PATH .'cms/classes/class.user.php');

require_once(APP_PATH .'cms/classes/class.premiumpayer.php');
require_once(APP_PATH .'cms/classes/class.contact.php');
require_once(APP_PATH .'cms/classes/class.document.php');
require_once(APP_PATH .'assets/classes/class.database.php');

require_once(APP_PATH .'cms/classes/class.stream.php');
require_once(APP_PATH .'cms/classes/class.multimail.php');

require_once(APP_PATH .'cms/classes/class.request.php');
require_once(APP_PATH .'cms/classes/class.sendrequest.php');

require_once(APP_PATH .'cms/functions.inc.php');

require_once(APP_PATH .'cms/classes/class.bgevent.php');
require_once(APP_PATH .'cms/classes/class.bg.php');

require_once(APP_PATH .'cms/classes/class.additional.php');

//REQUEST
$strAc   = @$_REQUEST['ac'];

//OBJECTS
$objDocument = new Document;
$objAdd      = new Additional;

//CONST

//FUNCTIONS

if ($strAc == 'addValue') {
  
  $_REQUEST['id'] = explode('_', $_REQUEST['id'])[1];
  
  $arrElement = array();
  $arrElement['ad_table'] = $_REQUEST['table'];
  $arrElement['ad_id']    = $_REQUEST['id'];
  $arrElement['ad_key']   = $_REQUEST['key'];
  $arrElement['ad_value'] = $_REQUEST['value'];
  
  $objAdd->boolInsert($arrElement);
  
  //table: "Dokument_Dokumentation__c", id: $(this).prop("id"), key: "received", value: $(this).checked
  //print_r($_REQUEST);
  
  echo 1;
  exit;
  
}

?>