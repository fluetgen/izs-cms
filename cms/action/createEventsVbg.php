<?php




die();





session_start();
error_reporting(E_ALL);
ini_set("display_errors", 1); 

if (strstr(__DIR__, 'test.') !== false) {
  define('APP_PATH', '/data/www/sites/test.izs.de/html/');
  $strPath = 'test.izs.de';
} else {
  define('APP_PATH', '/data/www/sites/www.izs.de/html/');
  $strPath = 'www.izs.de';
}



//CLASSES
require_once(APP_PATH .'cms/classes/class.user.php');
require_once(APP_PATH .'cms/classes/class.bgevent.php');


//REQUEST

//OBJECTS
$objBgEvent  = new BgEvent;
$objBg       = new Bg;
$intNewYear  = 2017;

//CONST

//FUNCTIONS

$strSql = 'SELECT `Meldestelle__c`, `Berufsgenossenschaft__c`, `Account`.`Name` FROM `Verbindung_Meldestelle_BG__c` ';
$strSql.= 'INNER JOIN  `Account` ON  `Meldestelle__c` =  `Account`.`Id` WHERE  `Berufsgenossenschaft__c` = "0013000001HH0sBAAT" ';
$strSql.= 'AND  `Verbindung_Meldestelle_BG__c`.`Aktiv__c` = "true" ORDER BY  `Account`.`Name` ASC';
$arrResult = MySQLStatic::Query($strSql);
$intCountResult = count($arrResult);

if ($intCountResult > 0) {
  
  foreach ($arrResult as $intKey => $arrBgEvent) {

    $arrEventInsert = array (
      'be_meldestelle' => $arrBgEvent['Meldestelle__c'],
      'be_bg' => $arrBgEvent['Berufsgenossenschaft__c'],
      'be_jahr' => $intNewYear,
      'be_befristet' => ($intNewYear + 1) .'-05-31'
    );

    $objBgEvent->boolInsertSingleEvent($arrEventInsert);

  }

}

echo $intCountResult .' Events angelegt.';

?>