<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

session_start();

if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
  define('APP_PATH', '/data/www/sites/test.izs.de/html/');
} else {
  define('APP_PATH', '/data/www/sites/www.izs.de/html/');
}

require_once(APP_PATH .'cms/const.inc.php');

//CLASSES
require_once(APP_PATH .'assets/classes/class.mysql.php');
require_once(APP_PATH .'cms/fpdf17/fpdf.php');
require_once(APP_PATH .'cms/fpdf17/fpdi.php');

require_once(APP_PATH .'cms/fpdf17/classes/fpdfmulticell.php');
require_once(APP_PATH .'cms/fpdf17/mypdf-multicell.php');

require_once(APP_PATH .'cms/classes/class.bg.php');
require_once(APP_PATH .'cms/classes/class.bgevent.php');
require_once(APP_PATH .'cms/classes/class.request.php');
require_once(APP_PATH .'cms/classes/class.premiumpayer.php');
require_once(APP_PATH .'cms/classes/class.informationprovider.php');


class concat_pdf extends fpdi {
  var $files = array();
  function concat_pdf($orientation='P',$unit='mm',$format='A4') {
    parent::__construct($orientation,$unit,$format);
  }
  function setFiles($files) {
    $this->files = $files;
  }
  function concat() {
    foreach($this->files AS $file) {
      $pagecount = $this->setSourceFile($file);
      for ($i = 1;  $i <= $pagecount;  $i++) {
       $tplidx = $this->ImportPage($i);
       $this->AddPage();
       $this->useTemplate($tplidx);
      }
    }
  }

  function WriteText($text) {
  
    $intPosIni = 0;
    $intPosFim = 0;
    $intLineHeight = 5;
    
    if (strpos($text,'<')!==false && strpos($text,'[')!==false) {
      if (strpos($text,'<')<strpos($text,'[')) {
        $this->Write($intLineHeight,substr($text,0,strpos($text,'<')));
        $intPosIni = strpos($text,'<');
        $intPosFim = strpos($text,'>');
        $this->SetFont('','B');
        $this->Write($intLineHeight,substr($text,$intPosIni+1,$intPosFim-$intPosIni-1));
        $this->SetFont('','');
        $this->WriteText(substr($text,$intPosFim+1,strlen($text)));
      } else {
        $this->Write($intLineHeight,substr($text,0,strpos($text,'[')));
        $intPosIni = strpos($text,'[');
        $intPosFim = strpos($text,']');
        $w=$this->GetStringWidth('a')*($intPosFim-$intPosIni-1);
        $this->Cell($w,$this->FontSize+0.75,substr($text,$intPosIni+1,$intPosFim-$intPosIni-1),1,0,'');
        $this->WriteText(substr($text,$intPosFim+1,strlen($text)));
      }
    } else {
      if (strpos($text,'<')!==false) {
        $this->Write($intLineHeight,substr($text,0,strpos($text,'<')));
        $intPosIni = strpos($text,'<');
        $intPosFim = strpos($text,'>');
        $this->SetFont('','B');
        $this->WriteText(substr($text,$intPosIni+1,$intPosFim-$intPosIni-1));
        $this->SetFont('','');
        $this->WriteText(substr($text,$intPosFim+1,strlen($text)));
      } elseif (strpos($text,'[')!==false) {
        $this->Write($intLineHeight,substr($text,0,strpos($text,'[')));
        $intPosIni = strpos($text,'[');
        $intPosFim = strpos($text,']');
        $w=$this->GetStringWidth('a')*($intPosFim-$intPosIni-1);
        $this->Cell($w,$this->FontSize+0.75,substr($text,$intPosIni+1,$intPosFim-$intPosIni-1),1,0,'');
        $this->WriteText(substr($text,$intPosFim+1,strlen($text)));
      } else {
        $this->Write($intLineHeight,$text);
      }
    }
  }
  
  function SetDash($black=false, $white=false) {
      if($black and $white) {
          $s=sprintf('[%.3f %.3f] 0 d', $black*$this->k, $white*$this->k);
      } else {
          $s='[] 0 d';
      }
      $this->_out($s);
  }

}


//REQUEST
$strBgId = @$_REQUEST['acid'];
$intYear = @$_REQUEST['year'];
$strDest = @$_REQUEST['show'];
$intCtId = @$_REQUEST['ctid'];

$boolDez = false;

//DATA
$objBg  = new Bg;
$objReq = new Request;
$pdf    = new concat_pdf();

$objBgEvent = new BgEvent;

$objIp = new InformationProvider;

$strDezId = '';
$arrDez = $objIp->arrDezentraleAnfrage($strBgId);
if (count($arrDez) > 0) {
  $strDezId = $strBgId;
  $strBgId = $arrDez[0]['Information_Provider__c'];
  $boolDez = true;

  $arrAnfragestelle = $objBg->arrGetAnfragestelle($strDezId);
  $arrBg[0]['ac_req_salut'] = $arrAnfragestelle[0]['Anrede__c'];
  $arrBg[0]['ac_req_last']  = $arrAnfragestelle[0]['Name__c'];
}

if (empty($intYear) || ($intYear == '')) {
  $intYear = date('Y') - 1;
}



$strOutput = '';


$pdf = new myPDF();
$pdf->AliasNbPages();

$pdf->AddFont('OfficinaSans','','OpenSans-Regular.php');
$pdf->AddFont('OpenSans','','OpenSans-Regular.php');
$pdf->AddFont('OpenSans','U','OpenSans-Regular.php');
$pdf->AddFont('OpenSans','B','OpenSans-Bold.php');
$pdf->AddFont('OpenSans','I','OpenSans-Italic.php');
$pdf->AddFont('OpenSans','BI','OpenSans-BoldItalic.php');
$pdf->AddFont('symbols','','symbols.php');

$pdf->AddPage();
$pdf->SetAutoPageBreak(true, 20);

$pdf->SetMargins(15, 60, 15);

$oMulticell = new FpdfMulticell($pdf);
$oMulticell->SetStyle("p", "OpenSans", "", 10, "0,0,0");
$oMulticell->SetStyle("b", "OpenSans", "B", 10, "0,0,0");
$oMulticell->setStyle("i", "OpenSans", "I", 10, "0,0,0");
$oMulticell->setStyle("u", "OpenSans", "U", 10, "0,0,0");
$oMulticell->setStyle("ub", "OpenSans", "UB", 10, "0,0,0");
$oMulticell->setStyle("bu", "OpenSans", "BU", 10, "0,0,0");

$oMulticell->SetStyle("h1", "OpenSans", "", 16, "0,0,0");
$oMulticell->SetStyle("h3", "OpenSans", "B", 10, "0,0,0");
$oMulticell->SetStyle("h4", "OpenSans", "BI", 10, "0,0,0");
$oMulticell->SetStyle("hh", "OpenSans", "B", 10, "0,0,0");


$arrBg     = $objBg->arrGetBg($strBgId);
$arrBgView = $objBg->arrGetBgView($strBgId);

//print_r($arrBg); die();
/*
if (count($arrBg) > 0) {
  $strAdress = '';
  $strAdress.= '<p>';
  $strAdress.= $arrBg[0]['ac_name'] .'<br>';
  if ($arrBg[0]['ac_req_abteilung'] != '') {
    $strAdress.= $arrBg[0]['ac_req_abteilung'] .'<br>';
  }
  if ($arrBg[0]['ac_req_salut'] != '') {
    $strAdress.= $arrBg[0]['ac_req_salut'];
    if ($arrBg[0]['ac_req_first'] != '') {
      $strAdress.= ' ' .$arrBg[0]['ac_req_first'];
    }
    if ($arrBg[0]['ac_req_last'] != '') {
      $strAdress.= ' ' .$arrBg[0]['ac_req_last'];
    }
    $strAdress.= '<br>';
  }
  if ($arrBg[0]['ac_req_street'] != '') {
    $strAdress.= $arrBg[0]['ac_req_street'] .'<br>';
  }
  $strAdress.= $arrBg[0]['ac_req_zip'] .' ' .$arrBg[0]['ac_req_city'];
  $strAdress.= '</p>';
  
  if (false) {
    $pdf->SetY(58);
    $oMulticell->multiCell(180, 5, utf8_decode(str_replace('<br>', "\n", $strAdress)), 0, 'L');
  }
}
*/
/*
if (($_REQUEST['type'] == 'E-Mail') || ($_REQUEST['type'] == 'Fax')) {
  $pdf->Image('pdf/briefpapier_s1_vorlage_schwarz.png', 0, 0, 210, 297, 'PNG');
}
*/

$strSalut = '';
if ($arrBg[0]['ac_req_salut'] != '') {
  $strSalut.= $arrBg[0]['ac_req_salut'];
} else {
  $strSalut.= 'Sehr geehrte Damen und Herren';
}

if ((strstr($strSalut, 'geehrte Frau') || strstr($strSalut, 'geehrter Herr')) && ($arrBg[0]['ac_req_last'] != '')) {
  $strSalut.= ' ' .$arrBg[0]['ac_req_last'];
} else {
  $strSalut = 'Sehr geehrte Damen und Herren';
}


$arrText = $objBgEvent->arrGetAnschreiben($intCtId);
$strText = $arrText[0]['ct_text'];

//print_r($arrText);


$strText = str_replace("\n", '', $arrText[0]['ct_text']);
$strText = str_replace("</p>", "</p>\n\n", $strText);
//$strText = str_replace("<br>", "<br>\n", $strText);
$strText = str_replace("</li>", "</li>\n", $strText);
$strText = str_replace("</ul>", "</ul>\n", $strText);

$strText = str_replace('{Salutation}', $strSalut, $strText);
$strText = str_replace('{Today}', date('d.m.Y'), $strText);
$strText = str_replace('{Year}', $intYear, $strText);
$strText = str_replace('{BgYear}', date('Y') - $arrBgView[0]['BeitragsjahrBg__c'], $strText);

$strText = str_replace('<br>', "\n", $strText);
$strText = str_replace('<u><b>', "<bu>", $strText);
$strText = str_replace('</u></b>', "</bu>", $strText);
$strText = str_replace('<b><u>', "<bu>", $strText);
$strText = str_replace('</b></u>', "</bu>", $strText);


$arrParts = array();
$arrMatches = array();
$strPattern = "/<div.*?>([^`]*?)<\/div>/";
preg_match_all($strPattern, $strText, $arrMatches);

//print_r($arrMatches[0]); //die();
    
if (count($arrMatches[0]) > 0) {
  
  foreach ($arrMatches[0] as $intKey => $strFound) {
    $arrTempParts = explode($strFound, $strText);
    
    if (strstr($strFound, 'right') != false) {
      $strAlign = 'R';
    } elseif (strstr($strFound, 'center') != false) {
      $strAlign = 'C';
    } elseif (strstr($strFound, 'left') != false) {
      $strAlign = 'L';
    } else {
      $strAlign = 'J';
    }
    
    if ($arrMatches[1][$intKey][0] != '<') {
      $arrMatches[1][$intKey] = '<p>' .$arrMatches[1][$intKey] .'</p>';
    }
      
    if ($arrTempParts[0] == '') { //Anfang
      $arrParts[] = array($strAlign, $arrMatches[1][$intKey]);
      if ($intKey == (count($arrMatches[0]) - 1)) {
        $arrParts[] = array('J', $arrTempParts[1]);
      }
      $strText = $arrTempParts[1];
    } elseif ($arrTempParts[1] == '') { // Ende
      $arrParts[] = array('J', $arrTempParts[0]);
      $arrParts[] = array($strAlign, $arrMatches[1][$intKey]);
      $strText = '';
    } else { // Mitte
      $arrParts[] = array('J', $arrTempParts[0]);
      $arrParts[] = array($strAlign, $arrMatches[1][$intKey]);
      if ($intKey == (count($arrMatches[0]) - 1)) {
        $arrParts[] = array('J', $arrTempParts[1]);
      }
      $strText = $arrTempParts[1];
    }
    
  }

} else {
  $arrParts[] = array('J', $strText);
}

$arrParts[1] = str_replace('Portal www.izs.de', 'Portal <a href="https://www.izs.de/">www.izs.de</a>', $arrParts[1]);
$arrParts[1] = str_replace('unter www.izs.de/orizon', 'unter <a href="https://www.izs.de/orizon/">www.izs.de/orizon</a>', $arrParts[1]);

//print_r($arrParts);

$pdf->SetY(84);
$pdf->SetFont('OpenSans','',10);

foreach($arrParts as $intKey => $arrPart) {
  $oMulticell->multiCell(180, 5, utf8_decode($arrPart[1]), 0, $arrPart[0]);
}

$pdf->Output('Anschreiben - ' .$arrBg[0]['ac_name'] .'.pdf', 'D');



if ($_REQUEST['type'] == 'Post') { 
  
  /*  
  
  $strAnschreiben = 'pdf/anschreiben/' .$strNameFiles .'_anschreiben_sv.pdf';
  if (file_exists($strAnschreiben)) {
    unlink($strAnschreiben);
  }
  $pdf->Output($strAnschreiben, 'F');
  
  
  $strSql = 'SELECT `cs_id`, DATE_FORMAT(`cs_sent`, "%d.%m.%Y_%H%i%s") AS `cs_sent` FROM `cms_sent` WHERE `cs_account_id` = "' .$arrAcc[0]['Id'] .'" AND `cs_month` = ' .$arrPeriod[0] .' AND `cs_year` = ' .$arrPeriod[1] .'';
  $arrSent = MySQLStatic::Query($strSql);
  
  if (count($arrSent) > 0) {
    $intCsId = $arrSent[0]['cs_id'];
    $strSql = 'UPDATE `cms_sent` SET `cs_account_id` = "' .$arrAcc[0]['Id'] .'", `cs_month` = ' .$arrPeriod[0] .', `cs_year` = ' .$arrPeriod[1] .', `cs_type` = "post", `cs_sent` = NOW(), `cs_recipients` = "'.$to .'" WHERE `cs_id` = ' .$intCsId;
    $intSent = MySQLStatic::Update($strSql);
  } else {
    $strSql = 'INSERT INTO `cms_sent` (`cs_account_id`, `cs_month`, `cs_year`, `cs_type`, `cs_sent`, `cs_recipients`) VALUES ("' .$arrAcc[0]['Id'] .'", ' .$arrPeriod[0] .', ' .$arrPeriod[1] .', "post", NOW(), "'.$to .'")';
    $intSent = MySQLStatic::Insert($strSql);
  }
  */
  
}


//echo 1;

?>