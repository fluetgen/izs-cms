<?php

session_start();
error_reporting(E_ALL & ~E_NOTICE);
ini_set("display_errors", 1); 

if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
  define('APP_PATH', '/data/www/sites/test.izs.de/html/');
} else {
  define('APP_PATH', '/data/www/sites/www.izs.de/html/');
}

//CLASSES
require_once(APP_PATH .'cms/classes/class.document.php');
require_once(APP_PATH .'cms/classes/class.group.php');
require_once(APP_PATH .'cms/inc/refactor.inc.php');

//OK!?
require_once(APP_PATH .'cms/classes/class.premiumpayer.php');
require_once(APP_PATH .'cms/classes/class.contact.php');

$objDocument = new Document;


// REQUEST
$_SERVER['SERVER_NAME'] = 'www.izs.de';

$strAc = 'ddStatusClick';
$_SESSION['id'] = 3;
$intDdId = 'a0f3A000008yGadQAE';
//

if ($strAc == 'ddStatusClick') {

    if (isset($_REQUEST['data'])) {
      $intDdId = $_REQUEST['data']['ddid'];
      $_SESSION['id'] = $_REQUEST['data']['_SESSION']['id'];
    }
    
    $arrDocument = $objDocument->arrGetDocument($intDdId);

    //FAKE
    $arrDocument[0]['dd_ustatus'] = ''; // oder ''?

    $intTime = 1637048678; //FAKE
    $strUStatusNew = $objDocument->strGetNewUpdateStatus($arrDocument[0]['dd_ustatus']);

    echo '$strUStatusNew: ' .$strUStatusNew .'<br />' .chr(10);
    echo '$intTime: ' .$intTime .' (' .date('d.m.Y H:i:s', $intTime) .')<br />' .chr(10);
  
    $objGroup = new Group;
    $arrGroup = $objGroup->arrGetGroup($arrDocument[0]['dd_grid']);
  
    if ($arrDocument[0]['dd_ustatus'] == '3. Erinnerung') {
  
      //antwort schicken
      $arrJson = array(
        'Status' => 'OK',
        'ddid' => $intDdId,
        'Action' => 'modal',
        'Name' => 'Archive'
      );
      
      echo json_encode($arrJson);
      exit;
  
    }

    //print_r($arrDocument);
    //print_r($arrGroup);

    //die();

    //Empfänfer ermitteln
    $arrRecipients = $objDocument->arrGetRecipients($intDdId);

    //print_r($arrRecipients);
  
    if (count($arrRecipients) > 0) {
  
      //sf aktualisieren
      if (strstr($_SERVER['SERVER_NAME'], 'test.') === false) {
        //$boolSfStatus = $objDocument->boolUpdateUpdateStatusSf($intDdId, $strUStatusNew, $intTime);
      } 
      
      //FAKE: Update_Status__c = 'Anfrage versandt'
      //$boolDbStatus = $objDocument->boolUpdateUpdateStatusDb($intDdId, $strUStatusNew, $intTime);
      $boolDbStatus = true;
    
      //Stream schreiben
      //$intStream = $objStream->intInsertStream($intDdId, $arrDocument[0]['dd_grid'], 'Update_Status__c', $arrDocument[0]['dd_ustatus'], $strUStatusNew);
      
      if ($arrDocument[0]['dd_ustatus'] == '') { // TRUE
        $strAdd = 'Neues Dokument angefordert.';
        if ($arrDocument[0]['dd_ivisible'] != '') {
          $strAdd.= ' ' .$arrDocument[0]['dd_ivisible'];
        }
        //$intStream = $objStream->intInsertStream($intDdId, $arrDocument[0]['dd_grid'], 'Info_sichtbar__c', $strAdd);
        //$objDocument->boolUpdateField($intDdId, 'Info_sichtbar__c', $strAdd);
        //FAKE = `Info_sichtbar__c` = 'Neues Dokument angefordert.'
      }
      
      
      //mail verschicken
      $strTo    = implode(', ', $arrRecipients);
      $arrFiles = array();
      $strFrom  = 'basisdokumentation@izs-institut.de';
      $strBcc   = $strFrom;
  
      if (strstr($_SERVER['SERVER_NAME'], 'test.') !== false) {
        $strTo  = 'system@izs-institut.de';
        $strBcc = '';
      }
  
      $strSubject = $objDocument->strGetSubject($arrDocument[0]['dd_name'], $strUStatusNew);

      //echo '$strSubject: ' .$strSubject .'<br />' .chr(10);

      $strMessageHtml = $objDocument->strGetMessage($arrDocument[0]['ddid'], $strUStatusNew, $intTime);

      //echo '$strMessageHtml: ' .'<br />' .chr(10) .$strMessageHtml .'<br />' .chr(10);

      $strMessage = $strMessageHtml;
  
      $strUStatusNewForStream = $strUStatusNew;
  
      //MAIL SENDING CUTTED HERE AND PASETED AT THE AND 
  
      //geänderte Daten abrufen
      $arrDocument = $objDocument->arrGetDocument($intDdId);

      //FAKE
      $arrDocument[0]['dd_ustatus'] = 'Anfrage versandt';

      $strUStatusNew = $objDocument->strGetNewUpdateAction($arrDocument[0]['dd_ustatus']);
      echo '$strUStatusNew: ' .$strUStatusNew .'<br />' .chr(10);
  
      $arrDocStatus = $objDocument->arrGetDocumentStatus($arrDocument[0]['ddid']);
      //print_r($arrDocStatus);

      $arrDocStatus[0] = $arrDocStatus[6];
      
      $strAm = '';
      $strEscDate  = '';
      $strEscDateH = '';
      $strDeadlineClass = '';
      if (count($arrDocStatus) > 0) {
        $strAm = strConvertDate($arrDocStatus[0]['bc_time'], 'Y-m-d H:i:s', 'd.m.Y');
        $strEscDate = strAddWorkingDays((strConvertDate($strAm, 'd.m.Y', 'U')), $objDocument->intGetEscalation($arrDocument[0]['dd_ustatus']));
        $strEscDateH = strConvertDate($strEscDate, 'd.m.Y', 'Ymd');
        if (strConvertDate($strEscDate, 'd.m.Y', 'U') <= strConvertDate(date('d.m.Y'), 'd.m.Y', 'U')) {
          $strDeadlineClass = 'red-600';
        }
      }
      
      //FAKE
      $strDeadlineClass = '';
      
      //antwort schicken
      $arrJson = array(
        'Status' => 'OK',
        'ddid' => $intDdId,
        'UTime' => date('d.m.Y', $intTime),
        'UTimeH' => date('Ymd', $intTime),
        'Deadline' => $strEscDate,
        'DeadlineH' => $strEscDateH,
        'UStatus' => $arrDocument[0]['dd_ustatus'],
        'UStatusNew' => $strUStatusNew,
        'Recipients' => '"' .implode('", "', $arrRecipients) .'"',
        'SfResult' => @$boolSfStatus,
        'DbResult' => @$boolDbStatus,
        'StResult' => @$intStream,
        'MaResult' => @$intMail,
        'DeadlineClass' => $strDeadlineClass,
        'InfoExtern' => $arrDocument[0]['dd_ivisible']
      );

      print_r($arrJson);

  
      //CREATE CHAT ON "ANFRAGE SENDEN"
      if (true) {
  
        $intUser = $_SESSION['id'];
  
        $arrTeam = array(
          //32 => 'Serap',
          '47' => 'Ecem',
          '42' => 'Seda',
          '44' => 'Karo', 
          '36' => 'Gordon'
        );
  
        $arrHeader = array(
          'apikey: e9d99940-1aed-414a-b836-b929fe97d093',
          'user: ' .$intUser,
          'Accept: application/json'
        );
  
        if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
          $arrHeader[] = 'test: true';
        }
  
        //Create User List
        $arrRes = curl_get('http://api.izs-institut.de/api/sg/user', array(), array(), $arrHeader);
        $arrUserList = json_decode($arrRes, true);
  
        //Create Context (name, uri)
        $arrContext = array(
          'name' => 'IZS/Basisdokumentation/' .$arrGroup[0]['gr_name'] .'/' .$arrDocument[0]['dd_dtype'],
          'uri' => 'http://www.izs.de/cms/index_neu.php?ac=bd_aktualisieren#' .$intDdId
        );
  
        if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
          $arrContext['uri'] = 'http://test.izs.de/cms/index_neu.php?ac=bd_aktualisieren#' .$intDdId;
        }

        print_r($arrContext);
        

        //$arrRes = curl_post('http://api.izs-institut.de/api/sg/context', $arrContext, array(), $arrHeader);
        $arrContext = json_decode($arrRes, true);

        //FAKE
        $arrContext['id'] = 'context-18276d22-7387-4c88-9816-f31c5232891c';

        echo '$strUStatusNew: ' .$strUStatusNew .'<br />' .chr(10);
  
        if ($strUStatusNew == '1. Erinnerung senden') {
    
          $arrRes = curl_get('http://api.izs-institut.de/api/sg/category', array(), array(), $arrHeader);
          $arrCatList = json_decode($arrRes, true);
  
          $arrMember = array($arrUserList[$intUser]);
    
          $arrFollower = array();
          foreach ($arrTeam as $intTeamId => $strName) {
            if ($intTeamId != $intUser) {
              $arrFollower[] = $arrUserList[$intTeamId];
            }
          }
  
          $arrDocument = $objDocument->arrGetDocument($intDdId);
    
          $intEscDate = (strtotime($strEscDate) + 7200) * 1000; // 2:00 Uhr

          echo '$intEscDate: ' .$intEscDate .'<br />' .chr(10);

          die();
          ///////////////////////          

          $arrTopic = array(
            'title' => 'Warten auf Eingang: ' .$arrGroup[0]['gr_name'] .' - ' .$arrDocument[0]['dd_dtype'] .' - ' .$arrDocument[0]['dd_name'],
            'category' => array(array_search('Basisdokumentation', $arrCatList)),
            'context' => $arrContext['id'],
            'member' => $arrMember,
            'follow' => $arrFollower,
            // 'followup' => array($arrUserList[$_SESSION['id']] => (int) $intEscDate),
            //'text' => '', //'E-Mail Benachrichtigung gesendet am ' .date('d.m.Y') .' um ' .date('H:i:s') .' an folgende Empfänger:<br /><br />' .$strTo,
            'due' => (int) $intEscDate
          );
    
          $arrRes = curl_post('http://api.izs-institut.de/api/sg/topic', $arrTopic, array(), $arrHeader);
          $arrTop = json_decode($arrRes, true);
    
        } else {
  
          $arrRes = curl_get('http://api.izs-institut.de/api/sg/topic/' .$arrContext['id'], array(), array(), $arrHeader);
          $arrTopicList = json_decode($arrRes, true);
  
          if (count($arrTopicList) > 0) {
  
            foreach ($arrTopicList as $intSort => $arrTopic) {
  
              $strTopicId = key($arrTopic);
              $strTopicTitle = current($arrTopic);
  
              if ((strstr($strTopicTitle, 'Warten auf Eingang') !== false) && (strstr($strTopicTitle, $arrDocument[0]['dd_name']) !== false)) {
                break;
              }
  
            }
  
            $intEscDate = (strtotime($strEscDate) + 7200) * 1000; // 2:00 Uhr
  
            $arrTopic = array(
              'due' => (int) $intEscDate
            );
      
            $arrRes = curl_post('http://api.izs-institut.de/api/sg/topic/' .$strTopicId, $arrTopic, array(), $arrHeader);
            $arrTop = json_decode($arrRes, true);
  
            $arrTop['id'] = $strTopicId;
  
          }
  
        }
  
        //Write chat content
        if (!isset($arrTop['id'])) {
          
          $arrRes = curl_get('http://api.izs-institut.de/api/sg/topic/' .$arrContext['id'], array(), array(), $arrHeader);
          $arrTopicList = json_decode($arrRes, true);
  
          foreach ($arrTopicList as $intSort => $arrTopic) {
  
            foreach ($arrTopic as $strId => $strTitle) {
              if (strstr($strTitle, 'Warten auf Eingang: ') !== false) {
                $arrTop['id'] = $strId;
                break;
              }
            }
  
          }
  
        }
  
  
        if (isset($arrTop['id'])) {
  
          $arrPost = array(
            'author' => $arrUserList[$intUser],
            'context' => $arrContext['id'],
            'text' => '<p>E-Mail ("' .$arrDocument[0]['dd_ustatus'] .'") gesendet am ' .date('d.m.Y') .' um ' .date('H:i:s') .' an folgende Empfänger:<br />' .$strTo .'</p>',
            'target' => $arrTop['id'],
            'topic' => $arrTop['id'],
            'type' => 'content',
            'contenttype' => 'message'
          );
  
          $arrRes = curl_post('http://api.izs-institut.de/api/sg/content', $arrPost, array(), $arrHeader);
  
          //update due
          if ($strUStatusNew != '1. Erinnerung senden') {
            ;
          }
  
        }
  
      }
  
      $strMessageAdd = '';
      if (isset($arrTop['id']) && ($arrTop['id'] != '')) {
        $strMessageAdd.= '<hr>' .chr(10);
        $strMessageAdd.= '<p>Bitte diesen Abschnitt nicht verändern und beim Antworten in der E-Mail belassen:<br />' .chr(10);
        $strMessageAdd.= '<a href="https://izs.smartgroups.io/topic/' .$arrTop['id'] .'">' .'__' .$arrTop['id'] .'</a></p>' .chr(10);
        $strMessageAdd.= '<hr>' .chr(10);
      }  
  
      /*
      $strTo  = 'km@izs-institut.de';
      $strTo  = 'system@izs-institut.de';
      $strBcc = 'system@izs-institut.de';
      */
  
      $objEmail = new MultiMail;
      $arrMail = $objEmail->intSendMultiMail($strTo, $arrFiles, $strSubject, $strFrom, $strMessageAdd .$strMessage, $strMessageAdd .$strMessageHtml, $strBcc);
      $arrMail['UStatus'] = $strUStatusNewForStream;
  
      //Stream schreiben
      $intStream = $objStream->intInsertStream($intDdId, $arrDocument[0]['dd_grid'], '', '', serialize($arrMail), 5);
    
    } else {
  
      //antwort schicken
      $arrJson = array(
        'Status' => 'FAIL',
        'ddid' => $intDdId,
        'Reason' => 'Keine Empfänger mit Merkmal "Basisdokumentation" vorhanden.'
      );
  
    }
    
    echo json_encode($arrJson);
    
  }

?>