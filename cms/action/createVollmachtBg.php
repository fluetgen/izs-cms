<?php

session_start();

ini_set('memory_limit', '512M'); 
ini_set('display_errors', '1');

error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
  define('APP_PATH', '/data/www/sites/test.izs.de/html/');
} else {
  define('APP_PATH', '/data/www/sites/www.izs.de/html/');
}

require_once(APP_PATH .'cms/const.inc.php');

//CLASSES
require_once(APP_PATH .'assets/classes/class.mysql.php');
require_once(APP_PATH .'cms/fpdf17/fpdf.php');
require_once(APP_PATH .'cms/fpdf17/fpdi.php');

require_once(APP_PATH .'cms/classes/class.bg.php');
require_once(APP_PATH .'cms/classes/class.bgevent.php');
require_once(APP_PATH .'cms/classes/class.auth.php');
require_once(APP_PATH .'cms/classes/class.premiumpayer.php');
require_once(APP_PATH .'cms/classes/class.informationprovider.php');

require_once(APP_PATH .'cms/inc/refactor.inc.php');


class concat_pdf extends fpdi {
    var $files = array();
    function concat_pdf($orientation='P',$unit='mm',$format='A4') {
        parent::__construct($orientation,$unit,$format);
    }
    function setFiles($files) {
        $this->files = $files;
    }
    function concat() {
        foreach($this->files AS $file) {
            $pagecount = $this->setSourceFile($file);
            for ($i = 1;  $i <= $pagecount;  $i++) {
                 $tplidx = $this->ImportPage($i);
                 $this->AddPage();
                 $this->useTemplate($tplidx);
            }
        }
    }
}


//REQUEST
$strBgId = @$_REQUEST['acid'];
$intYear = @$_REQUEST['year'];
$strDest = @$_REQUEST['show'];
$strBnr  = @$_REQUEST['bnr'];
$strOnl  = @$_REQUEST['online'];

$boolDez = false;

$objIp = new InformationProvider;

$strDezId = '';
$arrDez = $objIp->arrDezentraleAnfrage($strBgId);
if (count($arrDez) > 0) {
  $strDezId = $strBgId;
  $strBgId = $arrDez[0]['Information_Provider__c'];
  $boolDez = true;
}

if (empty($intYear) || ($intYear == '')) {
  $intYear = date('Y') - 1;
}

//DATA
$objBg      = new Bg;
$objBgEvent = new BgEvent;

$objAuth = new Auth;
$objPdf  = new concat_pdf();

$strOutput = '';

$arrAuthList = array();

if ($strBgId != '') {
  
  $arrBg = $objBg->arrGetBg($strBgId);

  $intUser = $_SESSION['id'];

  $arrHeader = array(
    'apikey: bb9494bc-8f19-415f-9ace-cd19fe3a05f6',
    'user: ' .$intUser,
    'Accept: application/json'
  );

  if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
    $arrHeader[] = 'test: true';
  }

  $arrGet = array(
    'bg' => ($boolDez) ? $strDezId : $strBgId,
    'on' => -1 //$strOnl
  );

  //Create User List
  $arrRes = curl_get('http://api.izs-institut.de/api/bg/events/', $arrGet, array(), $arrHeader);

  //var_dump($arrRes); die();

  $arrAnfrage = json_decode($arrRes, true);
  
  //print_r($arrAnfrage); die();
  
  if (count($arrAnfrage) > 0) {
    foreach ($arrAnfrage as $intKey => $arrEvent) {

      if (isset($strBnr) && ($strBnr != '')) {
        if ($arrEvent[2] == $strBnr) {
          $arrAuthList = array_merge($arrAuthList, $objAuth->arrGetAuthLink($arrEvent[2]));
        }
      } else {
        $arrAuthList = array_merge($arrAuthList, $objAuth->arrGetAuthLink($arrEvent[2]));
      }

    }
  }
  
}

//print_r($arrAuthList);

$objPdf->AliasNbPages();

if (count($arrAuthList) > 0) {
  $arrNoDuplicate = array();
  foreach($arrAuthList as $intKey => $strAuthLink) {
  
    if (in_array($strAuthLink, $arrNoDuplicate) === false) {
      
      $objPdf->AddPage();
      $objPdf->SetAutoPageBreak(false);
      $objPdf->Image($strAuthLink, 0, 0, 210, 297, 'JPG');

      //echo $strAuthLink .chr(10);
      
      $arrNoDuplicate[] = $strAuthLink;
    }
  
  }
} else {
  $objPdf->AddPage();
}

//print_r($arrNoDuplicate); die();

// 15.10.23: Keine Ahnnung, warum das doppelt drin war, habe es deaktiviert.
if (0) {

  if (strstr(__DIR__, 'test.') !== false) {
    // INIT -> /download/
    if (!file_exists('/data/www/sites/test.izs.de/html/download')) {
      mkdir('/data/www/sites/test.izs.de/html/download', 0777, true);
    }
    // YEAR -> /download/2017/
    if (!file_exists('/data/test/sites/www.izs.de/html/download/bg')) {
      mkdir('/data/www/sites/test.izs.de/html/download/bg', 0777, true);
    }
    //authority
    $strPathAuthority = '/data/test/sites/www.izs.de/html/download/bg/';
  } else {
    // INIT -> /download/
    if (!file_exists('/data/www/sites/www.izs.de/html/download')) {
      mkdir('/data/www/sites/www.izs.de/html/download', 0777, true);
    }
    // YEAR -> /download/2017/
    if (!file_exists('/data/www/sites/www.izs.de/html/download/bg')) {
      mkdir('/data/www/sites/www.izs.de/html/download/bg', 0777, true);
    }
    //authority
    $strPathAuthority = '/data/www/sites/www.izs.de/html/download/bg/';
  }
  

  $arrDone = array();

  $arrJpg = $arrAuthList;

  foreach($arrJpg as $strFile) {

    $arrFileName = explode('/', $strFile);
    $arrCompanyId = explode('_', $arrFileName[count($arrFileName) - 1]);

    $strSql = 'SELECT `SF42_Company_Group__c` FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$arrCompanyId[0] .'"';
    $arrGroup = MySQLStatic::Query($strSql);
    
    if (!in_array($arrGroup[0]['SF42_Company_Group__c'], $arrDone)) {

      if (count($arrDone) > 0) {

        if (file_exists($strFileName)) {
          //echo 'DELETE FILE: ' .$strFileName .'<br>' .chr(10);
          unlink($strFileName);
        }
        //echo 'WRITE FILE: ' .$strFileName .'<br>' .chr(10);
        $pdf->Output($strFileName, 'F');

      }
      
      $arrDone[] = $arrGroup[0]['SF42_Company_Group__c'];

      $strFileName = $strPathAuthority .'' .$arrGroup[0]['SF42_Company_Group__c'] .'_vollmacht_bg.pdf';
      //echo 'CREATE FILE: ' .$strFileName .'<br>' .chr(10);
      $pdf = new concat_pdf();
      $pdf->AliasNbPages();
    
    }

    $pdf->AddPage();
    $pdf->SetAutoPageBreak(false);
    $pdf->Image($strFile, 0, 0, 210, 297, 'JPG');
    //echo 'ADD PAGE: ' .$strFile .' TO: ' .$strFileName .'<br>' .chr(10);
    
  }

  if (file_exists($strFileName)) {
    //echo 'DELETE FILE: ' .$strFileName .'<br>' .chr(10);
    unlink($strFileName);
  }
  //echo 'WRITE FILE: ' .$strFileName .'<br>' .chr(10);
  if (count($arrJpg) > 0) {
    $pdf->Output($strFileName, 'F');
  }
  
  //die();

}


//echo 'here'; die();


if ($strDest == 'D') {
  
  $objPdf->Output(date('Y.m.d_His - ') .'Vollmacht - ' .$arrBg[0]['ac_name'] .'.pdf', 'D');
  
} else {

  $strId = ($boolDez) ? $strDezId : $strBgId;

  $strAdd = '';
  if (isset($strBnr) && ($strBnr != '')) {
    $strAdd = '_' .$strBnr;
  } else {
    $strBnr = '';
  }

  $strFileName = CMS_PATH .'pdf/vollmacht/bg/' .$strId .$strAdd .'_vollmacht.pdf';
  //die();

  if (file_exists($strFileName)) {
    unlink($strFileName);
  }
  $objPdf->Output($strFileName, 'F');
  
  $strOutput = $objAuth->boolWriteLog($strId, $intYear, $strBnr);
  
  //header('Location: /cms/index_neu.php?ac=bg_events&year=' .$intYear .'');

}

echo $strOutput;

?>
