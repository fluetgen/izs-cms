<?php

session_start();
error_reporting(E_ALL);
ini_set("display_errors", 1);

if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
  define('APP_PATH', '/data/www/sites/test.izs.de/html/');
} else {
  define('APP_PATH', '/data/www/sites/www.izs.de/html/');
}

//CLASSES
require_once(APP_PATH .'cms/classes/class.user.php');

require_once(APP_PATH .'cms/classes/class.premiumpayer.php');
require_once(APP_PATH .'cms/classes/class.contact.php');
require_once(APP_PATH .'cms/classes/class.document.php');
require_once(APP_PATH .'assets/classes/class.database.php');

require_once(APP_PATH .'cms/classes/class.stream.php');
require_once(APP_PATH .'cms/classes/class.multimail.php');

require_once(APP_PATH .'cms/classes/class.request.php');
require_once(APP_PATH .'cms/classes/class.sendrequest.php');

require_once(APP_PATH .'cms/functions.inc.php');

require_once(APP_PATH .'cms/inc/refactor.inc.php');

require_once(APP_PATH .'cms/classes/class.bgevent.php');
require_once(APP_PATH .'cms/classes/class.bg.php');


//WICHTIG!!! http lassen, sonst funktionieren die alten sg-Links nicht mehr.
//KEIN HTTPS
$strCmsUrl = 'http://www.izs.de/';


//REQUEST
$strAc   = @$_REQUEST['ac'];
$intDdId = @$_REQUEST['ddid'];
$intBgId = @$_REQUEST['bgid'];
$intBeId = @$_REQUEST['beid'];
$intCtId = @$_REQUEST['ctid'];
$intYear = @$_REQUEST['year'];
$strVal  = @$_REQUEST['value'];

$arrFields = @$_REQUEST['values'];

//OBJECTS
$objDocument = new Document;
$objStream   = new Stream('bgevent');
$objUser     = new User;

$objRequest     = new Request;
$objSendRequest = new SendRequest;

$objBgEvent  = new BgEvent;
$objBg       = new Bg;

//CONST
$arrUpdateStatus = $objDocument->arrGetUpdateStatus();
$arrNextStatus   = $objDocument->arrGetNextStatus();
$intMaxUpdStatus = $objDocument->intGetMaxUpdateStatus();

//FUNCTIONS

//var_dump($_REQUEST);

if ($strAc == 'getOptionsBg') {
  
  $strBgOption = '';
  
  if ($_REQUEST['be_meldestelle'] != '') {
    $arrBgList = $objBg->arrGetBgFromMeldestelle($_REQUEST['be_meldestelle']);

    if (is_array($arrBgList) && (count($arrBgList) > 0)) {
      foreach ($arrBgList as $intKey => $arrBgFM) {
        $arrBg = $objBg->arrGetBg($arrBgFM['acid']);
        $strBgOption.= '<option value="' .$arrBg[0]['acid'] .'">' .$arrBg[0]['ac_name'] .' (' .$arrBgFM['untnr'] .')</option>'; //
      }
    }

  }

  //print_r($arrBg);
  if (isset($arrBg[0]['ac_anfrage_zum']) && ($arrBg[0]['ac_anfrage_zum'] != '')) {
    $arrList = explode(';', $arrBg[0]['ac_anfrage_zum']);
    if (count($arrList) > 0) {
      $arrLastRequest = [];
      foreach ($arrList as $intKey => $strDate) {
        
        $arrPart = explode('.', $strDate);
        
        $strRequestDate = date('Y') .'-' .$arrPart[1] .'-' .$arrPart[0];
        
        $objDate1 = new DateTime($strRequestDate);
        $objDate2 = new DateTime();
        $objInterval = $objDate1->diff($objDate2);

        if ($objInterval->invert == 1) {

          $strRequestDate = (date('Y') - 1) .'-' .$arrPart[1] .'-' .$arrPart[0];
          
          $objDate1 = new DateTime($strRequestDate);
          $objInterval = $objDate1->diff($objDate2);

        }

        if ((!isset($arrLastRequest['days'])) || ($objInterval->days < $arrLastRequest['days'])) {
          $arrLastRequest = array(
            'days' => $objInterval->days,
            'date' => $strRequestDate
          );

          //print_r($arrLastRequest);

        }

      }
    }
  }
  
  echo $strBgOption;
  echo chr(10) .date('d.m.Y', strtotime($arrLastRequest['date']));

  exit;
  
}

if ($strAc == 'formBgEventAddSubmit') {
  

  $arrEvent = $_REQUEST;
  unset($arrEvent['ac']);
  $strMoFilter = @$arrEvent['mofilter'];
  unset($arrEvent['mofilter']);

  $arrEvent['be_status_bearbeitung'] = 'anzufragen';
  $arrEvent['be_meldepflicht'] = 'ja';
  $arrEvent['be_dokument_art'] = 'Dynamische Erzeugung';

  $arrBgList = $objBg->arrGetBgFromMeldestelle($_REQUEST['be_meldestelle']);

  if (count($arrBgList) > 0) {

    foreach ($arrBgList as $intKey => $arrBg) {
      if ($arrBg['acid'] == $_REQUEST['be_bg']) {
        $arrEvent['be_unternehmensnummer'] = $arrBg['untnr'];
        $arrEvent['be_mitgliedsnummer'] = $arrBg['mglnr'];
      } 
    }

  }

  if ($arrEvent['be_unternehmensnummer'] != "") {

    $strSql = 'SELECT `Id` FROM `Verbindung_Meldestelle_BG__c` WHERE `Berufsgenossenschaft__c` = "' .$_REQUEST['be_bg'] .'" AND `Unternehmensnummer_BG__c` = "' .$arrEvent['be_unternehmensnummer'] .'"';
    $arrSql = MySQLStatic::Query($strSql);

    if (count($arrSql) > 0) {
      $arrEvent['be_verbindung'] = $arrSql[0]['Id'];
    }
    
  }

  $strSql = 'SELECT `Name` FROM `Account` WHERE `Id` = "' .$_REQUEST['be_bg'] .'"';
  $arrSql = MySQLStatic::Query($strSql);

  $arrEvent['be_name_bg'] = $arrSql[0]['Name'];


  $strSql = 'SELECT `Name` FROM `Account` WHERE `Id` = "' .$_REQUEST['be_meldestelle'] .'"';
  $arrSql = MySQLStatic::Query($strSql);

  $arrEvent['be_name_meldestelle'] = $arrSql[0]['Name'];

  //print_r($arrEvent); die();

  if ($arrEvent['be_beitragsfaelligkeit'] != '') {
    $arrEvent['be_beitragsfaelligkeit']  = strConvertDate($arrEvent['be_beitragsfaelligkeit'], 'd.m.Y', 'Y-m-d'); 
  } else {
    $arrEvent['be_beitragsfaelligkeit']  = '0000-00-00';
  }

  $boolInsert = $objBgEvent->boolInsertSingleEvent($arrEvent);

  $strName = 'BG-' .str_pad($boolInsert, 6,'0', STR_PAD_LEFT);

  foreach ($arrEvent as $strField => $mxdValue) {
    if ($mxdValue != '') {
      $intStream = $objStream->intInsertStream($boolInsert, $strName, $strField, '', $mxdValue, 2);
    }
  }

  
  if ($boolInsert != false) {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'OK',
      'beid' => $boolInsert,
      'mofilter' => $strMoFilter,
      'Reload' => true
    );
    
    if (true) {

      $strSql = 'SELECT `cl_first`, `cl_last`, `cl_admin`, `cl_mail` FROM `cms_login` WHERE `cl_id` = "' .MySQLStatic::esc($_SESSION['id']) .'"';
      $arrResult = MySQLStatic::Query($strSql);
      if (count($arrResult) > 0) {
        $strUserFirst = $arrResult[0]['cl_first'];
        $strUserLast = $arrResult[0]['cl_last'];
      }

      $strSql = 'SELECT `Name` FROM `Account` WHERE `Id` = "' .$arrEvent['be_bg'] .'"';
      $arrResult = MySQLStatic::Query($strSql);
      if (count($arrResult) > 0) {
        $strBgName = $arrResult[0]['Name'];
      }

      $strSql = 'SELECT `Name` FROM `Account` WHERE `Id` = "' .$arrEvent['be_meldestelle'] .'"';
      $arrResult = MySQLStatic::Query($strSql);
      if (count($arrResult) > 0) {
        $strMeldestelleName = $arrResult[0]['Name'];
      }

      $strMessage = 'von: ' .$strUserFirst .' ' .$strUserLast .chr(10) .chr(10);
      $strMessage.= 'BG: ' .$strBgName .chr(10);
      $strMessage.= 'Meldestelle: ' .$strMeldestelleName .chr(10);
      $strMessage.= 'Jahr: ' .$arrEvent['be_jahr'] .chr(10);

      if ($_SESSION['id'] > 3) {
        mail('km@izs-institut.de', 'BG-Event hinzugefuegt', $strMessage);
      }

      if (true) { // ($strBg != '0013000001HH0sBAAT') { //VBG

        $intUser = $_SESSION['id'];

        // Auch zu ändern in crons/bg.notify_event.cron.php
        $arrTeam = array(
          //32 => 'Serap',
          //'47' => 'Ecem',
          //'42' => 'Seda',
          //'2'  => 'Kerstin',
          //'44' => 'Karo', 
          //'36' => 'Gordon'
        );
  
        $arrHeader = array(
          'apikey: e9d99940-1aed-414a-b836-b929fe97d093',
          'user: ' .$intUser,
          'Accept: application/json'
        );
      
        //Create User List
        $arrRes = curl_get('http://api.izs-institut.de/api/sg/user', array(), array(), $arrHeader);
        $arrUserList = json_decode($arrRes, true);

        $arrRes = curl_get('http://api.izs-institut.de/api/sg/category', array(), array(), $arrHeader);
        $arrCatList = json_decode($arrRes, true);

        //Context
        $arrContext = array(
            'name' => 'IZS/BG/' .$strBgName .'/' .$strMeldestelleName .'/' .$strName,
            'uri' => '' .$strCmsUrl .'cms/index_neu.php?ac=bg_detail&beid=' .$boolInsert
        );
        
        $arrRes = curl_post('http://api.izs-institut.de/api/sg/context', $arrContext, array(), $arrHeader);
        $arrContext = json_decode($arrRes, true);

        //print_r($arrContext); die();

        $arrMember = array(); //array($arrUserList[$intUser]);
        
        $arrFollower = array();
        foreach ($arrTeam as $intTeamId => $strNameRaw) {
          if ($intTeamId != $intUser) {
            $arrFollower[] = $arrUserList[$intTeamId];
          }
        }

        //Topic
        $arrTopic = array(
            'title' => '' .$strBgName .' - ' .$strMeldestelleName .' - ' .$strName,
            'category' => array(array_search('BG', $arrCatList)),
            'context' => $arrContext['id'],
            'member' => $arrMember,
            'follow' => $arrFollower,
            'due' => (int) time() * 1000,
            'cmsid' => $boolInsert,
            'type' => 'BG Event'
        );

        //print_r($arrTopic); 

        $arrRes = curl_post('http://api.izs-institut.de/api/sg/topic', $arrTopic, array(), $arrHeader);
        $arrTop = json_decode($arrRes, true);

        //print_r($arrRes);
        //print_r($arrTop);

      }

    }
    
  } else {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'FAIL',
      'beid' => $intBeId,
      'Reason' => 'Ein allgemeiner Fehler ist aufgetreten.'
    );    
    
  }
  
  echo json_encode($arrJson);
  exit;    
  
}

if ($strAc == 'formBgEventEditSubmit') {
  
  $arrEvent = $_REQUEST;
  unset($arrEvent['ac']);
  
  $strMoFilter = @$arrEvent['mofilter'];
  unset($arrEvent['mofilter']);

  $boolUpdate = $objBgEvent->boolChangeValue('be_jahr', $arrEvent['be_jahr'], $arrEvent['editid']);

  if ($boolUpdate === true) {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'OK',
      'beid' => $arrEvent['editid'],
      'mofilter' => $strMoFilter,
      'Reload' => true
    );

    if ($_SESSION['id'] > 3) {
      
      $strSql = 'SELECT `cl_first`, `cl_last`, `cl_admin`, `cl_mail` FROM `cms_login` WHERE `cl_id` = "' .MySQLStatic::esc($_SESSION['id']) .'"';
      $arrResult = MySQLStatic::Query($strSql);
      if (count($arrResult) > 0) {
        $strUserFirst = $arrResult[0]['cl_first'];
        $strUserLast = $arrResult[0]['cl_last'];
      }

      $strSql = 'SELECT `be_meldestelle`, `be_bg` FROM `izs_bg_event` WHERE `beid` = "' .$arrEvent['editid'] .'"';
      $arrResult = MySQLStatic::Query($strSql);
      if (count($arrResult) > 0) {
        $arrEvent['be_bg'] = $arrResult[0]['be_bg'];
        $arrEvent['be_meldestelle'] = $arrResult[0]['be_meldestelle'];
      }

      $strSql = 'SELECT `Name` FROM `Account` WHERE `Id` = "' .$arrEvent['be_bg'] .'"';
      $arrResult = MySQLStatic::Query($strSql);
      if (count($arrResult) > 0) {
        $strBgName = $arrResult[0]['Name'];
      }

      $strSql = 'SELECT `Name` FROM `Account` WHERE `Id` = "' .$arrEvent['be_meldestelle'] .'"';
      $arrResult = MySQLStatic::Query($strSql);
      if (count($arrResult) > 0) {
        $strMeldestelleName = $arrResult[0]['Name'];
      }

      $strMessage = 'von: ' .$strUserFirst .' ' .$strUserLast .chr(10) .chr(10);
      $strMessage.= 'BG: ' .$strBgName .chr(10);
      $strMessage.= 'Meldestelle: ' .$strMeldestelleName .chr(10);
      $strMessage.= 'Jahr (neu): ' .$arrEvent['be_jahr'] .chr(10);
      
      mail('km@izs-institut.de', 'BG-Event bearbeitet', $strMessage);

    }
    
    
  } else {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'FAIL',
      'beid' => $intBeId,
      'Reason' => 'Ein allgemeiner Fehler ist aufgetreten.'
    );    
    
  }
  
  echo json_encode($arrJson);
  exit;    
  
}

if ($strAc == 'formBgEventDuplicateSubmit') {
  
  $arrEvent = $_REQUEST;

  $arrEventSource = $objBgEvent->arrGetBgEvent($arrEvent['duplicateid']);

  if ($arrEventSource[0]['be_unternehmensnummer'] == '') {
    $strSql = 'SELECT `Unternehmensnummer_BG__c` FROM `Verbindung_Meldestelle_BG__c` WHERE `Aktiv__c` = "true" ';
    $strSql.= 'AND `Meldestelle__c` = "' .$arrEventSource[0]['be_meldestelle'] .'" AND `Berufsgenossenschaft__c` = "' .$arrEventSource[0]['be_bg'] .'"';
    $arrResult = MySQLStatic::Query($strSql);
    if (count($arrResult) > 0) {
      $arrEventSource[0]['be_unternehmensnummer'] = $arrResult[0]['Unternehmensnummer_BG__c'];
    }
  }

  unset($arrEvent['ac']);
  unset($arrEvent['duplicateid']);

  //print_r($arrEventSource);

  $strMoFilter = @$arrEvent['mofilter'];
  unset($arrEvent['mofilter']);

  $arrEvent['be_status_bearbeitung'] = 'angefragt';
  $arrEvent['be_bg'] = $arrEventSource[0]['be_bg'];
  $arrEvent['be_meldestelle'] = $arrEventSource[0]['be_meldestelle'];

  $arrEvent['be_verbindung'] = $arrEventSource[0]['be_verbindung'];
  $arrEvent['be_name_meldestelle'] = $arrEventSource[0]['be_name_meldestelle'];
  $arrEvent['be_name_bg'] = $arrEventSource[0]['be_name_bg'];
  $arrEvent['be_mitgliedsnummer'] = $arrEventSource[0]['be_mitgliedsnummer'];
  $arrEvent['be_unternehmensnummer'] = $arrEventSource[0]['be_unternehmensnummer'];

  $arrEvent['be_dokument_art'] = 'Dynamische Erzeugung';

  //print_r($arrEvent); die();

  $boolInsert = $objBgEvent->boolInsertSingleEvent($arrEvent);

  $objStream = new Stream('bgevent');
  $strName = 'BG-' .str_pad($boolInsert, 6,'0', STR_PAD_LEFT);
  $intStream = $objStream->intInsertStream($boolInsert, $strName, 'beid', $arrEventSource[0]['beid'], $boolInsert, 6);

  foreach ($arrEvent as $strField => $mxdValue) {
    if (($mxdValue != '') && ($mxdValue != $arrEventSource[0][$strField])) {
      $intStream = $objStream->intInsertStream($boolInsert, $strName, $strField, $arrEventSource[0][$strField], $mxdValue, 2);
    }
  }

  if ($boolInsert !== true) {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'OK',
      'beid' => $boolInsert,
      'mofilter' => $strMoFilter,
      'Reload' => true
    );

    if (true) {
      
      $strSql = 'SELECT `cl_first`, `cl_last`, `cl_admin`, `cl_mail` FROM `cms_login` WHERE `cl_id` = "' .MySQLStatic::esc($_SESSION['id']) .'"';
      $arrResult = MySQLStatic::Query($strSql);
      if (count($arrResult) > 0) {
        $strUserFirst = $arrResult[0]['cl_first'];
        $strUserLast = $arrResult[0]['cl_last'];
      }

      $strSql = 'SELECT `Name` FROM `Account` WHERE `Id` = "' .$arrEvent['be_bg'] .'"';
      $arrResult = MySQLStatic::Query($strSql);
      if (count($arrResult) > 0) {
        $strBgName = $arrResult[0]['Name'];
      }

      $strSql = 'SELECT `Name` FROM `Account` WHERE `Id` = "' .$arrEvent['be_meldestelle'] .'"';
      $arrResult = MySQLStatic::Query($strSql);
      if (count($arrResult) > 0) {
        $strMeldestelleName = $arrResult[0]['Name'];
      }

      $strMessage = 'von: ' .$strUserFirst .' ' .$strUserLast .chr(10) .chr(10);
      $strMessage.= 'BG: ' .$strBgName .chr(10);
      $strMessage.= 'Meldestelle: ' .$strMeldestelleName .chr(10);
      $strMessage.= 'Jahr (neu): ' .$arrEvent['be_jahr'] .chr(10);
      
      if ($_SESSION['id'] > 3) {
        mail('km@izs-institut.de', 'BG-Event dupliziert', $strMessage);
      }

      if (true) { // ($strBg != '0013000001HH0sBAAT') { //VBG

        $intUser = $_SESSION['id'];

        // Auch zu ändern in crons/bg.notify_event.cron.php
        $arrTeam = array(
          //32 => 'Serap',
          //'47' => 'Ecem',
          //'42' => 'Seda',
          //'2'  => 'Kerstin',
          //'44' => 'Karo', 
          //'36' => 'Gordon'
        );
  
        $arrHeader = array(
          'apikey: e9d99940-1aed-414a-b836-b929fe97d093',
          'user: ' .$intUser,
          'Accept: application/json'
        );
      
        //Create User List
        $arrRes = curl_get('http://api.izs-institut.de/api/sg/user', array(), array(), $arrHeader);
        $arrUserList = json_decode($arrRes, true);

        $arrRes = curl_get('http://api.izs-institut.de/api/sg/category', array(), array(), $arrHeader);
        $arrCatList = json_decode($arrRes, true);

        //Context
        $arrContext = array(
            'name' => 'IZS/BG/' .$strBgName .'/' .$strMeldestelleName .'/' .$strName,
            'uri' => '' .$strCmsUrl .'cms/index_neu.php?ac=bg_detail&beid=' .$boolInsert
        );
        
        $arrRes = curl_post('http://api.izs-institut.de/api/sg/context', $arrContext, array(), $arrHeader);
        $arrContext = json_decode($arrRes, true);

        //print_r($arrContext); die();

        $arrMember = array(); //array($arrUserList[$intUser]);
        
        $arrFollower = array();
        foreach ($arrTeam as $intTeamId => $strNameRaw) {
          if ($intTeamId != $intUser) {
            $arrFollower[] = $arrUserList[$intTeamId];
          }
        }

        //Topic
        $arrTopic = array(
            'title' => '' .$strBgName .' - ' .$strMeldestelleName .' - ' .$strName,
            'category' => array(array_search('BG', $arrCatList)),
            'context' => $arrContext['id'],
            'member' => $arrMember,
            'follow' => $arrFollower,
            'due' => (int) time() * 1000,
            'cmsid' => $boolInsert,
            'type' => 'BG Event'
        );

        //print_r($arrTopic); 

        $arrRes = curl_post('http://api.izs-institut.de/api/sg/topic', $arrTopic, array(), $arrHeader);
        $arrTop = json_decode($arrRes, true);

        //print_r($arrRes);
        //print_r($arrTop);

      }

    }
    
  } else {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'FAIL',
      'beid' => $intBeId,
      'Reason' => 'Ein allgemeiner Fehler ist aufgetreten.'
    );    
    
  }
  
  echo json_encode($arrJson);
  exit;  
    
}

if ($strAc == 'formBgEventDeleteSubmit') {
  
  $arrEvent = $_REQUEST;

  $strMoFilter = @$arrEvent['mofilter'];
  unset($arrEvent['mofilter']);

  if ($_SESSION['id'] > 3) {
    $strSql = 'SELECT `be_meldestelle`, `be_bg`, `be_jahr` FROM `izs_bg_event` WHERE `beid` = "' .$arrEvent['deleteid'] .'"';
    $arrResult = MySQLStatic::Query($strSql);
    if (count($arrResult) > 0) {
      $arrEvent['be_bg'] = $arrResult[0]['be_bg'];
      $arrEvent['be_meldestelle'] = $arrResult[0]['be_meldestelle'];
      $arrEvent['be_jahr'] = $arrResult[0]['be_jahr'];
    }
  }

  $boolDelete = $objBgEvent->boolDeleteEvent(0, $arrEvent['deleteid']);
  $boolDelete = true;

  if ($boolDelete === true) {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'OK',
      'beid' => $arrEvent['deleteid'],
      'mofilter' => $strMoFilter,
      'Reload' => true
    );

    if ($_SESSION['id'] > 3) {
      
      $strSql = 'SELECT `cl_first`, `cl_last`, `cl_admin`, `cl_mail` FROM `cms_login` WHERE `cl_id` = "' .MySQLStatic::esc($_SESSION['id']) .'"';
      $arrResult = MySQLStatic::Query($strSql);
      if (count($arrResult) > 0) {
        $strUserFirst = $arrResult[0]['cl_first'];
        $strUserLast = $arrResult[0]['cl_last'];
      }

      $strSql = 'SELECT `Name` FROM `Account` WHERE `Id` = "' .$arrEvent['be_bg'] .'"';
      $arrResult = MySQLStatic::Query($strSql);
      if (count($arrResult) > 0) {
        $strBgName = $arrResult[0]['Name'];
      }

      $strSql = 'SELECT `Name` FROM `Account` WHERE `Id` = "' .$arrEvent['be_meldestelle'] .'"';
      $arrResult = MySQLStatic::Query($strSql);
      if (count($arrResult) > 0) {
        $strMeldestelleName = $arrResult[0]['Name'];
      }

      $strMessage = 'von: ' .$strUserFirst .' ' .$strUserLast .chr(10) .chr(10);
      $strMessage.= 'BG: ' .$strBgName .chr(10);
      $strMessage.= 'Meldestelle: ' .$strMeldestelleName .chr(10);
      $strMessage.= 'Jahr (neu): ' .$arrEvent['be_jahr'] .chr(10);
      
      mail('km@izs-institut.de', 'BG-Event gelöscht', $strMessage);

    }
    
  } else {
    
    //antwort schicken
    $arrJson = array(
      'Status' => 'FAIL',
      'beid' => $intBeId,
      'Reason' => 'Ein allgemeiner Fehler ist aufgetreten.'
    );    
    
  }
  
  echo json_encode($arrJson);
  exit;    
  
}

if ($strAc == 'getModal') {
  
  $strOutput = '';

  if ($_REQUEST['ty'] == 'edit') {
    $strType = 'Edit';
    $strModalName = 'Event bearbeiten';
  } else {
    $strType = 'Duplicate';
    $strModalName = 'Event duplizieren';
  }

  if (!isset($_REQUEST['beid'])) {

    $strOutput.= '
      <form id="formBgEvent' .$strType .'" class="modal-content form-horizontal" autocomplete="off" method="post" action="action/bgevent.ajax.php?ac=getModal&ty=' .strtolower($strType) .'&beid=">
      <input type="hidden" name="' .strtolower($strType) .'id" id="editid" value="" />
      <input type="hidden" name="mofilter" class="mofilter" value="" />
      <div class="modal-header ">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title" id="modal' .$strType .'BgEventLabel">' .$strModalName .'</h4>
        </div>
        <div class="modal-body">
        </div> <!-- modal-body -->
        <button id="formBg' .$strType .'EditSubmit" type="submit" class="btn btn-primary margin-top-5 pull-right">' .$strModalName  .'</button>
      </form>
    ';

  } else {
    
    $arrEvent       = $objBgEvent->arrGetBgEvent($_REQUEST['beid']);
    $arrMeldestelle = $objBg->arrGetMeldestellen('', $arrEvent[0]['be_meldestelle']);

    if (count($arrMeldestelle) > 0) {

      $strYearOptions = '';
      for ($intCount = 0; $intCount <= 5; $intCount++) {
        $intYear = date('Y') - 3 + $intCount;
        $strSelected = '';
        if ($intYear == (date('Y') - 1)) {
          $strSelected = ' selected="selected"';
        }
        $strYearOptions.= '              <option value="' .$intYear. '"' .$strSelected. '>' .$intYear. '</option>' .chr(10);
      }
      
      $strMeldestelleOptions = '';
      $strMeldestelleOptions.= '              <option value="' .$arrEvent[0]['be_meldestelle']. '" selected="selected">' .$arrMeldestelle[0]['ac_name']. '</option>' .chr(10);

      $arrBg = $objBg->arrGetBg($arrEvent[0]['be_bg']);
      
      $strBgOptions = '';
      $strBgOptions.= '              <option value="' .$arrEvent[0]['be_bg']. '" selected="selected">' .$arrBg[0]['ac_name']. '</option>' .chr(10);
    
      $strOutput.= '
          <div class="row">
            <div class="form-group form-material ">
              <label class="col-sm-3 control-label">Jahr</label>
              <div class="col-sm-8">
                <select class="form-control" id="be_jahr" name="be_jahr" data-plugin="select2-">
' .$strYearOptions .'
                </select>
              </div>
            </div> <!-- form-group -->
          </div> <!-- row -->
    ';
    
      if ($strType == 'Duplicate') {

      $strOutput.= '
          <div class="row">
            <div class="form-group form-material ">
              <label class="col-sm-3 control-label">Meldestelle</label>
              <div class="col-sm-8">
                <select class="form-control" id="be_meldestelle" name="be_meldestelle" data-plugin="select2-" disabled="disabled">
' .$strMeldestelleOptions .'
                </select>
              </div>
            </div> <!-- form-group -->
          </div> <!-- row -->

          <div class="row">
            <div class="form-group form-material bg">
              <label class="col-sm-3 control-label">Berufsgenossenschaft</label>
              <div class="col-sm-8">
                <select class="form-control" id="be_bg" name="be_bg" data-plugin="select2-" disabled="disabled">
' .$strBgOptions .'
                </select>
              </div>
            </div> <!-- form-group -->
          </div> <!-- row -->
          <script>
          $("#formBgEventDuplicateSubmit").css("display", "block");
        </script>
    ';
    
      }

    } else {

      $strOutput.= '
      <div class="row">
        <div style="text-align: center;">
          <p class="red">Duplizieren nicht moglich! Keine aktive Verbindung.
        </div> <!-- form-group -->
      </div> <!-- row -->
      <script>
        $("#formBgEventDuplicateSubmit").css("display", "none");
      </script>
';


    }
    
  }
  
  echo $strOutput;
  exit;    
  
}

/*


*/

?>