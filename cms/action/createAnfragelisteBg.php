<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

session_start();

if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
  define('APP_PATH', '/data/www/sites/test.izs.de/html/');
} else {
  define('APP_PATH', '/data/www/sites/www.izs.de/html/');
}

require_once(APP_PATH .'cms/const.inc.php');

//CLASSES
require_once(APP_PATH .'assets/classes/class.mysql.php');
require_once(APP_PATH .'cms/fpdf17/fpdf.php');
require_once(APP_PATH .'cms/fpdf17/fpdi.php');

require_once(APP_PATH .'cms/classes/class.bg.php');
require_once(APP_PATH .'cms/classes/class.bgevent.php');
require_once(APP_PATH .'cms/classes/class.request.php');
require_once(APP_PATH .'cms/classes/class.premiumpayer.php');
require_once(APP_PATH .'cms/classes/class.informationprovider.php');

require_once(APP_PATH .'cms/inc/refactor.inc.php');


class concat_pdf extends fpdi {
  var $files = array();
  function concat_pdf($orientation='P',$unit='mm',$format='A4') {
    parent::__construct($orientation,$unit,$format);
  }
  function setFiles($files) {
    $this->files = $files;
  }
  function concat() {
    foreach($this->files AS $file) {
      $pagecount = $this->setSourceFile($file);
      for ($i = 1;  $i <= $pagecount;  $i++) {
       $tplidx = $this->ImportPage($i);
       $this->AddPage();
       $this->useTemplate($tplidx);
      }
    }
  }
  // Page footer
  function Footer() {
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('OpenSans','',8);
    // Page number
    $this->Cell(0,10,' - Seite '.$this->PageNo().'/{nb}' .' -',0,0,'C');
  }
}



//REQUEST
$strBgId = @$_REQUEST['acid'];
$intYear = @$_REQUEST['year'];
$strDest = @$_REQUEST['show'];
$strBnr  = @$_REQUEST['bnr'];
$strOnl  = @$_REQUEST['online'];

$boolDez = false;

$objIp = new InformationProvider;

$strDezId = '';
$arrDez = $objIp->arrDezentraleAnfrage($strBgId);
if (count($arrDez) > 0) {
  $strDezId = $strBgId;
  $strBgId = $arrDez[0]['Information_Provider__c'];
  $boolDez = true;
}

if (empty($intYear) || ($intYear == '')) {
  $intYear = date('Y') - 1;
}

//DATA
$objBg      = new Bg;
$objBgEvent = new BgEvent;
$objReq     = new Request;
$pdf        = new concat_pdf();

$strOutput = '';

$arrCertList = array();

$arrAnfrage = array();

//echo 'here'; die();

if ($strBgId != '') {

  $intUser = $_SESSION['id'];

  $arrHeader = array(
    'apikey: bb9494bc-8f19-415f-9ace-cd19fe3a05f6',
    'user: ' .$intUser,
    'Accept: application/json'
  );

  if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
    $arrHeader[] = 'test: true';
  }

  $arrGet = array(
    'bg' => ($boolDez) ? $strDezId : $strBgId,
    'on' => $strOnl
  );

  //Create User List
  $arrRes = curl_get('http://api.izs-institut.de/api/bg/events/', $arrGet, array(), $arrHeader);
  $arrAnfrage = json_decode($arrRes, true);

  //print_r($arrAnfrage);
  //die('here');
  
  $arrBg = $objBg->arrGetBg($strBgId);
  
}

//echo 'here'; die();

/* ------------ PDF CREATION -------------- */

$pdf = new concat_pdf();
$pdf->AliasNbPages();

$pdf->AddFont('OpenSans','','OpenSans-Regular.php');
$pdf->AddFont('OpenSans','B','OpenSans-Bold.php');
$pdf->AddFont('arialbd','','arialbd.php');

$pdf->AddPage();
$pdf->SetAutoPageBreak(false);

$pdf->Image(CMS_PATH .'pdf/Vorlage_ohne.png', 0, 0, 210, 297, 'PNG'); 
$pdf->SetMargins(20, 57, 20);

$pdf->SetFont('OpenSans', 'B', 10);
//$pdf->Text(20.4, 58.2, utf8_decode($arrBg[0]['ac_name']));
$pdf->Text(20.4, 58.2, '');

$pdf->SetFont('OpenSans', 'B', 10);
$pdf->Text(20.4, 68, utf8_decode('Anforderung von Unbedenklichkeitsbescheinigungen für folgende Zeitarbeitsunternehmen:'));
$pdf->SetFont('OpenSans', '', 6);
$pdf->Text(20.4, 73, utf8_decode('(Hinweis: Vollmachten liegen bei)'));

$pdf->SetFont('OpenSans', 'B', 8);
$pdf->Text(20.4, 90, 'Unternehmensnummer');
$pdf->Text(60, 90, 'Beitragszahler');
$pdf->Text(146, 90, '');
$pdf->Text(163, 90, '');
$pdf->Text(182, 90, '');


$pdf->SetDrawColor(0, 0, 0);
$pdf->SetLineWidth(0.2);
$pdf->Line(20, 93, 190, 93);

$pdf->SetFont('OpenSans', '', 8);
$pdf->Cell(20.4, 83, '', 0, 1);



//CONTENT

$intAccountPage = 1;
$pdf->SetY(93);

//print_r($arrAnfrage); die();

      
foreach($arrAnfrage as $arrMeldestelle) {

  if (isset($strBnr) && ($strBnr != '')) {
    if ($arrMeldestelle[2] != $strBnr) {
      continue;
    }
  }
  
  if ($intAccountPage >= 2) {
    $intY = 100.5 + (($intAccountPage - 2) * 8);
    $pdf->SetDrawColor(0, 0, 0);
    $pdf->SetLineWidth(0.2);
    $pdf->Line(20.4, $intY, 189.6, $intY);
  }
  
  $pdf->Cell(39, 8, utf8_decode($arrMeldestelle[0]), 0, 0);
  $pdf->Cell(96, 8, utf8_decode($arrMeldestelle[1]), 0, 0);
  $pdf->Cell(18, 8, utf8_decode(''), 0, 0);
	  
  $pdf->Cell(25, 8, '', 0, 0);
  
  $pdf->Ln();
  
  $intAccountPage++;
  
  if ((($intAccountPage % 23) == 0) && (($intAccountPage - 1) != count($arrLine))) {
    
    $intAccountPage = 1;
    
    $pdf->AddPage();
    $pdf->SetAutoPageBreak(false);
    
    $pdf->Image(CMS_PATH .'pdf/Vorlage_ohne.png', 0, 0, 210, 297, 'PNG'); 
    $pdf->SetMargins(20, 57, 20);
    
    $pdf->SetFont('OpenSans', 'B', 10);
    //$pdf->Text(20.4, 58.2, utf8_decode($arrBg[0]['ac_name']));
    $pdf->Text(20.4, 58.2, utf8_decode(''));
    

    $pdf->SetFont('OpenSans', 'B', 10);
    $pdf->Text(20.4, 68, utf8_decode('Anforderung von Unbedenklichkeitsbescheinigungen für folgende Zeitarbeitsunternehmen:'));
    $pdf->SetFont('OpenSans', '', 6);
    $pdf->Text(20.4, 73, utf8_decode('(Hinweis: Vollmachten liegen bei)'));
    
    $pdf->SetFont('OpenSans', 'B', 8);
    $pdf->Text(20.4, 90, 'Kundennummer');
    $pdf->Text(46, 90, 'Beitragszahler');
    $pdf->Text(146, 90, '');
    $pdf->Text(163, 90, '');
    $pdf->Text(182, 90, '');
    
    $pdf->SetDrawColor(0, 0, 0);
    $pdf->SetLineWidth(0.2);
    $pdf->Line(20, 93, 190, 93);
    
    $pdf->SetFont('OpenSans', '', 8);
    $pdf->Cell(20.4, 36, '', 0, 1);

  }
  
  
}


if ($strDest == 'D') {

  $objPdf->Output(date('Y.m.d_His - ') .'Anfrage - ' .$arrBg[0]['ac_name'] .'.pdf', 'D');
  
} else {

  if (true) {

    $strId = ($boolDez) ? $strDezId : $strBgId;

    $strAdd = '';
    if (isset($strBnr) && ($strBnr != '')) {
      $strAdd = '_' .$strBnr;
    } else {
      $strBnr = '';
    }

    $intUser = $_SESSION['id'];

    $arrHeader = array(
      'apikey: bb9494bc-8f19-415f-9ace-cd19fe3a05f6',
      'user: ' .$intUser,
      'Accept: application/json'
    );

    if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
      $arrHeader[] = 'test: true';
    }

    $arrGet = array(
      'format' => 'xlsx',
      'bnr' => $strBnr,
      'bg' => $strId,
      'on' => $strOnl
    );

    //print_r($arrGet); die();

    //Create User List
    $arrRes = curl_get('http://api.izs-institut.de/api/bg/events/', $arrGet, array(), $arrHeader);
    $arrEventList = json_decode($arrRes, true);

    //print_r($arrRes);
    //die('here2');

    $strFileName = CMS_PATH .'pdf/anfrage/bg/' .$strId .$strAdd .'_anfrage.xlsx';
    
    if (file_exists($strFileName)) {
      unlink($strFileName);
    }

    //print_r($arrEventList); die();
    
    file_put_contents($strFileName, $arrRes);
      
  }

  $strFileName = CMS_PATH .'pdf/anfrage/bg/' .$strId .$strAdd .'_anfrage.pdf';

  if (file_exists($strFileName)) {
    unlink($strFileName);
  }
  $pdf->Output($strFileName, 'F');
  
  $strOutput = $objReq->boolWriteLog($strId, $intYear, $strBnr);

  //header('Location: /cms/index_neu.php?ac=bg_events&year=' .$intYear .'');

}

echo $strOutput;

?>
