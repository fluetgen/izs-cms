<?php

//require_once('classes/global.inc.php');

$strUploadDir = '../pdf/bg/';

if (!empty($_FILES)) {
  
  foreach ($_FILES as $strName => $arrFiles) {
	  $tempFile   = $arrFiles['tmp_name'];
	  $targetFile = $strUploadDir . $arrFiles['name'];

    if (file_exists($targetFile)) {
      unlink($targetFile);
    }

  	// Save the file
  	move_uploaded_file($tempFile, $targetFile);
  	echo $arrFiles['name'];
  }

} else {
  
  header('HTTP/1.0 400 Please select at least one file to upload.');
  
}
?>