<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

include('conf/global.inc.php');
include('classes/fklCsv.php');
include('inc/sv-import.inc.php');

$strOutput = '';
$strOutput .= '<h1>Events importieren</h1>' . chr(10);


if ($_REQUEST['import'] == '1') {

  $arrData = unserialize(str_replace("'", '"', $_REQUEST['arrData']));
  $arrStat = unserialize(str_replace("'", '"', $_REQUEST['arrStat']));
  $arrStatDetail = unserialize(str_replace("'", '"', $_REQUEST['arrStatDetail']));

  $strMd5 = md5($_REQUEST['arrData']);

  $strSqlM = 'SELECT * FROM `import_log` WHERE il_md5 = "' . $strMd5 . '"';
  $arrResultM = MySQLStatic::Query($strSqlM);


  if (isset($_REQUEST['hash'])) {
    
    $strSqlM = 'SELECT * FROM `import_log` WHERE il_md5 = "' . $_REQUEST['hash'] . '"';
    $arrResultM = MySQLStatic::Query($strSqlM);

    $strSql = 'DELETE FROM `import_log` WHERE `ilid` = "' .$arrResultM[0]['ilid'] .'"';
    $arrRes = MySQLStatic::Query($strSql);

    $strSql = 'DELETE FROM `import_stats` WHERE `is_ilid` = "' .$arrResultM[0]['ilid'] .'"';
    $arrRes = MySQLStatic::Query($strSql);

    foreach ($arrStat as $intKey => $arrDataset) {

      foreach ($arrDataset as $strDate => $intValue) {
        $arrPart = explode('-', $strDate);
        $strSql = 'DELETE FROM `SF42_IZSEvent__c` WHERE `group_id` = "' .$intKey .'" AND `SF42_Month__c` = "' .$arrPart[1] .'" AND `SF42_Year__c` = "' .$arrPart[0] .'"';
        $arrRes = MySQLStatic::Query($strSql);
      }
            
    }

    $arrResultM = array();

    header('Location: index.php?ac=eimp');
    exit;

  }




  if (count($arrResultM) > 0) {

    $strSql = 'SELECT `cl_first`, `cl_last` FROM `cms_login` WHERE `cl_id` = "' .$arrResultM[0]['il_cl_id'].'"';
    $arrRes = MySQLStatic::Query($strSql);

    echo '<html><head><meta charset="utf-8"/><title>Duplicate file!</title></head><body>';
    echo '<p>Die CSV-Datei: "' . $arrResultM[0]['il_filename'] . '" wurde bereits am ' . date('d.m.Y', strtotime($arrResultM[0]['il_date'])) .' ';
    echo 'um '  .date('H:i:s', strtotime($arrResultM[0]['il_date'])) .' Uhr von ' .$arrRes[0]['cl_first'] .' ' .$arrRes[0]['cl_last'] .' hochgeladen.</p>';

    if (($_SESSION['id'] == 3) || ($_SESSION['id'] == 2)) {
      echo '<form method="post" action="index.php?ac=eimp">';
      echo '<input type="hidden" name="hash" value="' .$strMd5 .'" />';
      echo '<input type="hidden" name="import" value="' .$_REQUEST['import'] .'" />';
      echo '<input type="hidden" name="strFileName" value="' .$_REQUEST['strFileName'] .'" />';
      echo '<input type="hidden" name="arrStat" value="' .$_REQUEST['arrStat'] .'" />';
        
      /*
      echo '<input type="hidden" name="arrData" value="' .$_REQUEST['arrData'] .'" />';
      echo '<input type="hidden" name="arrStatDetail" value="' .$_REQUEST['arrStatDetail'] .'" />';
     */
      
      echo '<input type="submit" value="Datensätzle löschen und Import erneut starten" /><br />';
      echo '<input type="button" value="Abbrechen" onclick="window.location=\'index.php?ac=eimp\';" style="margin-top: 15px;" />'; 
      echo '</form>';
    } else {
      echo '<form method="post" action="index.php?ac=eimp">';
      echo '<input type="button" value="Abbrechen" onclick="window.location=\'index.php?ac=eimp\';" />'; 
      echo '</form>';
    }

    echo '</body></html>';

    exit;

  }

  if (is_array($arrData) && (count($arrData) > 0)) {

    $strSqlM = 'INSERT INTO `import_log` (`ilid`, `il_md5`, `il_filename`, `il_date`, `il_cl_id`) ';
    $strSqlM.= 'VALUES (NULL, "' .$strMd5 .'", "' .$_REQUEST['strFileName'] .'", NOW(), "' .$_SESSION['id'] .'")';

    $intResultM = MySQLStatic::Insert($strSqlM);

    //WRITE STAT
    if (is_array($arrStat) && (count($arrStat) > 0)) {
      foreach ($arrStat as $strGrId => $arrMonth) {
        if (is_array($arrMonth) && (count($arrMonth) > 0)) {
          foreach ($arrMonth as $strMonth => $intDatasets) {

            //Bestehende Daten für PP und Monat löschen. Filename muss identisch sein!
            $strSqlD = 'SELECT `isid` FROM `import_stats` WHERE `is_filename` = "' . $_REQUEST['strFileName'] . '" AND `is_date` = "' . $strMonth . '" AND `is_grid` = "' . $strGrId . '"';
            $arrResD = MySQLStatic::Query($strSqlD);
            if (count($arrResD) > 0) {
              foreach ($arrResD as $intKey => $arrStat) {
                $strSqlD2 = 'DELETE FROM `import_stats` WHERE `isid` = "' . $arrStat['isid'] . '"';
                $arrResD2 = MySQLStatic::Query($strSqlD2);
              }
            }

            if ($strGrId == '') {
              $intResultOk  = 0;
              $intResultErr = $intDatasets;
              $intResultDS  = 0;
              $intResultDF  = 0;
              $intResultSN  = 0;
              $intResultSNo = 0;
              $intResultSU  = 0;
            } else {
              $intResultOk  = $arrStatDetail[$strMonth]['is_result_ok'];
              $intResultErr = $arrStatDetail[$strMonth]['is_result_error'];
              $intResultDS  = $arrStatDetail[$strMonth]['is_result_doublet_system'];
              $intResultDF  = $arrStatDetail[$strMonth]['is_result_doublet_file'];
              $intResultSN  = $arrStatDetail[$strMonth]['is_result_saldo_null'];
              $intResultSNo = $arrStatDetail[$strMonth]['is_result_saldo_no'];
              $intResultSU  = $arrStatDetail[$strMonth]['is_result_saldo_unknown'];
            }

            $strSqlStat = 'INSERT INTO `import_stats` (`isid`, `is_grid`, `is_date`, `is_datasets`, `is_import_time`, ';
            $strSqlStat .= '`is_result_ok`, `is_result_error`, `is_result_doublet_system`, `is_result_doublet_file`, `is_result_saldo_null`, `is_result_saldo_no`, `is_result_saldo_unknown`, ';
            $strSqlStat .= '`is_filename`, `is_ilid`) VALUES (NULL, "' . $strGrId . '", "' . $strMonth . '", "' . $intDatasets . '", NOW(), ';
            $strSqlStat .= '"' . $intResultOk . '", "' . $intResultErr . '", ';
            $strSqlStat .= '"' . $intResultDS . '", "' . $intResultDF . '", "' . $intResultSN . '", ';
            $strSqlStat .= '"' . $intResultSNo . '", "' . $intResultSU . '", ';
            $strSqlStat .= '"' . $_REQUEST['strFileName'] . '", "' . $intResultM . '");';
            $intResultStat = MySQLStatic::Insert($strSqlStat);
          }
        }
      }
    }

    foreach ($arrData as $intKey => $arrLine) {

      $strSql = 'SELECT (MAX(`evid`) + 1) AS `evid` FROM `SF42_IZSEvent__c`';
      $result = MySQLStatic::Query($strSql);

      foreach ($result as $row) {
        $intId = $row['evid'];
      }

      if ($arrLine[7] == 'abgelehnt / Dublette') {
        $strDoublette = 'true';
      } else {
        $strDoublette = 'false';
      }


      $strSql2 = 'SELECT `SF42_Company_Group__c` FROM `Account` WHERE `SF42_Comany_ID__c` = "' . $arrLine[5] . '"';
      $arrResult2 = MySQLStatic::Query($strSql2);
      $intGroupId = $arrResult2[0]['SF42_Company_Group__c'];

      //0: OK/KONS
      //1: Monat
      //2: Jahr
      //3: PPNAME
      //4: IDIP
      //5: BTNR PP
      //6: BTNR IP
      //7: STATUS

      $strSql = "
INSERT INTO `SF42_IZSEvent__c` (`evid`, `Id`, `IsDeleted`, `Name`, `RecordTypeId`, 
`CreatedDate`, `CreatedById`, `LastModifiedDate`, `LastModifiedById`, `SystemModstamp`, 
`LastActivityDate`, `SF42_Payment_Period__c`, `OLD_SF42_PremiumPayer__c`, 
`SF42_DeliverDate__c`, `SF42_DocumentUrl__c`, `SF42_EventComment__c`, `SF42_EventPeriod__c`, 
`SF42_EventStatus__c`, `SF42_IntSelUpdate__c`, `SF42_LastEventStatus__c`, `SF42_Month__c`, 
`SF42_OnlineStatus__c`, `SF42_Premium_Payer__c`, `SF42_PublishingStatus__c`, 
`SF42_RoadNumber__c`, `SF42_SIO__c`, `SF42_StatusFlag__c`, `SF42_Year__c`, 
`SF42_informationProvider__c`, `SF42_event_ID_old__c`, `Status_Klaerung__c`, `Grund__c`, 
`Beitragsrueckstand__c`, `Info__c`, `Naechster_Meilenstein__c`, `bis_am__c`, 
`Betriebsnummer_ZA__c`, `Beitragsmonat__c`, `Beitragsjahr__c`, `Sperrvermerk__c`, 
`Information_Provider_Period__c`, `Betriebsnummer_IP__c`, `SF42_IntCount__c`, 
`Art_des_Dokuments__c`, `Auskunft_von__c`, `DAK_Team__c`, `Verspaetete_Beitragszahlung__c`, 
`Rueckmeldung_am__c`, `Art_der_Rueckmeldung__c`, `Unique_ID__c`, `Dublette__c`, `group_id`  
) VALUES (
" . $intId . ", '" . $intId . "', 'false', 'E-" . $intId . "', '" . $arrLine[0] . "', NOW(), '" . $_SESSION['id'] . "', NOW(), 
'" . $_SESSION['id'] . "', NOW(), NOW(), '', '', '0000-00-00', '', NULL, '" . $arrLine[2] . " / " . $arrLine[1] . "', 
'" . $arrLine[7] . "', 'false', '" . $arrLine[7] . "', '" . $arrLine[1] . "', 'true', '" . $arrLine[3] . "', 'online', 
NULL, '', 'ok', '" . $arrLine[2] . "', '" . $arrLine[4] . "', NULL, '', '', NULL, NULL, '', 
'0000-00-00', '" . $arrLine[5] . "', '" . $arrLine[1] . "', '" . $arrLine[2] . "', 'false', '', '" . $arrLine[6] . "', 
'0', '', 'Krankenkasse', '', 'false', '0000-00-00', '', '', '" . $strDoublette . "', '" . $intGroupId . "');
";

      $result = MySQLStatic::Query($strSql);

      $strSql2 = 'INSERT INTO `izs_event_change` 
       (`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (
       NULL, "' . $intId . '", ' . $_SESSION['id'] . ', "E-' . $intId . '", "", "", "", NOW(), 1)';
      $result2 = MySQLStatic::Query($strSql2);
    }

    $strOutput .= '<p>' . count($arrData) . ' Datensätze importiert. (File: "' . $_REQUEST['strFileName'] . '")</p>';
  } else {
    $strOutput .= '<p class="red">Keine Daten importiert. (File: "' . $_REQUEST['strFileName'] . '")</p>';
  }
}

if ($_REQUEST['fname'] != '') {

  $boolHasHeader = false;
  $intCsvLines = 0;

  $arrAc2Gr = array();
  $strSql = 'SELECT `SF42_Comany_ID__c`, `SF42_Company_Group__c` FROM `Account` WHERE `RecordTypeId` = "01230000001Ao71AAC" AND `SF42_isPremiumPayer__c` = "true" AND `SF42_Comany_ID__c` != ""';
  $arrResult = MySQLStatic::Query($strSql);

  if (count($arrResult) > 0) {
    foreach ($arrResult as $intKey => $arrAccount) {
      $arrAc2Gr[$arrAccount['SF42_Comany_ID__c']] = $arrAccount['SF42_Company_Group__c'];
    }
  }


  $strFile = $_REQUEST['fname'];
  $strPath = $strUploadDir . '/' . $strFile;

  if (($strPath != '') && is_file($strPath)) {

    $objCsvFile = new fklCsv($strPath);

    $arrImport = $objCsvFile->to_array();

    if (count($arrImport) > 0) {

      $arrPeriod = array();
      foreach ($arrImport as $intKey => $arrDataset) {
        if (($intKey == 0) && (is_numeric($arrDataset[1]) == false)) {
          continue;
        }

        $arrDataset[0] = preg_replace('/[\x00-\x1F\x7F]/', '', $arrDataset[0]);

        if (trim($arrDataset[0]) == '') {
          continue;
        }

        $arrPart = explode('.', $arrDataset[0]);
        if (strlen($arrPart[2]) == 4) {
          $arrDataset[0] = $arrPart[0] .'.' .$arrPart[1] .'.' .substr($arrPart[2], -2);
        }

        if (!isset($arrPeriod[$arrDataset[0]])) {
          $arrPeriod[$arrDataset[0]] = 0;
        }
        $arrPeriod[$arrDataset[0]]++;
      }

      asort($arrPeriod);

      $arrOutput = createTable($arrImport, $arrPeriod);

      $strNvmGroupSql = '';
      foreach ($arrOutput[2] as $strGroupNvm => $arrGroupNvm) {
        $strNvmGroupSql = $strGroupNvm;
      }

      $strOutputTable = $arrOutput[0];

      $arrNegativMerkmal = array();
      $boolNegativMerkmal = false;
      $strNvm = 'SELECT `PP`.`SF42_Comany_ID__c` AS `PpBnr`, `KK`.`SF42_Comany_ID__c` AS `KkBnr`, `Negativmerkmal__c`.* FROM `Negativmerkmal__c` INNER JOIN `Account` AS `PP` ON `AccountId` = `PP`.`Id` INNER JOIN `Account` AS `KK` ON `Auskunftsgeber__c` = `KK`.`Id` WHERE `GroupId` = "' .$strNvmGroupSql .'" AND `Negativmerkmal__c`.`Aktiv__c` = "true" AND `Auskunftsgeber_Typ__c` = "01230000001Ao72AAC"';
      $arrNvm = MySQLStatic::Query($strNvm);
  
      if (count($arrNvm) > 0) {
        foreach ($arrNvm as $intKey => $arrNDataset) {
          $strKey = $arrNDataset['PpBnr'] .'_' .$arrNDataset['KkBnr'] .'_ja';
          $arrNegativMerkmal[$strKey] = $arrNDataset['Art_Negativmerkmal__c'];
  
          $strOutputTable = str_replace('{' .$strKey .'}', $arrNDataset['Art_Negativmerkmal__c'], $strOutputTable);
  
        }
      }
  
      $strOutputTable = preg_replace('/\{.*\}/', '', $strOutputTable);

      $intCount = count($arrImport);
      //print_r($arrImport);
      if ($boolHasHeader) {
        $intCount--;
      }

      $strOutput .= '<form method="post" action="/cms/index.php?ac=eimp" style="display: inline;">' . chr(10);
      $strOutput .= '<input type="hidden" name="import" value="1">' . chr(10);
      $strOutput .= '<input type="hidden" name="arrData" value="' . str_replace('"', "'", serialize($arrOutput[1])) . '">' . chr(10);

      $strOutput .= '<input type="hidden" name="arrStat" value="' . str_replace('"', "'", serialize($arrOutput[2])) . '">' . chr(10);
      $strOutput .= '<input type="hidden" name="arrStatDetail" value="' . str_replace('"', "'", serialize($arrOutput[3])) . '">' . chr(10);
      $strOutput .= '<input type="hidden" name="strFileName" value="' . $strFile . '">' . chr(10);

      $strSql = 'SELECT * FROM `izs_sv_month` LIMIT 1';
      $arrSql = MySQLStatic::Query($strSql);
  
      $arrMonthAllowed = array(
        //date('m.Y', mktime(0, 0, 0, $arrSql[0]['Beitragsmonat__c'], -1, $arrSql[0]['Beitragsjahr__c'])),
        date('01.m.Y', mktime(0, 0, 0, $arrSql[0]['Beitragsmonat__c'], 1, $arrSql[0]['Beitragsjahr__c'])),
        //date('m.Y', mktime(0, 0, 0, $arrSql[0]['Beitragsmonat__c'], 32, $arrSql[0]['Beitragsjahr__c'])),
      );
  
      // allow : d=25; d=5,m=3
      $intDay   = date('j');
      $intMonth = date('n');
  
      if ($intDay >= 25) {
        $arrMonthAllowed[] = date('01.m.Y', mktime(0, 0, 0, $arrSql[0]['Beitragsmonat__c'], 32, $arrSql[0]['Beitragsjahr__c']));
      }
      
      if ($intDay <= 5) {
        $strMonth = date('01.m.Y', mktime(0, 0, 0, $intMonth, -1, date('Y')));
        if (!in_array($strMonth, $arrMonthAllowed)) {
          $arrMonthAllowed[] = $strMonth;
        }
      }
  
      $allowImport = true;
      $strNotAllowed = '';
      foreach ($arrPeriod as $strImportMonth => $intDatasetCount) {
        $arrPart = explode('.', $strImportMonth);
        $strImportMonth = $arrPart[0] .'.' .$arrPart[1] .'.20' .$arrPart[2];
        if (!in_array($strImportMonth, $arrMonthAllowed)) {
          $allowImport = false;
          if ($strNotAllowed != '') {
            $strNotAllowed.= ', ';
          }
          $strNotAllowed.= substr($strImportMonth, -7);
        }
      }
  
      if ($allowImport === false) {
        
        $strOutput .= '</form>' . chr(10);

        $strOutput .= '  <div class="ui-state-error" style="padding: 0px 7px; margin-right: 15px; margin-bottom: 10px; width: 1045px;">' . chr(10);
        $strOutput .= '    <h3>Achtung! Ein Import für diese(n) Monat(e) ist nicht möglich: ' .$strNotAllowed .'</h3>' . chr(10);
        $strOutput .= '  </div>' . chr(10);


      } else if (count($arrPeriod) != 1) {

        $strperiodList = '';
        foreach ($arrPeriod as $strPeriod => $intCount) {
          $strperiodList.= str_replace('.', '.20', preg_replace('/(\d{2}\.)(\d{2}\.)(\d{2})/', '$2$3', $strPeriod)) .', ';
        }
  
        $strperiodList = substr($strperiodList, 0, -2);

        $strOutput .= '<div class="clearfix" style="height: 153px; width: 1291px;">' . chr(10);
        $strOutput .= '  <div>' . chr(10);
        $strOutput .= '    <h3>' . $intCsvLines . ' Ergebnisse gefunden:</h3>' . chr(10);
        $strOutput .= '  </div>' . chr(10);
        $strOutput .= '  <div padding-top: 10px;">' . chr(10);

        $strOutput .= '  <div class="ui-state-error" style="padding: 0px 7px; margin-right: 15px; margin-bottom: 10px;">' . chr(10);
        $strOutput .= '    <h3>Achtung! Es sind mehrere Monate in der Import-Datei enthalten: ' .$strperiodList .' <button style="margin-left: 15px;">Trotzdem importieren!</button></h3>' . chr(10);
        $strOutput .= '  </div>' . chr(10);

        $strImport = 'Importieren';

        $strOutput .= '</form>' . chr(10);

      } else {

        $strOutput .= '<div class="clearfix" style="height: 48px; width: 1291px;">' . chr(10);
        $strOutput .= '  <div class="form-left">' . chr(10);
        $strOutput .= '    <h3>' . $intCsvLines . ' Ergebnisse gefunden:</h3>' . chr(10);
        $strOutput .= '  </div>' . chr(10);
        $strOutput .= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' . chr(10);

        $strImport = 'Importieren';

        $strOutput .= '<button id="import" class="ui-button ui-state-default ui-corner-all">' .$strImport .'</button>' . chr(10);
        
        $strOutput .= '</form>' . chr(10);

      }

      if (count($arrPeriod) != 1) {
        $strOutput .= '    <form method="post" action="_get_csv.php" style="display: inline;">';
      } else {
        $strOutput .= '    <form method="post" action="_get_csv.php" style="display: inline; float: right; padding-left: 10px;">';
      }

      $strOutput .= '    <input type="hidden" name="strTable" value="' . str_replace('"', "'", $strOutputTable) . '">' . chr(10);
      $strOutput .= '    <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' . chr(10);
      $strOutput .= '    </form>';

      $strOutput .= '  </div>' . chr(10);

      $strOutput .= '</div>' . chr(10);

      $strOutput .= $strOutputTable;

      /* ---------------- 

//echo 'arrData:' .chr(10);
//print_r($arrOutput[1]);

echo chr(10) .chr(10);

echo 'arrStat:' .chr(10);
print_r($arrOutput[2]);

echo chr(10) .chr(10);

echo 'arrStatDetail:' .chr(10);
print_r($arrOutput[3]);


 ---------------- */

      $strOutput .= "
<script>
$('#5').fixheadertable({ 
   colratio    : [60, 70, 58, 241, 70, 58, 70, 210, 58, 45, 150, 150], 
   height      : 500, 
   width       : 1265, 
   zebra       : true, 
   sortable    : true,
   sortType    : ['string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string'],
   sortedColId : 10, 
   dateFormat  : 'Y-m'
});
</script>
";
    } else {

      $strOutput .= '<p>Keine Daten gefunden.</p>' . chr(10);
    }


    //echo $objCsvFile->build_table();


  }
} else {

  $intNow = time();

  $arrFiles = glob($strUploadDir . '/*');
  if (is_array($arrFiles) && (count($arrFiles) > 0)) {
    foreach ($arrFiles as $strFile) {
      if (is_file($strFile)) {
        $intModified = filectime($strFile);
        if (($intNow - $intModified) > 5 * 24 * 3600) {
          @unlink($strFile);
        }
      }
    }
  }


  $timestamp = time();

  $strOutput .= '
<p>&nbsp;</p>
<form method="post" action="index.php?ac=eimp">
	<div id="queue"><span id="emp">Drop Files here...</span></div>
	<input type="hidden" name="fname" id="fname" />
	<input type="hidden" name="ac" value="eimp" />
	<input type="file" name="file_upload" id="file_upload" />
	<p><input type="submit" id="fsubmit" disabled="disabled" /></p>
</form>

<script type="text/javascript">
$(document).ready(function($) {
    $(\'#file_upload\').uploadifive({
        \'auto\'             : true,
        \'multi\'            : false, 
        \'queueSizeLimit\'   : 10,
        \'uploadLimit\'      : 0, 
				\'formData\'         : { \'timestamp\' : \'' . $timestamp . '\',
        									     \'token\'     : \'' . md5('unique_salt' . $timestamp) . '\' },
        \'queueID\'          : \'queue\',
        \'itemTemplate\'     : \'<div class="uploadifive-queue-item"><span class="filename"></span><span class="fileinfo"></span><div class="close"></div></div>\', 
        \'uploadScript\'     : \'uploadifive.php\',
        \'onUploadComplete\' : function(file, data) { $("#fname").val(file.name); $("#fsubmit").removeAttr("disabled"); }, 
        \'onAddQueueItem\'   : function(file) { $("#emp").css("display", "none"); }, 
        \'onCancel\'         : function(file) { $("#fsubmit").attr("disabled", "disabled"); $("#fname").val(); setTimeout("$(\'#emp\').css(\'display\', \'block\');", 2000); } 
    });
});
</script>
';
}
