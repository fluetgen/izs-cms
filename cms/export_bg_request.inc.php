<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

include('conf/global.inc.php');
require_once('../assets/classes/class.mysql.php');
require_once('../assets/classes/class.database.php');

require_once('functions.inc.php');
require_once('bootstrap.inc.php');

require_once('classes/class.informationprovider.php');

$boolShowIp = false;
$strOutput = '';

$arrRecordType = array (
  'Delivery' => '01230000001Ao73AAC',
  'FollowUp' => '01230000001Ao74AAC',
  'Publication' => '01230000001Ao75AAC',
  'Review' => '01230000001Ao76AAC'
);

$arrRecordTypeFlip = array_flip($arrRecordType);

$strOutput.= '<h1>Export BG-Anfrageliste</h1>' .chr(10);

if (true) {

// SUCHE BEGIN 

if (true) {
  
  $strOutput.= '<form method="post" action="">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);
  $strOutput.= '<input type="hidden" name="ac" value="' .$_REQUEST['ac'] .'">' .chr(10);
  
  $strOutput.= '<table>' .chr(10);
  $strOutput.= '<tbody>' .chr(10);
  
  $strSql6 = 'SELECT * FROM `izs_bg` ORDER BY `Name`';
  
  $strIpSelected = '';
  
  $arrResult6 = MySQLStatic::Query($strSql6);
  if (count($arrResult6) > 0) {

    if ($_REQUEST['strSelInf'] == 'all') {
      $strSelectedAll = ' selected="selected"';
    } else {
      $strSelectedAll = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Berufsgenossenschaft: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelInf" id="strSelInf">' .chr(10);
    //$strOutput.= '    <option value=""' .$strSelectedAll .'>- alle Berufsgenossenschaften -</option>' .chr(10);
    
    foreach ($arrResult6 as $arrProvider) {
      $strSelected = '';
      if ($arrProvider['Id'] == $_REQUEST['strSelInf']) {
        $strSelected = ' selected="selected"';
        $strIpSelected = $arrProvider['Name'];
      } 
      $strOutput.= '    <option' .$strOptClass .' value="' .$arrProvider['Id'] .'"' .$strSelected .'>' .$arrProvider['Name'] .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }

  /*
  $strPwNo = '';
  $strPwYes = '';

  if ($_REQUEST['strSelPass'] == 'ja') {

    $strPwYes = ' selected="selected"';

  } elseif ($_REQUEST['strSelPass'] == 'nein') {

    $strPwNo = ' selected="selected"';

  }


  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Passwortschutz (Links): </td>' .chr(10);
  $strOutput.= '    <td><table><tr><td><select name="strSelPass" id="strSelPass">' .chr(10);
  $strOutput.= '    <option value="ja"' .$strPwYes .'>ja</option>' .chr(10);
  $strOutput.= '    <option value="nein"' .$strPwNo .'>nein</option>' .chr(10);
  $strOutput.= '    </select></td></tr></table></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  */

  $strOutput.= '</tbody>' .chr(10);

  $strOutput.= '  <tfoot>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td align="right" colspan="2"><input type="submit" name="Suche" value="Suche" /></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);
  $strOutput.= '  </tfoot>' .chr(10);

  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);
    
}

if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {

  //date_format(str_to_date('February 2015','%M %Y'),'%Y-%m-01 %H:%i:%s') as date
  //UNIX_TIMESTAMP(STR_TO_DATE('Apr 15 2012 12:00AM', '%M %d %Y %h:%i%p')

  $strSql = 'SELECT `Berufsgenossenschaft__c` AS `acid`, `Meldestelle__c` AS `acid_meldestelle`, `Mitgliedsnummer_BG__c` AS `ac_bg_number`, ';
  $strSql.= '`SF42_Comany_ID__c` AS `ac_betriebsnummer`, `SF42_Company_Group__c` AS `ac_cgid`, `Account`.`Name` AS `ac_name`, ';
  $strSql.= '`Strasse_Meldestelle__c` AS `ac_street`, `PLZ_Meldestelle__c` AS `ac_zip`, `Ort_Meldestelle__c` AS `ac_city` ';
  $strSql.= 'FROM `Verbindung_Meldestelle_BG__c` ';
  $strSql.= 'INNER JOIN `Account` ON `Verbindung_Meldestelle_BG__c`.`Meldestelle__c` = `Account`.`Id` ';
  $strSql.= 'INNER JOIN `Group__c` ON `Account`.`SF42_Company_Group__c` = `Group__c`.`Id` ';
  $strSql.= 'WHERE `Verbindung_Meldestelle_BG__c`.`Aktiv__c` = "true" AND `Berufsgenossenschaft__c` = "' .$_REQUEST['strSelInf'] .'" ';
  $strSql.= 'AND `Account`.`Ist_BG_Meldestelle__c` = "true" ';
  $strSql.= 'AND `Group__c`.`Liefert_Daten__c` = "true" ';
  $strSql.= 'AND ((`Befristet_bis__c` = "0000-00-00") OR (`Befristet_bis__c` <= CURDATE())) ';
  $strSql.= 'ORDER BY `Account`.`Name`';

  $arrInformationProvider = MySQLStatic::Query($strSql);
  
  //echo $strSql;
  //print_r($arrInformationProvider); die();
  //print_r($arrDecentral);
  
  $strExportTable = '';
  
  if (count($arrInformationProvider) > 0) {
    //natcasesort($arrInformationProvider);

    //	BNR	Arbeitgeber	Konto	Rückstand	EUR > 500	Unbedenklichkeitsbescheinigung	Haupt-Betriebsnummer	Vollmacht-Link

    $strOutputTable = '';
    $strOutputTable.= '<table class="" id="5">' .chr(10);
    $strOutputTable.= '  <thead>' .chr(10);
    $strOutputTable.= '    <tr>' .chr(10);

    $strOutputTable.= '      <th class="header">Mitgliedsnummer</th>' .chr(10);
  	$strOutputTable.= '      <th class="header">Meldestelle</th>' .chr(10); //class="header"

  	$strOutputTable.= '      <th class="header">Straße / Meldestelle</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header">PLZ / Meldestelle</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header">Ort / Meldestelle</th>' .chr(10); //class="header"

    $strOutputTable.= '      <th class="header">Vollmacht-Link</th>' .chr(10); //class="header"

    $strOutputTable.= '    </tr>' .chr(10);
    $strOutputTable.= '  </thead>' .chr(10);
    $strOutputTable.= '  <tbody>' .chr(10);
    
    $strExportTable.= $strOutputTable;
    
    //print_r($arrInformationProvider);

    foreach ($arrInformationProvider as $strId => $arrEvent) {

        $strOutputTable.= '    <tr>' .chr(10);
        $strExportTable.= '    <tr>' .chr(10);

        $strOutputTable.= '      <td>' .$arrEvent['ac_bg_number'] .'</td>' .chr(10);
        $strExportTable.= '      <td>' .$arrEvent['ac_bg_number'] .'</td>' .chr(10);

        $strOutputTable.= '      <td>' .$arrEvent['ac_name'] .'</a></td>' .chr(10);
        $strExportTable.= '      <td>' .$arrEvent['ac_name'] .'</td>' .chr(10);

        $strOutputTable.= '      <td>' .$arrEvent['ac_street'] .'</a></td>' .chr(10);
        $strExportTable.= '      <td>' .$arrEvent['ac_street'] .'</td>' .chr(10);

        $strOutputTable.= '      <td>' .$arrEvent['ac_zip'] .'</a></td>' .chr(10);
        $strExportTable.= '      <td>' .$arrEvent['ac_zip'] .'</td>' .chr(10);

        $strOutputTable.= '      <td>' .$arrEvent['ac_city'] .'</a></td>' .chr(10);
        $strExportTable.= '      <td>' .$arrEvent['ac_city'] .'</td>' .chr(10);

        //http://www.izs-institut.de/dl.php?f=a083000000UW4WuAAL_vollmacht_bg.pdf
        $strUrl = 'https://www.izs-institut.de/dl.php?f=' .$arrEvent['ac_cgid'] .'_vollmacht_bg.pdf';

        /*
        if ($_REQUEST['strSelPass'] == 'ja') {
            $strUrl = $arrEvent['Vollmacht-Link'];
        } elseif ($_REQUEST['strSelPass'] == 'nein') {
            $strUrl = str_replace('protect/', 'download/', $arrEvent['Vollmacht-Link']);
        }
        */
    
        $strOutputTable.= '      <td><a href="' .$strUrl .'" target="_blank">Download</a></td>' .chr(10);

        $strExportTable.= '      <td>' .$strUrl .'</td>' .chr(10);

      $strOutputTable.= '    </tr>' .chr(10);
      $strExportTable.= '    </tr>' .chr(10);
      
    }
      
    $strOutputTable.= '  </tbody>' .chr(10);
    $strOutputTable.= '</table>' .chr(10);

    $strExportTable.= '  </tbody>' .chr(10);
    $strExportTable.= '</table>' .chr(10);

if (@$_SERVER['HTTP_FKLD'] == 'on') {
  //echo $intCatchAllContinue;
}  
    
    $strOutput.= '<div class="clearfix" style="height: 48px; width: 1280px;">' .chr(10);
    $strOutput.= '  <div class="form-left">' .chr(10);
    $strOutput.= '    <h3>' .count($arrInformationProvider) .' Ergebnisse gefunden:</h3>' .chr(10);
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' .chr(10);
    $strOutput.= '    <form method="post" action="_get_csv.php">';
    $strOutput.= '    <input type="hidden" name="strTable" value="' .str_replace('"', "'", $strExportTable) .'">' .chr(10);
    $strOutput.= '    <input type="hidden" name="format" value="xlsx-bg">' .chr(10);
    $strOutput.= '    <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' .chr(10);
    $strOutput.= '    </form>';
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '</div>' .chr(10);
    
    $strOutput.= $strOutputTable;
    
    $strOutput.= "
<script>

$('#5').fixheadertable({ 
   colratio    : [150, 470, 200, 120, 200, 120], 
   height      : 700, 
   width       : 1278, 
   zebra       : true, 
   resizeCol   : true,
   sortable    : true,
   sortType    : ['string', 'string', 'string', 'string', 'string', 'string'],
   sortedColId : '1', 
   dateFormat  : 'Y-m'
});
</script>
";
          
  } else {
    
    $strOutput.= '<p>Keine Events im gewählten Zeitraum vorhanden.</p>' .chr(10);
    
  }

}

$strOutput.= '';

}

?>