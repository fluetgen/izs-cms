<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

include('conf/global.inc.php');
require_once('../assets/classes/class.mysql.php');
require_once('../assets/classes/class.database.php');

require_once('functions.inc.php');
require_once('bootstrap.inc.php');

require_once('classes/class.informationprovider.php');

$objIp = new InformationProvider;

$boolShowIp = false;
$strOutput = '';

$strPageTitle = 'Neue Vollmachten';

$strOutput.= '<h1>Export ' .$strPageTitle .'</h1>' .chr(10);

if (true) {

// SUCHE BEGIN 

$strSql0 = "SELECT `Month` FROM `izs_month_sv` ORDER BY `SF42_Year__c`  DESC, `SF42_Month__c`  DESC";
$arrResult0 = MySQLStatic::Query($strSql0);

if (count($arrResult0) > 0) {
  
  $strOutput.= '<form method="post" action="">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);
  $strOutput.= '<input type="hidden" name="ac" value="' .$_REQUEST['ac'] .'">' .chr(10);
  
  $strOutput.= '<table>' .chr(10);
  $strOutput.= '<tbody>' .chr(10);

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Monat: </td>' .chr(10);
  $strOutput.= '    <td><table><tr><td><select name="strSelDateFrom" id="strSelDateFrom">' .chr(10);
  
  $intPeriod = 0;

  foreach ($arrResult0 as $arrPeriod0) {
    $strSelected = '';
    $strValue = $arrPeriod0['Month'];

    if (empty($_REQUEST['strSelDateFrom']) && ($intPeriod == 0)) {
      $_REQUEST['strSelDateFrom'] = $strValue;
      //$_REQUEST['send'] = 1;
    }
    
    if ($strValue == $_REQUEST['strSelDateFrom']) {
      $strSelected = ' selected="selected"';
    } 
    $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .$arrPeriod0['Month'] .'</option>' .chr(10);
    $intPeriod++;
  }
  
  $strOutput.= '    </select></td></tr></table></td>' .chr(10);

  $strOutput.= '  </tr>' .chr(10);
  
  $strOutput.= '</tbody>' .chr(10);

  $strOutput.= '  <tfoot>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td align="right" colspan="2"><input type="submit" name="Suche" value="Suche" /></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);
  $strOutput.= '  </tfoot>' .chr(10);

  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);
    
}

if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {

  $arrPeriod = explode('-', $_REQUEST['strSelDateFrom']);

  $arrVollmacht = $objIp->arrGetNewVollmachtList ('', $arrPeriod[0], $arrPeriod[1]);
  
  if (count($arrVollmacht) > 0) {
    foreach ($arrVollmacht as $intKey => $strGroupIdVollmacht) {
        $arrVerRequest[] = $strGroupIdVollmacht;
    }
  }

  $arrPostData = http_build_query(array('id' => $arrVerRequest));
  $arrOpt = array(
      'http' => array(
          'method'  => 'POST',
          'header'  => 'Content-type: application/x-www-form-urlencoded',
          'content' => $arrPostData
      )
  );
  $resContext = stream_context_create($arrOpt);
  $strJson    = file_get_contents('https://www.izs-institut.de/api/ver/', false, $resContext);
  $arrRequest = json_decode($strJson, true);
  $arrVerListRaw = $arrRequest['content']['company'];

  
  //echo $strSql;
  //print_r($arrInformationProvider); die();
  //print_r($arrDecentral);
  
  $strExportTable = '';
  
  if (count($arrVerListRaw) > 0) {
    //natcasesort($arrInformationProvider);

    //	BNR	Arbeitgeber	Konto	Rückstand	EUR > 500	Unbedenklichkeitsbescheinigung	Haupt-Betriebsnummer	Vollmacht-Link

    $strOutputTable = '';
    $strOutputTable.= '<table class="" id="5">' .chr(10);
    $strOutputTable.= '  <thead>' .chr(10);
    $strOutputTable.= '    <tr>' .chr(10);
  	//$strOutputTable.= '      <th class="{sorter: false}">Status</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header">Name</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="header">BNR</th>' .chr(10);
    $strOutputTable.= '      <th class="header">Vollmacht</th>' .chr(10); //class="header"

    $strOutputTable.= '    </tr>' .chr(10);
    $strOutputTable.= '  </thead>' .chr(10);
    $strOutputTable.= '  <tbody>' .chr(10);
    
    $strExportTable.= $strOutputTable;
    
    //print_r($arrInformationProvider);

    foreach ($arrVerListRaw as $strId => $arrEvent) {

      $strOutputTable.= '    <tr>' .chr(10);
      $strExportTable.= '    <tr>' .chr(10);

      $strOutputTable.= '      <td>' .$arrEvent['name'] .'</a></td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['name-ID'] .'</td>' .chr(10);

      $strOutputTable.= '      <td>' .$arrEvent['btnr'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['btnr'] .'</td>' .chr(10);

      $strFile = '../download/' .$arrPeriod[1] .'/' .$arrPeriod[0] .'/' .$arrEvent['id'] .'_' .$arrPeriod[1] .'_' .$arrPeriod[0] .'_vollmacht_sv.pdf';
      $strDown = 'https://www.izs.de/download/' .$arrPeriod[1] .'/' .$arrPeriod[0] .'/' .$arrEvent['id'] .'_' .$arrPeriod[1] .'_' .$arrPeriod[0] .'_vollmacht_sv.pdf';

      if (file_exists(__DIR__ .'/' .$strFile)) {
        $strOutputTable.= '      <td><a href="' .$strDown .'" target="_blank">Download</a></td>' .chr(10);
        $strExportTable.= '      <td>' .$strFile .'</td>' .chr(10);
      } else {
        $strOutputTable.= '      <td></td>' .chr(10);
        $strExportTable.= '      <td></td>' .chr(10);
      }

      $strOutputTable.= '    </tr>' .chr(10);
      $strExportTable.= '    </tr>' .chr(10);
      
    }
      
    $strOutputTable.= '  </tbody>' .chr(10);
    $strOutputTable.= '</table>' .chr(10);

    $strExportTable.= '  </tbody>' .chr(10);
    $strExportTable.= '</table>' .chr(10);

if (@$_SERVER['HTTP_FKLD'] == 'on') {
  //echo $intCatchAllContinue;
}  


if (isset($_REQUEST['fu']) && ($_REQUEST['fu'] == 'download')) {

    $strDate = $_REQUEST['strSelDateFrom'];

    $arrPostData = http_build_query(array('id' => $arrVerRequest));
    $arrOpt = array(
        'http' => array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $arrPostData
        )
    );
    $resContext  = stream_context_create($arrOpt);
    $strUrl      = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') ? 'https' : 'http') . '://' .$_SERVER['HTTP_HOST'] .'/cms/concatVollmacht.php';
    $strResponse = file_get_contents($strUrl, false, $resContext);

    header("Content-type: application/pdf");
    header("Content-Disposition: attachment; filename=\"Neue_Vollmachten_" .$strDate .".pdf\"");
    echo $strResponse;
    exit;
  
}
    
    $strOutput.= '<div class="clearfix" style="height: 48px; width: 680px;">' .chr(10);
    $strOutput.= '  <div class="form-left">' .chr(10);
    $strOutput.= '    <h3>' .count($arrVerListRaw) .' Vollmachten gefunden:</h3>' .chr(10);
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' .chr(10);
    $strOutput.= '    <form method="post" action="' .$_SERVER['PHP_SELF'] .'?' .$_SERVER['QUERY_STRING'] .'&fu=download">';
    $strOutput.= '    <input type="hidden" name="strSelDateFrom" value="' .$_REQUEST['strSelDateFrom'] .'">' .chr(10);
    $strOutput.= '    <input type="hidden" name="send" value="1">' .chr(10);
    $strOutput.= '    <button id="doXlsxExport" class="ui-button ui-state-default ui-corner-all">Export</button> ' .chr(10);
    $strOutput.= '    <button id="export-pdf" class="ui-button ui-state-default ui-corner-all">PDF herunterladen</button>' .chr(10);
    $strOutput.= '    </form>';
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '</div>' .chr(10);
    
    $strOutput.= $strOutputTable;
    
    $strOutput.= "
<script>

$('#5').fixheadertable({ 
   colratio    : [430, 100, 100], 
   height      : 400, 
   width       : 648, 
   zebra       : true, 
   resizeCol   : true,
   sortable    : true,
   sortType    : ['string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string'],
   sortedColId : '0', 
   dateFormat  : 'Y-m'
});

</script>
";

$strJsFootCodeRun.= '
$("#doXlsxExport").bind("click", function(e) {

    var header = new Array();
    var list   = new Array();
  
    $.each($("th.header"), function () {
        //if ($(this).text() != "") header.push($(this).text());
        header.push($(this).text());
    });
  
    $.each($("#5 tbody tr"), function () {
        var tdarr = new Array();
        $.each($(this).find("td"), function () {
            var text = $(this).text();
            if ((text != "") && (text != "Download")) {
              tdarr.push(text);
            } else {
              tdarr.push($(this).children().attr("href"));
            }
        });
        list.push(tdarr);
    });
  
    //console.log(header);
    //console.log(list);
    
    var d = new Date;
    //var formattedDate = dateFormat(d, "yyyyMMdd_Hmmss");

    var datestring = d.getFullYear() + ("0"+(d.getMonth()+1)).slice(-2) + ("0" + d.getDate()).slice(-2) + "_" +
                     ("0" + d.getHours()).slice(-2) + ("0" + d.getMinutes()).slice(-2) + ("0" + d.getSeconds()).slice(-2);

    var fn = "' .str_replace(' ', '_', str_replace('/', '_', $strPageTitle)) .'_" + datestring + ".xlsx";
    //console.log(fn);

    $.ajax({

        type: "POST",
        url:  "inc/export_vollmacht.ajax.php",
        data: { ac: "export", list: list, header: header, format: "xlsx", name: fn },

        success: function() {
            location.href="/cms/files/temp/" + fn;
        }

    });


    
  });
';
          
  } else {
    
    $strOutput.= '<p>Keine Vollmachten im gewählten Zeitraum vorhanden.</p>' .chr(10);
    
  }

}

$strOutput.= '';

}

?>