<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

ini_set('memory_limit', '2048M');

session_start();

define('FPDF_FONTPATH','fpdf17/font/');
define('TEMP_PATH', 'temp/');

require_once('../assets/classes/class.mysql.php');
require('fpdf17/fpdf.php');
require('fpdf17/fpdi.php');

class concat_pdf extends fpdi {
    var $files = array();
    function concat_pdf($orientation='P',$unit='mm',$format='A4') {
        parent::__construct($orientation,$unit,$format);
    }
    function setFiles($files) {
        $this->files = $files;
    }
    function concat() {
        foreach($this->files AS $file) {
            $pagecount = $this->setSourceFile($file);
            for ($i = 1;  $i <= $pagecount;  $i++) {
                 $tplidx = $this->ImportPage($i);
                 $this->AddPage();
                 $this->useTemplate($tplidx);
            }
        }
    }
}

$strSql0 = 'SELECT `Id`, `Name`, `Betriebsnummer__c` FROM `Group__c` WHERE `SF42_Group_Type__c` = "Lender" AND `Liefert_Daten__c` = "true" ORDER BY `Name`';
$arrResult0 = MySQLStatic::Query($strSql0);
if (count($arrResult0) > 0) {
    
    foreach ($arrResult0 as $arrGroup) {

        $strOutputName = '../download/bg/' .$arrGroup['Id'] .'_vollmacht_bg.pdf';
  
        $boolMoreFiles = false;
        
        $strFile_00 = 'img/vollmacht/' .$arrGroup['Betriebsnummer__c'] .'_Vollmacht.jpg';
        $strFile_01 = 'img/vollmacht/' .$arrGroup['Betriebsnummer__c'] .'_Vollmacht_01.jpg';
        
        if (file_exists($strFile_00) || file_exists($strFile_01)) {
            if (file_exists($strFile_01)) {
                $boolMoreFiles = true;
            }
            $boolFileExists = true;
        } else {
            $boolFileExists = false;
        }

        if ($boolFileExists) {

            $pdf = new concat_pdf();
            $pdf->AliasNbPages();
          
            if ($boolMoreFiles) {

                for ($intFile = 1; $intFile < 20; $intFile++) {
                    $strFile = 'img/vollmacht/' .$arrGroup['Betriebsnummer__c'] .'_Vollmacht_' .str_pad($intFile, 2, '0', STR_PAD_LEFT) .'.jpg';
                    
                    echo $strFile;
                    echo chr(10);

                    if (file_exists($strFile)) {

                        $pdf->AddPage();
                        $pdf->SetAutoPageBreak(false);
                        $pdf->Image($strFile, 0, 0, 210, 297, 'JPG');
              
                    } else {
                        break;            
                    }
                }

            } else {
                $strFile = 'img/vollmacht/' .$arrGroup['Betriebsnummer__c'] .'_Vollmacht.jpg';

                echo $strFile;
                echo chr(10);

                if (file_exists($strFile)) {

                    $pdf->AddPage();
                    $pdf->SetAutoPageBreak(false);
                    $pdf->Image($strFile, 0, 0, 210, 297, 'JPG');
          
                }
            }

            if (file_exists($strOutputName)) {
                unlink($strOutputName);
            }

            echo $strOutputName;
            echo chr(10);
            echo chr(10);

            $pdf->Output($strOutputName, 'F');

        }

    }

}

?>