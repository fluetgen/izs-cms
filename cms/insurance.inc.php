<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

require_once('../assets/classes/class.mysql.php');
require_once('../assets/classes/class.database.php');

$uploadDir = 'pdf/';
$downloDir = 'pdf/bg/';
$strServer = 'http://' .$_SERVER['SERVER_NAME'] .'/cms/' .$downloDir;

$boolShowIp = false;

if (isset($_REQUEST['del']) && ($_REQUEST['del'] != '')) {

  $strSql = 'SELECT `Dokumenten_Link__c` FROM `Berufsgenossenschaft__c` WHERE `Dokumenten_Link__c` != "" AND `Id` = "' .$_REQUEST['del'] .'"';
  $arrResult = MySQLStatic::Query($strSql);
  
  unlink($downloDir .$arrResult[0]['Dokumenten_Link__c']);
 
  $strSql = 'DELETE FROM `Berufsgenossenschaft__c` WHERE `Id` = "' .$_REQUEST['del'] .'"';
  $arrResult = MySQLStatic::Query($strSql);
  
}

$strOutput = '';

$arrBGCss = array ( 
  'OK' => 'gruen', 
  'NOT OK' => 'rot'
);

$arrSperr = array (
  'true' => 'ja',
  'false' => 'nein'
);

$arrSichtbar = array (
  'online' => 'ja',
  'private' => 'nein',
  'ready' => 'auf Anfrage'  
);

$strOutput.= '<h1>Berufsgenossenschaft</h1>' .chr(10);



$_REQUEST['send'] = 1;


if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {

  
  //$arrPeriod = explode('_', $_REQUEST['strSelDate']);
  
  $strSql = 'SELECT Id, Name, Beitragsschuldner__c, Mitgliedsnummer__c,   Berufsgenossenschaft__c, ';
  $strSql.= 'Status__c, Bemerkung__c,   Beitragsjahr__c, Dokumenten_Link__c, Company_Group__c ';
  $strSql.= 'FROM Berufsgenossenschaft__c ';
  $strSql.= 'ORDER BY Beitragsschuldner__c ';
 
  $arrResult = MySQLStatic::Query($strSql);
  
  $arrBerufsGenossenschaft = array();
  
  if (count($arrResult) > 0) {
    
    $arrBerufsGenossenschaft = $arrResult;
  
  }
  
  //echo count($arrResult);
  //print_r($arrBerufsGenossenschaft);
  //print_r($arrDecentral);
  
  if (count($arrBerufsGenossenschaft) > 0) {
    //natcasesort($arrBerufsGenossenschaft);
    
    $strOutputTable.= '<table class="" id="5">' .chr(10);
    $strOutputTable.= '  <thead>' .chr(10);
    $strOutputTable.= '    <tr>' .chr(10);

    $strOutputTable.= '      <th>Status</th>' .chr(10);
    $strOutputTable.= '      <th>Jahr</th>' .chr(10);
    $strOutputTable.= '      <th>Company_Group</th>' .chr(10);
    $strOutputTable.= '      <th>Beitragsschuldner</th>' .chr(10);
    $strOutputTable.= '      <th>Berufsgenossenschaft</th>' .chr(10);
    $strOutputTable.= '      <th>Nummer</th>' .chr(10);
    $strOutputTable.= '      <th>Bemerkung</th>' .chr(10);			
    $strOutputTable.= '      <th>DL</th>' .chr(10);
    $strOutputTable.= '      <th></th>' .chr(10);

    $strOutputTable.= '    </tr>' .chr(10);
    $strOutputTable.= '  </thead>' .chr(10);
    $strOutputTable.= '  <tbody>' .chr(10);
    
    //$arrPeriod = explode('_', $_REQUEST['strSelDate']);
    
    $strNext = '';

    foreach ($arrBerufsGenossenschaft as $strId => $arrBG) {
      
      
      $strOutputTable.= '    <tr>' .chr(10);
      if ($arrBG['Status__c'] != '') {
        $strOutputTable.= '      <td align="center"><img src="/assets/images/sys/ampel_' .$arrBGCss[$arrBG['Status__c']] .'.png" /></td>' .chr(10);
      } else {
        $strOutputTable.= '      <td align="center"></td>' .chr(10);
      }

      $strOutputTable.= '      <td>' .$arrBG['Beitragsjahr__c'] .'</td>' .chr(10);
      
      $strSql3 = 'SELECT Name FROM Group__c WHERE Id = "' .$arrBG['Company_Group__c'] .'"';
      $arrResult3 = MySQLStatic::Query($strSql3);
      $strOutputTable.= '      <td>' .$arrResult3[0]['Name'] .'</td>' .chr(10);
      
      $strOutputTable.= '      <td>' .$arrBG['Beitragsschuldner__c'] .'</td>' .chr(10);
      
      $strSql3 = 'SELECT Name FROM Account WHERE Id = "' .$arrBG['Berufsgenossenschaft__c'] .'"';
      $arrResult3 = MySQLStatic::Query($strSql3);
      $strOutputTable.= '      <td>' .$arrResult3[0]['Name'] .'</td>' .chr(10);
      
      $strOutputTable.= '      <td>' .$arrBG['Mitgliedsnummer__c'] .'</td>' .chr(10);
      $strOutputTable.= '      <td>' .$arrBG['Bemerkung__c'] .'</td>' .chr(10);
      
      if ($arrBG['Dokumenten_Link__c'] != '') {
        $strDL = '<a target="_blank" href="pdf/bg/' .$arrBG['Dokumenten_Link__c'] .'"><img src="img/icon_pdf.png" alt="' .$arrBG['Dokumenten_Link__c'] .'" /></a>';
      } else {
        $strDL = '';
      }
      $strOutputTable.= '      <td id="file_e_' .$arrBG['Id'] .'_l">' .$strDL .'</td>' .chr(10);
      $strOutputTable.= '      <td><a href="index.php?ac=insu&del=' .$arrBG['Id'] .'"><img src="/cms/img/f_delete.png"></a></td>' .chr(10);
      $strOutputTable.= '    </tr>' .chr(10);
      
    }
   
    
    $strOutputTable.= '  </tbody>' .chr(10);
    $strOutputTable.= '</table>' .chr(10);

    
    $strOutput.= '<div class="clearfix" style="height: 48px; width: 1470px;">' .chr(10);
    $strOutput.= '  <div class="form-left">' .chr(10);
    $strOutput.= '    <h3>' .count($arrBerufsGenossenschaft) .' Ergebnisse gefunden:</h3>' .chr(10);
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' .chr(10);
    $strOutput.= '    <button class="ui-button ui-state-default ui-corner-all" id="start-note">Bescheinigung erfassen</button>&nbsp;' .chr(10);
    $strOutput.= '    <form method="post" action="_get_csv.php" style="display: inline;">';
    $strOutput.= '    <input type="hidden" name="strTable" value="' .str_replace('"', "'", $strOutputTable) .'">' .chr(10);
    $strOutput.= '    <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' .chr(10);
    $strOutput.= '    </form>';
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '</div>' .chr(10);

    $strOutput.= $strOutputTable;
    
    $strOutput.= "
<script>
$('#5').fixheadertable({ 
   colratio    : [40, 40, 300, 300, 300, 100, 300, 40, 40], 
   height      : 500, 
   width       : 1490, 
   zebra       : true, 
   sortable    : true,
   sortType    : ['string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', ''],
   sortedColId : 2, 
   dateFormat  : 'Y-m'
});
</script>
";
    
  } else {
    
    $strOutput.= '<p>Keine BG-Bescheinigung vorhanden.</p>' .chr(10);
    $strOutput.= '<button class="ui-button ui-state-default ui-corner-all" id="start-note">Bescheinigung erfassen</button>&nbsp;' .chr(10);
    
  }
  

$strOutput.= '

<style>
#queue, #queueM {
	height: 79px;
	overflow: auto;
	margin-bottom: 10px;
	padding: 0 3px 3px;
	width: 365px;

  background-color: #5BB75B;
  background-repeat: repeat-x;
  border-radius: 4px;
  color: #FFFFFF;
}

#validateTips {
  margin: 10px 0 5px;
  width: 590px;
  color: #ff0000;
  font-weight: bold;
}


td {
  padding-bottom: 5px;
}

#dialog_note textarea {
  margin-bottom: 0px;
}

#emp, #empM {
  text-align: center;
  width: 365px;
  display: block;
  padding-top: 30px;
}

.uploadifive-queue-item .close {
    background: url("/cms/img/uploadifive-cancel.png") no-repeat scroll 0 2px rgba(0, 0, 0, 0);
}
</style>

<div id="dialog_note" title="Neue BG-Bescheinigung erfassen">
</div>

	<script type="text/javascript">

	
  $(document).ready(function() {


		$(\'#start-note\').click(function() {

		  $("#date_all").val("");
		  $("#artr_all").val("<leer>");
		  $("#artd_all").val("<leer>");
		  $("#arts_all").val("<leer>");


      $(\'#queue\').html(\'<span id="emp">Drop Files here...</span>\');
      $(\'#file_upload\').uploadifive(\'clearQueue\');
			$(\'#dialog_note\').load(\'_ajax_bg.php\', {ac: "add"}).dialog(\'open\');
			$("#dialog_note ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');
			  
		})
		.hover(
			function(){ 
				$(this).addClass("ui-state-hover"); 
			},
			function(){ 
				$(this).removeClass("ui-state-hover"); 
			}
		).mousedown(function(){
			$(this).addClass("ui-state-active"); 
		})
		.mouseup(function(){
				$(this).removeClass("ui-state-active");
		});



		var tips = $("#validateTips");

		function updateTips(t) {
			$("#validateTips").text(t).effect("highlight",{color:"#F49595"},1500);
		}

		function checkLength(o,f,min) {
			if ( o.val().length < min ) {
				o.addClass(\'ui-state-error\');
				updateTipsM("Bitte das Feld \'" +f +"\' ausfüllen.");
				return false;
			} else {
				return true;
			}
		}
		
		function checkEnty(o,min) {
			if ( o.val().length < min ) {
				return false;
			} else {
				return true;
			}
		}

		$("#dialog_note").dialog({
			bgiframe: true,
			autoOpen: false,
			resizable: false,
			height: 550,
			width: 620,
			modal: true,
			buttons: {
				\'OK\': function() {

					var bValid = true;
					bValid = bValid && checkEnty($(\'#fnames\'),1);
					if (!bValid) {
					  updateTips("Bitte lade ein Dokument hoch.");
					} else {
					  bValid = bValid && checkEnty($(\'#Company_Group__c\'),1);
					  if (!bValid) {
					    updateTips("Bitte wähle die Company Group.");
					  } else {
					    bValid = bValid && checkEnty($(\'#Beitragsschuldner__c\'),1);
					    if (!bValid) {
					      updateTips("Bitte gib den Namen des Beitragsschuldners ein.");
					    } else {
					      bValid = bValid && checkEnty($(\'#Mitgliedsnummer__c\'),1);
  					    if (!bValid) {
  					      updateTips("Bitte gib die Mitgliedsnummer ein.");
  					    } else {
                  if ($(\'#Bemerkung__c\').val().length > 160) {
    					      updateTips("Bitte gib bei Bemerkung max. 160 Zeichen ein.");
    					      bValid = false;
    					    } 
  					    }
					    }
					  }
					}
									  
				  if (bValid) {
  				
            $.ajax({
              type: "POST",
              url:  "_ajax_bg.php",
              dataType: "html; charset=utf-8", 
              data: {               
                ac: "save", 
                Company_Group__c: $("#Company_Group__c").val(), 
                Beitragsjahr__c: $("#Beitragsjahr__c").val(), 
                Beitragsschuldner__c: $("#Beitragsschuldner__c").val(), 
                Mitgliedsnummer__c: $("#Mitgliedsnummer__c").val(), 
                Status__c: $("#Status__c").val(), 
                Berufsgenossenschaft__c: $("#Berufsgenossenschaft__c").val(), 
                Bemerkung__c: $("#Bemerkung__c").val(), 
                fnames: $("#fnames").val()
               },
              success: function() {
                //var timeout = setTimeout("location.reload()", 3000);
                //location.reload();
                location.href = "/cms/index.php?ac=insu";
              },
            });
  
  		      $(this).dialog(\'close\');
            
          }

				},
				\'Abbrechen\': function() {
					$(this).dialog(\'close\');
				}
			},
			close: function() {
			  tips.text(\' \');
			}
		});
		
	});
	
	</script>

  ';     


}


?>