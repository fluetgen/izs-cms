<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

if ($boolStandAlone === true) {

    require_once('../const.inc.php');
    require_once('../../assets/classes/class.mysql.php');

    require_once('../inc/refactor.inc.php');

    require_once ('../fpdf182/fpdf.php');
    require_once ('../fpdf182/class.pdfhtml.php');

}

$boolDebug = false;
if (@$_COOKIE['id'] == 3) {
    $boolDebug = false;
}

$intDurationTime = microtime(true) - $intBeginTime;
if ($_REQUEST['d'] == 't') echo " Include: $intDurationTime Sek." .chr(10);

// INPUT
if (!isset($intEventId) || ($intEventId == '')) {
    //http_response_code(404);
    //include('my_404.php'); // provide your own HTML for the error page
    //die();
} 



function boolIsUnique ($strUnique = '') {

    $strSql = 'SELECT `ed_id` FROM `izs_event_download` WHERE `ed_hash` = "' .$strUnique .'"';
    $arrSql = MySQLStatic::Query($strSql);
    
    if (count($arrSql) == 0) {

        return $strUnique;

    } else {

        boolIsUnique(uniqid());

    }

}

function createCert ($intEventId = 0, &$objPdf = null, $strReturnType = 'string') {

    global $intBeginTime, $arrPdfLinkListEvent, $boolDebug;

    if (!isset($strReturnType)) {
        $strReturnType = 'string';
    }

    // OUTPUT
    $strOutput = '';

    $strSql = 'SELECT * FROM `izs_bg_event` WHERE `beid` = "' .$intEventId .'"';
    $arrSql = MySQLStatic::Query($strSql);

    $strSql1 = 'SELECT * FROM `Account` WHERE `Id` = "' .$arrSql[0]['be_meldestelle'] .'"'; //
    $arrSql1 = MySQLStatic::Query($strSql1);
    //if ($boolDebug) { echo $strSql1; print_r($arrSql1); die(); } 

    $strSql2 = 'SELECT * FROM `Account` WHERE `Id` = "' .$arrSql[0]['be_bg'] .'"';
    $arrSql2 = MySQLStatic::Query($strSql2);

    $strSql4 = 'SELECT * FROM `Account` WHERE `Id` = "' .$arrSql1[0]['Id'] .'"';
    $arrSql4 = MySQLStatic::Query($strSql4);


    $intDurationTime = microtime(true) - $intBeginTime;
    if ($_REQUEST['d'] == 't') echo " Select: $intDurationTime Sek." .chr(10);

    //print_r($arrSql); die();

    if (count($arrSql) > 0) {

        $boolStandAlone = false;


        if (!isset($objPdf)) {

            $boolStandAlone = true;
        }

        $strUnique = boolIsUnique(uniqid());

        $strSql = 'INSERT INTO `izs_event_download` (`ed_id`, `ed_hash`, `ed_ev_id`, `ed_dataset`, `ed_download_time`, `ed_download_ip`, `ed_type`) VALUES (NULL, "'.$strUnique .'", "' .$arrSql[0]['evid'] .'", "' .(addslashes(serialize($arrSql[0]))) .'", NOW(), "' .$_SERVER['REMOTE_ADDR'] .'", "bg")';
        $intSql = MySQLStatic::Insert($strSql);

        $strHost = 'www';
        if (isset($_SERVER['SERVER_NAME']) && (strstr($_SERVER['SERVER_NAME'], 'test.') !== false)) {
            $strHost = 'test';
        }

        $strHash = 'https://' .$strHost .'.izs.de/cms/bg-event/' .$strUnique;

        $strUid  = md5(uniqid(rand(), true));

        if ($boolStandAlone === true) {
            $strPath = '../files/temp/' .$strUid .'.png';
        } else {
            $strPath = '../cms/files/temp/' .$strUid .'.png';
        }
        
        
        QRcode::png($strHash, $strPath);

        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " QR-Code: $intDurationTime Sek." .chr(10);

        $floatLeft = 10.8;
        $floatCol1 = 81.5;

        if (!isset($objPdf)) {

            $objPdf = new pdfHtml('L');
            $objPdf->AliasNbPages();

            $objPdf->AddFont('Lato', '', 'Lato-Regular.php');
            $objPdf->AddFont('Lato', 'B', 'Lato-Bold.php');

            $objPdf->AddFont('OfficinaSerif', '', 'itcofficinaserifw04medium-webfont.php');
            $objPdf->AddFont('OfficinaSerif', 'B', 'itcofficinaserifw04medium-webfont.php');
            
        } 

        $objPdf->AddPage('L');
        $objPdf->SetAutoPageBreak(false);

        if ($boolStandAlone === false) {
            $objPdf->SetLink($arrPdfLinkListEvent[$arrSql[0]['beid']], 0, -1);
        }

        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " Init PDF: $intDurationTime Sek." .chr(10);
        
        $objPdf->Image( __DIR__ .'/img/bg_pdf.jpg', 0, 0, 297, 210, 'JPG'); 


        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " Include BG: $intDurationTime Sek." .chr(10);

        //Page margin
        $objPdf->SetMargins($floatLeft, 10.8);

        //Logo
        $objPdf->Image( __DIR__ .'/img/logo.png', $floatLeft, 10.8, 47.8, 28);

        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " Include Logo: $intDurationTime Sek." .chr(10);

        $objPdf->SetTextColor(96, 94, 85); //#605e55
        $objPdf->SetFont('OfficinaSerif', 'B', 45);
        $objPdf->Text($floatCol1, 25.8, utf8_decode('BG-ZERTIFIKAT'));

        $objPdf->SetTextColor(96, 94, 85); //#605e55
        $objPdf->SetFont('Lato', '', 18);
        $objPdf->Text($floatCol1, 38.5, utf8_decode('mit Haftungsschutz für berechtigte Entleiher*'));


        //QR-Code
        $objPdf->Image($strPath, 271.3, $floatLeft, 15, 15);
        unlink($strPath);


        $objPdf->SetTextColor(96, 94, 85); //#605e55
        $objPdf->SetFont('Lato', '', 8);
        $objPdf->Text($floatLeft, 61, utf8_decode('Dieses Zertifikat wurde erstellt:'));

        //Icons
        $objPdf->SetTextColor(96, 94, 85); //#605e55
        $objPdf->SetFont('Lato', '', 8);
        $objPdf->Image( __DIR__ .'/img/calendar.png', $floatLeft, 65, 4.2, 4.2);
        $objPdf->Text(17, 68, utf8_decode(date('d.m.Y')));
        $objPdf->Image( __DIR__ .'/img/wall-clock.png', 36, 65, 4.2, 4.2);
        $objPdf->Text(42, 68, utf8_decode(date('H:i:s') .' CET'));
        $objPdf->Image( __DIR__ .'/img/documents.png', $floatLeft, 73, 4.2, 4.2);
        $objPdf->Text(17, 76.5, utf8_decode($arrSql[0]['be_name']));


        $strUbb = '';
        /*
        if (($arrSql4[0]['UbbFormulierung__c'] != '') && ($arrSql4[0]['UbbSeitMonat__c'] != '') && ($arrSql4[0]['UbbSeitJahr__c'] != '')) {
            $strUbb = 'Nach unserem Kenntnisstand wurden seit ' .$arrSql4[0]['UbbSeitMonat__c'] .'/' .$arrSql4[0]['UbbSeitJahr__c'] .' fällige SV-Beiträge ' .$arrSql4[0]['UbbFormulierung__c'] .' bezahlt.';
        }
        */
        $strTextAdd = '';
        if ($arrSql[0]['be_meldepflicht'] == 'nein') {
            $strTextAdd.= 'Die Meldepflicht wurde nicht erfüllt.';
        }

        if ($arrSql[0]['be_ergebnis'] == 'OK') {

            if (($arrSql[0]['be_stundung'] == '') && ($strTextAdd == '')) { // OK

                $intY = 71;
                $objPdf->Image( __DIR__ .'/img/ok.gif', $floatCol1, 58, 21.5, 21.5);
                $objPdf->SetTextColor(96, 94, 85); //#605e55
                $objPdf->SetFont('Lato', 'B', 22);

                if ($strUbb != '') $intY = $intY - 3;
                $objPdf->Text(111, $intY, utf8_decode('Das Beitragskonto ist ausgeglichen'));
                $objPdf->SetFont('Lato', 'B', 10);
                if ($strUbb != '') {
                    $objPdf->Text(111, $intY + 6, utf8_decode($strUbb));
                }

            } else { // OK + Stundung / Rate

                if ($arrSql[0]['be_stundung'] == 'Ratenzahlung') {
                    $strStundung = 'Es besteht eine Ratenzahlungsvereinbarung.';
                } elseif ($arrSql[0]['be_stundung'] == 'Stundung') {
                    $strStundung = 'Es besteht eine Stundungsvereinbarung.';
                } elseif ($arrSql[0]['be_stundung'] == 'Stundung oder Ratenzahlung') {
                    $strStundung = 'Es besteht eine Stundungs- oder Ratenzahlungsvereinbarung.';
                }

                if ($strTextAdd != '') {
                    if ($strStundung != '') {
                        $strStundung.= ' ';
                    } 
                    $strStundung.= $strTextAdd;
                } 

                $intY = 71;
                $objPdf->Image( __DIR__ .'/img/oknote.gif', $floatCol1, 58, 26.1, 21.5);
                $objPdf->SetTextColor(96, 94, 85); //#605e55
                $objPdf->SetFont('Lato', 'B', 22);

                if ($strUbb != '') $intY = $intY - 5;
                $objPdf->Text(116, $intY, utf8_decode('Das Beitragskonto ist ausgeglichen'));
                $objPdf->SetFont('Lato', 'B', 10);
                if ($strUbb != '') {
                    $objPdf->Text(116, $intY + 6, utf8_decode($strUbb));
                }

                $objPdf->SetTextColor(218, 117, 56); //#DA7538
                $objPdf->Text(116, 79, utf8_decode($strStundung));

            }


        } else { // NOTOK

            $strUbb = '';
            $strStundung = '';

            if ($arrSql[0]['be_stundung'] == 'Ratenzahlung') {
                $strStundung = 'Es besteht eine Ratenzahlungsvereinbarung.';
            } elseif ($arrSql[0]['be_stundung'] == 'Stundung') {
                $strStundung = 'Es besteht eine Stundungsvereinbarung.';
            } elseif ($arrSql[0]['be_stundung'] == 'Stundung oder Ratenzahlung') {
                $strStundung = 'Es besteht eine Stundungs- oder Ratenzahlungsvereinbarung.';
            }

            if ($strStundung != '') {
                $strStundung.= ' ' .$strTextAdd;
            }

            $intY = 71;
            $objPdf->Image( __DIR__ .'/img/notok.gif', $floatCol1, 58, 21.5, 21.5);
            $objPdf->SetTextColor(96, 94, 85); //#605e55
            $objPdf->SetFont('Lato', 'B', 22);

            //if ($strUbb != '') $intY = $intY - 3;
            if ($strStundung != '') $intY = $intY - 3;
            $objPdf->Text(111, $intY, utf8_decode('Das Beitragskonto ist nicht ausgeglichen'));
            
            /*
            $objPdf->SetFont('Lato', 'B', 10);
            if ($strUbb != '') {
                $objPdf->Text(111, $intY + 6, utf8_decode($strUbb));
            }
            */

            if ($strStundung != '') {
                $objPdf->SetTextColor(218, 117, 56); //#DA7538
                $objPdf->SetFont('Lato', 'B', 10);
                $objPdf->Text(111, $intY + 6, utf8_decode($strStundung));
            }


        }


        $strText = utf8_decode('Dieses Zertifikat ist nur gültig mit dem QR-Code rechts oben, über den der Inhalt dieses Zertifikats überprüft werden kann.' .chr(10) . chr(10) .'Alternativ kann die Überprüfung auch über folgenden Link erfolgen:' .chr(10) . chr(10));

        $objPdf->SetY(95);
        $objPdf->SetTextColor(96, 94, 85); //#605e55
        $objPdf->SetFont('Lato', '', 8);
        $objPdf->MultiCell(48, 4, $strText, 0, 'L');

        $strUrl = $strHost .'.izs.de/cms/bg-event/' .$strUnique;
        $strHtml = '<a href="https://' .$strUrl .'">' .$strUrl .'</a>';

        $objPdf->setLinkColor(240, 102, 0); //#f06600
        $objPdf->WriteHTML(utf8_decode($strHtml));


    //www.izs.de/e-10424092

        if (isset($_COOKIE['id']) && ($_COOKIE['id'] == 3)) {
            //print_r($arrSql4); die();
            //echo $arrSql4[0]['Registriert_seit__c']; die();
        }

        $strRegistriert = '';
        if ($arrSql4[0]['Registriert_seit__c'] == '0000-00-00') {
            $strRegistriert = '12/2011';
        } else {
            $arrPart = explode('-', $arrSql4[0]['Registriert_seit__c']);
            $strRegistriert = $arrPart[1] .'/' .$arrPart[0];
        }


        $objPdf->SetFillColor(128, 128, 128);
        $boolFill = false;

        $floatCol1 = 81.5; //FIX!
        $floatCol2 = 141.5;
        $floatCol3 = 201.5;

        $floatMinWidth = 55;

        $floatWidthCol1 = $floatMinWidth;
        $floatWidthCol2 = $floatMinWidth;
        $floatWidthCol3 = $floatMinWidth;

        $floatMaxWidth12 = 148;
        $floatSpaceWidth = 5;

        $strNameAdd = '';
        if (strstr($arrSql1[0]['Name'], 'GmbH & Co. KG') !== false) {
            $strNameAdd = 'GmbH' .chr(160) .'&' .chr(160) .'Co.' .chr(160) .'KG';
        }

        $objPdf->SetFont('Lato', 'B', 14);

        //$floatMaxHeight = max($floatHeight1, $floatHeight2, $floatHeight3);
        //echo $floatMaxHeight; die();

        if ($arrSql2[0]['SF42_Account_name_short__c'] != '') {
            $strBGName = $arrSql2[0]['SF42_Account_name_short__c'];
        } else {
            $strBGName = $arrSql2[0]['Name'];
        }

        $floatWidth1  = $objPdf->GetStringWidth($arrSql1[0]['Name']);
        $floatWidth2  = $objPdf->GetStringWidth($strBGName);
        $floatWidth3  = $objPdf->GetStringWidth('Direkte Auskunft an IZS');

        $boolGreater1 = false;
        if ($floatWidth1 > ($floatMinWidth - 3)) {
            $floatWidthCol1 = $floatWidth1 + 3;
            $boolGreater1 = true;
        }

        $boolGreater2 = false;
        if ($floatWidth2 > ($floatMinWidth - 3)) {
            $floatWidthCol2 = $floatWidth2 + 3;
            $boolGreater2 = true;
        }

        //die('blubb');

        $floatX1 = $floatCol1;
        $floatX2 = $floatX1 + $floatWidthCol1 + $floatSpaceWidth;
        $floatX3 = $floatX2 + $floatWidthCol2 + $floatSpaceWidth;

        if (($floatWidthCol1 + $floatSpaceWidth + $floatWidthCol2 + $floatSpaceWidth) > $floatMaxWidth12) { //UMBRUCH NÖTIG!

            if ($boolGreater1 && $boolGreater2) { //BEIDE größer
                
                $floatWidth1 = $floatMinWidth + 14;
                $floatWidthCol1 = $floatWidth1;

                $floatWidth2 = $floatMinWidth + 14;
                $floatWidthCol2 = $floatWidth2;

            } else {

                if ($boolGreater1) { // 1.Spalte größer

                    if ($floatWidth1 > $floatMinWidth + 28) {
                        $floatWidth1 = $floatMinWidth + 28;
                        $floatWidthCol1 = $floatWidth1;
                    } else {
                        $floatWidth1 = $floatMinWidth;
                        $floatWidthCol1 = $floatWidth1;
                    }


                }
                if ($boolGreater2) { // 2.Spalte größer

                    if ($floatWidth2 > $floatMinWidth + 28) {
                        $floatWidth2 = $floatMinWidth + 28;
                        $floatWidthCol2 = $floatWidth2;
                    } else {
                        $floatWidth2 = $floatMinWidth;
                        $floatWidthCol2 = $floatWidth2;
                    }

                }
            }

            $floatX1 = $floatCol1;
            $floatX2 = $floatX1 + $floatWidthCol1 + $floatSpaceWidth;
            $floatX3 = $floatX2 + $floatWidthCol2 + $floatSpaceWidth;

        }

        if ($strNameAdd == '')  {
            $strNamePp = utf8_decode($arrSql1[0]['Name']);
        } else {
            $strNamePp = str_replace('GmbH & Co. KG', $strNameAdd, utf8_decode($arrSql1[0]['Name']));
        }

        if ($arrSql2[0]['SF42_Account_name_short__c'] != '') {
            $strBGName = $arrSql2[0]['SF42_Account_name_short__c'];
        } else {
            $strBGName = $arrSql2[0]['Name'];
        }

        //Höhe berechnen
        $floatHeight1 = $objPdf->GetMultiCellHeight($floatWidthCol1, 6, $strNamePp, 0, 'L');
        $floatHeight2 = $objPdf->GetMultiCellHeight($floatWidthCol2, 6, utf8_decode($strBGName), 0, 'L');
        $floatHeight3 = $objPdf->GetMultiCellHeight($floatWidthCol3, 6, utf8_decode('Direkte Auskunft an IZS'), 0, 'L');

        //Maximalhöhe
        $floatMaxHeight = max($floatHeight1, $floatHeight2, $floatHeight3);



        $intPlus = 0;

        $floatX = $floatX1;
        $floatY = 98 + $intPlus;
        $objPdf->SetTextColor(96, 94, 85); //#605e55
        $objPdf->SetFont('Lato', '', 9);
        $objPdf->Text($floatX, $floatY, utf8_decode('PERSONALDIENSTLEISTER'));

        $floatX = $floatX1 - 1;
        $floatY = 102.5 + $intPlus;
        $objPdf->SetFont('Lato', 'B', 14);
        $objPdf->SetXY($floatX, $floatY);
        $objPdf->MultiCell($floatWidthCol1, 6, str_replace('GmbH & Co. KG', 'GmbH' .chr(160) .'&' .chr(160) .'Co.' .chr(160) .'KG', utf8_decode($arrSql1[0]['Name'])), 0, 'L', $boolFill);
        
        $objPdf->SetFont('Lato', '', 8);

        $strNumberName = 'Unternehmensnummer';
        $strNumberValue = $arrSql[0]['be_unternehmensnummer'];

        if (($arrSql[0]['be_unternehmensnummer'] == '') && ($arrSql[0]['be_mitgliedsnummer'] != '')) {
            $strNumberName = 'Mitgliedsnummer';
            $strNumberValue = $arrSql[0]['be_mitgliedsnummer'];
        }

        $strText = str_replace(chr(10), '', $arrSql1[0]['BillingStreet']) .chr(10) .$arrSql1[0]['BillingPostalCode'] .' ' .$arrSql1[0]['BillingCity'] .chr(10) .$strNumberName .': ' .chr(10) .$strNumberValue .chr(10) .'Registriert seit: ' .$strRegistriert;

        $floatX = $floatX1 - 1;
        $floatY = $floatY + $floatMaxHeight + 2;
        $objPdf->SetXY($floatX, $floatY);
        $objPdf->SetFont('Lato', '', 8);
        $objPdf->MultiCell($floatWidthCol1, 4, utf8_decode($strText), 0, 'L');

        
        $intPlus = 0;

        $floatX = $floatX2;
        $floatY = 98 + $intPlus;
        $objPdf->SetTextColor(96, 94, 85); //#605e55
        $objPdf->SetFont('Lato', '', 9);
        $objPdf->Text($floatX, $floatY, utf8_decode('BERUFSGENOSSENSCHAFT'));

        if ($arrSql2[0]['SF42_Account_name_short__c'] != '') {
            $strBGName = $arrSql2[0]['SF42_Account_name_short__c'];
        } else {
            $strBGName = $arrSql2[0]['Name'];
        }

        $floatX = $floatX2 - 1;
        $floatY = 102.5 + $intPlus;
        $objPdf->SetXY($floatX, $floatY);
        $objPdf->SetFont('Lato', 'B', 14);
        $objPdf->MultiCell($floatWidthCol2, 6, utf8_decode($strBGName), 0, 'L', $boolFill);

        $objPdf->SetFont('Lato', '', 8);
        $strText = $arrSql2[0]['Anfrage_Stra_e__c'] .chr(10) .$arrSql2[0]['Anfrage_PLZ__c'] .' ' .$arrSql2[0]['Anfrage_Ort__c']; // .chr(10) .'Betriebsnummer: ' .$arrSql2[0]['SF42_Comany_ID__c'];
        
        $floatX = $floatX2 - 1;
        $floatY = $floatY + $floatMaxHeight + 2;
        $objPdf->SetXY($floatX, $floatY);
        $objPdf->SetFont('Lato', '', 8);
        $objPdf->MultiCell($floatWidthCol2, 4, utf8_decode($strText), 0, 'L');


        ///*
        $intPlus = 0;

        $floatX = $floatX3;
        $floatY = 98 + $intPlus;
        $objPdf->SetTextColor(96, 94, 85); //#605e55
        $objPdf->SetFont('Lato', '', 9);
        $objPdf->Text($floatX, $floatY, utf8_decode('ART DER MITTEILUNG'));

        $floatX = $floatX3 - 1;
        $floatY = 102.5 + $intPlus;
        $objPdf->SetXY($floatX, $floatY);
        $objPdf->SetFont('Lato', 'B', 14);
        $objPdf->MultiCell($floatWidthCol3, 6, utf8_decode('Direkte Auskunft an IZS'), 0, 'L', $boolFill);

        $objPdf->SetFont('Lato', '', 8);

        $strDueName = 'Beitragsfälligkeit';
        $strDueValue = date('m/Y', strtotime($arrSql[0]['be_beitragsfaelligkeit']));

        if ($arrSql[0]['be_beitragsfaelligkeit'] == '0000-00-00') {
            $strDueName = 'Beitragsjahr';
            $strDueValue = $arrSql[0]['be_jahr'];
        } else {

        }

        $strText = '';
        if ($arrSql[0]['be_datum_rueckmeldung'] != '0000-00-00') {
            $strText.= 'Auskunft erteilt am: ' .date('d.m.Y', strtotime($arrSql[0]['be_datum_rueckmeldung'])) .chr(10);
        }
        $strText.= $strDueName .': ' .$strDueValue; // .str_pad($arrSql[0]['SF42_Month__c'], 2, '0', STR_PAD_LEFT) .'/' .$arrSql[0]['SF42_Year__c'];

        $floatX = $floatX3 - 1;
        $floatY = $floatY + $floatMaxHeight + 2;
        $objPdf->SetXY($floatX, $floatY);
        $objPdf->SetFont('Lato', '', 8);
        $objPdf->MultiCell($floatWidthCol3, 4, utf8_decode($strText), 0, 'L');
        //*/


        $objPdf->SetXY(0, 148);
        $objPdf->SetFont('Lato', 'B', 10);
        $objPdf->MultiCell(70, 4, utf8_decode('Integrierter Haftungsschutz*'), 0, 'C');

        $objPdf->SetXY(0, 156);
        $objPdf->SetFont('Lato', '', 9);
        $objPdf->MultiCell(70, 4, utf8_decode('bis zu'), 0, 'C');

        $objPdf->SetXY(0, 163);
        $objPdf->SetFont('Lato', 'B', 24);
        $objPdf->MultiCell(70, 4, utf8_decode('5.000 ') .chr(128), 0, 'C');

        $objPdf->Image( __DIR__ .'/img/button.png', $floatLeft, 173, 47.8, 10.2);

        $objPdf->SetXY(0, 176);
        $objPdf->SetTextColor(255, 255, 255); //#ffffff
        $objPdf->SetFont('Lato', 'B', 11);
        $objPdf->MultiCell(70, 4, utf8_decode('Kostenlos beantragen'), 0, 'C');

        /*
        $strUrl = 'https://' .$strHost .'.izs.de/' .strtolower($arrSql[0]['Name']);
        $strHtml = '<a href="' .$strUrl .'">' .$strUrl .'</a>';

        $objPdf->setLinkColor(240, 102, 0); //#f06600
        $objPdf->WriteHTML(utf8_decode($strHtml));
        */

        $objPdf->Link($floatLeft, 173, 47.8, 10.2, 'https://antrag.izs.de/haftungsschutz');

        $strHtml = '<a href="https://antrag.izs.de/haftungsschutz">antrag.izs.de/haftungsschutz</a>';
        
        $intPlus = 1;

        $objPdf->SetXY($floatLeft + $intPlus, 186);

        $objPdf->SetFont('Lato', '', 8);
        $objPdf->setLinkColor(240, 102, 0); //#f06600
        $objPdf->WriteHTML(utf8_decode($strHtml));


        $strHtml1 = '<b>Bagatellfall-Regelung:</b> Rückstände auf dem Beitragskonto von bis zu 500 ';
        $strHtml2 = ' werden nicht als Rückstand gewertet.';

        $objPdf->SetXY($floatCol1, 160);

        $objPdf->SetFont('Lato', '', 8);
        $objPdf->SetTextColor(96, 94, 85); //#605e55
        $objPdf->setLinkColor(96, 94, 85); //#605e55
        $objPdf->WriteHTML(utf8_decode($strHtml1) .chr(128) .utf8_decode($strHtml2));

        $strHtml = '<strong>* Integrierter Haftungsschutz</strong><br>';
        $strHtml.= 'IZS steht gegenüber berechtigten Entleihunternehmen für die inhaltlich und technisch korrekte Verarbeitung von Auskünften von Berufsgenossenschaften über den Stand von Beitragskonten <strong>bis zu einem Betrag in Höhe von EUR 5.000,00 je Zertifikat und Subsidiär-Schadensfall</strong> ein.<br>';
        $strHtml.= 'Einzelheiten zur Anspruchsberechtigung entnehmen Sie bitte unseren <a href="https://www.izs-institut.de/agb/">AGB</a>.';
        $strHtml1 = '<br>Voraussetzung für die Nutzung des Haftungsschutzes ist eine Online-Registrierung, die Sie <a href="https://antrag.izs.de/haftungsschutz">hier</a>';
        $strHtml2 = ' bequem in nur einer Minute durchführen können, sowie die Annahmeerklärung von IZS.';

        $objPdf->SetXY($floatCol1, 167);
        $objPdf->SetMargins($floatCol1, 10, 10.8);

        $objPdf->SetFont('Lato', '', 8);
        $objPdf->SetTextColor(96, 94, 85); //#605e55
        $objPdf->WriteHTML(utf8_decode($strHtml));
        $objPdf->SetFont('Lato', '', 8);
        $objPdf->SetTextColor(96, 94, 85); //#605e55
        $objPdf->WriteHTML(utf8_decode($strHtml1));
        $objPdf->SetTextColor(96, 94, 85); //#605e55
        $objPdf->WriteHTML(utf8_decode($strHtml2));

        
        $strHtml = 'IZS Institut für Zahlungssicherheit GmbH  ' .chr(127) .'  Würmtalstraße 20a | 81375 München  ' .chr(127) .'  Telefon: +49 (0) 89 122 237 77 0  ' .chr(127) .'  E-Mail: <a href="mailto:zertifikate@izs-institut.de">zertifikate@izs-institut.de</a>';
        
        $floatX = 38.5;
        $objPdf->SetXY($floatX, 204.5);
        $objPdf->SetMargins($floatX, 10, 10.8);

        $objPdf->SetFont('Lato', '', 9);
        $objPdf->SetTextColor(255, 255, 255); //#ffffff 
        $objPdf->setLinkColor(255, 255, 255, ''); //#ffffff 

        $objPdf->WriteHTML(utf8_decode($strHtml));

        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " Process PDF: $intDurationTime Sek." .chr(10);

        if (($strReturnType == 'string') && ($boolStandAlone === true)) {
            return $objPdf->Output('S'); //Output($strFileName, 'F')
        }


    }

}

if ($boolStandAlone === true) {
    $strOutput = createCert($intEventId);
}

?>