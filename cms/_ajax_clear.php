<?php

session_start();

require_once('../assets/classes/class.mysql.php');
require_once('../assets/classes/class.database.php');

$strReturnA = '';
$strReturnC = '';

if (isset($_REQUEST['search1']) && ($_REQUEST['search1'] != '')) {
  
  $arrPeriod = explode('_', $_REQUEST['search1']);
  
  $strSql2 = 'SELECT `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c`, `SF42_IZSEvent__c`.`SF42_informationProvider__c`, ';
  $strSql2.= '`Acc`.`Id`, `Account`.`Dezentrale_Anfrage__c`, `Account`.`Name`, `Group__c`.`Id` AS `IdG`, `Group__c`.`Name` AS `NameG` ';
  $strSql2.= 'FROM `SF42_IZSEvent__c` ';
  $strSql2.= 'INNER JOIN `Account` AS `Acc` ON `Acc`.`SF42_Comany_ID__c` = `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c` ';
  $strSql2.= 'INNER JOIN `Group__c` ON `Group__c`.`Id` = `Acc`.`SF42_Company_Group__c` ';
  $strSql2.= 'INNER JOIN `Account` ON `Account`.`Id` = `SF42_IZSEvent__c`.`SF42_informationProvider__c` ';
  $strSql2.= 'WHERE `SF42_Month__c` = "' .$arrPeriod[0] .'" AND `SF42_Year__c` = "' .$arrPeriod[1] .'" AND `Status_Klaerung__c` IN ("in Klärung", "geschlossen / positiv", "geschlossen / negativ") ';
  $arrResult2 = MySQLStatic::Query($strSql2);

  $strReturnA.= '<option value="all">Alle Anfragestellen</option>' .chr(10);
  $strReturnC.= '<option value="all">Alle Company Groups</option>' .chr(10);
  
  if (count($arrResult2) > 0) {
    
    $arrOptionA = array();
    $arrOptionC = array();
    
    $arrSel = explode('_', $_REQUEST['search2']);
    $strSel = $arrSel[0];
    
    foreach ($arrResult2 as &$arrEvent) {
      
      $boolSecond = false;
      
      if ($arrEvent['Dezentrale_Anfrage__c'] == 'true') { //DEZENTRAL
      
        $strSql4 = 'SELECT `Anfragestelle__c`  FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrEvent['Id'] .'" AND `Information_Provider__c` = "' .$arrEvent['SF42_informationProvider__c'] .'"';
        $arrResult4 = MySQLStatic::Query($strSql4);
        
        if (count($arrResult4) > 0) {
        
          $strSql5 = 'SELECT `Id`, `Name` FROM `Anfragestelle__c` WHERE `Id` = "' .$arrResult4[0]['Anfragestelle__c'] .'"';
          $arrResult5 = MySQLStatic::Query($strSql5);
          
          $arrOptionA[$arrResult5[0]['Id'] .'_c'] = $arrResult5[0]['Name'];
          
          if ($arrResult5[0]['Id'] == $strSel) {
            $boolSecond = true;
          }
        
        } else { //CATCH ALL
        
          $strSql5b = 'SELECT `Id`, `Name` FROM `Anfragestelle__c` WHERE `Information_Provider__c` = "' .$arrEvent['SF42_informationProvider__c'] .'" AND `CATCH_ALL__c` = "true"';
          $arrResult5b = MySQLStatic::Query($strSql5b);
          
          $arrOptionA[$arrResult5b[0]['Id'] .'_c'] = $arrResult5b[0]['Name'];
          
          if ($arrResult5b[0]['Id'] == $strSel) {
            $boolSecond = true;
          }
          
        }
      
      } else { // ZENTRAL
        
        $arrOptionA[$arrEvent['SF42_informationProvider__c'] .'_z'] = $arrEvent['Name'];

        if ($arrEvent['SF42_informationProvider__c'] == $strSel) {
          $boolSecond = true;
        }
        
      }
      
      //if (($_REQUEST['search2'] == 'all') || ($_REQUEST['search2'] == '')) {
        $boolSecond = true;
      //}
      if ($boolSecond) {
        $arrOptionC[$arrEvent['IdG']] = $arrEvent['NameG'];
      }
      
    } 
    
    natcasesort($arrOptionA);  
    foreach ($arrOptionA as $strValue => $strName) {
      if ($strValue == $_REQUEST['search2']) {
        $strSelected = ' selected="selected"';
        $strSelectA  = $strValue;
      } else {
        $strSelected = '';
      }
      $strReturnA.= '<option value="' .$strValue .'"' .$strSelected.'>' .$strName .'</option>' .chr(10);
    }
    
    natcasesort($arrOptionC);  
    foreach ($arrOptionC as $strValue => $strName) {
      if ($strValue == $_REQUEST['search3']) {
        $strSelected = ' selected="selected"';
        $strSelectC  = $strValue;
      } else {
        $strSelected = '';
      }
      $strReturnC.= '<option value="' .$strValue .'"' .$strSelected.'>' .$strName .'</option>' .chr(10);
    }
    
  }
  
  
}

echo $strReturnA .'|' .$strReturnC.'|' .$strSelectA .'|' .$strSelectC;


?>