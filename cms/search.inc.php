<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

include('conf/global.inc.php');
require_once('../assets/classes/class.mysql.php');
require_once('../assets/classes/class.database.php');

require_once('functions.inc.php');
require_once('bootstrap.inc.php');

require_once('classes/class.informationprovider.php');
require_once('classes/class.premiumpayer.php');
require_once('classes/class.anfragestelle.php');
require_once('classes/class.contact.php');

$objPremiumPayer = new PremiumPayer;
$objAnfragestelle = new Anfragestelle;
$objContact = new Contact;

$arrAllPpList = array();
$arrAllPpListRaw = $objPremiumPayer->arrGetPremiumPayerList();
if (count($arrAllPpListRaw) > 0) {
  foreach ($arrAllPpListRaw as $intKey => $arrPp) {
    $arrAllPpList[$arrPp['Btnr']] = $arrPp['Name'];
  }
}



function arrGetInformationProvider ($strIp = '', $strPp = '') {
  
  $arrReturn = array();

  $strSql = 'SELECT `Name`, `Abweichender_Anfrageprozess__c` FROM `Account` WHERE `Id` = "' .$strIp .'"';
  $resSql = MySQLStatic::Query($strSql);
  
  $arrReturn['IsFallback'] = false; 
  
  if ($resSql[0]['Abweichender_Anfrageprozess__c'] == 'Dezentrale Anfrage') { // IP hat Abweichenden Prozess = Dezentral
    
    $strSql2 = 'SELECT `Anfragestelle__c`.`Name` AS `Name` FROM `Anfragestelle__c` INNER JOIN `Dezentrale_Anfrage_KK__c` ON `Anfragestelle__c`.`Id` = `Dezentrale_Anfrage_KK__c`.`Anfragestelle__c` WHERE `Dezentrale_Anfrage_KK__c`.`Premium_Payer__c` = "' .$strPp .'" AND `Dezentrale_Anfrage_KK__c`.`Information_Provider__c` = "' .$strIp .'"';
    $resSql2 = MySQLStatic::Query($strSql2);
    
    if (count($resSql2) > 0) { //Gibt es eine Anfragestelle mit IP / PP?
      $arrReturn['Name'] = $resSql2[0]['Name'];
    } else {
      $strSql3 = 'SELECT `Name` FROM `Anfragestelle__c` WHERE `CATCH_ALL__c` = "true" AND `Information_Provider__c` = "' .$strIp .'"';
      $resSql3 = MySQLStatic::Query($strSql3);
      $arrReturn['Name'] = $resSql3[0]['Name'];
      $arrReturn['IsFallback'] = true;
    }
    
  } else {
    $arrReturn['Name'] = $resSql[0]['Name'];
  }

  return $arrReturn;

}


function replace_placeholder($strText = '', $intId) {
  global $arrPlaceholder;
  
  $strReturn = $strText;
  
  $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$_REQUEST['det'] .'"';
  $event  = MySQLStatic::Query($strSql);

  $strSql = 'SELECT * FROM `Account` WHERE `Id` = "' .$event[0]['SF42_informationProvider__c'] .'"';
  $Ip  = MySQLStatic::Query($strSql); // == KK

  $strSql = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$event[0]['Betriebsnummer_ZA__c'] .'"';
  $Pp  = MySQLStatic::Query($strSql);

  $strReturn = str_replace('{Month}', $event[0]['SF42_Month__c'], $strReturn);  
  $strReturn = str_replace('{Year}', $event[0]['SF42_Year__c'], $strReturn);  
  
  $arrDate = explode('-', $event[0]['bis_am__c']);
  $strDate = $arrDate[2] .'.' .$arrDate[1] .'.' .$arrDate[0];

  //$strReturn = str_replace('{Salutation}', $Ip[0]['Anrede_Anschreiben__c'] .' ' .$Ip[0]['Nachname__c'], $strReturn);  
  $strReturn = str_replace('{Beitragszahler}', $Pp[0]['Name'], $strReturn);  
  $strReturn = str_replace('{BNR Beitragszahler}', $Pp[0]['SF42_Comany_ID__c'], $strReturn);  
  $strReturn = str_replace('{Krankenkasse}', $Ip[0]['Name'], $strReturn);  
  $strReturn = str_replace('{Sachbearbeiter KK}', $Ip[0]['Anrede_Briefkopf__c'] .' ' .$Ip[0]['Nachname__c'], $strReturn);  
  $strReturn = str_replace('{Sachbearbeiter KK Telefon}', $Ip[0]['Phone'], $strReturn);  
  $strReturn = str_replace('{Erledigung bis}', $strDate, $strReturn);  
  $strReturn = str_replace('{Event Id}', $event[0]['Name'], $strReturn);  
  
  $strReturn = str_replace('{AP Klärung}', $event[0]['AP_Klaerung'], $strReturn);  
  $strReturn = str_replace('{Tel Klärung}', $event[0]['Tel_Klaerung'], $strReturn);  
  $strReturn = str_replace('{Grund}', $event[0]['Grund__c'], $strReturn);  

  //$strReturn = str_replace('{}', [0][''], $strReturn);  
  
  $strReturn = str_replace(array("\n", "\r"), '', $strReturn);
  
  return $strReturn;
}

$boolShowIp = false;
$strOutput = '';

$strOutput.= '

<style>
.m-select-d-box__list-item_selected:before {
  content: \'\' !important;
}
.label {
    color: black !important;
    font-size: 100% !important;
    padding-left: 3px !important;
}
</style>
';

if ($_REQUEST['conly'] == 1) {
  $strSql = 'SELECT `Name` FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$_REQUEST['strSelPay'] .'"';
  $resSql = MySQLStatic::Query($strSql);
  $strOutput.= '<a href="#" onclick="wclose(); return false;" class="tclose">Tab schließen</a>';
  $strOutput.= '<h1>Event Suche (' .$resSql[0]['Name'] .')</h1>' .chr(10);
  $strPageTitle = 'Events :: ' .$resSql[0]['Name'];
} elseif ($_REQUEST['det'] != '') {
  $strSql = 'SELECT `Name` FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$_REQUEST['det'] .'"';
  $resSql = MySQLStatic::Query($strSql);
  $strOutput.= '<a href="#" onclick="wclose(); return false;" class="tclose">Tab schließen</a>';
  $strOutput.= '<h1>Event Detail (' .$resSql[0]['Name'] .')</h1>' .chr(10);
  $strPageTitle = 'Events :: ' .$resSql[0]['Name'];
} else {
  $strOutput.= '<h1>Event Suche</h1>' .chr(10);
}

$arrSystemStream = array (
  'SF42_OnlineStatus__c' => 'änderte Online Status', 
  'Info__c' => 'änderte globale Status-Info', 
  'bis_am__c' => 'änderte Wiedervorlage-Datum', 
  'RecordTypeId' => 'änderte Phase', 
  'SF42_EventStatus__c' => 'änderte Ergebnis', 
  'Stundung__c' => 'änderte Stundung / Ratenzahlung', 
  'SF42_PublishingStatus__c' => 'änderte Sichtbarkeit des Dokuments auf dem Portal', 
  'Art_des_Dokuments__c' => 'änderte Art des Dokuments', 
  'Auskunft_von__c' => 'änderte Auskunftsgeber', 
  'Sperrvermerk__c' => 'änderte Sperrvermerk-Status', 
  'SF42_EventComment__c' => 'änderte den auf dem Portal sichtbaren Kommentar', 
  'SF42_DocumentUrl__c' => 'änderte das Dokument',
  'Rueckmeldung_am__c' => 'änderte Rückmelde-Datum', 
  'Art_der_Rueckmeldung__c' => 'änderte Art der Rückmeldung', 
  'Status_Klaerung__c' => 'änderte den Klärungs-Status', 
  'Grund__c' => 'änderte den Grund für die Klärung', 
  'Beitragsrueckstand__c' => 'änderte die Höhe des Beitragsrückstands', 
  'Naechster_Meilenstein__c' => 'änderte die Information zum nächsten Schritt', 
  'AP_Klaerung' => 'änderte den Ansprechparter für die Klärung', 
  'Tel_Klaerung' => 'änderte die Telefonnummer für die Klärung'
); 

$arrRequestWay = array();
$arrDecentral = array();

$arrRecordType = array (
  'Delivery' => '01230000001Ao73AAC',
  'FollowUp' => '01230000001Ao74AAC',
  'Publication' => '01230000001Ao75AAC',
  'Review' => '01230000001Ao76AAC'
);

$arrRecordTypeFlip = array_flip($arrRecordType);

$arrDokArt = array (
  'Dynamische Erzeugung' => 'Digitale Auskunft',
  'Kontoauskunft' => 'Form',
  'Kontoauszug' => 'KtoA',
  'Unbedenklichkeitsbescheinigung' => 'UBB',
  'Sonstiges' => 'Sonst.'
);

$arrStundung = array(
  'Stundung' => 'Stundung',
	'Ratenzahlung' => 'Ratenzahlung',
	'Stundung oder Ratenzahlung' => 'Stundung oder Ratenzahlung'
);

/*
$arrChangePreview = array ( 
  'abgelehnt / Dublette' => 'abgelehnt / Dublette', 
  'abgelehnt / Frist' => 'abgelehnt / Frist', 
  'accepted' => 'angenommen', 
  'bereit für REVIEW' => 'bereit für REVIEW', 
  'enquired' => 'angefragt', 
  'in progress' => 'in Bearbeitung', 
  'new' => 'neu', 
  'no Feedback' => 'keine Antwort', 
  'no result' => 'NO RESULT', 
  'not assignable' => 'nicht zuordenbar', 
  'not OK' => 'NOT OK', 
  'OK' => 'OK', 
  'refused' => 'abgelehnt', 
  'to clear' => 'zu klären', 
  'to enquire' => 'anzufragen', 
  'zugeordnet / abgelegt' => 'zugeordnet / abgelegt', 
  'zurückgestellt von IZS' => 'zurückgestellt von IZS'
);
*/

$arrOnline = array (
  'true' => 'sichtbar',
  'false' => 'nicht sichtbar'
);

$arrSperr = array (
  'true' => 'ja',
  'false' => 'nein'
);

$arrSichtbar = array (
  'online' => 'ja',
  'private' => 'nein',
  'ready' => 'auf Anfrage'  
);

$arrAuskunft = array (
  'Krankenkasse' => 'Krankenkasse',
  'Zeitarbeitsunternehmen' => 'Zeitarbeitsunternehmen',
);

$arrRueck = array (
  'Email' => 'Email',
  'Fax' => 'Fax',
  'Post' => 'Post',
  'Telefon' => 'Telefon'
);

$arrStatusKlaerung = array (
  'in Klärung' => 'in Klärung',
  'geschlossen / positiv' => 'geschlossen / positiv',
  'geschlossen / negativ' => 'geschlossen / negativ'  
);


if (isset($_REQUEST['det']) && ($_REQUEST['det'] != '')) {
  
  if (isset($_REQUEST['save']) && ($_REQUEST['save'] == 1)) { //"", datum

    
    if ($_REQUEST['bis_am__c'] != '') {
      $arrDate = explode('.', $_REQUEST['bis_am__c']);
      $_REQUEST['bis_am__c'] = $arrDate[2] .'-' .$arrDate[1] .'-' .$arrDate[0];
    } else {
      $_REQUEST['bis_am__c'] = '0000-00-00';
    }
    
    if ($_REQUEST['Rueckmeldung_am__c'] != '') {
      $arrDate = explode('.', $_REQUEST['Rueckmeldung_am__c']);
      $_REQUEST['Rueckmeldung_am__c'] = $arrDate[2] .'-' .$arrDate[1] .'-' .$arrDate[0];
    } else {
      $_REQUEST['Rueckmeldung_am__c'] = '0000-00-00';
    }
    
    $_REQUEST['Naechster_Meilenstein__c'] = str_replace("'", '"', $_REQUEST['Naechster_Meilenstein__c']);
    
    if ($_REQUEST['deletelink'] == 1) {

      if ($_REQUEST['SF42_DocumentUrl__c'] != '') {
        $arrPart = explode('/', $_REQUEST['SF42_DocumentUrl__c']);
        $strFile = 'server/php/files/' .$arrPart[count($arrPart) - 1];
        if (file_exists($strFile)) {
          unlink($strFile);
        }
      }

      $_REQUEST['SF42_DocumentUrl__c'] = '';
    }
    
    $strSql = "    
SELECT `RecordTypeId`, `SF42_EventStatus__c`, `SF42_DocumentUrl__c`, `SF42_PublishingStatus__c`, 
`Art_des_Dokuments__c`, `Auskunft_von__c`, `Sperrvermerk__c`, `SF42_EventComment__c`, 
`Rueckmeldung_am__c`, `Art_der_Rueckmeldung__c`, `Status_Klaerung__c`, `Grund__c`, `Beitragsrueckstand__c`, 
`Naechster_Meilenstein__c`, `Info__c`, `bis_am__c`, `Name`, `SF42_DocumentUrl__c`, `SF42_OnlineStatus__c`, 
`AP_Klaerung`, `Tel_Klaerung`, `Stundung__c`   
FROM `SF42_IZSEvent__c` 
WHERE `evid` = '" .$_REQUEST['det'] ."'";
    $result = MySQLStatic::Query($strSql);
    
    $strEventName = $result[0]['Name'];
    
    foreach ($result[0] as $strKey => $strValue) {
      if (($strKey != 'Name') && ($_REQUEST[$strKey] != $strValue)) {
        
        $strSql2 = 'INSERT INTO `izs_event_change` 
         (`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (
         NULL, "' .$_REQUEST['det'] .'", ' .$_SESSION['id'] .', "' .$result[0]['Name'] .'", "' .$strKey .'", "' .str_replace('"', '\\"', $strValue) .'", "' .str_replace('"', '\\"', $_REQUEST[$strKey]) .'", NOW(), 2)';
        $result2 = MySQLStatic::Query($strSql2);
        
      }
    }
    
    if (strstr($_REQUEST['Status_Klaerung__c'], 'geschlossen') !== false) {
      $_REQUEST['Naechster_Meilenstein__c'] = '';
    }
  
    $strSql3 = "
UPDATE `SF42_IZSEvent__c` SET 
`RecordTypeId` = '" .$_REQUEST['RecordTypeId'] ."', `SF42_EventStatus__c` = '" .$_REQUEST['SF42_EventStatus__c'] ."', 
`SF42_DocumentUrl__c` = '" .$_REQUEST['SF42_DocumentUrl__c'] ."', `Info__c` = '" .str_replace("'", "\\'", $_REQUEST['Info__c']) ."', 
`SF42_PublishingStatus__c` = '" .$_REQUEST['SF42_PublishingStatus__c'] ."', `Art_des_Dokuments__c` = '" .$_REQUEST['Art_des_Dokuments__c'] ."', 
`Auskunft_von__c` = '" .$_REQUEST['Auskunft_von__c'] ."', `Sperrvermerk__c` = '" .$_REQUEST['Sperrvermerk__c'] ."', 
`SF42_EventComment__c` = '" .str_replace("'", "\\'", $_REQUEST['SF42_EventComment__c']) ."', `Rueckmeldung_am__c` = '" .$_REQUEST['Rueckmeldung_am__c'] ."', 
`Art_der_Rueckmeldung__c` = '" .$_REQUEST['Art_der_Rueckmeldung__c'] ."', `Status_Klaerung__c` = '" .$_REQUEST['Status_Klaerung__c'] ."', 
`Grund__c` = '" .$_REQUEST['Grund__c'] ."', `Beitragsrueckstand__c` = '" .$_REQUEST['Beitragsrueckstand__c'] ."', `SF42_OnlineStatus__c` = '" .$_REQUEST['SF42_OnlineStatus__c'] ."',  
`Naechster_Meilenstein__c`  = '" .str_replace("'", "\\'", $_REQUEST['Naechster_Meilenstein__c']) ."', `bis_am__c` = '" .$_REQUEST['bis_am__c'] ."', 
`AP_Klaerung` = '" .str_replace("'", "\\'", $_REQUEST['AP_Klaerung']) ."', `Tel_Klaerung` = '" .str_replace("'", "\\'", $_REQUEST['Tel_Klaerung']) ."', 
`Stundung__c` = '" .str_replace("'", "\\'", $_REQUEST['Stundung__c']) ."'  
WHERE `evid` = '" .$_REQUEST['det'] ."'";

      $result = MySQLStatic::Query($strSql3);
    
  }
  

  $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$_REQUEST['det'] .'"';
  $resSql = MySQLStatic::Query($strSql);
  
  $strEventName = $resSql[0]['Name'];
  
  $strSql2 = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$resSql[0]['Betriebsnummer_ZA__c'] .'"';
  $resSql2 = MySQLStatic::Query($strSql2);
  
  $strSql3 = 'SELECT * FROM `Account` WHERE `Id` = "' .$resSql[0]['SF42_informationProvider__c'] .'"';
  $resSql3 = MySQLStatic::Query($strSql3);
  
  $strContactIp = $resSql[0]['SF42_informationProvider__c'];
  $strContactPp = $resSql2[0]['Id'];
  $strCountactGroup = $resSql2[0]['SF42_Company_Group__c'];
  
  if ($resSql[0]['bis_am__c'] != '0000-00-00') {
    $arrDate = explode('-', $resSql[0]['bis_am__c']);
    $resSql[0]['bis_am__c'] = $arrDate[2] .'.' .$arrDate[1] .'.' .$arrDate[0];
  } else {
    $resSql[0]['bis_am__c'] = '';
  }
  
  if ($resSql[0]['Rueckmeldung_am__c'] != '0000-00-00') {
    $arrDate = explode('-', $resSql[0]['Rueckmeldung_am__c']);
    $resSql[0]['Rueckmeldung_am__c'] = $arrDate[2] .'.' .$arrDate[1] .'.' .$arrDate[0];
  } else {
    $resSql[0]['Rueckmeldung_am__c'] = '';
  }  

/*

<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
	<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#tabs-1">Basis Daten</a></li>
		<li class="ui-state-default ui-corner-top"><a href="#tabs-2">Activity Stream</a></li>
	</ul>
	<div id="tabs-1" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
	
*/

$strClassTabs = 'ui-tabs ui-widget ui-widget-content ui-corner-all';
$strClassTabsUl = 'ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all';
$strUrlT1 = '/cms/index.php?ac=sear&det=' .$_REQUEST['det'] .'&tabs=1';
$strUrlT2 = '/cms/index.php?ac=sear&det=' .$_REQUEST['det'] .'&tabs=2';
$strUrlT3 = 'index_neu.php?ac=contacts&acid=' .$resSql3[0]['Id'] .'';
$strUrlT4 = '/cms/index.php?ac=sear&det=' .$_REQUEST['det'] .'&tabs=4';


if ((empty($_REQUEST['tabs'])) || ($_REQUEST['tabs'] == 1)) {

  $strClassT1 = 'ui-corner-top ui-tabs-selected ui-state-active';
  $strClassT2 = 'ui-corner-top ui-state-default';
  $strClassT3 = 'ui-corner-top ui-state-default';
  $strClassT4 = 'ui-corner-top ui-state-default';
  $strClassC1 = 'ui-tabs-panel ui-widget-content ui-corner-bottom';
  $strClassC2 = 'ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide';
  $strClassC4 = 'ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide';
} elseif ($_REQUEST['tabs'] == 2) {
  $strClassT1 = 'ui-corner-top ui-state-default';
  $strClassT2 = 'ui-corner-top ui-tabs-selected ui-state-active';
  $strClassT3 = 'ui-corner-top ui-state-default';
  $strClassT4 = 'ui-corner-top ui-state-default';
  $strClassC1 = 'ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide';
  $strClassC2 = 'ui-tabs-panel ui-widget-content ui-corner-bottom';
  $strClassC4 = 'ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide';
} elseif ($_REQUEST['tabs'] == 4) {
  $strClassT1 = 'ui-corner-top ui-state-default';
  $strClassT2 = 'ui-corner-top ui-state-default';
  $strClassT3 = 'ui-corner-top ui-state-default';
  $strClassT4 = 'ui-corner-top ui-tabs-selected ui-state-active';
  $strClassC1 = 'ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide';
  $strClassC2 = 'ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide';
  $strClassC4 = 'ui-tabs-panel ui-widget-content ui-corner-bottom';
}


$strDataName = $strIChatPath .'/SV/' .$resSql[0]['SF42_Year__c'] .'-' .$resSql[0]['SF42_Month__c'] .'/' .$resSql2[0]['Name'] .'/' .$resSql[0]['Name'];
$intDetailId = $resSql[0]['Id'];

  $strOutput.= '
  
<div id="tabs" class="' .$strClassTabs .'" style="float: left;">
	<ul class="' .$strClassTabsUl .'">
		<li class="' .$strClassT1 .'"><a href="' .$strUrlT1 .'">Basis Daten</a></li>
		<li class="' .$strClassT2 .'"><a href="' .$strUrlT2 .'">Activity Stream</a></li>
		<li class="' .$strClassT3 .'"><a href="' .$strUrlT3 .'" target="_blank" title="Zeige Kontakte">Alle IP-Kontakte</a></li>
		<li class="' .$strClassT4 .'"><a href="' .$strUrlT4 .'" title="Chats">Chats<span smartchat data-type="MyCustomIndicatorComponent" data-uri="' .$strIChatUri .'cms/index.php?ac=sear&det=' .$intDetailId .'"></span></a></li>
	</ul>
	<div id="tabs-1" class="' .$strClassC1 .'">
	
';


if ($_REQUEST['edit'] == 1) {

$strOutput.= '
	
      <form action="/cms/index.php?ac=sear&det=' .$_REQUEST['det'] .'" method="post" id="evedit">
      
			  <input type="hidden" name="save" value="1">
			  
			  <div id="eventhead">
  			  <div id="eventcaption" class="clearfix">
  			    <div class="caption">' .$resSql[0]['Name'] .' - ' .$resSql[0]['SF42_Year__c'] .'/' .$resSql[0]['SF42_Month__c'] .' - PP: ' .$resSql2[0]['Name'] .' (' .$resSql[0]['Betriebsnummer_ZA__c'] .') - IP: ' .$resSql3[0]['Name'] .'</div>
  			    <div style="float: right;"><span id="add-mail" class="button">E-Mail</span> <span id="add-note" class="button">Notiz</span></div>
  			  </div>
  			  <div id="eventinfo" class="clearfix">
  			    <div class="form-left">
  			      <label for="Info__c">Status-Info</label> <textarea rows="4" cols="50" name="Info__c">' .$resSql[0]['Info__c'] .'</textarea>
  			    </div>
  			    <div class="form-right">
  			      <label for="bis_am__c">Wiedervorlage</label> <input type="text" id="datepicker" name="bis_am__c" value="' .$resSql[0]['bis_am__c'] .'">
  			    </div>
  			  </div>
        </div>
        
        <h2>Ergebnis</h2>
			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="SF42_OnlineStatus__c">Online Status</label> <select name="SF42_OnlineStatus__c">' .chr(10);

$arrEmpty[''] = '';
$arrOnline = $arrEmpty + $arrOnline;

foreach ($arrOnline as $strKey => $strName) {
  if ($resSql[0]['SF42_OnlineStatus__c'] == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}
$strOutput.= '
			      </select>
			    </div>
			  </div>

			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="RecordTypeId">Phase</label> <select name="RecordTypeId">' .chr(10);

$arrEmpty[''] = '';
$arrRecordType = $arrEmpty + $arrRecordType;

foreach ($arrRecordType as $strName => $strKey) {
  if ($resSql[0]['RecordTypeId'] == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}
$strOutput.= '
			      </select>
			    </div>
			    <div class="form-right">
			      <label for="SF42_EventStatus__c">Ergebnis</label> <select name="SF42_EventStatus__c">' .chr(10);

$arrEmpty[''] = '';
$arrChangePreview = $arrEmpty + $arrChangePreview;

foreach ($arrChangePreview as $strKey => $strName) {
  if ($resSql[0]['SF42_EventStatus__c'] == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}

$strOutput.= '
			      </select> 
			    </div>
			  </div>


			  <div class="eventline clearfix">
        <div class="form-left">&nbsp;
        </div>
        <div class="form-right">
          <label for="Stundung__c">Stundung / Ratenzahlung</label> <select name="Stundung__c">' .chr(10);

$arrEmpty[''] = '';
$arrChangeStundung = $arrEmpty + $arrStundung;

foreach ($arrChangeStundung as $strKey => $strName) {
if ($resSql[0]['Stundung__c'] == $strKey) {
$strSel = ' selected="selected"';
} else {
$strSel = '';
}
$strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}

$strOutput.= '
            </select>
          </div>
			  </div>

			  <div class="eventline clearfix">
			    <div class="form-left">
';

$strSqlD = 'SELECT `Id` FROM `Attachment` WHERE `ParentId` = "' .$resSql[0]['Id'] .'"';
$resSqlD = MySQLStatic::Query($strSqlD);

if (count($resSqlD) > 0) {
  
  $strLink = '';
  foreach ($resSqlD as $intKey => $arrDataset) {
    $strLink.= '<a href="https://na1.salesforce.com/' .$arrDataset['Id'] .'" target="_blank">Link ' .($intKey + 1) .'</a><br />';
  }

} elseif ($resSql[0]['SF42_DocumentUrl__c'] != '') {
  $resSql[0]['SF42_DocumentUrl__c'] = str_replace('www.izs-institut.de', 'www.izs.de', $resSql[0]['SF42_DocumentUrl__c']);
  $resSql[0]['SF42_DocumentUrl__c'] = str_replace('http://izs-institut.de', 'https://www.izs.de', $resSql[0]['SF42_DocumentUrl__c']);
  $strLink = '<a href="' .$resSql[0]['SF42_DocumentUrl__c'] .'" target="_blank">Link</a> <input type="checkbox" name="deletelink" value="1" style="margin-left: 20px;"> Löschen';
} else {
  $strLink = '';
}

$strOutput.= '
			      <label for="SF42_DocumentUrl__c">Dokument URL</label> ' .$strLink .'
			      <input type="hidden" name="SF42_DocumentUrl__c" value="' .$resSql[0]['SF42_DocumentUrl__c'] .'">
			    </div>
			    <div class="form-right">
			      <label for="SF42_PublishingStatus__c">Dokument auf Portal sichtbar</label> <select name="SF42_PublishingStatus__c">' .chr(10);

$arrEmpty[''] = '';
$arrSichtbar = $arrEmpty + $arrSichtbar;

foreach ($arrSichtbar as $strKey => $strName) {
  if ($resSql[0]['SF42_PublishingStatus__c'] == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}
$strOutput.= '
			      </select>			    </div>
			  </div>

			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="Art_des_Dokuments__c">Art des Dokuments</label> <select name="Art_des_Dokuments__c">' .chr(10);

$arrEmpty[''] = '';
$arrDokArt = $arrEmpty + $arrDokArt;

foreach ($arrDokArt as $strKey => $strName) {
  if ($resSql[0]['Art_des_Dokuments__c'] == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strKey .'</option>';
}
$strOutput.= '
			      </select>
			    </div>
			    <div class="form-right">
			      <label for="Auskunft_von__c">Auskunft von</label> <select name="Auskunft_von__c">' .chr(10);

$arrEmpty[''] = '';
$arrAuskunft = $arrEmpty + $arrAuskunft;

foreach ($arrAuskunft as $strName => $strKey) {   
  if ($resSql[0]['Auskunft_von__c'] == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}
$strOutput.= '
			      </select>			    </div>
			  </div>

			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="Sperrvermerk__c">Sperrvermerk</label> <select name="Sperrvermerk__c">' .chr(10);

$arrEmpty[''] = '';
$arrSperr = $arrEmpty + $arrSperr;

foreach ($arrSperr as $strKey => $strName) {
  if ($resSql[0]['Sperrvermerk__c'] == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}
$strOutput.= '
			      </select>
			    </div>
			    <div class="form-right">
			      <label for="SF42_EventComment__c">Kommentar auf Portal</label> <textarea rows="2" cols="45" name="SF42_EventComment__c">' .$resSql[0]['SF42_EventComment__c'] .'</textarea>
			    </div>
			  </div>


			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="Rueckmeldung_am__c">Rückmeldung am</label> <input type="text" id="datepicker2" name="Rueckmeldung_am__c" value="' .$resSql[0]['Rueckmeldung_am__c'] .'">
			    </div>
			    <div class="form-right">
 			      <label for="Art_der_Rueckmeldung__c">Art der Rückmeldung</label>  <select name="Art_der_Rueckmeldung__c">' .chr(10);

$arrEmpty[''] = '';
$arrRueck = $arrEmpty + $arrRueck;

foreach ($arrRueck as $strKey => $strName) {
  if ($resSql[0]['Art_der_Rueckmeldung__c'] == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}
$strOutput.= '
			      </select>
			    </div>
			  </div>
			  
        <h2>Klärungs-Details</h2>

			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="Status_Klaerung__c">Status</label> <select name="Status_Klaerung__c">' .chr(10);

$arrEmpty[''] = '';
$arrStatusKlaerung = $arrEmpty + $arrStatusKlaerung;

foreach ($arrStatusKlaerung as $strKey => $strName) {
  if ($resSql[0]['Status_Klaerung__c'] == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}
$strOutput.= '
			      </select>
			    </div>
			    <div class="form-right">
			      <label for="Grund__c">Grund</label> <select name="Grund__c">' .chr(10);

$arrEmpty[''] = '';
$arrGrund = $arrEmpty + $arrGrund;

foreach ($arrGrund as $strKey => $strName) {
  if ($resSql[0]['Grund__c'] == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}
$strOutput.= '
			      </select>
			    </div>
			  </div>

			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="Beitragsrueckstand__c">Beitragsrückstand in EUR</label> <input id="msdb-b" type="text" name="Beitragsrueckstand__c" value="' .$resSql[0]['Beitragsrueckstand__c'] .'">
			    </div>
			    <div class="form-right">
			      <label for="Naechster_Meilenstein__c">Nächster Schritt</label> <select name="Naechster_Meilenstein__c">' .chr(10);

$arrEmpty[''] = '';
$arrMeilenstein = $arrEmpty + $arrMeilenstein;

foreach ($arrMeilenstein as $strKey => $strName) {
  if (str_replace('"', "'", $resSql[0]['Naechster_Meilenstein__c']) == $strKey) {
    $strSel = ' selected="selected"';
  } else {
    $strSel = '';
  }
  $strOutput.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
}
$strOutput.= '
			      </select>
			    </div>
			  </div>


			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="AP_Klaerung">Ansprechpartner Klärung</label> <textarea rows="2" cols="45" name="AP_Klaerung">' .$resSql[0]['AP_Klaerung'] .'</textarea>
			    </div>
			    <div class="form-right">
			      <label for="Tel_Klaerung">Telefon Klärung</label> <textarea rows="2" cols="45" name="Tel_Klaerung">' .$resSql[0]['Tel_Klaerung'] .'</textarea>
			    </div>
			  </div>


			</form>

      <p><button id="cancel-edit" class="ui-button ui-state-default ui-corner-all">Abbrechen</button>
      <button id="reset-edit" style="float: right;" class="ui-button ui-state-default ui-corner-all">Ergebnis zurücksetzen</button>
      <button id="save-edit" class="ui-button ui-state-default ui-corner-all ui-state-hover">Speichern</button></p>
      
<script>
$("#reset-edit").bind("click", function() {
  $("select[name=\'RecordTypeId\']").val("01230000001Ao76AAC");
  $("input[name=\'deletelink\']").prop("checked", true);;
  $("select[name=\'SF42_EventStatus__c\']").val("enquired");
  $("select[name=\'Stundung__c\']").val("");
})
$("#cancel-edit").bind("click", function() {
  location.href = "/cms/index.php?ac=sear&det=' .$_REQUEST['det'] .'";
})
$("#save-edit").bind("click", function() {
  $("#evedit").submit();
})
</script>

';

} else {

$strOutput.= '
			  <div id="eventhead">
  			  <div id="eventcaption" class="clearfix">
  			    <div class="caption">' .$resSql[0]['Name'] .' - ' .$resSql[0]['SF42_Year__c'] .'/' .$resSql[0]['SF42_Month__c'] .' - PP: ' .$resSql2[0]['Name'] .' (' .$resSql[0]['Betriebsnummer_ZA__c'] .') - IP: ' .$resSql3[0]['Name'] .'</div>
  			    <div style="float: right;"><span id="add-mail" class="button">E-Mail</span> <span id="add-note" class="button">Notiz</span></div>
  			  </div>
  			  <div id="eventinfo" class="clearfix">
  			    <div class="form-left">
  			      <label for="Info__c">Status-Info</label> <span class="field" style="height: 88px;">' .nl2br($resSql[0]['Info__c']) .'</span>
  			    </div>
  			    <div class="form-right">
  			      <label for="bis_am__c">Wiedervorlage</label> <span class="field">' .$resSql[0]['bis_am__c'] .'</span>
  			    </div>
  			  </div>
        </div>
        
        <h2>Ergebnis</h2>
			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="SF42_OnlineStatus__c">Online Status</label> <span class="field">' .chr(10);

$arrEmpty[''] = '';
$arrOnline = $arrEmpty + $arrOnline;

foreach ($arrOnline as $strKey => $strName) {
  if ($resSql[0]['SF42_OnlineStatus__c'] == $strKey) {
    $strOutput.= '' .$strName .'';
  }
}
$strOutput.= '
			      </span>
			    </div>
			  </div>

			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="RecordTypeId">Phase</label> <span class="field">' .chr(10);

$arrEmpty[''] = '';
$arrRecordType = $arrEmpty + $arrRecordType;

foreach ($arrRecordType as $strName => $strKey) {
  if ($resSql[0]['RecordTypeId'] == $strKey) {
    $strOutput.= '' .$strName .'';
  }
}
$strOutput.= '
			      </span>
			    </div>
			    <div class="form-right">
			      <label for="SF42_EventStatus__c">Ergebnis</label> <span class="field">' .chr(10);

$arrEmpty[''] = '';
$arrChangePreview = $arrEmpty + $arrChangePreview;

foreach ($arrChangePreview as $strKey => $strName) {
  if ($resSql[0]['SF42_EventStatus__c'] == $strKey) {
    $strOutput.= '' .$strName .'';
  }
}
$strOutput.= '
			      </span>
			    </div>
			  </div>

			  <div class="eventline clearfix">
        <div class="form-left">&nbsp;
        </div>
        <div class="form-right">
          <label for="Stundung__c">Stundung / Ratenzahlung</label> <span class="field">' .chr(10);

$arrEmpty[''] = '';
$arrChangeStundung = $arrEmpty + $arrStundung;

foreach ($arrChangeStundung as $strKey => $strName) {
  if ($resSql[0]['Stundung__c'] == $strKey) {
    $strOutput.= '' .$strName .'';
  }
}

$strOutput.= '
            </span>
          </div>
			  </div>


			  <div class="eventline clearfix">
			    <div class="form-left">
';


$strSqlD = 'SELECT `Id` FROM `Attachment` WHERE `ParentId` = "' .$resSql[0]['Id'] .'"';
$resSqlD = MySQLStatic::Query($strSqlD);

if (count($resSqlD) > 0) {
  
  $strLink = '';
  foreach ($resSqlD as $intKey => $arrDataset) {
    $strLink.= '<a href="https://na1.salesforce.com/' .$arrDataset['Id'] .'" target="_blank">Link ' .($intKey + 1) .'</a><br />';
  }

} elseif ($resSql[0]['SF42_DocumentUrl__c'] != '') {
  $resSql[0]['SF42_DocumentUrl__c'] = str_replace('www.izs-institut.de', 'www.izs.de', $resSql[0]['SF42_DocumentUrl__c']);
  $resSql[0]['SF42_DocumentUrl__c'] = str_replace('http://izs-institut.de', 'https://www.izs.de', $resSql[0]['SF42_DocumentUrl__c']);
  $strLink = '<a href="' .$resSql[0]['SF42_DocumentUrl__c'] .'" target="_blank">Link</a>';
} else {
  $strLink = '';
}


$strOutput.= '
			      <label for="SF42_DocumentUrl__c">Dokument URL</label> ' .$strLink .'
			    </div>
			    <div class="form-right">
			      <label for="SF42_PublishingStatus__c">Dokument auf Portal sichtbar</label> <span class="field">' .chr(10);

$arrEmpty[''] = '';
$arrSichtbar = $arrEmpty + $arrSichtbar;

foreach ($arrSichtbar as $strKey => $strName) {
  if ($resSql[0]['SF42_PublishingStatus__c'] == $strKey) {
    $strOutput.= '' .$strName .'';
  }
}
$strOutput.= '
			      </span>			    </div>
			  </div>

			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="Art_des_Dokuments__c">Art des Dokuments</label> <span class="field">' .chr(10);

$arrEmpty[''] = '';
$arrDokArt = $arrEmpty + $arrDokArt;

foreach ($arrDokArt as $strKey => $strName) {
  if ($resSql[0]['Art_des_Dokuments__c'] == $strKey) {
    $strOutput.= '' .$strKey .'';
  }
}
$strOutput.= '
			      </span>
			    </div>
			    <div class="form-right">
			      <label for="Auskunft_von__c">Auskunft von</label> <span class="field">' .chr(10);

$arrEmpty[''] = '';
$arrAuskunft = $arrEmpty + $arrAuskunft;

foreach ($arrAuskunft as $strName => $strKey) {   
  if ($resSql[0]['Auskunft_von__c'] == $strKey) {
    $strOutput.= '' .$strName .'';
  }
}
$strOutput.= '
			      </span>			    </div>
			  </div>

			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="Sperrvermerk__c">Sperrvermerk</label> <span class="field">' .chr(10);

$arrEmpty[''] = '';
$arrSperr = $arrEmpty + $arrSperr;

foreach ($arrSperr as $strKey => $strName) {
  if ($resSql[0]['Sperrvermerk__c'] == $strKey) {
    $strOutput.= '' .$strName .'';
  }
}
$strOutput.= '
			      </span>
			    </div>
			    <div class="form-right">
			      <label for="SF42_EventComment__c">Kommentar auf Portal</label> <span class="field" style="height: 52px;">' .$resSql[0]['SF42_EventComment__c'] .'</span>
			    </div>
			  </div>


			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="Rueckmeldung_am__c">Rückmeldung am</label> <span class="field">' .$resSql[0]['Rueckmeldung_am__c'] .'</span>
			    </div>
			    <div class="form-right">
 			      <label for="Art_der_Rueckmeldung__c">Art der Rückmeldung</label>  <span class="field">' .chr(10);

$arrEmpty[''] = '';
$arrRueck = $arrEmpty + $arrRueck;

foreach ($arrRueck as $strKey => $strName) {
  if ($resSql[0]['Art_der_Rueckmeldung__c'] == $strKey) {
    $strOutput.= '' .$strName .'';
  }
}
$strOutput.= '
			      </span>
			    </div>
			  </div>
			  
        <h2>Klärungs-Details</h2>

			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="Status_Klaerung__c">Status</label> <span class="field">' .chr(10);

$arrEmpty[''] = '';
$arrStatusKlaerung = $arrEmpty + $arrStatusKlaerung;

foreach ($arrStatusKlaerung as $strKey => $strName) {
  if ($resSql[0]['Status_Klaerung__c'] == $strKey) {
    $strOutput.= '' .$strName .'';
  }
}
$strOutput.= '
			      </span>
			    </div>
			    <div class="form-right">
			      <label for="Grund__c">Grund</label> <span class="field">' .chr(10);

$arrEmpty[''] = '';
$arrGrund = $arrEmpty + $arrGrund;

foreach ($arrGrund as $strKey => $strName) {
  if ($resSql[0]['Grund__c'] == $strKey) {
    $strOutput.= '' .$strName .'';
  }
}
$strOutput.= '
			      </span>
			    </div>
			  </div>

			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="Beitragsrueckstand__c">Beitragsrückstand in EUR</label> <span class="field">' .$resSql[0]['Beitragsrueckstand__c'] .'</span>
			    </div>
			    <div class="form-right">
			      <label for="Naechster_Meilenstein__c">Nächster Schritt</label> <span class="field">' .chr(10);

    $strOutput.= '' .$resSql[0]['Naechster_Meilenstein__c'] .'';

$strOutput.= '
			      </span>
			    </div>
			  </div>


			  <div class="eventline clearfix">
			    <div class="form-left">
			      <label for="AP_Klaerung">Ansprechpartner Klärung</label> <span class="field" style="height: 52px;">' .$resSql[0]['AP_Klaerung'] .'</span>
			    </div>
			    <div class="form-right">
			      <label for="Tel_Klaerung">Telefon Klärung</label> <span class="field" style="height: 52px;">' .$resSql[0]['Tel_Klaerung'] .'</span>
			    </div>
			  </div>


      <button id="start-edit" class="ui-button ui-state-default ui-corner-all ui-state-hover">Bearbeiten</button>
      
<script>
$("#start-edit").bind("click", function() {
  location.href = "/cms/index.php?ac=sear&det=' .$_REQUEST['det'] .'&edit=1";
})
</script>
';
}

$strOutput.= '			  
        <h2 style="margin-top: 20px">Sachbearbeiter IP</h2>
';

//ALLE
$arrAll = $objContact->arrGetContactListFromIp($resSql3[0]['Id']);

//INDIREKT
$arrPPs = $objPremiumPayer->arrGetPPFromGroup($resSql2[0]['SF42_Company_Group__c']);
$arrAccList = arrExtractArray($arrPPs, 'acid');
$arrIndirect = $objContact->arrGetContactFromAccountList($arrAccList, $resSql3[0]['Id']);

//DIREKT
$arrDirect = $objContact->arrGetContactListFromIpPp($strContactIp, $strContactPp);

$arrTableHead = array(
  'Anrede', 
  'Name', 
  'Vorname', 
  'Telefon', 
  'Email',
  'Anfragestelle',
  //'Aktiv',
  'Info'
);


$arrContactListIndirect = array();
foreach ($arrIndirect as $intRow => $arrRow) {
  
  $strAnfragestelle = '';
  if ($arrRow['co_anfrage'] != '') {
    $arrAnfragestelle = $objAnfragestelle->arrGetAnfragestelle($arrRow['co_anfrage']);
    $strAnfragestelle = $arrAnfragestelle[0]['as_name'];
  }
  
  $strActive = 'nein';
  if ($arrRow['co_active'] == 'true') {
    $strActive = 'ja';
  }
  
  $strInfo = '';
  if ($arrRow['co_info'] != '') {
    $strInfo = $arrRow['co_info'];
  }
  
  $arrContactListIndirect[] = array(
    'Anrede'  => $arrRow['co_salut'],
    'Name'    => '<a href="index_neu.php?ac=contact_details&coid=' .$arrRow['coid'] .'" target="_blank">' .$arrRow['co_last'] .'</a>',
    'Vorname' => $arrRow['co_first'],
    'Telefon' => $arrRow['co_phone'],
    'Email'   => $arrRow['co_mail'],
    'Anfragestelle' => $strAnfragestelle,
    //'Aktiv' => $strActive,
    'Info' => $strInfo
  );
}

$arrContactListDirect = array();

foreach ($arrDirect as $intRow => $arrRow) {
  
  $strAnfragestelle = '';
  if ($arrRow['co_anfrage'] != '') {
    $arrAnfragestelle = $objAnfragestelle->arrGetAnfragestelle($arrRow['co_anfrage']);
    $strAnfragestelle = $arrAnfragestelle[0]['as_name'];
  }
  
  $strActive = 'nein';
  if ($arrRow['co_active'] == 'true') {
    $strActive = 'ja';
  }
  
  $strInfo = '';
  if ($arrRow['co_info'] != '') {
    $strInfo = $arrRow['co_info'];
  }
  
  $arrContactListDirect[] = array(
    'Anrede'  => $arrRow['co_salut'],
    'Name'    => '<a href="index_neu.php?ac=contact_details&coid=' .$arrRow['coid'] .'" target="_blank">' .$arrRow['co_last'] .'</a>',
    'Vorname' => $arrRow['co_first'],
    'Telefon' => $arrRow['co_phone'],
    'Email'   => $arrRow['co_mail'],
    'Anfragestelle' => $strAnfragestelle,
    //'Aktiv' => $strActive,
    'Info' => $strInfo
  );
}

$arrContactListAll = array();
foreach ($arrAll as $intRow => $arrRow) {
  
  $strAnfragestelle = '';
  if ($arrRow['co_anfrage'] != '') {
    $arrAnfragestelle = $objAnfragestelle->arrGetAnfragestelle($arrRow['co_anfrage']);
    $strAnfragestelle = $arrAnfragestelle[0]['as_name'];
  }
  
  $strActive = 'nein';
  if ($arrRow['co_active'] == 'true') {
    $strActive = 'ja';
  }
  
  $strInfo = '';
  if ($arrRow['co_info'] != '') {
    $strInfo = $arrRow['co_info'];
  }
  
  $arrContactListAll[] = array(
    'Anrede'  => $arrRow['co_salut'],
    'Name'    => '<a href="index_neu.php?ac=contact_details&coid=' .$arrRow['coid'] .'" target="_blank">' .$arrRow['co_last'] .'</a>',
    'Vorname' => $arrRow['co_first'],
    'Telefon' => $arrRow['co_phone'],
    'Email'   => $arrRow['co_mail'],
    'Anfragestelle' => $strAnfragestelle,
    //'Aktiv' => $strActive,
    'Info' => $strInfo
  );

  if ($arrRow['co_mail'] != '') {
    if ($arrRow['co_first'] != '') { 
      $arrRow['co_last'] = $arrRow['co_first'] .' ' .$arrRow['co_last'];
    };
  }


}


$strActiveTab = '0';
if (count($arrDirect) == 0) {
  $strActiveTab = '1';
} 
if (count($arrIndirect) == 0) {
  $strActiveTab = '2';
}


$strOutput.= '
        <div id="tabsip" style="width: 1055px; margin-bottom: 20px;">
          <ul>
            <li><a href="#frag-1"><span>direkt</span></a></li>
            <li><a href="#frag-2"><span>indirekt</span></a></li>
            <li><a href="#frag-3"><span>alle</span></a></li>
          </ul>
          <div id="frag-1">
';

$strContactTable = strCreateTableOld($arrContactListDirect, $arrTableHead);
$strOutput.= $strContactTable;

$strOutput.= '
          </div>
          <div id="frag-2">
';

$strContactTable = strCreateTableOld($arrContactListIndirect, $arrTableHead);
$strOutput.= $strContactTable;

$strOutput.= '
          </div>
          <div id="frag-3">
';

$strContactTable = strCreateTableOld($arrContactListAll, $arrTableHead);
$strOutput.= $strContactTable;

$strOutput.= '
          </div>
        </div>
         
<script>
$("#tabsip").tabs({ selected: ' .$strActiveTab .' });
</script>

';


$strOutput.= '			
	</div>
  <div id="tabs-2" class="' .$strClassC2 .'">
	
	  <div id="stream" class="clearfix">
	    <div class="stream-left">

	      <p><a onclick="show(\'all\')" id="sall" class="snav">Alle</a></p>
	      <a onclick="show(\'notiz\')" id="snotiz" class="snav">Notizen</a><br>
	      <a onclick="show(\'file\')" id="sfile" class="snav">Files</a><br>
	      <a onclick="show(\'mail\')" id="smail" class="snav">Emails</a><br>
	      <a onclick="show(\'system\')" id="ssystem" class="snav">System</a><br>

	    </div>
	    <div class="stream-right">
	      
	      <h2>Activities</h2>
	      
	      <div id="stream-list">
';

$strStreamAside = '';

$arrUser = array();
$strSql = 'SELECT `cl_id`, `cl_first`, `cl_last` FROM `cms_login` ORDER BY `cl_id`';
$resSql = MySQLStatic::Query($strSql);
if (is_array($resSql) && (count($resSql) > 0)) {
  foreach ($resSql as $intLine => $arrUserData) {
    $arrUser[$arrUserData['cl_id']] = $arrUserData['cl_first'] .' ' .$arrUserData['cl_last'];
  }
}


$strSql = 'SELECT * FROM `izs_event_change` WHERE `ev_id` = "' .$_REQUEST['det'] .'" ORDER BY `ec_time` DESC';
$resSql = MySQLStatic::Query($strSql);

$arrStreamType = array (
  0 => 'other', 
  1 => 'create', 
  2 => 'update', 
  3 => 'message',
  4 => 'mail'
);

$arrStreamClass = array (
  1 => 'system',
  2 => 'system', 
  3 => 'notiz',
  4 => 'file',
  5 => 'mail'
);


if (is_array($resSql) && (count($resSql) > 0)) {
  foreach ($resSql as $intLine => $arrStreamLine) {
    $intEcType = $arrStreamLine['ec_type'];
    $strStreamType  = $arrStreamType[$intEcType];
    $strStreamClass = $arrStreamClass[$intEcType];
    
    $strFiles = '';
    if (($arrStreamLine['ec_type'] == 3) || ($arrStreamLine['ec_type'] == 5)) {
      $strSql2 = 'SELECT * FROM `izs_event_file` WHERE `ec_id` = "' .$arrStreamLine['ec_id'] .'" ORDER BY `ef_date` DESC';
      $resSql2 = MySQLStatic::Query($strSql2);
      if (is_array($resSql2) && (count($resSql2) > 0)) {
        if ($arrStreamLine['ec_type'] == 3) {
          $strStreamClass = $arrStreamClass[4];
        } else {
          $strStreamClass = $arrStreamClass[5];
        }
        foreach ($resSql2 as $intLine => $arrFile) {
          $strFiles.= '<br /> - <a href="/cms/uploads_events/' .str_pad($arrFile['ef_id'], 11, '0', STR_PAD_LEFT) .'.' .$arrFile['ef_type'] .'" target="_blank">' .$arrFile['ef_name'] .'</a>' .chr(10);
        }
      }
    }
    
    $strOutputList = '<div class="stream-line ' .$strStreamType .' ' .$strStreamClass .'">' .chr(10);
  
    $strDbDate = strtotime($arrStreamLine['ec_time']);
    $strRefDay = gregoriantojd(date('m', $strDbDate), date('d', $strDbDate), date('Y', $strDbDate));
    $strToday  = unixtojd();
    
    $intInterval = ($strToday - $strRefDay);
    
    if ($intInterval == 0) {
      $strInterval = 'heute (' .date('H:i:s', $strDbDate) .' Uhr)';
    } elseif ($intInterval == 1) {
      $strInterval = 'gestern (' .date('H:i:s', $strDbDate) .' Uhr)';
    } else {
      //$strInterval = 'vor ' .$intInterval .' Tagen';
      $strInterval = 'am ' .date('d.m.Y', $strDbDate) .' / ' .date('H:i:s', $strDbDate) .' Uhr';
    }

    $strOutputList.= '<span class="stream-head">' .$arrUser[$arrStreamLine['cl_id']] .' - ' .$strInterval .'</span>' .chr(10);
    
    if ($strStreamClass == 'system') {

      if ($arrStreamLine['ec_fild'] == 'RecordTypeId') {
        $strNew = $arrRecordTypeFlip[$arrStreamLine['ec_new']];
        $strOld = $arrRecordTypeFlip[$arrStreamLine['ec_old']];
      } elseif (($arrStreamLine['ec_fild'] == 'bis_am__c') || ($arrStreamLine['ec_fild'] == 'Rueckmeldung_am__c')) {
        
        if (($arrStreamLine['ec_new'] == '0000-00-00') || ($arrStreamLine['ec_new'] == '')) {
          $strNew = '';
        } else {        
          $strNew = date('d.m.Y', strtotime($arrStreamLine['ec_new']));
        }
        if (($arrStreamLine['ec_old'] == '0000-00-00') || ($arrStreamLine['ec_old'] == '')) {
          $strOld = '';
        } else {
          $strOld = date('d.m.Y', strtotime($arrStreamLine['ec_old']));
        }

      } elseif ($arrStreamLine['ec_fild'] == 'SF42_PublishingStatus__c') {
        $strNew = $arrSichtbar[$arrStreamLine['ec_new']];
        $strOld = $arrSichtbar[$arrStreamLine['ec_old']];
      } elseif ($arrStreamLine['ec_fild'] == 'SF42_EventStatus__c') {
        $strNew = $arrChangePreview[$arrStreamLine['ec_new']];
        $strOld = $arrChangePreview[$arrStreamLine['ec_old']];
      } elseif ($arrStreamLine['ec_fild'] == 'Stundung__c') {
        $strNew = $arrChangeStundung[$arrStreamLine['ec_new']];
        $strOld = $arrChangeStundung[$arrStreamLine['ec_old']];
      } elseif ($arrStreamLine['ec_fild'] == 'Sperrvermerk__c') {
        $strNew = $arrSperr[$arrStreamLine['ec_new']];
        $strOld = $arrSperr[$arrStreamLine['ec_old']];
      } elseif ($arrStreamLine['ec_fild'] == 'SF42_OnlineStatus__c') {
        $strNew = $arrOnline[$arrStreamLine['ec_new']];
        $strOld = $arrOnline[$arrStreamLine['ec_old']];
      } else {
        $strNew = $arrStreamLine['ec_new'];
        $strOld = $arrStreamLine['ec_old'];
      }
      
      if ($arrStreamLine['ec_type'] == 1) {
        $strOutputList.= '<span>Event importiert.</span>' .chr(10);
      } else {
        $strOutputList.= '<span>' .$arrSystemStream[$arrStreamLine['ec_fild']] .'<br/>neu: "' .$strNew .'"<br/>alt: "' .$strOld .'"</span>' .chr(10);
      }
    
    } elseif (($strStreamClass == 'notiz') || ($strStreamClass == 'file') || ($strStreamClass == 'mail')) {
      
      $strRecipients = '';
      
      if ($strStreamClass != 'mail') {
        $strNew = nl2br($arrStreamLine['ec_new']);
      } else {
        $arrMail = unserialize($arrStreamLine['ec_new']);
        
        $strNew = '';
        //$strNew.= print_r($arrMail, true);
        $strNew.= '<p>sendete folgende Nachricht:<br /><strong>' .$arrMail['subject'] .'</strong>';
        $strNew.= '<p>' .nl2br($arrMail['message']) .'</p>';

        $strRecipients.= "<p><strong>Empfänger:</strong><br />\n";
        $strRecipients.= "An: " .$arrMail['to'] ."<br />\n";
        $strRecipients.= "CC: " .$arrMail['cc'] ."<br />\n";
        $strRecipients.= "BCC: " .$arrMail['bcc'] ."<br />\n";
        $strRecipients.= "</p>\n";
      }

      if (($strStreamClass == 'file') || ($strStreamClass == 'mail')) {
        
        $strOutputList.= '<div class="comment">' .chr(10);
        $strOutputList.= '  ' .$strNew .'' .chr(10);
        
        if ($strFiles != '') {
          $strOutputList.= '<p><strong>Files:</strong>' .chr(10);
          $strOutputList.= $strFiles;
          $strOutputList.= '</p>' .chr(10);
        }
        if ($strRecipients != '') {
          $strOutputList.= $strRecipients;
        }
        
        $strOutputList.= '</div>' .chr(10);
        
      } else {
        $strOutputList.= '<div class="comment more">' .chr(10);
        $strOutputList.= '  ' .$strNew .'' .chr(10);
        if ($strRecipients != '') {
          $strOutputList.= $strRecipients;
        }
        $strOutputList.= '</div>' .chr(10);
      }
      
    }
    
    $strOutputList.= '';
    $strOutputList.= '</div>' .chr(10);

    $strOutput.= $strOutputList;

    if (($intEcType >= 3) && ($intEcType <= 5)) {
      $strStreamAside.= $strOutputList;
    }

  }
}
   

$strOutput.= '	      
	      </div> <!-- Stream-List -->
	    </div> <!-- Stream-Right -->
	  </div>
	</div>  <!-- Stream -->

  <div id="tabs-4" class="' .$strClassC4 .'">
    <div id="chats" class="clearfix">
      <h2>Chats</h2>
      
      <div style="padding: 10px; background-color: #fff;">
      <div kng smartchat data-type="topics" data-uri="' .$strIChatUri .'cms/index.php?ac=sear&det=' .$intDetailId .'" data-name="' .$strDataName .'"></div>
      </div>

    </div>  <!-- chats -->
  </div>  <!-- tabs-4 -->
    
</div>



</div><!-- End demo -->

<div style="width: 300px; background-color: #DCDCDC; height: 40px; float: left; border: 1px solid #808080;">
<h3 style="padding-left: 10px;">Kommunikation</h3>
</div>
<div id="streamAside" style="width: 300px; background-color: white; height: 800px; float: left; overflow: scroll; position: relative; top: 40px; margin-left: -302px; border: 1px solid #808080;">

' .$strStreamAside .'

</div>

 
	<script type="text/javascript">
	$(function() {
		//$("#tabs").tabs();
	});

	$(function() {
		$("#datepicker").datepicker();
	});

	$(function() {
		$("#datepicker2").datepicker();
	});
	
  function show(type) {
    if (type == "all") {
      $(".stream-line").removeClass(\'hidden\');
    } else {
      $(".stream-line").addClass(\'hidden\');
      $("." + type).removeClass(\'hidden\');
    }
    $(".snav").css("font-weight", "normal");
    $("#s" + type).css("font-weight", "bold");
  }	
  
$(document).ready(function() {
/*
	var showChar = 200;
	var ellipsestext = "...";
	var moretext = "more";
	var lesstext = "less";
	$(\'.more\').each(function() {
		var content = $(this).html();

		if(content.length > showChar) {

			var c = content.substr(0, showChar);
			var h = content.substr(showChar-1, content.length - showChar);

			var html = c + \'<span class="moreelipses">\'+ellipsestext+\'</span>&nbsp;<span class="morecontent"><span>\' + h + \'</span>&nbsp;&nbsp;<a href="" class="morelink">\'+moretext+\'</a></span>\';

			$(this).html(html);
		}

	});

	$(".morelink").click(function(){
		if($(this).hasClass("less")) {
			$(this).removeClass("less");
			$(this).html(moretext);
		} else {
			$(this).addClass("less");
			$(this).html(lesstext);
		}
		$(this).parent().prev().toggle();
		$(this).prev().toggle();
		return false;
	});
*/
});
  </script>
  
	
<style>
#queue, #queueM {
	height: 79px;
	overflow: auto;
	margin-bottom: 10px;
	padding: 0 3px 3px;
	width: 365px;

  background-color: #5BB75B;
  background-repeat: repeat-x;
  border-radius: 4px;
  color: #FFFFFF;
}

#emp, #empM {
  text-align: center;
  width: 365px;
  display: block;
  padding-top: 30px;
}
.uploadifive-queue-item {
    padding: 3px;
}
.uploadifive-queue-item .close {
    background: url("/cms/img/uploadifive-cancel.png") no-repeat scroll 0 2px rgba(0, 0, 0, 0);
}

.selectize-input {
  width: 504px !important;
}

.items .name {
  margin-right: 5px;
}

.salut, .info {
  display: none;
}

</style>

<div id="dialog_note" title="Neue Notiz">
	<p id="validateTips"></p>

	<form>
	<fieldset>
		<label for="message"></label>
		<textarea name="message" id="message" class="text ui-widget-content ui-corner-all"></textarea>
	</fieldset>

	<div id="queue"><span id="emp">Drop Files here...</span></div>
	<input type="hidden" name="fnames" id="fnames" />
	<input type="file" name="file_upload_n" id="file_upload_n" />

';

$timestamp = time();

$strOutput.= '
<script type="text/javascript">

function dump(obj) {
    var out = "";
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }
    alert(out);
}

$(document).ready(function($) {
      $(\'#file_upload_n\').uploadifive({
        \'auto\'             : true,
        \'multi\'            : false, 
        \'queueSizeLimit\'   : 999,
        \'uploadLimit\'      : 999, 
				\'formData\'         : { \'source\': \'note\', \'timestamp\': \'' .$timestamp .'\', \'token\': \'' .md5('unique_salt' .$timestamp) .'\' },
        \'queueID\'          : \'queue\',
        \'itemTemplate\'     : \'<div class="uploadifive-queue-item"><span class="filename"></span><span class="fileinfo"></span><div class="close"></div></div>\', 
        \'uploadScript\'     : \'uploadifive.php\',
        \'onUploadComplete\' : function(file, data) { console.log("onUploadComplete"); if (file.name != "") { var input = $("#fnames"); input.val(input.val() + "|" + file.name);} }, 
        \'onAddQueueItem\'   : function(file) { console.log(file.name); $("#emp").css("display", "none"); }, 
        \'onQueueComplete\'  : function() { console.log("onQueueComplete"); }, 
        \'onCancel\'         : function(file) { console.log("onCancel"); var input = $("#fnames"); if (file.name != "") { input.val(input.val().replace("|" + file.name, "")); } if (input.val() == "") { setTimeout("$(\'#emp\').css(\'display\', \'block\');", 2000); } } 
      });
    });

</script>


	</form>
</div>


<div id="dialog_mail" title="Neue E-Mail" class="clearfix">
<div style="width: 270px; border-right: 1px solid #DDDDDD; float: left; height: 700px; overflow: hidden;">
';

$strSql = 'SELECT * FROM `cms_text` WHERE `ct_type` = 1 ORDER BY `ct_name`';
$arrResult = MySQLStatic::Query($strSql);

$strJs = '';
$strJs.= 'var subj = new Array(); ' .chr(10);
$strJs.= 'var mess = new Array(); ' .chr(10);
$strJs.= 'subj[0] = "";' .chr(10);
$strJs.= 'mess[0] = "";' .chr(10);

$strOutput.= '<p><a onclick="pattern(\'0\')" id="p_0" class="pnav" style="font-weight: bold;">keine Vorlage</a></p>';

$intOrder = 100;

if (count($arrResult) > 0) {
  foreach ($arrResult as $arrText) {
    
    $arrText['ct_head'] = str_replace(chr(10), '', $arrText['ct_head']);
    $arrText['ct_text'] = str_replace(chr(10), '', $arrText['ct_text']);

    $strOutput.= '<a onclick="pattern(\'' .$arrText['ct_id'] .'\')" id="p_' .$arrText['ct_id'] .'" class="pnav">' .$arrText['ct_name'] .'</a><br /><br />' .chr(10);
    $strJs.= 'subj[' .$arrText['ct_id'] .'] = "' .str_replace('"', '\"', replace_placeholder($arrText['ct_head'], $_REQUEST['det'])) .'";' .chr(10);
    $strJs.= 'mess[' .$arrText['ct_id'] .'] = "' .str_replace('"', '\"', replace_placeholder($arrText['ct_text'], $_REQUEST['det'])) .'";' .chr(10);

    $intOrder = $arrText['ct_order'];

  }
}

$strOutput.= '
</div>

<div style="width: 580px; float: left; padding: 0 0px 10px 20px;">
	<p id="validateTipsM" class="validateTips"></p>

	<form id="eMail">
	  <fieldset class="clearfix">

<label for="select-to">An:</label>
<select id="to" class="contacts" placeholder="Empfänger auswählen..." autocomplete="off"></select>
<label for="cc">CC:</label>
<select id="cc" class="contacts" placeholder="Empfänger auswählen..." autocomplete="off"></select>
<label for="bcc">BCC:</label>
<input id="bcc" class="text ui-widget-content ui-corner-all" type="text" name="bcc" autocomplete="off">

<label for="subject" class="space">Betreff:</label>
<input id="subject" class="text ui-widget-content ui-corner-all space" type="text" name="subject">

</fieldset>

	<fieldset class="clearfix">
		<label for="messageM">Nachricht</label><span class="clearfix"></span>
		<textarea name="messageM" id="messageM" class="text ui-widget-content ui-corner-all"></textarea>
	</fieldset>

	<div id="queueM" style="margin-top: 10px;"><span id="empM">Drop Files here...</span></div>
	<input type="hidden" name="fnames" id="fnames" />
	<input type="file" name="file_upload" id="file_upload" />

';

$timestamp = time();

$strOutput.= '
<script type="text/javascript">

var salutation = new Array();

bkLib.onDomLoaded(function() {
  mM = new nicEditor({
    buttonList : ["bold","italic","underline","left","center","right"], 
    maxHeight : 370
  }).panelInstance("messageM");
});

' .$strJs .'
  function pattern(type) {
    $("#subject").val(subj[type]);
    $("#messageM").val(mess[type]);
    $(".nicEdit-main").focus();
    mM.nicInstances[0].setContent(mess[type]);
    $(".pnav").css("font-weight", "normal");
    $("#p_" + type).css("font-weight", "bold");
    $(".nicEdit-main").html($(".nicEdit-main").html().replace("{Salutation}", "<span class=\"salutToReplace\">{Salutation}</span>"));
    if (salutation.length > 0) {
      $(".salutToReplace").html(salutation.join(",<br>") + ",");
    } 
  }	

function dump(obj) {
    var out = "";
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }
    alert(out);
}

$(document).ready(function($) {
      $(\'#file_upload\').uploadifive({
        \'auto\'             : true,
        \'multi\'            : false, 
        \'queueSizeLimit\'   : 999,
        \'uploadLimit\'      : 999, 
				\'formData\'         : { \'source\': \'note\', \'timestamp\': \'' .$timestamp .'\', \'token\': \'' .md5('unique_salt' .$timestamp) .'\' },
        \'queueID\'          : \'queueM\',
        \'itemTemplate\'     : \'<div class="uploadifive-queue-item"><span class="filename"></span><span class="fileinfo"></span><div class="close"></div></div>\', 
        \'uploadScript\'     : \'uploadifive.php\',
        \'onUploadComplete\' : function(file, data) { if (file.name != "") { var input = $("#fnames"); input.val(input.val() + "|" + file.name);} }, 
        \'onAddQueueItem\'   : function(file) { $("#empM").css("display", "none"); }, 
        \'onCancel\'         : function(file) { var input = $("#fnames"); if (file.name != "") { input.val(input.val().replace("|" + file.name, "")); } if (input.val() == "") { setTimeout("$(\'#empM\').css(\'display\', \'block\');", 2000); } } 
      });
    });

</script>


	</form>
</div>
</div>


	<script type="text/javascript">

	
  $(document).ready(function() {


  var REGEX_EMAIL = "([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@" +
  "(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)";

  $(\'#to\').selectize({
    persist: false,
    maxItems: null,
    valueField: \'email\',
    labelField: \'name\',
    searchField: [\'name\', \'email\'],
    options: [
';

$strSqlContact1 = 'SELECT `group_id` FROM `SF42_IZSEvent__c` WHERE `Id` = "' .$_REQUEST['det'] .'"';
$arrSqlContact1 = MySQLStatic::Query($strSqlContact1);

/*
$strSqlContact2 = 'SELECT * FROM  `Account` WHERE  `SF42_Company_Group__c` = "' .$arrSqlContact1[0]['group_id'] .'" AND  `Hauptsitz__c` = "true"';
$arrSqlContact2 = MySQLStatic::Query($strSqlContact2);

if (count($arrSqlContact2) > 0) {

  if (@$_SERVER['HTTP_FKLD'] == 'on') {
    //echo 'SELECT * FROM `Contact` WHERE `AccountId` = "' .$arrSqlContact2[0]['Id'] .'" AND `Ansprechpartner_fuer__c` LIKE "%Beitragsrückstand%" AND `Aktiv__c` = "true" AND `Email` != "" ORDER BY `LastName`' .chr(10);
  }

  $strSqlContact3 = 'SELECT * FROM `Contact` WHERE `AccountId` = "' .$arrSqlContact2[0]['Id'] .'" AND `Ansprechpartner_fuer__c` LIKE "%Beitragsrückstand%" AND `Aktiv__c` = "true" AND `Email` != "" ORDER BY `LastName`';

  GroupId

  */
  $strSqlContact3 = 'SELECT * FROM `Contact` WHERE `GroupId` = "' .$arrSqlContact1[0]['group_id'] .'" AND `Ansprechpartner_fuer__c` LIKE "%Beitragsrückstand%" AND `Aktiv__c` = "true" AND `Email` != "" ORDER BY `LastName`';
  $arrSqlContact3 = MySQLStatic::Query($strSqlContact3);

  foreach ($arrSqlContact3 as $intCount => $arrContact) {
    $strContactSalut = $arrContact['Anrede_Brief_E_Mail__c'];
    
    if (strstr($arrContact['Anrede_Brief_E_Mail__c'], 'Herren') === false) {
      $strContactSalut.= ' ' .$arrContact['LastName'];
      $strContactName  = $arrContact['Salutation'] .' ' .$arrContact['Name'];
    } else {
      $strContactName = $arrContact['Name'];
    }

    $strOutput.= '
      {email: \'' .$arrContact['Email'] .'\', name: \'' .$strContactName .'\', salut: \'' .$strContactSalut .'\', info: \'' .str_replace(chr(10), '', $arrContact['Info__c']) .'\'},
';
  }

/* } */

$strOutput.= '
    ],
    render: {
      item: function(item, escape) {
        var cl = ""
        if (item.info != "") cl = "btninfo";
        return \'<div class="\' + cl + \'" title="\' + escape(item.info) + \'">\' +
          (item.name ? \'<span class="name">\' + escape(item.name) + \'</span>\' : \'\') +
          (item.email ? \'<span class="email">\' + escape(item.email) + \'</span>\' : \'\') +
          (item.salut ? \'<span class="salut">\' + escape(item.salut) + \'</span>\' : \'\') +
          (item.info ? \'<span class="info">\' + escape(item.info) + \'</span>\' : \'\') +
          \'</div>\';
      },
      option: function(item, escape) {
        var label = item.name || item.email;
        var caption = item.name ? item.email : null;
        var salut = item.salut;
        var info = item.info;
        var cl = ""
        if (item.info != "") cl = "btninfo";
        return \'<div class="\' + cl + \'" title="\' + escape(info) + \'">\' +
        \'<span class="label">\' + escape(label) + \'</span>\' +
        (caption ? \'<span class="caption">\' + escape(caption) + \'</span>\' : \'\') +
        \'<span class="salut">\' + escape(salut) + \'</span>\' +
        \'<span class="info">\' + escape(info) + \'</span>\' +
        \'</div>\';
      }
    },
    createFilter: function(input) {
      var match, regex;

      // email@address.com
      regex = new RegExp(\'^\' + REGEX_EMAIL + \'$\', \'i\');
      match = input.match(regex);
      if (match) return !this.options.hasOwnProperty(match[0]);

      // name <email@address.com>
      regex = new RegExp(\'^([^<]*)\<\' + REGEX_EMAIL + \'\>$\', \'i\');
      match = input.match(regex);
      if (match) return !this.options.hasOwnProperty(match[2]);

      return false;
    },
    create: function(input) {
      if ((new RegExp(\'^\' + REGEX_EMAIL + \'$\', \'i\')).test(input)) {
        return {email: input};
      }
      var match = input.match(new RegExp(\'^([^<]*)\<\' + REGEX_EMAIL + \'\>$\', \'i\'));
      if (match) {
        return {
          email : match[2],
          name  : $.trim(match[1])
        };
      }
      alert(\'Ungültige E-Mail-Adresse.\');
      return false;
    },
    onItemAdd: function (value, $item) {
      if ($item.context.childNodes.length > 1) {
        salutation.push($item.context.childNodes[2].innerText);
        if (salutation.length > 0) {
          $(".salutToReplace").html(salutation.join(",<br>") + ",");
        } else {
          $(".salutToReplace").html("{Salutation}");
        }
      }
      //console.log(salutation);
    },
    onItemRemove: function (value, $item) {
      if ($item.context.childNodes.length > 1) {
        var index = salutation.indexOf($item.context.childNodes[2].innerText);
        if (index > -1) {
          salutation.splice(index, 1);
          if (salutation.length > 0) {
            $(".salutToReplace").html(salutation.join(",<br>") + ",");
          } else {
            $(".salutToReplace").html("{Salutation}");
          }
        }
      }
      //console.log(salutation);
    }
  });

  $(\'#cc\').selectize({
    persist: false,
    maxItems: null,
    valueField: \'email\',
    labelField: \'name\',
    searchField: [\'name\', \'email\'],
    options: [
';

if (count($arrSqlContact3) > 0) {

  foreach ($arrSqlContact3 as $intCount => $arrContact) {
    $strOutput.= '
      {email: \'' .$arrContact['Email'] .'\', name: \'' .$arrContact['Name'] .'\', salut: \'' .$arrContact['Anrede_Brief_E_Mail__c'] .'\', info: \'' .str_replace(chr(10), '', $arrContact['Info__c']) .'\'},
';
  }

}

$strOutput.= '
    ],
    render: {
      item: function(item, escape) {
        var cl = ""
        if (item.info != "") cl = "btninfo";
        return \'<div class="\' + cl + \'" title="\' + escape(item.info) + \'">\' +
          (item.name ? \'<span class="name">\' + escape(item.name) + \'</span>\' : \'\') +
          (item.email ? \'<span class="email">\' + escape(item.email) + \'</span>\' : \'\') +
          (item.salut ? \'<span class="salut">\' + escape(item.salut) + \'</span>\' : \'\') +
          (item.info ? \'<span class="info">\' + escape(item.info) + \'</span>\' : \'\') +
          \'</div>\';
      },
      option: function(item, escape) {
        var label = item.name || item.email;
        var caption = item.name ? item.email : null;
        var salut = item.salut;
        var info = item.info;
        var cl = ""
        if (item.info != "") cl = "btninfo";
        return \'<div class="\' + cl + \'" title="\' + escape(info) + \'">\' +
        \'<span class="label">\' + escape(label) + \'</span>\' +
        (caption ? \'<span class="caption">\' + escape(caption) + \'</span>\' : \'\') +
        \'<span class="salut">\' + escape(salut) + \'</span>\' +
        \'<span class="info">\' + escape(info) + \'</span>\' +
        \'</div>\';
      }
    },
    createFilter: function(input) {
      var match, regex;

      // email@address.com
      regex = new RegExp(\'^\' + REGEX_EMAIL + \'$\', \'i\');
      match = input.match(regex);
      if (match) return !this.options.hasOwnProperty(match[0]);

      // name <email@address.com>
      regex = new RegExp(\'^([^<]*)\<\' + REGEX_EMAIL + \'\>$\', \'i\');
      match = input.match(regex);
      if (match) return !this.options.hasOwnProperty(match[2]);

      return false;
    },
    create: function(input) {
      if ((new RegExp(\'^\' + REGEX_EMAIL + \'$\', \'i\')).test(input)) {
        return {email: input};
      }
      var match = input.match(new RegExp(\'^([^<]*)\<\' + REGEX_EMAIL + \'\>$\', \'i\'));
      if (match) {
        return {
          email : match[2],
          name  : $.trim(match[1])
        };
      }
      alert(\'Ungültige E-Mail-Adresse.\');
      return false;
    }
  });

  $("div#dialog_mail").on("dialogclose", function(event) {
    var $select = $("#to").selectize();
    var control = $select[0].selectize;
    control.clear();
    var $select = $("#cc").selectize();
    var control = $select[0].selectize;
    control.clear();
  })



		var message = $("#message"), messageM = $("#messageM"), to = $("#to"), cc = $("#cc"), bcc = $("#bcc"), subject = $("#subject"),
			allFields = $([]).add(message).add(to).add(cc).add(bcc).add(subject),
			tips = $("#validateTips"), 
			tipsM = $("#validateTipsM");

		function updateTips(t) {
			tips.text(t).effect("highlight",{},1500);
		}

		function updateTipsM(t) {
			tipsM.text(t).effect("highlight",{},1500);
		}

		function checkLength(o,f,min) {
			if ( o.val().length < min ) {
				o.addClass(\'ui-state-error\');
				updateTipsM("Bitte das Feld \'" +f +"\' ausfüllen.");
				return false;
			} else {
				return true;
			}
		}
		
		function checkEnty(o,n,min) {
			if ( o.val().length < min ) {
				o.addClass(\'ui-state-error\');
				updateTips("Es steht nichts im Notizfeld. Bitte nutze das Feld :-).");
				return false;
			} else {
				return true;
			}
		}

		function checkRegexp(o,regexp,n) {
			if ( !( regexp.test( o.val() ) ) ) {
				o.addClass(\'ui-state-error\');
				updateTipsM(n);
				return false;
			} else {
				return true;
			}
		}
    
		$("#dialog_note").dialog({
			bgiframe: true,
			autoOpen: false,
			resizable: false,
			height: 365,
			width: 400,
			modal: true,
			buttons: {
				\'OK\': function() {
					var bValid = true;
					var mes = message.val();
          allFields.removeClass(\'ui-state-error\');
          
          //var fi = queue.innerText.split("\n").join("|");
          //var fnames = fi.substring(0, fi.length - 1);

					bValid = bValid && checkEnty(message,"message",1);
					
					if (bValid) {
            $.ajax({
              type: "POST",
              dataType: "html; charset=utf-8", 
              url:  "_ajax.php",
              data: { id: ' .$_REQUEST['det'] .', fnames: $("#fnames").val(), message: mes, type: \'event\', name: "' .$strEventName .'" },
            });
            $(this).dialog(\'close\');
					}
				},
				\'Abbrechen\': function() {
					$(this).dialog(\'close\');
				}
			},
			close: function() {
			  tips.text(\' \');
				allFields.val(\'\').removeClass(\'ui-state-error\');
			}
		});
		
		
		$(\'#add-note\').click(function() {
      $(\'#queue\').html(\'<span id="emp">Drop Files here...</span>\');
      $(\'#file_upload_n\').uploadifive(\'clearQueue\');
			$(\'#dialog_note\').dialog(\'open\');
			
			$("#dialog_note ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');
			
		})
		.hover(
			function(){ 
				$(this).addClass("ui-state-hover"); 
			},
			function(){ 
				$(this).removeClass("ui-state-hover"); 
			}
		).mousedown(function(){
			$(this).addClass("ui-state-active"); 
		})
		.mouseup(function(){
				$(this).removeClass("ui-state-active");
		});


    
		$("#dialog_mail").dialog({
			bgiframe: true,
			autoOpen: false,
			resizable: false,
			height: 800,
			width: 920,
			modal: true,
			buttons: {
				\'OK\': function() {
					var bValid = true;
					var mesM = $(\'#eMail\').find(\'.nicEdit-main\').html();
					var toM = to.val();
					var ccM = cc.val();
					var bccM = bcc.val();
					var subjectM = subject.val();
					allFields.removeClass(\'ui-state-error\');
					
          bValid = $("#to").val().length;
          
					bValid = bValid && checkLength($("#subject"),"Betreff",1);

					bValid = bValid && checkEnty(messageM,"message",1);

          //bValid = bValid && checkRegexp($("#to"),/^([_a-z0-9\-]+)(\.[_a-z0-9\-]+)*@([a-z0-9\-]{2,}\.)*([a-z]{2,4})(\s*;\s*([_a-z0-9\-]+)(\.[_a-z0-9\-]+)*@([a-z0-9\-]{2,}\.)*([a-z]{2,4}))*$/i,"Keine gültige E-Mail-Adresse.");
          
          if ($("#cc").val() != "") {
            //bValid = bValid && checkRegexp($("#cc"),/^([_a-z0-9\-]+)(\.[_a-z0-9\-]+)*@([a-z0-9\-]{2,}\.)*([a-z]{2,4})(\s*;\s*([_a-z0-9\-]+)(\.[_a-z0-9\-]+)*@([a-z0-9\-]{2,}\.)*([a-z]{2,4}))*$/i,"Keine gültige E-Mail-Adresse.");
          }
          
          if ($("#bcc").val() != "") {
            bValid = bValid && checkRegexp($("#bcc"),/^([_a-z0-9\-]+)(\.[_a-z0-9\-]+)*@([a-z0-9\-]{2,}\.)*([a-z]{2,4})(\s*;\s*([_a-z0-9\-]+)(\.[_a-z0-9\-]+)*@([a-z0-9\-]{2,}\.)*([a-z]{2,4}))*$/i,"Keine gültige E-Mail-Adresse.");
				  }
				  	
					if (bValid) {
            $.ajax({
              type: "POST",
              url:  "_ajax.php",
              dataType: "html; charset=utf-8", 
              data: { id: ' .$_REQUEST['det'] .', to: $("#to").val(), cc: $("#cc").val(), bcc: $("#bcc").val(), subject: $("#subject").val(), fnames: $("#fnames").val(), message: mesM, type: \'mail\', name: "' .$strEventName .'" },
            });
            $(this).dialog(\'close\');

					}
				},
				\'Abbrechen\': function() {
					$(this).dialog(\'close\');
				}
			},
			close: function() {
			  tips.text(\' \');
				allFields.val(\'\').removeClass(\'ui-state-error\');
			}
		});
		
		
		$(\'#add-mail\').click(function() {
		  $(\'#messageM\').val(\'\');
      $(\'#queueM\').html(\'<span id="empM">Drop Files here...</span>\');
      $(\'#file_upload\').uploadifive(\'clearQueue\');
      pattern("0");
			$(\'#dialog_mail\').dialog(\'open\');
			
			$("#dialog_mail ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');
			
		})
		.hover(
			function(){ 
				$(this).addClass("ui-state-hover"); 
			},
			function(){ 
				$(this).removeClass("ui-state-hover"); 
			}
		).mousedown(function(){
			$(this).addClass("ui-state-active"); 
		})
		.mouseup(function(){
				$(this).removeClass("ui-state-active");
		});

	});
	
	</script>

  ';

} else {

// SUCHE BEGIN 

$strSql0 = "SELECT DISTINCT `Beitragsmonat__c`, `Beitragsjahr__c` FROM `SF42_IZSEvent__c` ORDER BY `Beitragsjahr__c` DESC, `Beitragsmonat__c` DESC";
$arrResult0 = MySQLStatic::Query($strSql0);
if (count($arrResult0) > 0) {
  
  $strOutput.= '<form method="post" action="">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);
  $strOutput.= '<input type="hidden" name="ac" value="' .$_REQUEST['ac'] .'">' .chr(10);
  
  if ($_REQUEST['conly'] == 1) {
    $strOutput.= '<input type="hidden" name="strSelPay" value="' .$_REQUEST['strSelPay'] .'">' .chr(10);
    $strOutput.= '<input type="hidden" name="strSelGru" value="all">' .chr(10);
  }
  
  $strOutput.= '<table>' .chr(10);
  $strOutput.= '<tbody>' .chr(10);

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Zeitraum: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelDate" id="strSelDate">' .chr(10);
  
  $intPeriod = 0;

  if ($_REQUEST['strSelDate'] == '%_%') {
    $strSelected = ' selected="selected"';
  } 
  $strOutput.= '  <option value="%_%"' .$strSelected .'> </option>' .chr(10);

  foreach ($arrResult0 as $arrPeriod0) {
    $strSelected = '';
    $strValue = $arrPeriod0['Beitragsmonat__c'] .'_' .$arrPeriod0['Beitragsjahr__c'];

    if (empty($_REQUEST['strSelDate']) && ($intPeriod == 0)) {
      $_REQUEST['strSelDate'] = $strValue;
      //$_REQUEST['send'] = 1;
    }
    
    if ($strValue == $_REQUEST['strSelDate']) {
      $strSelected = ' selected="selected"';
    } 
    $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .str_pad($arrPeriod0['Beitragsmonat__c'], 2 ,'0', STR_PAD_LEFT) .'-' .$arrPeriod0['Beitragsjahr__c'] .'</option>' .chr(10);
    $intPeriod++;
  }
  
  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  if ($_REQUEST['conly'] != 1) {
    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Event: </td>' .chr(10);
    $strOutput.= '    <td>E- <input type="text" name="strSearchValue" value="' .$_REQUEST['strSearchValue'] .'"></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }

  $arrStatusCss = array ( 
    'abgelehnt / Dublette' => 'warning', 
    'abgelehnt / Frist' => 'warning', 
    'accepted' => 'warning', 
    'bereit für REVIEW' => 'warning', 
    'enquired' => 'wait', 
    'in progress' => 'warning', 
    'new' => 'warning', 
    'no Feedback' => 'question', 
    'no result' => 'warning', 
    'not assignable' => 'warning', 
    'not OK' => 'warning', 
    'OK' => 'thick', 
    'refused' => 'warning', 
    'to clear' => 'warning', 
    'to enquire' => 'warning', 
    'zugeordnet / abgelegt' => 'warning', 
    'zurückgestellt von IZS' => 'warning'
  );

  $arrStatus = $arrChangePreview;
    
  if (count($arrStatus) > 0) {
    
    if (($_REQUEST['strSelSta'] == 'all') || ($_REQUEST['strSelSta'] == '')) {
      $strSelectedAll = ' selected="selected"';
      $_REQUEST['strSelSta'] = 'all';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Event-Status: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelSta" id="strSelSta">' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>Alle</option>' .chr(10);
    
    foreach ($arrStatus as $strStatus => $strValue) {
      $strSelected = '';
      if ($strStatus == $_REQUEST['strSelSta']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option value="' .$strStatus .'"' .$strSelected .'>' .$strValue .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
  
  if (count($arrRecordType) > 0) {
    
    if (($_REQUEST['strSelRec'] == 'all') || ($_REQUEST['strSelRec'] == '')) {
      $strSelectedAll = ' selected="selected"';
      $_REQUEST['strSelRec'] = 'all';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Datensatz-Typ: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelRec" id="strSelRec">' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>Alle</option>' .chr(10);
    
    foreach ($arrRecordType as $strValue => $strId) {
      $strSelected = '';
      if ($strId == $_REQUEST['strSelRec']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option value="' .$strId .'"' .$strSelected .'>' .$strValue .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
    
  $strSql6 = 'SELECT DISTINCT `SF42_IZSEvent__c`.`SF42_informationProvider__c` AS `Id`, `Account`.`Name` AS `Name`, `Account`.`Dezentrale_Anfrage__c` ';
  $strSql6.= 'FROM `SF42_IZSEvent__c` INNER JOIN `Account` ';
  $strSql6.= 'ON `SF42_IZSEvent__c`.`SF42_informationProvider__c` = `Account`.`Id` ';
  $strSql6.= 'WHERE `SF42_isInformationProvider__c` = "true" ';
  if ($_REQUEST['conly'] == 1) {
    $strSql6.= 'AND `Betriebsnummer_ZA__c` = "' .$_REQUEST['strSelPay'] . '" '; 
    $arrPeriod = explode('_', $_REQUEST['strSelDate']);
    $strSql.= 'AND (`Beitragsmonat__c` = "' .$arrPeriod[0] .'") AND (`Beitragsjahr__c` = "' .$arrPeriod[1] .'") ';
  }
  $strSql6.= 'ORDER BY `Account`.`Name` ASC';
  
  $strIpSelected = '';
  
  $arrResult6 = MySQLStatic::Query($strSql6);
  if (count($arrResult6) > 0) {

    if ($_REQUEST['strSelInf'] == 'all') {
      $strSelectedAll = ' selected="selected"';
    } else {
      $strSelectedAll = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Information Provider: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelInf" id="strSelInf">' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Information Provider -</option>' .chr(10);
    
    foreach ($arrResult6 as $arrProvider) {
      $strSelected = '';
      if ($arrProvider['Id'] == $_REQUEST['strSelInf']) {
        $strSelected = ' selected="selected"';
        $strIpSelected = $arrProvider['Name'];
      } 
      
      $strAddDez = '';
      
      if ($arrProvider['Dezentrale_Anfrage__c'] == 'true') {
        $strOptClass = ' class="optv"';
        
        $strSqlDez = 'SELECT `Id`, `Name`, `CATCH_ALL__c` FROM `Anfragestelle__c` WHERE `Information_Provider__c`="' .$arrProvider['Id'] .'" ORDER BY `Name`';
        $arrSqlDez = MySQLStatic::Query($strSqlDez);

        //$arrProvider['Id'] = 'da_' .$arrProvider['Id'];
        
        if (count($arrSqlDez) > 0) {
          foreach ($arrSqlDez as $arrDez) {
            
            $strSelectedDez = '';
            if (('d_' .$arrDez['Id']) == $_REQUEST['strSelInf']) {
              $strSelectedDez = ' selected="selected"';
              $strIpSelected = $arrDez['Name'];
            } 
            
            $strAddDez.= '    <option value="d_' .$arrDez['Id'] .'"' .$strSelectedDez .'>&nbsp;&nbsp;- ' .$arrDez['Name'] .'</option>' .chr(10);
          } 
        }
        
      } else {
        $strOptClass = '';
      }
      
      $strOutput.= '    <option' .$strOptClass .' value="' .$arrProvider['Id'] .'"' .$strSelected .'>' .$arrProvider['Name'] .'</option>' .chr(10);
      if ($_REQUEST['conly'] != 1) {
        $strOutput.= $strAddDez;
      }
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
  
  if ($_REQUEST['conly'] != 1) {
      
    $strSql6 = 'SELECT `Name`, `SF42_Comany_ID__c` AS `Btnr`  FROM `Account` WHERE `SF42_isPremiumPayer__c` = "true" AND `SF42_Comany_ID__c` != "" ORDER BY `Name`';
    
    $arrResult6 = MySQLStatic::Query($strSql6);
    if (count($arrResult6) > 0) {
  
      if ($_REQUEST['strSelPay'] == 'all') {
        $strSelectedAll = ' selected="selected"';
      } else {
        $strSelectedAll = '';
      }
  
      $strOutput.= '  <tr>' .chr(10);
      $strOutput.= '    <td>Premium Payer: </td>' .chr(10);
      $strOutput.= '    <td><select name="strSelPay" id="strSelPay">' .chr(10);
      $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Premium Payer -</option>' .chr(10);
      
      foreach ($arrResult6 as $intKey => $arrProvider) {
        $strSelected = '';
        if ($arrProvider['Btnr'] == $_REQUEST['strSelPay']) {
          $strSelected = ' selected="selected"';
        } 
        
        if (($strLastName == $arrProvider['Name']) || ($arrResult6[$intKey + 1]['Name'] == $arrProvider['Name'])) {
           $strAdd = ' (' .$arrProvider['Btnr'] .')';
        } else {
          $strAdd = '';
        }
        
        $strOutput.= '    <option value="' .$arrProvider['Btnr'] .'"' .$strSelected .'>' .$arrProvider['Name'] .'' .$strAdd .'</option>' .chr(10);
        $strLastName = $arrProvider['Name'];
        
      }
    
      $strOutput.= '    </select></td>' .chr(10);
      $strOutput.= '  </tr>' .chr(10);
    }
  
  
      
    $strSql6 = 'SELECT `Name`, `Id`  FROM `izs_premiumpayer_group` ORDER BY `Name`';
    
    $arrResult6 = MySQLStatic::Query($strSql6);
    if (count($arrResult6) > 0) {
  
      if ($_REQUEST['strSelPay'] == 'all') {
        $strSelectedAll = ' selected="selected"';
      } else {
        $strSelectedAll = '';
      }
  
      $strOutput.= '  <tr>' .chr(10);
      $strOutput.= '    <td>Company Group: </td>' .chr(10);
      $strOutput.= '    <td><select name="strSelGru" id="strSelGru">' .chr(10);
      $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Company Groups -</option>' .chr(10);
      
      foreach ($arrResult6 as $intKey => $arrProvider) {
        $strSelected = '';
        if ($arrProvider['Id'] == $_REQUEST['strSelGru']) {
          $strSelected = ' selected="selected"';
        } 
        
        $strOutput.= '    <option value="' .$arrProvider['Id'] .'"' .$strSelected .'>' .$arrProvider['Name'] .'</option>' .chr(10);
        //$strLastName = $arrProvider['Name'];
        
      }
    
      $strOutput.= '    </select></td>' .chr(10);
      $strOutput.= '  </tr>' .chr(10);
    }
    
  
      
    if (count($arrStatusKlaerung) > 0) {
  
      if (($_REQUEST['strSelKla'] == 'all') || ($_REQUEST['strSelKla'] == '')) {
        $strSelectedAll = ' selected="selected"';
      } else {
        $strSelectedAll = '';
      }
  
      if ($_REQUEST['strSelKla'] == 'empty') {
        $strSelectedEmpty = ' selected="selected"';
      } else {
        $strSelectedEmpty = '';
      }

      $strOutput.= '  <tr>' .chr(10);
      $strOutput.= '    <td>Klärung: </td>' .chr(10);
      $strOutput.= '    <td><select name="strSelKla" id="strSelKla">' .chr(10);
      $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Status -</option>' .chr(10);
      $strOutput.= '    <option value="empty"' .$strSelectedEmpty .'>kein Status</option>' .chr(10);
      
      foreach ($arrStatusKlaerung as $strKey => $strKlaerung) {
        $strSelected = '';
        if ($strKey == $_REQUEST['strSelKla']) {
          $strSelected = ' selected="selected"';
        } 
        
        $strOutput.= '    <option value="' .$strKey .'"' .$strSelected .'>' .$strKlaerung .'</option>' .chr(10);
        //$strLastName = $arrProvider['Name'];
        
      }
    
      $strOutput.= '    </select></td>' .chr(10);
      $strOutput.= '  </tr>' .chr(10);
    }

    $arrStundung = array(
      'Stundung' => 'Stundung',
      'Ratenzahlung' => 'Ratenzahlung',
      'Stundung oder Ratenzahlung' => 'Stundung oder Ratenzahlung',
    );
      
    if (count($arrStundung) > 0) {
  
      if (($_REQUEST['strSelStu'] == 'all') || ($_REQUEST['strSelStu'] == '')) {
        $strSelectedAll = ' selected="selected"';
      } else {
        $strSelectedAll = '';
      }
  
      if ($_REQUEST['strSelStu'] == 'empty') {
        $strSelectedEmpty = ' selected="selected"';
      } else {
        $strSelectedEmpty = '';
      }

      $strOutput.= '  <tr>' .chr(10);
      $strOutput.= '    <td width="160">Stundung/Ratenzahlung: </td>' .chr(10);
      $strOutput.= '    <td><select name="strSelStu" id="strSelStu">' .chr(10);
      $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle anzeigen -</option>' .chr(10);
      $strOutput.= '    <option value="empty"' .$strSelectedEmpty .'>kein Eintrag</option>' .chr(10);
      
      foreach ($arrStundung as $strKey => $strStundung) {
        $strSelected = '';
        if ($strKey == $_REQUEST['strSelStu']) {
          $strSelected = ' selected="selected"';
        } 
        
        $strOutput.= '    <option value="' .$strKey .'"' .$strSelected .'>' .$strStundung .'</option>' .chr(10);
        //$strLastName = $arrProvider['Name'];
        
      }
    
      $strOutput.= '    </select></td>' .chr(10);
      $strOutput.= '  </tr>' .chr(10);
    }

  } //conly

  $strOutput.= '</tbody>' .chr(10);

  $strOutput.= '  <tfoot>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td align="right" colspan="2"><input type="submit" name="Suche" value="Suche" /></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);
  $strOutput.= '  </tfoot>' .chr(10);

  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);
    
}

if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {
  
  $arrPeriod = explode('_', $_REQUEST['strSelDate']);
  
  if (strstr($_REQUEST['strSelInf'], 'd_') != false) {
    
    $_REQUEST['strSelInf'] = substr($_REQUEST['strSelInf'], 2);
    $strAnfragestelleId = $_REQUEST['strSelInf'];
    
    //IST CATCH ALL?
    $strSqlDez = 'SELECT `Id`, `Name`, `CATCH_ALL__c` FROM `Anfragestelle__c` WHERE `Id`="' .$_REQUEST['strSelInf'] .'"';
    $arrSqlDez = MySQLStatic::Query($strSqlDez);

    //if ($_SERVER['REMOTE_ADDR'] == '62.245.254.82') { echo $strSqlDez .chr(10); print_r($arrSqlDez); /* die(); */ }
    
    $strSqlPp = 'SELECT `Premium_Payer__c`, `Information_provider__c` FROM `Dezentrale_Anfrage_KK__c` WHERE `Anfragestelle__c` = "' .$_REQUEST['strSelInf'] .'"';
    $arrSqlPp = MySQLStatic::Query($strSqlPp);

    //if ($_SERVER['REMOTE_ADDR'] == '62.245.254.82') { echo $strSqlPp .chr(10); print_r($arrSqlPp); die(); }

    $arrPayer = array();
    $arrPayerAccount = array();
    $strPremiumPayer = '';
    
    if (count($arrSqlPp) > 0) {
      foreach ($arrSqlPp as $arrPremiumPayer) {
        $arrPayer[] = '"' .$arrPremiumPayer['Premium_Payer__c'] .'"';
        $_REQUEST['strSelInf'] = $arrPremiumPayer['Information_provider__c'];
      }
    
      $strPremiumPayer = implode(',', $arrPayer);
      
      $strSqlPpA = 'SELECT `SF42_Comany_ID__c` FROM `Account` WHERE `Id` IN (' .$strPremiumPayer .')';
      $arrSqlPpA = MySQLStatic::Query($strSqlPpA);

      if (count($arrSqlPpA) > 0) {
        foreach ($arrSqlPpA as $arrPpAccount) {
          $arrPayerAccount[] = '"' .$arrPpAccount['SF42_Comany_ID__c'] .'"';
        }
        $strPpAccount = implode(',', $arrPayerAccount);
      }
      
    }
    
    if ($arrSqlDez[0]['CATCH_ALL__c'] == 'true') {
     
      $strPpAccount = '';
      
      $strSqlKK = 'SELECT * FROM `Anfragestelle__c` WHERE `Id`="' .$strAnfragestelleId .'"';
      $arrKK = MySQLStatic::Query($strSqlKK);
      
      //if ($_SERVER['REMOTE_ADDR'] == '62.245.254.82') { echo $strSqlKK .chr(10); print_r($arrKK); }
      
      $boolIsCatchAll = true;
      $arrCatchAll = $arrKK[0];
      
      $strSqlKK = 'SELECT * FROM `Account` WHERE `Id`="' .$arrKK[0]['Information_Provider__c'] .'"';
      $arrKK = MySQLStatic::Query($strSqlKK);

      //if ($_SERVER['REMOTE_ADDR'] == '62.245.254.82') { echo $strSqlKK .chr(10); print_r($arrKK); die(); }

      $strSql6 = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Beitragsmonat__c` = "' .$arrPeriod[0] .'" AND `Beitragsjahr__c` = "' .$arrPeriod[1] .'" ';
      if ($_REQUEST['strSelSta'] != '') {
        $strSql6.= 'AND (`SF42_EventStatus__c` = "' .$_REQUEST['strSelSta'] .'") ';
      }
      if ($_REQUEST['strSearchValue'] != '') {
        $strSql6.= 'AND `Name` LIKE "E-' .$_REQUEST['strSearchValue'] .'%" ';
      }
      $strSql6.= 'AND `SF42_informationProvider__c` = "' .$arrKK[0]['Id'] .'" GROUP BY `Betriebsnummer_ZA__c`';
      $arrEventList = MySQLStatic::Query($strSql6);
      
      //if ($_SERVER['REMOTE_ADDR'] == '62.245.254.82') { echo $strSql6 .chr(10); print_r($arrEventList); die(); }
      
      foreach ($arrEventList as $intEv => $arrEvent) {
        
        $strSqlAc = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c`="' .$arrEvent['Betriebsnummer_ZA__c'] .'"';
        $arrAc = MySQLStatic::Query($strSqlAc);

        //PremiumPayer gesuchtem Team zugeordnet
        $strSql7 = 'SELECT * FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrAc[0]['Id'] .'" AND `Information_Provider__c` = "' .$arrKK[0]['Id'] .'" AND `Anfragestelle__c` = "' .$strAnfragestelleId .'"';
        $arrResult7 = MySQLStatic::Query($strSql7);

        if (count($arrResult7) == 0) {
          
          $strSql8 = 'SELECT `Id` FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrAc[0]['Id'] .'" AND `Information_Provider__c` = "' .$arrKK[0]['Id'] .'"';
          $arrResult8 = MySQLStatic::Query($strSql8);
          
          //keinem Team zugeordnet
          if (count($arrResult8) == 0) {
            $arrPayerAccount[] = '"' .$arrEvent['Betriebsnummer_ZA__c'] .'"';
          }
          
        }
        
      }

      //if ($_SERVER['REMOTE_ADDR'] == '62.245.254.82') { echo 'here 2'; die(); }

      $strPpAccount = implode(',', $arrPayerAccount);
        
      if ($_SERVER['REMOTE_ADDR'] == '88.217.15.113') {
        // echo '|' .print_r($arrPayerAccount, true) . chr(10) .print_r($arrCatchAllPp, true); die();
      }          



      
    }
  
    $strSql = 'SELECT `evid`, `Id`, `Name`, `SF42_EventStatus__c`, `SF42_Premium_Payer__c`, `Status_Klaerung__c`, ';
    $strSql.= '`Betriebsnummer_IP__c`, `Betriebsnummer_ZA__c`, `SF42_informationProvider__c`, ';
    $strSql.= '`SF42_EventComment__c`, `Art_des_Dokuments__c`, `SF42_PublishingStatus__c`, `Sperrvermerk__c`, ';
    $strSql.= '`RecordTypeId`, `SF42_EventStatus__c`, `bis_am__c`, `group_id`, `Status_Klaerung__c`, `Stundung__c` ';
    $strSql.= 'FROM `SF42_IZSEvent__c` ';
    $strSql.= 'WHERE 1 ';
    
    if (($_REQUEST['strSelSta'] != 'all') && ($_REQUEST['strSelSta'] != '')) {
      $strSql.= 'AND (`SF42_EventStatus__c` = "' .$_REQUEST['strSelSta'] .'") ';
    }
    
    if (($_REQUEST['strSelPay'] != 'all') && ($_REQUEST['strSelPay'] != '')) {
      $strSql.= 'AND (`Betriebsnummer_ZA__c` = "' .$_REQUEST['strSelPay'] .'") ';
    }
    
    if (($_REQUEST['strSelGru'] != 'all') && ($_REQUEST['strSelGru'] != '')) {
      $strSql.= 'AND (`group_id` = "' .$_REQUEST['strSelGru'] .'") ';
    }
    
    if (($_REQUEST['strSelInf'] != '') && ($_REQUEST['strSelInf'] != 'all')) {
      $strSql.= 'AND ((`SF42_informationProvider__c` = "' .$arrKK[0]['Id'] .'") OR (`SF42_informationProvider__c` = "' .$_REQUEST['strSelInf'] .'")) ';
      //$strSql.= 'AND ((`SF42_informationProvider__c` = "' .$_REQUEST['strSelInf'] .'")) ';
    }

    if (($_REQUEST['strSelKla'] != '') && ($_REQUEST['strSelKla'] != 'all')) {
      if ($_REQUEST['strSelKla'] == 'empty') {
        $strSql.= 'AND (`Status_Klaerung__c` = "") ';
      } else {
        $strSql.= 'AND (`Status_Klaerung__c` = "' .$_REQUEST['strSelKla'] .'") ';
      }
    }

    if (($_REQUEST['strSelStu'] != '') && ($_REQUEST['strSelStu'] != 'all')) {
      if ($_REQUEST['strSelStu'] == 'empty') {
        $strSql.= 'AND (`Stundung__c` = "") ';
      } else {
        $strSql.= 'AND (`Stundung__c` = "' .$_REQUEST['strSelStu'] .'") ';
      }
    }

    if ($_REQUEST['strSearchValue'] != '') {
      $strSql.= 'AND `Name` LIKE "E-' .$_REQUEST['strSearchValue'] .'%" ';
    }

    $strSql.= 'AND (`Beitragsmonat__c` = "' .$arrPeriod[0] .'") AND (`Beitragsjahr__c` = "' .$arrPeriod[1] .'") ';
    
    /*
    if ($_REQUEST['strSelSta'] == 'enquired') {
      $strSql.= 'AND `RecordTypeId` = "01230000001Ao76AAC" '; //Review -> angefragt
    } elseif ($_REQUEST['strSelSta'] == 'OK') {
      $strSql.= 'AND `RecordTypeId` = "01230000001Ao75AAC" '; //Publication -> OK
    }
    */
    if (($_REQUEST['strSelRec'] != 'all') && ($_REQUEST['strSelRec'] != '')) {
      $strSql.= 'AND `RecordTypeId` = "' .$_REQUEST['strSelRec'] .'" '; 
    }
    
    if (strlen($strPpAccount) > 0) {
      $strSql.= 'AND `Betriebsnummer_ZA__c` IN (' .$strPpAccount .') '; //Betriebsnummer  
    }
    
    $strSql.= 'ORDER BY `Betriebsnummer_ZA__c` ASC';

    if ($_SERVER['REMOTE_ADDR'] == '46.244.171.73') {
      //echo $strSql;
    }
    
    //if ($_SERVER['REMOTE_ADDR'] == '62.245.254.82') { echo $strSql .chr(10); /* die(); */ }
    
  } else {
    
    if (strstr($_REQUEST['strSelInf'], 'da_') != false) {
      $_REQUEST['strSelInf'] = substr($_REQUEST['strSelInf'], 3);
      //$boolShowIp = true;
    }
  
    $strSql = 'SELECT `evid`, `Id`, `Name`, `SF42_EventStatus__c`, `SF42_Premium_Payer__c`, `Status_Klaerung__c`, ';
    $strSql.= '`Betriebsnummer_IP__c`, `Betriebsnummer_ZA__c`, `SF42_informationProvider__c`, ';
    $strSql.= '`SF42_EventComment__c`, `Art_des_Dokuments__c`, `SF42_PublishingStatus__c`, `Sperrvermerk__c`, `SF42_DocumentUrl__c`, ';
    $strSql.= '`RecordTypeId`, `SF42_EventStatus__c`, `bis_am__c`, `group_id`, `Status_Klaerung__c`, `Beitragsmonat__c`, `Beitragsjahr__c`, `Rueckmeldung_am__c`, `Stundung__c` ';
    $strSql.= 'FROM `SF42_IZSEvent__c` ';
    $strSql.= 'WHERE 1 ';
    
    if (($_REQUEST['strSelSta'] != 'all') && ($_REQUEST['strSelSta'] != '')) {
      $strSql.= 'AND(`SF42_EventStatus__c` = "' .$_REQUEST['strSelSta'] .'") ';
    }
    
    if (($_REQUEST['strSelPay'] != 'all') && ($_REQUEST['strSelPay'] != '')) {
      $strSql.= 'AND (`Betriebsnummer_ZA__c` = "' .$_REQUEST['strSelPay'] .'") ';
    }
    
    if (($_REQUEST['strSelGru'] != 'all') && ($_REQUEST['strSelGru'] != '')) {
      $strSql.= 'AND (`group_id` = "' .$_REQUEST['strSelGru'] .'") ';
    }
    
    if (($_REQUEST['strSelInf'] != '') && ($_REQUEST['strSelInf'] != 'all')) {
      $strSql.= 'AND (`SF42_informationProvider__c` = "' .$_REQUEST['strSelInf'] .'") ';
    }

    if (($_REQUEST['strSelKla'] != '') && ($_REQUEST['strSelKla'] != 'all')) {
      if ($_REQUEST['strSelKla'] == 'empty') {
        $strSql.= 'AND (`Status_Klaerung__c` = "") ';
      } else {
        $strSql.= 'AND (`Status_Klaerung__c` = "' .$_REQUEST['strSelKla'] .'") ';
      }
    }

    if (($_REQUEST['strSelStu'] != '') && ($_REQUEST['strSelStu'] != 'all')) {
      if ($_REQUEST['strSelStu'] == 'empty') {
        $strSql.= 'AND (`Stundung__c` = "") ';
      } else {
        $strSql.= 'AND (`Stundung__c` = "' .$_REQUEST['strSelStu'] .'") ';
      }
    }

    if ($_REQUEST['strSearchValue'] != '') {
      $strSql.= 'AND `Name` LIKE "E-' .$_REQUEST['strSearchValue'] .'%" ';
    }

    $strSql.= 'AND (`Beitragsmonat__c` LIKE "' .$arrPeriod[0] .'") AND (`Beitragsjahr__c` LIKE "' .$arrPeriod[1] .'") ';
    
    /*
    if ($_REQUEST['strSelSta'] == 'enquired') {
      $strSql.= 'AND `RecordTypeId` = "01230000001Ao76AAC" '; //Review -> angefragt
    } elseif ($_REQUEST['strSelSta'] == 'OK') {
      $strSql.= 'AND `RecordTypeId` = "01230000001Ao75AAC" '; //Publication -> OK
    }
    */
    
    if (($_REQUEST['strSelRec'] != 'all') && ($_REQUEST['strSelRec'] != '')) {
      $strSql.= 'AND `RecordTypeId` = "' .$_REQUEST['strSelRec'] .'" '; 
    }
    
    $strSql.= 'ORDER BY `Betriebsnummer_ZA__c` ASC';
    
  }

  $arrResult = MySQLStatic::Query($strSql);
  
  if (@$_SERVER['HTTP_FKLD'] == 'on') {
    //echo $strSql;
  }  

  //if ($_SERVER['REMOTE_ADDR'] == '62.245.254.82') { echo $strSql .chr(10); /* print_r($arrResult); die(); */ }
  
  $arrInformationProvider = array();
  
  if (count($arrResult) > 0) {
    
    $arrInformationProvider = $arrResult;
  
  }
  
  if (@$_SERVER['HTTP_FKLD'] == 'on') {
    //echo count($arrInformationProvider);
  }  
  
  
  //echo count($arrResult);
  //print_r($arrInformationProvider);
  //print_r($arrDecentral);
  
  $strExportTable = '';
  
  if (count($arrInformationProvider) > 0) {
    //natcasesort($arrInformationProvider);

    $strOutputTable = '';
    $strOutputTable.= '<table class="" id="5">' .chr(10);
    $strOutputTable.= '  <thead>' .chr(10);
    $strOutputTable.= '    <tr>' .chr(10);
  	$strOutputTable.= '      <th class="{sorter: false}">Status</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header">Event-ID</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="header">Company Group</th>' .chr(10);
  	if ($_REQUEST['strSelDate'] == '%_%') {
    	$strOutputTable.= '      <th class="header">Beitragsmonat</th>' .chr(10); //class="header"
  	}
  	$strOutputTable.= '      <th class="header">BNR IP</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="header">Information Provider</th>' .chr(10);
  	$strOutputTable.= '      <th class="header">BNR PP</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header">Premium Payer</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="header">DS-Typ</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="header">Wiederv.</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="header">Klärung</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="header">Rückm. am</th>' .chr(10); //class="header"
    $strOutputTable.= '    </tr>' .chr(10);
    $strOutputTable.= '  </thead>' .chr(10);
    $strOutputTable.= '  <tbody>' .chr(10);
    
    $strExportTable.= $strOutputTable;
    
    $arrPeriod = explode('_', $_REQUEST['strSelDate']);
    
    $strNext = '';
    
    //print_r($arrInformationProvider);
    
    $intCatchAllContinue = 0;

    foreach ($arrInformationProvider as $strId => $arrEvent) {
      
      if ($arrEvent['Status_Klaerung__c'] == 'in Klärung') {
        $strTrClass = 'red';
      } else {
        $strTrClass = '';
      }
      
      if ($strNext == 'blubb') {
        $strNext = $arrEvent['Id'];
      }
      
      if (($_REQUEST['done'] != '') && ($_REQUEST['done'] == $arrEvent['Id'])) {
        $strNext = 'blubb';
      }

      $strSqlAc = 'SELECT `Name`, `SF42_Comany_ID__c` FROM `Account` WHERE `Id` = "' .$arrEvent['SF42_informationProvider__c'] .'"';
      $arrSqlAc = MySQLStatic::Query($strSqlAc);

      $strSql2 = 'SELECT `Id` FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$arrEvent['Betriebsnummer_ZA__c'] .'"';
      $arrSql2 = MySQLStatic::Query($strSql2);

      $arrIp = arrGetInformationProvider($arrEvent['SF42_informationProvider__c'], $arrSql2[0]['Id']);

if (@$_SERVER['HTTP_FKLD'] == 'on') {
  //echo $arrIp['Name'] .': '; echo ($arrIp['Name'] != $strIpSelected) ?  'true' : 'false'; echo chr(10);
}      
      if ($_SERVER['REMOTE_ADDR'] == '1162.245.254.82') { 
        if ($boolIsCatchAll && ($strIpSelected != '') && ($arrIp['Name'] != $strIpSelected)) {
          //echo $strIpSelected .chr(10);
          if (!$arrIp['IsFallback']) {
            $intCatchAllContinue++;
            continue;
          }
        }  
      
      }


      $strOutputTable.= '    <tr class="' .$strTrClass .'" id="e_' .$arrEvent['Id'] .'">' .chr(10);
      $strOutputTable.= '      <td align="center"><img src="/assets/images/sys/status_' .$arrStatusCss[$arrEvent['SF42_EventStatus__c']] .'.png" /> ' .$arrEvent['SF42_EventStatus__c'] .'</td>' .chr(10);
      $strOutputTable.= '      <td><a target="_blank" href="/cms/index.php?ac=sear&det=' .$arrEvent['evid'] .'">' .$arrEvent['Name'] .'</a></td>' .chr(10);
      
      $strExportTable.= '    <tr>' .chr(10);
      $strExportTable.= '      <td></td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['Name'] .'</td>' .chr(10);

      $strSql = 'SELECT `Name` FROM `Group__c` WHERE `Id` = "' .$arrEvent['group_id'] .'"';
      $arrSql = MySQLStatic::Query($strSql);
      //$strOutputTable.= '      <td>' .$arrSql[0]['Name'] .'</td>' .chr(10);
      $strOutputTable.= '      <td><span class="hidden">' .$arrSql[0]['Name'] .'</span><a href="index_neu.php?ac=contacts&grid=' .$arrEvent['group_id'] .'" title="Zeige Kontakte" target="_blank">' .$arrSql[0]['Name'] .'</a></td>' .chr(10);
      $strExportTable.= '      <td>' .$arrSql[0]['Name'] .'</td>' .chr(10);

    	if ($_REQUEST['strSelDate'] == '%_%') {//
      	$strOutputTable.= '      <td>' .$arrEvent['Beitragsjahr__c'] .'-' .sprintf("%02d", $arrEvent['Beitragsmonat__c']) .'</td>' .chr(10); //class="header"
    	}
    	if ($_REQUEST['strSelDate'] == '%_%') {
      	$strExportTable.= '      <td>' .$arrEvent['Beitragsjahr__c'] .'-' .sprintf("%02d", $arrEvent['Beitragsmonat__c']) .'</td>' .chr(10); //class="header"
    	}

      $strOutputTable.= '      <td>' .$arrSqlAc[0]['SF42_Comany_ID__c'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrSqlAc[0]['SF42_Comany_ID__c'] .'</td>' .chr(10);
      
      $strOutputTable.= '      <td><span class="hidden">' .$arrIp['Name'] .'</span><a href="index_neu.php?ac=contacts&acid=' .$arrEvent['SF42_informationProvider__c'] .'" title="Zeige Kontakte" target="_blank">' .$arrIp['Name'] .'</a></td>' .chr(10);
      $strExportTable.= '      <td>' .$arrIp['Name'] .'</td>' .chr(10);

      $strOutputTable.= '      <td>' .$arrEvent['Betriebsnummer_ZA__c'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['Betriebsnummer_ZA__c'] .'</td>' .chr(10);
      
      
      $strOutputTable.= '      <td>' .$arrAllPpList[$arrEvent['Betriebsnummer_ZA__c']] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrAllPpList[$arrEvent['Betriebsnummer_ZA__c']] .'</td>' .chr(10);
      $strOutputTable.= '      <td>' .$arrRecordTypeFlip[$arrEvent['RecordTypeId']] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrRecordTypeFlip[$arrEvent['RecordTypeId']] .'</td>' .chr(10);
      
      if ($arrEvent['bis_am__c'] != '0000-00-00') {
        $arrDate = explode('-', $arrEvent['bis_am__c']);
        $strOutputTable.= '      <td>' .$arrDate[2] .'.' .$arrDate[1] .'.' .$arrDate[0] .'</td>' .chr(10);
        $strExportTable.= '      <td>' .$arrDate[2] .'.' .$arrDate[1] .'.' .$arrDate[0] .'</td>' .chr(10);
      } else {
        $strOutputTable.= '      <td></td>' .chr(10);
        $strExportTable.= '      <td></td>' .chr(10);
      }
      
      $strOutputTable.= '      <td>' .$arrEvent['Status_Klaerung__c'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['Status_Klaerung__c'] .'</td>' .chr(10);
      
      if ($arrEvent['Rueckmeldung_am__c'] != '0000-00-00') {
        $arrDate = explode('-', $arrEvent['Rueckmeldung_am__c']);
        $strOutputTable.= '      <td><span class="hidden">' .$arrEvent['Rueckmeldung_am__c'] .'</span>' .$arrDate[2] .'.' .$arrDate[1] .'.' .$arrDate[0] .'</td>' .chr(10);
        $strExportTable.= '      <td>' .$arrDate[2] .'.' .$arrDate[1] .'.' .$arrDate[0] .'</td>' .chr(10);
      } else {
        $strOutputTable.= '      <td></td>' .chr(10);
        $strExportTable.= '      <td></td>' .chr(10);
      }
      
      $strOutputTable.= '    </tr>' .chr(10);
      $strExportTable.= '    </tr>' .chr(10);
      
    }
      
    $strOutputTable.= '  </tbody>' .chr(10);
    $strOutputTable.= '</table>' .chr(10);

    $strExportTable.= '  </tbody>' .chr(10);
    $strExportTable.= '</table>' .chr(10);

if (@$_SERVER['HTTP_FKLD'] == 'on') {
  //echo $intCatchAllContinue;
}  
    
    $strOutput.= '<div class="clearfix" style="height: 48px; width: 1900px;">' .chr(10);
    $strOutput.= '  <div class="form-left">' .chr(10);
    $strOutput.= '    <h3>' .(count($arrInformationProvider) - $intCatchAllContinue) .' Ergebnisse gefunden:</h3>' .chr(10);
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' .chr(10);
    $strOutput.= '    <form method="post" action="_get_csv.php">';
    $strOutput.= '    <input type="hidden" name="strTable" value="' .str_replace('"', "'", $strExportTable) .'">' .chr(10);
    $strOutput.= '    <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' .chr(10);
    $strOutput.= '    </form>';
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '</div>' .chr(10);
    
    $strOutput.= $strOutputTable;
    
    if (($_REQUEST['strSelInf'] != 'all') && ($_REQUEST['strSelPay'] == 'all')) {
      $intSort = 5;
    } elseif (($_REQUEST['strSelInf'] == 'all') && (($_REQUEST['strSelPay'] != 'all') || ($_REQUEST['strSelGru'] != 'all'))) {
      $intSort = 3;
    } else {
      $intSort = 1;
    }
    
    $strOutput.= "
<script>

$('#5').fixheadertable({ 
   colratio    : [170, 80, 320, 80, 320, 80, 320, 80, 120, 160, 100], 
   height      : 700, 
   width       : 1860, 
   zebra       : true, 
   resizeCol   : true,
   sortable    : true,
   sortType    : ['string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string'],
   sortedColId : " .$intSort .", 
   dateFormat  : 'Y-m'
});
</script>
";
          
  } else {
    
    $strOutput.= '<p>Keine Premium Payer im gewählten Zeitraum vorhanden.</p>' .chr(10);
    
  }

}

$strOutput.= '';

}

?>