<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

include('conf/global.inc.php');
require_once('../assets/classes/class.mysql.php');
//require_once('../assets/classes/class.database.php');

$arrPie = array();

$strOutput = '';
$strOutput.= '<h1>Übersicht</h1>' .chr(10);

$startzeit = microtime(true); // Startzeit

$strSql0 = "SELECT * FROM `cron_izs_sv_month` ORDER BY `Beitragsjahr__c` DESC, `Beitragsmonat__c` DESC";
//$strOutput.= $strSql0;
$arrResult0 = MySQLStatic::Query($strSql0);
if (count($arrResult0) > 0) {
  
  $strOutput.= '<form method="post" action="index.php?ac=over">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);

  $strOutput.= '<table>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Zeitraum: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelDate" id="strSelDate" onchange="this.form.submit()">' .chr(10);
  
  $intPeriod = 0;

  foreach ($arrResult0 as $arrPeriod0) {
    $strSelected = '';
    $strValue = $arrPeriod0['Beitragsmonat__c'] .'_' .$arrPeriod0['Beitragsjahr__c'];
    
    if (empty($_REQUEST['strSelDate']) && ($intPeriod == 0)) {
      $_REQUEST['strSelDate'] = $strValue;
      //$_REQUEST['send'] = 1;
    }
    
    if ($strValue == $_REQUEST['strSelDate']) {
      $strSelected = ' selected="selected"';
      $intYear = $arrPeriod0['Beitragsjahr__c'];
      $intMonth = $arrPeriod0['Beitragsmonat__c'];
    } 
    $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .str_pad($arrPeriod0['Beitragsmonat__c'], 2 ,'0', STR_PAD_LEFT) .'-' .$arrPeriod0['Beitragsjahr__c'] .'</option>' .chr(10);
    $intPeriod++;
  }
  
  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);
  
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td></td>' .chr(10);
  $strOutput.= '    <td><input type="submit" value="anzeigen"></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);
    
}

/*
$stopzeit = microtime(true); // Stopzeit
$laufzeit = ($stopzeit-$startzeit)*1000; // Berechnung
$laufzeit = substr($laufzeit, 0, 5); // Auf 5 Stellen begrenzen
$laufzeit = str_replace(".", ",", $laufzeit);
$strOutput.= "Scriptlaufzeit: ".$laufzeit." Millisekunden"; // Ausgabe
$startzeit = microtime(true); // Startzeit
*/

$arrGroupData = array(
  'status'   => array(),
  'rueck'    => array(0 => 0, 1 => 0),
  'grund'    => array(),
  'sichtbar' => array(),
  'info'     => array('ok' => array(), 'notok'=> array(), 'all' => array(), 'sent' => array())
);

$arrGroupList = array();

//print_r($arrGroupData); die();

$strSql = 'SELECT *  FROM `SF42_IZSEvent__c` ';
$strSql.= 'WHERE `SF42_Year__c` = "' .$intYear .'" AND `SF42_Month__c` = "' .$intMonth .'" ';
$strSql.= 'AND `SF42_EventStatus__c` != "abgelehnt / Dublette" AND `SF42_EventStatus__c` != "abgelehnt / Frist" ';
$resResult = MySQLStatic::Query($strSql);

if (count($resResult) > 0) {
  foreach ($resResult as $arrAll) {

    //STATUS
    if (!isset($arrGroupData['status'][$arrAll['SF42_EventStatus__c']])) {
      $arrGroupData['status'][$arrAll['SF42_EventStatus__c']] = 0;
    }
    $arrGroupData['status'][$arrAll['SF42_EventStatus__c']]++;

    //RÜCKMELDUNG
    if (in_array($arrAll['RecordTypeId'], array('01230000001Ao75AAC', '01230000001Ao76AAC'))) {
      if (($arrAll['SF42_EventStatus__c'] == 'enquired') && (($arrAll['Rueckmeldung_am__c'] == '0000-00-00') || ($arrAll['Rueckmeldung_am__c'] == ''))) {
        $arrGroupData['rueck'][0]++;
      } elseif (($arrAll['SF42_EventStatus__c'] != 'abgelehnt / Dublette') && ($arrAll['Rueckmeldung_am__c'] != '000-00-00') && ($arrAll['Rueckmeldung_am__c'] != '')) {
        $arrGroupData['rueck'][1]++;
      }
    }
    if ($arrAll['SF42_EventStatus__c'] == 'zurückgestellt von IZS') {
      $arrGroupData['rueck'][1]++;
    }

    //GRUND
    if ($arrAll['Status_Klaerung__c'] == 'in Klärung') {
      if (!isset($arrGroupData['grund'][$arrAll['Grund__c']])) {
        $arrGroupData['grund'][$arrAll['Grund__c']] = 0;
      }
      $arrGroupData['grund'][$arrAll['Grund__c']]++;
    }

    //SICHTBAR
    if (($arrAll['RecordTypeId'] == '01230000001Ao75AAC') && ($arrAll['SF42_PublishingStatus__c'] != 'online')) {
      if (!isset($arrGroupData['sichtbar'][$arrAll['SF42_PublishingStatus__c']])) {
        $arrGroupData['sichtbar'][$arrAll['SF42_PublishingStatus__c']] = 0;
      }
      $arrGroupData['sichtbar'][$arrAll['SF42_PublishingStatus__c']]++;
    }

    //INFO
    if (!isset($arrGroupData['info']['all'][$arrAll['group_id']])) {
      $arrGroupData['info']['all'][$arrAll['group_id']] = 0;
    }
    $arrGroupData['info']['all'][$arrAll['group_id']]++;

    if (in_array($arrAll['SF42_EventStatus__c'], array("to enquire", "enquired", "no result", "not OK"))) {
      if (!isset($arrGroupData['info']['notok'][$arrAll['group_id']])) {
        $arrGroupData['info']['notok'][$arrAll['group_id']] = 0;
      }
      $arrGroupData['info']['notok'][$arrAll['group_id']]++;   
      unset($arrGroupData['info']['ok'][$arrAll['group_id']]);
    } elseif (($arrAll['SF42_EventStatus__c'] == 'OK') && (!isset($arrGroupData['info']['notok'][$arrAll['group_id']]))) {
      if (!isset($arrGroupData['info']['ok'][$arrAll['group_id']])) {
        $arrGroupData['info']['ok'][$arrAll['group_id']] = 0;
      }
      $arrGroupData['info']['ok'][$arrAll['group_id']]++;     
    }

  }
}

ksort($arrGroupData['status']); // SETS "OK" FIRST

$strSql = 'SELECT `cc_group_id` FROM `cms_cert` WHERE `cc_sent` != "0000-00-00 00:00:00" AND `cc_month` = "' .$intMonth .'" AND `cc_year` = "' .$intYear .'"';
$arrCert = MySQLStatic::Query($strSql);
if (count($arrCert) > 0) {
  foreach ($arrCert as $intKey => $arrGroup) {
    $arrGroupData['info']['sent'][$arrGroup['cc_group_id']] = 1;
  };
}

//print_r($arrGroupData);

$arrGroupData['info']['all'] = count($arrGroupData['info']['all']);
$arrGroupData['info']['ok'] = count($arrGroupData['info']['ok']);
$arrGroupData['info']['notok'] = count($arrGroupData['info']['notok']);
$arrGroupData['info']['sent'] = count($arrGroupData['info']['sent']);

//print_r($arrGroupData); 

/*
$stopzeit = microtime(true); // Stopzeit
$laufzeit = ($stopzeit-$startzeit)*1000; // Berechnung
$laufzeit = substr($laufzeit, 0, 5); // Auf 5 Stellen begrenzen
$laufzeit = str_replace(".", ",", $laufzeit);
$strOutput.= "Scriptlaufzeit: ".$laufzeit." Millisekunden"; // Ausgabe
$startzeit = microtime(true); // Startzeit
*/


$arrChangePreview = array ( 
  'abgelehnt / Dublette' => 'abgelehnt / Dublette', 
  'abgelehnt / Frist' => 'abgelehnt / Frist', 
  'accepted' => 'angenommen', 
  'bereit für REVIEW' => 'bereit für REVIEW', 
  'enquired' => 'angefragt', 
  'in progress' => 'in Bearbeitung', 
  'new' => 'neu', 
  'no Feedback' => 'keine Antwort', 
  'no result' => 'NO RESULT', 
  'not assignable' => 'nicht zuordenbar', 
  'not OK' => 'NOT OK', 
  'OK' => 'OK', 
  'refused' => 'abgelehnt', 
  'to clear' => 'zu klären', 
  'to enquire' => 'anzufragen', 
  'zugeordnet / abgelegt' => 'zugeordnet / abgelegt', 
  'zurückgestellt von IZS' => 'zurückgestellt von IZS',
  'gestundet' => 'gestundet'
);

$arrGrundAnz = $arrGrund;
$arrGrundAnz[] = array('keine Angabe' => 'keine Angabe');

$arrSichtbar = array (
  'online' => 'ja',
  'private' => 'nein',
  'ready' => 'auf Anfrage'  
);


$strOutput.= '    <script type="text/javascript" src="https://www.google.com/jsapi"></script>' .chr(10);


$strSqlR = "SELECT * FROM `cms_right` WHERE `cr_cl_id` = '" .$_SESSION['id'] ."' AND `cr_cp_id` = '2' AND `cr_access` = 1";
$arrResultR = MySQLStatic::Query($strSqlR);

if (count($arrResultR) > 0) {
  
  $arrPie[] = 1;

  
  $arrChangePreviewColor = array ( 
    'accepted' => 'white', 
    'bereit für REVIEW' => 'white', 
    'enquired' => 'orange', 
    'in progress' => 'white', 
    'new' => 'white', 
    'no Feedback' => 'purple', 
    'no result' => 'white', 
    'not assignable' => 'white', 
    'not OK' => 'red', 
    'OK' => 'green', 
    'refused' => 'white', 
    'to clear' => 'white', 
    'to enquire' => 'white', 
    'zugeordnet / abgelegt' => 'white', 
    'zurückgestellt von IZS' => 'gray',
    'gestundet' => 'purple'
  );
  
  
  $strData = '';
  $strColor = '';
  $intColor = 0;
  $intSum1  = 0;
  if (count($arrGroupData['status']) > 0) {
    foreach ($arrGroupData['status'] as $strKey => $strValue) {
      $strData.= '          [\'' .$arrChangePreview[$strKey] .' (' .$strValue .')\',     ' .$strValue .'],' .chr(10);
      $strColor.= '                         ' .$intColor .': { color: \'' .$arrChangePreviewColor[$strKey] .'\' }, ' .chr(10);
      $intSum1+= $strValue;
      $intColor++;
    }
  }
  $strData = substr($strData, 0, -2);
  $strColor = substr($strColor, 0, -3);
  
  $strOutput.= '    <script type="text/javascript">' .chr(10);
  $strOutput.= '      google.load("visualization", "1", {packages:["corechart"]});' .chr(10);
  $strOutput.= '      google.setOnLoadCallback(drawChart1);' .chr(10);
        
  $strOutput.= '      var Status1 = new Object();' .chr(10);
  $arrChangePreviewFlip = array_flip($arrChangePreview);
  foreach ($arrChangePreviewFlip as $strKey => $strValue) {
    $strOutput.= '      Status1["' .$strKey .'"] = "' .$strValue .'";' .chr(10);
  }

  $strOutput.= '      function drawChart1() {' .chr(10);
  $strOutput.= '        var data = google.visualization.arrayToDataTable([' .chr(10);
  $strOutput.= '          [\'Grund\', \'Anzahl\'],' .chr(10);
  $strOutput.= $strData .chr(10);
  $strOutput.= '        ]);' .chr(10);

  $strOutput.= '        var options = { pieSliceText: \'value\', ' .chr(10);
  $strOutput.= '                        title: \'Alle Events (' .$intSum1 .')\', ' .chr(10);
  $strOutput.= '                        backgroundColor: \'transparent\', ' .chr(10);
  $strOutput.= '                        slices: {' .chr(10);
  $strOutput.= $strColor .chr(10);
  $strOutput.= '                        }, ' .chr(10);
  $strOutput.= '                        sliceVisibilityThreshold: 1/100000 ' .chr(10);
  $strOutput.= '        };' .chr(10);

  $strOutput.= '        var chart = new google.visualization.PieChart(document.getElementById(\'piechart1\'));' .chr(10);

  $strOutput.= '        function selectHandler1() {' .chr(10);
  $strOutput.= '          var selectedItem = chart.getSelection()[0];' .chr(10);
  $strOutput.= '          if (selectedItem) {' .chr(10);
  $strOutput.= '            var topping = data.getValue(selectedItem.row, 0).match(/.*[^\ \(\d*\)]/);' .chr(10);
  $strOutput.= '            var url = "/cms/index.php?ac=sear&strSelDate=' .$intMonth .'_' .$intYear .'&strSelSta=" + Status1[topping[0]] + "&send=1";' .chr(10);
  $strOutput.= '            window.location = url;' .chr(10);
  $strOutput.= '          }' .chr(10);
  $strOutput.= '        } ' .chr(10);

  $strOutput.= '        google.visualization.events.addListener(chart, \'select\', selectHandler1); ' .chr(10);    

  $strOutput.= '        chart.draw(data, options);' .chr(10);
  $strOutput.= '      }' .chr(10);
  $strOutput.= '    </script>' .chr(10);
}  



$strSqlR = "SELECT * FROM `cms_right` WHERE `cr_cl_id` = '" .$_SESSION['id'] ."' AND `cr_cp_id` = '3' AND `cr_access` = 1";
$arrResultR = MySQLStatic::Query($strSqlR);

if (count($arrResultR) > 0) {
  
  $arrPie[] = 2;
  
  /*
  $strSql = 'SELECT COUNT(*) AS `Rueck`  FROM `SF42_IZSEvent__c` WHERE ';
  $strSql.= '`RecordTypeId` IN ("01230000001Ao75AAC","01230000001Ao76AAC") AND ';
  $strSql.= '`SF42_Month__c` = "' .$intMonth .'" AND `SF42_Year__c` = "' .$intYear .'" ';
  $strSql.= 'AND ((`Rueckmeldung_am__c` = "0000-00-00") || (`Rueckmeldung_am__c` = "")) ';
  $strSql.= 'AND `SF42_EventStatus__c` IN ("enquired") ';
  $resResult1 = MySQLStatic::Query($strSql);
  
  $strSql = 'SELECT COUNT(*) AS `Rueck`  FROM `SF42_IZSEvent__c` WHERE ';
  $strSql.= '`RecordTypeId` IN ("01230000001Ao75AAC","01230000001Ao76AAC") AND ';
  $strSql.= '`SF42_Month__c` = "' .$intMonth .'" AND `SF42_Year__c` = "' .$intYear .'" ';
  $strSql.= 'AND ((`Rueckmeldung_am__c` != "0000-00-00") && (`Rueckmeldung_am__c` != "")) ';
  $strSql.= 'AND `SF42_EventStatus__c` != "abgelehnt / Dublette" ';
  $resResult2 = MySQLStatic::Query($strSql);
  */
  
  $strData = '';
  $intSum  = 0;
  if (is_array($arrGroupData['rueck'])) {
    $strData.= '          [\'erhalten (' .$arrGroupData['rueck'][1] .')\',     ' .$arrGroupData['rueck'][1]  .'],' .chr(10);
    $strData.= '          [\'fehlt (' .$arrGroupData['rueck'][0] .')\',     ' .$arrGroupData['rueck'][0] .']' .chr(10);
    $intSum = $arrGroupData['rueck'][0] + $arrGroupData['rueck'][1] ;
  }

  $strOutput.= '    <script type="text/javascript">' .chr(10);
  $strOutput.= '      google.load("visualization", "1", {packages:["corechart"]});' .chr(10);
  $strOutput.= '      google.setOnLoadCallback(drawChart2);' .chr(10);

  $strOutput.= '      var Status2 = new Object();' .chr(10);
  $strOutput.= '      Status2["fehlt"] = "1";' .chr(10);
  $strOutput.= '      Status2["erhalten"] = "0";' .chr(10);
        
  $strOutput.= '      function drawChart2() {' .chr(10);
  $strOutput.= '        var data = google.visualization.arrayToDataTable([' .chr(10);
  $strOutput.= '          [\'Ergebnis\', \'Anzahl\'],' .chr(10);
  $strOutput.= $strData;
  $strOutput.= '        ]);' .chr(10);

  $strOutput.= '        var options = { pieSliceText: \'value\', ' .chr(10);
  $strOutput.= '                        title: \'Rückmeldung (' .$intSum .')\', ' .chr(10);
  $strOutput.= '                        backgroundColor: \'transparent\', ' .chr(10);
  $strOutput.= '                        slices: {' .chr(10);
  $strOutput.= '                         0: { color: \'green\' }, ' .chr(10);
  $strOutput.= '                         1: { color: \'orange\' } ' .chr(10);
  $strOutput.= '                        }, ' .chr(10);
  $strOutput.= '                        sliceVisibilityThreshold: 1/100000 ' .chr(10);
  $strOutput.= '        };' .chr(10);

  $strOutput.= '        var chart = new google.visualization.PieChart(document.getElementById(\'piechart2\'));' .chr(10);

  $strOutput.= '        function selectHandler2() {' .chr(10);
  $strOutput.= '          var selectedItem = chart.getSelection()[0];' .chr(10);
  $strOutput.= '          if (selectedItem) {' .chr(10);
  $strOutput.= '            var topping = data.getValue(selectedItem.row, 0).match(/.*[^\ \(\d*\)]/);' .chr(10);
  $strOutput.= '            var url = "/cms/index.php?ac=retu&strSelDate=' .$intMonth .'_' .$intYear .'&outstanding=" + Status2[topping[0]] + "&send=1";' .chr(10);
  $strOutput.= '            window.location = url;' .chr(10);
  $strOutput.= '            //alert(url);' .chr(10);
  $strOutput.= '          }' .chr(10);
  $strOutput.= '        } ' .chr(10);

  $strOutput.= '        google.visualization.events.addListener(chart, \'select\', selectHandler2); ' .chr(10);    

  $strOutput.= '        chart.draw(data, options);' .chr(10);
  $strOutput.= '      }' .chr(10);
  $strOutput.= '    </script>' .chr(10);
}

  
$strSqlR = "SELECT * FROM `cms_right` WHERE `cr_cl_id` = '" .$_SESSION['id'] ."' AND `cr_cp_id` = '6' AND `cr_access` = 1";
$arrResultR = MySQLStatic::Query($strSqlR);

if (count($arrResultR) > 0) {
  
  $arrPie[] = 3;

  /*
  //Klärung
  $strSql = 'SELECT COUNT(*) AS "Anz", `Grund__c` FROM `SF42_IZSEvent__c` WHERE ';
  $strSql.= '`SF42_Month__c` = "' .$intMonth .'" AND `SF42_Year__c` = "' .$intYear .'" ';
  $strSql.= 'AND `Status_Klaerung__c` = "in Klärung" ';
  $strSql.= 'GROUP BY `Grund__c` ORDER BY `Grund__c`';
  $resResult = MySQLStatic::Query($strSql);
  */
  
  $strData = '';
  $strColor = '';
  $intColor = 0;
  $intSum  = 0;
  foreach ($arrGroupData['grund'] as $strGrund => $intAnzahl) {
    if ($strGrund == '') {
      $strGrund = 'keine Angabe';
    }
    $strData.= '          [\'' .$strGrund .' (' .$intAnzahl .')\', ' .$intAnzahl .'], ' .chr(10);
    $strColor.= '                         ' .$intColor .': { color: \'' .$arrGrundColor[$strGrund] .'\' }, ' .chr(10);
    $intSum+= $intAnzahl;
    $intColor++;
  }
  $strData = substr($strData, 0, -2);
  $strColor = substr($strColor, 0, -3);

  $strOutput.= '    <script type="text/javascript">' .chr(10);
  $strOutput.= '      google.load("visualization", "1", {packages:["corechart"]});' .chr(10);
  $strOutput.= '      google.setOnLoadCallback(drawChart3);' .chr(10);

  $strOutput.= '      function drawChart3() {' .chr(10);
  $strOutput.= '        var data = google.visualization.arrayToDataTable([' .chr(10);
  $strOutput.= '          [\'Ergebnis\', \'Anzahl\'],' .chr(10);
  $strOutput.= $strData;
  $strOutput.= '        ]);' .chr(10);

  $strOutput.= '        var options = { pieSliceText: \'value\', ' .chr(10);
  $strOutput.= '                        title: \'In Klärung (' .$intSum .')\', ' .chr(10);
  $strOutput.= '                        backgroundColor: \'transparent\', ' .chr(10);
  $strOutput.= '                        slices: {' .chr(10);
  $strOutput.= $strColor .chr(10);
  $strOutput.= '                        }, ' .chr(10);
  $strOutput.= '                        sliceVisibilityThreshold: 1/100000 ' .chr(10);
  $strOutput.= '        };' .chr(10);

  $strOutput.= '        var chart = new google.visualization.PieChart(document.getElementById(\'piechart3\'));' .chr(10);

  $strOutput.= '        function selectHandler3() {' .chr(10);
  $strOutput.= '          var selectedItem = chart.getSelection()[0];' .chr(10);
  $strOutput.= '          if (selectedItem) {' .chr(10);
  $strOutput.= '            var topping = data.getValue(selectedItem.row, 0).match(/.*[^\ \(\d*\)]/);' .chr(10);
  $strOutput.= '            var url = "/cms/index.php?ac=clea&strSelDate=' .$intMonth .'_' .$intYear .'&Grund2=" + topping[0] + "&send=1";' .chr(10);
  $strOutput.= '            window.location = url;' .chr(10);
  $strOutput.= '            //alert(url);' .chr(10);
  $strOutput.= '          }' .chr(10);
  $strOutput.= '        } ' .chr(10);

  $strOutput.= '        google.visualization.events.addListener(chart, \'select\', selectHandler3); ' .chr(10);    

  $strOutput.= '        chart.draw(data, options);' .chr(10);
  $strOutput.= '      }' .chr(10);
  $strOutput.= '    </script>' .chr(10);
} 


$strSqlR = "SELECT * FROM `cms_right` WHERE `cr_cl_id` = '" .$_SESSION['id'] ."' AND `cr_cp_id` = '5' AND `cr_access` = 1";
$arrResultR = MySQLStatic::Query($strSqlR);

if (count($arrResultR) > 0) {
  
  $arrPie[] = 4;

  /*
  //Dokumente sichtbar
  $strSql = 'SELECT COUNT(*) AS "Anz", `SF42_PublishingStatus__c` FROM `SF42_IZSEvent__c` WHERE ';
  $strSql.= '`SF42_Month__c` = "' .$intMonth .'" AND `SF42_Year__c` = "' .$intYear .'" ';
  $strSql.= 'AND `RecordTypeId` = "01230000001Ao75AAC" AND `SF42_PublishingStatus__c` != "online" ';
  $strSql.= 'GROUP BY `SF42_PublishingStatus__c` ORDER BY `SF42_PublishingStatus__c` DESC';
  $resResult = MySQLStatic::Query($strSql);
  */
  
  $strData = '';
  $intSum  = 0;
  foreach ($arrGroupData['sichtbar'] as $strSichtbar => $intAnzahl) {
    $strData.= '          [\'' .$arrSichtbar[$strSichtbar] .' (' .$intAnzahl .')\', ' .$intAnzahl .'], ' .chr(10);
    $intSum+= $intAnzahl;
  }
  $strData = substr($strData, 0, -3);

  $strOutput.= '    <script type="text/javascript">' .chr(10);
  $strOutput.= '      google.load("visualization", "1", {packages:["corechart"]});' .chr(10);
  $strOutput.= '      google.setOnLoadCallback(drawChart4);' .chr(10);

  //Auf Anfrage = Gelb 
  //Sichtbar = Grün 
  //Nicht sichtbar = Rot
        
  $strOutput.= '      function drawChart4() {' .chr(10);
  $strOutput.= '        var data = google.visualization.arrayToDataTable([' .chr(10);
  $strOutput.= '          [\'Ergebnis\', \'Anzahl\'],' .chr(10);
  $strOutput.= $strData .chr(10);
  $strOutput.= '        ]);' .chr(10);

  $strOutput.= '        var options = { pieSliceText: \'value\', ' .chr(10);
  $strOutput.= '                        title: \'Dokumente sichtbar (' .$intSum .')\', ' .chr(10);
  $strOutput.= '                        backgroundColor: \'transparent\', ' .chr(10);
  $strOutput.= '                        slices: {' .chr(10);
  $strOutput.= '                         0: { color: \'orange\' }, ' .chr(10);
  $strOutput.= '                         1: { color: \'red\' } ' .chr(10);
  //$strOutput.= '                         2: { color: \'yellow\' }' .chr(10);
  $strOutput.= '                        }, ' .chr(10);
  $strOutput.= '                        sliceVisibilityThreshold: 1/100000 ' .chr(10);
  $strOutput.= '        };' .chr(10);

  $strOutput.= '        var chart = new google.visualization.PieChart(document.getElementById(\'piechart4\'));' .chr(10);

  $strOutput.= '        function selectHandler4() {' .chr(10);
  $strOutput.= '          var selectedItem = chart.getSelection()[0];' .chr(10);
  $strOutput.= '          if (selectedItem) {' .chr(10);
  $strOutput.= '            var topping = data.getValue(selectedItem.row, 0).match(/.*[^\ \(\d*\)]/);' .chr(10);
  $strOutput.= '            var url = "/cms/index.php?ac=lock&strSelDate=' .$intMonth .'_' .$intYear .'&send=1";' .chr(10);
  $strOutput.= '            window.location = url;' .chr(10);
  $strOutput.= '            //alert(url);' .chr(10);
  $strOutput.= '          }' .chr(10);
  $strOutput.= '        } ' .chr(10);

  $strOutput.= '        google.visualization.events.addListener(chart, \'select\', selectHandler4); ' .chr(10);    

  $strOutput.= '        chart.draw(data, options);' .chr(10);
  $strOutput.= '      }' .chr(10);
  $strOutput.= '    </script>' .chr(10);
}


  
$strSqlR = "SELECT * FROM `cms_right` WHERE `cr_cl_id` = '" .$_SESSION['id'] ."' AND `cr_cp_id` = '14' AND `cr_access` = 1";
$arrResultR = MySQLStatic::Query($strSqlR);

if (count($arrResultR) > 0) {
  
  $arrPie[] = 5;

  //Infoservice
  $arrGroups = array();
  $arrNotPayedGroup = array();
  $arrPayedGroup = array();
  $arrSentGroup = array();
  $arrAllGroup = array();

  /*
  $strSql2 = 'SELECT `SF42_IZSEvent__c`.*, `Acc`.`SF42_Company_Group__c`, `Acc`.`SF42_Comany_ID__c` FROM `SF42_IZSEvent__c` ';
  $strSql2.= 'INNER JOIN `Account` AS `Acc` ON `Acc`.`SF42_Comany_ID__c` = `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c` ';
  $strSql2.= 'WHERE `SF42_Month__c` = "' .$intMonth .'" AND `SF42_Year__c` = "' .$intYear .'" ';
  $strSql2.= 'AND `SF42_EventStatus__c` IN ("to enquire", "enquired", "no result", "not OK", "OK")';
  $arrResult2 = MySQLStatic::Query($strSql2);
  
  if (count($arrResult2) > 0) {

    foreach ($arrResult2 as $intEventKey => $arrEvent) {
      
      if (in_array($arrEvent['SF42_EventStatus__c'], array("to enquire", "enquired", "no result", "not OK")) ) {
        if (!in_array($arrEvent['SF42_Company_Group__c'], $arrNotPayedGroup)) {
          $arrNotPayedGroup[] = $arrEvent['SF42_Company_Group__c'];
        }
      }
      if (!in_array($arrEvent['SF42_Company_Group__c'], $arrAllGroup)) {
        $arrAllGroup[] = $arrEvent['SF42_Company_Group__c'];
      }

    }
  }
  
  $arrPayedGroup = array_diff($arrAllGroup, $arrNotPayedGroup);

  
  if (count($arrPayedGroup) > 0) {

    foreach ($arrPayedGroup as $intKey => $strGroup) {

      $strSql = 'SELECT `cc_id` FROM `cms_cert` WHERE `cc_sent` != "0000-00-00 00:00:00" AND `cc_group_id` = "' .$strGroup .'" AND `cc_month` = "' .$intMonth .'" AND `cc_year` = "' .$intYear .'"';
      $arrCert = MySQLStatic::Query($strSql);
       
      if (count($arrCert) != 0) {
        $arrSentGroup[] = $strGroup;
        unset($arrPayedGroup[$intKey]);
      }
      
    }
  }

$arrGroupData = array(
  'status'   => array(),
  'rueck'    => array(0 => 0, 1 => 0),
  'grund'    => array(),
  'sichtbar' => array(),
  'info'     => array('ok' => array(), 'notok'=> array(), 'all' => array(), 'sent' => array())
);

  */

  $strData = '';
  $intSum  = 0;
  
  $arrVersand = array(
    'Noch nicht fertig' => 'Noch nicht fertig',
    'Versendbar' => 'Versendbar',
    'Versandt' => 'Versandt'
  );
  
  $strData.= '          [\'Noch nicht fertig (' .$arrGroupData['info']['notok'] .')\', ' .$arrGroupData['info']['notok'] .'], ' .chr(10);
  $strData.= '          [\'Versendbar (' .($arrGroupData['info']['ok'] - $arrGroupData['info']['sent']) .')\', ' .($arrGroupData['info']['ok'] - $arrGroupData['info']['sent']) .'], ' .chr(10);
  $strData.= '          [\'Versandt (' .$arrGroupData['info']['sent'] .')\', ' .$arrGroupData['info']['sent'] .'], ' .chr(10);
  
  $intSum = $arrGroupData['info']['all'];

  $strOutput.= '    <script type="text/javascript">' .chr(10);
  $strOutput.= '      google.load("visualization", "1", {packages:["corechart"]});' .chr(10);
  $strOutput.= '      google.setOnLoadCallback(drawChart5);' .chr(10);

  $strOutput.= '      var Status5 = new Object();' .chr(10);
  foreach ($arrVersand as $strKey => $strValue) {
    $strOutput.= '      Status5["' .$strKey .'"] = "' .$strValue .'";' .chr(10);
  }
        
  $strOutput.= '      function drawChart5() {' .chr(10);
  $strOutput.= '        var data = google.visualization.arrayToDataTable([' .chr(10);
  $strOutput.= '          [\'Ergebnis\', \'Anzahl\'],' .chr(10);
  $strOutput.= $strData .chr(10);
  $strOutput.= '        ]);' .chr(10);

  $strOutput.= '        var options = { pieSliceText: \'value\', ' .chr(10);
  $strOutput.= '                        title: \'Infoservice (' .$intSum .')\', ' .chr(10);
  $strOutput.= '                        backgroundColor: \'transparent\', ' .chr(10);
  $strOutput.= '                        slices: {' .chr(10);
  $strOutput.= '                         0: { color: \'gray\' }, ' .chr(10);
  $strOutput.= '                         1: { color: \'orange\' }, ' .chr(10);
  $strOutput.= '                         2: { color: \'green\' }' .chr(10);
  $strOutput.= '                        }, ' .chr(10);
  $strOutput.= '                        sliceVisibilityThreshold: 1/100000 ' .chr(10);
  $strOutput.= '                      };' .chr(10);

  $strOutput.= '        var chart = new google.visualization.PieChart(document.getElementById(\'piechart5\'));' .chr(10);

  $strOutput.= '        function selectHandler5() {' .chr(10);
  $strOutput.= '          var selectedItem = chart.getSelection()[0];' .chr(10);
  $strOutput.= '          if (selectedItem) {' .chr(10);
  $strOutput.= '            var topping = data.getValue(selectedItem.row, 0).match(/.*[^\ \(\d*\)]/);' .chr(10);
  $strOutput.= '            var url = "/cms/index.php?ac=cert&strSelDate=' .$intMonth .'_' .$intYear .'&Grund=" + Status5[topping[0]] + "&send=1";' .chr(10);
  $strOutput.= '            window.location = url;' .chr(10);
  $strOutput.= '            //alert(url);' .chr(10);
  $strOutput.= '          }' .chr(10);
  $strOutput.= '        } ' .chr(10);

  $strOutput.= '        google.visualization.events.addListener(chart, \'select\', selectHandler5); ' .chr(10);    

  $strOutput.= '        chart.draw(data, options);' .chr(10);
  $strOutput.= '      }' .chr(10);
  $strOutput.= '    </script>' .chr(10);
}

/*

*/

if (count($arrPie) > 0) {
  
  foreach ($arrPie as $intKey => $intPieNo) {
  
    $strOutput.= '    <div id="piechart' .$intPieNo .'" style="width: 570px; height: 300px; float: left;"></div>' .chr(10);
    
    if (($intKey != 0) && ((($intKey + 1) % 2) == 0)) {
      $strOutput.= '    <p class="clearfix"></p>' .chr(10);
    }
  
  }

}    

?>
