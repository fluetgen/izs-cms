<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

require_once('../assets/classes/class.mysql.php');
require_once('../assets/classes/class.database.php');

$strFirstPreview = '';
$intId = 1;

$strDir01 = 'events/upload01';
$strDir01Scan = array_diff(scandir($strDir01), array('..', '.'));

$strDir02 = 'events/upload02';
$strDir02Scan = array_diff(scandir($strDir02), array('..', '.'));

$strDir03 = 'events/upload03';
$strDir03Scan = array_diff(scandir($strDir03), array('..', '.'));

$strDir04 = 'events/upload04';
$strDir04Scan = array_diff(scandir($strDir04), array('..', '.'));

$strOutput = '';
$arrRequestWay = array();
$arrDecentral = array();

$arrRecordType = array (
  'Delivery' => '01230000001Ao73AAC',
  'FollowUp' => '01230000001Ao74AAC',
  'Publication' => '01230000001Ao75AAC',
  'Review' => '01230000001Ao76AAC'
);

$arrDokArt = array (
  'Kontoauskunft' => 'Form',
  'Kontoauszug' => 'KtoA',
  'Unbedenklichkeitsbescheinigung' => 'UBB',
  'Sonstiges' => 'Sonst.'
);

$arrChangePreview = array ( 
  'OK' => 'OK', 
  'not OK' => 'nicht OK', 
  'no Feedback' => 'keine Auskunft'
);

$arrSperr = array (
  'true' => 'ja',
  'false' => 'nein'
);

$arrSichtbar = array (
  'online' => 'ja',
  'private' => 'nein',
  'ready' => 'auf Anfrage'  
);

$strOutput.= '<h1>Review</h1>' .chr(10);

$strSql0 = "SELECT DISTINCT `Beitragsmonat__c`, `Beitragsjahr__c` FROM `SF42_IZSEvent__c` ORDER BY `Beitragsjahr__c` DESC, `Beitragsmonat__c` DESC";
$arrResult0 = MySQLStatic::Query($strSql0);
if (count($arrResult0) > 0) {
  
  $strOutput.= '<form method="post" action="" onSubmit="if ($(\'#strSelInf\').val() == \'\') { alert(\'Bitte wähle einen Information Provider.\'); return false;}">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);

  $strOutput.= '<table>' .chr(10);
  $strOutput.= '<tbody>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Zeitraum: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelDate" id="strSelDate">' .chr(10);
  
  $intPeriod = 0;

  foreach ($arrResult0 as $arrPeriod0) {
    $strSelected = '';
    $strValue = $arrPeriod0['Beitragsmonat__c'] .'_' .$arrPeriod0['Beitragsjahr__c'];
    
    if (empty($_REQUEST['strSelDate']) && ($intPeriod == 0)) {
      $_REQUEST['strSelDate'] = $strValue;
      //$_REQUEST['send'] = 1;
    }
    
    if ($strValue == $_REQUEST['strSelDate']) {
      $strSelected = ' selected="selected"';
    } 
    $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .str_pad($arrPeriod0['Beitragsmonat__c'], 2 ,'0', STR_PAD_LEFT) .'-' .$arrPeriod0['Beitragsjahr__c'] .'</option>' .chr(10);
    $intPeriod++;
  }
  
  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);


  $arrStatusCss = array ( 
    'OK' => 'thick', 
    'not OK' => 'warning', 
    'enquired' => 'wait', 
    'no Feedback' => 'question'
  );

  $arrStatus = array ( 
    'OK' => 'OK', 
    'not OK' => 'nicht OK', 
    'enquired' => 'angefragt', 
    'no Feedback' => 'keine Auskunft'
  );
    
  if (count($arrStatus) > 0) {
    
    if ($_REQUEST['strSelSta'] == 'all') {
      $strSelectedAll = ' selected="selected"';
    } elseif ($_REQUEST['strSelSta'] == '') {
      $_REQUEST['strSelSta'] = 'enquired';
    } else {
      $strSelectedAll = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Event-Status: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelSta" id="strSelSta">' .chr(10);
    //$strOutput.= '    <option value="all"' .$strSelectedAll .'>Alle</option>' .chr(10);
    
    foreach ($arrStatus as $strStatus => $strValue) {
      $strSelected = '';
      if ($strStatus == $_REQUEST['strSelSta']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option value="' .$strStatus .'"' .$strSelected .'>' .$strValue .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
    
    
  $strSql6 = 'SELECT DISTINCT `SF42_IZSEvent__c`.`SF42_informationProvider__c` AS `Id`, `Account`.`Name` AS `Name`, `Account`.`Dezentrale_Anfrage__c` ';
  $strSql6.= 'FROM `SF42_IZSEvent__c` INNER JOIN `Account` ';
  $strSql6.= 'ON `SF42_IZSEvent__c`.`SF42_informationProvider__c` = `Account`.`Id` ';
  $strSql6.= 'WHERE `SF42_isInformationProvider__c` = "true" ';
  $strSql6.= 'ORDER BY `Account`.`Name` ASC';
  
  $arrResult6 = MySQLStatic::Query($strSql6);
  if (count($arrResult6) > 0) {

    if ($_REQUEST['strSelInf'] == 'all') {
      $strSelectedAll = ' selected="selected"';
    } else {
      $strSelectedAll = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Information Provider: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelInf" id="strSelInf">' .chr(10);
    $strOutput.= '    <option value="">Bitte auswählen</option>' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Infoprovider -</option>' .chr(10);
    
    foreach ($arrResult6 as $arrProvider) {
      $strSelected = '';
      if ($arrProvider['Id'] == $_REQUEST['strSelInf']) {
        $strSelected = ' selected="selected"';
      } 
      
      $strAddDez = '';
      
      if ($arrProvider['Dezentrale_Anfrage__c'] == 'true') {
        $strOptClass = ' class="optv"';
        
        $strSqlDez = 'SELECT `Id`, `Name`, `CATCH_ALL__c` FROM `Anfragestelle__c` WHERE `Information_Provider__c`="' .$arrProvider['Id'] .'" ORDER BY `Name`';
        $arrSqlDez = MySQLStatic::Query($strSqlDez);

        //$arrProvider['Id'] = 'da_' .$arrProvider['Id'];
        
        if (count($arrSqlDez) > 0) {
          foreach ($arrSqlDez as $arrDez) {
            
            $strSelectedDez = '';
            if (('d_' .$arrDez['Id']) == $_REQUEST['strSelInf']) {
              $strSelectedDez = ' selected="selected"';
            } 
            
            $strAddDez.= '    <option value="d_' .$arrDez['Id'] .'"' .$strSelectedDez .'>&nbsp;&nbsp;- ' .$arrDez['Name'] .'</option>' .chr(10);
          } 
        }
        
      } else {
        $strOptClass = '';
      }
      
      $strOutput.= '    <option' .$strOptClass .' value="' .$arrProvider['Id'] .'"' .$strSelected .'>' .$arrProvider['Name'] .'</option>' .chr(10);
      $strOutput.= $strAddDez;
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }

    $strOutput.= '</tbody>' .chr(10);

    $strOutput.= '  <tfoot>' .chr(10);
    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td align="right" colspan="2"><input type="submit" name="Suche" value="Suche" /></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
    $strOutput.= '  </tfoot>' .chr(10);


  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);
    
}



$strOutput.= '

	<div id="tabs">
	
		<ul style="float: left; width: 100%">
			<li><a href="#tabs-1" title="">01</a></li>
			<li><a href="#tabs-2" title="">02</a></li>
			<li><a href="#tabs-3" title="">03</a></li>
			<li><a href="#tabs-4" title="">04</a></li>
		</ul>
		
		<!-- <div id="ucontrol" style="float: left; width: 39%; height: 30px;">ggg</div> -->

		<div id="tabs_container" style="overflow-y: scroll; height:535px; width: 20%; float: left;">
		
		<div id="tabs-1">
		<p>
';

if (is_array($strDir01Scan)) {
  foreach($strDir01Scan as $strFile) {
    if ($strFirstPreview == '') {
      $strFirstPreview = 'events/upload01/' .$strFile;
      $strStyle = 'font-weight: bold;';
    } else {
      $strStyle = '';
    }
    $strOutput.= '<span id="prevId' .$intId .'" style="' .$strStyle .'" onclick="setSrc(\'events/upload01/' .$strFile .'\', \'prevId' .$intId .'\');">' .$strFile .'</span><br />';
    $intId++;
  }
}

$strOutput.= '
			</p>
		</div> <!-- tabs-1 -->

		<div id="tabs-2">
		<p>
';

if (is_array($strDir02Scan)) {
  foreach($strDir02Scan as $strFile) {
    if ($strFirstPreview == '') {
      $strFirstPreview = 'events/upload02/' .$strFile;
      $strStyle = 'font-weight: bold;';
    } else {
      $strStyle = '';
    }
    $strOutput.= '<span id="prevId' .$intId .'" style="' .$strStyle .'" onclick="setSrc(\'events/upload02/' .$strFile .'\', \'prevId' .$intId .'\');">' .$strFile .'</span><br />';
    $intId++;
  }
}

$strOutput.= '
			</p>
		</div> <!-- tabs-2 -->

		<div id="tabs-3">
		<p>
';

if (is_array($strDir03Scan)) {
  foreach($strDir03Scan as $strFile) {
    if ($strFirstPreview == '') {
      $strFirstPreview = 'events/upload03/' .$strFile;
      $strStyle = 'font-weight: bold;';
    } else {
      $strStyle = '';
    }
    $strOutput.= '<span id="prevId' .$intId .'" style="' .$strStyle .'" onclick="setSrc(\'events/upload03/' .$strFile .'\', \'prevId' .$intId .'\');">' .$strFile .'</span><br />';
    $intId++;
  }
}

$strOutput.= '
			</p>
		</div> <!-- tabs-3 -->

		<div id="tabs-4">
		<p>
';

if (is_array($strDir04Scan)) {
  foreach($strDir04Scan as $strFile) {
    if ($strFirstPreview == '') {
      $strFirstPreview = 'events/upload04/' .$strFile;
      $strStyle = 'font-weight: bold;';
    } else {
      $strStyle = '';
    }
    $strOutput.= '<span id="prevId' .$intId .'" style="' .$strStyle .'" onclick="setSrc(\'events/upload04/' .$strFile .'\', \'prevId' .$intId .'\');">' .$strFile .'</span><br />';
    $intId++;
  }
}

$strOutput.= '
			</p>
		</div> <!-- tabs-4 -->

		</div> <!-- tabs_container -->
		
    <div id="pv" style="float: left; width: 40%">
  	  <iframe id="preview" src="http://docs.google.com/gview?url=http://izs-institut.de/cms/' .$strFirstPreview .'&embedded=true" style="width:100%; height:555px;" frameborder="0"></iframe>
  	</div>


';

if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {

$strOutput.= '<div id="list" style="overflow: scroll; height:555px; width: 37%; float: left;">
';

  if (in_array($_REQUEST['strSelSta'], $arrChangePreview) == true) { 
    $boolMore = true;
  } else {
    $boolMore = false;
  }
  
  $arrPeriod = explode('_', $_REQUEST['strSelDate']);
  
  if (strstr($_REQUEST['strSelInf'], 'd_') != false) {
    
    $_REQUEST['strSelInf'] = substr($_REQUEST['strSelInf'], 2);
    $strAnfragestelleId = $_REQUEST['strSelInf'];
    
    $strSqlDez = 'SELECT `Id`, `Name`, `CATCH_ALL__c` FROM `Anfragestelle__c` WHERE `Id`="' .$_REQUEST['strSelInf'] .'"';
    $arrSqlDez = MySQLStatic::Query($strSqlDez);
    
    $strSqlPp = 'SELECT `Premium_Payer__c`, `Information_provider__c` FROM `Dezentrale_Anfrage_KK__c` WHERE `Anfragestelle__c` = "' .$_REQUEST['strSelInf'] .'"';
    $arrSqlPp = MySQLStatic::Query($strSqlPp);

    if ($_SERVER['REMOTE_ADDR'] == '46.244.171.57') {
      foreach ($arrSqlPp as $arrPremiumPayer) {

        $strSqlPpA1 = 'SELECT `Id`, `Name`, `SF42_Comany_ID__c`, `SF42_Comany_ID__c` FROM `Account` WHERE `Id` = "' .$arrPremiumPayer['Premium_Payer__c'] .'"';
        $arrSqlPpA1 = MySQLStatic::Query($strSqlPpA1);
        
        print_r($arrSqlPpA1);
        
        $strSql = 'SELECT `Id`, `Name`, `RecordTypeId`, `SF42_EventStatus__c`, `SF42_Premium_Payer__c`, `Status_Klaerung__c`, ';
        $strSql.= '`Betriebsnummer_IP__c`, `Betriebsnummer_ZA__c`, ';
        $strSql.= '`SF42_EventComment__c`, `Art_des_Dokuments__c`, `SF42_PublishingStatus__c`, `Sperrvermerk__c` ';
        $strSql.= 'FROM `SF42_IZSEvent__c` ';
        $strSql.= 'WHERE 1 '; //(`SF42_EventStatus__c` = "' .$_REQUEST['strSelSta'] .'") ';
        $strSql.= 'AND (`SF42_informationProvider__c` = "' .$arrSqlPp[0]['Information_provider__c'] .'") ';
        $strSql.= 'AND (`Beitragsmonat__c` = "' .$arrPeriod[0] .'") AND (`Beitragsjahr__c` = "' .$arrPeriod[1] .'") ';
        //$strSql.= 'AND `RecordTypeId` = "01230000001Ao76AAC" '; //Review -> angefragt  
        $strSql.= 'AND `Betriebsnummer_ZA__c` = "' .$arrSqlPpA1[0]['SF42_Comany_ID__c'] .'" '; //Betriebsnummer  
        $strSql.= 'ORDER BY `Betriebsnummer_ZA__c` ASC';        
        
        $arrEv = MySQLStatic::Query($strSql);
        echo $strSql;
        print_r($arrEv);

      }
    }

    $arrPayer = array();
    $arrPayerAccount = array();
    $strPremiumPayer = '';
    
    if (count($arrSqlPp) > 0) {
      foreach ($arrSqlPp as $arrPremiumPayer) {
        $arrPayer[] = '"' .$arrPremiumPayer['Premium_Payer__c'] .'"';
        $_REQUEST['strSelInf'] = $arrPremiumPayer['Information_provider__c'];
      }
    
      $strPremiumPayer = implode(',', $arrPayer);
      
      $strSqlPpA = 'SELECT `SF42_Comany_ID__c` FROM `Account` WHERE `Id` IN (' .$strPremiumPayer .')';
      $arrSqlPpA = MySQLStatic::Query($strSqlPpA);

      if (count($arrSqlPpA) > 0) {
        foreach ($arrSqlPpA as $arrPpAccount) {
          $arrPayerAccount[] = '"' .$arrPpAccount['SF42_Comany_ID__c'] .'"';
        }
        $strPpAccount = implode(',', $arrPayerAccount);
      }
      
    }
    
    if ($arrSqlDez[0]['CATCH_ALL__c'] == 'true') {
      
      
      $strSqlKK = 'SELECT * FROM `Anfragestelle__c` WHERE `Id`="' .$strAnfragestelleId .'"';
      $arrKK = MySQLStatic::Query($strSqlKK);
      
      $boolIsCatchAll = true;
      $arrCatchAll = $arrKK[0];
      
      $strSqlKK = 'SELECT * FROM `Account` WHERE `Id`="' .$arrKK[0]['Information_Provider__c'] .'"';
      $arrKK = MySQLStatic::Query($strSqlKK);

      $strSql6 = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Beitragsmonat__c` = "' .$arrPeriod[0] .'" AND `Beitragsjahr__c` = "' .$arrPeriod[1] .'" AND ';
      $strSql6.= '((`SF42_EventStatus__c` = "enquired") OR (`SF42_EventStatus__c` = "to enquire")) ';
      $strSql6.= 'AND `SF42_informationProvider__c` = "' .$arrKK[0]['Id'] .'" GROUP BY `Betriebsnummer_ZA__c`';
      $arrEventList = MySQLStatic::Query($strSql6);
      
      foreach ($arrEventList as $intEv => $arrEvent) {
    
        $strSqlAc = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c`="' .$arrEvent['Betriebsnummer_ZA__c'] .'"';
        $arrAc = MySQLStatic::Query($strSqlAc);

        //PremiumPayer gesuchtem Team zugeordnet
        $strSql7 = 'SELECT * FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrAc[0]['Id'] .'" AND `Information_Provider__c` = "' .$arrKK[0]['Id'] .'" AND `Anfragestelle__c` = "' .$strAnfragestelleId .'"';
        $arrResult7 = MySQLStatic::Query($strSql7);

        if (count($arrResult7) == 0) {
          
          $strSql8 = 'SELECT `Id` FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrAc[0]['Id'] .'" AND `Information_Provider__c` = "' .$arrKK[0]['Id'] .'"';
          $arrResult8 = MySQLStatic::Query($strSql8);
          
          //keinem Team zugeordnet
          if (count($arrResult8) == 0) {
            $arrPayerAccount[] = $arrEvent['Betriebsnummer_ZA__c'];
          }
          
        }
        
      }

      $strPpAccount = implode(',', $arrPayerAccount);
        
      if ($_SERVER['REMOTE_ADDR'] == '88.217.23.84') {
        // echo '|' .print_r($arrPayerAccount, true) . chr(10) .print_r($arrCatchAllPp, true); die();
      }          



      
    }
  
    $strSql = 'SELECT `Id`, `Name`, `SF42_EventStatus__c`, `SF42_Premium_Payer__c`, `Status_Klaerung__c`, ';
    $strSql.= '`Betriebsnummer_IP__c`, `Betriebsnummer_ZA__c`, `SF42_informationProvider__c`, ';
    $strSql.= '`SF42_EventComment__c`, `Art_des_Dokuments__c`, `SF42_PublishingStatus__c`, `Sperrvermerk__c` ';
    $strSql.= 'FROM `SF42_IZSEvent__c` ';
    $strSql.= 'WHERE (`SF42_EventStatus__c` = "' .$_REQUEST['strSelSta'] .'") ';
    if (($_REQUEST['strSelInf'] != '') && ($_REQUEST['strSelInf'] != 'all')) {
      $strSql.= 'AND (`SF42_informationProvider__c` = "' .$_REQUEST['strSelInf'] .'") ';
    }
    $strSql.= 'AND (`Beitragsmonat__c` = "' .$arrPeriod[0] .'") AND (`Beitragsjahr__c` = "' .$arrPeriod[1] .'") ';
    
    /*
    if ($_REQUEST['strSelSta'] == 'enquired') {
      $strSql.= 'AND `RecordTypeId` = "01230000001Ao76AAC" '; //Review -> angefragt
    } elseif ($_REQUEST['strSelSta'] == 'OK') {
      $strSql.= 'AND `RecordTypeId` = "01230000001Ao75AAC" '; //Publication -> OK
    }
    */
    
    $strSql.= 'AND `RecordTypeId` = "01230000001Ao76AAC" '; //Review -> angefragt  
    
    if (strlen($strPpAccount) > 0) {
      $strSql.= 'AND `Betriebsnummer_ZA__c` IN (' .$strPpAccount .') '; //Betriebsnummer  
    }
    
    $strSql.= 'ORDER BY `Betriebsnummer_ZA__c` ASC';

    if ($_SERVER['REMOTE_ADDR'] == '46.244.171.57') {
      echo $strSql;
    }
    
  } else {
    
    if (strstr($_REQUEST['strSelInf'], 'da_') != false) {
      $_REQUEST['strSelInf'] = substr($_REQUEST['strSelInf'], 3);
      //$boolShowIp = true;
    }
  
    $strSql = 'SELECT `Id`, `Name`, `SF42_EventStatus__c`, `SF42_Premium_Payer__c`, `Status_Klaerung__c`, ';
    $strSql.= '`Betriebsnummer_IP__c`, `Betriebsnummer_ZA__c`, `SF42_informationProvider__c`, ';
    $strSql.= '`SF42_EventComment__c`, `Art_des_Dokuments__c`, `SF42_PublishingStatus__c`, `Sperrvermerk__c`, `SF42_DocumentUrl__c` ';
    $strSql.= 'FROM `SF42_IZSEvent__c` ';
    $strSql.= 'WHERE (`SF42_EventStatus__c` = "' .$_REQUEST['strSelSta'] .'") ';
    if (($_REQUEST['strSelInf'] != '') && ($_REQUEST['strSelInf'] != 'all')) {
      $strSql.= 'AND (`SF42_informationProvider__c` = "' .$_REQUEST['strSelInf'] .'") ';
    }
    $strSql.= 'AND (`Beitragsmonat__c` = "' .$arrPeriod[0] .'") AND (`Beitragsjahr__c` = "' .$arrPeriod[1] .'") ';
    
    /*
    if ($_REQUEST['strSelSta'] == 'enquired') {
      $strSql.= 'AND `RecordTypeId` = "01230000001Ao76AAC" '; //Review -> angefragt
    } elseif ($_REQUEST['strSelSta'] == 'OK') {
      $strSql.= 'AND `RecordTypeId` = "01230000001Ao75AAC" '; //Publication -> OK
    }
    */
    
    $strSql.= 'AND `RecordTypeId` = "01230000001Ao76AAC" '; //Review -> angefragt  
    
    $strSql.= 'ORDER BY `Betriebsnummer_ZA__c` ASC';
    
  }
  
  $arrResult = MySQLStatic::Query($strSql);
  
  $arrInformationProvider = array();
  
  if (count($arrResult) > 0) {
    
    $arrInformationProvider = $arrResult;
  
  }
  
  //echo count($arrResult);
  //print_r($arrInformationProvider);
  //print_r($arrDecentral);
  
  if (count($arrInformationProvider) > 0) {
    //natcasesort($arrInformationProvider);

    if ($boolMore) {
      $strOutput.= '<form class="events" action="/cms/index.php?ac=revi" method="post">' .chr(10);
      $strOutput.= '<input type="hidden" name="change" value="1">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelDate" value="' .$_REQUEST['strSelDate'] .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelSta" value="' .$_REQUEST['strSelSta'] .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelInf" value="' .$_REQUEST['strSelInf'] .'">' .chr(10);

      $strOutput.= '<div align="right" class="buttons">veröffentliche <input type="submit" name="marked" value="Nur markierte" /> <input type="submit" name="all" value="Alle" /></div>' .chr(10);
    }
    
    $strOutput.= '<table class="tablesorter" id="tables" style="min-width: 600px">' .chr(10);
    $strOutput.= '  <thead>' .chr(10);
    $strOutput.= '    <tr>' .chr(10);
    if ($boolMore) {
      $strOutput.= '      <th class="{sorter: false}"><input type="checkbox" value="0" name="check_all"></th>' .chr(10); //class="header"
    }
  	$strOutput.= '      <th class="{sorter: false}">Status</th>' .chr(10); //class="header"
  	$strOutput.= '      <th class="header">Event-ID</th>' .chr(10); //class="header"
    if (($_REQUEST['strSelInf'] == 'all') || ($boolShowIp)) {
      $strOutput.= '      <th class="header">Infoprovider</th>' .chr(10);
    }
  	$strOutput.= '      <th class="header">BNR PP</th>' .chr(10); //class="header"
  	$strOutput.= '      <th class="header">Premium Payer</th>' .chr(10); //class="header"
    $strOutput.= '      <th class="header">DL</th>' .chr(10); //class="header"
    //$strOutput.= '      <th class="header">DS</th>' .chr(10); //class="header"
    if ($boolMore) {
      $strOutput.= '      <th class="header">DokArt</th>' .chr(10); //class="header"
      $strOutput.= '      <th class="header">sichtbar</th>' .chr(10); //class="header"
      $strOutput.= '      <th class="header">SVermerk</th>' .chr(10); //class="header"
      $strOutput.= '      <th class="header">Kommentar</th>' .chr(10); //class="header"
    }
    $strOutput.= '    </tr>' .chr(10);
    $strOutput.= '  </thead>' .chr(10);
    $strOutput.= '  <tbody>' .chr(10);
    
    $arrPeriod = explode('_', $_REQUEST['strSelDate']);
    
    $strNext = '';

    foreach ($arrInformationProvider as $strId => $arrEvent) {
      
      if ($arrEvent['Status_Klaerung__c'] == 'in Klärung') {
        $strTrClass = 'red';
      } else {
        $strTrClass = '';
      }
      
      if ($boolMore && ($arrEvent['SF42_PublishingStatus__c'] == 'private')) {
        $strTrClass = 'red';
      } else {
        $strTrClass = '';
      }
      
      if ($strNext == 'blubb') {
        $strNext = $arrEvent['Id'];
      }
      
      if (($_REQUEST['done'] != '') && ($_REQUEST['done'] == $arrEvent['Id'])) {
        $strNext = 'blubb';
      }
      
      $strOutput.= '    <tr class="' .$strTrClass .'" id="e_' .$arrEvent['Id'] .'">' .chr(10);
      if ($boolMore) {
        $strOutput.= '      <td><input type="checkbox" name="publish[' .$arrEvent['Id'] .']" value="1"></td>' .chr(10);
        //$strOutput.= '      <td>&nbsp;</td>' .chr(10);
      }
      $strOutput.= '      <td align="center"><img src="/assets/images/sys/status_' .$arrStatusCss[$arrEvent['SF42_EventStatus__c']] .'.png" /></td>' .chr(10);
      $strOutput.= '      <td><span style="cursor:pointer;" onclick="selEv(\'' .$arrEvent['Id'] .'\')">' .$arrEvent['Name'] .'</span></td>' .chr(10);
      if (($_REQUEST['strSelInf'] == 'all') || ($boolShowIp)) {
        $strSql = 'SELECT `Name` FROM `Account` WHERE `Id` = "' .$arrEvent['SF42_informationProvider__c'] .'"';
        $arrSql = MySQLStatic::Query($strSql);
        $strOutput.= '      <td>' .$arrSql[0]['Name'] .'</td>' .chr(10);
      }
      $strOutput.= '      <td>' .$arrEvent['Betriebsnummer_ZA__c'] .'</td>' .chr(10);
      $strOutput.= '      <td>' .$arrEvent['SF42_Premium_Payer__c'] .'</td>' .chr(10);
      
      if ($arrEvent['SF42_DocumentUrl__c'] != '') {
        $strDL = '<a href="' .$arrEvent['SF42_DocumentUrl__c'] .'" target="_blank"><img src="img/icon_pdf.png" alt="' .$arrEvent['SF42_DocumentUrl__c'] .'" /></a>';
      } else {
        $strDL = '';
      }
      $strOutput.= '      <td id="file_e_' .$arrEvent['Id'] .'_l">' .$strDL .'</td>' .chr(10);
      
      $strSql = 'SELECT SF42_DocumentUrl__c FROM SF42_IZSEvent__c WHERE Id = \'' .$arrEvent['Id'] .'\'';
      //$arrResult = SForceStatic::Query($strSql);

      /*
      if ($arrResult[0]['SF42_DocumentUrl__c'] != '') {
        $strDS = '<a href="' .$arrResult[0]['SF42_DocumentUrl__c'] .'" target="_blank"><img src="img/icon_pdf.png" alt="' .$arrResult[0]['SF42_DocumentUrl__c'] .'" /></a>';
      } else {
        $strDS = '';
      }
      $strOutput.= '      <td id="file_e_' .$arrEvent['Id'] .'_s">' .$strDS .'</td>' .chr(10);
      */
      
      if ($boolMore) {
        $strOutput.= '      <td>' .$arrDokArt[$arrEvent['Art_des_Dokuments__c']] .'</td>' .chr(10);
        $strOutput.= '      <td>' .$arrSichtbar[$arrEvent['SF42_PublishingStatus__c']] .'</td>' .chr(10);
        $strOutput.= '      <td>' .$arrSperr[$arrEvent['Sperrvermerk__c']] .'</td>' .chr(10);
        $strOutput.= '      <td>' .$arrEvent['SF42_EventComment__c'] .'</td>' .chr(10);
      }
      $strOutput.= '    </tr>' .chr(10);
      
    }
      
    
    $strOutput.= '  </tbody>' .chr(10);
    $strOutput.= '</table>' .chr(10);
    
    if ($boolMore) {
      $strOutput.= '</form>' .chr(10);
    }

    if (($strNext != '') && ($strNext != 'blubb') && ($_REQUEST['strSelSta'] != 'OK') && ($_SERVER['REQUEST_METHOD'] != 'POST')) {
      $strOutput.= '<script>$(document).ready(function () { openOffersDialog(\'' .$strNext .'\'); });</script>' .chr(10);
    }

          
  } else {
    
    $strOutput.= '<p>Keine Premium Payer im gewählten Zeitraum vorhanden.</p>' .chr(10);
    
  }
  
  $strOutput.= '</div>' .chr(10);

}

  
$strOutput.= '</div>' .chr(10);



?>