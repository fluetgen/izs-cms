<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

include('conf/global.inc.php');
require_once('../assets/classes/class.mysql.php');
require_once('../assets/classes/class.database.php');

$strOutput = '';
$strOutput.= '<h1>Dokument erstellen</h1>' .chr(10);

$strPath = 'vorlagen/';

$strUseCategory = '';

switch ($_REQUEST['ac']) {

  case 'offe' : 
    $strUseCategory = 'Angebot'; 
    $strLabel1 = 'Angebot'; 
    $strLabel2 = 'Opportunity'; 
  break;
  case 'ofmi' : 
    $strUseCategory = 'Mitgliedsbescheinigung'; 
    $strLabel1 = 'Mitgliedsbescheinigung'; 
    $strLabel2 = 'Company Group'; 
  break;
  case 'ofse' : 
    $strUseCategory = 'SEPA'; 
    $strLabel1 = 'SEPA'; 
    $strLabel2 = 'Company Group'; 
  break;
  case 'ofvo' : 
    $strUseCategory = 'Vollmacht'; 
    $strLabel1 = 'Vollmacht'; 
    $strLabel2 = 'Company Group'; 
  break;
  case 'offa' : 
    $strUseCategory = 'VollmachtFA'; 
    $strLabel1 = 'Vollmacht FA'; 
    $strLabel2 = 'Company Group'; 
  break;
  case 'ofku' : 
    $strUseCategory = 'Kuendigung'; 
    $strLabel1 = 'Kündigung'; 
    $strLabel2 = 'Company Group'; 
  break;
  case 'ofpr' : 
    $strUseCategory = 'Preiserhoehung'; 
    $strLabel1 = 'Preiserhöhung'; 
    $strLabel2 = 'Company Group'; 
  break;

}


function iterateDir($sPath) {
  $aRes = array();
    foreach(new DirectoryIterator($sPath) as $oItem) {
      if($oItem->isDir()) {
        (!$oItem->isDot() ? $aRes[$oItem->getFilename()] = iterateDir($oItem->getPathname()):0);
        continue;
      }
    $aRes[] = $oItem->getFilename();
    } 
  return $aRes;
}

$arrVorlagen = iterateDir($strPath);

//$strOutput.= print_r($arrVorlagen, true);

$strOutput.= '
<script>
function check() {
  var ret = true;
  
  $(".required").each(function() {

    if ($(this).val() == "") {
      $(this).css("border", "1px solid red");
      //$(this).focus();
      ret = false;
    } else {
      $(this).css("border", "1px solid #969696");
    }
    
  });
  
  if (ret === false) {
    alert("Bitte die markierten Felder ausfüllen.");
  }
  
  return ret;
  
}

</script>
' .chr(10);

$strOutput.= '<form method="post" action="" onsubmit="return check();">' .chr(10);
$strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);

$strOutput.= '<table>' .chr(10);
$strOutput.= '<tbody>' .chr(10);

$strOutput.= '  <tr>' .chr(10);
$strOutput.= '    <td style="width:110px;">' .$strLabel1 .': </td>' .chr(10);
$strOutput.= '    <td><select name="strSelOppo" id="strSelOppo" class="required">' .chr(10);
    
$strOutput.= '  <option value="">Bitte auswählen</option>' .chr(10);

$intPeriod = 0;

foreach ($arrVorlagen as $strCategory => $arrTemplate) {

  asort($arrTemplate);

  /*
  print_r($arrTemplate);
  echo "$strCategory == $strUseCategory" .chr(10);
  */

  if ($strCategory == $strUseCategory) {

    foreach ($arrTemplate as $strTemplate) {
  
      $strValue = $strCategory .'|' .$strTemplate;
  
      $strSelected = '';
      
      if ($strValue == $_REQUEST['strSelOppo']) {
        $strSelected = ' selected="selected"';
      } 
      
      $arrTemplateShow = explode('.', $strTemplate);
      $strTemplateSchow = $arrTemplateShow[0];
      
      //$strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .$strCategory .' - ' .$strTemplateSchow .'</option>' .chr(10);
      $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .$strTemplateSchow .'</option>' .chr(10);
    
    }

  }
  
}

$strOutput.= '    </select></td>' .chr(10);
$strOutput.= '  </tr>' .chr(10);

$strOutput.= '  <tr>' .chr(10);
$strOutput.= '    <td>' .$strLabel2 .': </td>' .chr(10);
$strOutput.= '    <td><input type="text" name="strSearchOppo" value="' .$_REQUEST['strSearchOppo'] .'" class="required"></td>' .chr(10);
$strOutput.= '  </tr>' .chr(10);

$strOutput.= '</tbody>' .chr(10);

$strOutput.= '  <tfoot>' .chr(10);
$strOutput.= '  <tr>' .chr(10);
$strOutput.= '    <td align="right" colspan="2"><input type="submit" name="Suche" value="Suche" /></td>' .chr(10);
$strOutput.= '  </tr>' .chr(10);
$strOutput.= '  </tfoot>' .chr(10);

$strOutput.= '</table>' .chr(10);
$strOutput.= '</form>' .chr(10);


if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {
  
  if ($strUseCategory == 'Angebot') {
    $strSql = 'SELECT `Opportunity`.* FROM `Opportunity` LEFT JOIN `Account` ON `AccountId` = `Account`.`Id` WHERE `Opportunity`.`Name` LIKE "%' .$_REQUEST['strSearchOppo'] .'%" ';
    $strSql.= 'OR `Bezeichner__c` LIKE "%' .$_REQUEST['strSearchOppo'] .'%" OR `Account`.`Name` LIKE "%' .$_REQUEST['strSearchOppo'] .'%" ';
  } elseif ($strUseCategory == 'Preiserhoehung') {
    $strSql = 'SELECT *, `Firmengruppe` AS `Name`, `Nr` AS `Id` FROM `temp_preiserhoehung` WHERE `Firmengruppe` LIKE "%' .$_REQUEST['strSearchOppo'] .'%"';
  } else {
    $strSql = 'SELECT * FROM `Group__c` WHERE ((`Liefert_Daten__c` = "true") OR (`Id` = "a083000000UGC3LAAX")) AND `Name` LIKE "%' .$_REQUEST['strSearchOppo'] .'%"';
  }

  $arrOpportunityList = MySQLStatic::Query($strSql);


  if ($strUseCategory == 'VollmachtFA') {
    $strSql = 'SELECT * FROM `Account` WHERE `Ist_FA_Meldestelle__c` = "true" AND `SF42_Company_Group__c` = "' .$arrOpportunityList[0]['Id'] .'"';
    $arrMeldestList = MySQLStatic::Query($strSql);

    foreach ($arrMeldestList as $intKey => $arrMeldestelle) {
      $strSql = 'SELECT * FROM `Verbindung_Meldestelle_FA__c` WHERE `FA_Meldestelle__c` = "' .$arrMeldestelle['Id'] .'"';
      $arrVerbindung = MySQLStatic::Query($strSql);
      if (count($arrVerbindung) > 0) {
        break;
      }
    }

  }

  if (($strUseCategory == 'VollmachtFA') && (count($arrVerbindung) == 0)) {
    
    $strOutput.= '<p>Keine (aktive) FA-Meldestelle vorhanden.</p>' .chr(10);
    
  } else if (count($arrOpportunityList) > 0) {
    //natcasesort($arrInformationProvider);

    $strOutputTable = '';
    $strOutputTable.= '<table class="" id="5">' .chr(10);
    $strOutputTable.= '  <thead>' .chr(10);
    $strOutputTable.= '    <tr>' .chr(10);

    if ($strUseCategory == 'Angebot') {
      $strOutputTable.= '      <th class="header">Nummer</th>' .chr(10);
      $strOutputTable.= '      <th class="header">Name</th>' .chr(10);
      $strOutputTable.= '      <th class="header">Account</th>' .chr(10);
      $strOutputTable.= '      <th class="header">Phase</th>' .chr(10);
    } else {
      $strOutputTable.= '      <th class="header">' .$$strLabel2 .' Name</th>' .chr(10); //class="header"
    }

    $strOutputTable.= '      <th class="header">' .$strLabel1 .'</th>' .chr(10);
    $strOutputTable.= '    </tr>' .chr(10);
    $strOutputTable.= '  </thead>' .chr(10);
    $strOutputTable.= '  <tbody>' .chr(10);
    
    $arrPeriod = explode('_', $_REQUEST['strSelDate']);
    
    $strNext = '';

    foreach ($arrOpportunityList as $arrOpportunity) {
      
      $strOutputTable.= '    <tr>' .chr(10);
      $strOutputTable.= '      <td>' .$arrOpportunity['Name'] .'</td>' .chr(10);
      
      if ($strUseCategory == 'Angebot') {

        $strSql = 'SELECT `Name` FROM `Account` WHERE `Id` = "' .$arrOpportunity['AccountId'] .'"';
        $arrSql = MySQLStatic::Query($strSql);
        $strOutputTable.= '      <td>' .$arrOpportunity['Bezeichner__c'] .'</td>' .chr(10);
        $strOutputTable.= '      <td>' .$arrSql[0]['Name'] .'</td>' .chr(10);
        $strOutputTable.= '      <td>' .$arrOpportunity['StageName'] .'</td>' .chr(10);

      }

      if (strstr($arrOpportunity['Name'], 'Alten GmbH') !== false) {
        $strOutputTable.= '      <td><a target="_blank" href="/cms/createOffer.php?Id=' .$arrOpportunity['Id'] .'&temp=' .$_REQUEST['strSelOppo'] .'&ac=' .$_REQUEST['ac'] .'&add=Alten GmbH">erstellen (Alten GmbH)</a><br />' .chr(10);
        $strOutputTable.= '      <a target="_blank" href="/cms/createOffer.php?Id=' .$arrOpportunity['Id'] .'&temp=' .$_REQUEST['strSelOppo'] .'&ac=' .$_REQUEST['ac'] .'&add=Alten SW GmbH">erstellen (Alten SW GmbH)</a><br />' .chr(10);
        $strOutputTable.= '      <a target="_blank" href="/cms/createOffer.php?Id=' .$arrOpportunity['Id'] .'&temp=' .$_REQUEST['strSelOppo'] .'&ac=' .$_REQUEST['ac'] .'&add=Alten Technology GmbH">erstellen (Alten Technology GmbH)</a></td>' .chr(10);
      } elseif (strstr($arrOpportunity['Name'], 'AlphaConsult KG') !== false) {
        $strOutputTable.= '      <td><p style="margin: 10px 0px;"><a target="_blank" href="/cms/createOffer.php?Id=' .$arrOpportunity['Id'] .'&temp=' .$_REQUEST['strSelOppo'] .'&ac=' .$_REQUEST['ac'] .'">erstellen (Gesamtliste)</a></p>' .chr(10);
        $strOutputTable.= '      <p><a target="_blank" href="/cms/createOffer.php?Id=' .$arrOpportunity['Id'] .'&temp=' .$_REQUEST['strSelOppo'] .'&ac=' .$_REQUEST['ac'] .'&add=AlphaConsult KG">erstellen (AlphaConsult KG)</a><br />' .chr(10);
        $strOutputTable.= '      <a target="_blank" href="/cms/createOffer.php?Id=' .$arrOpportunity['Id'] .'&temp=' .$_REQUEST['strSelOppo'] .'&ac=' .$_REQUEST['ac'] .'&add=Alpha-Engineering GmbH & Co. KG">erstellen (Alpha-Engineering GmbH & Co. KG)</a><br />' .chr(10);
          $strOutputTable.= '      <a target="_blank" href="/cms/createOffer.php?Id=' .$arrOpportunity['Id'] .'&temp=' .$_REQUEST['strSelOppo'] .'&ac=' .$_REQUEST['ac'] .'&add=Alpha-Med KG">erstellen (Alpha-Med KG)</a><br />' .chr(10);
          $strOutputTable.= '      <a target="_blank" href="/cms/createOffer.php?Id=' .$arrOpportunity['Id'] .'&temp=' .$_REQUEST['strSelOppo'] .'&ac=' .$_REQUEST['ac'] .'&add=AlphaConsult Premium KG">erstellen (AlphaConsult Premium KG)</a><br />' .chr(10);
          $strOutputTable.= '      <a target="_blank" href="/cms/createOffer.php?Id=' .$arrOpportunity['Id'] .'&temp=' .$_REQUEST['strSelOppo'] .'&ac=' .$_REQUEST['ac'] .'&add=Alpha-Students GmbH">erstellen (Alpha-Students GmbH)</a></p></td>' .chr(10);
      } else {
        $strOutputTable.= '      <td><a target="_blank" href="/cms/createOffer.php?Id=' .$arrOpportunity['Id'] .'&temp=' .$_REQUEST['strSelOppo'] .'&ac=' .$_REQUEST['ac'] .'">erstellen</a></td>' .chr(10);
      }

      

      $strOutputTable.= '    </tr>' .chr(10);
      
    }
      
    $strOutputTable.= '  </tbody>' .chr(10);
    $strOutputTable.= '</table>' .chr(10);

    $strOutput.= '<div class="clearfix" style="height: 48px; width: 1094px;">' .chr(10);
    $strOutput.= '  <div class="form-left">' .chr(10);
    $strOutput.= '    <h3>' .count($arrOpportunityList) .' Ergebnisse gefunden:</h3>' .chr(10);
    $strOutput.= '  </div> <!-- ' .chr(10);
    $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' .chr(10);
    $strOutput.= '    <form method="post" action="_get_csv.php">';
    $strOutput.= '    <input type="hidden" name="strTable" value="' .str_replace('"', "'", $strOutputTable) .'">' .chr(10);
    $strOutput.= '    <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' .chr(10);
    $strOutput.= '    </form>';
    $strOutput.= '  </div> -->' .chr(10);
    $strOutput.= '</div>' .chr(10);
    
    $strOutput.= $strOutputTable;
    
    $strOutput.= "
<script>

$('#5').fixheadertable({ 
   colratio    : [400, 400, 80], 
   height      : 250, 
   width       : 910, 
   zebra       : true, 
   resizeCol   : true,
   sortable    : true,
   sortType    : ['string', 'string', ''],
   sortedColId : 0, 
   dateFormat  : 'Y-m'
});
</script>
";
          
  } else {
    
    $strOutput.= '<p>Keine Datensätze vorhanden.</p>' .chr(10);
    
  }
  
}

      

?>