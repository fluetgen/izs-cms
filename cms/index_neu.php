<?php

ini_set('memory_limit', '256M');

header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

require_once('main.inc.php');

?><!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">
  <title><?php echo $strPageTitle; ?> | CMS :: IZS</title>
  <link rel="apple-touch-icon" href="classic/base/assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="classic/global/css/bootstrap.min.css">
  <link rel="stylesheet" href="classic/global/css/bootstrap-extend.min.css">
  <link rel="stylesheet" href="classic/base/assets/css/site.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="classic/global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="classic/global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="classic/global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="classic/global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="classic/global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="classic/global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="classic/global/vendor/bootstrap-select/bootstrap-select.css">

  <!-- Forms -->
  <link rel="stylesheet" href="classic/global/vendor/formvalidation/formValidation.css">
  <link rel="stylesheet" href="classic/base/assets/examples/css/forms/validation.css">
    
  <!-- Select box -->
  <link rel="stylesheet" href="css/select2.css">
  
  <!-- Notifications -->
  <link rel="stylesheet" href="classic/global/vendor/toastr/toastr.css">
  <link rel="stylesheet" href="classic/base/assets/examples/css/advanced/toastr.css">
  <link rel="stylesheet" href="classic/global/vendor/alertify-js/alertify.css">
  <link rel="stylesheet" href="classic/base/assets/examples/css/advanced/alertify.css">  
  
  <!-- Fonts -->
  <link rel="stylesheet" href="//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="classic/global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="classic/global/fonts/web-icons/web-icons.min.css">
  <link rel="stylesheet" href="classic/global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
  
<?php echo $strCssHead; ?>
  
  <!--[if lt IE 9]>
    <script src="classic/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="classic/global/vendor/media-match/media.match.min.js"></script>
    <script src="classic/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="classic/global/vendor/modernizr/modernizr.js"></script>
  <script src="classic/global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>

  <link rel="stylesheet" href="css/bootstrap.fd.css">
  <link rel="stylesheet" href="css/bootstrap.custom.css">
  
<?php echo $strJsHead; ?>

<?php   

  if (isset($_SERVER['HTTP_FKLD']) && (@$_SERVER['HTTP_FKLD'] == 'on')) {
    //print_r($_COOKIE);
    //print_r($_SESSION);
  }

  if (isset($arrIChatUserMap)) {
    $strToken = hash_hmac('sha256', $strIChatUser .$intIChatUserId, 'default');  
    echo '<script src="' .$strIChatUrl .'?uid=' .$strIChatUser .$intIChatUserId .'&email=' .$strUserMail .'&forename=' .$strUserFirst .'&surname=' .$strUserLast .'&appid=' .$strIChatApp .'&token=' .$strToken .'&lang="></script>' .chr(10);

    echo '<script>' .chr(10);
    echo '// Register custom component' .chr(10);
    echo 'window.smartchat.registerComponent("MyCustomIndicatorComponent", function(api, el, options) { ' .chr(10);
    echo '    return { ' .chr(10);
    echo '        link: function() { ' .chr(10);
    echo '            var uri = el.data("uri"); ' .chr(10);
    echo '            if (uri) { ' .chr(10);
    echo '                var that = this; ' .chr(10);
    echo '                api.request("context/byuri", { data : { uri : uri }}).then(function(data) { ' .chr(10);
    echo '                    that.context = data; ' .chr(10);
    echo '                    that.refresh(); ' .chr(10);
    echo '                }) ' .chr(10);
    echo '            } ' .chr(10);
    echo '        }, ' .chr(10);
    echo '        refresh: function() { ' .chr(10);
    echo '            var contextId = this.context.id;' .chr(10);
    echo '            return api.request("browse/count/topic/" + contextId, { data : { f : "open_me" }}).then(function(data_open_me) {    ' .chr(10);            
    echo '                return api.request("browse/count/topic/" + contextId, { data : { f : "done" }}).then(function(data_done) { ' .chr(10);
    echo '                    el.removeClass("done");' .chr(10);
    echo '                    el.removeClass("open_me");' .chr(10);
    echo '                    if (data_done.count > 0) { el.addClass("done"); el.attr("title", "Es gibt nur noch erledigte Chats"); }' .chr(10);
    echo '                    if (data_open_me.count > 0) { el.addClass("open_me");  el.attr("title", "Es gibt noch mindestens einen offenen Chat"); }' .chr(10);
    echo '                }) ' .chr(10);
    echo '            }) ' .chr(10);
    echo '        }, ' .chr(10);
    echo '        cmd: function(channel, data) { ' .chr(10);
    echo '            if (this.context && channel == "/topic/updated" && data && data.target == this.context.id) { ' .chr(10);
    echo '                this.refresh(); ' .chr(10);
    echo '            }      ' .chr(10); 
    echo '        } ' .chr(10);
    echo '    } ' .chr(10);
    echo '}) ' .chr(10);
    echo '</script>' .chr(10) .chr(10);

  }
?>
  
</head>
<body class="layout-boxed">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="//browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

<?php echo $strTopnav; ?>
<?php echo $strSiteMenubar; ?>
<?php echo $strSiteGridmenu; ?>

  <div id="ichat">
    <a kng smartchat data-type="inbox">Inbox</a><span kng smartchat data-type="unread" class="badge badge-danger up"></span> 
    <a class="newtab" href="<?php echo substr($strIChatUrl, 0, -4); ?>/inbox/unread" target="_blank">Open in new tab</a>
  </div>

<?php echo $strOutput; ?>

  <!-- Footer -->
  <footer class="site-footer">
    <div class="site-footer-legal">© <?php echo date('Y'); ?> <a href="//www.izs-institut.de/" target="_blank">IZS Institut für Zahlungssicherheit GmbH</a></div>
    <div class="site-footer-right"></div>
  </footer>
  <!-- Core  -->
  <script src="classic/global/vendor/jquery/jquery.js"></script>
  <script src="classic/global/vendor/bootstrap/bootstrap.js"></script>
  <script src="classic/global/vendor/animsition/animsition.js"></script>
  <script src="classic/global/vendor/asscroll/jquery-asScroll.js"></script>
  <script src="classic/global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="classic/global/vendor/asscrollable/jquery.asScrollable.all.js"></script>
  <script src="classic/global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <!-- Plugins -->
  <script src="classic/global/vendor/switchery/switchery.min.js"></script>
  <script src="classic/global/vendor/intro-js/intro.js"></script>
  <script src="classic/global/vendor/screenfull/screenfull.js"></script>
  <script src="classic/global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="classic/global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="classic/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
  <script src="classic/global/vendor/bootstrap-contextmenu/bootstrap-contextmenu.js"></script>

  <!-- Forms -->
  <script src="classic/global/vendor/formvalidation/formValidation.min.js"></script>
  <script src="classic/global/vendor/formvalidation/framework/bootstrap.min.js"></script>
  
  <!-- Select box -->
  <script src="js/select2.full.js"></script>

  <!-- Notifications -->
  <script src="classic/global/vendor/toastr/toastr.js"></script>
  <script src="classic/global/vendor/alertify-js/alertify.js"></script>
  
  <!-- Scripts -->
  <script src="classic/global/js/core.js"></script>
  <script src="classic/base/assets/js/site.js"></script>
  <script src="classic/base/assets/js/sections/menu.js"></script>
  <script src="classic/base/assets/js/sections/menubar.js"></script>
  <script src="classic/base/assets/js/sections/gridmenu.js"></script>
  <script src="classic/base/assets/js/sections/sidebar.js"></script>
  <script src="classic/global/js/configs/config-colors.js"></script>
  <script src="classic/base/assets/js/configs/config-tour.js"></script>
  <script src="classic/global/js/components/asscrollable.js"></script>
  <script src="classic/global/js/components/animsition.js"></script>
  <script src="classic/global/js/components/slidepanel.js"></script>
  <script src="classic/global/js/components/switchery.js"></script>
  <script src="classic/global/js/components/bootstrap-datepicker.js"></script>
  <script src="js/bootstrap-datepicker.de.min.js"></script>

  <script src="js/date_de.js"></script>

  <script src="js/jquery.dateformat.min.js"></script>

  <!-- Select box -->
  <script src="js/select2.js"></script>
  
  <!-- Context-menu -->
  <script src="classic/base/assets/examples/js/advanced/context-menu.js"></script>

  <script src="js/bootstrap.fd.js"></script>
  
  <script src="js/jquery.tabletojson.js"></script>
  
  <script src="js/cms.js"></script>

  <script src="/cms/js/standalone/selectize.js"></script>

<?php echo $strJsFootScript; ?>

  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();

<?php echo $strJsFootCodeRun; ?>

    });
  })(document, window, jQuery);
  </script>
</body>
</html>