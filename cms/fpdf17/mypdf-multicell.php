<?php


/**
 * Custom PDF class extention for Header and Footer Definitions
 * 
 * @author andy@interpid.eu
 *
 */
class myPDF extends FPDF {


    /**
     * Custom Header 
     *
     * @access public
     * @see FPDF::Header()
     */
    public function Header () {
    }


    /**
     * Custom Footer 
     *
     * @access public
     * @see FPDF::Footer()
     */
    public function Footer () {
      global $arrPeriod, $strCompanyGroup;
      // Position at 1.5 cm from bottom
      $this->SetY(-15);
      // symbols italic 8
      $this->SetFont('OfficinaSans','',8);
      // Page number
      if ($strCompanyGroup != '') {
        $strMonth = str_pad($arrPeriod[0], 2, "0", STR_PAD_LEFT);
        $this->Cell(0,10, utf8_decode('IZS-Prüfzertifikat '.$strMonth .'/' .$arrPeriod[1] .' für ' .$strCompanyGroup .' - Seite '.$this->PageNo().'/{nb}' .''),0,0,'R');
        //IZS-Prüfzertifikat 02/2013 für die Brunel Unternehmensgruppe - Seite 1/3
      }
    }
    
  function WriteText($text) {
  
    $intPosIni = 0;
    $intPosFim = 0;
    $intLineHeight = 5;
    
    if (strpos($text,'<')!==false && strpos($text,'[')!==false) {
      if (strpos($text,'<')<strpos($text,'[')) {
        $this->Write($intLineHeight,substr($text,0,strpos($text,'<')));
        $intPosIni = strpos($text,'<');
        $intPosFim = strpos($text,'>');
        $this->SetFont('','B');
        $this->Write($intLineHeight,substr($text,$intPosIni+1,$intPosFim-$intPosIni-1));
        $this->SetFont('','');
        $this->WriteText(substr($text,$intPosFim+1,strlen($text)));
      } else {
        $this->Write($intLineHeight,substr($text,0,strpos($text,'[')));
        $intPosIni = strpos($text,'[');
        $intPosFim = strpos($text,']');
        $w=$this->GetStringWidth('a')*($intPosFim-$intPosIni-1);
        $this->Cell($w,$this->FontSize+0.75,substr($text,$intPosIni+1,$intPosFim-$intPosIni-1),1,0,'');
        $this->WriteText(substr($text,$intPosFim+1,strlen($text)));
      }
    } else {
      if (strpos($text,'<')!==false) {
        $this->Write($intLineHeight,substr($text,0,strpos($text,'<')));
        $intPosIni = strpos($text,'<');
        $intPosFim = strpos($text,'>');
        $this->SetFont('','B');
        $this->WriteText(substr($text,$intPosIni+1,$intPosFim-$intPosIni-1));
        $this->SetFont('','');
        $this->WriteText(substr($text,$intPosFim+1,strlen($text)));
      } elseif (strpos($text,'[')!==false) {
        $this->Write($intLineHeight,substr($text,0,strpos($text,'[')));
        $intPosIni = strpos($text,'[');
        $intPosFim = strpos($text,']');
        $w=$this->GetStringWidth('a')*($intPosFim-$intPosIni-1);
        $this->Cell($w,$this->FontSize+0.75,substr($text,$intPosIni+1,$intPosFim-$intPosIni-1),1,0,'');
        $this->WriteText(substr($text,$intPosFim+1,strlen($text)));
      } else {
        $this->Write($intLineHeight,$text);
      }
    }
  }
  
  function SetDash($black=false, $white=false) {
      if($black and $white) {
          $s=sprintf('[%.3f %.3f] 0 d', $black*$this->k, $white*$this->k);
      } else {
          $s='[] 0 d';
      }
      $this->_out($s);
  }
}

