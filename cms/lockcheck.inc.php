<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

include('conf/global.inc.php');
require_once('../assets/classes/class.mysql.php');
require_once('../assets/classes/class.database.php');

$strOutput = '';

$strOutput.= '<h1>Sperrvermerk Check</h1>' .chr(10);


$arrStatusCss = array ( 
  'abgelehnt / Dublette' => 'warning', 
  'abgelehnt / Frist' => 'warning', 
  'accepted' => 'warning', 
  'bereit für REVIEW' => 'warning', 
  'enquired' => 'wait', 
  'in progress' => 'warning', 
  'new' => 'warning', 
  'no Feedback' => 'question', 
  'no result' => 'warning', 
  'not assignable' => 'warning', 
  'not OK' => 'warning', 
  'OK' => 'thick', 
  'refused' => 'warning', 
  'to clear' => 'warning', 
  'to enquire' => 'warning', 
  'zugeordnet / abgelegt' => 'warning', 
  'zurückgestellt von IZS' => 'warning'
);

$arrSichtbar = array (
  'online' => 'ja',
  'private' => 'nein',
  'ready' => 'auf Anfrage'  
);

$arrSperr = array (
  'true' => 'ja',
  'false' => 'nein'
);

// SUCHE BEGIN 

$strSql0 = "SELECT DISTINCT `Beitragsmonat__c`, `Beitragsjahr__c` FROM `SF42_IZSEvent__c` ORDER BY `Beitragsjahr__c` DESC, `Beitragsmonat__c` DESC";
$arrResult0 = MySQLStatic::Query($strSql0);
if (count($arrResult0) > 0) {
  
  $strOutput.= '<form method="post" action="">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);

  $strOutput.= '<table>' .chr(10);
  $strOutput.= '<tbody>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Zeitraum: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelDate" id="strSelDate">' .chr(10);
  
  $intPeriod = 0;

  foreach ($arrResult0 as $intKey => $arrPeriod0) {
    $strSelected = '';
    $strValue = $arrPeriod0['Beitragsmonat__c'] .'_' .$arrPeriod0['Beitragsjahr__c'];
    
    if (empty($_REQUEST['strSelDate']) && ($intPeriod == 0)) {
      $_REQUEST['strSelDate'] = $strValue;
      //$_REQUEST['send'] = 1;
    }
    
    if ($strValue == $_REQUEST['strSelDate']) {
      $strSelected = ' selected="selected"';
      $strActMonth = $strValue;
      $strLasMonth = $arrResult0[$intKey+1]['Beitragsmonat__c'] .'_' .$arrResult0[$intKey+1]['Beitragsjahr__c'];
    } 
    $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .str_pad($arrPeriod0['Beitragsmonat__c'], 2 ,'0', STR_PAD_LEFT) .'-' .$arrPeriod0['Beitragsjahr__c'] .'</option>' .chr(10);
    $intPeriod++;
  }
  
  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);


  $strOutput.= '</tbody>' .chr(10);

  $strOutput.= '  <tfoot>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td align="right" colspan="2"><input type="submit" name="Suche" value="Suche" /></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);
  $strOutput.= '  </tfoot>' .chr(10);


  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);
    
}

if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {
  
  $arrPeriod = explode('_', $_REQUEST['strSelDate']);

  $strSql = 'SELECT `evid`, `Id`, `Name`, `SF42_EventStatus__c`, `SF42_Premium_Payer__c`, `Status_Klaerung__c`, ';
  $strSql.= '`Betriebsnummer_IP__c`, `Betriebsnummer_ZA__c`, `SF42_informationProvider__c`, ';
  $strSql.= '`SF42_EventComment__c`, `Art_des_Dokuments__c`, `SF42_PublishingStatus__c`, `Sperrvermerk__c`, `SF42_DocumentUrl__c`, ';
  $strSql.= '`RecordTypeId`, `SF42_EventStatus__c`, `bis_am__c`, `group_id`, `SF42_EventComment__c` ';
  $strSql.= 'FROM `SF42_IZSEvent__c` ';
  $strSql.= 'WHERE ((`Sperrvermerk__c` = "true") ';
  $strSql.= 'OR (`SF42_PublishingStatus__c` IN ("ready"))) ';

  $strSql.= 'AND (`Beitragsmonat__c` = "' .$arrPeriod[0] .'") AND (`Beitragsjahr__c` = "' .$arrPeriod[1] .'") ';
  $strSql.= 'ORDER BY `Betriebsnummer_ZA__c` ASC';
  

  $arrResult = MySQLStatic::Query($strSql);
  
  $arrInformationProvider = array();
  
  if (count($arrResult) > 0) {
    
    $arrInformationProvider = $arrResult;
  
  }
  
  if (count($arrInformationProvider) > 0) {
    //natcasesort($arrInformationProvider);

/*
-> Detail
*/
  
    $strOutputTable = '';
    $strOutputTable.= '<table class="" id="5">' .chr(10);
    $strOutputTable.= '  <thead>' .chr(10);
    $strOutputTable.= '    <tr>' .chr(10);
  	$strOutputTable.= '      <th>Status</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th>Event-ID</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th>BNR IP</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th>Information Provider</th>' .chr(10);
  	$strOutputTable.= '      <th>BNR PP</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th>Premium Payer</th>' .chr(10); //class="header"

    $strOutputTable.= '      <th>Sichtbar</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th>Sperrvermerk</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th>Kommentar auf Portal</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th>Link</th>' .chr(10); //class="header"
    $strOutputTable.= '    </tr>' .chr(10);
    $strOutputTable.= '  </thead>' .chr(10);
    $strOutputTable.= '  <tbody>' .chr(10);
    
    $arrPeriod = explode('_', $_REQUEST['strSelDate']);
    
    $strNext = '';
  
    foreach ($arrInformationProvider as $strId => $arrEvent) {
      
      if (($arrEvent['Sperrvermerk__c'] == 'true') && ($arrEvent['SF42_PublishingStatus__c'] != 'ready')) {
        $strTrClass = 'bgred';
      } else if (($arrEvent['Sperrvermerk__c'] != 'true') && ($arrEvent['SF42_PublishingStatus__c'] == 'ready')) {
        $strTrClass = 'bgred';
      } else {
        $strTrClass = '';
      }
      
      if ($strNext == 'blubb') {
        $strNext = $arrEvent['Id'];
      }
      
      if (($_REQUEST['done'] != '') && ($_REQUEST['done'] == $arrEvent['Id'])) {
        $strNext = 'blubb';
      }
      
      $strOutputTable.= '    <tr class="' .$strTrClass .'" id="e_' .$arrEvent['Id'] .'">' .chr(10);
      $strOutputTable.= '      <td align="center"><img src="/assets/images/sys/status_' .$arrStatusCss[$arrEvent['SF42_EventStatus__c']] .'.png" /> ' .$arrEvent['SF42_EventStatus__c'] .'</td>' .chr(10);
      $strOutputTable.= '      <td><a target="_blank" href="/cms/index.php?ac=sear&det=' .$arrEvent['evid'] .'">' .$arrEvent['Name'] .'</a></td>' .chr(10);
  
      $strSql = 'SELECT `Name`, `SF42_Comany_ID__c` FROM `Account` WHERE `Id` = "' .$arrEvent['SF42_informationProvider__c'] .'"';
      $arrSql = MySQLStatic::Query($strSql);
      $strOutputTable.= '      <td>' .$arrSql[0]['SF42_Comany_ID__c'] .'</td>' .chr(10);
      $strOutputTable.= '      <td>' .$arrSql[0]['Name'] .'</td>' .chr(10);
  
      $strOutputTable.= '      <td>' .$arrEvent['Betriebsnummer_ZA__c'] .'</td>' .chr(10);
      $strOutputTable.= '      <td>' .$arrEvent['SF42_Premium_Payer__c'] .'</td>' .chr(10);

      $strOutputTable.= '      <td>' .$arrSichtbar[$arrEvent['SF42_PublishingStatus__c']] .'</td>' .chr(10); //
      $strOutputTable.= '      <td>' .$arrSperr[$arrEvent['Sperrvermerk__c']] .'</td>' .chr(10); //
      $strOutputTable.= '      <td>' .$arrEvent['SF42_EventComment__c'] .'</td>' .chr(10);
      
      if ($arrEvent['SF42_DocumentUrl__c'] != '') {
        $strDL = '<a href="' .$arrEvent['SF42_DocumentUrl__c'] .'" target="_blank"><img src="img/icon_pdf.png" alt="' .$arrEvent['SF42_DocumentUrl__c'] .'" /></a>';
      } else {
        $strDL = '';
      }
      $strOutputTable.= '      <td id="file_e_' .$arrEvent['Id'] .'_l">' .$strDL .'</td>' .chr(10);
      
      $strOutputTable.= '    </tr>' .chr(10);
      
    }
      
    $strOutputTable.= '  </tbody>' .chr(10);
    $strOutputTable.= '</table>' .chr(10);
  
  
    
    $strOutput.= '<div class="clearfix" style="height: 48px; width: 1094px;">' .chr(10);
    $strOutput.= '  <div class="form-left">' .chr(10);
    $strOutput.= '    <h3>' .count($arrInformationProvider) .' Ergebnisse gefunden:</h3>' .chr(10);
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' .chr(10);
    $strOutput.= '    <form method="post" action="_get_csv.php">';
    $strOutput.= '    <input type="hidden" name="strTable" value="' .str_replace('"', "'", $strOutputTable) .'">' .chr(10);
    $strOutput.= '    <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' .chr(10);
    $strOutput.= '    </form>';
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '</div>' .chr(10);
    
    $strOutput.= $strOutputTable;
    
    $strOutput.= "
<script>
$('#5').fixheadertable({ 
   colratio    : [170, 80, 80, 280, 80, 280, 100, 100, 280, 40], 
   height      : 700, 
   width       : 1520, 
   zebra       : true, 
   resizeCol   : true,
   sortable    : true,
   sortType    : ['string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string'],
   sortedColId : 4, 
   dateFormat  : 'Y-m'
});
</script>
";
  
  
  } else {
    
    $strOutput.= '<p>Keine Events im gewählten Zeitraum vorhanden.</p>' .chr(10);
    
  }
}


?>