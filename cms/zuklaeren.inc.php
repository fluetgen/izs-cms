<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

include('conf/global.inc.php');
require_once('../assets/classes/class.mysql.php');
require_once('../assets/classes/class.database.php');

if ($_SERVER['REMOTE_ADDR'] == '128.90.130.199') {
  //print_r($_REQUEST);
  //echo $strSql2;
}

//print_r($_REQUEST);

function replace_placeholder($strText = '', $intId) {
  global $arrPlaceholder;
  
  $strReturn = $strText;
  
  $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$_REQUEST['det'] .'"';
  $event  = MySQLStatic::Query($strSql);

  $strSql = 'SELECT * FROM `Account` WHERE `Id` = "' .$event[0]['SF42_informationProvider__c'] .'"';
  $Ip  = MySQLStatic::Query($strSql); // == KK

  $strSql = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$event[0]['Betriebsnummer_ZA__c'] .'"';
  $Pp  = MySQLStatic::Query($strSql);

  $strReturn = str_replace('{Month}', $event[0]['SF42_Month__c'], $strReturn);  
  $strReturn = str_replace('{Year}', $event[0]['SF42_Year__c'], $strReturn);  

  $strReturn = str_replace('{Salutation}', $Ip[0]['Anrede_Anschreiben__c'] .' ' .$Ip[0]['Nachname__c'], $strReturn);  
  $strReturn = str_replace('{Beitragszahler}', $Pp[0]['Name'], $strReturn);  
  $strReturn = str_replace('{BNR Beitragszahler}', $Pp[0]['SF42_Comany_ID__c'], $strReturn);  
  $strReturn = str_replace('{Krankenkasse}', $Ip[0]['Name'], $strReturn);  
  $strReturn = str_replace('{Sachbearbeiter KK}', $Ip[0]['Anrede_Briefkopf__c'] .' ' .$Ip[0]['Nachname__c'], $strReturn);  
  $strReturn = str_replace('{Sachbearbeiter KK Telefon}', $Ip[0]['Phone'], $strReturn);  
  $strReturn = str_replace('{Erledigung bis}', $event[0]['bis_am__c'], $strReturn);  
  $strReturn = str_replace('{Event Id}', $event[0]['Name'], $strReturn);  
  
  //$strReturn = str_replace('{}', [0][''], $strReturn);  
  
  return $strReturn;
}

$boolShowIp = false;
$strOutput = '';

$strOutput.= '<h1>Klärung</h1>' .chr(10);

$arrSystemStream = array (
  'SF42_OnlineStatus__c' => 'änderte Online Status', 
  'Info__c' => 'änderte globale Status-Info', 
  'bis_am__c' => 'änderte Wiedervorlage-Datum', 
  'RecordTypeId' => 'änderte Phase', 
  'SF42_EventStatus__c' => 'änderte Ergebnis', 
  'SF42_PublishingStatus__c' => 'änderte Sichtbarkeit des Dokuments auf dem Portal', 
  'Art_des_Dokuments__c' => 'änderte Art des Dokuments', 
  'Auskunft_von__c' => 'änderte Auskunftsgeber', 
  'Sperrvermerk__c' => 'änderte Sperrvermerk-Status', 
  'SF42_EventComment__c' => 'änderte den auf dem Portal sichtbaren Kommentar', 
  'Rueckmeldung_am__c' => 'änderte Rückmelde-Datum', 
  'Art_der_Rueckmeldung__c' => 'änderte Art der Rückmeldung', 
  'Status_Klaerung__c' => 'änderte den Klärungs-Status', 
  'Grund__c' => 'änderte den Grund für die Klärung', 
  'Beitragsrueckstand__c' => 'änderte die Höhe des Beitragsrückstands', 
  'Naechster_Meilenstein__c' => 'änderte die Information zum nächsten Schritt', 
);

$arrRequestWay = array();
$arrDecentral = array();

$arrRecordType = array (
  'Delivery' => '01230000001Ao73AAC',
  'FollowUp' => '01230000001Ao74AAC',
  'Publication' => '01230000001Ao75AAC',
  'Review' => '01230000001Ao76AAC'
);

$arrRecordTypeFlip = array_flip($arrRecordType);

$arrDokArt = array (
  'Kontoauskunft' => 'Form',
  'Kontoauszug' => 'KtoA',
  'Unbedenklichkeitsbescheinigung' => 'UBB',
  'Sonstiges' => 'Sonst.'
);

$arrChangePreview = array ( 
  'abgelehnt / Dublette' => 'abgelehnt / Dublette', 
  'abgelehnt / Frist' => 'abgelehnt / Frist', 
  'accepted' => 'angenommen', 
  'bereit für REVIEW' => 'bereit für REVIEW', 
  'enquired' => 'angefragt', 
  'in progress' => 'in Bearbeitung', 
  'new' => 'neu', 
  'no Feedback' => 'keine Antwort', 
  'no result' => 'NO RESULT', 
  'not assignable' => 'nicht zuordenbar', 
  'not OK' => 'NOT OK', 
  'OK' => 'OK', 
  'refused' => 'abgelehnt', 
  'to clear' => 'zu klären', 
  'to enquire' => 'anzufragen', 
  'zugeordnet / abgelegt' => 'zugeordnet / abgelegt', 
  'zurückgestellt von IZS' => 'zurückgestellt von IZS'
);

$arrOnline = array (
  'true' => 'sichtbar',
  'false' => 'nicht sichtbar'
);

$arrSperr = array (
  'true' => 'ja',
  'false' => 'nein'
);

$arrSichtbar = array (
  'online' => 'ja',
  'private' => 'nein',
  'ready' => 'auf Anfrage'  
);

$arrAuskunft = array (
  'Krankenkasse' => 'Krankenkasse',
  'Zeitarbeitsunternehmen' => 'Zeitarbeitsunternehmen',
);

$arrRueck = array (
  'Email' => 'Email',
  'Fax' => 'Fax',
  'Post' => 'Post'  
);

$arrStatusKlaerung = array (
  'in Klärung' => 'in Klärung',
  'geschlossen / positiv' => 'geschlossen / positiv',
  'geschlossen / negativ' => 'geschlossen / negativ'  
);


$arrGrundAnz = array('keine Angabe' => 0);
foreach ($arrGrund as $strGrund) {
  $arrGrundAnz[$strGrund] = 0;
}

$arrGrundAus = array(0 => 'keine Angabe');
foreach ($arrGrund as $strGrund) {
  $arrGrundAus[] = $strGrund;
}
$arrGrundAusFlip = array_flip($arrGrundAus);

if ($_SERVER['REMOTE_ADDR'] == '128.90.130.199') {
  //print_r($arrGrundAus);
  //echo $strSql2;
}

$arrMeilAus = array(0 => 'keine Angabe');
foreach ($arrMeilenstein as $strMeilenstein) {
  $arrMeilAus[] = $strMeilenstein;
}
$arrMeilAusFlip = array_flip($arrMeilAus);

//print_r($arrMeilenstein); die();


$arrGrundSearch = array();
$arrMeilSearch  = array();

foreach ($_REQUEST as $strName => $intValue) {
  if ($intValue == 1) {
    if (strstr($strName, 'grund_')) {
      $arrPart = explode('_h', $strName);
      $arrPartTwo = explode('grund_', $arrPart[0]);
      $arrGrundSearch[] = '' .$arrGrundAus[$arrPartTwo[1]] .'';

      if ($_SERVER['REMOTE_ADDR'] == '128.90.130.199') {
        //echo $arrPartTwo[1] .' | ';
        //echo $strSql2;
      }

    }
    if (strstr($strName, 'meil_')) {
      $arrPart = explode('_h', $strName);
      $arrPartTwo = explode('meil_', $arrPart[0]);
      $arrMeilSearch[] = '' .$arrMeilAus[$arrPartTwo[1]] .'';
    }
  }
}

if ($_SERVER['REMOTE_ADDR'] == '128.90.130.199') {
  //print_r($arrGrundSearch);
  //echo $strSql2;
}

if (true) {

// SUCHE BEGIN 

$strSql0 = "SELECT DISTINCT `Beitragsmonat__c`, `Beitragsjahr__c` FROM `SF42_IZSEvent__c` ORDER BY `Beitragsjahr__c` DESC, `Beitragsmonat__c` DESC";
$arrResult0 = MySQLStatic::Query($strSql0);
if (count($arrResult0) > 0) {
  
  $strOutput.= '<div class="clearfix">' .chr(10);
  
  $strOutput.= '<div class="form-left">' .chr(10);
  $strOutput.= '<form method="post" action="">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);

  $strOutput.= '<table>' .chr(10);
  $strOutput.= '<tbody>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td style="width: 110px;">Zeitraum: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelDate" id="strSelDate">' .chr(10);

  $strSelectedAll = '';
  if ($_REQUEST['strSelDate'] == 'ALL') {
    $strSelectedAll = ' selected="selected"';
  } 
  $strOutput.= '  <option value="ALL"' .$strSelectedAll .'>Alle</option>' .chr(10);
  
  $intPeriod = 0;

  foreach ($arrResult0 as $arrPeriod0) {
    $strSelected = '';
    $strValue = $arrPeriod0['Beitragsmonat__c'] .'_' .$arrPeriod0['Beitragsjahr__c'];
    
    if (empty($_REQUEST['strSelDate']) && ($intPeriod == 0)) {
      $_REQUEST['strSelDate'] = $strValue;
      //$_REQUEST['send'] = 1;
    }
    
    if ($strValue == $_REQUEST['strSelDate']) {
      $strSelected = ' selected="selected"';
    } 
    $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .str_pad($arrPeriod0['Beitragsmonat__c'], 2 ,'0', STR_PAD_LEFT) .'-' .$arrPeriod0['Beitragsjahr__c'] .'</option>' .chr(10);
    $intPeriod++;
  }
  
  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Event: </td>' .chr(10);
  $strOutput.= '    <td>E- <input type="text" name="strSearchValue" value="' .$_REQUEST['strSearchValue'] .'"></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $arrStatusCss = array ( 
    'abgelehnt / Dublette' => 'warning', 
    'abgelehnt / Frist' => 'warning', 
    'accepted' => 'warning', 
    'bereit für REVIEW' => 'warning', 
    'enquired' => 'wait', 
    'in progress' => 'warning', 
    'new' => 'warning', 
    'no Feedback' => 'question', 
    'no result' => 'warning', 
    'not assignable' => 'warning', 
    'not OK' => 'warning', 
    'OK' => 'thick', 
    'refused' => 'warning', 
    'to clear' => 'warning', 
    'to enquire' => 'warning', 
    'zugeordnet / abgelegt' => 'warning', 
    'zurückgestellt von IZS' => 'warning'
  );

  $arrStatus = $arrChangePreview;
    
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Anfragestelle: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelAnf" id="strSelAnf" class="sea">' .chr(10);
  $strOutput.= '      <option value="all">Alle Anfragestellen</option>' .chr(10);
  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Company Group: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelGru" id="strSelGru" class="sea">' .chr(10);
  $strOutput.= '      <option value="all">Alle Company Groups</option>' .chr(10);
  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Status: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelSta" id="strSelSta" class="sea">' .chr(10);
  $strOutput.= '      <option value="all">Alle Status</option>' .chr(10);
  
  if ($_REQUEST['strSelSta'] == '') {
    $_REQUEST['strSelSta'] = 'in Klärung';
  }

  foreach ($arrStatusKlaerung as $strKey => $strValue) {
    $strSelected = '';
    
    if ($strValue == $_REQUEST['strSelSta']) {
      $strSelected = ' selected="selected"';
    } 
    $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .$strValue .'</option>' .chr(10);
  }
  
  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $arrStundung = array(
    'Stundung' => 'Stundung',
    'Ratenzahlung' => 'Ratenzahlung',
    'Stundung oder Ratenzahlung' => 'Stundung oder Ratenzahlung',
  );
    
  if (count($arrStundung) > 0) {

    if (($_REQUEST['strSelStu'] == 'all') || ($_REQUEST['strSelStu'] == '')) {
      $strSelectedAll = ' selected="selected"';
    } else {
      $strSelectedAll = '';
    }

    if ($_REQUEST['strSelStu'] == 'empty') {
      $strSelectedEmpty = ' selected="selected"';
    } else {
      $strSelectedEmpty = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td width="160">Stundung/Ratenzahlung: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelStu" id="strSelStu">' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle anzeigen -</option>' .chr(10);
    $strOutput.= '    <option value="empty"' .$strSelectedEmpty .'>kein Eintrag</option>' .chr(10);
    
    foreach ($arrStundung as $strKey => $strStundung) {
      $strSelected = '';
      if ($strKey == $_REQUEST['strSelStu']) {
        $strSelected = ' selected="selected"';
      } 
      
      $strOutput.= '    <option value="' .$strKey .'"' .$strSelected .'>' .$strStundung .'</option>' .chr(10);
      //$strLastName = $arrProvider['Name'];
      
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }

  
  //if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {
  
    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Grund: </td>' .chr(10);
    
    $strOutput.= '    <td>';

//print_r($arrGrundSearch);
//print_r($arrMeilSearch);

    foreach ($arrGrundAus as $intKey => $strGrund) {
      if (in_array($strGrund, $arrGrundSearch)) {
        $strClassAdd = ' activeF';
        $strValue = 1;
      } else {
        $strClassAdd = '';
        $strValue = 0;
      }
      $strOutput.= '<input type="hidden" id="grund_' .$intKey .'_h" name="grund_' .$intKey .'_h" value="' .$strValue .'" />' .chr(10);
      $strOutput.= '<span class="filterG' .$strClassAdd .'" id="grund_' .$intKey .'">' .$strGrund .'</span>';
    }
    $strOutput.= '</td>';
    $strOutput.= '  </tr>' .chr(10);
    
    if ($_REQUEST['ohne_h'] == 1) {
      $strValueOhne = 1;
      $strValueOhneClass = ' activeF';
    } else {
      $strValueOhne = 0;
      $strValueOhneClass = '';
    }
    if ($_REQUEST['faellig_h'] == 1) {
      $strValueFaellig = 1;
      $strValueFaelligClass = ' activeF';
    } else {
      $strValueFaellig = 0;
      $strValueFaelligClass = '';
    }
    if ($_REQUEST['heute_h'] == 1) {
      $strValueHeute = 1;
      $strValueHeuteClass = ' activeF';
    } else {
      $strValueHeute = 0;
      $strValueHeuteClass = '';
    }
    if ($_REQUEST['bald_h'] == 1) {
      $strValueBald = 1;
      $strValueBaldClass = ' activeF';
    } else {
      $strValueBald = 0;
      $strValueBaldClass = '';
    }
    
    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Wiedervorlage: </td>' .chr(10);
    $strOutput.= '    <td><input type="hidden" id="ohne_h" name="ohne_h" value="' .$strValueOhne .'" /><input type="hidden" id="faellig_h" name="faellig_h" value="' .$strValueFaellig .'" /><input type="hidden" id="heute_h" name="heute_h" value="' .$strValueHeute .'" /><input type="hidden" id="bald_h" name="bald_h" value="' .$strValueBald .'" />' .chr(10);
    $strOutput.= '    <span class="filterW' .$strValueOhneClass .'" id="ohne">ohne</span><span class="filterW' .$strValueFaelligClass .'" id="faellig">überfällig</span><span class="filterW' .$strValueHeuteClass .'" id="heute">heute</span><span class="filterW' .$strValueBaldClass .'" id="bald">demnächst</span></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  
    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Nächster Schritt: </td>' .chr(10);
    
    $strOutput.= '    <td>';
    foreach ($arrMeilAus as $intKey => $strMeilenstein) {
      if (in_array($strMeilenstein, $arrMeilSearch)) {
        $strClassAdd = ' activeF';
        $strValue = 1;
      } else {
        $strClassAdd = '';
        $strValue = 0;
      }      
      $strOutput.= '<input type="hidden" id="meil_' .$intKey .'_h" name="meil_' .$intKey .'_h" value="' .$strValue .'" />' .chr(10);
      $strOutput.= '<span class="filterM' .$strClassAdd .'" id="meil_' .$intKey .'">' .$strMeilenstein .'</span>';
    }
    $strOutput.= '</td>';
    $strOutput.= '  </tr>' .chr(10);

  //}

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td align="right" colspan="2"><input type="submit" name="Suche" value="Suche" /></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $strOutput.= '</tbody>' .chr(10);

  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);

  
  $strSql = 'SELECT `ct_id`, `ct_text` FROM `cms_text` WHERE `ct_category` = "Hinweis Klärung"';
  $resHinweis = MySQLStatic::Query($strSql);
  
  if (count($resHinweis) > 0) {
    $strHinweis = $resHinweis[0]['ct_text'];
  }
  
  if ($_SESSION['id'] <= 3) {
    $strOutput.= '<textarea id="hinweis" style="margin-left: 160px; width: 453px; height: 80px;">' .$strHinweis .'</textarea>&nbsp;' .chr(10);
    $strOutput.= '<input type="button" id="hinweisSave" value="Speichern" style="vertical-align: bottom;">' .chr(10);
  } else {
    $strOutput.= '<textarea id="hinweis" readonly="readonly" style="margin-left: 110px; width: 453px; height: 80px;">' .$strHinweis .'</textarea></br>' .chr(10);
  }
  $strOutput.= '</div>' .chr(10);
  
  
  $strOutput.= '<script>
  
  $("#hinweisSave").bind(\'click\', function() {

    $.ajax({
      type: "POST",
      url:  "_ajax.php",
      dataType: "html; charset=utf-8", 
      data: { ac: "hsave", id: ' .$resHinweis[0]['ct_id'] .', ct_text: $("#hinweis").val() },
    });
    
  });
  
  
  $(".filterG").bind(\'click\', function() {
    if ($(this).hasClass("activeF")) {
      $(this).removeClass("activeF");
      $("#" + this.id +"_h").val(0);
    } else {
      $(this).addClass("activeF");
      $("#" + this.id +"_h").val(1);
    }
      
    
  });
  $(".filterW").bind(\'click\', function() {
  
    if ($(this).hasClass("activeF")) {
      $(this).removeClass("activeF");
      $("#" + this.id +"_h").val(0);
    } else {
      $(this).addClass("activeF");
      $("#" + this.id +"_h").val(1);
    }
      
  });

  $(".filterM").bind(\'click\', function() {
  
    if ($(this).hasClass("activeF")) {
      $(this).removeClass("activeF");
      $("#" + this.id +"_h").val(0);
    } else {
      $(this).addClass("activeF");
      $("#" + this.id +"_h").val(1);
    }
      
  });
  
</script>' .chr(10);


  $strOutput.= '<div class="form-right">' .chr(10);
  
  if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {
  
    $strOutput.= '    <div id="piechart" style="width: 570px; height: 300px;"></div>' .chr(10);
    
  }
  
  $strOutput.= '</div>' .chr(10);
  $strOutput.= '</div>' .chr(10);

    
  $strOutput.= '    
<script type="text/javascript">
  $(document).ready(function() {
  
  var id1 = $("#strSelDate").val();
';

if ($_REQUEST['strSelAnf'] != '') {
  $strOutput.= '  var id2 = "' .$_REQUEST['strSelAnf'] .'";' .chr(10);  
} else {
  $strOutput.= '  var id2 = $("#strSelAnf").val();' .chr(10); 
}

  $strOutput.= '    
  $.get("_ajax_clear.php", { search1: id1, search2: id2 }, function( data ) {
    $("#strSelAnf").html(data.split("|")[0]);
    $("#strSelGru").html(data.split("|")[1]);
';

if ($_REQUEST['strSelAnf'] != '') {
  $strOutput.= '    $("#strSelAnf").val("' .$_REQUEST['strSelAnf'] .'");' .chr(10);  
}
if ($_REQUEST['strSelGru'] != '') {
  $strOutput.= '    $("#strSelGru").val("' .$_REQUEST['strSelGru'] .'");' .chr(10);  
}

  $strOutput.= '    
  });
  
});

  $("#strSelDate").change(function(){
    var id1 = $("#strSelDate").val();
    $.get("_ajax_clear.php", { search1: id1 }, function( data ) {
      $("#strSelAnf").html(data.split("|")[0]);
      $("#strSelGru").html(data.split("|")[1]);
    });
  });

</script>
';   
    
}

if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {

  if (in_array($_REQUEST['strSelSta'], $arrChangePreview) == true) { 
    $boolMore = true;
  } else {
    $boolMore = false;
  }
  
  if ($_REQUEST['strSelDate'] != 'ALL') {
    $arrPeriod = explode('_', $_REQUEST['strSelDate']);
  }
/* NEU */

  $strSql2 = 'SELECT `evid`, `SF42_IZSEvent__c`.`Name` AS `EName`, `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c`, ';
  $strSql2.= '`SF42_IZSEvent__c`.`SF42_informationProvider__c`, `Acc`.`Id`, `Acc`.`Name` AS `PPname`, ';
  $strSql2.= '`SF42_IZSEvent__c`.`Betriebsnummer_ZA__c` AS `PPbtnr`, `Account`.`Dezentrale_Anfrage__c`, ';
  $strSql2.= '`Account`.`Name`, `Group__c`.`Id` AS `IdG`, `Group__c`.`Name` AS `NameG`, ';
  $strSql2.= '`Grund__c`, `Naechster_Meilenstein__c`, `bis_am__c`, `SF42_IZSEvent__c`.`Info__c`, `Status_Klaerung__c`, ';
  $strSql2.= '`AP_Klaerung`, `Tel_Klaerung`, `SF42_Month__c`,  `SF42_Year__c`, `Beitragsrueckstand__c`';
  $strSql2.= 'FROM `SF42_IZSEvent__c` ';
  $strSql2.= 'INNER JOIN `Account` AS `Acc` ON `Acc`.`SF42_Comany_ID__c` = `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c` ';
  $strSql2.= 'INNER JOIN `Group__c` ON `Group__c`.`Id` = `Acc`.`SF42_Company_Group__c` ';
  $strSql2.= 'INNER JOIN `Account` ON `Account`.`Id` = `SF42_IZSEvent__c`.`SF42_informationProvider__c` ';
  $strSql2.= 'WHERE `Liefert_Daten__c` = "true" ';
  
  if ($_REQUEST['strSelDate'] != 'ALL') {
    $strSql2.= 'AND `SF42_Month__c` = "' .$arrPeriod[0] .'" AND `SF42_Year__c` = "' .$arrPeriod[1] .'" ';
  }
  
  if (($_REQUEST['strSelSta'] != 'all') && ($_REQUEST['strSelSta'] != '')) {
    $strSql2.= 'AND `Status_Klaerung__c` = "' .$_REQUEST['strSelSta'] .'" ';
  } else {
    $strSql2.= 'AND `Status_Klaerung__c` IN ("in Klärung", "geschlossen / positiv", "geschlossen / negativ") ';
  }
  
  if (($_REQUEST['strSelGru'] != 'all') && ($_REQUEST['strSelGru'] != '')) {
    $strSql2.= 'AND `Group__c`.`Id` = "' .$_REQUEST['strSelGru'] .'" ';
  }
  if ($_REQUEST['strSearchValue'] != '') {
    $strSql2.= 'AND `SF42_IZSEvent__c`.`Name` LIKE "E-' .$_REQUEST['strSearchValue'] .'%" ';
  }

  if ($_REQUEST['Grund2'] != '') {
    //$arrGrundSearch = array($_REQUEST['Grund2']);
  }
  
  if (count($arrGrundSearch) > 0) {
    $strGrundSql = str_replace("keine Angabe", '', '"' .implode('", "', $arrGrundSearch) .'"');
    $strSql2.= 'AND `Grund__c` IN (' .$strGrundSql .') ';
  }
  
  if (count($arrMeilSearch) > 0) {
    $strMeilSql = str_replace("keine Angabe", '', '"' .implode('", "', $arrMeilSearch) .'"');
    $strSql2.= 'AND `Naechster_Meilenstein__c` IN (' .$strMeilSql .') ';
  }
  
  $strSqlTime = '';
  $strToday = date('Y-m-d');

  if ($_REQUEST['ohne_h'] == 1) {
    $strSqlTime.= '`bis_am__c` = ""';
  } 
  if ($_REQUEST['faellig_h'] == 1) {
    if ($strSqlTime != '') {
      $strSqlTime.= ' OR ';
    }
    $strSqlTime.= '`bis_am__c` < "' .$strToday .'"';
  }
  if ($_REQUEST['heute_h'] == 1) {
    if ($strSqlTime != '') {
      $strSqlTime.= ' OR ';
    }
    $strSqlTime.= '`bis_am__c` = "' .$strToday .'"';
  }
  if ($_REQUEST['bald_h'] == 1) {
    if ($strSqlTime != '') {
      $strSqlTime.= ' OR ';
    }
    $strSqlTime.= '`bis_am__c` > "' .$strToday .'"';
  }
  
  if ($strSqlTime != '') {
    $strSql2.= 'AND (' .$strSqlTime .') ';
  }

  if (($_REQUEST['strSelStu'] != '') && ($_REQUEST['strSelStu'] != 'all')) {
    if ($_REQUEST['strSelStu'] == 'empty') {
      $strSql2.= 'AND (`Stundung__c` = "") ';
    } else {
      $strSql2.= 'AND (`Stundung__c` = "' .$_REQUEST['strSelStu'] .'") ';
    }
  }
  
  if ($_SERVER['REMOTE_ADDR'] == '128.90.130.199') {
    //var_dump($_REQUEST['Grund2']);
    //echo $strSql2;
  }

  $arrResult2 = MySQLStatic::Query($strSql2);
  
  if (count($arrResult2) > 0) {
    
    $arrOptionA = array();
    $arrOptionC = array();
    
    $arrSel = explode('_', $_REQUEST['strSelAnf']);
    $strSel = $arrSel[0];
    
    $arrEventList = array();
    
    foreach ($arrResult2 as &$arrEvent) {
      
      $boolSecond = false;
      
      if ($arrEvent['Dezentrale_Anfrage__c'] == 'true') { //DEZENTRAL
      
        $strSql4 = 'SELECT `Anfragestelle__c`  FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrEvent['Id'] .'" AND `Information_Provider__c` = "' .$arrEvent['SF42_informationProvider__c'] .'"';
        $arrResult4 = MySQLStatic::Query($strSql4);
        
        if (count($arrResult4) > 0) {
        
          $strSql5 = 'SELECT `Id`, `Name` FROM `Anfragestelle__c` WHERE `Id` = "' .$arrResult4[0]['Anfragestelle__c'] .'"';
          $arrResult5 = MySQLStatic::Query($strSql5);
          
          $arrEvent['AnfrageId'] = $arrResult5[0]['Id'];
          $arrEvent['AnfrageName'] = $arrResult5[0]['Name'];
          $arrEvent['AnfrageType'] = 'd';
          
          if ($arrResult5[0]['Id'] == $strSel) {
            $boolSecond = true;
          }
        
        } else { //CATCH ALL
        
          $strSql5b = 'SELECT `Id`, `Name` FROM `Anfragestelle__c` WHERE `Information_Provider__c` = "' .$arrEvent['SF42_informationProvider__c'] .'" AND `CATCH_ALL__c` = "true"';
          $arrResult5b = MySQLStatic::Query($strSql5b);
          
          $arrEvent['AnfrageId'] = $arrResult5b[0]['Id'];
          $arrEvent['AnfrageName'] = $arrResult5b[0]['Name'];
          $arrEvent['AnfrageType'] = 'c';
          
          if ($arrResult5b[0]['Id'] == $strSel) {
            $boolSecond = true;
          }
          
        }
      
      } else { // ZENTRAL
        
        $arrEvent['AnfrageId'] = $arrEvent['SF42_informationProvider__c'];
        $arrEvent['AnfrageName'] = $arrEvent['Name'];
        $arrEvent['AnfrageType'] = 'z';

        if ($arrEvent['SF42_informationProvider__c'] == $strSel) {
          $boolSecond = true;
        }
        
      }
      
      if (($_REQUEST['strSelAnf'] == 'all') || ($_REQUEST['strSelAnf'] == '') && ($_REQUEST['strSelGru'] != 'all')) {
        $boolSecond = true;
      }
      if ($boolSecond) {
        $arrEventList[] = $arrEvent;
      }
      
    } 
    
    //$strOutput.= '<h3>' .count($arrEventList) .' Ergebnisse gefunden:</h3>' .chr(10);
    //$strOutput.= '<!-- ' .print_r($arrEventList, true) .' -->'; 
        
  }

  $strOutput.= '<style>select { width: 140px; }' .chr(10);
  $strOutput.= '#outer1 { padding-bottom: 20px !important; }' .chr(10);
  $strOutput.= 'select.sea { width: auto; }</style>' .chr(10);


/* NEU */

  $arrEscalation = [];
  $strSql = 'SELECT * FROM `Group__c` WHERE `Eskalation__c` = "ja"';
  $arrSql = MySQLStatic::Query($strSql);
  if (count($arrSql) > 0) {
    foreach ($arrSql as $intKey => $arrDataset) {
      $arrEscalation[$arrDataset['Id']] = $arrDataset;
    }
  }

  $strExportTable = '';

  //print_r($arrEventList); die();
  
  if (@count($arrEventList) > 0) {
    //natcasesort($arrInformationProvider);

    $strOutputTable = '';

    $strOutputTable.= '<script>' .chr(10);
    $strOutputTable.= 'function openSG(topicId) {' .chr(10);
    $strOutputTable.= '  if (topicId != "") {' .chr(10);
    $strOutputTable.= '    setTimeout(()=>smartchat.cmd("topic.show", {entity: {id: topicId}}), 150);' .chr(10);
    $strOutputTable.= '  }' .chr(10);
    $strOutputTable.= '}' .chr(10);
    $strOutputTable.= '</script>' .chr(10);

    $strOutputTable.= '<table class="" id="5">' .chr(10);
    $strOutputTable.= '  <thead>' .chr(10);
    $strOutputTable.= '    <tr>' .chr(10);
    $strOutputTable.= '      <th class=""><input type="checkbox" value="0" name="check_all_r"></th>' .chr(10); //class="header"
    
    if ($_REQUEST['strSelDate'] == 'ALL') {
    	$strOutputTable.= '      <th class="sortInitialOrder-desc">Monat</th>' .chr(10); //class="header"
    }

  	$strOutputTable.= '      <th class="">Event-ID</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Esc</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Premium Payer</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">BNR PP</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="">Company Group</th>' .chr(10);
    $strOutputTable.= '      <th class="">Anfragestelle</th>' .chr(10);
    $strOutputTable.= '      <th class="">Beitragsrückstand</th>' .chr(10);
    //$strOutputTable.= '      <th class="">Status</th>' .chr(10);
  	//$strOutputTable.= '      <th class="">Grund</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="">Nächster Schritt</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="">Wiedervorlage</th>' .chr(10); //class="header"
    //$strOutputTable.= '      <th class="">Info</th>' .chr(10); //class="header"
    //$strOutputTable.= '      <th class="">AP Klärung</th>' .chr(10); //class="header"
    //$strOutputTable.= '      <th class="">Tel. Klärung</th>' .chr(10); //class="header"
    $strOutputTable.= '    </tr>' .chr(10);
    $strOutputTable.= '  </thead>' .chr(10);
    $strOutputTable.= '  <tbody>' .chr(10);
    
    $strExportTable.= $strOutputTable;
    
    if ($_REQUEST['strSelDate'] != 'ALL') {
      $arrPeriod = explode('_', $_REQUEST['strSelDate']);
    }
    
    $strNext = '';

    foreach ($arrEventList as $strId => $arrEvent) {
      

      if ($arrEvent['bis_am__c'] != '0000-00-00') {
        $arrDate = explode('-', $arrEvent['bis_am__c']);
        $arrEvent['bis_am__c'] = $arrDate[2] .'.' .$arrDate[1] .'.' .$arrDate[0];
        
        $intDiff = ((mktime(0, 0, 0, date('m'), date('d'), date('Y')) - mktime(0, 0, 0, $arrDate[1], $arrDate[2], $arrDate[0])) / 86400);
        if ($intDiff >= 0) {
          $strClass = ' red';
        } else {
          $strClass = '';
        }
        
        if ($intDiff == 0) { //ohne überfällig heute demnächst
          $strWieder = 'heute';
        } elseif ($intDiff > 0) {
          $strWieder = 'faellig';
        } else {
          $strWieder = 'bald';
        }
        
      } else {
        $arrEvent['bis_am__c'] = '';
        $strWieder = 'ohne';
      }
      
      if ($arrEvent['Grund__c'] == '') {
        $strGrund = $arrGrundAus[0];
      } else {
        $strGrund = $arrEvent['Grund__c'];
      }
      
      if ($arrEvent['Naechster_Meilenstein__c'] == '') {
        $strMeil = $arrMeilAus[0];
      } else {
        $strMeil = $arrEvent['Naechster_Meilenstein__c'];
      }
      
      if (($_REQUEST['Grund'] != '') && ($_REQUEST['Grund'] != $arrEvent['Grund__c'])) {
        $strClassAdd = ' style="display: none;"';
      } else {
        $strClassAdd = '';
      }
      
      $strOutputTable.= '    <tr class="' .$strTrClass .'" id="e_' .$arrEvent['evid'] .'"' .$strClassAdd .'>' .chr(10);
      $strExportTable.= '    <tr>' .chr(10);

      $strOutputTable.= '      <td><input type="checkbox" name="klae[' .$arrEvent['evid'] .']" value="1" class="klae" rel="' .$arrEvent['evid'] .'" id="check_' .$arrEvent['evid'] .'"></td>' .chr(10);
      $strExportTable.= '      <td></td>' .chr(10);
      
      if ($_REQUEST['strSelDate'] == 'ALL') {
        $strOutputTable.= '      <td>' .$arrEvent['SF42_Year__c'] .'-' .$arrEvent['SF42_Month__c'] .'</td>' .chr(10);
        $strExportTable.= '      <td>' .$arrEvent['SF42_Year__c'] .'-' .$arrEvent['SF42_Month__c'] .'</td>' .chr(10);
      }

      $strOutputTable.= '      <td><a target="_blank" href="/cms/index.php?ac=sear&det=' .$arrEvent['evid'] .'">' .$arrEvent['EName'] .'</a>' .chr(10);
      $strOutputTable.= '        <span class="hidden grund_' .$arrGrundAusFlip[$strGrund] .'"></span><span class="hidden wieder_' .$strWieder .'"></span><span class="hidden meil_' .$arrMeilAusFlip[$strMeil] .'"></span></td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['EName'] .'</td>' .chr(10);

      $strGroupId = $arrEvent['IdG'];

      if (isset($arrEscalation[$strGroupId])) { // 'a083000000D4hUMAAZ' https://izs.smartgroups.io/frontend/main/topic/topic-97f48d16-eb79-48c7-b450-6085ab217700

        preg_match('/topic-[\w-]*/', $arrEscalation[$strGroupId]['Eskalation_Link__c'], $arrLink);
        $strSgLink = $arrLink[0] ?? '';

        $strEsc = '<a href="javascript:void(0)" alt="' .$arrEscalation[$strGroupId]['Eskalation_Grund__c'] .'" title="' .$arrEscalation[$strGroupId]['Eskalation_Grund__c'] .'" onclick="openSG(\'' .$strSgLink .'\')" style="color:red;"><i class="icon fa fa-warning" aria-hidden="true"></i></a>';
        $strOutputTable.= '      <td>' .$strEsc .'</td>' .chr(10);
        $strExportTable.= '      <td></td>' .chr(10);
  
      } else {
        $strOutputTable.= '      <td></td>' .chr(10);
        $strExportTable.= '      <td></td>' .chr(10);
      }
      
      $strOutputTable.= '      <td>' .$arrEvent['PPname'] .'</td>' .chr(10);
      $strOutputTable.= '      <td>' .$arrEvent['PPbtnr'] .'</td>' .chr(10);
      $strOutputTable.= '      <td><span class="hidden">' .$arrEvent['NameG'] .'</span><a href="index_neu.php?ac=contacts&grid=' .$arrEvent['IdG'] .'" title="Zeige Kontakte" target="_blank">' .$arrEvent['NameG'] .'</a></td>' .chr(10);
      //$strOutputTable.= '      <td>' .$arrEvent['NameG'] .'</td>' .chr(10);

      $strExportTable.= '      <td>' .$arrEvent['PPname'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['PPbtnr'] .'</td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['NameG'] .'</td>' .chr(10);

      //$strOutputTable.= '      <td>' .$arrEvent['AnfrageName'] .'</td>' .chr(10);

      $strOutputTable.= '      <td><span class="hidden">' .$arrEvent['AnfrageName'] .'</span><a href="index_neu.php?ac=contacts&acid=' .$arrEvent['SF42_informationProvider__c'] .'" title="Zeige Kontakte" target="_blank">' .$arrEvent['AnfrageName'] .'</a></td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['AnfrageName'] .'</td>' .chr(10);
      
      /*
      $strOutputTable.= '      <td><span id="h_status_' .$arrEvent['evid'] .'" class="hidden">' .$arrEvent['Status_Klaerung__c'] .'</span><select name="Status_Klaerung__c" id="status_' .$arrEvent['evid'] .'" class="update selS">' .chr(10);
      $strOutputTable.= '			        <option value=""></option>';
      foreach ($arrStatusKlaerung as $strKey => $strName) {
        if ($arrEvent['Status_Klaerung__c'] == $strName) {
          $strSel = ' selected="selected"';
        } else {
          $strSel = '';
        }
        $strOutputTable.= '			        <option value="' .$strName .'"' .$strSel .'>' .$strName .'</option>';
      }
      $strOutputTable.= '      </select></td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['Status_Klaerung__c'] .'</td>' .chr(10);
      */

      $strRueckstand = '';
      if ($arrEvent['Beitragsrueckstand__c'] != '') {
        $strRueckstand = $arrEvent['Beitragsrueckstand__c'] .' EUR'; //number_format((float) $arrEvent['Beitragsrueckstand__c'], 2, ',', '.') .' EUR';
      }
      $strOutputTable.= '      <td>' .$strRueckstand .'</td>' .chr(10);
      $strExportTable.= '      <td></td>' .chr(10);
      
      if ($arrEvent['Grund__c'] == '') {
        $arrGrundAnz['keine Angabe']++;
      } else {
        $arrGrundAnz[$arrEvent['Grund__c']]++;
      }
      
      /*
      $strOutputTable.= '      <td><span id="h_grund_' .$arrEvent['evid'] .'" class="hidden">' .$arrEvent['Grund__c'] .'</span><select name="Grund__c" id="grund_' .$arrEvent['evid'] .'" class="update selG">' .chr(10);
      $strOutputTable.= '			        <option value=""></option>';
      foreach ($arrGrund as $strKey => $strName) {
        if ($arrEvent['Grund__c'] == $strKey) {
          $strSel = ' selected="selected"';
        } else {
          $strSel = '';
        }
        $strOutputTable.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
      }
      $strOutputTable.= '      </select></td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['Grund__c'] .'</td>' .chr(10);
      */

      $strOutputTable.= '      <td><span id="h_meil_' .$arrEvent['evid'] .'" class="hidden">' .str_replace('"', "'", $arrEvent['Naechster_Meilenstein__c']) .'</span><select name="Naechster_Meilenstein__c" id="meil_' .$arrEvent['evid'] .'" class="update" style="width: 250px;">' .chr(10);
      $strOutputTable.= '			        <option value=""></option>';
      foreach ($arrMeilenstein as $strKey => $strName) {
        if (str_replace('"', "'", $arrEvent['Naechster_Meilenstein__c']) == $strKey) {
          $strSel = ' selected="selected"';
        } else {
          $strSel = '';
        }
        $strOutputTable.= '			        <option value="' .$strKey .'"' .$strSel .'>' .$strName .'</option>';
      }
      $strOutputTable.= '      </select></td>' .chr(10);
      $strExportTable.= '      <td>' .str_replace('"', "'", $arrEvent['Naechster_Meilenstein__c']) .'</td>' .chr(10);

      $arrDate = explode('.', $arrEvent['bis_am__c']);
      $strHiddenDate = $arrDate[2] .$arrDate[1] .$arrDate[0];
      
      $strOutputTable.= '      <td><span id="h_date_' .$arrEvent['evid'] .'" class="hidden">' .$strHiddenDate .'</span><input type="text" id="date_' .$arrEvent['evid'] .'" class="date update' .$strClass .' selD" name="bis_am__c" value="' .$arrEvent['bis_am__c'] .'" autocomplete="off"></td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['bis_am__c'] .'</td>' .chr(10);
      
      /*
      if ($arrEvent['Info__c'] != '') {
        $strOutputTable.= '      <td><span id="in_' .$arrEvent['evid'] .'"><img src="img/information.png" title="' .$arrEvent['Info__c'] .'"></span></td>' .chr(10);
      } else {
        $strOutputTable.= '      <td><span id="in_' .$arrEvent['evid'] .'"></span></td>' .chr(10);
      }
      $strExportTable.= '      <td>' .$arrEvent['Info__c'] .'</td>' .chr(10);
      */
      /*
      $strOutputTable.= '      <td><span name="' .$arrEvent['AP_Klaerung'] .'" id="ap_' .$arrEvent['evid'] .'">' .$arrEvent['AP_Klaerung'] .'</span></td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['AP_Klaerung'] .'</td>' .chr(10);
      $strOutputTable.= '      <td><span name="' .$arrEvent['Tel_Klaerung'] .'" id="tel_' .$arrEvent['evid'] .'">' .$arrEvent['Tel_Klaerung'] .'</span></td>' .chr(10);
      $strExportTable.= '      <td>' .$arrEvent['Tel_Klaerung'] .'</td>' .chr(10);
      */

      $strOutputTable.= '    </tr>' .chr(10);
      $strExportTable.= '    </tr>' .chr(10);
      
    }
      
    $strOutputTable.= '  </tbody>' .chr(10);
    $strOutputTable.= '</table>' .chr(10);

    $strExportTable.= '  </tbody>' .chr(10);
    $strExportTable.= '</table>' .chr(10);

    $strOutput.= '<div class="clearfix" style="height: 48px; width: 1580px; padding-top: 30px;">' .chr(10);
    $strOutput.= '  <div class="form-left">' .chr(10);
    $strOutput.= '    <h3><span id="ct">' .count($arrEventList) .'</span> Ergebnisse gefunden:</h3>' .chr(10);
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' .chr(10);
    $strOutput.= '    <button id="start-edit" class="ui-button ui-state-default ui-corner-all">Massenfunktion</button>&nbsp;' .chr(10);
    $strOutput.= '    <form method="post" action="_get_csv.php" style="display: inline;">';
    $strOutput.= '    <input type="hidden" name="strTable" value="' .str_replace('"', "'", $strExportTable) .'">' .chr(10);
    $strOutput.= '    <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' .chr(10);
    $strOutput.= '    </form>';
    $strOutput.= '    <button id="start-note" class="ui-button ui-state-default ui-corner-all">Massen-Notizen</button>&nbsp;' .chr(10);
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '</div>' .chr(10);
    
    $strOutput.= $strOutputTable;

$strOutput.= '
<div id="dialog_edit" title="Daten für Massenfunktion" class="clearfix">
	<p id="validateTipsE" class="validateTips"></p>
	
	<p>Bitte gebe die Daten ein, die aktualisiert werden sollen:</p>

	<form>
	  <fieldset class="clearfix">

<div class="clearfix">
  <label for="Grund__c">Grund</label>
  <select name="Grund__c" id="artr_all" class="">' .chr(10);
      $strOutput.= '			      <option value="<leer>"></option>';
      $strOutput.= '			      <option value="">Mit "leer" überschreiben</option>';
      foreach ($arrGrund as $strKey => $strName) {
        $strOutput.= '			        <option value="' .$strKey .'">' .$strName .'</option>';
      }
      $strOutput.= '
			      </select>
</div>
<div class="clearfix">
<label for="Naechster_Meilenstein__c">Nächster Schritt</label>
<select name="Naechster_Meilenstein__c" id="artd_all" class="">' .chr(10);
      $strOutput.= '			      <option value="<leer>"></option>';
      $strOutput.= '			      <option value="">Mit "leer" überschreiben</option>';
      foreach ($arrMeilenstein as $strKey => $strName) {
        $strOutput.= '			        <option value="' .$strKey .'">' .$strKey .'</option>';
      }
      $strOutput.= '
			      </select>
</div>
<div class="clearfix">
<label for="Status_Klaerung__c">Status</label>
<select name="Status_Klaerung__c" id="arts_all" class="">' .chr(10);
      $strOutput.= '			      <option value="<leer>"></option>';
      $strOutput.= '			      <option value="">Mit "leer" überschreiben</option>';
      foreach ($arrStatusKlaerung as $strKey => $strName) {
        $strOutput.= '			        <option value="' .$strKey .'">' .$strKey .'</option>';
      }
      $strOutput.= '
			      </select>
</div>
<div class="clearfix">
  <label for="bis_am__c">Wiedervorlage</label>
  <input type="text" id="date_all" class="date" name="bis_am__c" autocomplete="off" style="display: inline;">
  &nbsp;<img src="img/f_delete.png" style="margin-bottom: 3px;" id="datedel" />
</div>

<div class="clearfix">
  <label for="AP_Klaerung">AP Klärung</label>
  <input type="text" id="ap_all" name="AP_Klaerung" autocomplete="off" style="width: 180px;">
</div>

<div class="clearfix">
  <label for="Tel_Klaerung">Telefon Klärung</label>
  <input type="text" id="tel_all" name="Tel_Klaerung" autocomplete="off" style="width: 180px;">
</div>

<div class="clearfix">
  <label for="Notizen">Status-Info</label>
  <textarea id="in_all" name="Notizen" style="width: 155px; height: 70px;"></textarea>
  &nbsp;<img src="img/f_delete.png" style="margin-bottom: 3px;" id="indel" />
</div>

</fieldset>

	</form>

</div>

<div id="dialog_massnote" title="Daten für Massen-Notizen" class="clearfix">
<p id="validateTipsN" class="validateTips"></p>
	
<p>Bitte gib die Notiz ein:</p>

<form>
  <fieldset class="clearfix">

<div class="clearfix">
<label for="Notizen">Massen-Notiz</label>
<textarea id="in_allmass" name="Notizen" style="width: 222px; height: 200px;"></textarea>
<!-- &nbsp;<img src="img/f_delete.png" style="margin-bottom: 3px;" id="allmassdel" /> -->
</div>

</fieldset>

</form>

</div>

      
<script>
  $(document).ready(function() {
  
    //$(\'.date\').datepicker();

		var message = daa = $("#date_all"), ara = $("#artr_all"), ada = $("#artd_all"), ads = $("#arts_all"), ap = $("#ap_all"), tel = $("#tel_all"), 
			allFields = $([]).add(daa).add(ara).add(ada).add(ads),
			tipsE = $("#validateTipsE");
			tipsN = $("#validateTipsN");

		function updateTipsE(t) {
			tipsE.text(t).effect("highlight",{},1500);
		}

    function updateTipsN(t) {
			tipsN.text(t).effect("highlight",{},1500);
    }
    
    function handle() {
      var strValues = "";
      $(".klae").each (function() {
        if($(this).is(":checked")) {
          strValues+= "|" + $(this).attr(\'rel\');
        }
      });
      strValues = strValues.substr(1);
      return strValues;
    }

		$(\'#start-note\').click(function() {
		  var echeck = handle();
		  if (echeck == "") {
			  $(\'#dialog_notice\').dialog(\'open\');
			  
			  $("#dialog_notice ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');
			  
			} else {

			  $("#in_allmass").val("");
			  $(\'#dialog_massnote\').dialog(\'open\');
			  
			  $("#dialog_massnote ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');
			  
			}
		})
		.hover(
			function(){ 
				$(this).addClass("ui-state-hover"); 
			},
			function(){ 
				$(this).removeClass("ui-state-hover"); 
			}
		).mousedown(function(){
			$(this).addClass("ui-state-active"); 
		})
		.mouseup(function(){
				$(this).removeClass("ui-state-active");
		});
    
		$(\'#start-edit\').click(function() {
		  var echeck = handle();
		  if (echeck == "") {
			  $(\'#dialog_notice\').dialog(\'open\');
			  
			  $("#dialog_notice ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');
			  
			} else {
			  $("#date_all").val("");
			  $("#artr_all").val("<leer>");
			  $("#artd_all").val("<leer>");
			  $("#arts_all").val("<leer>");
			  $("#ap_all").val("");
			  $("#tel_all").val("");
			  $("#in_all").val("");
			  $(\'#dialog_edit\').dialog(\'open\');
			  
			  $("#dialog_edit ~ .ui-dialog-buttonpane").children("button:contains(\'Abbrechen\')").css(\'float\', \'left\');
			  
			}
		})
		.hover(
			function(){ 
				$(this).addClass("ui-state-hover"); 
			},
			function(){ 
				$(this).removeClass("ui-state-hover"); 
			}
		).mousedown(function(){
			$(this).addClass("ui-state-active"); 
		})
		.mouseup(function(){
				$(this).removeClass("ui-state-active");
		});


    
		$("#dialog_edit").dialog({
			bgiframe: true,
			autoOpen: false,
			resizable: false,
			height: 460,
			width: 367,
			modal: true,
			buttons: {
				\'Jetzt ändern\': function() {
					var bValid = true;
					var date = daa.val();
					var artr = ara.val();
					var artd = ada.val();
					var arts = ads.val();
					var ap = $("#ap_all").val();
					var tel = $("#tel_all").val();
					var inx = $("#in_all").val();
					var cids = handle();
					//allFields.removeClass(\'ui-state-error\');
					
					
					if (($("#date_all").val() == "") && ($("#artr_all").val() == "<leer>") && ($("#artd_all").val() == "<leer>") && ($("#arts_all").val() == "<leer>") && ($("#ap_all").val() == "") && ($("#tel_all").val() == "") && ($("#in_all").val() == "")) {
					  updateTipsE("Bitte treffe zumindest eine Auswahl! ;-)");
					} else {
				  	
  					if (bValid) {
              $.ajax({
                type: "POST",
                url:  "_ajax.php",
                dataType: "html; charset=utf-8", 
                data: { id: cids, type: "klaeedit", date: $("#date_all").val(), artr: $("#artr_all").val(), artd: $("#artd_all").val(), arts: $("#arts_all").val(), ap: $("#ap_all").val(), tel: $("#tel_all").val(), info: $("#in_all").val() },
              });
              $(this).dialog(\'close\');
  					}
  					
  					cids.split("|").forEach(function(entry) {
              if (date != "") {
                if (date == "00.00.0000") {
                  $("#date_" + entry).val("");
                } else {
                  $("#date_" + entry).val(date);
                }
                $("#date_" + entry).parent().find("span").text(date);
                var arr = date.split(".");
                var date1 = new Date((new Date).getFullYear(), (new Date).getMonth() + 1, (new Date).getDate()); 
                var date2 = new Date(arr[2], arr[1], arr[0]);
                var dif = date2.getTime() - date1.getTime();
                dif = Math.ceil(dif / 1000 / 60 / 60 / 24);
                console.log(dif);
                if (dif == 0) {
                  $("#date_" + entry).addClass("red");
                } else if (dif < 0) {
                  $("#date_" + entry).addClass("red");
                } else {
                  $("#date_" + entry).removeClass("red");
                }
              }
              if (artr != "<leer>") {
                $("#grund_" + entry).val(artr);
                $("#grund_" + entry).parent().find("span").text(artr);
              }
              if (artd != "<leer>") {
                $("#meil_" + entry).val(artd);
                $("#meil_" + entry).parent().find("span").text(artd);
              }
              if (arts != "<leer>") {
                $("#status_" + entry).val(arts);
                $("#status_" + entry).parent().find("span").text(arts);
              }
              if (ap != "") {
                $("#ap_" + entry).html(ap);
              }
              if (tel != "") {
                $("#tel_" + entry).html(tel);
              }
              if (inx != "") {
                if (inx == "[LÖSCHEN]") {
                  $("#in_" + entry).html("");
                } else {
                  $("#in_" + entry).html(\'<img src="img/information.png" title="\' + inx + \'">\');
                }
              }
            });
            
					}
				},
				\'Abbrechen\': function() {
					$(this).dialog(\'close\');
				}
			},
			close: function() {
			  $("#date_all").datepicker("hide");
			  tipsE.text(\' \');
				//allFields.val(\'\').removeClass(\'ui-state-error\');
			}
		});


    $(function() {
      $("#dialog_massnote").dialog({
        bgiframe: true,
        autoOpen: false,
        resizable: false,
        height: 400,
        width: 367,
        modal: true,
        buttons: {
          \'Hinzufügen\': function() {

            var bValid = true;
            var inx = $("#in_allmass").val();
            var cids = handle();
            
            if ($("#in_allmass").val() == "") {
              updateTipsN("Bitte gebe die Daten ein:");
            } else {
              
              if (bValid) {
                $.ajax({
                  type: "POST",
                  url:  "_ajax.php",
                  dataType: "html; charset=utf-8", 
                  data: { id: cids, type: "massnote", info: $("#in_allmass").val() },
                });
                $(this).dialog(\'close\');
              }
              
            }
          },
          \'Abbrechen\': function() {
            $(this).dialog(\'close\');
          }
        },
        close: function() {
          $("#in_allmass").val("");
          tipsN.text(\' \');
          //allFields.val(\'\').removeClass(\'ui-state-error\');
        }
    
      });
    
    });
    


  });

</script>


<div id="dialog_notice" title="Ups!">
	<p>Es sind noch keine Events ausgewählt.</p>
	<p>Bitte wähle zuerst die Events, für welche die 
    Massenfunktion durchgeführt werden soll.</p>
</div>

<script type="text/javascript">
$(function() {
	$("#dialog_notice").dialog({
		bgiframe: true,
		autoOpen: false,
		resizable: false,
		modal: true,
		buttons: {
			\'Verstanden\': function() {
				$(this).dialog(\'close\');
			}
		}
	});

});
</script>

';
    
if ($_REQUEST['strSelDate'] != 'ALL') {
    $strOutput.= "
<script>
$('#5').fixheadertable({ 
   colratio    : [30, 80, 50, 300, 80, 250, 250, 120, 260, 100], 
   height      : 500, 
   width       : 1550, 
   zebra       : true, 
   sortable    : true,
   sortType    : ['', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string'],
   sortedColId : 5, 
   dateFormat  : 'd.m.Y'
});

</script>
";
} else {
    $strOutput.= "
<script>
$('#5').fixheadertable({ 
   colratio    : [30, 80, 50, 80, 150, 80, 80, 150, 200, 150, 150, 150, 100, 40, 150, 150], 
   height      : 500, 
   width       : 1820, 
   zebra       : true, 
   sortable    : true,
   sortType    : ['', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string'],
   sortedColId : 1, 
   dateFormat  : 'd.m.Y'
});

</script>
";
}

    if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {

      $arrGrundColor = array (
        'Beitragsnachweis fehlt' => 'orange',
        'Beitragsrückstand' => 'red',
        'Bescheinigung so nicht veröffentlichbar' => 'yellow',
        'Guthaben' => 'green',
        'Keine Auskunft von KK' => 'gray',
        'Kein Konto' => 'purple',
        'Konto geschlossen' => 'blue', 
        'Sonstiges' => 'brown'    
      );
      
      $strPieData = '';
      $strColor = '';
      $intColor = 0;
      $intSum  = 0;
      foreach ($arrGrundAnz as $strLabel => $intAmount) {
        if ($intAmount > 0) {
          $strPieData.= '          [\'' .$strLabel .' (' .$intAmount .')\', ' .$intAmount .'], ' .chr(10);
          $strColor.= '                         ' .$intColor .': { color: \'' .$arrGrundColor[$strLabel] .'\' }, ' .chr(10);
          $intSum+= $arrGrund['Anz'];
          $intColor++;
        }
      }
      $strPieData = substr($strPieData, 0, -2);
    
      $strOutput.= '    <script type="text/javascript" src="https://www.google.com/jsapi"></script>' .chr(10);
      $strOutput.= '    <script type="text/javascript">' .chr(10);
      $strOutput.= '      google.load("visualization", "1", {packages:["corechart"]});' .chr(10);
      $strOutput.= '      google.setOnLoadCallback(drawChart);' .chr(10);
      $strOutput.= '      function drawChart() {' .chr(10);
      $strOutput.= '        var data = google.visualization.arrayToDataTable([' .chr(10);
      $strOutput.= '          [\'Grund\', \'Anzahl\'],' .chr(10);
      $strOutput.= $strPieData .chr(10);
      $strOutput.= '        ]);' .chr(10);
    
      $strOutput.= '        var options = { pieSliceText: \'value\', ' .chr(10);
      $strOutput.= '                        title: \'Grund\', ' .chr(10);
      $strOutput.= '                        backgroundColor: \'transparent\', ' .chr(10);
      $strOutput.= '                        sliceVisibilityThreshold: 0, ' .chr(10);
      $strOutput.= '                        slices: {' .chr(10);
      $strOutput.= $strColor .chr(10);
      $strOutput.= '                        }' .chr(10);
      
      $strOutput.= '        };' .chr(10);
    
      $strOutput.= '        var chart = new google.visualization.PieChart(document.getElementById(\'piechart\'));' .chr(10);
      $strOutput.= '        chart.draw(data, options);' .chr(10);
      $strOutput.= '      }' .chr(10);
      $strOutput.= '    </script>' .chr(10);

      $strOutput.= '    <div id="piechart" style="width: 570px; height: 300px;"></div>' .chr(10);
      
    }
    
    if ($boolMore) {
      $strOutput.= '</form>' .chr(10);
    }

    if (($strNext != '') && ($strNext != 'blubb') && ($_REQUEST['strSelSta'] != 'OK') && ($_SERVER['REQUEST_METHOD'] != 'POST')) {
      $strOutput.= '<script>$(document).ready(function () { openOffersDialog(\'' .$strNext .'\'); });</script>' .chr(10);
    }

          
  } else {
    
    $strOutput.= '<p>Keine Events im gewählten Zeitraum vorhanden.</p>' .chr(10);
    
  }

}

$strOutput.= '';

}

?>