<?php

function getPictures($folderid, $access_token){
    //===================== Default cUrl options =================
    $options = array(
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_VERBOSE        => true,
        CURLOPT_HEADER         => false,
        CURLINFO_HEADER_OUT    => false,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => false,
    );
    $options[CURLOPT_HTTPHEADER] = array ("Authorization: Bearer ".$access_token);

    //======================= Proper url ==========================
    $url = "https://api.box.com/2.0/folders/{$folderid}/items";

    //======================= cUrl call ===========================
    $ch = curl_init($url);
    curl_setopt_array($ch, $options);
    $result = curl_exec($ch);
    curl_close($ch);
    $result = json_decode($result, true);

    // =============== Loop over items to search for photos =================
    $rez = array();
    if (isset($result['total_count']) && $result['total_count'] > 0){
        foreach ($result['entries'] as $elements){
            if (isPic($elements['name'])) $rez[] = $elements['name'];
        }
    }

    return $rez;
}

function isPic($value){
    $value = explode('.', $value);
    if (count($value) < 2) return false;
    $extensions = array ('jpg', 'bmp', 'png', 'gif');
    return in_array($value[1], $extensions);
}


?>