<?php

error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', 1); 

require('class.ical.php');

function strFetchEventList () {

    $strReturn = '';

    if (file_exists('calendar.ics')) {

        $strReturn  = file_get_contents('calendar.ics', false);

    } else {

        $strAgent = 'macOS/11.6.4 (20G417) Calendar/2811.5.1';
        $strUrl   = 'https://easyverein.com/public/BLMR/calendar/6abafbfd8c6c7d386789ef0c1d0c8140feca28c982a6e43d264664591296571/calendar.ics?id=22014754';
        
        $arrOptions = array(
            'http' => array(
                'method' => 'GET',
                'header' => 'Accept-language: en\r\n' .
                    'User-Agent: $strAgent\r\n'  
                )
            );
        
        $resContext = stream_context_create($arrOptions);
        $strReturn  = file_get_contents($strUrl, false, $resContext);

    }

    return $strReturn;

}

function boolDeleteOldEvents () {

    global $strToday;

    $arrEventFileList = glob('*_*.txt');

    foreach ($arrEventFileList as $intKey => $strFile) {
    
        $arrPart = explode('_', $strFile);
        if ($arrPart[1] != ($strToday .'.txt')) {
            unlink($strFile);
        }
    
    }

    return true;

}

function strClearTag ($strTag = '') {

    $strReturn = '';

    $strReturn = str_replace("\n", '', $strTag);
    $strReturn = strip_tags($strReturn);
    $strReturn = str_replace("\\", '', $strReturn);

    return $strReturn;

}

function strCreateEventEntry ($objEvent = null) {

    $strReturn = '';

    $strStart = date('d.m.Y H:i', strtotime($objEvent->dateStart)) .' Uhr';
    $strStop  = date('d.m.Y H:i', strtotime($objEvent->dateEnd)) .' Uhr';

    $strReturn.= '[Titel] : ' .utf8_decode($objEvent->summary) .chr(10);
    $strReturn.= '[Detail]: ' .utf8_decode(strClearTag($objEvent->description)) .chr(10);
    $strReturn.= '[Start] : ' .$strStart .chr(10);
    $strReturn.= '[Ende]  : ' .$strStop .chr(10);
    $strReturn.= '[Ort]   : ' .utf8_decode($objEvent->location) .chr(10);

    return $strReturn;

}

function strCompareEvents ($strEventOld = '', $strEventNew = '') {

    $strReturn = '';

    $arrLineListOld = explode(chr(10), $strEventOld);
    $arrLineListNew = explode(chr(10), $strEventNew);

    foreach ($arrLineListOld as $intLine => $strLine) {

        if ($strLine != $arrLineListNew[$intLine]) {
            $strReturn.= $arrLineListOld[$intLine] .chr(10);
        }

    }


    return $strReturn;

}

if (date('N') == 7) {
    die('Wrong Day');
}
if ((date('G') < 8) || (date('G') > 21)) {
    //die('Wrong Time');
}
die('ok');

$strToday  = date('Ymd');
$strMailTo = 'f.luetgen@gmx.de';

$strFile = strFetchEventList();

$boolDelete = boolDeleteOldEvents();

$objICal      = new iCal($strFile);
//$arrEventList = $objICal->eventsByDateSince('today');
$arrCalendar = $objICal->eventsByDateSince('yesterday');

if (count($arrCalendar) > 0) {

    foreach ($arrCalendar as $strDate => $arrEventList) {

        foreach ($arrEventList as $strDate => $objEvent) {

            $strFile = __DIR__ .'/' .$objEvent->uid .'_' .$strToday .'.txt';
        
            $strEventFromStream = strCreateEventEntry($objEvent);
        
            if (file_exists($strFile)) {
            
                $strExistingEvent = file_get_contents($strFile);
        
                if (md5($strExistingEvent) != md5($strEventFromStream)) {
        
                    $strChangeList = chr(10).chr(10) .utf8_decode('Änderungen:') .chr(10);
                    $strChangeList.= strCompareEvents($strExistingEvent, $strEventFromStream);
        
                    mail($strMailTo, 'Verteilung geändert', $strEventFromStream .$strChangeList);
        
                }
            
            } else {
        
                mail($strMailTo, 'Verteilung hinzugefügt', $strEventFromStream); 
        
            }
        
            file_put_contents($strFile, $strEventFromStream);
        
        }
    
    }

}


?>