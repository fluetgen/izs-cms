<?php

//print_r($_REQUEST['path']); die();

define('SIGNATURE', 'Signatur');
define('PUBLICKEY', 'PublicKey.pem');
define('METHOD1', 'SHA1');
define('METHOD2', 'SHA256');

$strHash = $_REQUEST['hash'];

$strKey = file_get_contents(PUBLICKEY);
$objZip = new ZipArchive;

foreach ($_FILES as $strUpload => $arrUpload) {

    $arrName       = pathinfo($_FILES[$strUpload]['name']);
    $strCsvFile    = '';
    $arrZipContent = array();

    if ($arrName['extension'] == 'zip') {

        $strZipFile = $_FILES[$strUpload]['tmp_name'];

        if ($objZip->open($strZipFile) === true) {

            for ($intFile = 0; $intFile < $objZip->numFiles; $intFile++) {   

                $strFileName = $objZip->getNameIndex($intFile);

                if (strtolower(pathinfo($strFileName)['extension']) == 'csv') {
                    $strCsvFile = $strFileName;
                }

                $resZip = $objZip->getStream($strFileName);

                $strFileContent = '';

                while (!feof($resZip)) {
                    $strFileContent.= fread($resZip, 2);
                }

                $arrZipContent[$strFileName] = $strFileContent;

                fclose($resZip);

            }

            unlink($_FILES[$strUpload]['tmp_name']);
        
        } else { 
        
            $arrItemList[] = array(
                'name' => $_FILES[$strUpload]['name'],
                'state' => 'error',
                'error' => 'Die ZIP Datei lässt sich nicht entpacken!'
            );
        
        }

        if (($strCsvFile != '') && (isset($arrZipContent[SIGNATURE]))) {

            $strSignatur = $arrZipContent[SIGNATURE];

            $boolProove = openssl_verify($arrZipContent[$strCsvFile], $strSignatur, $strKey, METHOD2);

			if ($boolProove != 1) {
				$boolProove = openssl_verify($arrZipContent[$strCsvFile], $strSignatur, $strKey, METHOD1);
			}	

        
            if ($boolProove == 1) {

                $arrItemList[] = array(
                    'name' => $_FILES[$strUpload]['name'],
                    'state' => 'ok'
                );

            } else {

                $arrItemList[] = array(
                    'name' => $_FILES[$strUpload]['name'],
                    'state' => 'error',
                    'error' => 'Die Signatur ist nicht korrekt. Die Datei wurde verändert!'
                );

            } 

        } else {
            
            $arrItemList[] = array(
                'name' => $_FILES[$strUpload]['name'],
                'state' => 'error',
                'error' => 'Keine gültige DATEV ZIP Datei!'
            );

        }

    } else {

        $arrItemList[] = array(
            'name' => $_FILES[$strUpload]['name'],
            'state' => 'error',
            'error' => 'Es sind nur ZIP Dateien erlaubt!'
        );

    }

    $arrResponce = array(

        'id' => $strHash,
        'done' => true,
        'items' => $arrItemList

    );

}

echo json_encode($arrResponce);

?>