<?php

/*

$strData = file_get_contents('Beitragsnachweis_05_2021.csv');

$strSignatur = file_get_contents('Signatur');

$strKey = file_get_contents('PublicKey.pem');

// state whether signature is okay or not
$ok = openssl_verify($strData, $strSignatur, $strKey, 'SHA256');
if ($ok == 1) {
    echo "good";
} elseif ($ok == 0) {
    echo "bad";
} else {
    echo "ugly, error checking signature";
}
// free the key from memory

//exit;

*/

?><?php

/*
var rsAalg = new RSACryptoServiceProvider();
rsAalg.FromXmlString(publicKey);
bool valid = rsAalg.VerifyData(dataToVerify, new SHA256CryptoServiceProvider(), signature);
*/

$strKey  = file_get_contents('PublicKey.pem');
$objZip  = new ZipArchive(); 
$strZipFile = 'Beitragsnachweis_05_2021.zip';

$objZip->open($strZipFile);

$boolProcessable = false;
$strCsvFile = '';

$arrFileList = array();

for ($intCount = 0; $intCount < $objZip->numFiles; $intCount++) { 

    $arrStat = $objZip->statIndex($intCount);
    $strFileName = basename($arrStat['name']);

    $arrFileList[] = $strFileName;

    $arrPart = explode('.', $strFileName);

    if (count($arrPart) >= 2) {
        if (strtolower($arrPart[count($arrPart) - 1]) == 'csv') {
            $strCsvFile = $strFileName;
        }
    }

}


if ((in_array('Signatur', $arrFileList) === true) && ($strCsvFile != '')) {

    $strData = '';
    $fp = fopen('zip://' . dirname(__FILE__) . '/' .$strZipFile .'#' .$strCsvFile, 'r');
    if (!$fp) {
        exit("Datei kann nicht geöffnet werden\n");
    }
    while (!feof($fp)) {
        $strData.= fread($fp, 2);
    }
    fclose($fp);

    $strSignatur = '';
    $fp = fopen('zip://' . dirname(__FILE__) . '/' .$strZipFile .'#Signatur', 'r');
    if (!$fp) {
        exit("Datei kann nicht geöffnet werden\n");
    }
    while (!feof($fp)) {
        $strSignatur.= fread($fp, 2);
    }
    fclose($fp);

    $boolProove = openssl_verify($strData, $strSignatur, $strKey, 'SHA256');

    if ($boolProove == 1) {
        echo "good";
    } else {
        echo "bad";
    } 

}



?>