<?php


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

define('FPDF_FONTPATH','../../fpdf17/font/');
define('TEMP_PATH', '../../temp/');

require('../../fpdf17/fpdf.php');
require('../../fpdf17/fpdi.php');

require_once('../../inc/refactor.inc.php');
require_once('../../../assets/classes/class.mysql.php');

$arrMonthList = array('', 'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember');

$strHash = $_REQUEST['hash'];

function getBnrIp ($strName = '') {

    $strReturn = '';

    $strName = str_replace('-', '%', $strName);
    $strName = str_replace('/', '%', $strName);

    //HACK
    if ((strstr($strName, 'IKK') !== false) && (strstr($strName, 'Inno') !== false)) {
        $strName = 'IKK Nord';
    }
    if ($strName == 'Handelskrankenkasse') {
        $strName = 'hkk';
    }
    if (strstr($strName, 'Koenig') !== false) {
        $strName = 'Koenig & Bauer BKK';
    }

    $strSql = 'SELECT `Btnr` FROM `izs_informationprovider` WHERE `Name` LIKE "' .MySQLStatic::esc($strName) .'%"';
    //echo $strSql .chr(10);
    $arrSql = MySQLStatic::Query($strSql);

    if (count($arrSql) == 1) {

        $strReturn = $arrSql[0]['Btnr'];

    } else {

        $strName = str_replace('%', ' ', $strName);

        $arrPart  = explode(' ', $strName);
        $intCount = count($arrPart);

        if ($intCount > 1) {

            for ($intRun = 1; $intRun < $intCount; $intRun++) {

                array_pop($arrPart);
                $strSearch = implode(' ', $arrPart);

                $strSql = 'SELECT `Btnr` FROM `izs_informationprovider` WHERE `Name` LIKE "%' .MySQLStatic::esc($strSearch) .'%"';
                //echo $strSql .chr(10);
                $arrSql = MySQLStatic::Query($strSql);
            
                if (count($arrSql) == 1) {
            
                    $strReturn = $arrSql[0]['Btnr'];
                    break;

                }

            }

        }

        if ($strReturn == '') {

            ; //What to do next?

        }

    }

    return $strReturn;

}

function getBnrPp ($strName = '') {

    $strReturn = '';

    if (strstr($strName, 'PPS') !== false) {
        $strName = 'Perfekter Personal Service GmbH';
    }

    if (strstr($strName, 'W & K') !== false) {
        $strName = 'W&K';
    }

    $strName = str_replace('-', ' ', $strName);

    $strSql = 'SELECT `Btnr` FROM `izs_premiumpayer_group` WHERE `Name` LIKE "' .MySQLStatic::esc($strName) .'%"';
    //echo $strSql .chr(10);
    $arrSql = MySQLStatic::Query($strSql);

    if (count($arrSql) == 1) {

        $strReturn = $arrSql[0]['Btnr'];

    } else {

        $arrPart  = explode(' ', $strName);
        $intCount = count($arrPart);

        if ($intCount > 1) {

            for ($intRun = 1; $intRun < $intCount; $intRun++) {

                array_pop($arrPart);
                $strSearch = implode(' ', $arrPart);

                $strSql = 'SELECT `Btnr` FROM `izs_premiumpayer_group` WHERE `Name` LIKE "%' .MySQLStatic::esc($strSearch) .'%"';
                $arrSql = MySQLStatic::Query($strSql);
            
                if (count($arrSql) == 1) {
            
                    $strReturn = $arrSql[0]['Btnr'];
                    break;

                }

            }

        }

        if ($strReturn == '') {

            ; //What to do next?

        }

    }

    return $strReturn;

}

function getPp ($strBnr = '') {

    $strReturn = '';

    $strSql = 'SELECT `Name` FROM `izs_premiumpayer_group` WHERE `Btnr` = "' .MySQLStatic::esc($strBnr) .'"';
    $arrSql = MySQLStatic::Query($strSql);

    if (count($arrSql) > 0) {

        $strReturn = $arrSql[0]['Name'];

    } 

    return $strReturn;

}


function createCsvLine ($arrLine, $strDelimiter = ";", $strEnclosure = '"', $strEscape = "\"") {

    $strReturn = '';

    $resBuffer = fopen('php://memory', 'r+');

    fputcsv($resBuffer, $arrLine, $strDelimiter, $strEnclosure, $strEscape);
    rewind($resBuffer);

    $strReturn = fgets($resBuffer);
    
    fclose($resBuffer);
    
    return $strReturn;

}

function handleAddison ($arrContent = array(), $strType = 'csv') {

    $strReturn = '';

    if (is_array($arrContent) && count($arrContent) > 1) {

        $strError = '';

        foreach ($arrContent as $intPage => $strPageContent) {

            if (strstr($strPageContent, 'Beitragsmeldung') === false) {
                continue;
            }

            $arrLine = array();

            //Date
            $arrLine[0] = '';
            preg_match('/schätzung\s*(\d{2})\.(\d{4})/', $strPageContent, $arrDate);
            if (count($arrDate) == 3) {
                $arrLine[0] = '01.' .$arrDate[1] .'.' .substr($arrDate[2], -2);
            }

            //BNR IP
            $arrLine[1] = '';
            preg_match('/Betriebs-Nr.:\s*(\d*)/', $strPageContent, $arrBnrIp);
            if (count($arrBnrIp) == 2) {
                $arrLine[1] = str_pad($arrBnrIp[1], 8, '0', STR_PAD_LEFT);
            }

            $arrPageContent = explode(chr(10), $strPageContent);

            $arrLine[2] = '';
            $arrLine[3] = '';
            $arrLine[4] = '';
            $arrLine[5] = '';

            if (count($arrPageContent) > 0) {

                foreach ($arrPageContent as $intLine => $strLine) {

                    //IP
                    if ((strstr($strLine, 'Schätzung ohne') !== false) || (strstr($strLine, 'keine Schätzung') !== false)) {
                        preg_match('/\s?(.*)\s{2,}/', $strLine, $arrIp);
                        if (isset($arrIp[1])) $arrLine[2] = trim($arrIp[1]);
                    }

                    //BNR PP
                    if ($intLine == 1) {
                        preg_match('/.*\s{2,}(.*)/', $strLine, $arrBnrPp);
                        if (count($arrBnrPp) == 2) {
                            $arrBnr = explode('/', $arrBnrPp[1]);
                            $arrLine[3] = $arrBnr[0];
                        }
                    }

                    //PP
                    if ($intLine == 1) {
                        preg_match('/\s?(.*)\s{2,}/', $strLine, $arrPp);
                        if (count($arrPp) == 2) {
                            $arrLine[4] = trim($arrPp[1]);
                        }
                    }

                    //Saldo
                    if (strstr($strLine, 'Summe') !== false) {
                        $arrSum = explode('  ', $strLine);
                        $strSum = trim(end($arrSum));
                        $arrLine[5] = getSaldo($strSum);
                    }

                }

            }
            
            $strReturn.= createCsvLine($arrLine);

            foreach ($arrLine as $intCol => $strContent) {
                if ($strContent == '') {
                    $strError = 'Fehler bei der Verarbeitung!';
                    break;
                }
            }

            //break;

        }

    }

    $arrReturn = array(
        'content' => $strReturn, 
        'error'   => $strError,
        'month'   => $arrLine[0],
        'name'    => $arrLine[4]
    );

    return $arrReturn;

}

function handleDatev ($arrContent = array(), $strType = 'csv') {

    global $arrMonthList;

    $strReturn = '';

    if (is_array($arrContent) && count($arrContent) > 1) {

        $strError = '';

        foreach ($arrContent as $intPage => $strPageContent) {

            if (strstr($strPageContent, 'AFP Form') === false) {
                continue;
            }

            $arrLine = array();

            //Page Type
            $strPageType = '';
            preg_match('/AFP Form\.\-Nr\. (.*)/', $strPageContent, $arrPageType);
            if (count($arrPageType) == 2) {
                $strPageType = $arrPageType[1];
            } 
            
            if ($strPageType == 'LNQN40') {

                //Date
                $arrLine[0] = '';
                preg_match('/Zeitraum von:\s*(\d{2})\.(\d{2}).\d{2}(\d{2})/', $strPageContent, $arrDate);
                if (count($arrDate) == 4) {
                    $arrLine[0] = '01.' .$arrDate[2] .'.' .$arrDate[3];
                }

                //BNR IP
                $arrLine[1] = '';
                preg_match('/Betriebs-Nr. der Krankenkasse:\s*(\d*)/', $strPageContent, $arrBnrIp);
                if (count($arrBnrIp) == 2) {
                    $arrLine[1] = str_pad($arrBnrIp[1], 8, '0', STR_PAD_LEFT);
                }


                //IP
                $arrLine[2] = '';
                preg_match('/\nKrankenkasse:\s*(.*)/', $strPageContent, $arrIp);
                if (count($arrIp) == 2) {
                    if (substr($arrIp[1], 0, 2) == 'EK') {
                        $arrIp[1] = trim(substr($arrIp[1], 2));
                    }
                    $arrLine[2] = $arrIp[1];
                }

                //BNR PP
                $arrLine[3] = '';
                preg_match('/Betriebs-Nr. des Arbeitgebers:\s*(\d*)/', $strPageContent, $arrBnrPp);
                if (count($arrBnrPp) == 2) {
                    $arrLine[3] = str_pad($arrBnrPp[1], 8, '0', STR_PAD_LEFT);
                }


                $arrPageContent = explode(chr(10), $strPageContent);

                //PP
                $arrLine[4] = '';
                $arrFirstLine = preg_split('/\s{2,}/', $arrPageContent[0]);
                if (count($arrFirstLine) == 5) {
                    $arrLine[4] = $arrFirstLine[2];
                } elseif (count($arrFirstLine) == 4) {
                    $arrLine[4] = $arrFirstLine[1];
                }

                //Saldo
                $arrLine[5] = '';  
                foreach ($arrPageContent as $intLine => $strLine) {

                    if (strstr($strLine, 'zu zahlender Betrag') !== false) {
                        $arrSum = explode('  ', $strLine);
                        $strSum = trim(end($arrSum));
                        $arrLine[5] = getSaldo($strSum);
                    }

                }

                $strReturn.= createCsvLine($arrLine);
              
            
            } elseif ($strPageType == 'LNQN50') {

                $arrResult = array();

                //Date
                $arrLine[0] = '';
                preg_match('/Übersicht Zahlungen im\s?([a-zA-Z]*)\s?(\d{4})/', $strPageContent, $arrDate);
                if (count($arrDate) == 3) {
                    $intMonth = array_search($arrDate[1], $arrMonthList);
                    $strMonth = str_pad($intMonth, 2, '0', STR_PAD_LEFT);
                    $arrLine[0] = '01.' .$strMonth .'.' .substr($arrDate[2], -2);
                }
                
                $arrPageContent = explode(chr(10), $strPageContent);

                //BNR IP
                $arrLine[1] = '';
                //IP
                $arrLine[2] = '';
                //BNR PP
                $arrLine[3] = '';
                
                //PP
                $arrLine[4] = '';
                $arrFirstLine = preg_split('/\s{2,}/', $arrPageContent[0]);
                if (count($arrFirstLine) == 5) {
                    $arrLine[4] = $arrFirstLine[2];
                }

                //Saldo
                $arrLine[5] = '';  


                $boolStart = false;
                foreach ($arrPageContent as $intLine => $strLine) {

                    if (strstr($strLine, 'Empfänger') !== false) {
                        $boolStart = true;
                        continue;
                    }

                    if ($boolStart) {

                        $arrAlternative = array('', '', '', '', '', '', '');

                        $arrDataset = preg_split('/\s{2,}/', $strLine);

                        if (count($arrDataset) == 5) {
                            if (substr($arrDataset[0], 0, 3) == 'EK ') {
                                $arrAlternative[1] = substr($arrDataset[0], 3);
                            }
                            $arrPartCity = explode(' ', $arrDataset[1]);
                            if (count($arrPartCity) >= 2) {
                                $arrAlternative[2] = $arrPartCity[0];
                                array_pop($arrPartCity);
                                $arrAlternative[3] = implode(' ', $arrPartCity);
                            }
                            $arrAlternative[4] = $arrDataset[2];
                            $arrAlternative[5] = $arrDataset[3];
                            $arrAlternative[6] = $arrDataset[4];

                            $arrDataset = $arrAlternative;
                        }

                        if (strstr($strLine, 'Fälligkeit') !== false) {
                            break;
                        }                        

                        if (count($arrDataset) == 7) {

                            $strIP = $arrDataset[1];
                            if ((in_array($arrDataset[0], array('AOK', 'BKK')) !== false) && (strstr($arrDataset[0], $strIP) === false)) {
                                $strIP = $arrDataset[0] .' ' .$strIP;
                            }
                            $arrLine[1] = getBnrIp($strIP, $arrDataset[2], $arrDataset[3]); 
    
                            $arrLine[2] = $strIP;
                            $arrLine[3] = $arrDataset[5];
    
                            $arrLine[5] = getSaldo($arrDataset[6]);

                            $arrResult[$intMonth][] = createCsvLine($arrLine);
    
                        }

                    }

                }

            } elseif ($strPageType = 'LOHN31') {


                //Date
                $arrLine[0] = '';
                preg_match('/Zeitraum\s{1,}von\s{1,}(\d{2})\s{1,}(\d{4})/', $strPageContent, $arrDate);
                if (count($arrDate) == 3) {
                    $arrLine[0] = '01.' .$arrDate[1] .'.' .substr($arrDate[2], -2);
                }

                //BNR IP
                $arrLine[1] = '';
                preg_match('/KK-Betriebs-Nr.:\s*(\d*)/', $strPageContent, $arrBnrIp);
                if (count($arrBnrIp) == 2) {
                    $arrLine[1] = str_pad($arrBnrIp[1], 8, '0', STR_PAD_LEFT);
                }


                //BNR PP
                $arrLine[3] = '';
                preg_match('/AG-Betriebs-Nr.:\s*(\d*)/', $strPageContent, $arrBnrPp);
                if (count($arrBnrPp) == 2) {
                    $arrLine[3] = str_pad($arrBnrPp[1], 8, '0', STR_PAD_LEFT);
                }


                $arrPageContent = explode(chr(10), $strPageContent);

                //IP
                $arrLine[2] = '';
                $arrIp = preg_split('/\s{2,}/', $arrPageContent[4]);
                if (count($arrIp) > 1) {
                    $arrLine[2] = $arrIp[1];
                }
                
                //PP
                $arrLine[4] = '';
                $arrFirstLine = preg_split('/\s{2,}/', $arrPageContent[0]);
                if (count($arrFirstLine) > 0) {
                    $arrLine[4] = $arrFirstLine[2];
                }

                //Saldo
                $arrLine[5] = '';  
                foreach ($arrPageContent as $intLine => $strLine) {

                    if (strstr($strLine, 'G u t h a b e n :') !== false) {
                        $arrSum = explode('  ', $strLine);
                        $strSum = trim(end($arrSum));
                        $arrLine[5] = getSaldo($strSum);
                    }

                }

                ksort($arrLine);

                $strReturn.= createCsvLine($arrLine);
              
            

            }

            /*
            foreach ($arrLine as $intCol => $strContent) {
                if ($strContent == '') {
                    $strError = 'Fehler bei der Verarbeitung!';
                    break;
                }
            }
            */

            //break;

        }

        if ($strPageType == 'LNQN50') {

            $arrLineList = max($arrResult);
            $arrLine = str_getcsv($arrLineList[0], ';', '"', "\"");
            $strReturn.= implode('', $arrLineList);
    
        }

    }

    $arrReturn = array(
        'content' => $strReturn, 
        'error'   => $strError,
        'month'   => $arrLine[0],
        'name'    => $arrLine[4]

    );

    //print_r($arrLine); die();

    return $arrReturn;

}

function getSaldo ($strValue = '') {

    $strReturn = '';

    if (substr($strValue, 0, 1) == '-') {
        $strReturn = 'nein';
    } elseif ($strValue == '0,00') {
        $strReturn = 'null';
    } else {
        $strReturn = 'ja';
    }

    return $strReturn;

}

function handleSbs ($arrContent = array(), $strType = 'csv') {

    global $arrMonthList;

    $strReturn = '';

    if (is_array($arrContent) && count($arrContent) > 1) {

        $strError = '';
        $strPageType = '';

        $strDate = '';
        $strPp = '';
        $strPpBnr = '';


        foreach ($arrContent as $intPage => $strPageContent) {

            $arrLine = array();

            if ($intPage == 0) {

                if (strstr($strPageContent, 'Gesetzliche Abgaben für') !== false) {

                    $strPageType = 'Format01';

                    preg_match_all('/Gesetzliche Abgaben für (\d{2})\/(\d{4});/', $strPageContent, $arrDate);

                    if (count($arrDate) == 3) {
                        $strDate = '01.' .$arrDate[1][0] .'.' .substr($arrDate[2][0], -2);
                    }

                    preg_match_all('/Betr.Nr.\s{1,}(\d{1,})/', $strPageContent, $arrPpBnr);

                    if (count($arrPpBnr) == 2) {
                        $strPpBnr = str_pad($arrPpBnr[1][0], 8, '0', STR_PAD_LEFT);
                        $strPp = getPp($strPpBnr);
                    }


                } elseif (strstr($strPageContent, 'Empfängerprotokoll') !== false) {

                    $strPageType = 'Format02';

                    preg_match_all('/Monat:\s{2,}(\d{1})\/(\d{2})/', $strPageContent, $arrDate);

                    if (count($arrDate) == 3) {
                        $strDate = '01.' .str_pad($arrDate[1][0], 2, '0', STR_PAD_LEFT) .'.' .$arrDate[2][0];
                    }

                    preg_match_all('/Firma:\s{1,}\d{1,}\s{1,}([a-zA-Z ]*)/', $strPageContent, $arrPp);

                    if (count($arrPp) == 2) {
                        $strPp = $arrPp[1][0];
                        $strPpBnr = getBnrPp($strPp);
                    }

                }

            }

            if ($strPageType == 'Format01') {

                preg_match_all('/\*\s{1,}(.*)\s{1,}Fällig bis\s{1,}[\d\.]*\s{1,}([0-9\.,-]*)/', $strPageContent, $arrSvList);

                if (count($arrSvList) == 3) {

                    foreach ($arrSvList[1] as $intKey => $strLine) {

                        $strLine = trim($strLine);

                        $arrLine = array(
                            $strDate,
                            getBnrIp($strLine),
                            $strLine,
                            $strPpBnr,
                            $strPp,
                            getSaldo($arrSvList[2][$intKey])
                        );

                        $strReturn.= createCsvLine($arrLine);
    
                    }
                    
                }

                
              
            } elseif ($strPageType == 'Format02') {

                $arrPageContent = explode(chr(10), $strPageContent);

                if (count($arrPageContent) > 0) {

                    $boolBegin = false;

                    foreach ($arrPageContent as $intLine => $strLine) {

                        if (strstr($strLine, 'EmpfNr') !== false) {
                            $boolBegin = true;
                            continue;
                        }

                        if (strlen($strLine) < 2) {
                            continue;
                        }

                        if ($boolBegin) {

                            $arrLinePart = preg_split('/\s{2,}/', $strLine);

                            $strIpTemp = '';
                            if (count($arrLinePart) == 5) {
                                $strIpTemp = $arrLinePart[1];
                            } elseif (count($arrLinePart) == 6) {
                                $strIpTemp = $arrLinePart[2];
                            }

                            preg_match('/\d{1,}\s(.*)/', $strIpTemp, $arrIpTemp);

                            if (count($arrIpTemp) == 2) {
                                $strIp = $arrIpTemp[1];
                            } 

                            $strSaldo = getSaldo(end($arrLinePart));

                            $arrLine = array(
                                $strDate,
                                getBnrIp($strIp),
                                $strIp,
                                $strPpBnr,
                                $strPp,
                                $strSaldo                                
                            );

                            $strReturn.= createCsvLine($arrLine);

                        }

                        if (strstr($strLine, 'summe') !== false) {
                            break;
                        }

                    }

                }
              
            }

            /*
            foreach ($arrLine as $intCol => $strContent) {
                if ($strContent == '') {
                    $strError = 'Fehler bei der Verarbeitung!';
                    break;
                }
            }
            */

            //break;

        }

    }

    $arrReturn = array(
        'content' => $strReturn, 
        'error'   => $strError,
        'month'   => $strDate,
        'name'    => $strPp
    );

    return $arrReturn;

}

function handleLexware ($arrContent = array(), $strType = 'csv') {

    $strReturn = '';

    if (is_array($arrContent) && count($arrContent) > 1) {

        $strError = '';

        foreach ($arrContent as $intPage => $strPageContent) {

            $arrLine = array();

            $arrPageContent = explode(chr(10), $strPageContent);

            if (count($arrPageContent) > 0) {

                if (strstr($arrPageContent[0], 'Arbeitgeber') !== false) { // Format01

                    $strPpBnr = '';
                    $strPp = '';
                    $strDate = '';
                    $strIp = '';


                    for ($intLine = 0; ($intLine < 5); $intLine++) {

                        $strLine = $arrPageContent[$intLine];

                        preg_match('/\d{8}/', $strLine, $arrHead);

                        if (count($arrHead) === 1) {
                            $strPpBnr = $arrHead[0];
                        }

                        if ($intLine == 1) {

                            preg_match('/\s{1,}(.*)/', $strLine, $arrPp);

                            if (count($arrPp) == 2)  {
                                $strPp = $arrPp[1];
                            }

                        }

                    }

                    preg_match('/Zeitraum\s{1,}von\s{1,}(\d{2})\s{1,}(\d{2})\s{1,}(\d{4})/', $strPageContent, $arrDate);

                    if (count($arrDate) == 4) {
                        $strDate = $arrDate[1] .'.' .$arrDate[2] .'.' .substr($arrDate[2], -2);
                    }

                    foreach ($arrPageContent as $intLine => $strLine) {

                        preg_match('/^\s{8}([a-zA-Z].*)/', $strLine, $arrIp);

                        if ((count($arrIp) == 2) && ($strIp == '')) {

                            $arrIpPart = explode('  ', $arrIp[1]);
                            $strIp = $arrIpPart[0];
                            $strIpBnr = getBnrIp($strIp);

                        }

                        preg_match('/Gesamtsumme\s{1,}([0-9\.,-]*)\s{1,}([0-9\.-]*)/', $strLine, $arrSaldo);

                        if (count($arrSaldo) == 3) {
                            
                            $strSaldo = getSaldo($arrSaldo[1] .',' .$arrSaldo[2]);
                        
                        }

                    }


                    $arrLine = array(
                        $strDate,
                        $strIpBnr,
                        $strIp,
                        $strPpBnr,
                        $strPp,
                        $strSaldo                                
                    );

                    $strReturn.= createCsvLine($arrLine);

                
                } elseif (strstr($arrPageContent[0], 'Archiv Beitragsnachweise') !== false) { // Format02

                    $strDate  = '';
                    $strPp    = '';
                    $strPpBnr = '';

                    foreach ($arrPageContent as $intLine => $strLine) {

                        $strIp = '';
                        $strIpBnr = '';
                        $strSaldo = '';

                        $arrLinePart = preg_split('/\s{2,}/', $strLine);

                        if (count($arrLinePart) == 6) {

                            if ($strDate == '') {

                                $arrDate = explode('.', $arrLinePart[3]);
                                if (count($arrDate) == 3) {
                                    $strDate = $arrDate[0] .'.' .$arrDate[1] .'.' .substr($arrDate[2], -2);
                                } else {
                                    $strDate = '';
                                }
                                
                            }

                            $strPp = $arrLinePart[0];

                            if ($strPpBnr == '') {

                                $strPpBnr = getBnrPp($strPp);

                            }

                            $strIp = $arrLinePart[2];
                            $strIpBnr = getBnrIp($strIp);
                            $strSaldo = getSaldo(str_replace(' EUR', '', $arrLinePart[4]));

                            $arrLine = array(
                                $strDate,
                                $strIpBnr,
                                $strIp,
                                $strPpBnr,
                                $strPp,
                                $strSaldo                                
                            );
        
                            $strReturn.= createCsvLine($arrLine);
    
                        }

                    }

                } elseif (strstr($arrPageContent[0], 'BNR Krankenkasse') !== false) { // Format03


                    foreach ($arrPageContent as $intLine => $strLine) {

                        $arrLinePart = preg_split('/\s{2,}/', $strLine);

                        if ((count($arrLinePart) == 6) && (is_numeric($arrLinePart[0][0]))) {

                            $strDate = '';
                            $arrDate = explode(' ', $arrLinePart[0]);
                            if (count($arrDate) == 2) {
                                $strDate = '01.' .$arrDate[0] .'.' .substr($arrDate[1], -2);
                            } 

                            $strIp    = $arrLinePart[2];
                            $strIpBnr = $arrLinePart[1];
                            $strPp    = $arrLinePart[4];
                            $strPpBnr = $arrLinePart[3];
                            $strSaldo = $arrLinePart[5];

                            $arrLine = array(
                                $strDate,
                                $strIpBnr,
                                $strIp,
                                $strPpBnr,
                                $strPp,
                                $strSaldo                                
                            );
        
                            $strReturn.= createCsvLine($arrLine);

                        }
                        
                    }

                }

            }

            /*
            foreach ($arrLine as $intCol => $strContent) {
                if ($strContent == '') {
                    $strError = 'Fehler bei der Verarbeitung!';
                    break;
                }
            }
            */

            //break;

        }

    }

    $arrReturn = array(
        'content' => $strReturn, 
        'error'   => $strError,
        'month'   => $strDate,
        'name'    => $strPp
    );

    //print_r($arrReturn); die();

    return $arrReturn;

}

function handleSage ($arrContent = array(), $strType = 'csv') {

    global $arrMonthList;

    $strReturn = '';

    //echo $arrContent[0]; die();

    if (is_array($arrContent) && count($arrContent) > 1) {

        $strError = '';

        foreach ($arrContent as $intPage => $strPageContent) {

            $arrLine = array();

            $arrPageContent = explode(chr(10), $strPageContent);

            if (count($arrPageContent) > 0) {

                if (strstr($strPageContent, 'Sage HR') !== false) { // Format01

                    //echo 'Format01'; die();

                    $strPpBnr = '';
                    $strPp = '';
                    $strDate = '';
                    $strIp = '';
                    $intKkLine = 0;

                    for ($intLine = 0; ($intLine < 25); $intLine++) {

                        $strLine = $arrPageContent[$intLine];

                        if (strstr($strLine, 'vom:') !== false) {
                            $intKkLine = $intLine + 1;
                            $arrDate = preg_split('/\s{2,}/', $strLine);
                            if (count($arrDate) == 5) {
                                $strDate = '01.' .str_replace(' ', '', $arrDate[3]) .'.' .substr(str_replace(' ', '', $arrDate[4]), -2);
                            }
                        }
                        
                        if (strstr($strLine, '[') !== false) {
                            $arrPp = preg_split('/\[\d*\]\s/', $strLine);
                            if (count($arrPp) == 2) {
                                $arrPpPart = explode(',', $arrPp[1]);
                                $strPp = trim($arrPpPart[0]);
                                $strPpBnr = getBnrPp($strPp);
                            }
                        }

                        if (($intKkLine > 0) && ($intLine == $intKkLine)) {

                            $arrIp = preg_split('/\s{2,}/', $strLine);

                            if ((count($arrIp) != 6) && (count($arrIp) != 2)) {
                                $intKkLine++;
                                continue;
                            }

                            $strIp = $arrIp[1];

                            if (strstr($strIp, 'Übersicht') === false) {
                                $strIpBnr = getBnrIp($strIp);
                            } else {
                                break(2);
                            }

                        }

                    }

                    preg_match('/Gesamtsumme\s{2,}([0-9,\.-]*)/', $strPageContent, $arrSaldo);

                    if (count($arrSaldo) == 2) {
                        $strSaldo = getSaldo($arrSaldo[1][0]);
                    }

                    $arrLine = array(
                        $strDate,
                        $strIpBnr,
                        $strIp,
                        $strPpBnr,
                        $strPp,
                        $strSaldo                                
                    );

                    $strReturn.= createCsvLine($arrLine);

                
                } elseif (strstr($arrPageContent[0], 'Lastschriften') !== false) { // Format02

                    //echo 'Format02'; die();

                    $strDate  = '';
                    $strPp    = '';
                    $strPpBnr = '';

                    $arrDate = preg_split('/\s{2,}/', $arrPageContent[0]);
                    if (count($arrDate) == 3) {

                        $arrDateTemp = explode(' ', $arrDate[2]);
                        
                        if (count($arrDateTemp) == 2) {
                            $intMonth = array_search($arrDateTemp[0], $arrMonthList);
                            $strMonth = str_pad($intMonth, 2, '0', STR_PAD_LEFT);
                            $strDate = '01.' .$strMonth .'.' .substr($arrDateTemp[1], -2);
                        }
                    }
                    
                    $arrPp = preg_split('/\[\d*\]\s/', $arrPageContent[1]);
                    if (count($arrPp) == 2) {
                        $arrPpPart = explode(',', $arrPp[1]);
                        $strPp = trim($arrPpPart[0]);
                        $strPpBnr = getBnrPp($strPp);
                    }

                    //echo $strDate .'*' .$strPp .'*' .$strPpBnr; die();

                    foreach ($arrPageContent as $intLine => $strLine) {

                        $strIp = '';
                        $strIpBnr = '';
                        $strSaldo = '';

                        $arrLinePart = preg_split('/\s{2,}/', $strLine);

                        if ((count($arrLinePart) == 4) && ($arrLinePart[1] != 'Bezeichnung')) {

                            $strIp = $arrLinePart[1];
                            $strIpBnr = getBnrIp($strIp);
                            
                            $arrSaldo = explode(' EUR ', $arrLinePart[3]);
                            if (count($arrSaldo) >= 2) {
                                $strSaldo = getSaldo($arrSaldo[0]);
                            }
                            
                            $arrLine = array(
                                $strDate,
                                $strIpBnr,
                                $strIp,
                                $strPpBnr,
                                $strPp,
                                $strSaldo                                
                            );
        
                            $strReturn.= createCsvLine($arrLine);
    
                        }

                    }

                } 

            }

            /*
            foreach ($arrLine as $intCol => $strContent) {
                if ($strContent == '') {
                    $strError = 'Fehler bei der Verarbeitung!';
                    break;
                }
            }
            */

            //break;

        }

    }

    $arrReturn = array(
        'content' => $strReturn, 
        'error'   => $strError,
        'month'   => $strDate,
        'name'    => $strPp
    );

    //print_r($arrReturn); die();

    return $arrReturn;

}

function handleLohnwerk ($arrContent = array(), $strType = 'csv') {

    $strReturn = '';

    if (is_array($arrContent) && count($arrContent) > 1) {

        $strError = '';

        foreach ($arrContent as $intPage => $strPageContent) {

            $arrPageContent = explode(chr(10), $strPageContent);

            if (count($arrPageContent) == 1) {
                continue;
            }

            $arrLine = array();

            //Date
            $arrLine[0] = '';
            preg_match('/(\d{2})\.(\d{2})\.(\d{4})/', $strPageContent, $arrDate);
            if (count($arrDate) == 4) {
                $arrLine[0] = '01.' .$arrDate[2] .'.' .substr($arrDate[3], -2);
            }

            //BNR PP
            $arrLine[3] = '';
            preg_match('/(\d*)\s*Beitrag/', $strPageContent, $arrBnrPp);
            if (count($arrBnrPp) == 2) {
                $arrLine[3] = str_pad($arrBnrPp[1], 8, '0', STR_PAD_LEFT);
            }

            //Name PP
            $arrLine[4] = '';
            $arrPPName = preg_split('/\s{2,}/', $arrPageContent[1]);
            if (count($arrPPName) == 3) {
                $arrLine[4] = $arrPPName[1];
            }

            $arrLine[1] = '';
            $arrLine[2] = '';
            $arrLine[5] = '';

            ksort($arrLine);

            if (count($arrPageContent) > 0) {

                foreach ($arrPageContent as $intLine => $strLine) {

                    $arrPart = preg_split('/\s{2,}/', $strLine);

                    if ((count($arrPart) == 4) || (count($arrPart) == 5)) {

                        if (ltrim($arrPart[0]) == 'Empfänger') {
                            continue;
                        }

                        //print_r($arrPart);

                        //Saldo
                        $arrLine[5] = getSaldo($arrPart[1]);

                        if (isset($arrPageContent[$intLine + 1]) && (count(preg_split('/\s{2,}/', $arrPageContent[$intLine + 1])) == 1)) {
                            $strIPRaw = ltrim($arrPart[0]) .$arrPageContent[$intLine + 1];
                        } else {
                            $strIPRaw = ltrim($arrPart[0]);
                        }

                        if (strstr($strIPRaw, 'Bundesknappschaft') != false) {
                            //$strIPRaw.= ' 98000001';
                        }

                        //BNR IP
                        preg_match('/(\d{7,})/', $strIPRaw, $arrBnrPp);

                        $arrLine[1] = '';
                        $arrLine[2] = '';

                        if (count($arrBnrPp) == 2) {
                            $arrLine[1] = $arrBnrPp[1];

                            //Name IP
                            $arrIPName = preg_split('/\d{7,}/', $strIPRaw);
                            if (count($arrIPName) == 2) {
                                $arrLine[2] = $arrIPName[0] .ltrim($arrIPName[1]);
                            }

                        }

                        $strReturn.= createCsvLine($arrLine);                        
                        
                    }

                }

            }

            //die();


            foreach ($arrLine as $intCol => $strContent) {
                if ($strContent == '') {
                    $strError = 'Fehler bei der Verarbeitung!';
                    break;
                }
            }

            //break;

        }

    }

    $arrReturn = array(
        'content' => $strReturn, 
        'error'   => $strError,
        'month'   => $arrLine[0],
        'name'    => $arrLine[4]
    );

    return $arrReturn;

}

function handleNavision ($arrContent = array(), $strType = 'csv') {

    //print_r($arrContent[0]);

    $strReturn = '';

    if (is_array($arrContent) && count($arrContent) > 1) {

        $strError = '';

        foreach ($arrContent as $intPage => $strPageContent) {

            $arrPageContent = explode(chr(10), $strPageContent);

            if (count($arrPageContent) == 1) {
                continue;
            }

            $arrLine = array();

            //Date
            $arrLine[0] = '';
            preg_match('/Abrechnungsmonat\s*(\d{2}\/\d{4})/', $strPageContent, $arrDate);
            if (count($arrDate) == 2) {
                $arrPart = explode('/', $arrDate[1]);
                $arrLine[0] = '01.' .$arrPart[0] .'.' .substr($arrPart[1], -2);
            }

            //echo $arrLine[0]; die();

            //Name PP
            $arrLine[4] = '';
            preg_match('/Firmen Konto:\s*(.*)/', $strPageContent, $arrPp);
            if (count($arrPp) == 2) {
                $arrLine[4] = $arrPp[1];
            }

            //echo $arrLine[3]; die();


            $arrLine[1] = '';
            $arrLine[2] = '';
            $arrLine[3] = '';
            $arrLine[5] = '';

            ksort($arrLine);

            if (count($arrPageContent) > 0) {

                foreach ($arrPageContent as $intLine => $strLine) {

                    $arrPart = preg_split('/\s{2,}/', $strLine);

                    if (count($arrPart) == 5) {

                        if ($arrPart[1] == 'Empfänger') {
                            continue;
                        }

                        //BNR PP
                        $arrLine[3] = '';
                        preg_match('/(\d{7,})\sfür/', $arrPart[2], $arrPpBnr);
                        if (count($arrPpBnr) == 2) {
                            $arrLine[3] = $arrPpBnr[1];
                        }
                        
                        //Saldo
                        $arrLine[5] = getSaldo($arrPart[4]);

                        //BNR IP
                        if (strstr($arrPart[0], 'AOK NIEDER') !== false) {
                            $arrPart[1] = 'AOK Niedersachsen';
                        }

                        if (strlen($arrPart[1]) <= 4) {

                            if (strstr($arrPart[0], 'SCHWENNINGER') !== false) {
                                $strTemp = $arrPart[0];
                                $arrPart[0] = 'vivida bkk';
                            }                           

                            $arrLine[1] = getBnrIp($arrPart[0]);

                            if (strstr($arrPart[0], 'vivida bkk') !== false) {
                                $arrPart[0] = $strTemp;
                            } 

                        } else {
                            $arrLine[1] = getBnrIp($arrPart[1]);
                        }

                        $arrLine[2] = $arrPart[0];

                        $strReturn.= createCsvLine($arrLine);  

                    }

                }

            }

            foreach ($arrLine as $intCol => $strContent) {
                if ($strContent == '') {
                    $strError = 'Fehler bei der Verarbeitung!';
                    break;
                }
            }

            //break;

        }

    }

    $arrReturn = array(
        'content' => $strReturn, 
        'error'   => $strError,
        'month'   => $arrLine[0],
        'name'    => $arrLine[4]
    );

    return $arrReturn;

}

function createCsv ($strContent = '') {

    $strReturn = '';

    $strReturn = "\xEF\xBB\xBF"; // UTF-8 BOM
    $strReturn.= 'Datum;BNR IP;IP;BNR PP;PP;Saldo' .chr(10);
    $strReturn.= $strContent;

    return $strReturn;

}

function getCsvFileName ($strMonth = '', $strName = '') {

    $strReturn = '';

    if (($strMonth != '') && ($strName != '')) {

        $arrDate = explode('.', $strMonth);
        $strDate = $arrDate[2] .$arrDate[1];

        $arrPp = explode(' ', $strName);

        if (($arrPp[0] == 'Dr.') && (isset($arrPp[2]))) { // Dr. Zweininger
            $arrPp[0].= $arrPp[2];
        }

        if (($arrPp[0] == 'Dipl.-Ökonom')) {
            $arrPp[0] = 'Dipl' .$arrPp[1] .$arrPp[2];
        }

        $strPp = preg_replace('/[^A-Za-z0-9\_]/', '', $arrPp[0]); //[^A-Za-z0-9\-\_]
        //$strPp = preg_replace('/[^A-Za-z0-9\-\_]/', '', $strName);

        $strReturn = 'IMPORT_' .$strPp .'_' .$strDate .'_CSV.csv';

    }

    return $strReturn;

}

$arrItemList = array();

foreach ($_FILES as $strUpload => $arrUpload) {

    $arrName = pathinfo($_FILES[$strUpload]['name']);

    if ($arrName['extension'] == 'pdf') {

        $arrHeader = array();
        $arrHeader[] = 'apikey: 9b0d3b1c-27f2-4e40-8e4d-d735a50dbbee';

        $arrPost = array(
            'url'     => '',
            'content' => base64_encode(file_get_contents($_FILES[$strUpload]['tmp_name']))
        );
    
        $arrRes = curl_post('http://api.izs-institut.de/api/func/pdf2text/', $arrPost, array(), $arrHeader);
        $arrPdfContent = json_decode($arrRes, true);

        if (isset($arrPdfContent['content']) && (count($arrPdfContent['content']) > 0)) {

            //echo $arrPdfContent['content'][0]; die();

            if ($arrPdfContent['content'][0] != '') {

                $arrLineList = explode(chr(10), $arrPdfContent['content'][0]);
                $strError    = 'Unbekanntes Dateiformat!';
    
                if (strstr($arrLineList[0], 'Mandanten-Nr.:') !== false) { // Addison

                    //echo $arrLineList[0]; die();

                    $arrResult = handleAddison($arrPdfContent['content']);
                    $strCsv    = $arrResult['content'];
                    $strError  = $arrResult['error'];
    
                } elseif (strstr($arrLineList[0], 'Berater') !== false) { // DATEV

                    $arrRes = curl_post('http://api.izs-institut.de/api/func/pdfinfo/', $arrPost, array(), $arrHeader);
                    $arrPdfInfo = json_decode($arrRes, true);

                    $strPdfHeader = implode('|', $arrPdfInfo);

                    if ((strstr($strPdfHeader, 'SkyPDF') !== false) || (strstr($strPdfHeader, 'Microsoft: Print To PDF') !== false)) {

                        $arrResult = handleDatev($arrPdfContent['content']);
                        $strCsv    = $arrResult['content'];
                        $strError  = $arrResult['error'];

                    } else {

                        $strError  = 'Keine verlässlichen PDF-Daten!';

                    }

                } elseif ((strstr($arrPdfContent['content'][0], 'Gesetzliche Abgaben für') !== false) || (strstr($arrPdfContent['content'][0], 'Empfängerprotokoll') !== false)) { // SBS
                    
                    //echo $arrPdfContent['content'][0]; die();

                    $arrResult = handleSbs($arrPdfContent['content']);
                    $strCsv    = $arrResult['content'];
                    $strError  = $arrResult['error'];                    

                } elseif ((strstr($arrPdfContent['content'][0], 'Sage HR') !== false) || (strstr($arrLineList[0], 'Lastschriften') !== false)) { // SAGE
                    
                    //echo $arrPdfContent['content'][0]; die();

                    $arrResult = handleSage($arrPdfContent['content']);
                    $strCsv    = $arrResult['content'];
                    $strError  = $arrResult['error'];                    

                } elseif ((strstr($arrLineList[0], 'Arbeitgeber') !== false) || (strstr($arrLineList[0], 'Archiv Beitragsnachweise') !== false) || (strstr($arrLineList[0], 'BNR Krankenkasse') !== false)) { // Lexware
                    
                    //echo $arrPdfContent['content'][0]; die();

                    $arrResult = handleLexware($arrPdfContent['content']);
                    $strCsv    = $arrResult['content'];
                    $strError  = $arrResult['error'];                    

                } elseif (strstr($arrPdfContent['content'][0], 'edlohn') !== false) { // Lohnwerk
                    
                    //echo $arrPdfContent['content'][0]; die();

                    $arrResult = handleLohnwerk($arrPdfContent['content']);
                    $strCsv    = $arrResult['content'];
                    $strError  = $arrResult['error'];                    

                } elseif (strstr($arrPdfContent['content'][0], 'NAVMVICLOUD') !== false) { // Navision
                    
                    //echo $arrPdfContent['content'][0]; die();

                    $arrResult = handleNavision($arrPdfContent['content']);
                    $strCsv    = $arrResult['content'];
                    $strError  = $arrResult['error'];                    

                } else {
                    
                    echo $arrPdfContent['content'][0]; die();
                }

                if ($strError == '') {

                    $strCsvFile = getCsvFileName($arrResult['month'], $arrResult['name']);
                    $strCsvPath = __DIR__ .'/../../files/temp/' .$strCsvFile;
    
                    file_put_contents($strCsvPath, createCsv($strCsv));
                
                    $arrItemList[] = array(
                        'name' => $strCsvFile,
                        'link' => 'files/temp/' .$strCsvFile, // 
                        'state' => 'ok'
                    );
    
                } else {
                    
                    $arrItemList[] = array(
                        'name' => $_FILES[$strUpload]['name'],
                        'state' => 'error',
                        'error' => $strError
                    );

                }

            } else {

                $arrItemList[] = array(
                    'name' => $_FILES[$strUpload]['name'],
                    'state' => 'error',
                    'error' => 'Keine lesbaren Daten vorhanden!'
                );

            }


        }

    } else {

        $arrItemList[] = array(
            'name' => $_FILES[$strUpload]['name'],
            'state' => 'error',
            'error' => 'Es sind nur PDF Dateien erlaubt!'
        );

    }

    $arrResponce = array(

        'id' => $strHash,
        'done' => true,
        'items' => $arrItemList

    );

    //rmdir($strHash);

}

echo json_encode($arrResponce);

?>