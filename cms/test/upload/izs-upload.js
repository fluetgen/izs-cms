const templateCss = /*html*/ `
<style>
    :host {
        --light: #f0f0f0;
        --muted: #6c757d;
        --border: rgba(0, 0, 0, .125);
        --danger: #dc3545;
        --success: #28a745;
        --active: #fff;
        --highlight: #007bff;
        --border-radius: 0.3rem;
        display: inline-block;
    }

    .ml-2 {
        margin-left: 1rem;
    }

    .mb-3 {
        margin-bottom: 1.5rem;
    }

    .small {
        font-size: 80%;
    }

    .text-danger {
        color: var(--danger);
    }

    .btn {
        padding: 0.75em 1em;
        font-size: inherit;
        border: none;
        cursor: pointer;
        border-radius: var(--border-radius);
        background: var(--highlight);
        color: var(--active);
    }

    .btn:hover {
        opacity: 0.9;
    }

    .alert {
        padding: 1em;
        border-radius: var(--border-radius);
        color: var(--active);
    }

    .alert-error {
        background: var(--danger);
    }

    .list-group {
        border-radius: var(--border-radius);
        border: 1px solid var(--border);
    }

    .list-group-item {
        padding: .5em 1em;
        border-top: 1px solid var(--border);
    }

    .list-group-item:first-child {
        border-top: none;
    }

    .flex {
        display: flex;
    }

    .flex-1 {
        flex: 1 1 auto;
    }

    .align-items-center {
        align-items: center;
    }

    .upload_button {
        position: relative;
        text-align: center;
    }

    .upload_button input {
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        position: absolute;
        opacity: 0.001;
        z-index: 1;
        cursor: pointer;
    }

    .upload-progessbar {
        height: 6px;
        border-radius: var(--border-radius);
        position: relative;
        margin-top: 4px;
        overflow: hidden;
        background: var(--light);
    }

    .upload-progessbar-indicator {
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        position: absolute;
        transform: scaleX(0.2);
        transform-origin: left;
        background: var(--highlight);
        transition: transform 0.15s ease-in-out;
    }

    .gg-ok {
        box-sizing: border-box;
        position: relative;
        display: block;
        transform: scale(var(--ggs, 1));
        width: 20px;
        height: 20px;
        border: 2px solid transparent;
        border-radius: 100px;
        color: var(--success);
    }

    .gg-ok::after {
        content: "";
        display: block;
        box-sizing: border-box;
        position: absolute;
        left: 3px;
        top: -1px;
        width: 6px;
        height: 10px;
        border-width: 0 2px 2px 0;
        border-style: solid;
        transform-origin: bottom left;
        transform: rotate(45deg)
    }

    .gg-error {
        box-sizing: border-box;
        position: relative;
        display: block;
        transform: scale(var(--ggs, 1));
        width: 20px;
        height: 20px;
        border: 2px solid;
        border-radius: 40px;
        color: var(--danger);
    }

    .gg-error::after,
    .gg-error::before {
        content: "";
        display: block;
        box-sizing: border-box;
        position: absolute;
        border-radius: 3px;
        width: 2px;
        background: currentColor;
        left: 7px
    }

    .gg-error::after {
        top: 2px;
        height: 8px
    }

    .gg-error::before {
        height: 2px;
        bottom: 2px
    }

    .gg-pending {
        color: var(--muted);
        transform: scale(var(--ggs, 1))
    }

    .gg-pending,
    .gg-pending::after,
    .gg-pending::before {
        box-sizing: border-box;
        position: relative;
        display: block;
        width: 20px;
        height: 20px
    }

    .gg-pending::after,
    .gg-pending::before {
        content: "";
        position: absolute;
        border-radius: 100px
    }

    .gg-pending::before {
        animation: pending 1s
        cubic-bezier(.6,0,.4,1) infinite;
        border: 3px solid transparent;
        border-top-color: currentColor
    }

    .gg-pending::after {
        border: 3px solid;
        opacity: .2
    }

    @keyframes pending {
        0% { transform: rotate(0deg) }
        to { transform: rotate(359deg) }
    }
</style>
`;

const templateForm = /*html*/ `
<div class="btn upload_button">
    <input type="file" />
    <div>Dateien auswählen</div>
</div>
`;

const templateUploading = /*html*/ `
<div class="upload-progess">
    <div>Lade Dateien hoch...</div>
    <div class="upload-progessbar">
        <div class="upload-progessbar-indicator"></div>
    </div>
</div>
`;

const templateState = /*html*/ `
<div>
    <div class="list-group mb-3">
        {{#state.items}}
            <div class="list-group-item flex align-items-center state-{{state}}">
                <i class="gg-{{state}}"></i>
                <div class="flex-1 ml-2">
                    <div>{{name}}</div>
                    {{#error}}
                        <div class="small text-danger">{{error}}</div>
                    {{/error}}
                </div>
            </div>
        {{/state.items}}
    </div>

    {{#state.done}}
        <button id="reset" class="btn">Zurück</button>
    {{/state.done}}
</div>
`;

const templateError = /*html*/ `
<div class="alert alert-error">
    {{error}}
</div>
`;

class IzsUpload extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: "open" });
        this.$scope = { template: templateForm, state: null };

        // DND
        this.addEventListener("dragenter", e => e.preventDefault());
        this.addEventListener("dragover", e => e.preventDefault());
        this.addEventListener("drop", e => {
            if (this.$scope.template === templateForm) {
                this.upload(Array.from(e.dataTransfer.files));
            }

            e.preventDefault();
        });
    }

    render(template = templateForm) {
        // Render
        this.shadowRoot.innerHTML = mustache(templateCss + template, this.$scope);

        // File upload
        const $input = this.shadowRoot.querySelector("input");
        if ($input) {
            if (this.getAttribute("multiple") !== null) {
                $input.setAttribute("multiple", "multiple");
            }

            if (this.getAttribute("accept")) {
                $input.setAttribute("accept", this.getAttribute("accept"));
            }

            $input.addEventListener("change", () => {
                this.upload($input.files);
            });
        }

        // Reset
        const $resetBtn = this.shadowRoot.querySelector("#reset");
        if ($resetBtn) {
            $resetBtn.addEventListener("click", () => {
                this.$scope.state = null;
                this.render();
            });
        }

        // Progress Bar
        const $progressIndicator = this.shadowRoot.querySelector(".upload-progessbar-indicator");
        if ($progressIndicator) {
            let percent = 0;
            function progress() {
                percent += 0.01;
                if (percent < 1) {
                    $progressIndicator.style.transform = `scaleX(${percent})`;
                    setTimeout(progress, 250);
                }
            }

            progress();
        }

        // Loading State
        if (this.$scope.state && this.$scope.state.id && !this.$scope.state.done) {
            setTimeout(() => {
                fetch(this.buildUrl("/status/" + this.$scope.state.id))
                    .then(res => res.json())
                    .then(state => {
                        this.$scope.state = this.prepareState(state);
                        this.render(templateState);
                    });
            }, 1000);
        }
    }

    upload(files) {
        let idx = 0;
        this.$scope.filenames = {};
        const body = new FormData();
        for (const file of files) {
            const fileId = "file_" + idx++;
            this.$scope.filenames[fileId] = file.name;
            body.append(fileId, file);
        }

        this.render(templateUploading);
        fetch(this.buildUrl("/upload/" + (this.getAttribute("action") || "test")), { body, method: "post" })
            .then(res => res.json())
            .then(state => {
                this.$scope.state = this.prepareState(state);
                this.render(templateState);
            })
            .catch(e => {
                this.$scope.error = e;
                this.render(templateError);
            });
    }

    connectedCallback() {
        this.render();
    }

    buildUrl(path) {
        return (this.getAttribute("base") || "") + path;
    }

    prepareState(result) {
        result.items = result.items.map(item => {
            item.name = this.$scope.filenames[item.name] || item.name;
            return item;
        });

        return result;
    }
}

window.customElements.define("izs-upload", IzsUpload);

//
function mustache(template, self, parent, invert) {
    var render = mustache;
    var output = "";
    var i;

    function get(ctx, path) {
        path = path.pop ? path : path.split(".");
        ctx = ctx[path.shift()];
        ctx = ctx != null ? ctx : "";
        return 0 in path ? get(ctx, path) : ctx;
    }

    self = Array.isArray(self) ? self : self ? [self] : [];
    self = invert ? (0 in self ? [] : [1]) : self;

    for (i = 0; i < self.length; i++) {
        var childCode = "";
        var depth = 0;
        var inverted;
        var ctx = typeof self[i] == "object" ? self[i] : {};
        ctx = Object.assign({}, parent, ctx);
        ctx[""] = { "": self[i] };

        template.replace(/([\s\S]*?)({{((\/)|(\^)|#)(.*?)}}|$)/g, function (match, code, y, z, close, invert, name) {
            if (!depth) {
                output += code.replace(/{{{(.*?)}}}|{{(!?)(&?)(>?)(.*?)}}/g, function (match, raw, comment, isRaw, partial, name) {
                    return raw ? get(ctx, raw) : isRaw ? get(ctx, name) : partial ? render(get(ctx, name), ctx) : !comment ? new Option(get(ctx, name)).innerHTML : "";
                });
                inverted = invert;
            } else {
                childCode += (depth && !close) || depth > 1 ? match : code;
            }
            if (close) {
                if (!--depth) {
                    name = get(ctx, name);
                    if (/^f/.test(typeof name)) {
                        output += name.call(ctx, childCode, function (template) {
                            return render(template, ctx);
                        });
                    } else {
                        output += render(childCode, name, ctx, inverted);
                    }
                    childCode = "";
                }
            } else {
                ++depth;
            }
        });
    }
    return output;
}
