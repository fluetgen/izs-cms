<?php

putenv('GNUPGHOME=/data/www/sites/www.izs.de/.gnupg');

error_reporting(E_ALL);

//$strTestcase = 'attached'; // Encoded part attached as file
//$strTestcase = 'withattach'; // Encoded part attached as file with another attachment
//$strTestcase = 'signed'; // Signed Text-Mail
$strTestcase = 'signedattach'; // Signed Text-Mail with attachment

$boolSign = true;

$strFrom    = 'auskunft@izs-institut.de';
$strTo      = 'system@izs-institut.de';
$strSubject = 'Encryption Test';
$strBody    = '';

$strEncryptText = 'Just a test äöüÄÖÜß';

include('gpg.exmples.php');

$resGpg    = gnupg_init();
//$boolCrypt = gnupg_addencryptkey($resGpg, '0C575B480E37CB195A3583D7213792F63981A298');
$boolSign  = gnupg_addsignkey($resGpg, '0C575B480E37CB195A3583D7213792F63981A298', 'mailkassi78%');

if ($strTestcase == 'attached') { // (German encoding fine, set charset to UTF-8)

    $strEnc = gnupg_encrypt($resGpg, $strTextOnly. $strEncryptText);

} else if ($strTestcase == 'withattach') {

    $strEnc = gnupg_encrypt($resGpg, $strTextWithAttachment);

} else if ($strTestcase == 'signed') {

    $signed = gnupg_sign($resGpg, 'Das ist ein Test.');

    $strBoundary = 'Apple-Mail=_93EB2CAF-6D2A-47D8-8E67-833D05BC6405'; //md5(time());

    $strHeader = 'From: =?UTF-8?B?' .base64_encode('IZS / Auskunft') .'?=<' .$strFrom .'>' ."\r\n";
    
    $strBody = substr($signed, 0, -1);

} else if ($strTestcase == 'signedattach') {

    $strBoundary = md5(time());

    ///*
    $msg = 'Content-Type: multipart/alternative;' ."\r\n";
    $msg.= '	boundary="Apple-Mail=_F8FD7B37-DE45-4BCD-A1C2-84AFBDC43044"' ."\r\n";
    $msg.= '' ."\r\n";
    $msg.= '' ."\r\n";
    $msg.= '--Apple-Mail=_F8FD7B37-DE45-4BCD-A1C2-84AFBDC43044' ."\r\n";
    $msg.= 'Content-Transfer-Encoding: quoted-printable' ."\r\n";
    $msg.= 'Content-Type: text/plain;' ."\r\n";
    $msg.= '	charset=utf-8' ."\r\n";
    $msg.= '' ."\r\n";
    $msg.= 'Hurra ÄÜÖ sie läuft!' ."\r\n";
    $msg.= '' ."\r\n";
    $msg.= '--Apple-Mail=_F8FD7B37-DE45-4BCD-A1C2-84AFBDC43044' ."\r\n";
    $msg.= 'Content-Transfer-Encoding: quoted-printable' ."\r\n";
    $msg.= 'Content-Type: text/html;' ."\r\n";
    $msg.= '	charset=utf-8' ."\r\n";
    $msg.= '' ."\r\n";
    $msg.= '<html><head><meta http-equiv=3D"Content-Type" content=3D"text/html; =' ."\r\n";
    $msg.= 'charset=3Dutf-8"></head><body style=3D"word-wrap: break-word; =' ."\r\n";
    $msg.= '-webkit-nbsp-mode: space; line-break: after-white-space;" class=3D"">Hurra=' ."\r\n";
    $msg.= ' ÄÜÖ sie <b class=3D"">läuft</b>!</body></html>=' ."\r\n";
    $msg.= '' ."\r\n";
    $msg.= '--Apple-Mail=_F8FD7B37-DE45-4BCD-A1C2-84AFBDC43044--' ."\r\n";
    $msg.= '' ."\r\n";
    //*/

    $msg = $strTextWithAttachment;

    gnupg_setsignmode($resGpg, GNUPG_SIG_MODE_DETACH);
    $signed = gnupg_sign($resGpg, $msg);

    $mxd = gnupg_verify($resGpg, $msg, $signed);

    //var_dump($mxd); die();

    $strBoundary = md5(time());

    $strHeader = 'From: =?UTF-8?B?' .base64_encode('IZS / Auskunft') .'?=<' .$strFrom .'>' ."\r\n" .
                'Content-Type: multipart/signed;' ."\r\n" .
                '	boundary="' .$strBoundary .'";' ."\r\n" .
                '	protocol="application/pgp-signature";' ."\r\n" .
                '	micalg=pgp-sha256' ."\r\n" .
                'X-Mailer: PHP/' . phpversion() ."\r\n" .
                'Mime-Version: 1.0';

    $strBody = '' ."\r\n";
    $strBody.= '--' .$strBoundary .'' ."\r\n";
    $strBody.= '' .$msg .'' ."\r\n";
    $strBody.= '--' .$strBoundary .'' ."\r\n";
    $strBody.= 'Content-Transfer-Encoding: 7bit' ."\r\n";
    $strBody.= 'Content-Disposition: attachment;' ."\r\n";
    $strBody.= '	filename=signature.asc' ."\r\n";
    $strBody.= 'Content-Type: application/pgp-signature;' ."\r\n";
    $strBody.= '	name=signature.asc' ."\r\n";
    $strBody.= 'Content-Description: Message signed with OpenPGP' ."\r\n";
    $strBody.= '' ."\r\n";
    $strBody.= '' .$signed .'' ."\r\n";
    $strBody.= '--' .$strBoundary .'--';

}

if (($strTestcase == 'attached') || ($strTestcase == 'withattach')) {

    $strBoundary = md5(time());

    $strHeader = 'From: =?UTF-8?B?' .base64_encode('Frank Lütgen') .'?=<' .$strFrom .'>' ."\r\n" .
                 'Content-Type: multipart/encrypted;' ."\r\n" .
                 '	boundary="' .$strBoundary .'";' ."\r\n" .
                 '	protocol="application/pgp-encrypted"' ."\r\n" .
                 'X-Mailer: PHP/' . phpversion() ."\r\n" .
                 'Mime-Version: 1.0';


    $strBody = '
--' .$strBoundary .'
Content-Transfer-Encoding: 7bit
Content-Type: application/pgp-encrypted
Content-Description: PGP/MIME Versions Identification

Version: 1

--' .$strBoundary .'
Content-Transfer-Encoding: 7bit
Content-Disposition: inline; filename=encrypted.asc
Content-Type: application/octet-stream;
    name=encrypted.asc
Content-Description: OpenPGP encrypted message

' .$strEnc .'

--' .$strBoundary .'--';

}

if ($strBody != '') {

    $boolMail = mail($strTo, $strSubject, $strBody, $strHeader);
    var_dump($boolMail);

}

?>