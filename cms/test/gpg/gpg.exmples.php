<?php

$strTextOnly = 'Content-Transfer-Encoding: 7bit
Content-Type: text/plain; charset=UTF-8

';

$strBoundaryEnc = md5(time());

$strFileName   = 'empty.pdf';
$strAttachment = file_get_contents($strFileName);
$strAttachment = chunk_split(base64_encode($strAttachment));

$strMimeType = mime_content_type($strFileName);

$strTextWithAttachment = 'Content-Type: multipart/mixed;' ."\n\r";
$strTextWithAttachment.= '	boundary="' . $strBoundaryEnc .'"' ."\n\r";
$strTextWithAttachment.= '' ."\n\r";
$strTextWithAttachment.= '' ."\n\r";
$strTextWithAttachment.= '--' . $strBoundaryEnc .'' ."\n\r";
$strTextWithAttachment.= 'Content-Transfer-Encoding: 7bit' ."\n\r";
$strTextWithAttachment.= 'Content-Type: text/plain; charset=UTF-8' ."\n\r";
$strTextWithAttachment.= '' ."\n\r";
$strTextWithAttachment.= '' .$strEncryptText .'' ."\n\r";
$strTextWithAttachment.= '' ."\n\r";
$strTextWithAttachment.= '--' . $strBoundaryEnc .'' ."\n\r";
$strTextWithAttachment.= 'Content-Disposition: inline; filename="' .basename($strFileName) .'"' ."\n\r";
$strTextWithAttachment.= 'Content-Type: ' .$strMimeType .'; name="' .basename($strFileName) .'";' ."\n\r";
$strTextWithAttachment.= 'Content-Transfer-Encoding: base64' ."\n\r";
$strTextWithAttachment.= '' ."\n\r";
$strTextWithAttachment.= '' .str_replace("\n", "\r\n", $strAttachment) .'' ."\n\r";
$strTextWithAttachment.= '--' . $strBoundaryEnc .'--' ."\n\r";

$strTextWithAttachment = 'Content-Type: multipart/alternative;' ."\n\r";
$strTextWithAttachment.= '	boundary="Apple-Mail=_31B9F1D8-6EB4-465F-98D1-ADDC4F7CFCD3"' ."\n\r";
$strTextWithAttachment.= '' ."\n\r";
$strTextWithAttachment.= '' ."\n\r";
$strTextWithAttachment.= '--Apple-Mail=_31B9F1D8-6EB4-465F-98D1-ADDC4F7CFCD3' ."\n\r";
$strTextWithAttachment.= 'Content-Transfer-Encoding: 7bit' ."\n\r";
$strTextWithAttachment.= 'Content-Type: text/plain;' ."\n\r";
$strTextWithAttachment.= '	charset=us-ascii' ."\n\r";
$strTextWithAttachment.= '' ."\n\r";
$strTextWithAttachment.= 'Test' ."\n\r";
$strTextWithAttachment.= '' ."\n\r";
$strTextWithAttachment.= '' ."\n\r";
$strTextWithAttachment.= '--Apple-Mail=_31B9F1D8-6EB4-465F-98D1-ADDC4F7CFCD3' ."\n\r";
$strTextWithAttachment.= 'Content-Type: multipart/mixed;' ."\n\r";
$strTextWithAttachment.= '	boundary="Apple-Mail=_8A419648-6889-41F5-BB8A-7F0473A7E938"' ."\n\r";
$strTextWithAttachment.= '' ."\n\r";
$strTextWithAttachment.= '' ."\n\r";
$strTextWithAttachment.= '--Apple-Mail=_8A419648-6889-41F5-BB8A-7F0473A7E938' ."\n\r";
$strTextWithAttachment.= 'Content-Transfer-Encoding: 7bit' ."\n\r";
$strTextWithAttachment.= 'Content-Type: text/html;' ."\n\r";
$strTextWithAttachment.= '	charset=us-ascii' ."\n\r";
$strTextWithAttachment.= '' ."\n\r";
$strTextWithAttachment.= '<html><head><meta http-equiv="Content-Type" content="text/html; charset=us-ascii"></head><body style="word-wrap: break-word; -webkit-nbsp-mode: space; line-break: after-white-space;" class="">Test<div class=""><br class=""></div><div class=""></div></body></html>' ."\n\r";
$strTextWithAttachment.= '--Apple-Mail=_8A419648-6889-41F5-BB8A-7F0473A7E938' ."\n\r";
$strTextWithAttachment.= 'Content-Disposition: inline;' ."\n\r";
$strTextWithAttachment.= '	filename="' .basename($strFileName) .'"' ."\n\r";
$strTextWithAttachment.= 'Content-Type: ' .$strMimeType .';' ."\n\r";
$strTextWithAttachment.= '	name="' .basename($strFileName) .'";' ."\n\r";
$strTextWithAttachment.= '	x-unix-mode=0644' ."\n\r";
$strTextWithAttachment.= 'Content-Transfer-Encoding: base64' ."\n\r";
$strTextWithAttachment.= '' ."\n\r";
$strTextWithAttachment.= '' .str_replace("\n", "\r\n", $strAttachment) .'';
$strTextWithAttachment.= '' ."\n\r";
$strTextWithAttachment.= '--Apple-Mail=_8A419648-6889-41F5-BB8A-7F0473A7E938' ."\n\r";
$strTextWithAttachment.= 'Content-Transfer-Encoding: 7bit' ."\n\r";
$strTextWithAttachment.= 'Content-Type: text/html;' ."\n\r";
$strTextWithAttachment.= '	charset=us-ascii' ."\n\r";
$strTextWithAttachment.= '' ."\n\r";
$strTextWithAttachment.= '<html><head><meta http-equiv="Content-Type" content="text/html; charset=us-ascii"></head><body style="word-wrap: break-word; -webkit-nbsp-mode: space; line-break: after-white-space;" class=""><div class=""></div></body></html>' ."\n\r";
$strTextWithAttachment.= '--Apple-Mail=_8A419648-6889-41F5-BB8A-7F0473A7E938--' ."\n\r";
$strTextWithAttachment.= '' ."\n\r";
$strTextWithAttachment.= '--Apple-Mail=_31B9F1D8-6EB4-465F-98D1-ADDC4F7CFCD3--';

?>