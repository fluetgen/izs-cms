<?php

session_start();
error_reporting(E_ALL);
ini_set("display_errors", 1); 

//FUNCTIONS
function curl_post ($strUrl, array $arrPost = NULL, array $arrOptions = array(), $arrHeader = array()) { 

  $arrDefault = array( 
      CURLOPT_POST => TRUE,
      CURLOPT_POSTFIELDS => json_encode($arrPost),
      CURLOPT_RETURNTRANSFER => TRUE, 
      CURLOPT_URL => $strUrl,
      CURLOPT_HEADER => TRUE,
      CURLOPT_SSL_VERIFYPEER => TRUE
  ); 

  $resCurl = curl_init(); 

  $arrHeader[] = 'Accept: application/json';
  $arrHeader[] = 'Content-Type: application/json';

  if (count($arrHeader) > 0) {
      curl_setopt ($resCurl, CURLOPT_HTTPHEADER, $arrHeader); 
  }
  
  curl_setopt_array($resCurl, ($arrOptions + $arrDefault)); 
  if(!$mxdResult = curl_exec($resCurl)) { 
      trigger_error(curl_error($resCurl)); 
  } 
  $arrInfo = curl_getinfo($resCurl);
  curl_close($resCurl);

  $strHeader = trim(substr($mxdResult, 0, $arrInfo['header_size']));
  $strBody = substr($mxdResult, $arrInfo['header_size']);

  return $strBody; 
} 

function curl_get ($strUrl, array $arrGet = NULL, array $arrOptions = array(), $arrHeader = array()) { 

  $arrDefault = array( 
      CURLOPT_URL => $strUrl. (strpos($strUrl, '?') === FALSE ? '?' : ''). http_build_query($arrGet), 
      CURLOPT_HEADER => true, 
      CURLOPT_RETURNTRANSFER => true, 
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_VERBOSE => true,
      CURLOPT_TIMEOUT => 4,
      CURLOPT_SSL_VERIFYPEER => true
  ); 
  
  $resCurl = curl_init();

  if (count($arrHeader) > 0) {
      curl_setopt ($resCurl, CURLOPT_HTTPHEADER, $arrHeader); 
  }

  curl_setopt_array($resCurl, ($arrOptions + $arrDefault)); 
  if(!$mxdResult = curl_exec($resCurl)) { 
      trigger_error(curl_error($resCurl)); 
  } 

  $arrInfo = curl_getinfo($resCurl);
  curl_close($resCurl);

  $strHeader = trim(substr($mxdResult, 0, $arrInfo['header_size']));
  $strBody = substr($mxdResult, $arrInfo['header_size']);

  return array('header' => $strHeader, 'body' =>$strBody); 
} 


//CREATE CHAT ON "ANFRAGE SENDEN"

$arrHeader = array(
  'apikey: e9d99940-1aed-414a-b836-b929fe97d093',
  'user: 3',
  'Accept: application/json'
);

if (1) {
  
    if (1 > 0) {
  
      //CREATE CHAT ON "ANFRAGE SENDEN"
      if (true) {
  
        $arrTeam = array(
          32 => 'Serap', 
          36 => 'Gordon'
        );
  
        $arrHeader = array(
          'apikey: e9d99940-1aed-414a-b836-b929fe97d093',
          'user: ' .$_SESSION['id'],
          'Accept: application/json'
        );
  
        //Create User List
        $arrRes = curl_get('http://api.izs-institut.de/api/sg/user', array(), array(), $arrHeader);
        $arrUserList = json_decode($arrRes, true);
  
        //Create Context (name, uri)
        $arrContext = array(
          'name' => 'IZS/Basisdokumentation/Testgruppe/Testdokument',
          'uri' => 'https://www.izs.de/cms/index_neu.php?ac=bd_aktualisieren#123456'
        );
  
        $arrRes = curl_post('http://api.izs-institut.de/api/sg/context', $arrContext, array(), $arrHeader);
        $arrContext = json_decode($arrRes, true);
  
        if (1) {
    
          $arrRes = curl_get('http://api.izs-institut.de/api/sg/category', array(), array(), $arrHeader);
          $arrCatList = json_decode($arrRes, true);
  
          $arrMember = array($arrUserList[$_SESSION['id']]);
    
          $arrFollower = array();
          foreach ($arrTeam as $intTeamId => $strName) {
            if ($intTeamId != $_SESSION['id']) {
              $arrFollower[] = $arrUserList[$intTeamId];
            }
          }
    
          $intEscDate = strtotime('30.12.1972') * 1000;
    
          $arrTopic = array(
            'title' => 'Warten auf Eingang: Gruppe - Typ - Dokument',
            'category' => array_search('Basisdokumentation', $arrCatList),
            'context' => $arrContext['id'],
            'member' => $arrMember,
            'follow' => $arrFollower,
            // 'followup' => array($arrUserList[$_SESSION['id']] => (int) $intEscDate),
            //'text' => '', //'E-Mail Benachrichtigung gesendet am ' .date('d.m.Y') .' um ' .date('H:i:s') .' an folgende Empfänger:<br /><br />' .$strTo,
            'due' => (int) $intEscDate
          );
    
          $arrRes = curl_post('http://api.izs-institut.de/api/sg/topic', $arrTopic, array(), $arrHeader);
          $arrTop = json_decode($arrRes, true);
    
        }
  
        //Write chat content
        if (!isset($arrTop['id'])) {
          
          $arrRes = curl_get('http://api.izs-institut.de/api/sg/topic/' .$arrContext['id'], array(), array(), $arrHeader);
          $arrTopicList = json_decode($arrRes, true);
  
          foreach ($arrTopicList as $intSort => $arrTopic) {
  
            foreach ($arrTopic as $strId => $strTitle) {
              if (strstr($strTitle, 'Warten auf Eingang: ') !== false) {
                $arrTop['id'] = $strId;
                break;
              }
            }
  
          }
  
        }
  
        $arrPost = array(
          'author' => $arrUserList[$_SESSION['id']],
          'context' => $arrContext['id'],
          'text' => '<p>E-Mail ("Status") gesendet am ' .date('d.m.Y') .' um ' .date('H:i:s') .' an folgende Empfänger:<br />keine</p>',
          'target' => $arrTop['id'],
          'topic' => $arrTop['id'],
          'type' => 'content',
          'contenttype' => 'message'
        );
  
        $arrRes = curl_post('http://api.izs-institut.de/api/sg/content', $arrPost, array(), $arrHeader);
  
        //update due
        if ($strUStatusNew != '1. Erinnerung senden') {
          ;
        }
  
      }
    

    }
    
    echo json_encode($arrJson);
    
  }


?>