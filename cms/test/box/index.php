<?php

function getCurl ($folderid, $access_token, $arrGet){
    //===================== Default cUrl options =================
    $options = array(
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_VERBOSE        => true,
        CURLOPT_HEADER         => false,
        CURLINFO_HEADER_OUT    => false,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => false,
    );
    $options[CURLOPT_HTTPHEADER] = array ("Authorization: Bearer ".$access_token);

    //======================= Proper url ==========================
    $url = "https://api.box.com/2.0/folders/{$folderid}?" .http_build_query($arrGet);

    //======================= cUrl call ===========================
    $ch = curl_init($url);
    curl_setopt_array($ch, $options);
    $result = curl_exec($ch);
    curl_close($ch);
    $result = json_decode($result, true);

    return $result;
}

function array_to_csv_download($array, $filename = "export.csv", $delimiter=";") {
    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename="'.$filename.'";');

    // open the "output" stream
    // see http://www.php.net/manual/en/wrappers.php.php#refsect2-wrappers.php-unknown-unknown-unknown-descriptioq
    $f = fopen('php://output', 'w');

    foreach ($array as $line) {
        fputcsv($f, $line, $delimiter);
    }
}  

// 271889689 -> entleiher
// 568303604 -> verleiher

$arrFolderList = getCurl(568303604, 'gqZELnU5UWeboFlwU1e59QqHk5J08Ayf', array('limit' => 1000));

foreach ($arrFolderList['item_collection']['entries'] as $intKey => $arrFolder) {    
    
    if (($intKey > 499) && ($intKey <= 599)) {
        $arrShared = getCurl($arrFolder['id'], 'gqZELnU5UWeboFlwU1e59QqHk5J08Ayf', array('fields' => 'shared_link'));
        $arrReturn[] = array($arrFolder['id'], $arrFolder['name'], $arrShared['shared_link']['url']);
    }

}

array_to_csv_download($arrReturn, 'verleiher-06.csv');

?>