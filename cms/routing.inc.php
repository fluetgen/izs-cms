<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}
include('conf/global.inc.php');
include('classes/fklCsv.php');

$strOutput = '';
$strOutput.= '<h1>Routing-Tabelle</h1>' .chr(10);
      
$strSql = 'SELECT `SF42_Comany_ID__c`, `Name`, `SF42_isInformationProvider__c`, `SF42_informationProvider__c` FROM `Account` WHERE `RecordTypeId` = "01230000001Ao72AAC"';
$result = MySQLStatic::Query($strSql);

$strHtml = '';
$strHtml.= '<table id="5">' .chr(10);
$strHtml.= '  <thead>' .chr(10);
$strHtml.= '      <th>BNR KK </th>' .chr(10);
$strHtml.= '      <th>Name KK</th>' .chr(10);
$strHtml.= '      <th>Ist Auskunftsstelle</th>' .chr(10);
$strHtml.= '      <th>BNR IP</th>' .chr(10);
$strHtml.= '      <th>Name IP</th>' .chr(10);
$strHtml.= '      <th>Status</th>' .chr(10);
$strHtml.= '  </thead>' .chr(10);
$strHtml.= '  <tbody>' .chr(10);
$strHtml.= '';

foreach ($result as $row) {
  
  $strSatus = 'OK';
  
  $strHtml.= '    <tr>' .chr(10);
  $strHtml.= '      <td>' .$row['SF42_Comany_ID__c'] .'</td>' .chr(10);
  $strHtml.= '      <td>' .$row['Name'] .'</td>' .chr(10);
  $strHtml.= '      <td>' .$row['SF42_isInformationProvider__c'] .'</td>' .chr(10);
  
  if ($row['SF42_informationProvider__c'] != '') {
    if ($row['SF42_isInformationProvider__c'] == 'true') {
      $strSatus = '<span class="red">ERROR</span>';
    }
    
    $strSql2 = 'SELECT `SF42_Comany_ID__c`, `Name` FROM `Account` WHERE `Id` = "' .$row['SF42_informationProvider__c'] .'"';
    $result2 = MySQLStatic::Query($strSql2);
    foreach ($result2 as $row2) {
      $strBnrIp = $row2['SF42_Comany_ID__c'];
      $strNameIp = $row2['Name'];
    }
    
  } else {
    $strBnrIp = '';
    $strNameIp = '';
  }
  
  $strHtml.= '      <td>' .$strBnrIp .'</td>' .chr(10);
  $strHtml.= '      <td>' .$strNameIp .'</td>' .chr(10);
  $strHtml.= '      <td>' .$strSatus .'</td>' .chr(10);
  $strHtml.= '    </tr>' .chr(10);
} 

$strHtml.= '  </tbody>' .chr(10);
$strHtml.= '</table>' .chr(10);

$strOutput.= $strHtml;

$strOutput.= "
<script>
$('#5').fixheadertable({ 
   colratio    : [100, 330, 80, 80, 300, 60], 
   height      : 500, 
   width       : 960, 
   zebra       : true, 
   sortable    : true,
   sortType    : ['string', 'string', 'string', 'string', 'string', 'string'],
   sortedColId : 5, 
   dateFormat  : 'Y-m'
});
</script>
";

/*
BNR KK (Account-Datensatztyp = Krankenkasse)
Name KK
Ist Auskunftsstelle
BNR IP
Name IP
*/

?>