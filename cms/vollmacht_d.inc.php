<?php

session_start();

$start = microtime(true);

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth_d');
}

require_once('../assets/classes/class.mysql.php');
require_once('classes/class.informationprovider.php');
require_once('classes/class.premiumpayer.php');
require_once('classes/class.anfragestelle.php');
require_once('classes/class.event.php');

if (isset($_SERVER['HTTP_FKLD']) && ($_SERVER['HTTP_FKLD'] == 'on')) {
  //phpinfo();
  //die();
}

function cleanForPdfOutput ($strInput) {
  
    return str_replace('–', '-', $strInput);
    
}

$objAnfragestelle = new Anfragestelle;
$objIP = new InformationProvider;
$objPp = new PremiumPayer;
$objEvent = new Event;

$boolCronSet = false;
$arrInformationProvider = array();
$arrIpAmpel = array();

$arrAngefragtStatus = array();
$arrKlaerungStatus = array();

if (isset($_REQUEST['startCron']) && ($_REQUEST['startCron'] != '')) {

  $strSql = 'UPDATE `_cron_url` SET `cu_status` = 1 WHERE `cu_status` = 0 AND `cu_hash` = "' .$_REQUEST['startCron'] .'"';
  $arrRow = MySQLStatic::Update($strSql);
  $boolCronSet = true;
  
}

$strHashVollmacht = md5(uniqid(rand(), true));
$strHashAnfrage = md5(uniqid(rand(), true));

$strOutput = '';
$arrRequestWay = array();
$arrDecentral = array();
$arrEventStatus = array();

$strOutput.= '<h1>Anfrage dezentral</h1>' .chr(10);

$strSql0 = "SELECT DISTINCT `Beitragsmonat__c`, `Beitragsjahr__c` FROM `cron_izs_sv_month` ORDER BY `Beitragsjahr__c` DESC, `Beitragsmonat__c` DESC";
$arrResult0 = MySQLStatic::Query($strSql0);
if (count($arrResult0) > 0) {
  
  $strOutput.= '<form method="post" action="">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);

  $strOutput.= '<table>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Zeitraum: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelDate" id="strSelDate" onchange="this.form.submit()">' .chr(10);
  
  $intPeriod = 0;

  foreach ($arrResult0 as $arrPeriod0) {
    $strSelected = '';
    $strValue = $arrPeriod0['Beitragsmonat__c'] .'_' .$arrPeriod0['Beitragsjahr__c'];
    
    if (empty($_REQUEST['strSelDate']) && ($intPeriod == 0)) {
      $_REQUEST['strSelDate'] = $strValue;
      //$_REQUEST['send'] = 1;
    }
    
    if ($strValue == $_REQUEST['strSelDate']) {
      $strSelected = ' selected="selected"';
    } 
    $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .str_pad($arrPeriod0['Beitragsmonat__c'], 2 ,'0', STR_PAD_LEFT) .'-' .$arrPeriod0['Beitragsjahr__c'] .'</option>' .chr(10);
    $intPeriod++;
  }
  
  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $arrResult5 = [
    ['SF42_Anfrageweg__c' => "E-Mail"],
    ['SF42_Anfrageweg__c' => "Fax"],
    ['SF42_Anfrageweg__c' => "Post"],
    ['SF42_Anfrageweg__c' => "Webmailer"]
  ];

  //$strSql5 = 'SELECT `Anfrageweg__c` FROM `Anfragestelle__c`WHERE `Anfrageweg__c` != "" GROUP BY `Anfrageweg__c` ORDER BY `Anfrageweg__c`';
  //$arrResult5 = MySQLStatic::Query($strSql5);
  if (count($arrResult5) > 0) {
    
    if (($_REQUEST['strSelWay'] == 'all') || ($_REQUEST['strSelWay'] == '')) {
      $strSelectedAll = ' selected="selected"';
      $_REQUEST['strSelWay'] = 'all';
    } else {
      $strSelectedAll = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Anfrageweg: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelWay" id="strSelWay" onchange="this.form.submit()">' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>Alle</option>' .chr(10);
    
    foreach ($arrResult5 as $arrReqWay) {
      $strSelected = '';

      if ($arrReqWay['Anfrageweg__c'] == $_REQUEST['strSelWay']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option value="' .$arrReqWay['Anfrageweg__c'] .'"' .$strSelected .'>' .$arrReqWay['Anfrageweg__c'] .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
    
  $strSql6 = 'SELECT `ct_id`, `ct_name` FROM `cms_text` WHERE `ct_type` = 0 ';
  if (($_REQUEST['strSelWay'] != '') && ($_REQUEST['strSelWay'] != 'all')) {
    $strSql6.= 'AND `ct_category` = "' .$_REQUEST['strSelWay'] .'" ';
  }  
  $strSql6.= 'ORDER BY `ct_name`';
  $arrResult6 = MySQLStatic::Query($strSql6);
  if (count($arrResult6) > 0) {
    
    if ($_REQUEST['strSelText'] == '') {
      $strClass = ' class="red"';
    } else {
      $strClass = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Textvorlage: </td>' .chr(10);
    $strOutput.= '    <td><select' .$strClass .' name="strSelText" id="strSelText" onchange="this.form.submit()">' .chr(10);
    $strOutput.= '    <option value="">Bitte auswählen</option>' .chr(10);
    
    foreach ($arrResult6 as $arrText) {
      $strSelected = '';

      if ($arrText['ct_id'] == $_REQUEST['strSelText']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option class="black" value="' .$arrText['ct_id'] .'"' .$strSelected .'>' .$arrText['ct_name'] .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
  
  $arrRueckSel = array('gelb' => 'offen', 'gruen' => 'vollständig');

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Rücklauf: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelRuec" id="strSelRuec">' .chr(10);
  $strOutput.= '    <option value=""></option>' .chr(10);
  
  foreach ($arrRueckSel as $strKey => $strText) {
    $strSelected = '';

    if ($strKey == $_REQUEST['strSelRuec']) {
      $strSelected = ' selected="selected"';
    } 
    $strOutput.= '    <option value="' .$strKey .'"' .$strSelected .'>' .$strText .'</option>' .chr(10);
  }

  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td></td>' .chr(10);
  $strOutput.= '    <td><input type="submit" value="anzeigen"></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);
    
}

if (!isset($_REQUEST['strSelWay'])) {
  $_REQUEST['strSelWay'] = 'all';
}

if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {
  
  $arrPeriod = explode('_', $_REQUEST['strSelDate']);

  $arrAnfragestelleList = [];
  $arrAnfragestelleCatchAllList = [];
  $strSql5 = 'SELECT `Id`, `Name`, `CATCH_ALL__c`, `Information_Provider__c`, `Anfrageweg__c` FROM `Anfragestelle__c`'; // WHERE `Id` = "' .$arrResult4[0]['Anfragestelle__c'] .'"
  $arrResult5 = MySQLStatic::Query($strSql5);
  if (count($arrResult5) > 0) {
    foreach ($arrResult5 as $intKey => $arrAnf) {

      if ($arrAnf['CATCH_ALL__c'] == 'true') {
        unset($arrAnf['CATCH_ALL__c']);
        $strKey = $arrAnf['Information_Provider__c'];
        unset($arrAnf['Information_Provider__c']);
        $arrAnfragestelleCatchAllList[$strKey] = [0 => $arrAnf];
      }

      unset($arrAnf['CATCH_ALL__c']);
      unset($arrAnf['Information_Provider__c']);
      $arrAnfragestelleList[$arrAnf['Id']] = [0 => $arrAnf];
    }
  }

  $strSql3 = 'SELECT * FROM `Account` WHERE `Dezentrale_Anfrage__c` = "true" AND `SF42_isInformationProvider__c` = "true" ORDER BY `Name`';
  $arrKKList = MySQLStatic::Query($strSql3);

  $arrCatchAllIdList = [];
  if (count($arrKKList) > 0) { 
    foreach ($arrKKList as $intAcc => $arrKK) {
      $arrCatchAllIdList[] = $arrKK['Id'];
    }

    $strKKIdList = '"' .implode('", "', $arrCatchAllIdList) .'"';

    $strSql6 = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Beitragsmonat__c` = "' .$arrPeriod[0] .'" AND `Beitragsjahr__c` = "' .$arrPeriod[1] .'" ';
    $strSql6.= 'AND (`SF42_EventStatus__c` != "abgelehnt / Dublette") ';
    $strSql6.= 'AND (`SF42_EventStatus__c` != "zurückgestellt von IZS") ';
    $strSql6.= 'AND `SF42_informationProvider__c` IN (' .$strKKIdList .') GROUP BY `Betriebsnummer_ZA__c`';
    $arrEventList = MySQLStatic::Query($strSql6);

    $arrEventListRaw = [];
    if (count($arrEventList) > 0) {
      foreach ($arrEventList as $intKey => $arrEventRaw) {
        $arrEventListRaw[$arrEventRaw['SF42_informationProvider__c']][] = $arrEventRaw;
      }
    }

  }

  $arrAccListRaw = [];
  $strSqlAc = 'SELECT * FROM `Account` '; // WHERE `SF42_Comany_ID__c`="' .$arrEvent['Betriebsnummer_ZA__c'] .'"
  $arrAc = MySQLStatic::Query($strSqlAc);
  if (count($arrAc) > 0) {
    foreach ($arrAc as $intKey => $arrAccRaw) {
      $arrAccListRaw[$arrAccRaw['SF42_Comany_ID__c']] = [0 => $arrAccRaw];
    }
  }

  $arrDezAnfList = [];
  $strSql4 = 'SELECT `Anfragestelle__c`, `Premium_Payer__c`, `Information_Provider__c` FROM `Dezentrale_Anfrage_KK__c`'; // WHERE `Premium_Payer__c` = "' .$arrEvent['PpId'] .'" AND `Information_Provider__c` = "' .$arrEvent['SF42_informationProvider__c'] .'"';
  $arrRes4 = MySQLStatic::Query($strSql4);
  if (count($arrRes4) > 0) {
    foreach ($arrRes4 as $intKey => $arrDezAnf) {
      $strKey = $arrDezAnf['Premium_Payer__c'] .'_' .$arrDezAnf['Information_Provider__c'];
      unset($arrDezAnf['Premium_Payer__c']);
      unset($arrDezAnf['Information_Provider__c']);
      $arrDezAnfList[$strKey] = [0 => $arrDezAnf];
    }
  }


  //print_r($arrEventListRaw); die();
  
  foreach ($arrKKList as $intAcc => $arrKK) {
    
    //echo $arrKK['Name'] .'<br />' .chr(10); continue;

    //$start = microtime(true);   //ZEIT
    
    //$strSqlCA = 'SELECT * FROM `Anfragestelle__c` WHERE `Information_Provider__c`="' .$arrKK['Id'] .'" AND `CATCH_ALL__c` = "true"';
    //$arrCatchAll = MySQLStatic::Query($strSqlCA);

    $arrCatchAll = $arrAnfragestelleCatchAllList[$arrKK['Id']] ?? [];

    if ($arrIpAmpel[$arrCatchAll[0]['Id']] != 'gelb') {
      $arrIpAmpel[$arrCatchAll[0]['Id']] = 'gruen';
    }

    if ($_SERVER['REMOTE_ADDR'] == '193.174.158.110') {
      //echo $strSqlCA .'<br />' .chr(10);
    }
    
    //print_r($arrCatchAll);die();
    
    if (count($arrCatchAll) > 0) {

      if ($_SERVER['REMOTE_ADDR'] == '193.174.158.110') {
        //echo $arrKK['Name'] .' -> ' .$arrCatchAll[0]['Name'] .'<br />' .chr(10);
      }
      
      //$strSql6 = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Beitragsmonat__c` = "' .$arrPeriod[0] .'" AND `Beitragsjahr__c` = "' .$arrPeriod[1] .'" ';
      //$strSql6.= 'AND (`SF42_EventStatus__c` != "abgelehnt / Dublette") ';
      //$strSql6.= 'AND (`SF42_EventStatus__c` != "zurückgestellt von IZS") ';
      //$strSql6.= 'AND `SF42_informationProvider__c` = "' .$arrKK['Id'] .'" GROUP BY `Betriebsnummer_ZA__c`';
      //$arrEventList = MySQLStatic::Query($strSql6);

      $arrEventList = $arrEventListRaw[$arrKK['Id']] ?? [];
      
      //echo count($arrEventList); die();

      foreach ($arrEventList as $intEv => $arrEvent) {

        //$strSqlAc = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c`="' .$arrEvent['Betriebsnummer_ZA__c'] .'"';
        //$arrAc = MySQLStatic::Query($strSqlAc);

        $arrAc = $arrAccListRaw[$arrEvent['Betriebsnummer_ZA__c']] ?? [];
        
        //$strSql8 = 'SELECT `Id` FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrAc[0]['Id'] .'" AND `Information_Provider__c` = "' .$arrKK['Id'] .'"';
        //$arrResult8 = MySQLStatic::Query($strSql8);

        $strKey = $arrAc[0]['Id'] .'_' .$arrKK['Id'];
        $arrResult8 = $arrDezAnfList[$strKey] ?? [];
        
        //keinem Team zugeordnet
        if (count($arrResult8) == 0) {

          if (1) {
            //echo $arrKK['Name'] .' -> ' .$arrCatchAll[0]['Name'] .'<br />' .chr(10);
          }
          
          if ($arrAngefragtStatus[$arrEvent['SF42_informationProvider__c']] == '') $arrAngefragtStatus[$arrEvent['SF42_informationProvider__c']] = 0; 
          if ($arrKlaerungStatus[$arrEvent['SF42_informationProvider__c']] == '') $arrKlaerungStatus[$arrEvent['SF42_informationProvider__c']] = 0; 

          if ($arrEvent['SF42_EventStatus__c'] == 'enquired') {
            $arrAngefragtStatus[$arrEvent['SF42_informationProvider__c']]++;
            if ($arrEvent['Status_Klaerung__c'] == 'in Klärung') {
              $arrKlaerungStatus[$arrEvent['SF42_informationProvider__c']]++;
            }
          }
          
          if (($arrIpAmpel[$arrCatchAll[0]['Id']] == 'gruen') && (($arrEvent['SF42_EventStatus__c'] == 'enquired') && (($arrEvent['Rueckmeldung_am__c'] == '0000-00-00') || ($arrEvent['Rueckmeldung_am__c'] == '')))) {
            $arrIpAmpel[$arrCatchAll[0]['Id']] = 'gelb';
          } 

          if ((($arrEvent['SF42_EventStatus__c'] == 'enquired') && (($arrEvent['Rueckmeldung_am__c'] == '0000-00-00') || ($arrEvent['Rueckmeldung_am__c'] == '')))) {
            if (!isset($arrEventStatus[$arrCatchAll[0]['Id']][0])) {
              $arrEventStatus[$arrCatchAll[0]['Id']][0] = 0;
            }
            $arrEventStatus[$arrCatchAll[0]['Id']][0]++;
          } else {
            if (!isset($arrEventStatus[$arrCatchAll[0]['Id']][1])) {
              $arrEventStatus[$arrCatchAll[0]['Id']][1] = 0;
            }
            $arrEventStatus[$arrCatchAll[0]['Id']][1]++;
          }

          if (($arrEvent['SF42_EventStatus__c'] == 'enquired') || ($arrEvent['SF42_EventStatus__c'] == 'to enquire')) {
            $arrInformationProvider[$arrCatchAll[0]['Id']] = $arrCatchAll[0]['Name'];
            $arrRequestWay[$arrCatchAll[0]['Id']] = $arrCatchAll[0]['Anfrageweg__c'];
          }
          
        }
        
      }

      
    }

    //print_r($arrRequestWay);
    //die();
    
    //print_r($arrAngefragtStatus); die();

    $end = microtime(true);     //ZEIT
    $laufzeit = $end - $start;  //ZEIT
    //echo "Laufzeit: ".$laufzeit." Sekunden!" .'<br />' .chr(10); //ZEIT
    //die();
      
    /*
    if ($_SERVER['REMOTE_ADDR'] == '193.174.158.110') {
      //echo ' -> ' .$arrCatchAll[0]['Name'];
    }
    
    //ALT
    $strSql4 = 'SELECT * FROM `Dezentrale_Anfrage_KK__c` WHERE `Information_Provider__c` = "' .$arrKK['Id'] .'"';

  
    
    $arrDezAnfrageListe = MySQLStatic::Query($strSql4);

    //echo $strSql4 .'<br />' .chr(10);
    //echo $arrKK['Name'] .' AnfrageListe: ' .count($arrDezAnfrageListe) .'<br />' .chr(10);
    
    foreach ($arrDezAnfrageListe as $intDezAnf => $arrDezAnfrage) {

      if ($_SERVER['REMOTE_ADDR'] == '82.135.80.73') {
        //echo $arrKK['Name'];
        //echo ' -> ' .$arrDezAnfrage['Name'];
      }

      $strSql5 = 'SELECT `SF42_Comany_ID__c` FROM `Account` WHERE `Id` = "' .$arrDezAnfrage['Premium_Payer__c'] .'" ORDER BY `Name`';
      $arrAccountList = MySQLStatic::Query($strSql5);
      
      //print_r($arrDezAnfrageListe); die();
      
      if ($_SERVER['REMOTE_ADDR'] == '82.135.80.73') {
        //echo ' -> ' .$arrAccountList[0]['Name'];
      }
  
      $strSql6 = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Beitragsmonat__c` = "' .$arrPeriod[0] .'" AND `Beitragsjahr__c` = "' .$arrPeriod[1] .'" AND ';
      $strSql6.= '((`SF42_EventStatus__c` = "enquired") OR (`SF42_EventStatus__c` = "to enquire")) ';
      $strSql6.= 'AND `Betriebsnummer_ZA__c` = "' .$arrAccountList[0]['SF42_Comany_ID__c'] .'" AND `SF42_informationProvider__c` = "' .$arrKK['Id'] .'" ';
      $arrEventList = MySQLStatic::Query($strSql6);
      
      if ($_SERVER['REMOTE_ADDR'] == '82.135.80.73') {
        //echo ': ' .count($arrEventList) .'<br />' .chr(10);
      }

      if (count($arrEventList) > 0) {

        $end = microtime(true);     //ZEIT
        $laufzeit = $end - $start;  //ZEIT
        //echo "Laufzeit: ".$laufzeit." Sekunden!" .'<br />' .chr(10); //ZEIT

        $strSql7 = 'SELECT * FROM `Anfragestelle__c` WHERE `Id` = "' .$arrDezAnfrage['Anfragestelle__c'] .'"';
        $arrAnfrageListe = MySQLStatic::Query($strSql7);

        //echo $arrAnfrageListe[0]['Name'] .'<br />' .chr(10);
        
        $arrInformationProvider[$arrAnfrageListe[0]['Id']] = $arrAnfrageListe[0]['Name'];
        $arrRequestWay[$arrAnfrageListe[0]['Id']] = $arrAnfrageListe[0]['Anfrageweg__c'];
        
      }
      
    } 
    
    //print_r($arrInformationProvider);
    
    $end = microtime(true);     //ZEIT
    $laufzeit = $end - $start;  //ZEIT
    //echo "Laufzeit: ".$laufzeit." Sekunden!" .'<br />' .chr(10); //ZEIT
    
    */
    
  }

  //echo count($arrKKList); die();

$end = microtime(true);
$durationInMs = ($end-$start) * 1000;
echo '<!-- Abchnitt 01: ' .round($durationInMs) .'ms -->' .chr(10);
$start = microtime(true);
  
  //print_r($arrAngefragtStatus);
  //print_r($arrKlaerungStatus);
  
  $arrIpId = array();
  
  $strSqlNeu = '
  SELECT `Anfragestelle__c`.Id AS `Id`, Anfrageweg__c, `Anfragestelle__c`.Name AS `Name`, `SF42_EventStatus__c`, `Rueckmeldung_am__c`, `SF42_IZSEvent__c`.`SF42_informationProvider__c` AS `SF42_informationProvider__c`, `Status_Klaerung__c`  
  FROM `Dezentrale_Anfrage_KK__c` 
  INNER JOIN `Account` ON `Dezentrale_Anfrage_KK__c`.Premium_Payer__c = `Account`.Id 
  INNER JOIN `Account` AS `Account2` ON `Dezentrale_Anfrage_KK__c`.Information_Provider__c = `Account2`.Id 
  INNER JOIN `SF42_IZSEvent__c` ON ((`SF42_IZSEvent__c`.`SF42_informationProvider__c` = `Dezentrale_Anfrage_KK__c`.Information_Provider__c) AND (`Account`.SF42_Comany_Id__c = `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c`))
  INNER JOIN `Anfragestelle__c` ON `Anfragestelle__c`.Id = `Dezentrale_Anfrage_KK__c`.Anfragestelle__c 
  WHERE `Beitragsmonat__c` = "' .$arrPeriod[0] .'" AND `SF42_EventStatus__c` != "abgelehnt / Dublette" AND `SF42_EventStatus__c` != "zurückgestellt von IZS" 
  AND `Beitragsjahr__c` = "' .$arrPeriod[1] .'" AND `Anfragestelle__c`.`Abweichender_Anfrageprozess__c` != "Anfrage nach BNR" 
  AND `Account2`.`Dezentrale_Anfrage__c` != "false" '; // ((`SF42_EventStatus__c` = "enquired") OR (`SF42_EventStatus__c` = "to enquire")) AND
  $arrAnfrageListe = MySQLStatic::Query($strSqlNeu);
  
  if (is_array($arrAnfrageListe) && (count($arrAnfrageListe) > 0)) {
    foreach ($arrAnfrageListe as $arrAList) {
      
      if ($arrIpAmpel[$arrAList['Id']] == '') {
        $arrIpAmpel[$arrAList['Id']] = 'gruen';
      }

      if (($arrIpAmpel[$arrAList['Id']] == 'gruen') && (($arrAList['SF42_EventStatus__c'] == 'enquired') && (($arrAList['Rueckmeldung_am__c'] == '0000-00-00') || ($arrAList['Rueckmeldung_am__c'] == '')))) {
        $arrIpAmpel[$arrAList['Id']] = 'gelb';
      } 


      if ($arrAngefragtStatus[$arrAList['Id']] == '') $arrAngefragtStatus[$arrAList['Id']] = 0; 
      if ($arrKlaerungStatus[$arrAList['Id']] == '') $arrKlaerungStatus[$arrAList['Id']] = 0; 

      if ($arrAList['SF42_EventStatus__c'] == 'enquired') {
        $arrAngefragtStatus[$arrAList['Id']]++;
        if ($arrAList['Status_Klaerung__c'] == 'in Klärung') {
          $arrKlaerungStatus[$arrAList['Id']]++;
        }
      }

      if ((($arrAList['SF42_EventStatus__c'] == 'enquired') && (($arrAList['Rueckmeldung_am__c'] == '0000-00-00') || ($arrAList['Rueckmeldung_am__c'] == '')))) {
        if (!isset($arrEventStatus[$arrAList['Id']][0])) {
          $arrEventStatus[$arrAList['Id']][0] = 0;
        }
        $arrEventStatus[$arrAList['Id']][0]++;
      } else {
        if (!isset($arrEventStatus[$arrAList['Id']][1])) {
          $arrEventStatus[$arrAList['Id']][1] = 0;
        }
        $arrEventStatus[$arrAList['Id']][1]++;
      }
      
      if (($arrAList['SF42_EventStatus__c'] == 'enquired') || ($arrAList['SF42_EventStatus__c'] == 'to enquire')) {
        $arrInformationProvider[$arrAList['Id']] = $arrAList['Name'];
        $arrRequestWay[$arrAList['Id']] = $arrAList['Anfrageweg__c']; 
        $arrIpId[$arrAList['Id']] = $arrAList['SF42_informationProvider__c'];
      }
              
    }
  }
  
  if ($_SERVER['REMOTE_ADDR'] == '46.244.179.250') {
    //print_r($arrInformationProvider);
  }
  
  //print_r($arrKlaerungStatus); //die();
  
  
  if (count($arrInformationProvider) > 0) {
    natcasesort($arrInformationProvider);
    
    $strOutputTable.= '<table class="" id="5">' .chr(10);
    $strOutputTable.= '  <thead>' .chr(10);
    $strOutputTable.= '    <tr>' .chr(10);
  	$strOutputTable.= '      <th class="">Information Provider</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Rücklauf</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Anschreiben</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Vollmacht</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">erzeugt am</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Anfrageliste</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">erzeugt am</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">xlsx</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Anfrageweg</th>' .chr(10); //
  	$strOutputTable.= '      <th class="">versandt am</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">nur Klärungsfälle</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Anlieferung bis</th>' .chr(10); //class="header"
    $strOutputTable.= '    </tr>' .chr(10);
    $strOutputTable.= '  </thead>' .chr(10);
    $strOutputTable.= '  <tbody>' .chr(10);
    
    $arrVollmachtLink = array();
    $arrAnfrageLink   = array();
    
      
    $intCountEntries = 0;

    $arrBackList = [];
    $strSqlB = 'SELECT `cb_id`, `cb_date`, `cb_account_id` FROM `cms_back` WHERE `cb_month` = "' .$arrPeriod[0] .'" AND `cb_year` = "' .$arrPeriod[1] .'"';
    $arrBack = MySQLStatic::Query($strSqlB);
    if (count($arrBack) > 0) {
      foreach ($arrBack as $intKey => $arrBackRaw) {
        $arrBackList[$arrBackRaw['cb_account_id']] = $arrBackRaw;
      }
    }

    $arrOrgList = [];
    $strSql7 = 'SELECT `Id`, `Nur_neue_Vollmachten__c`, `Originalvollmacht_ben_tigt__c` FROM `Account`'; // WHERE `Id` ="' .$strId .'"
    $arrOrgReq  = MySQLStatic::Query($strSql7);
    if (count($arrOrgReq) > 0) {
      foreach ($arrOrgReq as $intKey => $arrAccountRaw) {
        $strKey = $arrAccountRaw['Id'];
        unset($arrAccountRaw['Id']);
        $arrOrgList[$strKey] = [0 => $arrAccountRaw];
      }
    }

    $arrCertList = [];
    $strSql = 'SELECT `ca_id`, DATE_FORMAT(`ca_created`, "%d.%m.%Y %H:%i:%s") AS `ca_created`, `ca_account_id` FROM `cms_auth` WHERE `ca_month` = "' .$arrPeriod[0] .'" AND `ca_year` = "' .$arrPeriod[1] .'"'; // `ca_account_id` = "' .$strId .'" AND 
    $arrCert = MySQLStatic::Query($strSql);
    if (count($arrCert) > 0) {
      foreach ($arrCert as $intKey => $arrCertRaw) {
        $strKey = $arrCertRaw['ca_account_id'];
        unset($arrCertRaw['ca_account_id']);
        $arrCertList[$strKey] = [0 => $arrCertRaw];
      }
    }

    $arrReqList = [];
    $strSql = 'SELECT `cr_id`, DATE_FORMAT(`cr_created`, "%d.%m.%Y %H:%i:%s") AS `cr_created`, `cr_account_id` FROM `cms_req` WHERE `cr_month` = "' .$arrPeriod[0] .'" AND `cr_year` = "' .$arrPeriod[1] .'"'; // `cr_account_id` = "' .$strId .'" AND 
    $arrReq = MySQLStatic::Query($strSql);
    if (count($arrReq) > 0) {
      foreach ($arrReq as $intKey => $arrReqRaw) {
        $strKey = $arrReqRaw['cr_account_id'];
        unset($arrReqRaw['cr_account_id']);
        $arrReqList[$strKey] = [0 => $arrReqRaw];
      }
    }

    $arrSentList = [];
    $strSql = 'SELECT `cs_id`, DATE_FORMAT(`cs_sent`, "%d.%m.%Y %H:%i:%s") AS `cs_sent`, `cs_account_id` FROM `cms_sent` WHERE `cs_month` = ' .$arrPeriod[0] .' AND `cs_year` = ' .$arrPeriod[1] .''; // `cs_account_id` = "' .$strId .'" AND 
    $arrSent = MySQLStatic::Query($strSql);
    if (count($arrSent) > 0) {
      foreach ($arrSent as $intKey => $arrSentRaw) {
        $strKey = $arrSentRaw['cs_account_id'];
        unset($arrSentRaw['cs_account_id']);
        $arrSentList[$strKey] = [0 => $arrSentRaw];
      }
    }

    foreach ($arrInformationProvider as $strId => $strName) {
      
      //echo '  ' .$strId .'<br />' .chr(10);
      
      if ((isset($_REQUEST['strSelWay']) && ($_REQUEST['strSelWay'] == $arrRequestWay[$strId])) || ($_REQUEST['strSelWay'] == 'all')) {

        if ((isset($_REQUEST['strSelRuec']) && ($_REQUEST['strSelRuec'] == $arrIpAmpel[$strId])) || ($_REQUEST['strSelRuec'] == '')) {

          $arrIp = $objAnfragestelle->arrGetIpFromAnfragestelle($strId);
          

          $arrBack['cb_date'] = '';
          //$strSqlB = 'SELECT `cb_id`, `cb_date` FROM `cms_back` WHERE `cb_month` = "' .$arrPeriod[0] .'" AND `cb_year` = "' .$arrPeriod[1] .'" AND `cb_account_id` ="' .$strId .'"';
          //$arrBack = MySQLStatic::Query($strSqlB);

          $arrBack = isset($arrBackList[$strId]) ? [0 => $arrBackList[$strId]] : [];
          
          if ((count($arrBack) > 0) && (@$arrBack[0]['cb_date'] != '0000-00-00')) {
          
            $arrDate = explode('-', $arrBack[0]['cb_date']);
            $arrBack['cb_date'] = $arrDate[2] .'.' .$arrDate[1] .'.' .$arrDate[0];
            $arrBack['cb_id'] = $arrBack[0]['cb_id'];
            
            $intDiff = ((mktime(0, 0, 0, date('m'), date('d'), date('Y')) - mktime(0, 0, 0, $arrDate[1], $arrDate[2], $arrDate[0])) / 86400);
            
            if ($intDiff >= 0) {
              $strClassBack = ' red';
            } else {
              $strClassBack = '';
            }
          
          } else {
            $arrBack['cb_id'] = $arrPeriod[1] .'-' .$arrPeriod[0] .'-' .$strId .'-';
          }

          
          $strOutputTable.= '    <tr>' .chr(10);
          $strOutputTable.= '      <td><span class="hidden">' .$strName .'</span><a href="index_neu.php?ac=contacts&acid=' .$arrIp[0]['acid'] .'" title="Zeige Kontakte" target="_blank">' .$strName .'</a></td>' .chr(10);
          
          //echo $strId;

          if (!isset($arrEventStatus[$strId][0])) {
            $arrEventStatus[$strId][0] = 0;
          }
          if (!isset($arrEventStatus[$strId][1])) {
            $arrEventStatus[$strId][1] = 0;
          }
          
          $strTitle = 'gesamt: ' .($arrEventStatus[$strId][1] + $arrEventStatus[$strId][0]);
          $strTitle.= ' | gruen: ' .$arrEventStatus[$strId][1];
          $strTitle.= ' | rot: ' .$arrEventStatus[$strId][0];
          
          if (($arrEventStatus[$strId][1] > 0) && ($arrEventStatus[$strId][0] == 0)) {
            $strAmpelStatus = 'gruen';
          } else if (($arrEventStatus[$strId][1] == 0) && ($arrEventStatus[$strId][0] > 0)) {
            $strAmpelStatus = 'rot';
          } else {
            $strAmpelStatus = 'gelb';
          }
          
          $strOutputTable.= '      <td><span class="hidden">' .$strAmpelStatus .'</span><img src="/assets/images/sys/ampel_' .$strAmpelStatus .'.png" title="' .$strTitle .'"></td>' .chr(10);
          //$strOutputTable.= '      <td><img src="/assets/images/sys/ampel_' .$arrIpAmpel[$strId] .'.png"></td>' .chr(10);

          if ($arrRequestWay[$strId] == 'E-Mail') {
            $strOutputTable.= '      <td style="text-align: center;"><img src="img/icon_email.png" class="ic_preview_m" id="d_' .$strId .'" rel="' .$intEvId .'" /></td>' .chr(10);
          } else {
            $strOutputTable.= '      <td style="text-align: center;"><img src="img/icon_pdf.png" class="ic_preview" id="p_' .$strId .'" /></td>' .chr(10);
          }

          /*
          $strVollmachtLink = '';
          $arrVollmachtLink[] = $strVollmachtLink;
          
          $strOutputTable.= '      <td><a href="' .$strVollmachtLink .'">erzeugen</a></td>' .chr(10);
          */
          


          //$strSql7 = 'SELECT `Nur_neue_Vollmachten__c`, `Originalvollmacht_ben_tigt__c` FROM `Account` WHERE `Id` ="' .$arrIp[0]['acid'] .'"';
          //$arrOrg  = MySQLStatic::Query($strSql7);

          $arrOrg = $arrOrgList[$arrIp[0]['acid']] ?? [];

          $strVollmachtLink = 'createVollmacht.neu.php?DezId=' .$strId .'&Name=' .rawurlencode($strName) .'&strSelDate=' .$_REQUEST['strSelDate'] .'&strSelText=' .$_REQUEST['strSelText'] .'&strSelWay=' .$_REQUEST['strSelWay'] .'&strSelRuec=' .$_REQUEST['strSelRuec'] .'&show=C';
          $strVollmachtLinkNeu = 'createVollmacht.neu.php?DezId=' .$strId .'&Name=' .rawurlencode($strName) .'&strSelDate=' .$_REQUEST['strSelDate'] .'&strSelText=' .$_REQUEST['strSelText'] .'&strSelWay=' .$_REQUEST['strSelWay'] .'&strSelRuec=' .$_REQUEST['strSelRuec'] .'&show=C&new=1';

          $arrVollmachtLink[] = $strVollmachtLink;
          
          $arrDiff = null;
          
          if ($arrOrg[0]['Nur_neue_Vollmachten__c'] == 'true1') {
          	$arrDiff = $objIP->arrGetNewVollmachtList($arrIp[0]['acid'], $arrPeriod[0], $arrPeriod[1]);
          	
          	$arrAnfragestelleList = array();
          	if (count($arrDiff) > 0) {
          	  foreach ($arrDiff as $intKey => $strBtnr) {
          	    $arrPp = $objPp->arrGetPPFromBetriebsnummer($strBtnr);
          	    $arrEvent = $objEvent->arrGetEventListFromIpPp($arrIp[0]['acid'], $arrPp[0]['acid']);
          	    $arrAnfragestelleList[] = $objAnfragestelle->arrGetAnfragestelleFromEvent($arrEvent[0]['evid']);
          	  }
          	}  
          	
          	//print_r($arrAnfragestelleList); die();
            
            $strOutputTable.= '      <td><a href="' .$strVollmachtLink .'">alle</a> / ';
            if (count($arrDiff) > 0) { 
              $strOutputTable.= '<a href="' .$strVollmachtLinkNeu .'">neue (' .count($arrDiff) .')</a>';
              $arrVollmachtLink[] = $strVollmachtLinkNeu;
            } else {
              $strOutputTable.= 'neue (0)';
            }
            $strOutputTable.= '</td>' .chr(10);
            
          } else {
            $strOutputTable.= '      <td><a href="' .$strVollmachtLink .'">erzeugen</a></td>' .chr(10);
          }


          //$strSql = 'SELECT `ca_id`, DATE_FORMAT(`ca_created`, "%d.%m.%Y %H:%i:%s") AS `ca_created` FROM `cms_auth` WHERE `ca_account_id` = "' .$strId .'" AND `ca_month` = "' .$arrPeriod[0] .'" AND `ca_year` = "' .$arrPeriod[1] .'"';
          //$arrCert = MySQLStatic::Query($strSql);

          $arrCert = $arrCertList[$strId] ?? [];
          
          if (count($arrCert) > 0) {
            $arrDate = explode(' ', $arrCert[0]['ca_created']);
            $strFileName = 'pdf/vollmacht/' .$strId .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_vollmacht_sv.pdf?' .substr(md5(uniqid(mt_rand(), true)), 0, 4);
            $strOutputTable.= '      <td><a href="' .$strFileName .'" target="_blank" alt="' .$arrCert[0]['ca_created'] .'" title="' .$arrCert[0]['ca_created'] .'" id="v_' .$strId .'">' .$arrDate[0] .'</a></td>' .chr(10);
          } else {
            $strOutputTable.= '      <td></td>' .chr(10);
          }
  
  
          $strAnfrageLink = 'createAnfrageliste.php?DezId=' .$strId .'&Name=' .rawurlencode(cleanForPdfOutput($strName)) .'&strSelDate=&strSelDate=' .$_REQUEST['strSelDate'] .'&strSelText=' .$_REQUEST['strSelText'] .'&strSelWay=' .$_REQUEST['strSelWay'] .'&strSelRuec=' .$_REQUEST['strSelRuec'] .'&show=D';
          $arrAnfrageLink[] = $strAnfrageLink;
  
          $strOutputTable.= '      <td><a href="' .$strAnfrageLink .'">erzeugen</a></td>' .chr(10);
  
          //$strSql = 'SELECT `cr_id`, DATE_FORMAT(`cr_created`, "%d.%m.%Y %H:%i:%s") AS `cr_created` FROM `cms_req` WHERE `cr_account_id` = "' .$strId .'" AND `cr_month` = "' .$arrPeriod[0] .'" AND `cr_year` = "' .$arrPeriod[1] .'"';
          //$arrReq = MySQLStatic::Query($strSql);

          $arrReq = $arrReqList[$strId] ?? [];
          
          if (count($arrReq) > 0) {
            $arrDate = explode(' ', $arrReq[0]['cr_created']);
            $strFileName = 'pdf/anfrage/' .$strId .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_anfrage_sv.pdf';
            $strOutputTable.= '      <td><a href="' .$strFileName .'?' .substr(md5(uniqid(mt_rand(), true)), 0, 4) .'" target="_blank" alt="' .$arrReq[0]['cr_created'] .'" title="' .$arrReq[0]['cr_created'] .'" id="a_' .$strId .'">' .$arrDate[0] .'</a></td>' .chr(10);
            
            $strFileNameXlsx = str_replace('.pdf', '.xlsx', $strFileName);
            if (file_exists($strFileNameXlsx)) {
              $strOutputTable.= '      <td><a href="' .$strFileNameXlsx .'?' .substr(md5(uniqid(mt_rand(), true)), 0, 4) .'" target="_blank" alt="' .$arrReq[0]['cr_created'] .'" title="' .$arrReq[0]['cr_created'] .'" id="xl_' .$strId .'"><img src="img/icon_xls.png" id="xi_' .$strId .'" /></a></td>' .chr(10);
            } else {
              $strOutputTable.= '      <td></td>' .chr(10);
            }
          } else {
            $strOutputTable.= '      <td></td>' .chr(10);
            $strOutputTable.= '      <td></td>' .chr(10);
          }

  
          if ($arrRequestWay[$strId] == 'E-Mail') {
            $strOutputTable.= '      <td><span id="e_' .$strId .'" class="send">' .$arrRequestWay[$strId] .'</span></td>' .chr(10);
            
            //$strSql = 'SELECT `cs_id`, DATE_FORMAT(`cs_sent`, "%d.%m.%Y %H:%i:%s") AS `cs_sent` FROM `cms_sent` WHERE `cs_account_id` = "' .$strId .'" AND `cs_month` = ' .$arrPeriod[0] .' AND `cs_year` = ' .$arrPeriod[1] .'';
            //$arrSent = MySQLStatic::Query($strSql);

            $arrSent = $arrSentList[$strId] ?? [];
            
            if (count($arrSent) > 0) {
              $arrDate = explode(' ', $arrSent[0]['cs_sent']);
              $strOutputTable.= '      <td><span class="hidden">' .$arrDate[0] .'</span><a id="sd_' .$strId .'" alt="' .$arrSent[0]['cs_sent'] .'" title="' .$arrSent[0]['cs_sent'] .'">' .$arrDate[0] .'</a></td>' .chr(10);
            } else {
              $strOutputTable.= '      <td></td>' .chr(10);
            }
  
          } elseif ($arrRequestWay[$strId] == 'Fax') {
            $strOutputTable.= '      <td><span id="f_' .$strId .'" class="send">' .$arrRequestWay[$strId] .'</span></td>' .chr(10);
            
            //$strSql = 'SELECT `cs_id`, DATE_FORMAT(`cs_sent`, "%d.%m.%Y %H:%i:%s") AS `cs_sent` FROM `cms_sent` WHERE `cs_account_id` = "' .$strId .'" AND `cs_month` = ' .$arrPeriod[0] .' AND `cs_year` = ' .$arrPeriod[1] .'';
            //$arrSent = MySQLStatic::Query($strSql);

            $arrSent = $arrSentList[$strId] ?? [];
            
            if (count($arrSent) > 0) {
              $arrDate = explode(' ', $arrSent[0]['cs_sent']);
              $strOutputTable.= '      <td><span class="hidden">' .$arrDate[0] .'</span><a id="sd_' .$strId .'" alt="' .$arrSent[0]['cs_sent'] .'" title="' .$arrSent[0]['cs_sent'] .'">' .$arrDate[0] .'</a></td>' .chr(10);
            } else {
              $strOutputTable.= '      <td></td>' .chr(10);
            }
  
          } elseif ($arrRequestWay[$strId] == 'Post') {
            $strOutputTable.= '      <td><span id="b_' .$strId .'" class="send">' .$arrRequestWay[$strId] .'</span></td>' .chr(10);
            
            //$strSql = 'SELECT `cs_id`, DATE_FORMAT(`cs_sent`, "%d.%m.%Y %H:%i:%s") AS `cs_sent` FROM `cms_sent` WHERE `cs_account_id` = "' .$strId .'" AND `cs_month` = ' .$arrPeriod[0] .' AND `cs_year` = ' .$arrPeriod[1] .'';
            //$arrSent = MySQLStatic::Query($strSql);

            $arrSent = $arrSentList[$strId] ?? [];
            
            if (count($arrSent) > 0) {
              $arrDate = explode(' ', $arrSent[0]['cs_sent']);
              $strOutputTable.= '      <td><span class="hidden">' .$arrDate[0] .'</span><a id="sd_' .$strId .'" alt="' .$arrSent[0]['cs_sent'] .'" title="' .$arrSent[0]['cs_sent'] .'">' .$arrDate[0] .'</a></td>' .chr(10);
            } else {
              $strOutputTable.= '      <td></td>' .chr(10);
            }
  
          } else {
            $strOutputTable.= '      <td><span id="b_' .$strId .'" class="send">' .$arrRequestWay[$strId] .'</span></td>' .chr(10);
            $strOutputTable.= '      <td></td>' .chr(10);
          }
          
          $strNurKlaerung = 'nein';
          if (($arrAngefragtStatus[$strId] == $arrKlaerungStatus[$strId]) && ($arrAngefragtStatus[$strId] != 0)) {
            $strNurKlaerung = 'ja (' .$arrKlaerungStatus[$strId] .')';
          }

          $strOutputTable.= '      <td>' .$strNurKlaerung .'<!--  (' .$arrAngefragtStatus[$strId] .'/' .$arrKlaerungStatus[$strId] .') --></td>' .chr(10);

          $strOutputTable.= '      <td><span id="h_date_' .$arrBack['cb_id']  .'" class="hidden">' .$arrBack['cb_date'] .'</span><input type="text" id="date_' .$arrBack['cb_id']  .'" class="date update_field' .$strClassBack .' selD" name="cb_date" value="' .$arrBack['cb_date'] .'" autocomplete="off"></td>' .chr(10);
          
          $strOutputTable.= '    </tr>' .chr(10);
          
          $intCountEntries++;
        
        }
        
      }
      
    }
    
    if ($_SERVER['REMOTE_ADDR'] == '62.245.254.82') {
      //print_r($arrAngefragtStatus);
      //print_r($arrKlaerungStatus);
    }
    
    $strOutputTable.= '  </tbody>' .chr(10);
    $strOutputTable.= '</table>' .chr(10);
    
    $strOutput.= '<div class="clearfix" style="height: 48px; width: 1444px;">' .chr(10);
    $strOutput.= '  <div class="form-left">' .chr(10);
    $strOutput.= '    <h3>' .$intCountEntries .' Ergebnisse gefunden:</h3>' .chr(10);
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' .chr(10);

    if ($_REQUEST['startType'] == 'auth') {
      $strOutput.= '<span class="buttons">Dokumente werden erzeugt...</span>' .chr(10);
    } else {
      $strOutput.= '<form action="" method="post" class="buttons" style="display: inline;">' .chr(10);
      $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);
      $strOutput.= '<input type="hidden" name="startType" value="auth">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelDate" value="' .$_REQUEST['strSelDate'] .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelWay" value="' .$_REQUEST['strSelWay'] .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelText" value="' .$_REQUEST['strSelText'] .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="startCron" value="' .$strHashVollmacht .'">' .chr(10);
      $strOutput.= '<button id="doc-create" class="ui-button ui-state-default ui-corner-all">Dokumente erzeugen</button>' .chr(10);
      $strOutput.= '</form> ' .chr(10);
    }

    $strOutput.= '    <form method="post" action="_get_csv.php" style="display: inline; float: right; padding-left: 10px;">';
    $strOutput.= '    <input type="hidden" name="strTable" value="' .str_replace('"', "'", $strOutputTable) .'">' .chr(10);
    $strOutput.= '    <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' .chr(10);
    $strOutput.= '    </form>';
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '</div>' .chr(10);


    $strOutput.= $strOutputTable;
   
    
    $strOutput.= "
<script>
$('#5').fixheadertable({ 
   colratio    : [270, 80, 100, 110, 110, 110, 110, 50, 110, 110, 120, 110], 
   height      : 500, 
   width       : 1420, 
   zebra       : true, 
   sortable    : true,
   sortType    : ['string', 'string', 'string', 'string', 'date', 'string', 'date', 'string', 'string', 'date', 'string', 'date'],
   sortedColId : 0, 
   dateFormat  : 'd.m.Y'
});
</script>
";
          
  } else {
    
    $strOutput.= '<p>Keine Events im gewählten Zeitraum vorhanden.</p>' .chr(10);
    
  }

}


$strOutput.= '

<style>
td {
  padding-bottom: 5px;
}

#dialog_prev textarea {
  margin-bottom: 0px;
}
</style>

<div id="dialog_prev" title="Vorschau">
</div>

<script type="text/javascript">
	
$(document).ready(function() {

	$("#dialog_prev").dialog({
		bgiframe: true,
		autoOpen: false,
		resizable: false,
		height: 500,
		width: 590,
		modal: true,
		buttons: {
			\'OK\': function() {

		    $(this).dialog(\'close\');

			}
		},
		close: function() {

		}
	});
	
});	

</script>

  ';     

  
$strSql = 'DELETE FROM `_cron_url` WHERE `cu_status` = 0 AND `cu_time` < DATE_ADD(CURDATE(), INTERVAL -1 DAY)';
$arrRow = MySQLStatic::Query($strSql);

if (is_array($arrVollmachtLink) && (count($arrVollmachtLink) > 0)) {
  
  foreach ($arrVollmachtLink as $intId => $strLink) {
    
    $strSql = 'INSERT INTO `_cron_url` (`cu_id`, `cu_hash`, `cu_url`, `cu_time`, `cu_status`) VALUES (NULL, "' .$strHashVollmacht .'", "' .$strLink .'", NOW(), 0)';
    $intCt_id = MySQLStatic::Insert($strSql);
    
    $strSql = 'INSERT INTO `_cron_url` (`cu_id`, `cu_hash`, `cu_url`, `cu_time`, `cu_status`) VALUES (NULL, "' .$strHashVollmacht .'", "' .$arrAnfrageLink[$intId] .'", NOW(), 0)';
    $intCt_id = MySQLStatic::Insert($strSql);

  }
  
}

$end = microtime(true);
$durationInMs = ($end-$start) * 1000;
echo '<!-- Abchnitt 02: ' .round($durationInMs) .'ms -->' .chr(10);
$start = microtime(true);

/*

print_r($arrVollmachtLink);
print_r($arrAnfrageLink);

*/

?>