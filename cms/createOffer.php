<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

session_start();

function str_split_unicode($str, $l = 0) {
  if ($l > 0) {
      $ret = array();
      $len = mb_strlen($str, "UTF-8");
      for ($i = 0; $i < $len; $i += $l) {
          $ret[] = mb_substr($str, $i, $l, "UTF-8");
      }
      return $ret;
  }
  return preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
}

function clean_for_word ($strInput = '') {
  
  $strReturn = '';
  
  if ($strInput != '') {
    $arrCharInput = str_split_unicode($strInput);
    foreach ($arrCharInput as $intKey => $strChar) {
      if (ord($strChar) != 194) $strReturn.= $strChar;
    }
  }

  $strReturn = htmlspecialchars($strReturn);

  return $strReturn;

}

//print_r($_REQUEST);

/*
require_once('PhpWord/PhpWord.php');
require_once('PhpWord/TemplateProcessor.php');
require_once('PhpWord/Settings.php');
require_once('PhpWord/Shared/ZipArchive.php');
require_once('PhpWord/Shared/Text.php');
*/

require_once('vendor/phpoffice/phpword/bootstrap.php');

require_once('../assets/classes/class.mysql.php');

$strPath = 'vorlagen/';
$strAltFileName = '';

if (($_REQUEST['Id'] != '') && ($_REQUEST['temp'] != '')) {

  if (strstr('/', $_REQUEST['temp']) != false) {
    die('Pfui.');
  } else {
    $strTempPath = $strPath .str_replace('|', '/', $_REQUEST['temp']);
  }

  if (file_exists($strTempPath)) {
    
    $arrName = explode('|', $_REQUEST['temp']);
    $strNewName = substr(md5(uniqid(mt_rand(), true)), 0, 8);
    
    $arrNewName = explode('.', $arrName[1]);
    
    //echo $arrNewName[0] .'_' .date('dmY_His') .'.' .$arrNewName[1]; die();
    
    //$templateProcessor = $PHPWord->loadTemplate($strTempPath);

    if ($_REQUEST['ac'] == 'offe') {

      $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($strTempPath);

      $strSql = 'SELECT * FROM `Opportunity` WHERE `Id` = "' .$_REQUEST['Id'] .'"';
      $arrOpportunity = MySQLStatic::Query($strSql);

      $strEntleiherName = '';
      if ($arrOpportunity[0]['Entleiher__c'] != '') {
        $strSqlE = 'SELECT `Name` FROM `Account` WHERE `Id` = "' .$arrOpportunity[0]['Entleiher__c'] .'"';
        $arrSqlE = MySQLStatic::Query($strSqlE);
        if (count($arrSqlE) > 0) {
          $strEntleiherName = $arrSqlE[0]['Name'];
        }
      }
    
      //print_r($templateProcessor); die();

      $arrDate = explode('-', $arrOpportunity[0]['Angebot_befristet_bis__c']);
      $strDate = $arrDate[2] .'.' .$arrDate[1] .'.' .$arrDate[0];

      $templateProcessor->setValue('IZSdate', $strDate);
      
      $strSql = 'SELECT * FROM `Account` WHERE `Id` = "' .$arrOpportunity[0]['AccountId'] .'"';
      $arrSql = MySQLStatic::Query($strSql);

      $strSql = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$arrOpportunity[0]['Company_Group__c'] .'"';
      $arrGroup = MySQLStatic::Query($strSql);

      //print_r($arrSql); die();


      $templateProcessor->setValue('IZScompany', clean_for_word($arrSql[0]['Name']));
      $templateProcessor->setValue('IZSstreet', $arrSql[0]['BillingStreet']);
      $templateProcessor->setValue('IZSzip', $arrSql[0]['BillingPostalCode']);
      $templateProcessor->setValue('IZScity', $arrSql[0]['BillingCity']);

      $templateProcessor->setValue('IZSsv_3monate', number_format((float) $arrOpportunity[0]['Beitragsnachweise_3_Monate__c'], 0, '', '.'));
      
      $templateProcessor->setValue('flex1b', number_format((float) $arrOpportunity[0]['Einrichtung_12M__c'], 2, ',', '.') .' €');
      $templateProcessor->setValue('flex1c', number_format((float) $arrOpportunity[0]['Einrichtung_24M__c'], 2, ',', '.') .' €');

      $templateProcessor->setValue('flex2b', number_format((float) $arrOpportunity[0]['Weitere_Meldestelle_12M__c'], 2, ',', '.') .' €'); //
      $templateProcessor->setValue('flex2c', number_format((float) $arrOpportunity[0]['Weitere_Meldestelle_24M__c'], 2, ',', '.') .' €'); //
      
      $templateProcessor->setValue('IZSmeldestellen', $arrOpportunity[0]['Anzahl_Meldestellen__c']);

      $templateProcessor->setValue('flex3b', number_format((float) $arrOpportunity[0]['DV_Pauschale_12M__c'], 2, ',', '.') .' €');
      $templateProcessor->setValue('flex3c', number_format((float) $arrOpportunity[0]['DV_Pauschale_24M__c'], 2, ',', '.') .' €');

      $templateProcessor->setValue('proDS12', number_format((float) $arrOpportunity[0]['Einzelpreis_Datensatz_12M__c'], 2, ',', '.') .' €');
      $templateProcessor->setValue('proDS24', number_format((float) $arrOpportunity[0]['Einzelpreis_Datensatz_24M__c'], 2, ',', '.') .' €');

      //Mit Rabatt
      $arrDateRabatt = explode('-', $arrOpportunity[0]['Rabatt_gueltig_bis__c']);
      $arrDateRabatt = $arrDateRabatt[2] .'.' .$arrDateRabatt[1] .'.' .$arrDateRabatt[0];
      $templateProcessor->setValue('rebate_duedate', $arrDateRabatt);

      $templateProcessor->setValue('rab12', number_format((float) $arrOpportunity[0]['Rabatt__c'], 0, ',', '.') .' %');
      $templateProcessor->setValue('prsRab12', number_format((float) $arrOpportunity[0]['DV_Pauschale_12M_mit_Rabatt__c'], 2, ',', '.') .' €');
      $templateProcessor->setValue('rab24', number_format((float) $arrOpportunity[0]['Rabatt_24M__c'], 0, ',', '.') .' %');
      $templateProcessor->setValue('prsRab24', number_format((float) $arrOpportunity[0]['DV_Pauschale_24M_mit_Rabatt__c'], 2, ',', '.') .' €');

      //Sonderkonditionen
      $templateProcessor->setValue('flex1d', number_format((float) ($arrOpportunity[0]['Einrichtung_24M__c'] - ($arrOpportunity[0]['Rabatt_24M__c'] * $arrOpportunity[0]['Einrichtung_24M__c'] / 100)), 2, ',', '.') .' €');

      //Neue Angebote
      $templateProcessor->setValue('angebotsnummer', clean_for_word($arrOpportunity[0]['Name']));
      $templateProcessor->setValue('Erstellungsdatum', clean_for_word(date('d.m.Y')));
      $templateProcessor->setValue('gueltig_bis', clean_for_word($strDate));

      $templateProcessor->setValue('unternehmensgruppe_name', clean_for_word($arrGroup[0]['Name']));
      $templateProcessor->setValue('ugruppe_strasse', clean_for_word($arrGroup[0]['Strasse__c']));
      $templateProcessor->setValue('ugruppe_plz', clean_for_word($arrGroup[0]['PLZ__c']));
      $templateProcessor->setValue('ugruppe_ort', clean_for_word($arrGroup[0]['Ort__c']));

      $templateProcessor->setValue('angebotsanrede', clean_for_word($arrOpportunity[0]['f42_Angebotsanrede__c']));

      $templateProcessor->setValue('name_var1', $arrOpportunity[0]['offer2var_v1_name']);
      $templateProcessor->setValue('name_var2', $arrOpportunity[0]['offer2var_v2_name']);

      $templateProcessor->setValue('anzahl_svmeldestellen', clean_for_word(number_format((float) $arrOpportunity[0]['Anzahl_Meldestellen__c'], 0, ',', '.')));
      $templateProcessor->setValue('anzahl_svdatensaetze_quartal', clean_for_word(number_format((float) $arrOpportunity[0]['Beitragsnachweise_3_Monate__c'], 0, ',', '.')));
      $templateProcessor->setValue('anzahl_klaerungsfaelle_quartal', clean_for_word(number_format((float) $arrOpportunity[0]['offer2var_sv-clarification_total-quartly'], 0, ',', '.')));

      $templateProcessor->setValue('einrichtung_v1', clean_for_word(number_format((float) $arrOpportunity[0]['offer2var_v1_installation-fee'], 2, ',', '.')));
      $templateProcessor->setValue('einrichtung_v2', clean_for_word(number_format((float) $arrOpportunity[0]['offer2var_v2_installation-fee'], 2, ',', '.')));
      
      $templateProcessor->setValue('dvpauschale_v1', clean_for_word(number_format((float) $arrOpportunity[0]['offer2var_v1_dataprocessing_fee-monthly'], 2, ',', '.')));
      $templateProcessor->setValue('dvpauschale_v2', clean_for_word(number_format((float) $arrOpportunity[0]['offer2var_v2_dataprocessing_fee-monthly'], 2, ',', '.')));

      $templateProcessor->setValue('dvpauschale_rabatt_v2', clean_for_word($arrOpportunity[0]['offer2var_v2_dataprocessing_discount']));
      $templateProcessor->setValue('ersparnis_v2vsv1', clean_for_word(number_format((float) $arrOpportunity[0]['offer2var_v2_dataprocessing_savings-vs12'], 2, ',', '.')));
      
      $templateProcessor->setValue('preis_add_meldestelle_v1', clean_for_word(number_format((float) $arrOpportunity[0]['offer2var_v1_installationfee_add-reportingoffice_price'], 2, ',', '.')));
      $templateProcessor->setValue('preis_add_svdatensatz_v1', clean_for_word(number_format((float) $arrOpportunity[0]['offer2var_v1_sv-records_add-record_price'], 2, ',', '.')));
      $templateProcessor->setValue('preis_add_klaerungsfall_v1', clean_for_word(number_format((float) $arrOpportunity[0]['offer2var_v1_sv-clarification_add_price'], 2, ',', '.')));
      $templateProcessor->setValue('preis_add_meldestelle_v2', clean_for_word(number_format((float) $arrOpportunity[0]['offer2var_v2_installationfee_add-reportingoffice_price'], 2, ',', '.')));
      $templateProcessor->setValue('preis_add_svdatensatz_v2', clean_for_word(number_format((float) $arrOpportunity[0]['offer2var_v2_sv-records_add-record_price'], 2, ',', '.')));
      $templateProcessor->setValue('preis_add_klaerungsfall_v2', clean_for_word(number_format((float) $arrOpportunity[0]['offer2var_v2_sv-clarification_add_price'], 2, ',', '.')));

      $templateProcessor->setValue('besondere_bedingungen', clean_for_word(strip_tags(str_replace('&nbsp;', ' ', $arrOpportunity[0]['Bedingungen__c']))));

      $arrDatePreis = explode('-', $arrOpportunity[0]['AnstehendePreiserhoehung__c']);
      $strDatePreis = $arrDatePreis[2] .'.' .$arrDatePreis[1] .'.' .$arrDatePreis[0];
      $templateProcessor->setValue('erhoehung_zum', $strDatePreis);

      /* ALT

      $templateProcessor->setValue('flex1a', number_format((float) $arrOpportunity[0]['Einrichtung_Flex__c'], 2, ',', '.') .' €');
      $templateProcessor->setValue('flex2a', number_format((float) $arrOpportunity[0]['Weitere_Meldestelle_Flex__c'], 2, ',', '.') .' €');
      $templateProcessor->setValue('flex3a', number_format((float) $arrOpportunity[0]['Datenverarbeitungspauschale_Flex__c'], 2, ',', '.') .' €');

      $templateProcessor->setValue('bg12', number_format((float) $arrOpportunity[0]['BG_12M__c'], 2, ',', '.') .' €');
      $templateProcessor->setValue('au12', number_format((float) $arrOpportunity[0]['AU_12M__c'], 2, ',', '.') .' €');
      $templateProcessor->setValue('dok12', number_format((float) $arrOpportunity[0]['Dok_12M__c'], 2, ',', '.') .' €');
      $templateProcessor->setValue('info12', number_format((float) $arrOpportunity[0]['Infoservice_12M__c'], 2, ',', '.') .' €');

      $templateProcessor->setValue('flex3b', number_format((float) $arrOpportunity[0]['Datenverarbeitungspauschale_12M__c'], 2, ',', '.') .' €');
      $templateProcessor->setValue('flex3c', number_format((float) $arrOpportunity[0]['Datenverarbeitungspauschale_24M__c'], 2, ',', '.') .' €');
      */

      $strSonder01 = '';
      $strSonder02 = '';

      if (($arrOpportunity[0]['Entleiher_Sonderkonditionen__c'] != '') && ($arrOpportunity[0]['Sonderkonditionen_anbieten__c'] == 'ja')) {

        $strSql = 'SELECT `Name` FROM `Account` WHERE `Id` = "' .$arrOpportunity[0]['Entleiher__c'] .'"';
        $arrSql = MySQLStatic::Query($strSql);

        $strSonder01 = 'Sonderkonditionen exklusiv für Personaldienstleister von ' .clean_for_word($arrSql[0]['Name']) .':';
        $strSonder02 = '10% Rabatt auf alle Angebotspositionen bei fristgerechter Annahme dieses Angebots';

        if (strstr($strTempPath, 'Gruppe') !== false) {
          $templateProcessor->setValue('Sonder', '(' .clean_for_word($arrSql[0]['Name']) .')');
        }

      }

      $templateProcessor->setValue('IZSsonder01', $strSonder01);
      $templateProcessor->setValue('IZSsonder02', $strSonder02);

      $templateProcessor->setValue('nameENT', clean_for_word($strEntleiherName));

      $templateProcessor->setValue('anzahl_svdatensaetze_monat', clean_for_word(number_format((float) $arrOpportunity[0]['Beitragsnachweise_pro_Monat__c'], 0, ',', '.')));
      $templateProcessor->setValue('anzahl_klaerungsfaelle_monat', clean_for_word(number_format((float) ceil($arrOpportunity[0]['offer2var_sv-clarification_total-quartly'] / 3), 0, ',', '.')));


    } elseif ($_REQUEST['ac'] == 'ofmi') {

      $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($strTempPath);

      $strSql = 'SELECT * FROM `Group__c` WHERE ((`Liefert_Daten__c` = "true") OR (`Id` = "a083000000UGC3LAAX")) AND `Id` = "' .$_REQUEST['Id'] .'"';
      $arrGroup = MySQLStatic::Query($strSql);

      $templateProcessor->setValue('Name', clean_for_word($arrGroup[0]['Name']));
      $templateProcessor->setValue('Strasse', $arrGroup[0]['Strasse__c']);
      $templateProcessor->setValue('PLZ', $arrGroup[0]['PLZ__c']);
      $templateProcessor->setValue('Ort', $arrGroup[0]['Ort__c']);
      $templateProcessor->setValue('Btnr', $arrGroup[0]['Betriebsnummer__c']);

      $templateProcessor->setValue('Beginn', date('d.m.Y', strtotime($arrGroup[0]['Registriert_bei_IZS_seit__c'])));

      $strSql = 'SELECT * FROM `Vertrag__c` WHERE `Company_Group__c` = "' .$_REQUEST['Id'] .'" AND `Aktiv__c` = "true"';
      $arrVertrag = MySQLStatic::Query($strSql);

      if (count($arrVertrag) == 0) {
        $arrVertrag[0]['Pruefintervall__c'] = 'monatlich';
        $arrVertrag[0]['Vertrag_g_ltig_f_r__c'] == 'Alle';
      }

      $templateProcessor->setValue('Intervall', $arrVertrag[0]['Pruefintervall__c']);

      if ($arrVertrag[0]['Vertrag_g_ltig_f_r__c'] == 'Entleiherportal') {

        $templateProcessor->setValue('Einsehen', 'Die aktuellen und historischen Prüfungsergebnisse können ausschließlich über einen passwortgeschützten Zugang zu einem Entleiherportal abgerufen werden.');
        $templateProcessor->setValue('Link', '');

      } else {

        $templateProcessor->setValue('Einsehen', 'Alle aktuellen und historischen Prüfungsergebnisse können jederzeit über folgenden Link online eingesehen und bei Bedarf heruntergeladen werden:');
        $templateProcessor->setValue('Link', 'https://www.izs.de/' .$arrGroup[0]['Direktlink_zum_Konto__c']);

      }

      $templateProcessor->setValue('Heute', date('d.m.Y'));

      $strAltFileName = 'Mitgliedsurkunde_IZS_' .str_replace(array(' ', '/', '%', '*', '\\'), '_', $arrGroup[0]['Name']);

      if (strstr($strTempPath, 'Mehrseitig') !== false) { //Mehrseitig

        $intRowsPerPage = 18;

        $strSql = 'SELECT * FROM `Account` WHERE `RecordTypeId` = "01230000001Ao71AAC" AND `SF42_Sub_Type__c` = "Premium Payer" ';
        $strSql.= 'AND ((`SF42_isPremiumPayer__c` = "true" AND `Aktiv__c` = "true" AND `SF42_use_data__c` = "true") OR (`SF42_Company_Group__c` = "a083000000UGC3LAAX")) ';
        $strSql.= 'AND `SF42_Company_Group__c` = "' .$_REQUEST['Id'] .'" AND `Name` LIKE "%' .$_REQUEST['add'] .'%"  ORDER BY `SF42_Comany_ID__c`';
        $arrMeldestList = MySQLStatic::Query($strSql);

        $intRowSum = count($arrMeldestList);

        $templateProcessor->cloneRow('NameN', $intRowSum);

        foreach ($arrMeldestList as $intKey => $arrMeldestelle) {
          $templateProcessor->setValue('NameN#' .($intKey + 1), clean_for_word($arrMeldestelle['Name']));
          $templateProcessor->setValue('BtnrN#' .($intKey + 1), clean_for_word($arrMeldestelle['SF42_Comany_ID__c']));
          $templateProcessor->setValue('StrasseN#' .($intKey + 1), clean_for_word($arrMeldestelle['BillingStreet']));
          $templateProcessor->setValue('PLZN#' .($intKey + 1), clean_for_word($arrMeldestelle['BillingPostalCode']));
          $templateProcessor->setValue('OrtN#' .($intKey + 1), clean_for_word($arrMeldestelle['BillingCity']));
        }


      }

    } elseif ($_REQUEST['ac'] == 'ofvo') {

      $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($strTempPath);

      $strSql = 'SELECT * FROM `Group__c` WHERE `Liefert_Daten__c` = "true" AND `Id` = "' .$_REQUEST['Id'] .'"';
      $arrGroup = MySQLStatic::Query($strSql);

      if (strstr($strTempPath, 'Einseitig') !== false) {

        $strSql = 'SELECT * FROM `Account` WHERE `RecordTypeId` = "01230000001Ao71AAC" AND `SF42_Sub_Type__c` = "Premium Payer" ';
        $strSql.= 'AND ((`SF42_isPremiumPayer__c` = "true") OR (`Ist_BG_Meldestelle__c` = "true")) '; //Meldestelle oder BG Meldestelle
        $strSql.= 'AND `SF42_use_data__c` = "true" '; //AND `Aktiv__c` = "true"
        $strSql.= 'AND `SF42_Company_Group__c` = "' .$_REQUEST['Id'] .'"  ORDER BY CONVERT(`SF42_Comany_ID__c` , UNSIGNED INTEGER)';
        $arrMeldestList = MySQLStatic::Query($strSql);

        $intRowCount = count($arrMeldestList);

        $templateProcessor->cloneRow('Name', $intRowCount);

        foreach ($arrMeldestList as $intKey => $arrMeldestelle) {
          $templateProcessor->setValue('Name#' .($intKey + 1), clean_for_word($arrMeldestelle['Name']));
          $templateProcessor->setValue('Btnr#' .($intKey + 1), clean_for_word($arrMeldestelle['SF42_Comany_ID__c']));
          $templateProcessor->setValue('Strasse#' .($intKey + 1), clean_for_word($arrMeldestelle['BillingStreet']));
          $templateProcessor->setValue('PLZ#' .($intKey + 1), clean_for_word($arrMeldestelle['BillingPostalCode']));
          $templateProcessor->setValue('Ort#' .($intKey + 1), clean_for_word($arrMeldestelle['BillingCity']));
        }

      } else { //Mehrzeilig

        $strSql = 'SELECT * FROM `Group__c` WHERE `Liefert_Daten__c` = "true" AND `Id` = "' .$_REQUEST['Id'] .'"';
        $arrGroup = MySQLStatic::Query($strSql);

        if ((strstr($arrGroup[0]['Name'], 'Alten') !== false) && ($_REQUEST['add'] != '')) {
          $arrGroup[0]['Name'] = str_replace('Alten GmbH', $_REQUEST['add'], $arrGroup[0]['Name']);
        }

        if ((strstr($arrGroup[0]['Name'], 'AlphaConsult KG') !== false) && ($_REQUEST['add'] != '')) {
          $arrGroup[0]['Name'] = str_replace('AlphaConsult KG', $_REQUEST['add'], $arrGroup[0]['Name']);
        }

        $intRowsPerPage = 18;

        if ($_REQUEST['add'] != '') {
          $strSql = 'SELECT * FROM `Account` WHERE `RecordTypeId` = "01230000001Ao71AAC" AND `SF42_Sub_Type__c` = "Premium Payer" ';
          $strSql.= 'AND ((`SF42_isPremiumPayer__c` = "true") OR (`Ist_BG_Meldestelle__c` = "true")) AND `SF42_use_data__c` = "true" '; //AND `Aktiv__c` = "true" 
          $strSql.= 'AND `SF42_Company_Group__c` = "' .$_REQUEST['Id'] .'" AND `Name` LIKE "%' .$_REQUEST['add'] .'%" ORDER BY CONVERT(`SF42_Comany_ID__c` , UNSIGNED INTEGER)';
        } else {
          $strSql = 'SELECT * FROM `Account` WHERE `RecordTypeId` = "01230000001Ao71AAC" AND `SF42_Sub_Type__c` = "Premium Payer" ';
          $strSql.= 'AND ((`SF42_isPremiumPayer__c` = "true") OR (`Ist_BG_Meldestelle__c` = "true")) AND `SF42_use_data__c` = "true" '; //AND `Aktiv__c` = "true"  
          $strSql.= 'AND `SF42_Company_Group__c` = "' .$_REQUEST['Id'] .'"  ORDER BY CONVERT(`SF42_Comany_ID__c` , UNSIGNED INTEGER)';
        }

        $arrMeldestList = MySQLStatic::Query($strSql);

        $intRowSum = count($arrMeldestList);

        $templateProcessor->cloneRow('Name', $intRowSum);

        foreach ($arrMeldestList as $intKey => $arrMeldestelle) {
          $templateProcessor->setValue('Name#' .($intKey + 1), clean_for_word($arrMeldestelle['Name']));
          $templateProcessor->setValue('Btnr#' .($intKey + 1), clean_for_word($arrMeldestelle['SF42_Comany_ID__c']));
          $templateProcessor->setValue('Strasse#' .($intKey + 1), clean_for_word($arrMeldestelle['BillingStreet']));
          $templateProcessor->setValue('PLZ#' .($intKey + 1), clean_for_word($arrMeldestelle['BillingPostalCode']));
          $templateProcessor->setValue('Ort#' .($intKey + 1), clean_for_word($arrMeldestelle['BillingCity']));
        }

        $templateProcessor->setValue('NameGruppe', clean_for_word($arrGroup[0]['Name']));

      }

    } elseif ($_REQUEST['ac'] == 'ofse') {
    
      $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($strTempPath);

      $strSql = 'SELECT * FROM `Group__c` WHERE `Liefert_Daten__c` = "true" AND `Id` = "' .$_REQUEST['Id'] .'"';
      $arrGroup = MySQLStatic::Query($strSql);

      $templateProcessor->setValue('Name', clean_for_word($arrGroup[0]['Name']));
      $templateProcessor->setValue('Strasse', $arrGroup[0]['Strasse__c']);
      $templateProcessor->setValue('PLZ', $arrGroup[0]['PLZ__c']);
      $templateProcessor->setValue('Ort', $arrGroup[0]['Ort__c']);

      $strSql = 'SELECT * FROM `Vertrag__c` WHERE `Company_Group__c` = "' .$_REQUEST['Id'] .'" AND `Aktiv__c` = "true"';
      $arrVertrag = MySQLStatic::Query($strSql);

      //print_r($arrVertrag[0]); die();

      $templateProcessor->setValue('Referenz', $arrVertrag[0]['Mandatsreferenz__c']);

      $templateProcessor->setValue('Kreditinstitut', clean_for_word($arrVertrag[0]['Kreditinstitut__c']));
      $templateProcessor->setValue('VStrasse', $arrVertrag[0]['Strasse__c']);
      $templateProcessor->setValue('VPLZ', $arrVertrag[0]['PLZ__c']);
      $templateProcessor->setValue('VOrt', $arrVertrag[0]['Ort__c']);

      $templateProcessor->setValue('BIC', $arrVertrag[0]['BIC__c']);
      $templateProcessor->setValue('IBAN', $arrVertrag[0]['IBAN__c']);

      $templateProcessor->setValue('Heute', date('d.m.Y'));


    } elseif ($_REQUEST['ac'] == 'offa') {
    
      $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($strTempPath);

      $strSql = 'SELECT * FROM `Account` WHERE `Ist_FA_Meldestelle__c` = "true" AND `SF42_Company_Group__c` = "' .$_REQUEST['Id'] .'"';
      $arrMeldestList = MySQLStatic::Query($strSql);

      $intRowCount = count($arrMeldestList);

      $templateProcessor->cloneRow('Name', $intRowCount);

      foreach ($arrMeldestList as $intKey => $arrMeldestelle) {
        $templateProcessor->setValue('Name#' .($intKey + 1), clean_for_word($arrMeldestelle['Name']));
        $templateProcessor->setValue('Strasse#' .($intKey + 1), clean_for_word($arrMeldestelle['BillingStreet']));
        $templateProcessor->setValue('PLZ#' .($intKey + 1), clean_for_word($arrMeldestelle['BillingPostalCode']));
        $templateProcessor->setValue('Ort#' .($intKey + 1), clean_for_word($arrMeldestelle['BillingCity']));

        $strSql = 'SELECT * FROM `Verbindung_Meldestelle_FA__c` WHERE `FA_Meldestelle__c` = "' .$arrMeldestelle['Id'] .'"';
        $arrVerbindung = MySQLStatic::Query($strSql);
        $templateProcessor->setValue('Steuernummer#' .($intKey + 1), $arrVerbindung[0]['Steuernummer__c']);

      }

      if ($intRowCount == 1) {
        $strTextErteilen = 'der nachfolgend genannten Gesellschaft';
      } else {
        $strTextErteilen = 'den nachfolgend genannten Gesellschaften';
      }

      //$strTextErteilen.= 'eine Vollmacht in Steuersachen:';
      $templateProcessor->setValue('TextErteilen', clean_for_word($strTextErteilen));

      $strSql = 'SELECT * FROM `Group__c` WHERE `Liefert_Daten__c` = "true" AND `Id` = "' .$_REQUEST['Id'] .'"';
      $arrGroup = MySQLStatic::Query($strSql);

      $templateProcessor->setValue('NameG', clean_for_word($arrGroup[0]['Name']));
      $templateProcessor->setValue('StrasseG', clean_for_word($arrGroup[0]['Strasse__c']));
      $templateProcessor->setValue('PLZG', clean_for_word($arrGroup[0]['PLZ__c']));
      $templateProcessor->setValue('OrtG', clean_for_word($arrGroup[0]['Ort__c']));

    } elseif ($_REQUEST['ac'] == 'ofku') {
    
      $strSql = 'SELECT * FROM `Group__c` WHERE `Liefert_Daten__c` = "true" AND `Id` = "' .$_REQUEST['Id'] .'"';
      $arrGroup = MySQLStatic::Query($strSql);

      $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($strTempPath);

      $templateProcessor->setValue('IZScompany', clean_for_word($arrGroup[0]['Name']));
      $templateProcessor->setValue('IZSstreet', $arrGroup[0]['Strasse__c']);
      $templateProcessor->setValue('IZSzip', $arrGroup[0]['PLZ__c']);
      $templateProcessor->setValue('IZScity', $arrGroup[0]['Ort__c']);

      $templateProcessor->setValue('Heute', date('d.m.Y'));

      $strSql = 'SELECT * FROM `Vertrag__c` WHERE `Company_Group__c` = "' .$_REQUEST['Id'] .'" AND `Aktiv__c` = "true"';
      $arrVertrag = MySQLStatic::Query($strSql);

      if ($arrVertrag[0]['Eingang_der_Kuendigung__c'] != '') {
        $templateProcessor->setValue('Schreiben', date('d.m.Y', strtotime($arrVertrag[0]['Eingang_der_Kuendigung__c'])));
      }
      if ($arrVertrag[0]['Kuendigung_wirksam_ab__c'] != '') {
        $templateProcessor->setValue('Endet', date('d.m.Y', strtotime($arrVertrag[0]['Kuendigung_wirksam_ab__c'])));
      }
      if ($arrVertrag[0]['Vertragsbeginn__c'] != '') {
        $templateProcessor->setValue('Beginn', date('d.m.Y', strtotime($arrVertrag[0]['Vertragsbeginn__c'])));
      }
      if ($arrVertrag[0]['Vertragslaufzeit_Monate__c'] != '') {
        $templateProcessor->setValue('Laufzeit', $arrVertrag[0]['Vertragslaufzeit_Monate__c']);
      }
      if ($arrVertrag[0]['K_ndigungsfrist_Monate__c'] != '') {
        $templateProcessor->setValue('Frist', $arrVertrag[0]['K_ndigungsfrist_Monate__c']);
      }

    } elseif ($_REQUEST['ac'] == 'ofpr') {

      $strSql = 'SELECT * FROM `temp_preiserhoehung` WHERE `Nr` = "' .$_REQUEST['Id'] .'"';
      $arrGroup = MySQLStatic::Query($strSql);

      //print_r($arrGroup); die();

      $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($strTempPath);

      $templateProcessor->setValue('AGB', clean_for_word($arrGroup[0]['AGB']));

      $templateProcessor->setValue('day_last', clean_for_word(date('d.m.Y', strtotime('last day of this month'))));
      $templateProcessor->setValue('day_new', clean_for_word(date('d.m.Y', strtotime('first day of next month'))));

      $templateProcessor->setValue('Firmengruppe', clean_for_word($arrGroup[0]['Firmengruppe']));

      $templateProcessor->setValue('SV_bisher', clean_for_word($arrGroup[0]['SV_bisher']));
      $templateProcessor->setValue('SV_neu', clean_for_word($arrGroup[0]['SV_neu']));
      
      $templateProcessor->setValue('DV_Zusatz_bisher', clean_for_word($arrGroup[0]['DV_Zusatz_bisher']));
      $templateProcessor->setValue('DV_Zusatz_neu', clean_for_word($arrGroup[0]['DV_Zusatz_neu']));
      
      $templateProcessor->setValue('Klaerung_bisher', clean_for_word($arrGroup[0]['Klaerung_bisher']));
      $templateProcessor->setValue('Klaerung_neu', clean_for_word($arrGroup[0]['Klaerung_neu']));
      
      $templateProcessor->setValue('AUe_bisher', clean_for_word($arrGroup[0]['AUe_bisher']));
      $templateProcessor->setValue('AUe_neu', clean_for_word($arrGroup[0]['AUe_neu']));
      
      $templateProcessor->setValue('BG_bisher', clean_for_word($arrGroup[0]['BG_bisher']));
      $templateProcessor->setValue('BG_neu', clean_for_word($arrGroup[0]['BG_neu']));
      
      $templateProcessor->setValue('Doku_bisher', clean_for_word($arrGroup[0]['Doku_bisher']));
      $templateProcessor->setValue('Doku_neu', clean_for_word($arrGroup[0]['Doku_neu']));
      
      $templateProcessor->setValue('Infoservice_bisher', clean_for_word($arrGroup[0]['Infoservice_bisher']));
      $templateProcessor->setValue('Infoservice_neu', clean_for_word($arrGroup[0]['Infoservice_neu']));
      
      $templateProcessor->setValue('Meldestelle_bisher', clean_for_word($arrGroup[0]['Meldestelle_bisher']));
      $templateProcessor->setValue('Meldestelle_neu', clean_for_word($arrGroup[0]['Meldestelle_neu']));      

    }    

    $templateProcessor->saveAs($strNewName);
    
    $fp = fopen($strNewName, 'rb');
    
    // send the right headers
    header('Content-type: application/vnd.ms-word');

    if ($strAltFileName != '') {
      header('Content-Disposition: attachment;Filename="' .$strAltFileName .'.' .$arrNewName[1] .'"');
    } else {
      header('Content-Disposition: attachment;Filename="' .$arrName[0] .'_' .date('dmY_His') .'.' .$arrNewName[1] .'"');
    }
    
    header("Content-Length: " . filesize($strNewName));
    
    // dump the picture and stop the script
    fpassthru($fp);
    
    unlink($strNewName);
    
    exit;


  }
  
} else {
  echo "Bitte Daten eingeben und Template auswählen.";  
}

?>