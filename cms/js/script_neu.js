  $('.bgaction').bind('click', function(e) {
      var id = $(this).attr('id');
      $(this).parent().parent().fadeTo('slow', 0.00);
      $.ajax({
          type: "POST",
          url: "action/ajax.php",
          dataType: "text",
          data: { ac: 'ddStatusClick', ddid: id },
          success: function(result) {
              jsonData = $.parseJSON(result);

              $('#' + jsonData['ddid']).parent().parent().fadeTo('slow', 1.00);


              if (jsonData['Status'] == 'OK') {

                  if (jsonData['Action']) {

                      $('#' + jsonData['Name']).modal('show');
                      $('#ddid').val(jsonData['ddid']);

                  } else {

                      $('#st_' + jsonData['ddid']).html(jsonData['UStatus']);
                      $('#da_' + jsonData['ddid']).html(jsonData['UTime']);
                      $('#es_' + jsonData['ddid']).html(jsonData['Deadline']);
                      $('#es_' + jsonData['ddid']).removeClass('red-600');
                      $('#es_' + jsonData['ddid']).addClass(jsonData['DeadlineClass']);
                      $('#dah_' + jsonData['ddid']).html(jsonData['UTimeH']);
                      $('#esh_' + jsonData['ddid']).html(jsonData['DeadlineH']);
                      $('#infoe_' + jsonData['ddid']).html(jsonData['InfoExtern']);
                      $('#' + jsonData['ddid']).html(jsonData['UStatusNew']);
                      $('#' + jsonData['ddid']).prop('title', jsonData['UStatusNew']);
                      //alert(jsonData['Recipients']);

                  }
              }

              if (jsonData['Status'] == 'FAIL') {

                  $('#ModalDanger').modal('show');
                  $("#ModalDanger #errMessage").html(jsonData['Reason']);

              }

          }
      });
  });

  $('.bgaction').bind('mouseover', function() {
      $(this).css('cursor', 'pointer');
  });

  $('.bginfo').bind('click', function(e) {
      var id = $(this).attr('id');
      var ent = $(this).parents('tr').children('td').find('span.inform').html();
      var tr = $(this).parents('tr');

      $(this).parent().parent().fadeTo('slow', 0.00);

      $.ajax({
          type: "POST",
          url: "action/ajax.php",
          dataType: "text",
          data: { ac: 'ddInfoClick', ddid: id, ent: ent },
          success: function(result) {
              jsonData = $.parseJSON(result);

              //$('#' + jsonData['ddid']).parent().parent().fadeTo('slow', 1.00);


              if (jsonData['Status'] == 'OK') {

                  tr.fadeOut('slow');
                  $("#DocCount").text(parseInt($("#DocCount").text()) - 1);

              }

              if (jsonData['Status'] == 'FAIL') {

                  $('#ModalDanger').modal('show');
                  $("#ModalDanger #errMessage").html(jsonData['Reason']);

              }

          }
      });
  });

  $('.bginfo').bind('mouseover', function() {
      $(this).css('cursor', 'pointer');
  });


  $('.cVollmacht').bind('click', function(e) {
      var id = $(this).attr('id');
      var year = $("#year").val();
      var ref = $(this).attr('ref');

      if (ref != '') {
          ref = $(this).attr('id');
          parts = ref.split('_');
          ref = parts[1];
          id = $(this).attr('ref');
      } else {
          parts = id.split('_');
          id = parts[1];
      }

      $(this).parent().parent().fadeTo('slow', 0.00);

      $.ajax({
          type: "POST",
          url: "action/ajax.php",
          dataType: "text",
          data: { ac: 'bgCreateVollmachtClick', bgid: id, year: year, bnr: ref },
          success: function(result) {
              jsonData = $.parseJSON(result);


              if (jsonData['Status'] == 'OK') {

                  setTimeout(function() {
                      console.log(jsonData);
                      console.log('#v_' + jsonData['bnr']);
                      if (jsonData['bnr']) {
                          $('#cv_' + jsonData['bnr']).attr("href", jsonData['CLink']);
                          $('#cv_' + jsonData['bnr']).attr("title", jsonData['CTimeHf']);
                          $('#cv_' + jsonData['bnr']).html(jsonData['CTime']);
                          $('#cvh_' + jsonData['bnr']).html(jsonData['CTimeH']);
                          $('#v_' + jsonData['bnr']).parent().parent().fadeTo('slow', 1.00);
                      } else {
                          $('#cv_' + jsonData['bgid']).attr("href", jsonData['CLink']);
                          $('#cv_' + jsonData['bgid']).attr("title", jsonData['CTimeHf']);
                          $('#cv_' + jsonData['bgid']).html(jsonData['CTime']);
                          $('#cvh_' + jsonData['bgid']).html(jsonData['CTimeH']);
                          $('#v_' + jsonData['bgid']).parent().parent().fadeTo('slow', 1.00);
                      }
                  }, 1000);

              }


              if (jsonData['Status'] == 'FAIL') {

                  $('#ModalDanger').modal('show');
                  $("#ModalDanger #errMessage").html(jsonData['Reason']);
                  $('#v_' + jsonData['bgid']).parent().parent().fadeTo('slow', 1.00);

              }

          }
      });


  });

  $('.cVollmacht').bind('mouseover', function() {
      $(this).css('cursor', 'pointer');
  });



  $('.cAnfrage').bind('click', function(e) {
      var id = $(this).attr('id');
      var year = $("#year").val();
      var ref = $(this).attr('ref');
      var online = $("#online").val();

      if (ref != '') {
          ref = $(this).attr('id');
          parts = ref.split('_');
          ref = parts[1];
          id = $(this).attr('ref');
      } else {
          parts = id.split('_');
          id = parts[1];
      }

      $(this).parent().parent().fadeTo('slow', 0.00);

      $.ajax({
          type: "POST",
          url: "action/ajax.php",
          dataType: "text",
          data: { ac: 'bgCreateAnfrageClick', bgid: id, year: year, bnr: ref, online: online },
          success: function(result) {
              jsonData = $.parseJSON(result);


              if (jsonData['Status'] == 'OK') {

                  setTimeout(function() {

                      if (jsonData['bnr']) {
                          $('#cx_' + jsonData['bnr']).attr("href", jsonData['XLink']);
                          $('#cx_' + jsonData['bnr']).attr("title", jsonData['ATimeHf']);
                          $('#cx_' + jsonData['bnr']).html(jsonData['ATime']);

                          $('#ca_' + jsonData['bnr']).attr("href", jsonData['ALink']);
                          $('#ca_' + jsonData['bnr']).attr("title", jsonData['ATimeHf']);
                          $('#ca_' + jsonData['bnr']).html(jsonData['ATime']);
                          $('#cah_' + jsonData['bnr']).html(jsonData['ATimeH']);
                          $('#a_' + jsonData['bnr']).parent().parent().fadeTo('slow', 1.00);
                      } else {
                          //alert(jsonData['Recipients']);
                          $('#cx_' + jsonData['bgid']).attr("href", jsonData['XLink']);
                          $('#cx_' + jsonData['bgid']).attr("title", jsonData['ATimeHf']);
                          $('#cx_' + jsonData['bgid']).html(jsonData['ATime']);

                          $('#ca_' + jsonData['bgid']).attr("href", jsonData['ALink']);
                          $('#ca_' + jsonData['bgid']).attr("title", jsonData['ATimeHf']);
                          $('#ca_' + jsonData['bgid']).html(jsonData['ATime']);
                          $('#cah_' + jsonData['bgid']).html(jsonData['ATimeH']);
                          $('#a_' + jsonData['bgid']).parent().parent().fadeTo('slow', 1.00);
                      }

                  }, 1000);

              }


              if (jsonData['Status'] == 'FAIL') {

                  $('#ModalDanger').modal('show');
                  $("#ModalDanger #errMessage").html(jsonData['Reason']);
                  if (jsonData['bnr']) {
                      $('#a_' + jsonData['bnr']).parent().parent().fadeTo('slow', 1.00);
                  } else {
                      $('#a_' + jsonData['bgid']).parent().parent().fadeTo('slow', 1.00);
                  }

              }

          }
      });


  });

  $('.cAnfrage').bind('mouseover', function() {
      $(this).css('cursor', 'pointer');
  });



  $('.cVorlage').bind('click', function(e) {
      var id = $(this).attr('id');
      var year = $("#year").val();

      parts = id.split('_');
      id = parts[1];

      var vorlage = $('#vorlage').val();

      parts = vorlage.split('_');
      opt = parts[1];

      //alert(id + ' ' + opt + ' ' + year);
      location.href = '/cms/action/createPreviewBg.php?acid=' + id + '&year=' + year + '&ctid=' + opt + '';


  });

  $('.cVorlage').bind('mouseover', function() {
      $(this).css('cursor', 'pointer');
  });


  $('.sAnfrage').bind('click', function(e) {
      var id = $(this).attr('id');
      var year = $("#year").val();
      var ref = $(this).attr('ref');

      if (ref != '') {
          ref = $(this).attr('id');
          parts = ref.split('_');
          ref = parts[1];
          id = $(this).attr('ref');
      } else {
          parts = id.split('_');
          id = parts[1];
      }

      var vorlage = $('#vorlage').val();

      parts = vorlage.split('_');
      opt = parts[1];

      $(this).parent().parent().fadeTo('slow', 0.00);

      $.ajax({
          type: "POST",
          url: "action/ajax.php",
          dataType: "text",
          data: { ac: 'bgSendAnfrageClick', bgid: id, year: year, ctid: opt, bnr: ref },
          success: function(result) {
              jsonData = $.parseJSON(result);


              if (jsonData['Status'] == 'OK') {

                  setTimeout(function() {

                      if (jsonData['bnr']) {
                          $('#sd_' + jsonData['bnr']).html(jsonData['STime']);
                          $('#sdh_' + jsonData['bnr']).html(jsonData['STimeH']);
                          $('#sr_' + jsonData['bnr']).parent().parent().fadeTo('slow', 1.00);
                      } else {
                          //alert(jsonData['Recipients']);
                          //$('#sr_' + jsonData['bgid']).html('angefragt');
                          //$('#sr_' + jsonData['bgid']).removeClass('sAnfrage');
                          $('#sd_' + jsonData['bgid']).html(jsonData['STime']);
                          $('#sdh_' + jsonData['bgid']).html(jsonData['STimeH']);
                          $('#sr_' + jsonData['bgid']).parent().parent().fadeTo('slow', 1.00);
                      }
                  }, 1000);

              }


              if (jsonData['Status'] == 'FAIL') {

                  $('#ModalDanger').modal('show');
                  $("#ModalDanger #errMessage").html(jsonData['Reason']);
                  if (jsonData['bnr']) {
                      $('#a_' + jsonData['bnr']).parent().parent().fadeTo('slow', 1.00);
                  } else {
                      $('#sr_' + jsonData['bgid']).parent().parent().fadeTo('slow', 1.00);
                  }

              }

          }
      });


  });

  $('.sAnfrage').bind('mouseover', function() {
      $(this).css('cursor', 'pointer');
  });


  $('.tab').bind('click', function(e) {
      location.href = $(this).attr('href');
  });

  $('.tab').bind('mouseover', function() {
      $(this).css('cursor', 'pointer');
  });