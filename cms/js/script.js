  function wopen(url) {
      var newWindow = window.open(url);
      return false;
  }

  function wclose() {
      window.close();
  }

  jQuery(function() {

      var changeTooltipPosition = function(event) {
          var tooltipX = event.pageX - 8;
          var tooltipY = event.pageY + 8;
          $('div.tooltip').css({ top: tooltipY, left: tooltipX });
      };

      var showTooltip = function(event) {
          $('div.tooltip').remove();
          $('<div class="tooltip">' + $(this).next("span").html() + '</div>').appendTo('body');
          changeTooltipPosition(event);
      };

      var hideTooltip = function() {
          $('div.tooltip').remove();
      };

      $(".hint").bind({
          mousemove: changeTooltipPosition,
          mouseenter: showTooltip,
          mouseleave: hideTooltip
      });

      $("#indel").bind('click', function() {
          $("#in_all").val("[LÖSCHEN]");
      });
      $('#indel').bind('mouseover', function() {
          $(this).css('cursor', 'pointer');
      });

      $("#allmassdel").bind('click', function() {
          $("#in_allmass").val("[LÖSCHEN]");
      });
      $('#allmassdel').bind('mouseover', function() {
          $(this).css('cursor', 'pointer');
      });

      $("#datedel").bind('click', function() {
          $("#date_all").val("00.00.0000");
          $("#date2_all").val("00.00.0000");
      });
      $('#datedel').bind('mouseover', function() {
          $(this).css('cursor', 'pointer');
      });

      $("#datedel3").bind('click', function() {
          $("#date3_all").val("00.00.0000");
      });
      $('#datedel3').bind('mouseover', function() {
          $(this).css('cursor', 'pointer');
      });


      $("#apdel").bind('click', function() {
          $("#ap_all").val("<delete>");
      });
      $('#apdel').bind('mouseover', function() {
          $(this).css('cursor', 'pointer');
      });

      $("#teldel").bind('click', function() {
          $("#tel_all").val("<delete>");
      });
      $('#teldel').bind('mouseover', function() {
          $(this).css('cursor', 'pointer');
      });

      $("input[name='check_all_r']").click(function() {
          var cblist = $("input[name^=\"rueck\"]");
          cblist.attr("checked", !cblist.attr("checked"));

          $("#5 tr:hidden").each(function() {
              var idp = $(this).attr("id").split("_");
              $("#check_" + idp[1]).attr("checked", false);
          });

      });

      $("input[name='check_all_r']").click(function() {
          var cblist = $("input[name^=\"check\"]");
          cblist.attr("checked", !cblist.attr("checked"));
      });

      $("input[name='check_all_r']").click(function() {
          var cblist = $("input[name^=\"klae\"]");
          cblist.attr("checked", !cblist.attr("checked"));

          $("#5 tr:hidden").each(function() {
              var idp = $(this).attr("id").split("_");
              $("#check_" + idp[1]).attr("checked", false);
          });

      });

      $("input[name='check_all']").click(function() {
          var cblist = $("input[name^=\"publish\"]");
          cblist.attr("checked", !cblist.attr("checked"));
      });

      $('.cat').bind('change', function(e) {
          var arr = e.target.id.split('_');
          $.ajax({
              type: "POST",
              url: "_ajax.php",
              dataType: "html; charset=utf-8",
              data: { id: arr[1], val: $('#' + e.target.id).val(), type: 'chcat' },
              success: function(result) {;
              }
          });
      });

      $('#strSelText').bind('input change paste', function() {
          if ($(this).val() == '') {
              $(this).addClass('red');
          } else {
              $(this).removeClass('red');
          }
      });

      $('.update_field').bind('input change paste', function(e) {
          var id = e.target.id.split('_')[1];
          $.ajax({
              type: "POST",
              url: "_ajax.php",
              dataType: "html; charset=utf-8",
              data: { id: id, val: $(this).val(), type: 'sfield' },
              success: function(result) {;
              }
          });
          $('#h_' + this.id).html($(this).val());
      });


      $(document).keydown(function(event) {
          //19 for Mac Command+S
          if (!(String.fromCharCode(event.which).toLowerCase() == 's' && event.ctrlKey) && !(event.which == 19)) return true;
          if (typeof $("#upl") != 'undefined') {
              //$("#upl").submit();
              //event.preventDefault();
          }
          return false;
      });

      //Name der Vorlage
      $(".inname").bind('keyup', function() {
          var dbid = $(this).attr('id').replace('in_name_', '');
          $("#sn_" + dbid).text($(this).val());
          $.ajax({
              type: "POST",
              url: "_ajax.php",
              dataType: "html; charset=utf-8",
              data: { id: $(this).attr('id'), content: $(this).val(), type: 'name' },
              success: function(result) {;
              }
          });
      });

      //Überschrift der Vorlage
      $(".inhead").bind('keyup', function() {
          var dbid = $(this).attr('id').replace('in_head_', '');
          //$("#sh_" + dbid).text($(this).val());
          $.ajax({
              type: "POST",
              url: "_ajax.php",
              dataType: "html; charset=utf-8",
              data: { id: $(this).attr('id'), content: $(this).val(), type: 'head' },
              success: function(result) {;
              }
          });
      });

      //Auf- und Zuklappen
      $(".head").bind('click', function() {
          if (typeof $(this).attr('id') !== 'undefined') {
              var dbid = $(this).attr('id').replace('sn_', '');
              if ($('tr#text_' + dbid).hasClass('hidden')) {
                  $('tr#name_' + dbid).removeClass('hidden');
                  $('tr#head_' + dbid).removeClass('hidden');
                  $('tr#text_' + dbid).removeClass('hidden');
                  $('#snf_' + dbid).removeClass('hidden');
                  $('#sn_' + dbid).removeClass('acc-c');
                  $('#sn_' + dbid).addClass('acc-o');
                  addArea('in_text_' + dbid);
              } else {
                  $('tr#name_' + dbid).addClass('hidden');
                  $('tr#head_' + dbid).addClass('hidden');
                  $('tr#text_' + dbid).addClass('hidden');
                  $('#snf_' + dbid).addClass('hidden');
                  $('#sn_' + dbid).removeClass('acc-o');
                  $('#sn_' + dbid).addClass('acc-c');
                  removeArea('in_text_' + dbid);
              }
          }
      });

      $('.head').bind('mouseover', function() {
          $(this).css('cursor', 'pointer');
      });

      $('.ic_preview_m').bind('mouseover', function() {
          $(this).css('cursor', 'pointer');
      });

      if ($(location).attr('href').indexOf('open') != -1) {
          $('#sn_' + $(location).attr('href').split('open=')[1]).trigger('click');
      }


      //Klick auf Vorschau
      $(".ic_preview").bind('click', function() {
          var add = '';
          if (($(location).attr('href').indexOf('auth_d') != -1) || ($(location).attr('href').indexOf('auth_e') != -1)) {
              add = '&dez=1'
          }
          if ($("#strSelText").val() == '') {
              alert("Bitte zuerst eine Vorlage auswählen.");
              $("#strSelText").focus();
          } else {
              var dbid = $(this).attr('id').replace('p_', '');
              var lnk = 'createPreview.php?ctId=' + $("#strSelText").val() + '&accId=' + dbid + '&strSelDate=' + $("#strSelDate").val() + '&strSelRuec=' + $("#strSelRuec").val() + '&type=Brief' + add;
              window.open(lnk, 'preview');
          }
      });

      $('.ic_preview').bind('mouseover', function() {
          $(this).css('cursor', 'pointer');
      });

      $('.send').bind('mouseover', function() {
          $(this).css('cursor', 'pointer');
      });


      //Hinzufügen
      $(".add").bind('click', function() {
          $.ajax({
              type: "POST",
              url: "_ajax.php",
              dataType: "html; charset=utf-8",
              data: { fdo: 'add' },
              success: function(result) {
                  //http://www.izs-institut.de/cms/
                  var lnk = 'index.php?ac=lett&open=' + result;
                  location.href = lnk;
              }
          });
      });
      $('.add').bind('mouseover', function() {
          $(this).css('cursor', 'pointer');
      });

      //Löschen
      $(".delete").bind('click', function() {
          var ctid = $(this).attr('id').replace('fd_', '');
          $.ajax({
              type: "POST",
              url: "_ajax.php",
              dataType: "html; charset=utf-8",
              data: { fdo: 'delete', ctId: ctid },
              success: function(result) {
                  $('#sn_' + ctid).trigger('click');
                  $('#h_' + ctid).parent().addClass('hidden');
              }
          });
      });
      $('.delete').bind('mouseover', function() {
          $(this).css('cursor', 'pointer');
      });

      //Kopieren
      $(".copy").bind('click', function() {
          var ctid = $(this).attr('id').replace('fc_', '');
          $.ajax({
              type: "POST",
              url: "_ajax.php",
              dataType: "html; charset=utf-8",
              data: { fdo: 'copy', ctId: ctid },
              success: function(result) {
                  var lnk = 'index.php?ac=lett&open=' + result;
                  //alert(result);
                  location.href = lnk;
              }
          });
      });
      $('.copy').bind('mouseover', function() {
          $(this).css('cursor', 'pointer');
      });


      //Kopieren Event
      $(".copyE").bind('click', function() {
          var ctid = $(this).attr('id').replace('fc_', '');
          $.ajax({
              type: "POST",
              url: "_ajax.php",
              dataType: "html; charset=utf-8",
              data: { fdo: 'copy', ctId: ctid, type: 1 },
              success: function(result) {
                  var lnk = 'index.php?ac=lete&open=' + result;
                  //alert(result);
                  location.href = lnk;
              }
          });
      });
      $('.copyE').bind('mouseover', function() {
          $(this).css('cursor', 'pointer');
      });



      //Hinzufügen Event
      $(".addE").bind('click', function() {
          $.ajax({
              type: "POST",
              url: "_ajax.php",
              dataType: "html; charset=utf-8",
              data: { fdo: 'add', type: 1 },
              success: function(result) {
                  //http://www.izs-institut.de/cms/
                  var lnk = 'index.php?ac=lete&open=' + result;
                  location.href = lnk;
              }
          });
      });
      $('.addE').bind('mouseover', function() {
          $(this).css('cursor', 'pointer');
      });

      //Hinzufügen Event
      $(".showPE").bind('click', function() {
          window.open('platzhalter_events.php');
      });
      $('.showPE').bind('mouseover', function() {
          $(this).css('cursor', 'pointer');
      });

      //Hinzufügen Event
      $(".showP").bind('click', function() {
          window.open('platzhalter.php');
      });
      $('.showP').bind('mouseover', function() {
          $(this).css('cursor', 'pointer');
      });

      //Löschen Event
      $(".deleteE").bind('click', function() {
          var ctid = $(this).attr('id').replace('fd_', '');
          $.ajax({
              type: "POST",
              url: "_ajax.php",
              dataType: "html; charset=utf-8",
              data: { fdo: 'delete', ctId: ctid },
              success: function(result) {
                  $('#sn_' + ctid).trigger('click');
                  $('#h_' + ctid).parent().addClass('hidden');
              }
          });
      });
      $('.deleteE').bind('mouseover', function() {
          $(this).css('cursor', 'pointer');
      });

      function addArea(inst) {
          area2 = new nicEditor({
              buttonList: ['save', 'bold', 'italic', 'underline', 'left', 'center', 'right', 'xhtml'],
              maxHeight: 300,
              onSave: function(content, id, instance) {
                  $.ajax({
                      type: "POST",
                      url: "_ajax.php",
                      dataType: "html; charset=utf-8",
                      data: { id: id, content: content, type: 'text' },
                      success: function(result) {
                          /*
                          $.blockUI({ 
                              message: 'Vorlage gespeichert.', 
                              fadeIn: 10, 
                              fadeOut: 700, 
                              timeout: 1500, 
                              showOverlay: false, 
                              centerY: false, 
                              css: { 
                                  width: '350px', 
                                  border: 'none', 
                                  padding: '5px', 
                                  backgroundColor: '#000', 
                                  '-webkit-border-radius': '10px', 
                                  '-moz-border-radius': '10px', 
                                  opacity: .6, 
                                  color: '#fff' 
                              } 
                          });
                          */
                      }
                  });
              }
          }).panelInstance(inst).addEvent('blur', function() {
              /*
              if (typeof nicEditors.findEditor(inst) != 'undefined') {
                var content = nicEditors.findEditor(inst).getContent();
                  $.ajax({
                    type: "POST",
                    url:  "_ajax.php",
                    data: { id: inst, content: content, type: 'text' },
                    success: function(result) {
                      $.blockUI({ 
                          message: 'Vorlage gespeichert.', 
                          fadeIn: 10, 
                          fadeOut: 700, 
                          timeout: 1500, 
                          showOverlay: false, 
                          centerY: false, 
                          css: { 
                              width: '350px', 
                              border: 'none', 
                              padding: '5px', 
                              backgroundColor: '#000', 
                              '-webkit-border-radius': '10px', 
                              '-moz-border-radius': '10px', 
                              opacity: .6, 
                              color: '#fff' 
                          } 
                      }); 
                    }
                  });
              }*/
          });
      }

      function removeArea(inst) {
          if (typeof area2 != 'undefined') {
              area2.removeInstance(inst);
          }
      }


      // ************************************************* //


      $('#filterMeldestelleStatus .item').bind('click', function() {
          $('#filterMeldestelleStatus .item').removeClass('active')
          $(this).addClass('active');
          if ($(this).attr('id') == 'iall') {
              $('tr.inactive').removeClass('hidden');
              $('tr.active').removeClass('hidden');
          }
          if ($(this).attr('id') == 'iact') {
              $('tr.inactive').addClass('hidden');
              $('tr.active').removeClass('hidden');
          }
          if ($(this).attr('id') == 'iina') {
              $('tr.inactive').removeClass('hidden');
              $('tr.active').addClass('hidden');
          }
      });
      $('#filterMeldestelleStatus .item').bind('mouseover', function() {
          $(this).css('cursor', 'pointer');
      });

      $("#export-csv, #import, #doc-create, #sel-marked, #sel-all").hover(
              function() {
                  $(this).addClass("ui-state-hover");
              },
              function() {
                  $(this).removeClass("ui-state-hover");
              }
          ).mousedown(function() {
              $(this).addClass("ui-state-active");
          })
          .mouseup(function() {
              $(this).removeClass("ui-state-active");
          });


  });

  function setSrc(url, id) {
      $(preview).attr('src', 'http://docs.google.com/gview?url=http://izs-institut.de/cms/' + url + '&embedded=true');
      $('#tabs_container').find('span').css('font-weight', 'normal');
      $('#' + id).css('font-weight', 'bold');
  }

  function selEv(id) {
      if ($('#e_' + id).css('font-weight') == '300') {
          $('#list').find('tr').css('font-weight', '300');
          $('#e_' + id).css('font-weight', '700');
      } else {
          $('#e_' + id).css('font-weight', '300');
      }
  }