
function rsForm (form) {
  
  f = $('#' + form);
  
  $("select").closest("form").on("reset",function(ev){
  var targetJQForm = $(ev.target);
  setTimeout((function(){
      this.find("select").trigger("change");
  }).bind(targetJQForm),0);
  });

  f.formValidation('resetForm', true);
  f[0].reset();

  setTimeout(function (){
    $('[data-plugin="select2"]').each(function() {
      f.data("formValidation").resetField(this.id);
    });
  }, 10);
  
}

    
function handleLogout() {
  window.location.replace('/login.php?err=2');
}
