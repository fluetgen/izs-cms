/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
$.components.register("select2", {
  mode: "init",
  defaults: {
    width: "style",
    theme: "material",
    dropdownAutoWidth : true,
    minimumResultsForSearch: 20
  },
  init: function(context) {
    if (!$.fn.select2) return;
    var defaults = $.components.getDefaults("select2");

    $('[data-plugin="select2"]', context).each(function() {
      var options = $.extend({}, defaults, $(this).data());

      $(this).select2(options);
      
    });
  }
});

/*
if (!detectIE()) {

  $(document).on('focus', '.select2', function() {
      $(this).siblings('select').select2('open');
  });
  
  var select2_open;
  // open select2 dropdown on focus
  $(document).on('focus', '.select2-selection--single', function(e) {
      select2_open = $(this).parent().parent().siblings('select');
      select2_open.select2('open');
  });

}

*/
/*

}).one('select2-focus', select2Focus).on("select2-blur", function () {
    $(this).one('select2-focus', select2Focus)
})

function select2Focus() {
    var select2 = $(this).data('select2');
    setTimeout(function() {
        if (!select2.opened()) {
            select2.open();
        }
    }, 0);  
}

*/