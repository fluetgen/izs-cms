function openOffersDialog(evId) {
  
  $('#e_' + evId).css('font-weight', 'bold') ;
  
	$('#overlay').fadeIn('fast', function() {
  	$('#boxpopup').css('display','block');
    $('#EventId').val(evId);
  
    $.ajax({
      type: "POST",
      url:  "_ajax.php",
      data: { event: evId, strSelDate: $('#strSelDate').val(), strSelSta: $('#strSelSta').val(), strSelInf: $('#strSelInf').val() },
      success: function(result) {
     		$('#boxpopup').html('<a onclick="closeOffersDialog(\'boxpopup\', \'e_' + evId + '\');" class="boxclose"></a>' + result);
      }
    });
    
    var w = ($(window).width() - 700) / 2;
    $('#boxpopup').animate({'left':w},400);
  
  });

}


function closeOffersDialog(prospectElementID, evId) {
	$(function($) {
		$(document).ready(function() {
		  
		  $('#' + evId).css('font-weight', 'normal') ;
		  
			//$('#' + prospectElementID).css('position','absolute');
			
			$('#' + prospectElementID).animate({'right':'-100%'}, 400, function() {
				$('#' + prospectElementID).css('position','fixed');
				$('#' + prospectElementID).css('left','100%');
				$('#overlay').fadeOut('fast');
			});
			
			
      $.ajax({
        type: "POST",
        url:  "_ajax.php",
        data: { geturl: evId },
        success: function(result) {
          //
          if (result == 'none') {
       		  $('#file_' + evId + '_l').html('');
       	  } else {
       	    $('#file_' + evId + '_l').html('<a href="' + result + '" target="_blank"><img src="img/icon_pdf.png" /></a>');
       	  }
        }
      });
			
      $.ajax({
        type: "POST",
        url:  "_ajax.php",
        data: { geturls: evId },
        success: function(result) {
          //
          if (result == 'none') {
       		  $('#file_' + evId + '_s').html('');
       	  } else {
       	    $('#file_' + evId + '_s').html('<a href="' + result + '" target="_blank"><img src="img/icon_pdf.png" /></a>');
       	  }
        }
      });
			
		});
	});
}

