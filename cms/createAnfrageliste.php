<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

session_start();

define('FPDF_FONTPATH','fpdf17/font/');
define('TEMP_PATH', 'temp/');

require_once('../assets/classes/class.mysql.php');
require('fpdf17/fpdf.php');
require('fpdf17/fpdi.php');

if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
  define('APP_PATH', '/data/www/sites/test.izs.de/html/');
} else {
  define('APP_PATH', '/data/www/sites/www.izs.de/html/');
}

//CLASSES
require_once(APP_PATH .'cms/inc/refactor.inc.php');

function cleanForPdfOutput ($strInput) {
  $strInput = str_replace(chr(150), '-', $strInput);
  $strInput = str_replace(chr(151), '-', $strInput);
  return $strInput;
}

$arrEventListRequest = array();

$arrMonth = array(
     1 => 'Januar',
     2 => 'Februar',
     3 => 'März',
     4 => 'April',
     5 => 'Mai',
     6 => 'Juni',
     7 => 'Juli',
     8 => 'August',
     9 => 'September',
    10 => 'Oktober',
    11 => 'November',
    12 => 'Dezember'
);

$arrBetrNrRandstad = array('45098331', '27817413');
$arrBeiblatt = array();

if (!isset($_REQUEST['AccountId'])) {
  $_REQUEST['AccountId'] = '';
}


$strSql = 'SELECT `SF42_Postfach_Name__c`, `SF42_Postfach_Anschrift__c`, `SF42_Postfach_Ort__c`, `SF42_Postfach_PLZ__c` FROM `Account` WHERE `Id` = "0013000000lzOjBAAU"';
$arrSql = MySQLStatic::Query($strSql);

$strPostfach = 'Postfach 1119';
$strPostfachCity = 'Neutraubling';
$strPostfachZip = '93067';

if (count($arrSql) > 0) {
  $strPostfachName = $arrSql[0]['SF42_Postfach_Name__c'];
  $strPostfach = $arrSql[0]['SF42_Postfach_Anschrift__c'];
  $strPostfachCity = $arrSql[0]['SF42_Postfach_Ort__c'];
  $strPostfachZip = $arrSql[0]['SF42_Postfach_PLZ__c'];
}

/*
$strCompany = '';
  
if ($_REQUEST['DezId'] != '') {
  $strCompany = $_REQUEST['DezId'];
} else {
  $strCompany = $_REQUEST['AccountId'];
}

$arrPeriod = explode('_', $_REQUEST['strSelDate']);

if ($_REQUEST['evid'] != '') {
  $strFileName = 'pdf/anfrage/' .$strCompany .'_' .$_REQUEST['evid'] .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_anfrage_sv.pdf';
} else {
  $strFileName = 'pdf/anfrage/' .$strCompany .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_anfrage_sv.pdf';
}

if (file_exists($strFileName) && (filesize($strFileName) == 0)) {
  unlink($strFileName);
} elseif (!file_exists($strFileName)) {
  ;
} else {
  exit;
}
*/

$strOutput = '';

$_REQUEST['Name'] = rawurldecode(cleanForPdfOutput($_REQUEST['Name']));

$strOutput.= '<h3>' .$_REQUEST['Name'] .'</h3>';

$arrEvents = array();
$arrAttachDocs = array();

if (isset($_REQUEST['strSelDate']) && ($_REQUEST['strSelDate'] != '')) {
  
  $arrPeriod = explode('_', $_REQUEST['strSelDate']);
  $strPdfMonth = $arrPeriod[0] .'/' .$arrPeriod[1];
  
}

/*
if (isset($_REQUEST['strSelDate']) && ($_REQUEST['strSelDate'] != '')) {
  
  $arrPeriod = explode('_', $_REQUEST['strSelDate']);
  
  $strOutput.= '<h1>Auskunft nach Abrechnung des Beitragsmonats ' .$arrPeriod[0] .'/' .$arrPeriod[1] .'</h1>';
  
  $strOutput.= '<table class="tablesorter" id="tables">' .chr(10);
  $strOutput.= '  <thead>' .chr(10);
  $strOutput.= '    <tr>' .chr(10);
	$strOutput.= '      <th class="{sorter: false}">Betriebsnummer</th>' .chr(10); //class="header"
	$strOutput.= '      <th class="{sorter: false}">Beitragszahler</th>' .chr(10); //class="header"
	$strOutput.= '      <th class="{sorter: false}">IZS-ID</th>' .chr(10); //class="header"
	$strOutput.= '      <th class="{sorter: false}">Beitragsrückstand</th>' .chr(10); //class="header"
  $strOutput.= '    </tr>' .chr(10);
  $strOutput.= '  </thead>' .chr(10);
  $strOutput.= '  <tbody>' .chr(10);
  
  
  $strPdfMonth = $arrPeriod[0] .'/' .$arrPeriod[1];
  
  $arrGroups = array();
  
  if (1) {
  
    //foreach ($arrResult as $arrPeriod) {
      
      //print_r($arrResult);
      
      //echo $arrPeriod['Name'] .'<br />' .chr(10);
      
      $strSql2 = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Beitragsmonat__c` = "' .$arrPeriod[0] .'" AND `Beitragsjahr__c` = "' .$arrPeriod[1] .'" AND ';
      $strSql2.= '((`SF42_EventStatus__c` = "enquired") OR (`SF42_EventStatus__c` = "to enquire")) ';
      
      if ($_REQUEST['evid'] != '') {
        $strSql2.= 'AND `evid` = "' .$_REQUEST['evid'] .'"';
      }

      if (@$_SERVER['HTTP_FKLD'] == 'on') {
        //echo $strSql2; die();
      }      
      
      $arrResult2 = MySQLStatic::Query($strSql2);
      
      if (count($arrResult2) > 0) {
        
        foreach ($arrResult2 as $arrEvent) {
          
          //if ($arrEvent['Name'] == 'E-077975') { echo 'Z.085: ' .''; }
          //echo '  ' .$arrEvent['Name'] .'<br />' .chr(10);
          //print_r($arrResult2);
          
          if ($arrEvent['SF42_informationProvider__c'] == $_REQUEST['AccountId']) {
          
            $strSql3 = 'SELECT * FROM `Account` WHERE `Name` = "' .$arrEvent['SF42_Premium_Payer__c'] .'" AND `SF42_Sub_Type__c` = "Premium Payer" ';
            $arrResult3 = MySQLStatic::Query($strSql3);
            
            //if ($arrEvent['Name'] == 'E-077975') { echo 'Acc: ' .$arrResult3[0]['Name'] .'; '; }
  
            if (count($arrResult3) > 0) {
              
              //print_r($arrResult3);
              
              foreach ($arrResult3 as $arrAccount) {
                //echo $arrAccount['Name'] .' -> ' .$arrAccount['Auskunftsart_der_Krankenkasse__c'] .'<br />' .chr(10);
                //print_r($arrAccount);
                
                if (in_array($arrEvent['Name'], $arrEvents) == false) {

                  $arrEvents[] = $arrEvent['Name'];
                  
                  $strOutput.= '    <tr>' .chr(10);
                	$strOutput.= '      <td>' .$arrEvent['Betriebsnummer_ZA__c'] .'</td>' .chr(10); //class="header"
                	$strOutput.= '      <td>' .$arrAccount['Name'] .'</td>' .chr(10); //class="header"
                	$strOutput.= '      <td>' .$arrEvent['Name'] .'</td>' .chr(10); //class="header"
                	
                	if ($arrAccount['Auskunftsart_der_Krankenkasse__c'] == 'IZS-Formular') {
                  	$strOutput.= '      <td>&#x2610; Ja&nbsp;&nbsp;&nbsp;&nbsp;&#x2610; Nein</td>' .chr(10); //class="header"
                	} elseif ($arrAccount['Auskunftsart_der_Krankenkasse__c'] == 'Beiblatt') {
                  	$strOutput.= '      <td><span class="red">Siehe Beiblatt</span></td>' .chr(10); //class="header"
                  	if ($arrEvent['SF42_DocumentUrl__c'] != '') {
                  	  $arrAttachDocs[] = $arrEvent['SF42_DocumentUrl__c'];
                    }
                	} elseif ($arrAccount['Auskunftsart_der_Krankenkasse__c'] == 'UBB') {
                  	$strOutput.= '      <td><span class="red">Bitte UBB beilegen</span></td>' .chr(10); //class="header"
                  	if ($arrEvent['SF42_DocumentUrl__c'] != '') {
                  	  $arrAttachDocs[] = $arrEvent['SF42_DocumentUrl__c'];
                    }
                	} elseif ($arrAccount['Auskunftsart_der_Krankenkasse__c'] == 'IZS-Formular + UBB') {
                	  
                	  $strSql4 = 'SELECT * FROM `UBB__c` WHERE `Information_Provider__c` = "' .$arrEvent['SF42_informationProvider__c'] .'" AND `Verleiher__c` = "' .$arrAccount['Id'] .'"';
                	  $arrResult4 = MySQLStatic::Query($strSql4);
                	  
                	  if (count($arrResult4) > 0) {                	  
                	    $strOutput.= '      <td><span class="red">Bitte UBB beilegen</span></td>' .chr(10); //class="header"
                    	if ($arrEvent['SF42_DocumentUrl__c'] != '') {
                    	  $arrAttachDocs[] = $arrEvent['SF42_DocumentUrl__c'];
                      }
                	  } else {
                    	$strOutput.= '      <td>&#x2610; Ja&nbsp;&nbsp;&nbsp;&nbsp;&#x2610; Nein</td>' .chr(10); //class="header"
                	  }
                	} elseif ($arrAccount['Auskunftsart_der_Krankenkasse__c'] == 'Beiblatt + UBB') {
                	  
                	  $strSql4 = 'SELECT * FROM `UBB__c` WHERE `Verleiher__c` = "' .$arrAccount['Id'] .'" AND `Information_Provider__c` = "' .$arrEvent['SF42_informationProvider__c'] .'"';
                	  $arrResult4 = MySQLStatic::Query($strSql4);
                	  
                	  if (count($arrResult4) > 0) {                	  
                	    $strOutput.= '      <td><span class="red">Bitte UBB beilegen</span></td>' .chr(10); //class="header"
                    	if ($arrEvent['SF42_DocumentUrl__c'] != '') {
                    	  $arrAttachDocs[] = $arrEvent['SF42_DocumentUrl__c'];
                      }
                	  } else {
                    	$strOutput.= '      <td><span class="red">Siehe Beiblatt</span></td>' .chr(10); //class="header"
                	  }
                  } else {
                	  $strOutput.= '      <td>' .$arrAccount['Auskunftsart_der_Krankenkasse__c'] .'</td>' .chr(10); //class="header"
                  }
                	
                  $strOutput.= '    </tr>' .chr(10);
                  
                }
                //
                //$strSql4 = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$arrAccount['SF42_Company_Group__c'] .'"';
                //$arrResult4 = MySQLStatic::Query($strSql4);
                
                //foreach ($arrResult4 as $arrCompanyGroup) {
                   //echo '  ' .$arrCompanyGroup['Name'] .'<br />' .chr(10);
                //   $arrGroups[$arrCompanyGroup['Id']] = utf8_decode($arrCompanyGroup['Name']);
                //}
                
                
              }
              
            }
          
          }
          
        }
        
      }
      
      
      
    //}
  
  }
  
  $strOutput.= '  </tbody>' .chr(10);
  $strOutput.= '</table>' .chr(10);      

}
*/

if ($_REQUEST['show'] == 'H') {
  
$strOutput = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us">
<head>
  <title>cms :: Vollmachten</title>
	<link rel="stylesheet" href="css/jq.css" type="text/css" media="print, projection, screen" />
	<link rel="stylesheet" href="css/style.css" type="text/css" media="print, projection, screen" />
	<script type="text/javascript" src="js/jquery-latest.js"></script>
	<script type="text/javascript" src="js/jquery.metadata.js"></script>
	<script type="text/javascript" src="js/__jquery.tablesorter.min.js"></script>

	<script type="text/javascript">
	$(function() {		
		$("#tables").tablesorter({widgets: [\'zebra\']});
	});	
	</script>
</head>
<body>

' .$strOutput .'

</body>';

header("Content-Type: text/html; charset=utf-8");
echo $strOutput;

} else {

  class concat_pdf extends fpdi {
    var $files = array();
    var $strCode = '';

    function concat_pdf($orientation='P',$unit='mm',$format='A4') {
      parent::__construct($orientation,$unit,$format);
    }

    function setFiles($files) {
      $this->files = $files;
    }

    function concat() {
      foreach($this->files AS $file) {
        $pagecount = $this->setSourceFile($file);
        for ($i = 1;  $i <= $pagecount;  $i++) {
         $tplidx = $this->ImportPage($i);
         $this->AddPage();
         $this->useTemplate($tplidx);
        }
      }
    }
    // Page footer

    function Footer() {
      // Position at 1.5 cm from bottom
      $this->SetY(-15);
      // Arial italic 8
      $this->SetFont('OpenSans','',8); 
      // Page number 

      if ($this->strCode != '') {
        $strFooterAdd = $this->strCode .' / ';
      } else {
        $strFooterAdd = '';
      }

      $this->Cell(0,10,' - ' .$strFooterAdd .'Seite '.$this->PageNo().'/{nb}' .' -',0,0,'C');
    }

    function setCode ($strCode = '') {
      $this->strCode = $strCode;
    }

  }

  $pdf = new concat_pdf();
  $pdf->AliasNbPages();

  $pdf->AddFont('OpenSans','','OpenSans-Regular.php');
  $pdf->AddFont('OpenSans','B','OpenSans-Bold.php');
  $pdf->AddFont('arialbd','','myfont.php');

  /*
  $pdf->AddPage();
  $pdf->SetAutoPageBreak(false);
  */

  /*
  $pdf->AddFont('arialbd','','arialbd.php');
  $pdf->AddFont('OpenSans','','OpenSans.php');
  $pdf->AddFont('OpenSans','B','OpenSansB.php');

  $pdf->AddPage();
  $pdf->SetAutoPageBreak(false);
  $pdf->Image('pdf/Deckblatt.jpg', 0, 0, 210, 297, 'JPG'); 
  
  $pdf->SetFont('OpenSans', 'B', 10);
  $pdf->Text(87.9, 95.9, $strPdfMonth);
  */
  
  $pdf->AddPage();
  $pdf->SetAutoPageBreak(false);
  $pdf->Image('pdf/zertifikat_vorlage.png', 0, 0, 210, 297, 'PNG'); 
  
  $pdf->SetFont('OpenSans', '', 16);
  $pdf->Text(19.4, 22.7, utf8_decode('DECKBLATT FÜR ANTWORT'));
  
  $pdf->SetMargins(19.4, 20);

  $pdf->SetY(46);

  /*
  IZS - Institut für Zahlungssicherheit GmbH
  Datenverarbeitungscenter A-II/7
  Postfach 1119
  93067 Neutraubling

  $pdf->SetFont('OpenSans', '', 10);
  $pdf->Write(5, utf8_decode('IZS Institut für Zahlungssicherheit GmbH') .chr(10));
  $pdf->Write(5, 'Abteilung Datenverarbeitung' .chr(10));
  $pdf->Write(5, utf8_decode('Würmtalstraße 20a') .chr(10));
  $pdf->Write(5, utf8_decode('81375 München') .chr(10));
  */
  
  $pdf->SetFont('OpenSans', '', 10);
  $pdf->Write(5, utf8_decode('IZS Institut für Zahlungssicherheit GmbH') .chr(10));
  $pdf->Write(5, utf8_decode($strPostfachName) .chr(10));
  $pdf->Write(5, utf8_decode($strPostfach) .chr(10));
  $pdf->Write(5, utf8_decode($strPostfachZip .' ' .$strPostfachCity) .chr(10));

  $pdf->SetY(87);
  $pdf->SetFont('OpenSans', 'B', 10);
  $pdf->Write(4, utf8_decode('Auskünfte zu beiliegend aufgelisteten Zeitarbeitsunternehmen') .chr(10));
  $pdf->Write(4, utf8_decode('nach Abrechnung des Beitragsmonats ' .$strPdfMonth) .chr(10));
  
  $pdf->SetY(100);
  $pdf->SetFont('OpenSans', 'B', 10);
  $pdf->Write(5, utf8_decode('Hinweise:') .chr(10));
  $pdf->SetFont('OpenSans', '', 8);
  $pdf->Write(3, utf8_decode('(z. B. geschlossene Konten, Ratenvereinbarungen, Zuständigkeiten. Bitte immer jeweilige BNR angeben!)') .chr(10));
  
  $pdf->SetLineWidth(0.2);
  $pdf->SetDrawColor(0, 0, 0);
  $pdf->Rect(20.4, 110, 170, 50); 

  $pdf->SetDrawColor(0, 0, 0);
  $pdf->SetLineWidth(0.2);
  $pdf->Line(20.4, 190, 103.4, 190);
  $pdf->SetFont('OpenSans', '', 8);
  $pdf->Text(21, 193, 'Ort, Datum');
  
  $pdf->SetDrawColor(0, 0, 0);
  $pdf->SetLineWidth(0.2);
  $pdf->Line(107.4, 190, 190.4, 190);
  $pdf->SetFont('OpenSans', '', 8);
  $pdf->Text(108, 193, 'Unterschrift des Bearbeiters');
  
  $pdf->SetDrawColor(0, 0, 0);
  $pdf->SetLineWidth(0.2);
  $pdf->Line(107.4, 203, 190.4, 203);
  $pdf->SetFont('OpenSans', '', 8);
  $pdf->Text(108, 206, 'Name und Funktion des Bearbeiters in Druckbuchstaben');

  $pdf->SetDrawColor(0, 0, 0);
  $pdf->SetLineWidth(0.2);
  $pdf->Line(107.4, 216, 190.4, 216);
  $pdf->SetFont('OpenSans', '', 8);
  $pdf->Text(108, 219, utf8_decode('Telefon/E-Mail für Rückfragen'));

  $pdf->SetDrawColor(0, 0, 0);
  $pdf->SetLineWidth(0.2);
  $pdf->Line(107.4, 229, 190.4, 229);
  $pdf->SetFont('OpenSans', '', 8);
  $pdf->Text(108, 232, '');
  
  $pdf->SetLineWidth(0.2);
  $pdf->SetDrawColor(0, 0, 0);
  $pdf->Rect(20.4, 203, 83, 26); 
  $pdf->SetFont('OpenSans', '', 8);
  $pdf->Text(21, 232, 'Stempel/Dienststempel');
  $pdf->Text(21, 236, utf8_decode($_REQUEST['Name']));

  $pdf->SetY(250);
  $pdf->SetFont('OpenSans', '', 10);
  $pdf->MultiCell(170, 5, utf8_decode('Alternativ zum Postweg zurück per E-Mail (auskunft@izs-institut.de)'), 0, 'C', 0); 

 
  $pdf->AddPage();
  $pdf->SetAutoPageBreak(false);
  
  $pdf->Image('pdf/Vorlage.png', 0, 0, 210, 297, 'PNG'); 
  $pdf->SetMargins(20, 57, 20);
  
  $pdf->SetFont('OpenSans', 'B', 10);
  $pdf->Text(20.4, 58.2, utf8_decode($_REQUEST['Name']));
  
  
  $arrPeriod = explode('_', $_REQUEST['strSelDate']);
  $pdf->SetFont('OpenSans', 'B', 14);
  $pdf->Text(20.4, 79, 'Auskunft nach Abrechnung des Beitragsmonats ' .$arrPeriod[0] .'/' .$arrPeriod[1]);
  
  $pdf->SetFont('OpenSans', 'B', 8);
  $pdf->Text(21, 90, 'BNR'); //Betriebsnummer
  $pdf->Text(37, 90, 'Beitragszahler'); //46
  $pdf->Text(130, 90, 'IZS-ID'); // 146
  $pdf->Text(148, 90, utf8_decode('Rückstand')); // 163
  $pdf->Text(167, 90, utf8_decode('<500') .chr(128)); // 182
  $pdf->Text(177, 90, 'SD/RV*'); // 182
  
  $pdf->SetDrawColor(0, 0, 0);
  $pdf->SetLineWidth(0.2);
  $pdf->Line(20, 93, 190, 93);
  
  $pdf->SetFont('OpenSans', '', 8);
  $pdf->Cell(20.4, 83, '', 0, 1);

  //CONTENT
  if (isset($_REQUEST['strSelDate']) && ($_REQUEST['strSelDate'] != '')) {
    
    if ($_REQUEST['DezId'] != '') {
    
      $strSql7 = 'SELECT * FROM `Anfragestelle__c` WHERE `Id` = "' .$_REQUEST['DezId'] .'"';
      $arrAnfrageListe = MySQLStatic::Query($strSql7);
      
      $_REQUEST['AccountId'] = $arrAnfrageListe[0]['Information_Provider__c'];
    
    }
    
    $strSqlKK = 'SELECT * FROM `Account` WHERE `Id`="' .$_REQUEST['AccountId'] .'"';
    $arrKK = MySQLStatic::Query($strSqlKK);

    $arrPeriod = explode('_', $_REQUEST['strSelDate']);

    // Techniker Krankenkasse
    // Zip assignment
    if ($arrKK[0]['Id'] == '001300000109k5uAAA') {

      $arrHeader = array(
        'apikey: 9b0d3b1c-27f2-4e40-8e4d-d735a50dbbee',
        'user: ' .$_SESSION['id'],
        'Accept: application/json'
      );
      
      $rarrAssign = curl_get('http://api.izs-institut.de/api/sv/zipassign', array(), array(), $arrHeader);
      $arrResult  = json_decode($rarrAssign, true);
      
      //print_r($arrResult); die();

    }
    //die();
   
    if ($_REQUEST['DezId'] != '') {
      
      $strSqlKK = 'SELECT * FROM `Anfragestelle__c` WHERE `Id`="' .$_REQUEST['DezId'] .'"';
      $arrKK = MySQLStatic::Query($strSqlKK);
      
      $arrCatchAll = array();
      $boolIsCatchAll = false;
      
      if ($arrKK[0]['CATCH_ALL__c'] == 'true') {
        $boolIsCatchAll = true;
        $arrCatchAll = $arrKK[0];
      }
      
      $strSqlKK = 'SELECT * FROM `Account` WHERE `Id`="' .$arrKK[0]['Information_Provider__c'] .'"';
      $arrKK = MySQLStatic::Query($strSqlKK);

      $strSql6 = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Beitragsmonat__c` = "' .$arrPeriod[0] .'" AND `Beitragsjahr__c` = "' .$arrPeriod[1] .'" AND ';
      $strSql6.= '((`SF42_EventStatus__c` = "enquired") OR (`SF42_EventStatus__c` = "to enquire")) ';
      $strSql6.= 'AND `SF42_informationProvider__c` = "' .$arrKK[0]['Id'] .'" ';
      if ($_REQUEST['evid'] != '') {
        $strSql6.= 'AND `evid` = "' .$_REQUEST['evid'] .'" ';
      }
      $strSql6.= 'GROUP BY `Betriebsnummer_ZA__c` ';
      $arrEventList = MySQLStatic::Query($strSql6);
      
      foreach ($arrEventList as $intEv => $arrEvent) {
        
        //if ($arrEvent['Name'] == 'E-077975') { echo 'Z.335: ' .'E-077975; '; }
    
        $strSqlAc = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c`="' .$arrEvent['Betriebsnummer_ZA__c'] .'"';
        $arrAc = MySQLStatic::Query($strSqlAc);

        //if ($arrEvent['Name'] == 'E-077975') { echo 'Account: ' .$arrAc[0]['Name'] .'; '; }
        
        //PremiumPayer gesuchtem Team zugeordnet
        $strSql7 = 'SELECT * FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrAc[0]['Id'] .'" AND `Information_Provider__c` = "' .$arrKK[0]['Id'] .'" AND `Anfragestelle__c` = "' .$_REQUEST['DezId'] .'"';
        $arrResult7 = MySQLStatic::Query($strSql7);

        //if ($arrEvent['Name'] == 'E-077975') { echo 'PP (DezAnfr): ' .$arrResult7[0]['Name'] .'; '; }
        
        if (count($arrResult7) > 0) {
          $arrLine[$arrAc[0]['SF42_Comany_ID__c']] = array(
            $arrAc[0]['SF42_Comany_ID__c'],
            utf8_decode($arrAc[0]['Name']),
            $arrEvent['Name'],
            $arrAc[0]['Auskunftsart_der_Krankenkasse__c']
          );

          //CREATE REQUEST LIST
          $arrEventListRequest[] = array(
            'evid' => $arrEvent['evid'],
            'pp' => $arrAc[0]['Id'],
            'pp_group' => $arrEvent['group_id']
          );

        } elseif ($boolIsCatchAll == true) {
          
          $strSql8 = 'SELECT `Id` FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrAc[0]['Id'] .'" AND `Information_Provider__c` = "' .$arrKK[0]['Id'] .'"';
          $arrResult8 = MySQLStatic::Query($strSql8);
          
          //if ($arrEvent['Name'] == 'E-077975') { echo 'PP (DezAnfr CA): ' .$arrResult8[0]['Name'] .'; '; }
          
          //keinem Team zugeordnet
          if (count($arrResult8) == 0) {
            $arrLine[$arrAc[0]['SF42_Comany_ID__c']] = array(
              $arrAc[0]['SF42_Comany_ID__c'],
              utf8_decode($arrAc[0]['Name']),
              $arrEvent['Name'],
              ''
            );

            //CREATE REQUEST LIST
            $arrEventListRequest[] = array(
              'evid' => $arrEvent['evid'],
              'pp' => $arrAc[0]['Id'],
              'pp_group' => $arrEvent['group_id']
            );

          }
          
        }
        
        //if ($arrEvent['Name'] == 'E-077975') { print_r($arrLine); die(); }
        
        if (is_array($arrLine[$arrAc[0]['SF42_Comany_ID__c']]) && count($arrLine[$arrAc[0]['SF42_Comany_ID__c']]) > 0) {

          //if ($arrEvent['Name'] == 'E-077975') {print_r($arrKK); }
  
        	if ($arrAc[0]['Auskunftsart_der_Krankenkasse__c'] == 'IZS-Formular') {
            $arrLine[$arrAc[0]['SF42_Comany_ID__c']][3] = 'IZS-Formular';
        	} elseif ($arrAc[0]['Auskunftsart_der_Krankenkasse__c'] == 'Beiblatt') {
            $arrLine[$arrAc[0]['SF42_Comany_ID__c']][3] = 'Beiblatt';
        	} elseif ($arrAc[0]['Auskunftsart_der_Krankenkasse__c'] == 'UBB') {
            $arrLine[$arrAc[0]['SF42_Comany_ID__c']][3] = 'UBB';
        	} elseif ($arrAc[0]['Auskunftsart_der_Krankenkasse__c'] == 'IZS-Formular + UBB') {
        	  
        	  $strSql4 = 'SELECT * FROM `UBB__c` WHERE `Information_Provider__c` = "' .$arrKK[0]['Id'] .'" AND `Verleiher__c` = "' .$arrAc[0]['Id'] .'"';
        	  $arrResult4 = MySQLStatic::Query($strSql4);
        	  
        	  if (count($arrResult4) > 0) {                	  
              $arrLine[$arrAc[0]['SF42_Comany_ID__c']][3] = 'UBB';
        	  } else {
              $arrLine[$arrAc[0]['SF42_Comany_ID__c']][3] = 'IZS-Formular';
        	  }
        	  
        	} elseif ($arrAc[0]['Auskunftsart_der_Krankenkasse__c'] == 'Beiblatt + UBB') {
        	  
        	  $strSql4 = 'SELECT * FROM `UBB__c` WHERE `Information_Provider__c` = "' .$arrKK[0]['Id'] .'" AND `Verleiher__c` = "' .$arrAc[0]['Id'] .'"';
        	  $arrResult4 = MySQLStatic::Query($strSql4);
        	  
        	  //if ($arrEvent['Name'] == 'E-077975') { print_r($arrResult4); }
        	  
        	  if (count($arrResult4) > 0) {                	  
              $arrLine[$arrAc[0]['SF42_Comany_ID__c']][3] = 'UBB';
        	  } else {
              $arrLine[$arrAc[0]['SF42_Comany_ID__c']][3] = 'Beiblatt';
        	  }
        	  
          } else {
        	  $arrLine[$arrAc[0]['SF42_Comany_ID__c']][3] = 'IZS-Formular';
          }
  
          
          if (in_array($arrAc[0]['SF42_Comany_ID__c'], $arrBetrNrRandstad) == true) {
            if ($arrLine[$arrAc[0]['SF42_Comany_ID__c']][3] == 'Beiblatt') {
              $arrBeiblatt[] = $arrLine[$arrAc[0]['SF42_Comany_ID__c']];
            }
          }
        
        }

        
        
      }

        
        
      if ($_SERVER['REMOTE_ADDR'] == '193.174.158.110') {
        //print_r($arrBeiblatt) .'|'; die(); //0013000001BjHp6AAF
      }          



    } else {
    
      $arrGroups = array();
      $arrEvents = array();
      $intAccountPage = 1;
      
      $arrLine = array();
      
      if (1) {
      
        //foreach ($arrResult as $arrPeriod) {
          
          $strSql2 = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Beitragsmonat__c` = "' .$arrPeriod[0] .'" AND `Beitragsjahr__c` = "' .$arrPeriod[1] .'" AND ';
          $strSql2.= '((`SF42_EventStatus__c` = "enquired") OR (`SF42_EventStatus__c` = "to enquire")';
          //if ($_SESSION['id'] == 3) { $strSql2.= ' OR (`SF42_EventStatus__c` = "OK")'; }
          $strSql2.= ') ';
          if ($_REQUEST['evid'] != '') {
            $strSql2.= 'AND `evid` = "' .$_REQUEST['evid'] .'" ';
          }
          $strSql2.= 'ORDER BY `Betriebsnummer_ZA__c` ';
          $arrResult2 = MySQLStatic::Query($strSql2);
          
          if (count($arrResult2) > 0) {
            foreach ($arrResult2 as $arrEvent) {
              
              //if ($arrEvent['Name'] == 'E-077975') { echo 'Z.444:' .''; }
              
              if ($arrEvent['SF42_informationProvider__c'] == $_REQUEST['AccountId']) {

                //CREATE REQUEST LIST
                $arrEventListRequest[] = array(
                  'evid' => $arrEvent['evid'],
                  'pp' => $_REQUEST['AccountId'],
                  'pp_group' => $arrEvent['group_id']
                );
        
              
                $strSql3 = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$arrEvent['Betriebsnummer_ZA__c'] .'" ';
                $arrResult3 = MySQLStatic::Query($strSql3);
                
                if (count($arrResult3) > 0) {
                  foreach ($arrResult3 as $arrAccount) {
                    
                    if (in_array($arrEvent['Name'], $arrEvents) == false) {
                      
                      $arrEvents[] = $arrEvent['Name'];
                      
                      $arrLine[$arrEvent['Betriebsnummer_ZA__c']] = array(
                        $arrEvent['Betriebsnummer_ZA__c'],
                        utf8_decode($arrAccount['Name']),
                        $arrEvent['Name'],
                        ''
                      );
                      
                    	if ($arrAccount['Auskunftsart_der_Krankenkasse__c'] == 'IZS-Formular') {
                        $arrLine[$arrEvent['Betriebsnummer_ZA__c']][3] = 'IZS-Formular';
                    	} elseif ($arrAccount['Auskunftsart_der_Krankenkasse__c'] == 'Beiblatt') {
                        $arrLine[$arrEvent['Betriebsnummer_ZA__c']][3] = 'Beiblatt';
                    	} elseif ($arrAccount['Auskunftsart_der_Krankenkasse__c'] == 'UBB') {
                        $arrLine[$arrEvent['Betriebsnummer_ZA__c']][3] = 'UBB';
                    	} elseif ($arrAccount['Auskunftsart_der_Krankenkasse__c'] == 'IZS-Formular + UBB') {
                    	  
                    	  $strSql4 = 'SELECT * FROM `UBB__c` WHERE `Information_Provider__c` = "' .$arrEvent['SF42_informationProvider__c'] .'" AND `Verleiher__c` = "' .$arrAccount['Id'] .'"';
                    	  $arrResult4 = MySQLStatic::Query($strSql4);
                    	  
                    	  if (count($arrResult4) > 0) {                	  
                          $arrLine[$arrEvent['Betriebsnummer_ZA__c']][3] = 'UBB';
                    	  } else {
                          $arrLine[$arrEvent['Betriebsnummer_ZA__c']][3] = 'IZS-Formular';
                    	  }
                    	} elseif ($arrAccount['Auskunftsart_der_Krankenkasse__c'] == 'Beiblatt + UBB') {
                    	  
                    	  $strSql4 = 'SELECT * FROM `UBB__c` WHERE `Information_Provider__c` = "' .$arrEvent['SF42_informationProvider__c'] .'" AND `Verleiher__c` = "' .$arrAccount['Id'] .'"';
                    	  $arrResult4 = MySQLStatic::Query($strSql4);
                    	  
                    	  if (count($arrResult4) > 0) {                	  
                          $arrLine[$arrEvent['Betriebsnummer_ZA__c']][3] = 'UBB';
                    	  } else {
                          $arrLine[$arrEvent['Betriebsnummer_ZA__c']][3] = 'Beiblatt';
                    	  }
                      } else {
                    	  $arrLine[$arrEvent['Betriebsnummer_ZA__c']][3] = 'IZS-Formular';
                      }
  
                      
                      if (in_array($arrEvent['Betriebsnummer_ZA__c'], $arrBetrNrRandstad) == true) {
                        if ($arrLine[$arrEvent['Betriebsnummer_ZA__c']][3] == 'Beiblatt') {
                          $arrBeiblatt[$arrEvent['Betriebsnummer_ZA__c']] = $arrLine[$arrEvent['Betriebsnummer_ZA__c']];
                        }
                      }
  
                      $arrLine[$arrEvent['Betriebsnummer_ZA__c']][4] = $arrEvent['Betriebsnummer_ZA__c'];
                      
                    }
                  }
                  
                }
              
              }
              
            }
            
          }
          
          
          
        //}
      
      }
      
    }
    
    //print_r($arrLine); die();
    
    $arrExcel = array();
    
    if (is_array($arrLine) && (count($arrLine) > 0)) {
      
      if ($_REQUEST['DezId'] != '') {
        $strSqlKK = 'SELECT * FROM `Anfragestelle__c` WHERE `Id`="' .$_REQUEST['DezId'] .'"';
        $arrKK = MySQLStatic::Query($strSqlKK);
      }

      if ($arrKK[0]['Abweichende_Sortierung_der_Anfrageliste__c'] == 'alphabetisch aufsteigend') {
    
        // Vergleichsfunktion
        function vergleich($wert_a, $wert_b) {
            // Sortierung nach dem zweiten Wert des Array (Index: 1)
            $a = strtolower($wert_a[1]);
            $b = strtolower($wert_b[1]);
        
            if ($a == $b) { 
              return 0;
            }
        
            return ($a < $b) ? -1 : +1;
        }

        usort($arrLine, 'vergleich');
        
      } else {
        ksort($arrLine);
      }
      
      //print_r($arrLine); die();

      $intNumEventList = count($arrLine);

      $strReq00 = 'SELECT MAX(`sr_count`) AS `sr_count` FROM `izs_sv_request` WHERE `sr_ip` = "' .$_REQUEST['AccountId'] .'" AND `sr_ip_loc` = "' .$_REQUEST['DezId'] .'" AND `sr_evid` = "' .$_REQUEST['evid'] .'" AND `sr_month` = "' .$arrPeriod[0] .'" AND `sr_year` = "' .$arrPeriod[1] .'" AND `sr_sent` != "0000-00-00 00:00:00"';
      $arrReq00 = MySQLStatic::Query($strReq00);

      if ($arrReq00[0]['sr_count'] > 0) {
        $intCoutList = $arrReq00[0]['sr_count'] + 1;
      } else {
        $intCoutList = 1;
      }

      $strEventRequest = '';
      if ($_REQUEST['evid'] != '') {
        $strEventRequest = $arrAc[0]['Id'];
      }

      $strReq01 = 'SELECT `rn_id` FROM `izs_sv_request_number` WHERE `rn_key` = "' .$_REQUEST['AccountId'] .'_D' .$_REQUEST['DezId'] .'_E' .$strEventRequest .'"';
      $arrReq01 = MySQLStatic::Query($strReq01);

      if ($arrReq01[0]['rn_id'] > 0) {

        $strReqId = dechex($arrReq01[0]['rn_id']);
      
      } else {

        $strReq02 = 'INSERT INTO `izs_sv_request_number` SET `rn_key` = "' .$_REQUEST['AccountId'] .'_D' .$_REQUEST['DezId'] .'_E' .$strEventRequest .'"';
        $arrReq02 = MySQLStatic::Query($strReq02);
        
        $strReq01 = 'SELECT `rn_id` FROM `izs_sv_request_number` WHERE `rn_key` = "' .$_REQUEST['AccountId'] .'_D' .$_REQUEST['DezId'] .'_E' .$strEventRequest .'"';
        $arrReq01 = MySQLStatic::Query($strReq01);
        $strReqId = dechex($arrReq01[0]['rn_id']);
  
      }


      //str_pad($value, 8, '0', STR_PAD_LEFT)
      $pdf->setCode($strReqId .'-' .substr($arrPeriod[1], -2) .str_pad($arrPeriod[0], 2, '0', STR_PAD_LEFT) .'-' .$intCoutList);
      
      $intAccountPage = 1;
      $pdf->SetY(93);
            
      foreach($arrLine as $arrRow) {
        
        $arrExcelLine['Vorgang'] = $arrRow[2];
        $arrExcelLine['BNR'] = $arrRow[0];
        $arrExcelLine['Arbeitgeber'] = $arrRow[1];
        $arrExcelLine['UBB'] = 'nein';
        $arrExcelLine['CompanyId'] = $arrRow[4];

        if ($intAccountPage >= 2) {
          $intY = 100.5 + (($intAccountPage - 2) * 8);
          $pdf->SetDrawColor(0, 0, 0);
          $pdf->SetLineWidth(0.2);
          $pdf->Line(20.4, $intY, 189.6, $intY);
        }
        
        $pdf->Cell(16, 8, $arrRow[0], 0, 0); //25
        $pdf->Cell(92, 8, $arrRow[1], 0, 0); // 100
        $pdf->Cell(18, 8, $arrRow[2], 0, 0); // 18
        
      	
      	if ($arrRow[3] == 'IZS-Formular') {
      	  
      	  $pdf->SetFont('arialbd', '', 8);
      	  $pdf->Write(8, 'O');
      	  $pdf->SetFont('OpenSans', '', 8);
      	  $pdf->Write(8, ' Nein  ');
      	  $pdf->SetFont('arialbd', '', 8);
      	  $pdf->Write(8, 'O');
      	  $pdf->SetFont('OpenSans', '', 8);
      	  $pdf->Write(8, ' Ja:         ');
      	  $pdf->SetFont('arialbd', '', 8);
          $pdf->Write(8, 'O');      	  

          $pdf->SetFont('OpenSans', '', 8);
      	  $pdf->Write(8, '           ');
      	  $pdf->SetFont('arialbd', '', 8);
          $pdf->Write(8, 'O');
          
          $pdf->SetFont('OpenSans', '', 8);
      	  
      	} elseif ($arrRow[3] == 'Beiblatt') {
      	  $pdf->SetTextColor(255, 0, 0);
      	  $pdf->Cell(25, 8, 'Siehe Beiblatt', 0, 0);
      	  $pdf->SetTextColor(0, 0, 0);
      	} elseif ($arrRow[3] == 'UBB') {
      	  $pdf->SetTextColor(255, 0, 0);
      	  $pdf->Cell(25, 8, 'Bitte UBB beilegen', 0, 0);
      	  $pdf->SetTextColor(0, 0, 0);
      	  
      	  $arrExcelLine['UBB'] = 'ja';
      	  
        } else {
      	  
      	  $pdf->SetFont('arialbd', '', 8);
      	  $pdf->Write(8, 'O');
      	  $pdf->SetFont('OpenSans', '', 8);
      	  $pdf->Write(8, ' Nein  ');
      	  $pdf->SetFont('arialbd', '', 8);
      	  $pdf->Write(8, 'O');
      	  $pdf->SetFont('OpenSans', '', 8);
      	  $pdf->Write(8, ' Ja:         ');
      	  $pdf->SetFont('arialbd', '', 8);
          $pdf->Write(8, 'O');
          
          $pdf->SetFont('OpenSans', '', 8);
      	  $pdf->Write(8, '           ');
      	  $pdf->SetFont('arialbd', '', 8);
          $pdf->Write(8, 'O');
          
      	  $pdf->SetFont('OpenSans', '', 8);
      	  
        }
        
        $pdf->Ln();
        
        $intAccountPage++;
        
        $arrExcel[] = $arrExcelLine;
        
        if ((($intAccountPage % 23) == 0) && (($intAccountPage - 1) != count($arrLine))) {
          
          $intAccountPage = 1;
          
          $pdf->AddPage();
          $pdf->SetAutoPageBreak(false);
          
          $pdf->Image('pdf/Vorlage.png', 0, 0, 210, 297, 'PNG'); 
          $pdf->SetMargins(20, 57, 20);
          
          $pdf->SetFont('OpenSans', 'B', 10);
          $pdf->Text(20.4, 58.2, utf8_decode($_REQUEST['Name']));
          
          
          $arrPeriod = explode('_', $_REQUEST['strSelDate']);
          $pdf->SetFont('OpenSans', 'B', 14);
          $pdf->Text(20.4, 79, 'Auskunft nach Abrechnung des Beitragsmonats ' .$arrPeriod[0] .'/' .$arrPeriod[1]);
          
          $pdf->SetFont('OpenSans', 'B', 8);
          $pdf->Text(21, 90, 'BNR');
          $pdf->Text(37, 90, 'Beitragszahler'); //46
          $pdf->Text(130, 90, 'IZS-ID'); //146
          $pdf->Text(148, 90, utf8_decode('Rückstand')); //163
          $pdf->Text(167, 90, utf8_decode('<500') .chr(128)); //182
          $pdf->Text(177, 90, 'SD/RV*'); //146
          
          $pdf->SetDrawColor(0, 0, 0);
          $pdf->SetLineWidth(0.2);
          $pdf->Line(20, 93, 190, 93);
          
          $pdf->SetFont('OpenSans', '', 8);
          $pdf->Cell(20.4, 36, '', 0, 1);

        }

        
      }

      
      //* Stundung oder Ratenzahlungsvereinbarung
      $pdf->SetY(280);
      $pdf->SetFont('OpenSans', '', 5);
      $pdf->Write(5, utf8_decode('* Stundung oder Ratenzahlungsvereinbarung') .chr(10));


    if (count($arrBeiblatt) > 0) {
      
      $pdf->AddPage();
      $pdf->SetAutoPageBreak(false);
      
      $pdf->SetY(25);

      $pdf->SetFont('OpenSans', 'B', 10);
      $pdf->Write(8, 'Absender:' .chr(10));
      $pdf->Write(8, utf8_decode($arrKK[0]['Name']) .chr(10));
      $pdf->SetFont('OpenSans', '', 10);
      $pdf->Write(5, utf8_decode($arrKK[0]['Anfrage_Stra_e__c']) .chr(10));
      $pdf->Write(5, $arrKK[0]['Anfrage_PLZ__c'] .' ' .utf8_decode($arrKK[0]['Anfrage_Ort__c']) .chr(10));
      
      /*
IZS - Institut für Zahlungssicherheit GmbH
Datenverarbeitungscenter A-II/7
Postfach 1119
93067 Neutraubling

      $pdf->Write(5, '' .chr(10));
      $pdf->Write(5, '' .chr(10));
      $pdf->SetFont('OpenSans', 'B', 10);
      $pdf->Write(8, utf8_decode('An: IZS Institut für Zahlungssicherheit GmbH') .chr(10));
      $pdf->SetFont('OpenSans', '', 10);
      $pdf->Write(8, utf8_decode('Würmtalstraße 20a') .chr(10));
      $pdf->Write(8, utf8_decode('81375 München') .chr(10));
      */

      $pdf->Write(5, '' .chr(10));
      $pdf->Write(5, '' .chr(10));
      $pdf->SetFont('OpenSans', 'B', 10);
      $pdf->Write(8, utf8_decode('An: IZS Institut für Zahlungssicherheit GmbH') .chr(10));
      $pdf->SetFont('OpenSans', '', 10);
      $pdf->Write(5, utf8_decode('Datenverarbeitungscenter A-II/7') .chr(10));
      $pdf->Write(5, utf8_decode($strPostfach) .chr(10));
      $pdf->Write(5, utf8_decode('93067 Neutraubling') .chr(10));

      $arrPeriod = explode('_', $_REQUEST['strSelDate']);
      
      $pdf->Write(5, '' .chr(10));
      $pdf->Write(5, '' .chr(10));
      $pdf->SetFont('OpenSans', 'B', 10);
      $pdf->Write(11, 'Auskunft zum Beitragsmonat ' .$arrPeriod[0] .'/' .$arrPeriod[1] .chr(10));
      $pdf->SetFont('OpenSans', '', 10);
      $pdf->Write(11, 'Sehr geehrte Damen und Herren,' .chr(10));
      //$pdf->Write(6, utf8_decode('bezugnehmend auf Ihre Anfrage vom ' .date('d.m.Y') .' möchten wir Sie hiermit über den aktuellen Stand der Beitragszahlungen im Monat ' .$arrMonth[$arrPeriod[0]] .' ' .$arrPeriod[1] .' der Unternehmen der Randstad-Gruppe informieren:') .chr(10));
      $pdf->Write(6, utf8_decode('bezugnehmend auf Ihre Anfrage möchten wir Sie hiermit über den aktuellen Stand der Beitragszahlungen im Monat ' .$arrPeriod[0] .'/' .$arrPeriod[1] .' der Unternehmen der Randstad-Gruppe informieren:') .chr(10));
      
      $pdf->SetFont('OpenSans', 'B', 8);

      $pdf->Text(21, 145, 'BNR');
      $pdf->Text(37, 145, 'Beitragszahler');
      $pdf->Text(130, 145, 'IZS-ID');
      $pdf->Text(148, 145, utf8_decode('Rückstand'));
      $pdf->Text(167, 145, utf8_decode('<500') .chr(128));
      $pdf->Text(177, 145, 'SD/RV*');
      
      $pdf->SetDrawColor(0, 0, 0);
      $pdf->SetLineWidth(0.2);
      $pdf->Line(20, 147, 190, 147);
      
      $pdf->SetFont('OpenSans', '', 8);
      $pdf->Write(8, '' .chr(10));
      $pdf->Write(6, '' .chr(10));
      $pdf->Cell(20.4, 5, '', 0, 1);

      ksort($arrBeiblatt);
      
      //print_r($arrBeiblatt); die();
      $intY = 148;
      $pdf->SetY($intY);

      foreach($arrBeiblatt as $arrRow) {
                
        $pdf->Cell(16, 8, $arrRow[0], 0, 0);
        $pdf->Cell(92, 8, $arrRow[1], 0, 0);
        $pdf->Cell(18, 8, $arrRow[2], 0, 0);
        
        if ($arrRow[3] == 'UBB') {
  	      
      	  $pdf->SetTextColor(255, 0, 0);
      	  $pdf->Cell(25, 8, 'Bitte UBB beilegen', 0, 0);
      	  $pdf->SetTextColor(0, 0, 0);
      	          
  	    } else {

      	  $pdf->SetFont('arialbd', '', 8);
      	  $pdf->Write(8, 'O');
      	  $pdf->SetFont('OpenSans', '', 8);
      	  $pdf->Write(8, ' Nein  ');
      	  $pdf->SetFont('arialbd', '', 8);
      	  $pdf->Write(8, 'O');
      	  $pdf->SetFont('OpenSans', '', 8);
      	  $pdf->Write(8, ' Ja:         ');
      	  $pdf->SetFont('arialbd', '', 8);
      	  $pdf->Write(8, 'O');

          $pdf->SetFont('OpenSans', '', 8);
      	  $pdf->Write(8, '           ');
      	  $pdf->SetFont('arialbd', '', 8);
      	  $pdf->Write(8, 'O');

          $pdf->SetFont('OpenSans', '', 8);
    	  
      	}

        $intY = 156;
        
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetLineWidth(0.2);
        $pdf->Line(20.4, $intY, 189.6, $intY);
      	  
        $pdf->Ln();

      }
      
      $pdf->SetLineWidth(0.2);
      $pdf->SetDrawColor(0, 0, 0);
      $pdf->Rect(20, 170, 80, 70); 
      
      $pdf->SetFont('OpenSans', '', 8);
      $pdf->SetY(232);
      $pdf->MultiCell(80, 3, utf8_decode("Stempel der\n" .$arrKK[0]['Name']), 0, 'C', 0); 
      
      $pdf->SetDrawColor(0, 0, 0);
      $pdf->SetLineWidth(0.2);
      $pdf->Line(105, 182, 190, 182);
      $pdf->Text(107, 185, '(Ort, Datum)');

      $pdf->SetDrawColor(0, 0, 0);
      $pdf->SetLineWidth(0.2);
      $pdf->Line(105, 207, 190, 207);
      $pdf->Text(107, 210, '(Unterschrift)');      

      $pdf->SetDrawColor(0, 0, 0);
      $pdf->SetLineWidth(0.2);
      $pdf->Line(105, 232, 190, 232);
      $pdf->Text(107, 235, '(Funktion des Unterzeichners)');

      $pdf->SetY(250);
      $pdf->SetFont('OpenSans', '', 10);
      $pdf->MultiCell(170, 3, utf8_decode('Alternativ zum Postweg zurück per E-Mail (auskunft@izs-institut.de)'), 0, 'C', 0); 
      

      //* Stundung oder Ratenzahlungsvereinbarung
      $pdf->SetY(280);
      $pdf->SetFont('OpenSans', '', 5);
      $pdf->Write(5, utf8_decode('* Stundung oder Ratenzahlungsvereinbarung') .chr(10));
      
      
    }

      
    }
    
    
  }


  //echo __LINE__; print_r($arrKK); die();

  //CONTENT
  
  //$pdf->Cell(20, 10, $_REQUEST['Name'], 0, 1);
  
  /*
  if (is_array($arrAttachDocs) && (count($arrAttachDocs) > 0)) {
    
    $arrConcat = array();
    
    foreach($arrAttachDocs as $strDoc) {
      if (file_exists($strDoc)) {
        $strDest = TEMP_PATH .md5($strDoc) .'pdf';
        copy($strDoc, $strDest);
        $arrConcat[] = $strDest;
      }
    }
    
    if (count($arrConcat) > 0) {
      $pdf->setFiles($arrConcat);
      $pdf->concat();
      
      foreach($arrConcat as $strFile) {
        unlink($strFile);
      }
      
    }
    
  }
  */

}

/*
$strFileName = 'pdf/anfrage/' .$strId .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_anfrage_sv.pdf';
$pdf->Output(date('Y.m.d_His - ') .'Anfrageliste - ' .$_REQUEST['Name'] .'.pdf', 'D');
*/

if ($_REQUEST['show'] == 'D') {
  
  if (count($arrExcel) > 0) {
  
    //Vorlage ermitteln

    //print_r($arrExcel); die();
    
    //XLSX Schreiben
    error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);
    ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
    date_default_timezone_set('Europe/London');
    
    define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
    
    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/phpexcel/Classes/PHPExcel.php';
    
    $objPHPExcel = new PHPExcel();

    //print_r($_REQUEST); die();
    $arrBnrList = [];
    $strSql = 'SELECT `SF42_Comany_ID__c`, `Betriebsnummer__c` FROM `Account` INNER JOIN `Group__c` ON `Account`.`SF42_Company_Group__c` = `Group__c`.`Id` WHERE `SF42_Group_Type__c` = "Lender" AND `Liefert_Daten__c` = "true";';
    $arrAccList = MySQLStatic::Query($strSql);

    if (count($arrAccList) > 0) {
      foreach ($arrAccList as $intKey => $arrAccRaw) {
        $arrBnrList[$arrAccRaw['SF42_Comany_ID__c']] = $arrAccRaw['Betriebsnummer__c'];
      }
    }

    if ($_REQUEST['AccountId'] != '') {
      $strSql = 'SELECT * FROM `Account` WHERE `Id`= "' .$_REQUEST['AccountId'] .'"';
      $arrIp = MySQLStatic::Query($strSql);
    }

    if (isset($arrIp[0]['PDF_Passwortschutz']) && ($arrIp[0]['PDF_Passwortschutz'] == 'true')) {

      //Set workbook security:
      $objPHPExcel->getSecurity()->setLockWindows(true);
      $objPHPExcel->getSecurity()->setLockStructure(true);
      $objPHPExcel->getSecurity()->setWorkbookPassword($arrIp[0]['PDF_Passwort']);

    }

    /*
    Set workbook security:

    $objPHPExcel->getSecurity()->setLockWindows(true);
    $objPHPExcel->getSecurity()->setLockStructure(true);

    $objPHPExcel->getSecurity()->setWorkbookPassword('secret');
    Set worksheet security:

    $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
    $objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
    $objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
    $objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);

    $objPHPExcel->getActiveSheet()->getProtection()->setPassword('password');
    */
  
    $objPHPExcel->getProperties()->setCreator("Kerstin Mutzel")
    							 ->setLastModifiedBy("Kerstin Mutzel")
    							 ->setTitle("PHPExcel Document from IZS")
    							 ->setSubject("PHPExcel Document")
    							 ->setDescription("Test document for PHPExcel, generated using PHP classes.")
    							 ->setKeywords("")
    							 ->setCategory("Database result file");
    
    //Write Header
    $intExcelRow = 1;
    if ($arrKK[0]['Anfrageliste_XLS__c'] == 'XLS + Vollmacht') { //MOMENTAN (30.11.17) nur AOK Bayern
      $objPHPExcel->setActiveSheetIndex(0)
                  ->setCellValue('A' .$intExcelRow, 'Vorgang-ID')
                  ->setCellValue('B' .$intExcelRow, 'BNR')
                  ->setCellValue('C' .$intExcelRow, 'Arbeitgeber')
                  ->setCellValue('D' .$intExcelRow, 'Konto')
                  ->setCellValue('E' .$intExcelRow, 'Rückstand')
                  ->setCellValue('F' .$intExcelRow, 'EUR > 500')
                  ->setCellValue('G' .$intExcelRow, 'Unbedenklichkeitsbescheinigung')
                  ->setCellValue('H' .$intExcelRow, 'Haupt-Betriebsnummer')
                  ->setCellValue('I' .$intExcelRow, 'Vollmacht-Link');
      $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setBold(true);
    } elseif ($arrKK[0]['Anfrageliste_XLS__c'] == 'Schnittstelle inkl. RV/SD') {						
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' .$intExcelRow, 'Vorgang-ID')
                ->setCellValue('B' .$intExcelRow, 'Betriebsnummer Meldestelle')
                ->setCellValue('C' .$intExcelRow, 'Name Meldestelle')
                ->setCellValue('D' .$intExcelRow, 'Haupt-Betriebsnummer')
                ->setCellValue('E' .$intExcelRow, 'Konto vorhanden?')
                ->setCellValue('F' .$intExcelRow, 'Rückstand')
                ->setCellValue('G' .$intExcelRow, 'Höhe Rückstand');
    if ($arrIp[0]['Id'] == '001300000109k6bAAA') {
      $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('H' .$intExcelRow, 'Stundung u. Ratenzahlung')
        ->setCellValue('I' .$intExcelRow, 'Vollmacht');
      $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setBold(true);
    } else {
      $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('H' .$intExcelRow, 'Stundung o. Ratenzahlung?')
        ->setCellValue('I' .$intExcelRow, 'Vollmacht');
      $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setBold(true);
    }
  } else {
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' .$intExcelRow, 'Vorgang-ID')
                ->setCellValue('B' .$intExcelRow, 'BNR')
                ->setCellValue('C' .$intExcelRow, 'Arbeitgeber')
                ->setCellValue('D' .$intExcelRow, 'Konto')
                ->setCellValue('E' .$intExcelRow, 'Rückstand')
                ->setCellValue('F' .$intExcelRow, 'EUR > 500')
                ->setCellValue('G' .$intExcelRow, 'Unbedenklichkeitsbescheinigung')
                ->setCellValue('H' .$intExcelRow, 'Haupt-Betriebsnummer');
    $objPHPExcel->getActiveSheet()->getStyle("A1:H1")->getFont()->setBold(true);
    }
    
    if ($_REQUEST['AccountId'] != '') {
      $strSql = 'SELECT * FROM `Account` WHERE `Id`= "' .$_REQUEST['AccountId'] .'"';
      $arrIp = MySQLStatic::Query($strSql);
    }

    //Write Data
    foreach ($arrExcel as $intLine => $arrExcelLine) {
      
      $intExcelRow = $intLine + 2;
      
      if ($arrKK[0]['Anfrageliste_XLS__c'] == 'XLS + Vollmacht') { //MOMENTAN (30.11.17) nur AOK Bayern
        $objPHPExcel->setActiveSheetIndex(0)
                  ->setCellValue('A' .$intExcelRow, $arrExcelLine['Vorgang'])
                  ->setCellValueExplicit('B' .$intExcelRow, $arrExcelLine['BNR'], PHPExcel_Cell_DataType::TYPE_STRING)
                  ->setCellValue('C' .$intExcelRow, utf8_encode($arrExcelLine['Arbeitgeber']))
                  ->setCellValue('G' .$intExcelRow, $arrExcelLine['UBB'])
                  ->setCellValue('H' .$intExcelRow, $arrBnrList[$arrExcelLine['BNR']] ?? '');
                
        $strSql = 'SELECT `SF42_Company_Group__c` FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$arrExcelLine['CompanyId'] .'"';
        $arrGroup = MySQLStatic::Query($strSql);

        if (isset($arrIp[0]['PDF_Passwortschutz']) && ($arrIp[0]['PDF_Passwortschutz'] == 'true')) { //Knappschaft
          $strDownloadLink = 'https://www.izs.de/protect/' .$arrPeriod[1] .'/' .$arrPeriod[0] .'/' .$arrGroup[0]['SF42_Company_Group__c'] .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_vollmacht_sv.pdf?id=' .$_REQUEST['AccountId'];
        } else {
          $strDownloadLink = 'https://www.izs.de/download/' .$arrPeriod[1] .'/' .$arrPeriod[0] .'/' .$arrGroup[0]['SF42_Company_Group__c'] .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_vollmacht_sv.pdf';
        }

        //echo $strDownloadLink; die();

        //$objPHPExcel->setActiveSheetIndex(0)->getCell('I' .$intExcelRow)->getHyperlink()->setUrl($strDownloadLink);
        //$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' .$intExcelRow, 'Download')->getHyperlink()->setUrl($strDownloadLink);

        $arrUrlStyle = array(
          'font' => array(
            'color' => array(
              'rgb' => '0000FF'
            ),
            'underline' => 'single'
          )
        );

        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(8, $intExcelRow)->setValueExplicit('Download', \PHPExcel_Cell_DataType::TYPE_STRING2);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(8, $intExcelRow)->getNumberFormat()->setFormatCode('@');
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(8, $intExcelRow)->applyFromArray($arrUrlStyle);
        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(8, $intExcelRow)->getHyperlink()->setUrl($strDownloadLink);        

      } else if ($arrKK[0]['Anfrageliste_XLS__c'] == 'Schnittstelle inkl. RV/SD') {

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' .$intExcelRow, $arrExcelLine['Vorgang'])
                ->setCellValueExplicit('B' .$intExcelRow, $arrExcelLine['BNR'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue('C' .$intExcelRow, utf8_encode($arrExcelLine['Arbeitgeber']))
                ->setCellValue('D' .$intExcelRow, $arrBnrList[$arrExcelLine['BNR']] ?? '');
              
        $strSql = 'SELECT `SF42_Company_Group__c` FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$arrExcelLine['CompanyId'] .'"';
        $arrGroup = MySQLStatic::Query($strSql);

        if (isset($arrIp[0]['PDF_Passwortschutz']) && ($arrIp[0]['PDF_Passwortschutz'] == 'true')) { //Knappschaft
        $strDownloadLink = 'https://www.izs.de/protect/' .$arrPeriod[1] .'/' .$arrPeriod[0] .'/' .$arrGroup[0]['SF42_Company_Group__c'] .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_vollmacht_sv.pdf?id=' .$_REQUEST['AccountId'];
        } else {
        $strDownloadLink = 'https://www.izs.de/download/' .$arrPeriod[1] .'/' .$arrPeriod[0] .'/' .$arrGroup[0]['SF42_Company_Group__c'] .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_vollmacht_sv.pdf';
        }

        //echo $strDownloadLink; die();

        //$objPHPExcel->setActiveSheetIndex(0)->getCell('I' .$intExcelRow)->getHyperlink()->setUrl($strDownloadLink);
        //$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' .$intExcelRow, 'Download')->getHyperlink()->setUrl($strDownloadLink);

        $arrUrlStyle = array(
        'font' => array(
          'color' => array(
            'rgb' => '0000FF'
          ),
          'underline' => 'single'
        )
        );

        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(8, $intExcelRow)->setValueExplicit('Download', \PHPExcel_Cell_DataType::TYPE_STRING2);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(8, $intExcelRow)->getNumberFormat()->setFormatCode('@');
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(8, $intExcelRow)->applyFromArray($arrUrlStyle);
        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(8, $intExcelRow)->getHyperlink()->setUrl($strDownloadLink);   

      } else {
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' .$intExcelRow, $arrExcelLine['Vorgang'])
        ->setCellValueExplicit('B' .$intExcelRow, $arrExcelLine['BNR'], PHPExcel_Cell_DataType::TYPE_STRING)
        ->setCellValue('C' .$intExcelRow, utf8_encode($arrExcelLine['Arbeitgeber']))
        ->setCellValue('G' .$intExcelRow, $arrExcelLine['UBB'])
        ->setCellValue('H' .$intExcelRow, $arrBnrList[$arrExcelLine['BNR']] ?? '');
      }

                

    }

    $objPHPExcel->getActiveSheet()->setTitle('IZS Export');
    $objPHPExcel->setActiveSheetIndex(0);

    //WRITE file
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

    $strCompany = '';
    
    if ($_REQUEST['DezId'] != '') {
      $strCompany = $_REQUEST['DezId'];
    } else {
      $strCompany = $_REQUEST['AccountId'];
    }
    if ($_REQUEST['evid'] != '') {
      $strFileName = __DIR__ .'/pdf/anfrage/' .$strCompany .'_' .$_REQUEST['evid'] .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_anfrage_sv.xlsx';
    } else {
      $strFileName = __DIR__ .'/pdf/anfrage/' .$strCompany .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_anfrage_sv.xlsx';
    }
    
    if (file_exists($strFileName)) {
      unlink($strFileName);
    }
    
    $objWriter->save($strFileName);
      
  }
  
  $strCompany = '';
  
  if ($_REQUEST['DezId'] != '') {
    $strCompany = $_REQUEST['DezId'];
    $strReqType = 'd';
  } else {
    $strCompany = $_REQUEST['AccountId'];
    $strReqType = 'z';
  }

  $arrPeriod = explode('_', $_REQUEST['strSelDate']);

  if ($_REQUEST['evid'] != '') {
    $strFileName = 'pdf/anfrage/' .$strCompany .'_' .$_REQUEST['evid'] .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_anfrage_sv.pdf';
    $strReqType = 'e';
  } else {
    $strFileName = 'pdf/anfrage/' .$strCompany .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_anfrage_sv.pdf';
  }

  /*

function pdfEncrypt ($origFile, $password, $destFile){
//include the FPDI protection http://www.setasign.de/products/pdf-php-solutions/fpdi-protection-128/
require_once('fpdi/FPDI_Protection.php');

$pdf =& new FPDI_Protection();
// set the format of the destinaton file, in our case 6×9 inch
$pdf->FPDF('P', 'in', array('6','9'));

//calculate the number of pages from the original document
$pagecount = $pdf->setSourceFile($origFile);

// copy all pages from the old unprotected pdf in the new one
for ($loop = 1; $loop <= $pagecount; $loop++) {
    $tplidx = $pdf->importPage($loop);
    $pdf->addPage();
    $pdf->useTemplate($tplidx);
}

// protect the new pdf file, and allow no printing, copy etc and leave only reading allowed
$pdf->SetProtection(array(),$password);
$pdf->Output($destFile, 'F');

return $destFile;
}

//password for the pdf file
$password = 'info@domain.com';

//name of the original file (unprotected)
$origFile = 'book.pdf';

//name of the destination file (password protected and printing rights removed)
$destFile ='book_protected.pdf';

//encrypt the book and create the protected file
pdfEncrypt($origFile, $password, $destFile );

  */

  
  if (file_exists($strFileName)) {
    unlink($strFileName);
  }
  $pdf->Output($strFileName, 'F');

  //DELETE NON SENT LIST
  $strReqDel = 'SELECT `sr_id` FROM `izs_sv_request` WHERE `sr_ip` = "' .$_REQUEST['AccountId'] .'" AND `sr_ip_loc` = "' .$_REQUEST['DezId'] .'" AND `sr_evid` = "' .$_REQUEST['evid'] .'" AND `sr_month` = "' .$arrPeriod[0] .'" AND `sr_year` = "' .$arrPeriod[1] .'" AND `sr_sent` = "0000-00-00 00:00:00"';
  $arrReqDel = MySQLStatic::Query($strReqDel);

  if (count($arrReqDel) > 0) {
    $strSqlReqDel1 = 'DELETE FROM `izs_sv_request` WHERE `sr_id` = "' .$arrReqDel[0]['sr_id'] .'"';
    $arrSqlReqDel1 = MySQLStatic::Query($strSqlReqDel1);

    //DELETE NON SENT DETAILS
    $strSqlReqDel2 = 'DELETE FROM `izs_sv_request_det` WHERE `rd_sr_id` = "' .$arrReqDel[0]['sr_id'] .'"';
    $arrSqlReqDel2 = MySQLStatic::Query($strSqlReqDel2);
  }

  //INSERT REQUEST

  $strReq0 = 'INSERT INTO `izs_sv_request` (`sr_id`, `sr_ip`, `sr_ip_loc`, `sr_evid`, `sr_month`, `sr_year`, `sr_created`, `sr_created_clid`, `sr_sent`, `sr_sent_clid`, `sr_ev_num`, `sr_type`, `sr_count`) ';
  $strReq0.= 'VALUES (NULL, "' .$_REQUEST['AccountId'] .'", "' .$_REQUEST['DezId'] .'", "' .$_REQUEST['evid'] .'", "' .$arrPeriod[0] .'", "' .$arrPeriod[1] .'", NOW(), "' .$_REQUEST['clid'] .'", "", "", "' .$intNumEventList .'", "' .$strReqType .'", "' .$intCoutList .'");';
  $intReq0 = MySQLStatic::Insert($strReq0);

  //print_r($arrEventListRequest); die();

  //INSERT REQUEST DETAIL
  foreach ($arrEventListRequest as $intKey => $arrRowReq) {
    $strReq1 = 'INSERT INTO `izs_sv_request_det` (`rd_id`, `rd_sr_id`, `rd_evid`, `rd_ip`, `rd_ip_loc`, `rd_pp`, `rd_pp_group`, `rd_month`, `rd_year`) ';
    $strReq1.= 'VALUES (NULL, "' .$intReq0 .'", "' .$arrRowReq['evid'] .'", "' .$_REQUEST['AccountId'] .'", "' .$_REQUEST['DezId'] .'", "' .$arrRowReq['pp'] .'", "' .$arrRowReq['pp_group'] .'", "' .$arrPeriod[0] .'", "' .$arrPeriod[1] .'");';
    $arrReq1 = MySQLStatic::Query($strReq1);
  }
  
  $strSql = 'SELECT `cr_id` FROM `cms_req` WHERE `cr_account_id` = "' .$strCompany .'" AND `cr_month` = "' .$arrPeriod[0] .'" AND `cr_year` = "' .$arrPeriod[1] .'" AND `cr_evid` = "' .$_REQUEST['evid'] .'"';
  $arrReq = MySQLStatic::Query($strSql);
  
  if (count($arrReq) > 0) {
    $strSql = 'UPDATE `cms_req` SET `cr_created` = NOW() WHERE `cr_id` = "' .$arrReq[0]['cr_id'] .'"';
    MySQLStatic::Update($strSql);
  } else {
    $strSql = 'INSERT INTO `cms_req` (`cr_account_id`, `cr_month`, `cr_year`, `cr_created`, `cr_evid`) VALUES ("' .$strCompany .'", "' .$arrPeriod[0] .'", "' .$arrPeriod[1] .'", NOW(), "' .$_REQUEST['evid'] .'")';
    MySQLStatic::Insert($strSql);
  }

  if ($_REQUEST['strSelWay'] == '') {
    $_REQUEST['strSelWay'] = 'all';
  }
  
  if ($_REQUEST['DezId'] != '') {
    if ($_REQUEST['evid'] != '') {
      header('Location: /cms/index.php?ac=auth_e&send=1&strSelText=' .$_REQUEST['strSelText'] .'&strSelWay=' .$_REQUEST['strSelWay'] .'&strSelRuec=' .$_REQUEST['strSelRuec'] .'&strSelDate=' .$arrPeriod[0] .'_' .$arrPeriod[1]);
    } else {
      header('Location: /cms/index.php?ac=auth_d&send=1&strSelText=' .$_REQUEST['strSelText'] .'&strSelWay=' .$_REQUEST['strSelWay'] .'&strSelRuec=' .$_REQUEST['strSelRuec'] .'&strSelDate=' .$arrPeriod[0] .'_' .$arrPeriod[1]);
    }
  } else {
    if (isset($_REQUEST['ty']) && ($_REQUEST['ty'] == 's')) {
      header('Location: /cms/index.php?ac=auth_s&send=1&strSelText=' .$_REQUEST['strSelText'] .'&strSelWay=' .$_REQUEST['strSelWay'] .'&strSelRuec=' .$_REQUEST['strSelRuec'] .'&strSelDate=' .$arrPeriod[0] .'_' .$arrPeriod[1]);
    } else {
      header('Location: /cms/index.php?ac=auth&send=1&strSelText=' .$_REQUEST['strSelText'] .'&strSelWay=' .$_REQUEST['strSelWay'] .'&strSelRuec=' .$_REQUEST['strSelRuec'] .'&strSelDate=' .$arrPeriod[0] .'_' .$arrPeriod[1]);
    }
  }

}


?>
