<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

$arrCategory = array (
  '', 
  'E-Mail', 
  'Fax', 
  'Post',
  'Webmailer'
);

require_once('../assets/classes/class.mysql.php');

$strOutput = '';

$strOutput.= '<h1>Vorlagen - Anfrage</h1>' .chr(10);

$strOutput.= '<div class="clearfix" style="height: 30px;">' .chr(10);
$strOutput.= '  <div style="float: left; width: 100px;">' .chr(10);
$strOutput.= '    <span class="add"><img src="/cms/img/f_add.png" alt="Hinzufügen" title="Hinzufügen" border="0" /> hinzufügen</span>' .chr(10);
$strOutput.= '  </div><div style="float: left; width: 150px;">' .chr(10);
$strOutput.= '    <span class="showP"><img id="show-info" src="/cms/img/placeholder.png" alt="Liste der Platzhalter" title="Liste der Platzhalter" border="0" /> Platzhalter anzeigen</span>' .chr(10);
$strOutput.= '  </div>' .chr(10);
$strOutput.= '</div>' .chr(10);


$strSql = 'SELECT * FROM `cms_text` WHERE `ct_type` = 0 ORDER BY `ct_name`';
$arrResult = MySQLStatic::Query($strSql);

if (count($arrResult) > 0) {
  
  $strOutput.= '<div style="padding: 5px; width: 617px;" class="ui-widget ui-widget-content ui-corner-all">' .chr(10);
  $strOutput.= '<div style="border: 1px solid #E78F08;">' .chr(10);
  $strOutput.= '<table class="lett" id="tables">' .chr(10);
  $strOutput.= '  <thead>' .chr(10);
  $strOutput.= '    <tr>' .chr(10);
	$strOutput.= '      <th class="ui-widget-content ui-state-default">Vorlage</th>' .chr(10); //class="header"
  $strOutput.= '    </tr>' .chr(10);
  $strOutput.= '  </thead>' .chr(10);
  $strOutput.= '  <tbody>' .chr(10);
  
  $intCount = 1;

  foreach ($arrResult as $arrText) {
    
    if ($intCount % 2 == 0) {
      $strTdClass = 'ui-state-active';
      $strTrClass = 'ui-state-active';
    } else {
      $strTdClass = 'ui-widget-content';
      $strTrClass = 'ui-widget-content';
    }
        
    $strOutput.= '    <tr class="' .$strTrClass .'">' .chr(10);
    $strOutput.= '      <td class="' .$strTdClass .'" id="h_' .$arrText['ct_id'] .'">' .chr(10);
    $strOutput.= '        <span id="sn_' .$arrText['ct_id'] .'" class="head acc-c">' .$arrText['ct_name'] .'</span>' .chr(10);
    $strOutput.= '        <span class="hidden" id="snf_' .$arrText['ct_id'] .'">' .chr(10);
    $strOutput.= '          <a href="createPreview.php?ctId=' .$arrText['ct_id'] .'&strSelDate=' .date('m\_Y') .'&type=Brief"><img src="/cms/img/f_preview.png" alt="Vorschau" title="Vorschau" border="0" /></a>' .chr(10);
    $strOutput.= '          <img id="fc_' .$arrText['ct_id'] .'" class="copy" src="/cms/img/f_copy.png" alt="Kopieren" title="Kopieren" border="0" />' .chr(10);
    $strOutput.= '          <img id="fd_' .$arrText['ct_id'] .'" class="delete" src="/cms/img/f_delete.png" alt="Löschen" title="Löschen" border="0" />' .chr(10);
    $strOutput.= '        </span>' .chr(10);

    $strOutput.= '        <span class="cat"><select id="cat_' .$arrText['ct_id'] .'">' .chr(10);
    
    foreach ($arrCategory as $strCategory) {
      if ($strCategory == $arrText['ct_category']) {
        $strSelected = ' selected="selected"';
      } else {
        $strSelected = '';
      }
      $strOutput.= '          <option value="' .$strCategory .'"' .$strSelected .'>' .$strCategory .'</option>' .chr(10);
    }
    
     
    $strOutput.= '        </select></span>' .chr(10);

    $strOutput.= '      </td>' .chr(10);
    $strOutput.= '    </tr>' .chr(10);

    $strOutput.= '    <tr class="hidden ' .$strTrClass .'" id="name_' .$arrText['ct_id'] .'">' .chr(10);
    $strOutput.= '      <td class="' .$strTdClass .'"><input type="text" name="in_name[' .$arrText['ct_id'] .']" value="' .$arrText['ct_name'] .'" id="in_name_' .$arrText['ct_id'] .'" class="inname"></td>' .chr(10);
    $strOutput.= '    </tr>' .chr(10);

    $strOutput.= '    <tr class="hidden ' .$strTrClass .'" id="head_' .$arrText['ct_id'] .'">' .chr(10);
    $strOutput.= '      <td class="' .$strTdClass .'"><input type="text" name="in_head[' .$arrText['ct_id'] .']" value="' .$arrText['ct_head'] .'" id="in_head_' .$arrText['ct_id'] .'" class="inhead"></td>' .chr(10);
    $strOutput.= '    </tr>' .chr(10);
    
    $strOutput.= '    <tr class="hidden ' .$strTrClass .'" id="text_' .$arrText['ct_id'] .'">' .chr(10);
    $strOutput.= '      <td class="' .$strTdClass .'"><textarea name="in_text[' .$arrText['ct_id'] .']" id="in_text_' .$arrText['ct_id'] .'">' .$arrText['ct_text'] .'</textarea></td>' .chr(10);
    $strOutput.= '    </tr>' .chr(10);
    
    $intCount++;
    
  }
  
  $strOutput.= '  </tbody>' .chr(10);
  $strOutput.= '</table>' .chr(10);
  
        
} else {
  
  $strOutput.= '<p>Keine Anschreiben vorhanden.</p>' .chr(10);
  
}
?>