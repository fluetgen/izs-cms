<?php

session_start();

require_once('../assets/classes/class.mysql.php');

// Set the uplaod directory
$uploadDir = 'server/php/temp/';

// Set the allowed file extensions
$fileTypes = array('csv', 'pdf', 'doc', 'docx', 'xls', 'txt', 'rtf', 'html', 'zip', 'mp3', 
'wma', 'mpg', 'flv', 'avi', 'jpg', 'jpeg', 'png', 'gif', 'odt', 'txt', 'ppt', 'pptx', 'xlsx'); // Allowed file extensions


if ($_REQUEST['id'] != '') { 
  
  foreach (glob($uploadDir .$_REQUEST['id'] .'.*') as $strFilename) {
    unlink($strFilename);
  }
  
  $verifyToken = md5('unique_salt' . $_POST['timestamp']);
  
  if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
  	$tempFile   = $_FILES['Filedata']['tmp_name'];
  	
    $arrParts = explode('.', $_FILES['Filedata']['name']);
    $strExtension = $arrParts[count($arrParts) -1];
    $strNewName = $_REQUEST['id'] .'.' .$strExtension;

  	$targetFile = $uploadDir .$strNewName;
  
  	// Validate the filetype
  	$fileParts = pathinfo($_FILES['Filedata']['name']);
  	if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
  
  		// Save the file
  		move_uploaded_file($tempFile, $targetFile);

      $strSql3 = "DELETE FROM `izs_event_temp` WHERE `ev_id` = '" .$_REQUEST['id'] ."'";
      $resSql3 = MySQLStatic::Query($strSql3);

      $strSql3 = "INSERT INTO `izs_event_temp` ";
      $strSql3.= "(`et_id`, `ev_id`, `et_file`, `et_original`, `cl_id`, `et_time` ";
      $strSql3.= ") VALUES (NULL, '" .$_REQUEST['id'] ."', '" .$strNewName ."', '" .$_FILES['Filedata']['name'] ."', '" .$_SESSION['id'] ."', NOW());";
      $resSql3 = MySQLStatic::Query($strSql3);

  		echo "$tempFile, $targetFile";

  
  	} else {
  
  		// The file type wasn't allowed
  		echo 'Invalid file type.';
  
  	}
  }

}

?>