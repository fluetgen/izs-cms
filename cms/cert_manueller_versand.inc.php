<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

require_once('../assets/classes/class.mysql.php');

$strOutput = '';
$arrCompanyGroupListPayed = array();
$arrCompanyGroupListNotPayed = array();
$arrAccountList = array();

$strOutput.= '<h1>Infoservice</h1>' .chr(10);

$strSql0 = "SELECT DISTINCT `Beitragsmonat__c`, `Beitragsjahr__c` FROM `SF42_IZSEvent__c` ORDER BY `Beitragsjahr__c` DESC, `Beitragsmonat__c` DESC";
$arrResult0 = MySQLStatic::Query($strSql0);
if (count($arrResult0) > 0) {

  $strHash = md5(uniqid(rand(), true));
  
  $strOutput.= '<form method="post" action="">' .chr(10);
  $strOutput.= '<input type="hidden" name="hash" id="hash" value="' .$strHash .'">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);

  $strOutput.= '<table>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Zeitraum: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelDate" onchange="this.form.submit()">' .chr(10);
  
  $intPeriod = 0;

  foreach ($arrResult0 as $arrPeriod0) {
    $strSelected = '';
    $strValue = $arrPeriod0['Beitragsmonat__c'] .'_' .$arrPeriod0['Beitragsjahr__c'];
    
    if (empty($_REQUEST['strSelDate']) && ($intPeriod == 0)) {
      $_REQUEST['strSelDate'] = $strValue;
      $_REQUEST['send'] = 1;
      $arrPeriod = $strValue;
    }
    
    if ($strValue == $_REQUEST['strSelDate']) {
      $strSelected = ' selected="selected"';
    } 
    $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .str_pad($arrPeriod0['Beitragsmonat__c'], 2 ,'0', STR_PAD_LEFT) .'-' .$arrPeriod0['Beitragsjahr__c'] .'</option>' .chr(10);
    $intPeriod++;
  }
  
  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);


  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);
    
}

if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {
  
  $arrPeriod = explode('_', $_REQUEST['strSelDate']);

  $arrCreated = array();
  $strSql = 'SELECT `izs_pruefbericht`.* FROM `izs_pruefbericht`, ';
  $strSql.= '(SELECT `pr_group_id`, MAX(`pr_time`) AS `pr_time` FROM `izs_pruefbericht` GROUP BY `pr_group_id`) `latest` ';
  $strSql.= 'WHERE `izs_pruefbericht`.`pr_group_id` = `latest`.`pr_group_id` ';
  $strSql.= 'AND `izs_pruefbericht`.`pr_time` = `latest`.`pr_time`';
  $arrSql = MySQLStatic::Query($strSql);

  if (count($arrSql) > 0) {

    foreach ($arrSql as $intKey => $arrReport) {

      $arrCreated[$arrReport['pr_group_id']] = '<a href="https://www.izs.de/sv-pruefbericht/' .$arrReport['pr_group_id'] .'" target="_blank" id="cre_' .$arrReport['pr_group_id'] .'">' .date('d.m.Y H:i', strtotime($arrReport['pr_time'])) .'<a>';

    }

  }

  $strSql = 'SELECT * FROM `_cron_url` WHERE `cu_status` = 1 AND `cu_url` LIKE "%sv-pruefbericht%"';
  $arrSql = MySQLStatic::Query($strSql);

  if (count($arrSql) > 0) {

      foreach ($arrSql as $intKey => $arrPending) {

          $arrPart = explode('/', $arrPending['cu_url']);
          $arrCreated[$arrPart[2]] = '<a href="javascript:void(0);" target="" id="cre_' .$arrPart[2] .'">pending...<a>';

      }

  }

  //print_r($arrCreated); die();
  
  $arrGroups = array();
  $arrInformationProvider = array();
  $arrNotPayedGroup = array();
  $arrNotPayedGroupRed = array();
  $arrNotPayedReason = array();

  $strSql2 = 'SELECT `evid`, `SF42_IZSEvent__c`.`Name` AS `EName`, `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c`, ';
  $strSql2.= '`SF42_IZSEvent__c`.`SF42_informationProvider__c`, `Acc`.`Id`, `Acc`.`Name` AS `PPname`, ';
  $strSql2.= '`SF42_IZSEvent__c`.`Betriebsnummer_ZA__c` AS `PPbtnr`, `Account`.`Dezentrale_Anfrage__c`, ';
  $strSql2.= '`Account`.`Name`, `Group__c`.`Id` AS `IdG`, `Group__c`.`Name` AS `NameG`, ';
  $strSql2.= '`Grund__c`, `Naechster_Meilenstein__c`, `bis_am__c`, `SF42_IZSEvent__c`.`Info__c`, `SF42_EventStatus__c` ';
  $strSql2.= 'FROM `SF42_IZSEvent__c` ';
  $strSql2.= 'INNER JOIN `Account` AS `Acc` ON `Acc`.`SF42_Comany_ID__c` = `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c` ';
  $strSql2.= 'INNER JOIN `Group__c` ON `Group__c`.`Id` = `Acc`.`SF42_Company_Group__c` ';
  $strSql2.= 'INNER JOIN `Account` ON `Account`.`Id` = `SF42_IZSEvent__c`.`SF42_informationProvider__c` ';
  $strSql2.= 'WHERE `SF42_Month__c` = "' .$arrPeriod[0] .'" AND `SF42_Year__c` = "' .$arrPeriod[1] .'" ';
  $strSql2.= 'AND `SF42_EventStatus__c` IN ("to enquire", "enquired", "no result", "not OK", "OK")';
  $arrResult2 = MySQLStatic::Query($strSql2);
  
  
  if (count($arrResult2) > 0) {

    foreach ($arrResult2 as $intEventKey => $arrEvent) {

      if ($arrEvent['IdG'] == 'a084S0000004iHSQAY') continue;
      
      if (!in_array($arrEvent['SF42_EventStatus__c'], array('OK')) ) {
        $arrNotPayedGroup[] = $arrEvent['IdG'];
        if ($arrEvent['SF42_EventStatus__c'] == 'not OK') {
          $arrNotPayedGroupRed[] = $arrEvent['IdG'];
        }
        $arrNotPayedReason[$arrEvent['IdG']][] = array(
          'Name' => $arrEvent['PPname'], 
          'Number' => $arrEvent['Betriebsnummer_ZA__c'], 
          'Reason' => $arrEvent['SF42_EventStatus__c']
        );
      }
      
      $arrCompanyGroupList[strtolower($arrEvent['NameG'])] = array(
        'Id' => $arrEvent['IdG'],
        'Name' => $arrEvent['NameG'],
        'PPbtnr' => $arrEvent['PPbtnr']
      );

    }
  }
      
  //print_r($arrNotPayedGroup);

  if (count($arrCompanyGroupList) > 0) {
    ksort($arrCompanyGroupList);
    
//    print_r($arrNotPayedGroup);
//    print_r($arrNotPayedReason);
//    print_r($arrCompanyGroupList); die();
    
    $strOutputTable = '';
    $strOutputTable.= '<table class="" id="5">' .chr(10);
    $strOutputTable.= '  <thead>' .chr(10);
    $strOutputTable.= '    <tr>' .chr(10);
  	$strOutputTable.= '      <th class="header">Company Group</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header">Status</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header">SV-Prüfbericht</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header" nowrap="nowrap">erzeugt am</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header">Empfänger</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header">Perma-Link</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header" nowrap="nowrap">letzter Versand am</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header" nowrap="nowrap">Aktion</th>' .chr(10); //class="header"
    $strOutputTable.= '    </tr>' .chr(10);
    $strOutputTable.= '  </thead>' .chr(10);
    $strOutputTable.= '  <tbody>' .chr(10);

    
    $arrPeriod = explode('_', $_REQUEST['strSelDate']);

    $arrCreateList = array();
    $arrSendList   = array();
    
    foreach ($arrCompanyGroupList as $strKeyName => $arrGroup) {
      
      //print_r($arrGroup); die();

      $strSql = 'SELECT * FROM `cms_cert` WHERE `cc_group_id` = "' .$arrGroup['Id'] .'" AND `cc_month` = "' .$arrPeriod[0] .'" AND `cc_year` = "' .$arrPeriod[1] .'" ORDER BY `cc_id` DESC LIMIT 1';
      $arrCert = MySQLStatic::Query($strSql);
      
      $strOutputTable.= '    <tr>' .chr(10);
      $strOutputTable.= '      <td>' .$arrGroup['Name'] .'</td>' .chr(10);
      
      if (!in_array($arrGroup['Id'], $arrNotPayedGroup) == true) {
        $strOutputTable.= '      <td align="left"><img src="/assets/images/sys/ampel_gruen.png" alt="vollständig" /></td>' .chr(10);
        $strOutputTable.= '      <td align="left"><a href="../sv-pruefbericht/' .$arrGroup['Id'] .'/force" target="_blank" class="create" id="create_' .$arrGroup['Id'] .'">erzeugen</a></td>' .chr(10);
        //$arrCreateList[] = 'createZertifikat.php?Id=' .$arrGroup['Id'] .'&strSelDate=' .$_REQUEST['strSelDate'];
      } elseif (in_array($arrGroup['Id'], $arrNotPayedGroupRed)) {
        $strOutputTable.= '      <td align="left"><img src="/assets/images/sys/ampel_rot.png" alt="Beitragsrückstand" title="' .print_r($arrNotPayedReason[$arrGroup['Id']], true) .'" /></td>' .chr(10);
        $strOutputTable.= '      <td><a href="../sv-pruefbericht/' .$arrGroup['Id'] .'/force" target="_blank" class="create" id="create_' .$arrGroup['Id'] .'">erzeugen</a></td>' .chr(10);
      } else {
        $strOutputTable.= '      <td align="left"><img src="/assets/images/sys/ampel_gelb.png" alt="in Bearbeitung" title="' .print_r($arrNotPayedReason[$arrGroup['Id']], true) .'" /></td>' .chr(10);
        $strOutputTable.= '      <td></td>' .chr(10);
      }
      
      if ($arrCreated[$arrGroup['Id']] != '') {
        $strOutputTable.= '      <td>' .$arrCreated[$arrGroup['Id']] .'</td>' .chr(10);
      } else {
        $strOutputTable.= '      <td><a href="javascript:void(0);" target="" id="cre_' .$arrGroup['Id'] .'"><a></td>' .chr(10);
      }
      
        
      $strSql = 'SELECT `E_Mail__c` FROM `Abonnent__c` WHERE `Verleiher_Gruppe__c` = "' .$arrGroup['Id'] .'" AND `aktiviert__c` = "true"';
      $arrAbo = MySQLStatic::Query($strSql);
      
      if (count($arrAbo) > 0) {        

        $arrRecipientList = array();

        foreach ($arrAbo as $intKey => $arrAboContact) {
          $arrRecipientList[] = $arrAboContact['E_Mail__c'];
        }

        $strOutputTable.= '      <td><textarea style="width: 250px; height: 80px;" class="sel">' .implode(', ', $arrRecipientList) .'</textarea></td>' .chr(10);

      } else {
        $strOutputTable.= '      <td>Kein Abonent</td>' .chr(10);
      }

      $strOutputTable.= '      <td><textarea style="width:250px" class="sel">https://www.izs.de/sv-pruefbericht/' .$arrGroup['PPbtnr'] .'</textarea></td>' .chr(10);
      
      
      if (count($arrCert) > 0) {

        $strOutputTable.= '      <td><span id="sent_' .$arrGroup['Id'] .'">' .date('d.m.Y H:i', strtotime($arrCert[0]['cc_sent'])) .'</span></td>' .chr(10);

        if (!isset($arrCreated[$arrGroup['Id']]) || (strstr($arrCreated[$arrGroup['Id']], 'pending') !== false)) {
          $strOutputTable.= '      <td><a href="javascript:void(0);" id="done_' .$arrGroup['Id'] .'" class="send"></a></td>' .chr(10);
        } else {
          $strOutputTable.= '      <td><a href="javascript:void(0);" id="done_' .$arrGroup['Id'] .'" class="send">habe ich erledigt</a></td>' .chr(10);
        }

      } else {

        $strOutputTable.= '      <td><span id="sent_' .$arrGroup['Id'] .'"></span</td>' .chr(10);

        if (strstr($arrCreated[$arrGroup['Id']], 'pending') !== false) {
          $strOutputTable.= '      <td><a href="javascript:void(0);" id="done_' .$arrGroup['Id'] .'" class="done"></a></td>' .chr(10);
        } else {
          $strOutputTable.= '      <td><a href="javascript:void(0);" id="done_' .$arrGroup['Id'] .'" class="done">habe ich erledigt</a></td>' .chr(10);
        }
        

      }
      
      $strOutputTable.= '    </tr>' .chr(10);

      
    }
    
    $strOutputTable.= '  </tbody>' .chr(10);
    $strOutputTable.= '</table>' .chr(10);   
    
    $strOutput.= '<div class="clearfix" style="height: 48px; width: 1528px;">' .chr(10);
    $strOutput.= '  <div class="form-left">' .chr(10);
    $strOutput.= '    <h3>' .count($arrCompanyGroupList) .' Ergebnisse gefunden:</h3>' .chr(10);
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' .chr(10);
    $strOutput.= '    <form method="post" action="_get_csv.php" style="display: inline; float: right; padding-left: 10px;">';
    $strOutput.= '    <input type="hidden" name="strTable" value="' .str_replace('"', "'", $strOutputTable) .'">' .chr(10);
    $strOutput.= '    <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' .chr(10);
    $strOutput.= '    </form>';
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '</div>' .chr(10);


    $strOutput.= $strOutputTable;
    

    $strOutput.= "
<script>
$('#5').fixheadertable({ 
   colratio    : [350, 80, 120, 120, 266, 266, 130, 150], 
   height      : 500, 
   width       : 1522, 
   zebra       : true, 
   sortable    : true,
   sortType    : ['string', 'string', 'string', 'date', 'string', 'string', 'date', 'string'],
   sortedColId : 0, 
   dateFormat  : 'd.m.Y'
});
</script>

<script>
$('.sel').on('focus', function (e) {
  $(this)
      .one('mouseup', function () {
          $(this).select();
          return false;
      })
      .select();
});

$('.done').on('click', function (e) {

  var id   = $(this).attr('id').split('_')[1];

  $.post('/cms/cert.ajax.inc.php', {
    ac: 'done', id: id, hash: $('#hash').val(), m: " .$arrPeriod[0] .", y: " .$arrPeriod[1] ."
  }).done(function(data) {
    $('#sent_' + id).text(data);
  });

}).bind('mouseover', function() {
  $(this).css('cursor', 'pointer');
});

$('.create').on('click', function (e) {

  e.preventDefault();

  var url  = $(this).attr('href');
  var elem = $(this);
  var id   = $(this).attr('id').split('_')[1];

  $.post('/cms/cert.ajax.inc.php', {
    ac: 'create', url: url, hash: $('#hash').val(), m: " .$arrPeriod[0] .", y: " .$arrPeriod[1] ."
  }).done(function(data) {
    $('#cre_' + id).html('pending...');
    $('#cre_' + id).attr('href', 'javascript:void(0);');
    $('#cre_' + id).attr('target', '');
    $('#send_' + id).text('');
  });

}).bind('mouseover', function() {
  $(this).css('cursor', 'pointer');
});

setInterval(function(){ 

  $.get('/cms/cert.ajax.inc.php', {
    ac: 'status', hash: $('#hash').val(), m: " .$arrPeriod[0] .", y: " .$arrPeriod[1] ."
  }).done(function(data) {
    //console.log(data);

    data = JSON.parse(data);

    $.each(data, function(index, value) {
      
      if ($('#cre_' + index).parent().html() != value) {

        if (value.indexOf('pending') !== -1) {
          $('#send_' + index).text('');
        } else {
          $('#send_' + index).text('habe ich erledigt');
        }

        $('#cre_' + index).parent().html(value);
      
      }
      
    });

  });

}, 10000);

</script>

";

/*
$strOutput.= '<!-- ' .chr(10);
foreach ($arrCreateList as $intKey => $strValue) {
  $strOutput.= '' .$strValue .'' .chr(10);
}

$strOutput.= '' .chr(10);

foreach ($arrSendList as $intKey => $strValue) {
  $strOutput.= '' .$strValue .'' .chr(10);
}
$strOutput.= '-->' .chr(10);
*/
  
  } else {
    
    $strOutput.= '<p>Keine Company Group im gewählten Zeitraum vorhanden.</p>' .chr(10);
    
  }

}

?>