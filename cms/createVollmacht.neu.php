<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

ini_set('memory_limit', '2048M');

session_start();

define('FPDF_FONTPATH','fpdf17/font/');
define('TEMP_PATH', 'temp/');

require_once('../assets/classes/class.mysql.php');
require('fpdf17/fpdf.php');
require('fpdf17/fpdi.php');

if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
  define('APP_PATH', '/data/www/sites/test.izs.de/html/');
} else {
  define('APP_PATH', '/data/www/sites/www.izs.de/html/');
}

//CLASSES
require_once(APP_PATH .'cms/inc/refactor.inc.php');

require_once('classes/class.informationprovider.php');
$objIP = new InformationProvider;

//

/*
$strCompany = '';
  
if ($_REQUEST['DezId'] != '') {
  $strCompany = $_REQUEST['DezId'];
} else {
  $strCompany = $_REQUEST['AccountId'];
}

$arrPeriod = explode('_', $_REQUEST['strSelDate']);

if ($_REQUEST['evid'] != '') {
  $strFileName = 'pdf/vollmacht/' .$strCompany .'_' .$_REQUEST['evid'] .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_vollmacht_sv.pdf';
} else {
  $strFileName = 'pdf/vollmacht/' .$strCompany .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_vollmacht_sv.pdf';
}
if (file_exists($strFileName) && (filesize($strFileName) == 0)) {
  unlink($strFileName);
} elseif (!file_exists($strFileName)) {
  ;
} else {
  exit;
}

*/

$strOutput = '';

$_REQUEST['Name'] = rawurldecode($_REQUEST['Name']);

$intNew = $_REQUEST['new'];

$arrJpg = array();
$arrVollmacht = array();
$arrGroups = array();
$arrInformationProvider = array();

if ($_REQUEST['AccountId'] != '') {
  
  $arrPeriod = explode('_', $_REQUEST['strSelDate']);
  
  $strSql1a = 'SELECT `SF42_Comany_ID__c` FROM `Account` WHERE `Id` = "' .$_REQUEST['AccountId'] .'"';
  $arrResult1a = MySQLStatic::Query($strSql1a);

  if (1) {
  
    //foreach ($arrResult as $arrPeriod) {
    
      $arrDiff = array();
      if ($intNew == 1) {
       	$arrDiff = $objIP->arrGetNewVollmachtList($_REQUEST['AccountId'], $arrPeriod[0], $arrPeriod[1]);
      }
      
      //echo $arrPeriod['Name'] .'<br />' .chr(10);
      
      $strSql2 = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Beitragsmonat__c` = "' .$arrPeriod[0] .'" AND `Beitragsjahr__c` = "' .$arrPeriod[1] .'" AND ';
      $strSql2.= '((`SF42_EventStatus__c` = "enquired") OR (`SF42_EventStatus__c` = "to enquire")) AND `Betriebsnummer_IP__c` = "' .$arrResult1a[0]['SF42_Comany_ID__c'] .'" '; // OR (`SF42_EventStatus__c` = "OK")
      
      if ($_REQUEST['evid'] != '') {
        $strSql2.= 'AND `evid` = "' .$_REQUEST['evid'] .'" ';
      }

      $strSql2.= 'ORDER BY `SF42_Premium_Payer__c`';

      $arrResult2 = MySQLStatic::Query($strSql2);

      if (count($arrResult2) > 0) {
        
        foreach ($arrResult2 as $arrEvent) {
          
          if ((count($arrDiff) > 0) && (!in_array($arrEvent['Betriebsnummer_ZA__c'], $arrDiff))) { continue; }
                   
          //echo '  ' .$arrEvent['Name'] .' -> ' .$arrEvent['SF42_informationProvider__c'] .'<br />' .chr(10);
          
          if ($arrEvent['SF42_informationProvider__c'] == $_REQUEST['AccountId']) {
          
            $strSql3 = 'SELECT * FROM `Account` WHERE `Name` = "' .$arrEvent['SF42_Premium_Payer__c'] .'"';
            $arrResult3 = MySQLStatic::Query($strSql3);
            
            if (count($arrResult3) > 0) {
              
              foreach ($arrResult3 as $arrAccount) {
                //echo '  ' .$arrAccount['Name'] .'<br />' .chr(10);
                
                $strSql4 = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$arrAccount['SF42_Company_Group__c'] .'"';
                $arrResult4 = MySQLStatic::Query($strSql4);
                
                foreach ($arrResult4 as $arrCompanyGroup) {
                  //echo '  ' .$arrCompanyGroup['Name'] .'<br />' .chr(10);
                  
                  if (in_array($arrCompanyGroup['Id'], $arrGroups) == false) {

                    $arrGroups[] = $arrCompanyGroup['Id'];

                    $strSql = 'SELECT * FROM `izs_file` WHERE `if_object` = "' .$arrCompanyGroup['Id'] .'" AND `if_type` = "Vollmacht" AND `if_show` = "true" ORDER BY `if_name` ASC';
                    $arrSql = MySQLStatic::Query($strSql);
                    $intSum = count($arrSql);

                    if ($intSum > 0) {
                      foreach ($arrSql as $intKey => $arrVollmachtRaw) {
                        $arrJpg[] = 'img/vollmacht/' .$arrVollmachtRaw['if_name'];
                      }
                    }

                    //echo '  ' .$arrCompanyGroup['Name'] .'<br />' .chr(10);
                    /*

                    $arrJpg[] = $strLink_00;

                    $strLink_00 = 'img/vollmacht/' .$arrCompanyGroup['Betriebsnummer__c'] .'_Vollmacht.jpg';
                    $strLink_01 = 'img/vollmacht/' .$arrCompanyGroup['Betriebsnummer__c'] .'_Vollmacht_01.jpg';
                    
                    if (file_exists($strLink_00) || file_exists($strLink_01)) {
                      
                      if (file_exists($strLink_01)) {
                        for ($intFile = 1; $intFile < 20; $intFile++) {
                          $strFileMore = 'img/vollmacht/' .$arrCompanyGroup['Betriebsnummer__c'] .'_Vollmacht_' .str_pad($intFile, 2, '0', STR_PAD_LEFT) .'.jpg';
                          if (file_exists($strFileMore)) {
                            $arrJpg[] = $strFileMore;
                          } else {
                            break;            
                          }
                        }                        
                      } else {
                        $arrJpg[] = $strLink_00;
                      }
                    
                      $arrGroups[] = $arrCompanyGroup['Id'];
        
                      $strSql5 = 'SELECT `Id`, `Link_zum_Dokument_jpg__c` FROM `Vollmacht__c` WHERE `Company_Group__c` = "' .$arrCompanyGroup['Id'] .'" AND `aktiviert__c` = "true"';
                      $arrResult5 = MySQLStatic::Query($strSql5);
        
                      $arrVollmacht[] = $arrResult5[0]['Id'];
                    
                    } else {

                      $arrGroups[] = $arrCompanyGroup['Id'];
  
                      $strSql5 = 'SELECT `Id`, `Link_zum_Dokument_jpg__c` FROM `Vollmacht__c` WHERE `Company_Group__c` = "' .$arrCompanyGroup['Id'] .'" AND `aktiviert__c` = "true"';
                      $arrResult5 = MySQLStatic::Query($strSql5);
                      if (count($arrResult5) > 0) {      
                        foreach ($arrResult5 as $arrVollmacht) {
                          
                          if (strstr($arrVollmacht['Link_zum_Dokument_jpg__c'], '.doc') != false) {
                            $arrVollmacht['Link_zum_Dokument_jpg__c'] = 'http://www.izs-institut.de/Vollmacht_130513.jpg';
                          }
                          
                          //print_r($arrVollmacht);
                          if ($_SERVER['REMOTE_ADDR'] == '46.244.185.172') {
                            //echo $arrVollmacht['Link_zum_Dokument_jpg__c'] .'_' .$_SESSION['id'];
                            //die();
                          }
                        
                          $strDest = TEMP_PATH .md5($arrVollmacht['Link_zum_Dokument_jpg__c'] .'_' .$_SESSION['id']) .'.jpg';
                          copy($arrVollmacht['Link_zum_Dokument_jpg__c'], $strDest);
                    
                          $arrJpg[] = $strDest;
                          
                          $arrVollmacht[] = $arrVollmacht['Id'];
  
                          
                        }
                      }
                    }
                    */
                    
                  }
                  
                }
                
              }
              
            }
            
          }
          
          
        }
        
      }
      
      
    //}
  
  }
    
}

if ($_SERVER['REMOTE_ADDR'] == '62.216.207.167') {
  //print_r($arrVollmacht);
  //print_r($arrJpg);
  //die();
}


if ($_REQUEST['DezId'] != '') {

  $arrPeriod = explode('_', $_REQUEST['strSelDate']);

  $strSql7 = 'SELECT * FROM `Anfragestelle__c` WHERE `Id` = "' .$_REQUEST['DezId'] .'"';
  $arrAnfrageListe = MySQLStatic::Query($strSql7);
  
  $strSqlKK = 'SELECT * FROM `Account` WHERE `Id`="' .$arrAnfrageListe[0]['Information_Provider__c'] .'"';
  $arrKK = MySQLStatic::Query($strSqlKK);

  // Techniker Krankenkasse
  // Zip assignment
  if ($arrKK[0]['Id'] == '001300000109k5uAAA') {

    $arrHeader = array(
      'apikey: 9b0d3b1c-27f2-4e40-8e4d-d735a50dbbee',
      'user: ' .$_SESSION['id'],
      'Accept: application/json'
    );
    
    $rarrAssign = curl_get('http://api.izs-institut.de/api/sv/zipassign', array(), array(), $arrHeader);
    $arrResult  = json_decode($rarrAssign, true);
    
    //print_r($arrResult); die();

  }
  //die();

  $strSqlCA = 'SELECT * FROM `Anfragestelle__c` WHERE `Information_Provider__c`="' .$arrKK[0]['Id'] .'" AND `CATCH_ALL__c` = "true"';
  $arrCatchAll = MySQLStatic::Query($strSqlCA);


  if ($_REQUEST['DezId'] == $arrCatchAll[0]['Id']) {

    $strSql6 = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Beitragsmonat__c` = "' .$arrPeriod[0] .'" AND `Beitragsjahr__c` = "' .$arrPeriod[1] .'" AND ';
    $strSql6.= '((`SF42_EventStatus__c` = "enquired") OR (`SF42_EventStatus__c` = "to enquire")) ';
    $strSql6.= 'AND `SF42_informationProvider__c` = "' .$arrKK[0]['Id'] .'" ';
    if ($_REQUEST['evid'] != '') {
      $strSql6.= 'AND `evid` = "' .$_REQUEST['evid'] .'" ';
    }
    $strSql6.= 'GROUP BY `Betriebsnummer_ZA__c` ';
    $strSql6.= 'ORDER BY `SF42_Premium_Payer__c`';
    $arrEventList = MySQLStatic::Query($strSql6);

    if ($_SERVER['REMOTE_ADDR'] == '62.216.207.167') {
      //echo $strSql6;
    }

    foreach ($arrEventList as $intEv => $arrEvent) {

      $strSqlAc = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c`="' .$arrEvent['Betriebsnummer_ZA__c'] .'"';
      $arrAc = MySQLStatic::Query($strSqlAc);

      $strSql8 = 'SELECT `Id` FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrAc[0]['Id'] .'" AND `Information_Provider__c` = "' .$arrKK[0]['Id'] .'"';
      $arrResult8 = MySQLStatic::Query($strSql8);
      
      //keinem Team zugeordnet
      if (count($arrResult8) == 0) {

        if ($_SERVER['REMOTE_ADDR'] == '193.174.158.110') {
          //echo $arrEvent['Name'] .' -> ' .$arrCatchAll[0]['Name'] .'<br />' .chr(10);
        }

        $strSql4 = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$arrAc[0]['SF42_Company_Group__c'] .'"';
        $arrResult4 = MySQLStatic::Query($strSql4);
        
        foreach ($arrResult4 as $arrCompanyGroup) {
          //echo '  ' .$arrCompanyGroup['Name'] .'<br />' .chr(10);
          
          if (in_array($arrCompanyGroup['Id'], $arrGroups) == false) {
               
            $arrGroups[] = $arrCompanyGroup['Id'];

            $strSql = 'SELECT * FROM `izs_file` WHERE `if_object` = "' .$arrCompanyGroup['Id'] .'" AND `if_type` = "Vollmacht" AND `if_show` = "true" ORDER BY `if_name` ASC';
            $arrSql = MySQLStatic::Query($strSql);
            $intSum = count($arrSql);

            if ($intSum > 0) {
              foreach ($arrSql as $intKey => $arrVollmachtRaw) {
                $arrJpg[] = 'img/vollmacht/' .$arrVollmachtRaw['if_name'];
              }
            }

            /*
            $arrJpg[] = $strLink_00;

            $strLink_00 = 'img/vollmacht/' .$arrCompanyGroup['Betriebsnummer__c'] .'_Vollmacht.jpg';
            $strLink_01 = 'img/vollmacht/' .$arrCompanyGroup['Betriebsnummer__c'] .'_Vollmacht_01.jpg';

            if (file_exists($strLink_00) || file_exists($strLink_01)) {
              
              if (file_exists($strLink_01)) {
                for ($intFile = 1; $intFile < 20; $intFile++) {
                  $strFileMore = 'img/vollmacht/' .$arrCompanyGroup['Betriebsnummer__c'] .'_Vollmacht_' .str_pad($intFile, 2, '0', STR_PAD_LEFT) .'.jpg';
                  if (file_exists($strFileMore)) {
                    $arrJpg[] = $strFileMore;
                  } else {
                    break;            
                  }
                }                        
              } else {
                $arrJpg[] = $strLink_00;
              }
          
              $arrGroups[] = $arrCompanyGroup['Id'];

              $strSql5 = 'SELECT `Id`, `Link_zum_Dokument_jpg__c` FROM `Vollmacht__c` WHERE `Company_Group__c` = "' .$arrCompanyGroup['Id'] .'" AND `aktiviert__c` = "true"';
              $arrResult5 = MySQLStatic::Query($strSql5);

              $arrVollmacht[] = $arrResult5[0]['Id'];
            
            } else {

              $arrGroups[] = $arrCompanyGroup['Id'];
    
              $strSql5 = 'SELECT `Id`, `Link_zum_Dokument_jpg__c` FROM `Vollmacht__c` WHERE `Company_Group__c` = "' .$arrCompanyGroup['Id'] .'" AND `aktiviert__c` = "true"';
              $arrResult5 = MySQLStatic::Query($strSql5);
              if (count($arrResult5) > 0) {      
                foreach ($arrResult5 as $arrVollmacht) {
                
                  $strDest = TEMP_PATH .md5($arrVollmacht['Link_zum_Dokument_jpg__c']) .'.jpg';
                  copy($arrVollmacht['Link_zum_Dokument_jpg__c'], $strDest);
            
                  $arrJpg[] = $strDest;
                  
                  $arrVollmacht[] = $arrVollmacht['Id'];
    
                  
                }
              }
              
            }
            */
  
          }
          
        }
      }


    }

    
  }

  $arrPeriod = explode('_', $_REQUEST['strSelDate']);
  
  $arrGroups = array();
  $arrInformationProvider = array();

  $strSql4 = 'SELECT * FROM `Dezentrale_Anfrage_KK__c` WHERE `Anfragestelle__c` = "' .$_REQUEST['DezId'] .'"';
  $arrDezAnfrageListe = MySQLStatic::Query($strSql4);
  
  if ($_SERVER['REMOTE_ADDR'] == '82.135.80.73') {
    //print_r($arrDezAnfrageListe); die();
  }
  
  foreach ($arrDezAnfrageListe as $intDezAnf => $arrDezAnfrage) {
    
    $strSql5 = 'SELECT * FROM `Account` WHERE `Id` = "' .$arrDezAnfrage['Premium_Payer__c'] .'" ORDER BY `Name`';
    $arrAccountList = MySQLStatic::Query($strSql5);
    
    //print_r($arrAccountList); die();

    $strSql6 = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Beitragsmonat__c` = "' .$arrPeriod[0] .'" AND `Beitragsjahr__c` = "' .$arrPeriod[1] .'" AND ';
    $strSql6.= '((`SF42_EventStatus__c` = "enquired") OR (`SF42_EventStatus__c` = "to enquire")) ';
    $strSql6.= 'AND `Betriebsnummer_ZA__c` = "' .$arrAccountList[0]['SF42_Comany_ID__c'] .'" AND `SF42_informationProvider__c` = "' .$arrDezAnfrage['Information_Provider__c'] .'" ';
    if ($_REQUEST['evid'] != '') {
      $strSql6.= 'AND `evid` = "' .$_REQUEST['evid'] .'"';
    }
    $arrEventList = MySQLStatic::Query($strSql6);
    
    if (count($arrEventList) > 0) {

      $strSql4 = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$arrAccountList[0]['SF42_Company_Group__c'] .'"';
      $arrResult4 = MySQLStatic::Query($strSql4);
      
      foreach ($arrResult4 as $arrCompanyGroup) {
        //echo '  ' .$arrCompanyGroup['Name'] .'<br />' .chr(10);
        
        if (in_array($arrCompanyGroup['Id'], $arrGroups) == false) {

          $arrGroups[] = $arrCompanyGroup['Id'];

          $strSql = 'SELECT * FROM `izs_file` WHERE `if_object` = "' .$arrCompanyGroup['Id'] .'" AND `if_type` = "Vollmacht" AND `if_show` = "true" ORDER BY `if_name` ASC';
          $arrSql = MySQLStatic::Query($strSql);
          $intSum = count($arrSql);

          if ($intSum > 0) {
            foreach ($arrSql as $intKey => $arrVollmachtRaw) {
              $arrJpg[] = 'img/vollmacht/' .$arrVollmachtRaw['if_name'];
            }
          }
          
          //echo '  ' .$arrCompanyGroup['Name'] .'<br />' .chr(10);
          /*
          $strLink_00 = 'img/vollmacht/' .$arrCompanyGroup['Betriebsnummer__c'] .'_Vollmacht.jpg';
          $strLink_01 = 'img/vollmacht/' .$arrCompanyGroup['Betriebsnummer__c'] .'_Vollmacht_01.jpg';
          
          if (file_exists($strLink_00) || file_exists($strLink_01)) {
            
            if (file_exists($strLink_01)) {
              for ($intFile = 1; $intFile < 20; $intFile++) {
                $strFileMore = 'img/vollmacht/' .$arrCompanyGroup['Betriebsnummer__c'] .'_Vollmacht_' .str_pad($intFile, 2, '0', STR_PAD_LEFT) .'.jpg';
                if (file_exists($strFileMore)) {
                  $arrJpg[] = $strFileMore;
                } else {
                  break;            
                }
              }                        
            } else {
              $arrJpg[] = $strLink_00;
            }
                        
            $arrGroups[] = $arrCompanyGroup['Id'];

            $strSql5 = 'SELECT `Id`, `Link_zum_Dokument_jpg__c` FROM `Vollmacht__c` WHERE `Company_Group__c` = "' .$arrCompanyGroup['Id'] .'" AND `aktiviert__c` = "true"';
            $arrResult5 = MySQLStatic::Query($strSql5);

            $arrVollmacht[] = $arrResult5[0]['Id'];
          
          } else {
            
            $arrGroups[] = $arrCompanyGroup['Id'];
  
            $strSql5 = 'SELECT `Id`, `Link_zum_Dokument_jpg__c` FROM `Vollmacht__c` WHERE `Company_Group__c` = "' .$arrCompanyGroup['Id'] .'" AND `aktiviert__c` = "true"';
            $arrResult5 = MySQLStatic::Query($strSql5);
            if (count($arrResult5) > 0) {      
              foreach ($arrResult5 as $arrVollmacht) {
              
                $strDest = TEMP_PATH .md5($arrVollmacht['Link_zum_Dokument_jpg__c']) .'.jpg';
                copy($arrVollmacht['Link_zum_Dokument_jpg__c'], $strDest);
          
                $arrJpg[] = $strDest;
                
                $arrVollmacht[] = $arrVollmacht['Id'];
  
                
              }
            }
          }
          
          */
          
        }
        
      }



    }
    
  }

}

if ($_SERVER['REMOTE_ADDR'] == '62.216.207.167') {
  //print_r($arrVollmacht);
  //print_r($arrJpg);
  //die();
}


class concat_pdf extends fpdi {
    var $files = array();
    function concat_pdf($orientation='P',$unit='mm',$format='A4') {
        parent::__construct($orientation,$unit,$format);
    }
    function setFiles($files) {
        $this->files = $files;
    }
    function concat() {
        foreach($this->files AS $file) {
            $pagecount = $this->setSourceFile($file);
            for ($i = 1;  $i <= $pagecount;  $i++) {
                 $tplidx = $this->ImportPage($i);
                 $this->AddPage();
                 $this->useTemplate($tplidx);
            }
        }
    }
}

if (1) {
  
  // INIT -> /download/
  if (!file_exists('../download')) {
    mkdir('../download', 0777, true);
  }

  // YEAR -> /download/2017/
  if (!file_exists('../download/' .$arrPeriod[1])) {
    mkdir('../download/' .$arrPeriod[1], 0777, true);
  }

  // MONTH -> /download/2017/10/
  if (!file_exists('../download/' .$arrPeriod[1] .'/' .$arrPeriod[0])) {
    mkdir('../download/' .$arrPeriod[1] .'/' .$arrPeriod[0], 0777, true);
  }

  //authority
  $strPathAuthority = '../download/' .$arrPeriod[1] .'/' .$arrPeriod[0] .'/';

  $arrDone = array();

  //print_r($arrJpg); die();
  
  foreach($arrJpg as $strFile) {

    $arrFileName = explode('/', $strFile);
    $arrCompanyId = explode('_', $arrFileName[count($arrFileName) - 1]);

    $strSql = 'SELECT `SF42_Company_Group__c` FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$arrCompanyId[0] .'"';
    $arrGroup = MySQLStatic::Query($strSql);
    
    if (!in_array($arrGroup[0]['SF42_Company_Group__c'], $arrDone)) {

      if (count($arrDone) > 0) {

        if (file_exists($strFileName)) {
          //echo 'DELETE FILE: ' .$strFileName .'<br>' .chr(10);
          unlink($strFileName);
        }
        //echo 'WRITE FILE: ' .$strFileName .'<br>' .chr(10);
        $pdf->Output($strFileName, 'F');

      }
      
      $arrDone[] = $arrGroup[0]['SF42_Company_Group__c'];

      $strFileName = $strPathAuthority .'' .$arrGroup[0]['SF42_Company_Group__c'] .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_vollmacht_sv.pdf';

      //echo 'CREATE FILE: ' .$strFileName .'<br>' .chr(10);
      $pdf = new concat_pdf();
      $pdf->AliasNbPages();
    
    }

    $pdf->AddPage();
    $pdf->SetAutoPageBreak(false);
    $pdf->Image($strFile, 0, 0, 210, 297, 'JPG');
    //echo 'ADD PAGE: ' .$strFile .' TO: ' .$strFileName .'<br>' .chr(10);
    
  }

  if (file_exists($strFileName)) {
    //echo 'DELETE FILE: ' .$strFileName .'<br>' .chr(10);
    unlink($strFileName);
  }
  //echo 'WRITE FILE: ' .$strFileName .'<br>' .chr(10);

  //var_dump($pdf); die();

  $pdf->Output($strFileName, 'F');
  
  //die();

}

$pdf = new concat_pdf();
$pdf->AliasNbPages();

foreach($arrJpg as $strFile) {
  $pdf->AddPage();
  $pdf->SetAutoPageBreak(false);
  $pdf->Image($strFile, 0, 0, 210, 297, 'JPG'); 
  //unlink($strFile);
}

if ($_REQUEST['show'] == 'C') {
  
  $strCompany = '';
  
  if ($_REQUEST['DezId'] != '') {
    $strCompany = $_REQUEST['DezId'];
  } else {
    $strCompany = $_REQUEST['AccountId'];
  }

  $arrPeriod = explode('_', $_REQUEST['strSelDate']);

  if ($_REQUEST['evid'] != '') {
    $strFileName = 'pdf/vollmacht/' .$strCompany .'_' .$_REQUEST['evid'] .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_vollmacht_sv.pdf';
  } else {
    $strFileName = 'pdf/vollmacht/' .$strCompany .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_vollmacht_sv.pdf';
  }
  if (file_exists($strFileName)) {
    unlink($strFileName);
  }
  $pdf->Output($strFileName, 'F');

  if ($_SERVER['REMOTE_ADDR'] == '62.216.207.167') {

    //echo $strFileName;

  }
  
  $strSql = 'SELECT `ca_id` FROM `cms_auth` WHERE `ca_account_id` = "' .$strCompany .'" AND `ca_month` = "' .$arrPeriod[0] .'" AND `ca_year` = "' .$arrPeriod[1] .'" ';
  if ($_REQUEST['evid'] != '') {
    $strSql.= 'AND `ca_evid` = "' .$_REQUEST['evid'] .'"';
  } 
  $arrCert = MySQLStatic::Query($strSql);
  
  if (count($arrCert) > 0) {
    $strSql = 'UPDATE `cms_auth` SET `ca_created` = NOW() WHERE `ca_id` = "' .$arrCert[0]['ca_id'] .'"';
    MySQLStatic::Update($strSql);
  } else {
    $strSql = 'INSERT INTO `cms_auth` (`ca_account_id`, `ca_month`, `ca_year`, `ca_created`, `ca_evid`) VALUES ("' .$strCompany .'", "' .$arrPeriod[0] .'", "' .$arrPeriod[1] .'", NOW(), "' .$_REQUEST['evid'] .'")';
    MySQLStatic::Insert($strSql);
  }

  if ($_REQUEST['strSelWay'] == '') {
    $_REQUEST['strSelWay'] = 'all';
  }
  

  if ($_SERVER['REMOTE_ADDR'] != '0') {
    if ($_REQUEST['DezId'] != '') {
      if ($_REQUEST['evid'] != '') {
        header('Location: /cms/index.php?ac=auth_e&send=1&strSelWay=' .$_REQUEST['strSelWay'] .'&strSelText=' .$_REQUEST['strSelText'] .'&strSelRuec=' .$_REQUEST['strSelRuec'] .'&strSelDate=' .$arrPeriod[0] .'_' .$arrPeriod[1]);
      } else {
        header('Location: /cms/index.php?ac=auth_d&send=1&strSelWay=' .$_REQUEST['strSelWay'] .'&strSelText=' .$_REQUEST['strSelText'] .'&strSelRuec=' .$_REQUEST['strSelRuec'] .'&strSelDate=' .$arrPeriod[0] .'_' .$arrPeriod[1]);
      }
    } else {
      if (isset($_REQUEST['ty']) && ($_REQUEST['ty'] == 's')) {
        header('Location: /cms/index.php?ac=auth_s&send=1&strSelWay=' .$_REQUEST['strSelWay'] .'&strSelText=' .$_REQUEST['strSelText'] .'&strSelRuec=' .$_REQUEST['strSelRuec'] .'&strSelDate=' .$arrPeriod[0] .'_' .$arrPeriod[1]);
      } else {
        header('Location: /cms/index.php?ac=auth&send=1&strSelWay=' .$_REQUEST['strSelWay'] .'&strSelText=' .$_REQUEST['strSelText'] .'&strSelRuec=' .$_REQUEST['strSelRuec'] .'&strSelDate=' .$arrPeriod[0] .'_' .$arrPeriod[1]);
      }
    }
  }

} else {
  if ($_REQUEST['evid'] != '') {
    $pdf->Output(date('Y.m.d_His - ') .'Vollmacht - ' .$_REQUEST['Name'] .' - ' .$_REQUEST['evid'] .'.pdf', 'D');
  } else {
    $pdf->Output(date('Y.m.d_His - ') .'Vollmacht - ' .$_REQUEST['Name'] .'.pdf', 'D');
  }
}


?> 