<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

require_once('../assets/classes/class.mysql.php');

$strOutput = '';
$arrRequestWay = array();
$arrDecentral = array();

$strOutput.= '<h1>Vollmachten</h1>' .chr(10);


$strUrl = 'https://www.izs-institut.de/api/realfile/?active=1&type=Vollmacht'; //to=' .date('Y-m-d') .'
$strJson = file_get_contents($strUrl);
$arrRequest = json_decode($strJson, true);
$arrFileList = $arrRequest['content']['realfile'];

$arrVollmachtList = array();
if (count($arrFileList) > 0) {

  foreach ($arrFileList as $intKey => $arrFile) {
    $arrVollmachtList[$arrFile['verid']][] = 'img/vollmacht/' .$arrFile['name'];
  }

}

$strUrl = 'https://www.izs-institut.de/api/ver/?data=1'; //to=' .date('Y-m-d') .'
$strJson = file_get_contents($strUrl);
$arrRequest = json_decode($strJson, true);
$arrVerList = $arrRequest['content']['company'];


if (count($arrVerList) > 0) {
  
  $strList = '';
  $intSum = 0;
  $intAury = 0;
  $intDisplay = 0;
  
  foreach ($arrVerList as $arrGroup) {
    if (isset($arrVollmachtList[$arrGroup['id']])) {
      $intAury++;
    }
    $intSum++;
  }
  
  foreach ($arrVerList as $arrGroup) {

    if (isset($_REQUEST['strSelAury']) && ($_REQUEST['strSelAury'] == 'miss')) {
      if (isset($arrVollmachtList[$arrGroup['id']])) continue;
    }

    if (isset($_REQUEST['strSelAury']) && ($_REQUEST['strSelAury'] == 'avail')) {
      if (!isset($arrVollmachtList[$arrGroup['id']])) continue;
    }    

    $strList.= '    <tr>' .chr(10);
    $strList.= '      <td>' .$arrGroup['name'] .'</td>' .chr(10);
    $strList.= '      <td>' .$arrGroup['btnr'] .'</td>' .chr(10);
    $strList.= '      <td align="center">';

    if (isset($arrVollmachtList[$arrGroup['id']])) {
      
      foreach ($arrVollmachtList[$arrGroup['id']] as $intKey => $strVollmachtLink) {
        $strList.= '<a href="' .$strVollmachtLink .'" target="_blank"><img src="img/icon_pdf.png" alt="pdf" style="padding-right: 3px;" /></a>';
      }

    } 

    $intDisplay++;
  
  }

  $strList.= '      </td>' .chr(10);
  $strList.= '    </tr>' .chr(10);


  $strOutput.= '<form method="post" action="">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);

  $strOutput.= '<table>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Vollmacht: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelAury" id="strSelAury" onchange="this.form.submit()">' .chr(10);
  
  $intPeriod = 0;

  $strSelectedAll = '';
  $strSelectedAvail = '';
  $strSelectedMiss = '';
  
  if ($_REQUEST['strSelAury'] == 'all') {
    $strSelectedAll = ' selected="selected"';
  }
  if ($_REQUEST['strSelAury'] == 'avail') {
    $strSelectedAvail = ' selected="selected"';
  }
  if ($_REQUEST['strSelAury'] == 'miss') {
    $strSelectedMiss = ' selected="selected"';
  }
  
  $strOutput.= '  <option value="">Bitte auswählen</option>' .chr(10);
  $strOutput.= '  <option value="all"' .$strSelectedAll .'>alle (' .count($arrVerList) .')</option>' .chr(10);
  $strOutput.= '  <option value="avail"' .$strSelectedAvail .'>vorhanden (' .$intAury .')</option>' .chr(10);
  $strOutput.= '  <option value="miss"' .$strSelectedMiss .'>fehlt (' .($intSum - $intAury) .')</option>' .chr(10);
  
  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);
    
}

if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1) && ($_REQUEST['strSelAury'] != '')) {
  
  if ($intDisplay > 0) {
  
    $strOutput.= '<p>&nbsp;</p>' .chr(10);
    $strOutput.= '<table class="" id="5">' .chr(10);
    $strOutput.= '  <thead>' .chr(10);
    $strOutput.= '    <tr>' .chr(10);
  	$strOutput.= '      <th class="header">Company Group</th>' .chr(10); //class="header"
  	$strOutput.= '      <th class="header">Btnr.</th>' .chr(10); //class="header"
  	$strOutput.= '      <th class="header">Vollmacht</th>' .chr(10); //class="header"
    $strOutput.= '    </tr>' .chr(10);
    $strOutput.= '  </thead>' .chr(10);
    $strOutput.= '  <tbody>' .chr(10);
    
    $strOutput.= $strList;
    
    $strOutput.= '  </tbody>' .chr(10);
    $strOutput.= '</table>' .chr(10);   

    $strOutput.= "
<script>
$('#5').fixheadertable({ 
   colratio    : [400, 80, 200], 
   height      : 500, 
   width       : 705, 
   zebra       : true, 
   sortable    : true,
   sortType    : ['string', 'string', 'string'],
   sortedColId : 0, 
   dateFormat  : 'Y-m'
});
</script>
";

          
  } else {
    
    $strOutput.= '<p>Keine Company Group vorhanden.</p>' .chr(10);
    
  }

}

?>