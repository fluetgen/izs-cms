<?php

namespace Main\Qrcode;

include (DIR_APPS .'qrcode/qrcode.class.inc.php');

$arrReturn = array();

//$strMethod (GET/POST/PUT/DELETE)
//$arrPost
//$arrGet
//$arrRequest

$objQrcode = new Qrcode;

$boolBinary = false;

if ($strMethod == 'GET') {

    if (count($arrRequest) >= 1) {

        if ($arrRequest[1] == 'create') {

            if (isset($arrGet['input'])) {

                $boolBinary = true;
                $strBinary = $objQrcode->createQrCode($arrGet['input'], 'png', 'binary');

            }

        } 

    }

}

if ($strMethod == 'POST') {

}

if ($strMethod == 'DELETE') {

}

if ($boolBinary) {
    $strReturn = $strBinary;
} else {
    $strReturn = json_encode($arrReturn);
}

?>