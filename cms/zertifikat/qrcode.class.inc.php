<?php

namespace Main\Qrcode;

include(DIR_MODULES .'phpqrcode/classes/phpqrcode.class.php');

class Qrcode {

    function createQrCode ($strInput = '', $strType = 'png', $strOutput = 'binary') {

        $strReturn = '';

        if ($strInput != '') {

            $strUid  = md5(uniqid(rand(), true));
            $strPath = DIR_TEMP .$strUid .'.png';

            \QRcode::png($strInput, $strPath);

            $strReturn = file_get_contents($strPath);

            unlink($strPath);

        }

        return $strReturn;

    }

}

?>