<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

if ($boolStandAlone === true) {

    require_once('../const.inc.php');
    require_once('../../assets/classes/class.mysql.php');

    require_once('../inc/refactor.inc.php');

    require_once ('../fpdf182/fpdf.php');
    require_once ('../fpdf182/class.pdfhtml.php');

}

$arrMonthList = array(
    1 => 'Januar',
    2 => 'Februar',
    3 => 'März',
    4 => 'April',
    5 => 'Mai',
    6 => 'Juni',
    7 => 'Juli',
    8 => 'August',
    9 => 'September',
    10 => 'Oktober',
    11 => 'November',
    12 => 'Dezember'
);

$intDurationTime = microtime(true) - $intBeginTime;
if ($_REQUEST['d'] == 't') echo " Include: $intDurationTime Sek." .chr(10);

// INPUT
if (!isset($strGroupId) || ($strGroupId == '')) {
    //http_response_code(404);
    //include('my_404.php'); // provide your own HTML for the error page
    //die();
} 

function replaceCharacter($text, $searchChar, $replaceChar, $occurrenceArray) {
    $count = 0;
    
    $searchLength = strlen($searchChar);
    $replaceLength = strlen($replaceChar);
    
    for ($i = 0; $i < strlen($text); $i++) {
        if (substr($text, $i, $searchLength) == $searchChar) {
            $count++;
            
            if (in_array($count, $occurrenceArray)) {
                $text = substr_replace($text, $replaceChar, $i, $searchLength);
                $i += $replaceLength - 1;
            }
        }
    }
    
    return $text;
}

function calculateLineBreak ($strText = '', $floatWidth = 0, $objPdf) {

    $strFinalText = $strText;

    $arrPartList = explode('-', $strText);

    if ((count($arrPartList) > 0) && (!is_null($objPdf))) {

        $floatHeight1 = $objPdf->GetMultiCellHeight($floatWidth, 6, $strText, 0, 'L');
        $floatHeight2 = $objPdf->GetMultiCellHeight($floatWidth, 6, replaceCharacter($strText, '-', '-' .chr(10), [1]), 0, 'L');

        if ($floatHeight2 < $floatHeight1) {
            if (count($arrPartList) > 1) {
                $floatHeight3 = $objPdf->GetMultiCellHeight($floatWidth, 6, replaceCharacter($strText, '-', '-' .chr(10), [2]), 0, 'L');
                if ($floatHeight3 <= $floatHeight2) {
                    $strFinalText = replaceCharacter($strText, '-', '-' .chr(10), [2]);
                } else {
                    $strFinalText = replaceCharacter($strText, '-', '-' .chr(10), [1]);
                }
            } else {
                $strFinalText = replaceCharacter($strText, '-', '-' .chr(10), [1]);
            }
        }

    }

    return $strFinalText;

}

function boolIsUnique ($strUnique = '') {

    $strSql = 'SELECT `ed_id` FROM `izs_event_download` WHERE `ed_hash` = "' .$strUnique .'"';
    $arrSql = MySQLStatic::Query($strSql);
    
    if (count($arrSql) == 0) {

        return $strUnique;

    } else {

        boolIsUnique(uniqid());

    }

}

function createCert ($intEventId = 0, &$objPdf = null, $strReturnType = 'string', $arrGroupSelected = array()) {

    global $intBeginTime, $arrPdfLinkListEvent, $arrSql, $arrMonthList, $boolCreate, $intCreateUser, $strKuendigung;

    if (count($arrGroupSelected) > 0) {
        $arrSql = $arrGroupSelected;
    }


    if (!isset($strReturnType)) {
        $strReturnType = 'string';
    }

    // OUTPUT
    $strOutput = '';

    $intDurationTime = microtime(true) - $intBeginTime;
    if ($_REQUEST['d'] == 't') echo " Select: $intDurationTime Sek." .chr(10);

    //print_r($arrSql); die();

    if (count($arrSql) > 0) {

        $boolStandAlone = false;


        if (!isset($objPdf)) {

            $boolStandAlone = true;
        }

        if ($boolCreate === true) {

            $strUnique = boolIsUnique(uniqid());

            $arrData = array(
                'name'   => $arrSql[0]['Name'],
                'street' => $arrSql[0]['Strasse__c'],
                'zip'    => $arrSql[0]['PLZ__c'],
                'city'   => $arrSql[0]['Ort__c'],
                'izsid'  => $arrSql[0]['Betriebsnummer__c'],
                'member' => date('Y-m-d', strtotime($arrSql[0]['Registriert_bei_IZS_seit__c'])),
                'link'   => '/company_group/' .$arrSql[0]['Id'] .'/stammdaten'
            );

            $strFileName = 'done/'.$arrSql[0]['Id'] .'_' .date('dmYHis') .'.pdf';
            $intUserId   = ($_COOKIE['id']) ?? '';

            $strSql = 'INSERT INTO `izs_zertifikat` (`pr_id`, `pr_hash`, `pr_group_id`, `pr_file`, `pr_file_name`, `pr_data`, `pr_cl_id`, ';
            $strSql.= '`pr_revoked`, `pr_revoked_by`, `pr_revoked_time`, `pr_time`) VALUES (NULL, "'.$strUnique .'", "' .$arrSql[0]['Id'] .'", "' .$strFileName .'", ';
            $strSql.= '"' .$arrSql[0]['Name'] .' - Zertifikat", "' .(addslashes(serialize($arrData))) .'", "' .$intCreateUser .'", "0", "0", "0000-00-00", NOW())';

            $intSql = MySQLStatic::Insert($strSql);

            $strSql  = 'SELECT `pr_time` FROM `izs_zertifikat` WHERE `pr_id` = "' .$intSql .'"';
            $arrTime = MySQLStatic::QUERY($strSql);

        } else {

            $arrTime = [];

            $strSql = 'SELECT * FROM `izs_zertifikat` WHERE `pr_group_id` = "' .$arrSql[0]['Id'] .'" AND `pr_revoked` = 0 ORDER BY `pr_time` DESC LIMIT 1';
            $arrCert = MySQLStatic::Query($strSql);

            $arrData = unserialize($arrCert[0]['pr_data']);
            $strFileName = $arrCert[0]['pr_file_name'];
            $arrTime[0]['pr_time'] = $arrCert[0]['pr_time'];

            $strUnique = $arrCert[0]['pr_hash'];

        }

        $strHost = 'www';
        if (isset($_SERVER['SERVER_NAME']) && (strstr($_SERVER['SERVER_NAME'], 'test.') !== false)) {
            $strHost = 'test';
        }

        $strHash = 'https://' .$strHost .'.izs.de/cms/zertifikat/' .$strUnique;

        $strUid  = md5(uniqid(rand(), true));

        if ($boolStandAlone === true) {
            $strPath = '../files/temp/' .$strUid .'.png';
        } else {
            $strPath =  '/data/www/sites/www.izs.de/html/cms/files/temp/' .$strUid .'.png';
        }
        
        
        QRcode::png($strHash, $strPath);

        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " QR-Code: $intDurationTime Sek." .chr(10);

        $floatLeft = 20;
        $floatCol1 = 81.5;

        if (!isset($objPdf)) {

            $objPdf = new pdfHtml('L');
            $objPdf->AliasNbPages();

            $objPdf->AddFont('Lato', '', 'Lato-Regular.php');
            $objPdf->AddFont('Lato', 'B', 'Lato-Bold.php');

            $objPdf->AddFont('OfficinaSerif', '', 'itcofficinaserifw04medium-webfont.php');
            $objPdf->AddFont('OfficinaSerif', 'B', 'itcofficinaserifw04medium-webfont.php');
            
        } 

        $objPdf->AddPage('L');
        $objPdf->SetAutoPageBreak(false);

        if ($boolStandAlone === false) {
            $objPdf->SetLink($arrPdfLinkListEvent[$arrSql[0]['evid']], 0, -1);
        }

        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " Init PDF: $intDurationTime Sek." .chr(10);
        
        $objPdf->Image( __DIR__ .'/img/bg_pdf4.png', 0, 0, 297, 210, 'PNG'); 


        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " Include BG: $intDurationTime Sek." .chr(10);

        //Page margin
        $objPdf->SetMargins($floatLeft, 10.8);

        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " Include Logo: $intDurationTime Sek." .chr(10);


        //LEFT SIDE

        $intLeft1 = 8.9;
        $intLeft2 = 13;

        $intTextSmall = 7;
        $intTextH3 = 8;

        $intTextH2 = 11;
        $intTextBig = 9;

        $intLineHeigtSmallText = 3.7;


        // Col left
        $objPdf->SetMargins($intLeft1, 0, 0);
        $objPdf->SetRightMargin(160);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->setLinkColor(233, 94, 53, ''); //#f06600

        $objPdf->SetFont('Lato', 'B', $intTextH2);
        $objPdf->SetX($intLeft1);
        $objPdf->SetY(32);
        $objPdf->WriteHTML(utf8_decode('Starke Vorteile für Entleiher'));

        $objPdf->SetFont('Lato', '', $intTextBig);
        $objPdf->SetXY($intLeft1, 39);
        $objPdf->WriteHTML(utf8_decode('IZS ist Deutschlands führende Plattform für reibungslose und sichere Zusammenarbeit in '));
        $objPdf->SetXY($intLeft1, 44);
        $objPdf->WriteHTML(utf8_decode('der Zeitarbeit, die Aufwände reduziert, Zahlungsprozesse vereinfacht, das Problem der '));
        $objPdf->SetXY($intLeft1, 49);
        $objPdf->WriteHTML(utf8_decode('Subsidiärhaftung löst und kontinuierlich neue, lukrative Geschäftsverbindungen generiert.'));

        $objPdf->SetMargins($intLeft1, 0, 0);
        $objPdf->SetY(55 - 0.4);
        $objPdf->SetFont('Symbol', 'B', 10);
        $objPdf->WriteHTML('<a href="https://www.izs.de/" target="_blank">' .chr(174) .'</a>');

        $objPdf->SetY(55);
        $objPdf->SetFont('Lato', '', 8);
        $objPdf->WriteHTML(utf8_decode('<a href="https://www.izs.de/" target="_blank">        Mehr zum IZS Portal</a>'));
        //$objPdf->Link(10, 55, 33, 3, 'https://www.izs.de/');



        $objPdf->SetMargins($intLeft1, 0, 0);
        $objPdf->SetY(67);
        $objPdf->SetFont('Lato', 'B', $intTextH3);
        $objPdf->WriteHTML(utf8_decode('Regelmäßige Prüfungen'));

        $objPdf->SetMargins($intLeft2, 0, 0);
        $objPdf->SetXY($intLeft2, 75.8);
        $objPdf->SetFont('Lato', '', $intTextSmall);
        $objPdf->WriteHTML(utf8_decode('IZS prüft, ob Beitragsrückstände, Stundungen, '));
        $objPdf->SetXY($intLeft2, 75.8 + ($intLineHeigtSmallText * 1));
        $objPdf->WriteHTML(utf8_decode('Ratenzahlungen oder Nachforderungen aus '));
        $objPdf->SetXY($intLeft2, 75.8 + ($intLineHeigtSmallText * 2));
        $objPdf->WriteHTML(utf8_decode('Betriebsprüfungen bestehen - monatlich bei allen '));
        $objPdf->SetXY($intLeft2, 75.8 + ($intLineHeigtSmallText * 3));
        $objPdf->WriteHTML(utf8_decode('Krankenkassen und nach Fälligkeit bei allen '));
        $objPdf->SetXY($intLeft2, 75.8 + ($intLineHeigtSmallText * 4));
        $objPdf->WriteHTML(utf8_decode('Berufsgenossenschaften.'));
        $objPdf->SetY(96);
        $objPdf->WriteHTML(utf8_decode('IZS prüft monatlich die Gültigkeit aller Erlaubnisse zur '));
        $objPdf->SetXY($intLeft2, 96 + ($intLineHeigtSmallText * 1));
        $objPdf->WriteHTML(utf8_decode('Arbeitnehmerüberlassung der Bundesagentur für '));
        $objPdf->SetXY($intLeft2, 96 + ($intLineHeigtSmallText * 2));
        $objPdf->WriteHTML(utf8_decode('Arbeit, um die Haftung der Entleiher für Mindestlohn '));
        $objPdf->SetXY($intLeft2, 96 + ($intLineHeigtSmallText * 3));
        $objPdf->WriteHTML(utf8_decode('und Lohnsteuern zu verhindern.'));


        $objPdf->SetMargins($intLeft1, 0, 0);
        $objPdf->SetY(116);
        $objPdf->SetFont('Lato', 'B', $intTextH3);
        $objPdf->WriteHTML(utf8_decode('100 % vertrauenswürdige Auskünfte'));

        $objPdf->SetMargins($intLeft2, 0, 0);
        $objPdf->SetY(124);
        $objPdf->SetFont('Lato', '', $intTextSmall);
        $objPdf->WriteHTML(utf8_decode('IZS erhält alle Auskünfte direkt von den '));
        $objPdf->SetXY($intLeft2, 124 + ($intLineHeigtSmallText * 1));
        $objPdf->WriteHTML(utf8_decode('Auskunftsgebern.'));
        $objPdf->SetY(134);
        $objPdf->WriteHTML(utf8_decode('Eine Manipulation durch Dritte ist ausgeschlossen.'));


        $objPdf->SetMargins($intLeft1, 0, 0);
        $objPdf->SetY(143);
        $objPdf->SetFont('Lato', 'B', $intTextH3);
        $objPdf->WriteHTML(utf8_decode('Fälschungssichere Zertifikate'));
        $objPdf->SetFont('Lato', '', $intTextSmall);
        
        $objPdf->SetMargins($intLeft2, 0, 0);
        $objPdf->SetY(151);
        $objPdf->WriteHTML(utf8_decode('Wertvoller und sicherer als Unbedenklichkeits- '));
        $objPdf->SetXY($intLeft2, 151 + ($intLineHeigtSmallText * 1));
        $objPdf->WriteHTML(utf8_decode('bescheinigungen: IZS stellt zu jedem Prüfvorgang '));
        $objPdf->SetXY($intLeft2, 151 + ($intLineHeigtSmallText * 2));
        $objPdf->WriteHTML(utf8_decode('digitale SV- und BG-Zertifikate</a> aus.'));

        $objPdf->SetY(164);
        $objPdf->WriteHTML(utf8_decode('Eine Echtheitsprüfung ist jederzeit online und per '));
        $objPdf->SetXY($intLeft2, 164 + ($intLineHeigtSmallText * 1));
        $objPdf->WriteHTML(utf8_decode('QR-Code möglich.'));


        $objPdf->SetMargins($intLeft1, 0, 0);
        $objPdf->SetY(178);
        $objPdf->SetFont('Lato', 'B', $intTextH3);
        $objPdf->WriteHTML(utf8_decode('Lückenlose Dokumentation'));
        $objPdf->SetFont('Lato', '', $intTextSmall);

        $objPdf->SetMargins($intLeft2 + 4.8, 0, 0);
        $objPdf->SetY(186 - 0.2);
        $objPdf->SetFont('Symbol', '', $intTextSmall - 2);
        $objPdf->WriteHTML('<a href="https://portal.izs.de/company_group/' .$arrSql[0]['Id'] .'/stammdaten" target="_blank">' .chr(174) .'</a>');

        $objPdf->SetMargins($intLeft2, 0, 0);
        $objPdf->SetFont('Lato', '', $intTextSmall);

        $objPdf->SetY(186);
        $objPdf->WriteHTML(utf8_decode('Alle <a href="https://portal.izs.de/company_group/' .$arrSql[0]['Id'] .'/stammdaten" target="_blank">     Prüfungsergebnisse</a> zu Personaldienstleistern '));
        $objPdf->SetXY($intLeft2, 186 + ($intLineHeigtSmallText * 1));
        $objPdf->WriteHTML(utf8_decode('sind jederzeit online einsehbar und herunterladbar.'));


        // Colr right
        $intLeft1 = 78;
        $intLeft2 = 82;
        $objPdf->SetMargins($intLeft1, 157);

        $objPdf->SetY(67);
        $objPdf->SetFont('Lato', 'B', $intTextH3);
        $objPdf->WriteHTML(utf8_decode('Versicherbarkeit über SubsidiärProtect'));

        $objPdf->SetMargins(127.9, 157);
        $objPdf->SetY(66.5);
        $objPdf->SetFont('Lato', 'B', $intTextH3 - 1);
        $objPdf->WriteHTML(chr(174) .utf8_decode(''));

        $strText = '';
        for ($i = 171; $i < 180; $i++) {
            $strText.= $i .':[' .chr($i) .'];';
        }
        
        $objPdf->SetMargins($intLeft2, 157);
        $objPdf->SetY(75.6);

        $objPdf->SetFont('Symbol', '', $intTextSmall - 2);
        $objPdf->WriteHTML('<a href="https://www.subsidiaerprotect.de/" target="_blank">' .chr(174) .'</a>');

        $objPdf->SetY(75.8);
        $objPdf->SetFont('Lato', '', $intTextSmall);
        $objPdf->WriteHTML(utf8_decode('<a href="https://www.subsidiaerprotect.de/" target="_blank">     ') .utf8_decode('SubsidiärProtect') .utf8_decode('</a>      ist eine spezielle, von IZS '));

        $objPdf->SetXY(102.4, 75);
        $objPdf->SetMargins(117, 10);
        $objPdf->SetFont('Lato', '', $intTextSmall - 1);
        $objPdf->WriteHTML(utf8_decode('<a href="https://www.subsidiaerprotect.de/" target="_blank"> ') .chr(174) .utf8_decode('</a>'));

        $objPdf->SetFont('Lato', '', $intTextSmall);
        $objPdf->SetMargins($intLeft2, 157);
        $objPdf->SetXY($intLeft2, 75.8 + ($intLineHeigtSmallText * 1));
        $objPdf->WriteHTML(utf8_decode('entwickelte Haftplichtversicherung, mit der sich '));
        $objPdf->SetXY($intLeft2, 75.8 + ($intLineHeigtSmallText * 2));
        $objPdf->WriteHTML(utf8_decode('Entleiher einfach und günstig gegen Subsidiärhaftung '));
        $objPdf->SetXY($intLeft2, 75.8 + ($intLineHeigtSmallText * 3));
        $objPdf->WriteHTML(utf8_decode('absichern können.'));
        $objPdf->SetY(92);
        $objPdf->WriteHTML(utf8_decode('Alle Entleihungen bei IZS-geprüften '));
        $objPdf->SetXY($intLeft2, 92 + ($intLineHeigtSmallText * 1));
        $objPdf->WriteHTML(utf8_decode('Personaldienstleistern sind automatisch gegen '));
        $objPdf->SetXY($intLeft2, 92 + ($intLineHeigtSmallText * 2));
        $objPdf->WriteHTML(utf8_decode('finanzielle Schäden aus Subsidiärhaftung bis zur '));
        $objPdf->SetXY($intLeft2, 92 + ($intLineHeigtSmallText * 3));
        $objPdf->WriteHTML(utf8_decode('vereinbarten Versicherungssumme abgedeckt, sofern '));
        $objPdf->SetXY($intLeft2, 92 + ($intLineHeigtSmallText * 4));
        $objPdf->WriteHTML(utf8_decode('der Entleiher eine SubsidiärProtect    -Police hat. Es '));


        $objPdf->SetMargins(120, 0);
        $objPdf->SetY(92 + ($intLineHeigtSmallText * 4) - 0.3);
        $objPdf->SetFont('Lato', '', 5);
        $objPdf->WriteHTML(chr(174) .utf8_decode(''));


        $objPdf->SetFont('Lato', '', $intTextSmall);
        $objPdf->SetMargins(117, 10);
        $objPdf->SetXY($intLeft2, 92 + ($intLineHeigtSmallText * 5));
        $objPdf->WriteHTML(utf8_decode('gelten die Versicherungsbedingungen.'));
        $objPdf->SetY(116);
        $objPdf->SetXY($intLeft2, 116);
        $objPdf->WriteHTML(utf8_decode('Sicherheitseinbehalte und Bankbürgschaften werden '));
        $objPdf->SetXY($intLeft2, 116 + ($intLineHeigtSmallText * 1));
        $objPdf->WriteHTML(utf8_decode('überflüssig.'));

        $objPdf->SetMargins($intLeft1, 0, 0);
        $objPdf->SetY(129.5);
        $objPdf->SetFont('Lato', 'B', $intTextH3);
        $objPdf->WriteHTML(utf8_decode('Maßgeschneidertes Web-Portal'));
        
        $objPdf->SetMargins($intLeft2, 157);
        $objPdf->SetY(139);
        $objPdf->SetFont('Lato', '', $intTextSmall);
        $objPdf->WriteHTML(utf8_decode('Über ein individuell anpassbares IZS-Portal, erhalten '));
        $objPdf->SetXY($intLeft2, 139 + ($intLineHeigtSmallText * 1));
        $objPdf->WriteHTML(utf8_decode('Entleiher weitere wertvolle Informationen, wie '));
        $objPdf->SetXY($intLeft2, 139 + ($intLineHeigtSmallText * 2));
        $objPdf->WriteHTML(utf8_decode('Zertifizierungen, Versicherungspolicen, Nachweise '));
        $objPdf->SetXY($intLeft2, 139 + ($intLineHeigtSmallText * 3));
        $objPdf->WriteHTML(utf8_decode('über die Zugehörigkeit zu einer Tarifgemeinschaft, '));
        $objPdf->SetXY($intLeft2, 139 + ($intLineHeigtSmallText * 4));
        $objPdf->WriteHTML(utf8_decode('Mindestlohnerklärungen usw. - immer auf dem '));
        $objPdf->SetXY($intLeft2, 139 + ($intLineHeigtSmallText * 5));
        $objPdf->WriteHTML(utf8_decode('aktuellsten Stand.'));
        $objPdf->SetY(163);
        $objPdf->WriteHTML(utf8_decode('Wertvolle Dashboards zeigen jederzeit, zu welchem '));
        $objPdf->SetXY($intLeft2, 163 + ($intLineHeigtSmallText * 1));
        $objPdf->WriteHTML(utf8_decode('Personaldienstleister in den letzten 24 Monaten '));
        $objPdf->SetXY($intLeft2, 163 + ($intLineHeigtSmallText * 2));
        $objPdf->WriteHTML(utf8_decode('Negativmerkmale vorlagen.'));
        $objPdf->SetY(176 );
        $objPdf->WriteHTML(utf8_decode('Das intelligente Frühwarnsystem benachrichtigt '));
        $objPdf->SetXY($intLeft2, 176 + ($intLineHeigtSmallText * 1));
        $objPdf->WriteHTML(utf8_decode('registrierte Entleiher auf Wunsch automatisch, wenn '));
        $objPdf->SetXY($intLeft2, 176 + ($intLineHeigtSmallText * 2));
        $objPdf->WriteHTML(utf8_decode('bei Personaldienstleistern Negativereignisse '));
        $objPdf->SetXY($intLeft2, 176 + ($intLineHeigtSmallText * 3));
        $objPdf->WriteHTML(utf8_decode('vorliegen.'));


        $objPdf->SetMargins($floatLeft + 141.4, 10.8);
        $objPdf->SetY(29);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Helvetica', '', 65);
        $objPdf->MultiCell(130, 5, utf8_decode('Zertifikat'), 0, 'L');    
        
        $objPdf->SetY(44);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Helvetica', '', 9);
        $objPdf->MultiCell(130, 5, utf8_decode('zum Nachweis der Prüfung durch die IZS Compliance- und'), 0, 'L');    
        $objPdf->MultiCell(130, 5, utf8_decode('Risikomanagement-Plattform für die Zeitarbeit'), 0, 'L');    

        $objPdf->SetFont('Helvetica', 'B', 15);
        $floatWidth1  = $objPdf->GetStringWidth($arrData['name']);

        if (($floatWidth1 > 103) && (!strstr($arrData['name'], 'Hans-Peter Pohl'))) {
            $intY = 70;
        } else {
            $intY = 74;
        }
        
        $objPdf->SetMargins($floatLeft + 153.4, 10.8);
        $objPdf->SetY($intY);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Helvetica', 'B', 15);
        $objPdf->MultiCell(105, 7, utf8_decode($arrData['name']), 0, 'L');   

        if ($arrData['member'] != '0000-00-00') {
            $intTime = strtotime($arrData['member']);
            $strRegistriert = date('d. ', $intTime) .$arrMonthList[date('n', $intTime)] .date(' Y', $intTime);
        } else {
            $strRegistriert = '';
        }
        
        $objPdf->SetMargins($floatLeft + 141.4, 10.8);
        $objPdf->SetY(100);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Lato', '', 9);
        $objPdf->MultiCell(130, 5, utf8_decode(str_replace("\n", '', $arrData['street'])), 0, 'L');   
        $objPdf->MultiCell(130, 5, utf8_decode($arrData['zip'] .' ' .str_replace("\n", '', $arrData['city'])), 0, 'L');   
        $objPdf->MultiCell(130, 5, utf8_decode('IZS-ID: ' .$arrData['izsid']), 0, 'L'); 

        if ($strRegistriert != '') {
            $objPdf->MultiCell(130, 5, utf8_decode('Mitglied bei IZS seit: ' .$strRegistriert), 0, 'L');
        }

        $intOffset = 0;
        if (strstr($arrSql[0]['Strasse__c'], chr(10)) !== false) {
            //$intOffset = 5;
        }

        $intKue = 0;
        if ($strKuendigung != '') {

            $objPdf->SetFont('Lato', 'B', 9);

            $floatWidth2  = $objPdf->GetStringWidth('Das Zertifikat läuft am ' .$strKuendigung .' ab.');

            $objPdf->SetLineWidth(0.5);
            $objPdf->SetDrawColor(255, 0, 0);
            $objPdf->Rect($floatLeft + 142, 123, $floatWidth2 + 3, 6);

            $objPdf->SetY(123.5);
            $objPdf->SetTextColor(255, 0, 0); //#000000
            $objPdf->SetFont('Lato', 'B', 9);
            $objPdf->MultiCell(130, 5, utf8_decode('   Das Zertifikat läuft am ' .$strKuendigung .' ab.'), 0, 'L');  

            $objPdf->SetTextColor(44, 45, 46); //#000000

            $intKue = 10;

        }



        $objPdf->Image( __DIR__ .'/img/button3.png', $floatLeft + 141.4, 125 + $intOffset + $intKue, 51.8, 10.2);

        $objPdf->SetMargins($floatLeft + 145, 10.8, 0);
        $objPdf->SetY(128.1 + $intOffset + $intKue);
        $objPdf->SetTextColor(255, 255, 255); //#ffffff

        $objPdf->SetFont('Symbol', '', 10);
        $objPdf->WriteHTML('' .chr(174) .'');

        $objPdf->SetMargins($floatLeft + 150, 10.8, 0);
        $objPdf->SetY(128.1 + $intOffset + $intKue);
        $objPdf->SetFont('Lato', '', 10);
        $objPdf->MultiCell(73, 4, utf8_decode('zum IZS-Mitgliedskonto'), 0, '');

        //$strUrl = 'izs-portal.netlify.app';
        $strUrl = 'portal.izs.de';

        $objPdf->Link($floatLeft + 141, 125 + $intOffset + $intKue, 51.8, 10.2, 'https://' .$strUrl .'/company_group/' .$arrSql[0]['Id'] .'/stammdaten');


        $objPdf->SetMargins($floatLeft + 141.4, 10.8);

        $objPdf->SetY(160);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Lato', '', 9);
        $objPdf->MultiCell(130, 6, utf8_decode('Dieses Zertifikat ist nur gültig mit QR-Code.'), 0, 'L');   
        $objPdf->MultiCell(130, 6, utf8_decode('Die Gültigkeit kann auch online überprüft werden:'), 0, 'L');   

        $strUrl = $strHost .'.izs.de/cms/zertifikat/' .$strUnique;
        
        $objPdf->SetMargins($floatLeft + 141.4, 0, 0);
        $objPdf->SetY(173 - 0.3);
        $objPdf->SetFont('Symbol', 'B', 9);
        $objPdf->setLinkColor(233, 94, 53, ''); //#f06600
        $objPdf->WriteHTML('<a href="' .$strUrl .'" target="_blank">' .chr(174) .'</a>');

        $strHtml = '<a href="https://' .$strUrl .'">' .$strUrl .'</a>';

        $objPdf->SetFont('Lato', '', 9);

        $objPdf->SetMargins($floatLeft + 141.4 + 4, 10.8, 0);
        $objPdf->SetX(140);
        $objPdf->SetY(173);
        $objPdf->setLinkColor(233, 94, 53, ''); //#f06600
        $objPdf->WriteHTML(utf8_decode($strHtml));


        $objPdf->SetMargins($floatLeft + 141.4, 0, 0);
        $objPdf->SetY(185);
        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Lato', '', 9);
        $objPdf->MultiCell(130, 4, utf8_decode('Dieses Zertifikat wurde erstellt:'), 0, 'L');

        $intTime = strtotime($arrTime[0]['pr_time']);

        $objPdf->SetTextColor(44, 45, 46); //#000000
        $objPdf->SetFont('Lato', '', 9);

        $objPdf->SetXY(166, 190.2);
        $objPdf->MultiCell(130, 4, utf8_decode(date('d.m.Y', $intTime)), 0, 'L');

        $objPdf->SetXY(191, 190.2);
        $objPdf->MultiCell(130, 4, utf8_decode(date('H:i:s', $intTime) .' CET'), 0, 'L');

        //QR-Code
        $objPdf->Image($strPath, 266.5, 177, 15, 15);
        unlink($strPath);


        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " Process PDF: $intDurationTime Sek." .chr(10);

        if (($strReturnType == 'string') && ($boolStandAlone === true)) {
            return $objPdf->Output('S'); //Output($strFileName, 'F')
        }


    }

}

if ($boolStandAlone === true) {
    $strOutput = createCert($intEventId);
}

?>