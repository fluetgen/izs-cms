<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

include ('conf/global.inc.php');
include ('classes/fklCsv.php');
include ('inc/sv-import.inc.php');

require_once(APP_PATH .'cms/classes/class.escalation.php');

$objEsc      = new Escalation;
$arrEscList  = $objEsc->getEscalationList();

if ($_REQUEST['fname'] != '') {

$boolHasHeader = false;
$intCsvLines = 0;

$arrAc2Gr = array();
$strSql = 'SELECT `SF42_Comany_ID__c`, `SF42_Company_Group__c` FROM `Account` WHERE `RecordTypeId` = "01230000001Ao71AAC" AND `SF42_isPremiumPayer__c` = "true" AND `SF42_Comany_ID__c` != ""';
$arrResult = MySQLStatic::Query($strSql);

// Alle Vorgänge im System
$strSql = 'SELECT * FROM `Vorgang` WHERE `case_type` = "Betriebsnummer-Klärung"';
$arrSql = MySQLStatic::Query($strSql);

$arrWorkList = array();
if (count($arrSql) > 0) {

  foreach ($arrSql as $intKey => $arrDataset) {
    $strKey = $arrDataset['case_bnr_nr'];
    $arrWorkList[$strKey][] = $arrDataset; 
  }

}


$strOutput = '';
$strOutput.= '<h1>SV-Daten Check</h1>' .chr(10);
$strOutput.= '<h3>Import Status: {IMPSTATUS}</h3>' .chr(10);

if (count($arrResult) > 0) {
  foreach ($arrResult as $intKey => $arrAccount) {
    $arrAc2Gr[$arrAccount['SF42_Comany_ID__c']] = $arrAccount['SF42_Company_Group__c'];
  }
}

$strFile = $_REQUEST['fname'];

$arrConcat = explode('|', substr($strFile, 1));

//print_r($arrConcat); die();

$boolUploadOk = false;
foreach ($arrConcat as $intKey => $strFile) {
  $strPath = $strUploadDir .'/' .$strFile;
  if (!is_file($strPath)) {
    $boolUploadOk = false;
    break;
  } else {
    $boolUploadOk = true;
    $arrConcat[$intKey] = $strPath;
  }
}

if ($boolUploadOk) {
  
  $arrImport = array();

  foreach ($arrConcat as $intKey => $strPath) {
    $objCsvFile = new fklCsv($strPath);
    $arrPart = $objCsvFile->to_array();

    if (($intKey >= 1) && (count($arrPart) > 0)) {
      if (is_numeric($arrPart[0][1]) === false) {
        unset($arrPart[0]);
      }
    }

    $arrImport = array_merge($arrImport, $arrPart);

  }

//print_r($arrImport); die();

  $arrPeriod = array();
  foreach ($arrImport as $intKey => $arrDataset) {
    if (($intKey == 0) && (is_numeric($arrDataset[1]) == false)) {
      //print_r($arrDataset);
      continue;
    }

    $arrDataset[0] = preg_replace('/[\x00-\x1F\x7F]/', '', $arrDataset[0]);

    if (trim($arrDataset[0]) == '') {
      continue;
    }

    $arrPart = explode('.', $arrDataset[0]);

    if (strlen($arrPart[2]) == 4) {
      $arrDataset[0] = $arrPart[0] .'.' .$arrPart[1] .'.' .substr($arrPart[2], -2);
      //echo $arrDataset[0] .chr(10);
    } else {
      /*
      for ($i = 0; $i < strlen($arrPart[2]); $i++) { 
        echo '|' .$arrPart[2][$i] .'| Ord: ' .ord($arrPart[2][$i]) .chr(10); 
      }
      echo '|' .$arrPart[2] .'|';
      echo strlen($arrPart[2]);
      print_r($arrPart);
      print_r($arrDataset); die();
      */
    }

    if (!isset($arrPeriod[$arrDataset[0]])) {
      //print_r($arrDataset);
      $arrPeriod[$arrDataset[0]] = 0;
    }
    $arrPeriod[$arrDataset[0]]++;
  }

  asort($arrPeriod);

  //print_r($arrImport); die();
    
  if (count($arrImport) > 0) {

    $arrOutput = createTable($arrImport);
    
    $strOutputTable = $arrOutput[0];
      
    $intCount = count($arrImport);
    //print_r($arrImport);
    if ($boolHasHeader) {
      $intCount--;
    }

    //list($strGroup, $arrMonth) = each($arrOutput[2]);

    $strGroup = key($arrOutput[2]);
    $arrMonth = current($arrOutput[2]);

    if ($strGroup == '') {
      //list($strGroup, $arrMonth) = each($arrOutput[2]);
      if (count($arrOutput[2]) > 1) unset($arrOutput[2]['']);

      $strGroup = key($arrOutput[2]);
      $arrMonth = current($arrOutput[2]);

      if ($strGroup == '') {
        if (count($arrOutput[5]) > 0) {
          foreach ($arrOutput[5] as $intKey => $arrDataRow) {
            if ($arrDataRow[8] == 'OK') {
              $strSql = 'SELECT `SF42_Company_Group__c` FROM `Account` WHERE `Account`.`SF42_Comany_ID__c` = "' .$arrDataRow[1] .'"';
              $arrGroupGuess = MySQLStatic::Query($strSql);
              if (count($arrGroupGuess) > 0) {
                $strGroupGuess = $arrGroupGuess[0]['SF42_Company_Group__c'];
                break;
              }
            }
          }
        }
      }

    }

    //echo $strGroup;

    //list($strMonth, $intSum) = each($arrMonth);
    $strMonth = key($arrMonth);
    $intSum = current($arrMonth);

    if ($_SESSION['id'] == 3) {
      //print_r($arrOutput[2]);
      //var_dump($intSum);
      //die();
    }

    $arrUpload = reset($arrOutput[3]);

    if (is_array($arrOutput[2]) && (count($arrOutput[2]) > 0)) {
      foreach ($arrOutput[2] as $strGroup => $arrMonth) {
        if ($strGroup != '') {
          break;
        }
      }
    }

    if ($strGroup == '') {
      $strGroup = $strGroupGuess;
    }

    //print_r($arrPeriod); die();

    $strSql = 'SELECT * FROM `izs_premiumpayer_group` WHERE `Id` = "' .$strGroup .'"';
    $arrGroupRaw = MySQLStatic::Query($strSql);

    if (count($arrGroupRaw) == 0) {
      $boolHasGroup = false;
    } else {
      $boolHasGroup = true;
    }

    $strSql = 'SELECT * FROM `izs_premiumpayer` ';
    $arrAllRaw = MySQLStatic::Query($strSql);

    $arrAll = array();
    $arrAllBnr = array();
    foreach ($arrAllRaw as $intKey => $arrMeldestelle) {
      $arrAll[$arrMeldestelle['Id']] = $arrMeldestelle['Name'];
      $arrAllBnr[$arrMeldestelle['Btnr']] = $arrMeldestelle['Name'];
    }

    $strSql = 'SELECT * FROM `izs_informationprovider` ';
    $arrAllIpRaw = MySQLStatic::Query($strSql);

    $arrAllIp = array();
    foreach ($arrAllIpRaw as $intKey => $arrKrankenkasse) {
      $arrAllIp[$arrKrankenkasse['Id']] = $arrKrankenkasse['Name'];
    }

    //NEGATIVMERKMAHL
    $arrNegativMerkmal = array();
    $boolNegativMerkmal = false;
    $strNvm = 'SELECT `PP`.`SF42_Comany_ID__c` AS `PpBnr`, `KK`.`SF42_Comany_ID__c` AS `KkBnr`, `Negativmerkmal__c`.* FROM `Negativmerkmal__c` INNER JOIN `Account` AS `PP` ON `AccountId` = `PP`.`Id` INNER JOIN `Account` AS `KK` ON `Auskunftsgeber__c` = `KK`.`Id` WHERE `GroupId` = "' .$strGroup .'" AND `Negativmerkmal__c`.`Aktiv__c` = "true" AND `Auskunftsgeber_Typ__c` = "01230000001Ao72AAC"';
    $arrNvm = MySQLStatic::Query($strNvm);

    if (count($arrNvm) > 0) {
      foreach ($arrNvm as $intKey => $arrNDataset) {
        $strKey = $arrNDataset['PpBnr'] .'_' .$arrNDataset['KkBnr'] .'_ja';
        $arrNegativMerkmal[$strKey] = $arrNDataset['Art_Negativmerkmal__c'];

        $strOutputTable = str_replace('{' .$strKey .'}', $arrNDataset['Art_Negativmerkmal__c'], $strOutputTable);

      }
    }

    $strOutputTable = preg_replace('/\{.*\}/', '', $strOutputTable);

    $strEscStatus = '<span style="background-color: green; padding: 3px; color: white;">NEIN</span>';
    if ($arrEscList[$strGroup]) {
      $strEscStatus = '<a href="' .$arrEscList[$strGroup]['Eskalation_Link__c'] .'" title="' .$arrEscList[$strGroup]['Eskalation_Grund__c'] .'" alt="' .$arrEscList[$strGroup]['Eskalation_Grund__c'] .'" style="background-color: red; padding: 3px; color: white; cursor: pointer;" id="hideshow">JA</a>';
    }

    $strNegStatus = '<span style="background-color: green; padding: 3px; color: white;">NEIN</span>';
    if (count($arrNvm) > 0) {
      $strNegStatus = '<span style="background-color: red; padding: 3px; color: white; cursor: pointer;" id="hideshow">JA</span>';
    }

    $strOutput = '';
    $strOutput.= '<h1>SV-Daten Check: <a href="index_neu.php?ac=contacts&grid=' .$strGroup .'" target="_blank">' .$arrGroupRaw[0]['Name'] .'</a></h1>' .chr(10);    
    $strOutput.= '    <h3>Import Status: {IMPORTSTATUS}</h3>' .chr(10);

    $strOutput.= '{BNRPROBLEM}' .chr(10);

    $strOutput.= '    <h3>Negativmerkmal: ' .$strNegStatus .'</h3>' .chr(10);
    $strOutput.= '    <h3>Eskalationsfall: ' .$strEscStatus .'</h3>' .chr(10);

    if (count($arrNvm) > 0) {

      $strOutput.= '<div id="hiddenContent" style="display: none;"><table class="tblover">
      <thead>
        <tr>
          <th class="tosort tblol">Negativmerkmale</th>
          <th>Krankenkasse</th>
          <th>Grund</th>
          <th>Link</th>
        </tr>
      </thead>
      ';
      foreach ($arrNvm as $intKey => $arrMekmal) {

        $strLink = '';
        if ($arrMekmal['Chat_Link__c'] != '') {
          $strLink = '<a href="' .$arrMekmal['Chat_Link__c'] .'" target="_blank">Chat anzeigen</a>';
        }

      $strOutput.= '
      <tr>
        <td><span class="red">' .$arrAll[$arrMekmal['AccountId']] .'</span></td>
        <td>' .$arrAllIp[$arrMekmal['Auskunftsgeber__c']] .'</td>
        <td>' .$arrMekmal['Art_Negativmerkmal__c'] .'</td>
        <td>' .$strLink .'</td>
      </tr>';
      }

      $strOutput.= '
      </table></div>';

    }

    $strImportDate = $strMonth;
    //echo $strMonth; die();

    //print_r($arrOutput[0]);
    //print_r($arrOutput[1]);
    //print_r($arrOutput[2]);
    //print_r($arrOutput[3]);
    //print_r($arrOutput[4]);
    //print_r($arrOutput[5]);
    //print_r($arrImport);

    $strSql = 'SELECT * FROM `izs_sv_month` LIMIT 1';
    $arrSql = MySQLStatic::Query($strSql);

    $arrMonthAllowed = array(
      //date('01.m.Y', mktime(0, 0, 0, $arrSql[0]['Beitragsmonat__c'], -1, $arrSql[0]['Beitragsjahr__c'])),
      date('01.m.Y', mktime(0, 0, 0, $arrSql[0]['Beitragsmonat__c'], 1, $arrSql[0]['Beitragsjahr__c'])),
      date('01.m.Y', mktime(0, 0, 0, $arrSql[0]['Beitragsmonat__c'], 32, $arrSql[0]['Beitragsjahr__c'])),
    );

    // allow : d=25; d=5,m=3
    $intDay   = date('j');
    $intMonth = date('n');

    if ($intDay >= 25) {
      //$arrMonthAllowed[] = date('01.m.Y', mktime(0, 0, 0, $arrSql[0]['Beitragsmonat__c'], 32, $arrSql[0]['Beitragsjahr__c']));
    }
    
    if ($intDay <= 5) {
      $strMonth = date('01.m.Y', mktime(0, 0, 0, $intMonth, -1, date('Y')));
      if (!in_array($strMonth, $arrMonthAllowed)) {
        $arrMonthAllowed[] = $strMonth;
      }
    }

    $allowImport = true;
    $strNotAllowed = '';
    foreach ($arrPeriod as $strImportMonth => $intDatasetCount) {
      $arrPart = explode('.', $strImportMonth);
      $strImportMonth = $arrPart[0] .'.' .$arrPart[1] .'.20' .$arrPart[2];
      if (!in_array($strImportMonth, $arrMonthAllowed)) {
        $allowImport = false;
        if ($strNotAllowed != '') {
          $strNotAllowed.= ', ';
        }
        $strNotAllowed.= substr($strImportMonth, -7);
      }
    }

    if ($allowImport === false) {

      $strOutput .= '  <div class="ui-state-error" style="padding: 0px 7px; margin-right: 15px; margin-bottom: 10px;">' . chr(10);
      $strOutput .= '    <h3>Achtung! Ein Import für diese(n) Monat(e) ist nicht möglich: ' .$strNotAllowed .'</h3>' . chr(10);
      $strOutput .= '  </div>' . chr(10);

    } else if (count($arrPeriod) != 1) {

      $strperiodList = '';
      foreach ($arrPeriod as $strPeriod => $intCount) {
        $strperiodList.= str_replace('.', '.20', preg_replace('/(\d{2}\.)(\d{2}\.)(\d{2})/', '$2$3', $strPeriod)) .', ';
      }

      $strperiodList = substr($strperiodList, 0, -2);
      
      $strOutput .= '  <div class="ui-state-error" style="padding: 0px 7px; margin-right: 15px; margin-bottom: 10px;">' . chr(10);
      $strOutput .= '    <h3>Achtung! Es sind mehrere Monate in der Import-Datei enthalten: ' .$strperiodList .'</h3>' . chr(10);
      $strOutput .= '  </div>' . chr(10);
   
      //echo $strOutput; die();
   
    }

    $arrMeldeDatei = array();
    foreach ($arrOutput[1] as $intKey => $arrEvent) {
      if (in_array($arrEvent[5], $arrMeldeDatei) === false) {
        $arrMeldeDatei[] = $arrEvent[5];
      } 
    }

    //print_r($arrMeldeDatei);

    $strSql = 'SELECT `is_date` FROM `import_stats` WHERE `is_grid` = "' .$strGroup .'" AND `is_date` < "' .$strImportDate .'" ORDER BY `is_date` DESC LIMIT 1 ';
    $arrVormonat = MySQLStatic::Query($strSql);

    $strSql = 'SELECT `is_date`, SUM(`is_datasets`) AS `is_datasets`, SUM(`is_result_ok`) AS `is_result_ok`, SUM(`is_result_error`) AS `is_result_error`, ';
    $strSql.= 'SUM(`is_result_doublet_system`) AS `is_result_doublet_system`, SUM(`is_result_doublet_file`) AS `is_result_doublet_file`, SUM(`is_result_saldo_null`) AS `is_result_saldo_null`, ';
    $strSql.= 'SUM(`is_result_saldo_no`) AS `is_result_saldo_no`, SUM(`is_result_saldo_unknown`) AS `is_result_saldo_unknown` ';
    $strSql.= 'FROM `import_stats` WHERE `is_grid` = "' .$strGroup .'" AND `is_date` = "' .$arrVormonat[0]['is_date'] .'" ORDER BY `is_date` DESC ';
    $arrVormonat = MySQLStatic::Query($strSql);

    $strSql = 'SELECT * FROM `izs_premiumpayer` WHERE `Group` = "' .$strGroup .'" ';
    $arrMeldestellen = MySQLStatic::Query($strSql);

    $strSql = 'SELECT `Account`.`Id` AS `Id`, `Account`.`Name` AS `Name`, `Account`.`SF42_Comany_ID__c` AS `Btnr`, `Account`.`SF42_Company_Group__c` AS `Group`, ';
    $strSql.= '`Account`.`BillingStreet` AS `BillingStreet`, `Account`.`BillingCity` AS `BillingCity`, `Account`.`BillingPostalCode` AS `BillingPostalCode`, `Account`.`SF42_isPremiumPayer__c` AS `SF42_isPremiumPayer__c`, ';
    $strSql.= '`Account`.`Aktiv__c` AS `Aktiv__c`, `Account`.`SF42_Sub_Type__c` AS `SubType`, `Account`.`SF42_use_data__c` AS `DatenAnnehmen` FROM `Account` WHERE ';
    $strSql.= '`Account`.`RecordTypeId` = "01230000001Ao71AAC" AND `Account`.`SF42_Sub_Type__c` IN ("Premium Payer", "Office") AND ';
    $strSql.= '`Account`.`SF42_Company_Group__c` = "' .$strGroup .'" ORDER BY `Account`.`SF42_Sub_Type__c` DESC, `Account`.`Aktiv__c` ASC';
    $arrMeldestellenAll = MySQLStatic::Query($strSql);

    if (@$_SERVER['HTTP_FKLD'] == 'on') {
      //echo $strSql;
    }
  

    //print_r($arrMeldestellenAll); die();

    $arrMeldestellenAktiv   = array();
    $arrMeldestellenIntern  = array();
    $arrMeldestellenInaktiv = array();

    if (count($arrMeldestellenAll) > 0) {
      foreach ($arrMeldestellenAll as $intKey => $arrMeldestelle) {
        if (($arrMeldestelle['DatenAnnehmen'] == 'false') && ($arrMeldestelle['SF42_isPremiumPayer__c'] == 'false') && ($arrMeldestelle['SubType'] == 'Office')) {
          $arrMeldestellenIntern[] = $arrMeldestelle['Btnr'];
        } elseif (($arrMeldestelle['DatenAnnehmen'] == 'false') && ($arrMeldestelle['SF42_isPremiumPayer__c'] == 'true') && ($arrMeldestelle['SubType'] == 'Premium Payer')) {
          $arrMeldestellenInaktiv[] = $arrMeldestelle['Btnr'];
        } else if (($arrMeldestelle['Aktiv__c'] == 'true') && ($arrMeldestelle['DatenAnnehmen'] == 'true') && ($arrMeldestelle['SF42_isPremiumPayer__c'] == 'true') && ($arrMeldestelle['SubType'] == 'Premium Payer')) {
          $arrMeldestellenAktiv[] = $arrMeldestelle['Btnr'];
        }

      }
    }

    /*
    Daten annehmen  = SF42_use_data__c
    Ist Meldestelle = SF42_isPremiumPayer__c
    Sub-Type = SF42_Sub_Type__c

    1. BNR war einmal Meldestelle und ist jetzt INAKTIV
    Daten annehmen = false 
    Ist Meldestelle = TRUE
    Sub-Type = Premium Payer 

    2. BNR ist INTERNE Meldestelle 
    Daten annehmen = false
    Ist Meldestelle = FALSE
    Sub-Type = Office
    */

    /*
    print_r($arrMeldestellenInaktiv); 
    print_r($arrMeldestellenIntern); 
    print_r($arrMeldestellenAktiv); 
    print_r($arrMeldestellenAll); 
    die();
    */

    $arrMeldeDateiAktiv = array();
    $arrMeldeDateiIntern = array();
    $arrMeldeDateiInaktiv = array();
    $arrMeldeDateiUnbekannt = array();

    $arrListSaldo = array();

    $arrImportMeldestellen = array();
    foreach ($arrOutput[5] as $intKey => $arrDataset) {
      if (!in_array($arrDataset[1], $arrImportMeldestellen)) {
        $arrImportMeldestellen[] = $arrDataset[1];
      }

      $strKey = 'Ja';
      if ($arrDataset[11] != '') {
        $strKey = $arrDataset[11];
      }
      if (!isset($arrListSaldo[$arrDataset[1]][$strKey])) {
        $arrListSaldo[$arrDataset[1]][$strKey] = 1;
      } else {
        $arrListSaldo[$arrDataset[1]][$strKey]++;
      }

    }

    //print_r($arrListSaldo); die();

    if (count($arrImportMeldestellen) > 0) {
      foreach ($arrImportMeldestellen as $intKey => $strMeldeBnr) {

        //echo $strMeldeBnr .chr(10);

        if (in_array($strMeldeBnr, $arrMeldestellenIntern)) {
          $arrMeldeDateiIntern[] = $strMeldeBnr;
        } elseif (in_array($strMeldeBnr, $arrMeldestellenAktiv)) {
          $arrMeldeDateiAktiv[] = $strMeldeBnr;
        } elseif (in_array($strMeldeBnr, $arrMeldestellenInaktiv)) {
          $arrMeldeDateiInaktiv[] = $strMeldeBnr;
        } else {
          $arrMeldeDateiUnbekannt[] = $strMeldeBnr;
        }
      }
    }

    /*
    print_r($arrMeldeDateiAktiv); 
    print_r($arrMeldeDateiInaktiv); 
    print_r($arrMeldeDateiIntern); 

    print_r($arrMeldestellenAktiv); 
    print_r($arrMeldestellenInaktiv); 
    print_r($arrMeldestellenIntern); 
    
    die();
    */
    
    /* ----------------- */

    //print_r($arrMeldeDateiInaktiv); 

    $arrEventsDatei = $arrOutput[4];

    $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE  `Beitragsmonat__c`= "' .substr($arrVormonat[0]['is_date'], 5, 2) .'" AND `Beitragsjahr__c` = "' .substr($arrVormonat[0]['is_date'], 0, 4) .'" AND group_id = "' .$strGroup .'" AND `SF42_EventStatus__c` != "abgelehnt / Dublette" ORDER BY `SF42_Premium_Payer__c`';
    $arrEvents = MySQLStatic::Query($strSql);

    $arrEventsSystem = array();
    foreach ($arrEvents as $intKey => $arrEvent) {

      if (!isset($arrEventsSystem[$arrEvent['Betriebsnummer_ZA__c']])) {
        $arrEventsSystem[$arrEvent['Betriebsnummer_ZA__c']] = 0;
      }
      
      $arrEventsSystem[$arrEvent['Betriebsnummer_ZA__c']]++;

    }

    //print_r($arrOutput[1]); die();

    $arrEventsUpload = array();
    foreach ($arrOutput[1] as $intKey => $arrEvent) {

      if (!isset($arrEventsUpload[$arrEvent[5]])) {
        $arrEventsUpload[$arrEvent[5]] = 0;
      }
      if ($arrEvent[7] == 'enquired') {
        $arrEventsUpload[$arrEvent[5]]++;
      }

    }

    /* $arrOutput[5]
    [1] => Array
        (
            [Monat] => 2019-07
            [BNR PP] => 37991239
            [Status PP] => OK
            [Name PP] => Jägers GmbH & Co.KG - NL Köln
            [BNR KK] => 15035218
            [Status KK] => OK
            [BNR IP] => 15035218
            [Name Ip] => DAK-Gesundheit
            [Status IP] => OK
            [Dublette System] => NO
            [Import Status] => OK
        )
        (
            [0] => 2019-07
            [1] => 37991239
            [2] => OK
            [3] => Jägers GmbH & Co.KG - NL Köln
            [4] => 15035218
            [5] => OK
            [6] => 15035218
            [7] => DAK-Gesundheit
            [8] => OK
            [9] => NO
            [10] => OK
        )
    */

    $arrMeldestellen = array();
      
    /*
    if (@$_SERVER['HTTP_FKLD'] == 'on') {
      echo $strSql;
      print_r($arrEventsSystem);
      print_r($arrMeldestellenAktiv); 
      die();
    }
    */

    foreach ($arrOutput[5] as $intRow => $arrRow) {

      if (!isset($arrMeldestellen[$arrRow[1]])) {
        $arrMeldestellen[$arrRow[1]] = array(
          'ges' => 0,
          'sja' => 0,
          'sne' => 0,
          'snu' => 0,
          'sun' => 0,
          'dub' => 0,
          'err' => 0,
          'ims' => array(),
          'nam' => $arrRow[3],
          'erd' => array()
        );
      }

      $arrMeldestellen[$arrRow[1]]['ges']++;
      
      $intProove = 10;
      if (count($arrRow) == 12) {
        $intProove = 11;
      }

      if ($arrRow[$intProove] == 'Guthaben') {
        $arrMeldestellen[$arrRow[1]]['sne']++;
      } elseif ($arrRow[$intProove] == 'Nullmeldung') {
        $arrMeldestellen[$arrRow[1]]['snu']++;
      } elseif ($arrRow[$intProove] == 'Unbekanntes Saldo') {
        $arrMeldestellen[$arrRow[1]]['sun']++;
        $arrMeldestellen[$arrRow[1]]['err']++;
        if (!in_array($arrRow[10], $arrMeldestellen[$arrRow[1]]['erd'])) {
          $arrMeldestellen[$arrRow[1]]['erd'][] = $arrRow[10];
        }
      } else {
        $arrMeldestellen[$arrRow[1]]['sja']++;
      }

      if ($arrRow[10] == 'OK (konsolidiert)') {
        $arrMeldestellen[$arrRow[1]]['dub']++;
        $arrMeldestellen[$arrRow[1]]['sja']--;
      }

      if (strstr($arrRow[10], 'ungültig') !== false) {
        $arrMeldestellen[$arrRow[1]]['err']++;
        if (!in_array($arrRow[10], $arrMeldestellen[$arrRow[1]]['erd'])) {
          $arrMeldestellen[$arrRow[1]]['erd'][] = $arrRow[10];
        }
      }
      
      if (!in_array($arrRow[10], $arrMeldestellen[$arrRow[1]]['ims'])) {
        $arrMeldestellen[$arrRow[1]]['ims'][] = $arrRow[10];
      }

    }

    /*
    if (@$_SERVER['HTTP_FKLD'] == 'on') {
      print_r($arrMeldestellenAktiv); 
      print_r($arrMeldestellen); 
      die();
    }
    */

    $strDivTable2 = '';

    $strDivTable2.= '<table class="tblover">' .chr(10);
    $strDivTable2.= '  <thead>' .chr(10);
    $strDivTable2.= '    <tr>' .chr(10);
    $strDivTable2.= '      <th class="tosort tblol">Meldestellen</th>' .chr(10);

    $strDivTable2.= '      <th class="tosort" style="background-color: #ffffff;">Gesamt</th>' .chr(10);
    $strDivTable2.= '      <th class="tosort" style="background-color: #ffffff;">Saldo ja</th>' .chr(10);
    $strDivTable2.= '      <th class="tosort" style="background-color: #ffffff;">Saldo nein</th>' .chr(10);
    $strDivTable2.= '      <th class="tosort" style="background-color: #ffffff;">Saldo Null</th>' .chr(10);
    $strDivTable2.= '      <th class="tosort" style="background-color: #ffffff;">Saldo unbekannt</th>' .chr(10);
    $strDivTable2.= '      <th class="tosort" style="background-color: #ffffff;">Dublette</th>' .chr(10);
    $strDivTable2.= '      <th class="tosort" style="background-color: #ffffff;">Fehler</th>' .chr(10);
    $strDivTable2.= '      <th class="tosort" style="background-color: #ffffff;">Import Status</th>' .chr(10);

    $strDivTable2.= '      <th class="tosort">' .substr($arrVormonat[0]['is_date'], 0, -3) .'</th>' .chr(10);
    $strDivTable2.= '      <th class="tosort">' .substr($strImportDate, 0, -3) .'</th>' .chr(10);
    $strDivTable2.= '      <th class="tosort">Differenz</th>' .chr(10);
    $strDivTable2.= '    </tr>' .chr(10);
    $strDivTable2.= '  </thead>' .chr(10);
    $strDivTable2.= '  <tbody>' .chr(10);

    //foreach ($arrEventsSystem as $strBtnr => $intAnzahl) {
    foreach ($arrMeldestellenAktiv as $intKey => $strBtnr) {

      //echo 'BNR (' .$intKey .'):' .$strBnr .'|';

      $intAnzahl = 0;
      if (isset($arrEventsSystem[$strBtnr])) {
        $intAnzahl = $arrEventsSystem[$strBtnr];
      }

      $strClass = '';
      if (!isset($arrEventsUpload[$strBtnr])) {
        $arrEventsUpload[$strBtnr] = 0;
        $strClass = 'red';
      }

      if (is_array($arrMeldestellen[$strBtnr]['ims'])) {
        asort($arrMeldestellen[$strBtnr]['ims']);
      }
      
      if (is_array($arrMeldestellen[$strBtnr]['erd'])) {
        asort($arrMeldestellen[$strBtnr]['erd']);
      }
      
      
      if (@in_array('Inaktive BNR', $arrMeldestellen[$strBtnr]['ims']) !== false) {
        $arrMeldestellen[$strBtnr]['err'] = $arrMeldestellen[$strBtnr]['ges'];
        $arrMeldestellen[$strBtnr]['erd'][] = 'Inaktive BNR';
      }

      if (@in_array('Interne BNR (Office)', $arrMeldestellen[$strBtnr]['ims']) !== false) {
        $arrMeldestellen[$strBtnr]['err'] = $arrMeldestellen[$strBtnr]['ges'];
        $arrMeldestellen[$strBtnr]['erd'][] = 'Interne BNR';
      }

      $strImpStatus = 'OK';
      if ($arrMeldestellen[$strBtnr]['err'] > 0) {
        $strImpStatus = '<span class="red">NOT OK</span> (' .implode(', ', $arrMeldestellen[$strBtnr]['erd']) .')';
        $arrMeldestellen[$strBtnr]['err'] = '<span class="red">' .$arrMeldestellen[$strBtnr]['err'] .'</span>';
        $strClass = 'red';
      }

      $strSaldoUnbekannt = $arrMeldestellen[$strBtnr]['sun'];
      if ($strSaldoUnbekannt > 0) {
        $strSaldoUnbekannt = '<span class="red">' .$arrMeldestellen[$strBtnr]['sun'] .'</span>';
      }

      $strName = $arrAllBnr[$strBtnr];
      if ($strName == '') {
        $strSqlName = 'SELECT `Name` FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$strBtnr .'"';
        $arrSqlName = MySQLStatic::Query($strSqlName);
        $strName = $arrSqlName[0]['Name'];
      }

      $strDivTable2.= '    <tr>' .chr(10);
      $strDivTable2.= '      <td><span class="' .$strClass .'">' .$strBtnr .' - ' .$strName .'</span></td>' .chr(10);

      if (isset($arrEventsUpload[$strBtnr]) && ($arrEventsUpload[$strBtnr] > 0)) {
        $strClass = '';
      }

      if ($arrMeldestellen[$strBtnr]['ges'] == '') {
        $strImpStatus = '';
      }

      $strDivTable2.= '      <td style="background-color: #ffffff;">' .$arrMeldestellen[$strBtnr]['ges'] .'</td>' .chr(10);
      $strDivTable2.= '      <td style="background-color: #ffffff;">' .$arrMeldestellen[$strBtnr]['sja'] .'</td>' .chr(10);
      $strDivTable2.= '      <td style="background-color: #ffffff;">' .$arrMeldestellen[$strBtnr]['sne'] .'</td>' .chr(10);
      $strDivTable2.= '      <td style="background-color: #ffffff;">' .$arrMeldestellen[$strBtnr]['snu'] .'</td>' .chr(10);
      $strDivTable2.= '      <td style="background-color: #ffffff;">' .$strSaldoUnbekannt .'</td>' .chr(10);
      $strDivTable2.= '      <td style="background-color: #ffffff;">' .$arrMeldestellen[$strBtnr]['dub'] .'</td>' .chr(10);
      $strDivTable2.= '      <td style="background-color: #ffffff;">' .$arrMeldestellen[$strBtnr]['err'] .'</td>' .chr(10);
      $strDivTable2.= '      <td style="background-color: #ffffff;">' .$strImpStatus .'</td>' .chr(10);

      $strDivTable2.= '      <td class="tblor">' .$intAnzahl .'</td>' .chr(10);
      $strDivTable2.= '      <td class="tblor"><span class="' .$strClass .'">' .$arrEventsUpload[$strBtnr] .'</span></td>' .chr(10);
      $strDivTable2.= '      <td class="tblor">' .($arrEventsUpload[$strBtnr] - $intAnzahl) .'</td>' .chr(10);
      $strDivTable2.= '    </tr>' .chr(10);

      unset($arrEventsUpload[$strBtnr]);
      unset($arrMeldestellen[$strBtnr]);

    } 

    if (@$_SERVER['HTTP_FKLD'] == 'on') {
      //print_r($arrMeldestellen); 
    }

    $intErr = 0;
    foreach ($arrMeldestellen as $strBtnr => $arrResult) {

      $strClass = '';
      if (!isset($arrEventsUpload[$strBtnr])) {
        $arrEventsUpload[$strBtnr] = 0;
        $strClass = 'red';
      }

      $intAnzahl = 0;

      if (in_array('Inaktive BNR', $arrMeldestellen[$strBtnr]['ims']) !== false) {
        $arrMeldestellen[$strBtnr]['err'] = $arrMeldestellen[$strBtnr]['ges'];
        $arrMeldestellen[$strBtnr]['erd'][] = 'Inaktive BNR';
      }

      if (in_array('Interne BNR (Office)', $arrMeldestellen[$strBtnr]['ims']) !== false) {
        $arrMeldestellen[$strBtnr]['err'] = $arrMeldestellen[$strBtnr]['ges'];
        $arrMeldestellen[$strBtnr]['erd'][] = 'Interne BNR';
      }

      asort($arrMeldestellen[$strBtnr]['ims']);
      asort($arrMeldestellen[$strBtnr]['erd']);
      
      $strImpStatus = 'OK';
      if (count($arrMeldestellen[$strBtnr]['erd']) > 0) {
        $strImpStatus = '<span class="red">NOT OK</span> (' .implode(', ', $arrMeldestellen[$strBtnr]['erd']) .')';
        $intErr++;
      }

      $strClass2 = '';
      if ($arrMeldestellen[$strBtnr]['err'] > 0) {
        $strClass2 = 'red';
      }

      if ($arrMeldestellen[$strBtnr]['ges'] == '') {
        $strImpStatus = '';
      }

      $strName = $arrMeldestellen[$strBtnr]['nam'];
      if ($strName == '') {
        $strSqlName = 'SELECT `Name` FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$strBtnr .'"';
        $arrSqlName = MySQLStatic::Query($strSqlName);
        $strName = $arrSqlName[0]['Name'];
      }

      $strDivTable2.= '    <tr>' .chr(10);
      $strDivTable2.= '      <td><span class="' .$strClass .'">' .$strBtnr .' - ' .$arrMeldestellen[$strBtnr]['nam'] .'</span></td>' .chr(10);

      $strDivTable2.= '      <td style="background-color: #ffffff;">' .$arrMeldestellen[$strBtnr]['ges'] .'</td>' .chr(10);
      $strDivTable2.= '      <td style="background-color: #ffffff;">' .$arrMeldestellen[$strBtnr]['sja'] .'</td>' .chr(10);
      $strDivTable2.= '      <td style="background-color: #ffffff;">' .$arrMeldestellen[$strBtnr]['sne'] .'</td>' .chr(10);
      $strDivTable2.= '      <td style="background-color: #ffffff;">' .$arrMeldestellen[$strBtnr]['snu'] .'</td>' .chr(10);
      $strDivTable2.= '      <td style="background-color: #ffffff;">' .$arrMeldestellen[$strBtnr]['sun'] .'</td>' .chr(10);
      $strDivTable2.= '      <td style="background-color: #ffffff;">' .$arrMeldestellen[$strBtnr]['dub'] .'</td>' .chr(10);
      $strDivTable2.= '      <td style="background-color: #ffffff;"><span class="' .$strClass2 .'">' .$arrMeldestellen[$strBtnr]['err'] .'</span></td>' .chr(10);
      $strDivTable2.= '      <td style="background-color: #ffffff;">' .$strImpStatus .'</td>' .chr(10);

      $strDivTable2.= '      <td class="tblor">' .$intAnzahl .'</td>' .chr(10);
      $strDivTable2.= '      <td class="tblor"><span class="' .$strClass .'">' .$arrEventsUpload[$strBtnr] .'</span></td>' .chr(10);
      $strDivTable2.= '      <td class="tblor">' .($arrEventsUpload[$strBtnr] - $intAnzahl) .'</td>' .chr(10);
      $strDivTable2.= '    </tr>' .chr(10);

      unset($arrEventsUpload[$strBtnr]);

    } 


  if (@$_SERVER['HTTP_FKLD'] == 'on') {
    //print_r($arrMeldestellen[$strBtnr]['erd']);
  }


    $strImportStatus = '<span style="background-color: green; padding: 3px; color: white;">OK</span>';
    if ((@count($arrMeldestellen[$strBtnr]['erd']) > 1) || ((@count($arrMeldestellen[$strBtnr]['erd']) == 1) && ($arrMeldestellen[$strBtnr]['erd'][0] != 'Interne BNR')) || ($boolHasGroup === false)) {
      $strImportStatus = '<span style="background-color: red; padding: 3px; color: white;">NOT OK</span>';
    }

    $strOutput = str_replace('{IMPORTSTATUS}', $strImportStatus, $strOutput);


    if (count($arrEventsUpload) > 0) {

      foreach ($arrEventsUpload as $strBtnr => $intAnzahl) {

        $strDivTable2.= '    <tr>' .chr(10);
        $strDivTable2.= '      <td><span class="red">' .$strBtnr .' - ' .$arrAllBnr[$strBtnr] .'</span></td>' .chr(10);
        $strDivTable2.= '      <td class="tblor"><span class="red">0</span></td>' .chr(10);
        $strDivTable2.= '      <td class="tblor">' .$intAnzahl .'</td>' .chr(10);
        $strDivTable2.= '      <td class="tblor">' .$intAnzahl .'</td>' .chr(10);
        $strDivTable2.= '    </tr>' .chr(10);
  
      }

    }

    $strDivTable2.= '  </tbody>' .chr(10);
    $strDivTable2.= '</table>' .chr(10);

    $strDivTable2.= "
    <script>
    const getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;

    const comparer = (idx, asc) => (a, b) => ((v1, v2) => 
        v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2)
        )(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));
    
    // do the work...
    document.querySelectorAll('th.tosort').forEach(th => th.addEventListener('click', (() => {
      const table = th.closest('table');
      const tbody = table.querySelector('tbody');
      Array.from(tbody.querySelectorAll('tr'))
        .sort(comparer(Array.from(th.parentNode.children).indexOf(th), this.asc = !this.asc))
        .forEach(tr => tbody.appendChild(tr) );
    })));
    </script>
    " .chr(10);


    $strOutput.= '

    <style>
    .tblover {
      display: table;
      border-color: grey;
      margin-bottom: 20px;
    }
    .tblover td, .tblover th {
      border: 1px solid #dddddd;
      padding: 5px;
      text-align: left;
      min-width: 50px;
    }
    .tblover td.tblor {
      text-align: right;
    }   
    .tblover td.tblol, .tblover th.tblol {
      text-align: left;
    }  
    .tblover thead th {
      text-align: center;
    }
    .tosort {
      cursor: pointer;
    }
    </style>

    <table class="tblover">
      <thead>
        <tr>
          <th></th>
          <th>gesamt</th>
          <th>aktiv</th>
          <th>intern</th>
          <th>inaktiv</th>
          <th>unbekannt</th>
        </tr>
      </thead>
      <tr>
        <td>Meldestellen lt. System</td>
        <td class="tblor">' .(int) (count($arrMeldestellenAktiv) + count($arrMeldestellenIntern) + count($arrMeldestellenInaktiv)) .'</td>
        <td class="tblor">' .count($arrMeldestellenAktiv) .'</td>
        <td class="tblor">' .count($arrMeldestellenIntern) .'</td>
        <td class="tblor">' .count($arrMeldestellenInaktiv) .'</td>
        <td class="tblor">0</td>
      </tr>
      <tr>
        <td>Meldestellen lt. Importdatei</td>
        <td class="tblor">' .(int) (count($arrMeldeDateiAktiv) + count($arrMeldeDateiIntern) + count($arrMeldeDateiInaktiv) + count($arrMeldeDateiUnbekannt)) .'</td>
        <td class="tblor">' .count($arrMeldeDateiAktiv) .'</td>
        <td class="tblor">' .count($arrMeldeDateiIntern) .'</td>
        <td class="tblor">';
        
        if (count($arrMeldeDateiInaktiv) == 0) {
          $strOutput.= count($arrMeldeDateiInaktiv);
        } else {
          $strOutput.= '<span class="red">' .count($arrMeldeDateiInaktiv) .'</span>';
        }

  $strOutput.= '</td>
        <td class="tblor">';
        
        if (count($arrMeldeDateiUnbekannt) == 0) {
          $strOutput.= count($arrMeldeDateiUnbekannt);
        } else {
          $strOutput.= '<span class="red">' .count($arrMeldeDateiUnbekannt) .'</span>';
        }

  $strOutput.= '</td>
      </tr>
      <tr>
        <td>Differenz</td>
        <td class="tblor"></td>
        <td class="tblor">' .(count($arrMeldeDateiAktiv) - count($arrMeldestellenAktiv)) .'</td>
        <td class="tblor">' .(count($arrMeldeDateiIntern) - count($arrMeldestellenIntern)) .'</td>
        <td class="tblor">' .(count($arrMeldeDateiInaktiv) - count($arrMeldestellenInaktiv)) .'</td>
        <td class="tblor">' .count($arrMeldeDateiUnbekannt) .'</td>
      </tr>
    </table>
    ';

  $strBnrProblem = '';

  //echo $strMonth; die();

  if ((count($arrMeldeDateiInaktiv) > 0) || (count($arrMeldeDateiUnbekannt) > 0)) {

    //print_r($arrMeldeDateiUnbekannt);

    $strBnrProblem.= '  <div class="ui-state-error" style="padding: 0px 7px; margin-right: 15px; margin-bottom: 10px;">' . chr(10);
    $strBnrProblem.= '    <h3>Es wurden folgende Unbekannte / Inaktive Betriebsnummern gefunden:</h3>' . chr(10);
    $strBnrProblem.= '  </div>' . chr(10);

    $strBnrProblem.= '
    <table class="tblover">
    <thead>
      <tr>
        <th>Betriebsnummer</th>
        <th>Grund</th>
        <th>Saldo(s)</th>
        <th>Aktion</th>
        <th>Bemerkung</th>
        </tr>
    </thead>
    ';

    if (count($arrMeldeDateiInaktiv) > 0) {

      foreach ($arrMeldeDateiInaktiv as $intKey => $strBnr) {

        //echo $strImportDate;
        
        $strSql = 'SELECT * FROM `Vorgang` WHERE `case_bnr_nr` = "' .$strBnr .'" AND `case_type` = "Betriebsnummer-Klärung" AND `case_month` = "' .(int) substr($strImportDate, 5, 2) .'" AND `case_year` = "' .substr($strImportDate, 0, 4) .'"';
        $arrSql = MySQLStatic::Query($strSql);

        //print_r($arrSql);

        if (count($arrSql) > 0) {
          $strButton = '<button class="ui-button ui-state-highlight ui-corner-all" style="background-color: gray;" disabled>Vorgang bereits angelegt</button>';
        } else {
          $strButton = '<button class="ui-button ui-state-highlight ui-corner-all inactive">Als Vorgang anlegen</button>';
        }

        $strAdd = '';
        $strVormonat = date('Y-m-d', mktime(0, 0, 0, ((int) substr($strImportDate, 5, 2) - 1), 1, substr($strImportDate, 0, 4)));
        $strSql = 'SELECT * FROM `Vorgang` WHERE `case_bnr_nr` = "' .$strBnr .'" AND `case_type` = "Betriebsnummer-Klärung" AND `case_month` = "' .(int) substr($strVormonat, 5, 2) .'" AND `case_year` = "' .substr($strVormonat, 0, 4) .'"';
        $arrSql = MySQLStatic::Query($strSql);
        if (count($arrSql) > 0) {
          $strAdd.= 'Vorgang bereits im Vormonat registriert.';
        }

        $strFolgeMonat = date('Y-m-d', mktime(0, 0, 0, ((int) substr($strImportDate, 5, 2) + 1), 1, substr($strImportDate, 0, 4)));
        $strSql = 'SELECT * FROM `Vorgang` WHERE `case_bnr_nr` = "' .$strBnr .'" AND `case_type` = "Betriebsnummer-Klärung" AND `case_month` = "' .(int) substr($strFolgeMonat, 5, 2) .'" AND `case_year` = "' .substr($strFolgeMonat, 0, 4) .'"';
        $arrSql = MySQLStatic::Query($strSql);
        if (count($arrSql) > 0) {
          if ($strAdd != '') $strAdd.= ' ';
          $strAdd.= 'Vorgang bereits im Folgemonat registriert.';
        }

        $strBnrProblem.= '
    <tr>
      <td style="vertical-align: middle;">' .$strBnr .'</td>
      <td style="vertical-align: middle;">Inaktive Betriebsnummer</td>
      <td style="vertical-align: middle;">' .implode(', ', array_keys($arrListSaldo[$strBnr])) .'</td>
      <td>' .$strButton .'</td>
      <td style="vertical-align: middle;">' .$strAdd .'</td>
    </tr>
    ';
        }
    }

    if (count($arrMeldeDateiUnbekannt) > 0) {
      foreach ($arrMeldeDateiUnbekannt as $intKey => $strBnr) {

        //echo $strImportDate .chr(10);

        $strSql = 'SELECT * FROM `Vorgang` WHERE `case_bnr_nr` = "' .$strBnr .'" AND `case_type` = "Betriebsnummer-Klärung" AND `case_month` = "' .(int) substr($strImportDate, 5, 2) .'" AND `case_year` = "' .substr($strImportDate, 0, 4) .'"';
        $arrSql = MySQLStatic::Query($strSql);
        if (count($arrSql) > 0) {
          $strButton = '<button class="ui-button ui-state-highlight ui-corner-all" style="background-color: gray;" disabled>Vorgang bereits angelegt</button>';
        } else {
          $strButton = '<button class="ui-button ui-state-highlight ui-corner-all unknown">Als Vorgang anlegen</button>';
        }

        //echo chr(10);

        $strAdd = '';
        $strVormonat = date('Y-m-d', mktime(0, 0, 0, ((int) substr($strImportDate, 5, 2) - 1), 1, substr($strImportDate, 0, 4)));
        $strSql = 'SELECT * FROM `Vorgang` WHERE `case_bnr_nr` = "' .$strBnr .'" AND `case_type` = "Betriebsnummer-Klärung" AND `case_month` = "' .(int) substr($strVormonat, 5, 2) .'" AND `case_year` = "' .substr($strVormonat, 0, 4) .'"';
        $arrSql = MySQLStatic::Query($strSql);
        if (count($arrSql) > 0) {
          $strAdd.= 'Vorgang bereits im Vormonat registriert.';
        }

        //echo chr(10);

        $strFolgeMonat = date('Y-m-d', mktime(0, 0, 0, ((int) substr($strImportDate, 5, 2) + 1), 1, substr($strImportDate, 0, 4)));
        $strSql = 'SELECT * FROM `Vorgang` WHERE `case_bnr_nr` = "' .$strBnr .'" AND `case_type` = "Betriebsnummer-Klärung" AND `case_month` = "' .(int) substr($strFolgeMonat, 5, 2) .'" AND `case_year` = "' .substr($strFolgeMonat, 0, 4) .'"';
        $arrSql = MySQLStatic::Query($strSql);
        if (count($arrSql) > 0) {
          if ($strAdd != '') $strAdd.= ' ';
          $strAdd.= 'Vorgang bereits im Folgemonat registriert.';
        }

        //die();

    $strBnrProblem.= '
    <tr>
      <td style="vertical-align: middle;">' .$strBnr .'</td>
      <td style="vertical-align: middle;">Unbekannte Betriebsnummer</td>
      <td style="vertical-align: middle;">' .implode(', ', array_keys($arrListSaldo[$strBnr])) .'</td>
      <td>' .$strButton .'</td>
      <td style="vertical-align: middle;">' .$strAdd .'</td>
    </tr>
    ';
        }
    }

    $strBnrProblem.= '
    </table>
    ';

    $strBnrProblem.= "
    <script>
      $('.unknown, .inactive').bind('click', function() {
        if ($(this).hasClass('ui-state-highlight')) {

          var el = $(this);
          var bnr = el.parent().parent().find('td:first-child').text();
          var ty = el.parent().parent().find('td:nth-child(2)').text();

          $.ajax({
            url: 'inc/svcheck.ajax.php',
            type: 'post',
            data: {
                ac: 'create',
                cg: '" .$strGroup ."',
                bnr: bnr,
                month: " .(int) substr($strImportDate, 5, 2) .",
                year: " .substr($strImportDate, 0, 4) .",
                ty: ty
            },
            dataType: 'json',
            success: function (data) {

              alert('Der Vorgang wurde angelegt: ' + bnr);
              el.removeClass('ui-state-highlight');
              el.addClass('ui-state-disabled');

            }
          });


        }
      });
    </script>
    " .chr(10);

  }

  //var_dump($strBnrProblem);
  

  $strOutput.= '

  <table class="tblover">
    <thead>
      <tr>
        <th></th>
        <th>Summe</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Datenmenge Vormonat</td>
        <td class="tblor">' .$arrVormonat[0]['is_datasets'] .'</td>
      </tr>
      <tr>
        <td>Datenmenge Importdatei</td>
        <td class="tblor">' .$intSum .'</td>
      </tr>
      <tr>
        <td>Differenz</td>
        <td class="tblor">' .($intSum - $arrVormonat[0]['is_datasets']) .'</td>

      </tr>
    </tbody>
  </table>

    ';
  
    $strOutput.= $strDivTable2;

    $strOutput.= '<div class="clearfix" style="height: 48px; width: 1341px;">' .chr(10);
    $strOutput.= '  <div class="form-left">' .chr(10);
    $strOutput.= '    <h3>' .$intCsvLines .' Events gefunden:</h3>' .chr(10);
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' .chr(10);

    $strOutput.= '<form method="get" href="/cms/index.php?ac=svch" style="display: inline;">' .chr(10);
    $strOutput.= '  <input type="hidden" name="ac" value="svch">' .chr(10);
    $strOutput.= '  <button id="checkback" type="submit" class="ui-button ui-state-default ui-corner-all">Fertig</button>' .chr(10);
    $strOutput.= '</form>' .chr(10);

    $strOutput.= '    <form method="post" action="_get_csv.php" style="display: inline; float: right; padding-left: 10px;">';
    $strOutput.= '      <input type="hidden" name="strTable" value="' .str_replace('"', "'", $strOutputTable) .'">' .chr(10);
    $strOutput.= '      <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' .chr(10);
    $strOutput.= '    </form>';
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '</div>' .chr(10);
    
    $strOutput.= $strOutputTable;
    
    $strOutput.= "
<script>
$('#5').fixheadertable({ 
   colratio    : [60, 70, 58, 241, 70, 58, 70, 210, 58, 45, 200, 150], 
   height      : 500, 
   width       : 1315, 
   zebra       : true, 
   sortable    : true,
   sortType    : ['string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'hidden', 'string'],
   sortedColId : 10, 
   dateFormat  : 'Y-m'
});

$(document).ready(function(){
  $('#hideshow').live('click', function(event) {
    var x = $('#hiddenContent');
    if (x.css('display') === 'none') {
      x.show();
    } else {
      x.hide();
    }
  });
});

</script>
";

$strOutput = str_replace('{BNRPROBLEM}', $strBnrProblem, $strOutput);
  
  } else {
  
    $strOutput.= '<p>Keine Daten gefunden.</p>' .chr(10);
    
  }


  //echo $objCsvFile->build_table();
  

}
  
  
} elseif ($_REQUEST['import'] != '1') {

$intNow = time();

$arrFiles = glob($strUploadDir .'/*');
if (is_array($arrFiles) && (count($arrFiles) > 0)) {
  foreach($arrFiles as $strFile){
    if(is_file($strFile)) {
      $intModified = filectime($strFile);
      if (($intNow - $intModified) > 5*24*3600) {
        @unlink($strFile);
      }
    }
  }
}


$timestamp = time();
  
$strOutput.= '
<p>&nbsp;</p>
<form method="post" action="index.php?ac=svch">
	<div id="queue"><span id="emp">Drop Files here...</span></div>
	<input type="hidden" name="fname" id="fname" />
	<input type="hidden" name="ac" value="svch" />
	<input type="file" name="file_upload" id="file_upload" />
	<p><input type="submit" value="Prüfen" id="fsubmit" disabled="disabled" /></p>
</form>

<script type="text/javascript">
$(document).ready(function($) {
    $(\'#file_upload\').uploadifive({
        \'auto\'             : true,
        \'multi\'            : true, 
        \'queueSizeLimit\'   : 10,
        \'uploadLimit\'      : 0, 
				\'formData\'         : { \'timestamp\' : \'' .$timestamp .'\',
        									     \'token\'     : \'' .md5('unique_salt' .$timestamp) .'\' },
        \'queueID\'          : \'queue\',
        \'itemTemplate\'     : \'<div class="uploadifive-queue-item"><span class="filename"></span><span class="fileinfo"></span><div class="close"></div></div>\', 
        \'uploadScript\'     : \'uploadifive.php\',
        \'onUploadComplete\' : function(file, data) { $("#fname").val($("#fname").val() + "|" + file.name); $("#fsubmit").removeAttr("disabled"); }, 
        \'onAddQueueItem\'   : function(file) { $("#emp").css("display", "none"); }, 
        \'onCancel\'         : function(file) { $("#fsubmit").attr("disabled", "disabled"); $("#fname").val(""); $("div.complete").css(\'display\', \'none\'); setTimeout("$(\'#emp\').css(\'display\', \'block\');", 1000); } 
    });
});
</script>
';


}


?>
