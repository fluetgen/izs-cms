<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth_d');
}

require_once('../assets/classes/class.mysql.php');

require_once('classes/class.informationprovider.php');
require_once('classes/class.anfragestelle.php');
require_once('classes/class.event.php');


$objIP = new InformationProvider;
$objAnfragestelle = new Anfragestelle;
$objEvent = new Event;

$boolCronSet = false;
$arrInformationProvider = array();
$arrIpAmpel = array();

if (isset($_REQUEST['startCron']) && ($_REQUEST['startCron'] != '') && !isset($_REQUEST['sendEmail'])) {

  $strSql = 'UPDATE `_cron_url` SET `cu_status` = 1 WHERE `cu_status` = 0 AND `cu_hash` = "' .$_REQUEST['startCron'] .'" AND `cu_url` NOT LIKE "%createPreview%"';
  $arrRow = MySQLStatic::Update($strSql);
  $boolCronSet = true;
  
}

if (isset($_REQUEST['sendEmail']) && ($_REQUEST['sendEmail'] == 1)) {

  $strSql = 'UPDATE `_cron_url` SET `cu_status` = 1 WHERE `cu_status` = 0 AND `cu_hash` = "' .$_REQUEST['startCron'] .'" AND `cu_url` LIKE "%createPreview%"';
  $arrRow = MySQLStatic::Update($strSql);
  
}

$strHashVollmacht = md5(uniqid(rand(), true));
$strHashAnfrage = md5(uniqid(rand(), true));

$strOutput = '';
$arrRequestWay = array();
$arrDecentral = array();

$strOutput.= '<h1>Anfrage nach BNR</h1>' .chr(10);

$strSql0 = "SELECT DISTINCT `Beitragsmonat__c`, `Beitragsjahr__c` FROM `cron_izs_sv_month` ORDER BY `Beitragsjahr__c` DESC, `Beitragsmonat__c` DESC";
$arrResult0 = MySQLStatic::Query($strSql0);
if (count($arrResult0) > 0) {
  
  $strOutput.= '<form method="post" action="">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);

  $strOutput.= '<table>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Zeitraum: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelDate" id="strSelDate" onchange="this.form.submit()">' .chr(10);
  
  $intPeriod = 0;

  foreach ($arrResult0 as $arrPeriod0) {
    $strSelected = '';
    $strValue = $arrPeriod0['Beitragsmonat__c'] .'_' .$arrPeriod0['Beitragsjahr__c'];
    
    if (empty($_REQUEST['strSelDate']) && ($intPeriod == 0)) {
      $_REQUEST['strSelDate'] = $strValue;
      //$_REQUEST['send'] = 1;
    }
    
    if ($strValue == $_REQUEST['strSelDate']) {
      $strSelected = ' selected="selected"';
    } 
    $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .str_pad($arrPeriod0['Beitragsmonat__c'], 2 ,'0', STR_PAD_LEFT) .'-' .$arrPeriod0['Beitragsjahr__c'] .'</option>' .chr(10);
    $intPeriod++;
  }
  
  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);


  $strSql5 = 'SELECT * FROM `izs_informationprovider` WHERE `Anfrage` = "Anfrage nach BNR" ORDER BY `Name`';
  $arrResult5 = MySQLStatic::Query($strSql5);
  if (count($arrResult5) > 0) {
    
    if (($_REQUEST['strSelIp'] == 'all') || ($_REQUEST['strSelIp'] == '')) {
      $strSelectedAll = ' selected="selected"';
      $_REQUEST['strSelIp'] = '';
    } else {
      $strSelectedAll = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Information Provider: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelIp" id="strSelIp" onchange="this.form.submit()">' .chr(10);
    $strOutput.= '    <option value=""' .$strSelectedAll .'>Alle</option>' .chr(10);
    
    foreach ($arrResult5 as $arrReqIp) {
      $strSelected = '';

      if ($arrReqIp['Id'] == $_REQUEST['strSelIp']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option value="' .$arrReqIp['Id'] .'"' .$strSelected .'>' .$arrReqIp['Name'] .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }

  $arrResult5 = [
    ['SF42_Anfrageweg__c' => "E-Mail"],
    ['SF42_Anfrageweg__c' => "Fax"],
    ['SF42_Anfrageweg__c' => "Post"],
    ['SF42_Anfrageweg__c' => "Webmailer"]
  ];

  //$strSql5 = 'SELECT `Anfrageweg__c` FROM `Anfragestelle__c`WHERE `Anfrageweg__c` != "" GROUP BY `Anfrageweg__c` ORDER BY `Anfrageweg__c`';
  //$arrResult5 = MySQLStatic::Query($strSql5);
  if (count($arrResult5) > 0) {
    
    if (($_REQUEST['strSelWay'] == 'all') || ($_REQUEST['strSelWay'] == '')) {
      $strSelectedAll = ' selected="selected"';
      $_REQUEST['strSelWay'] = 'all';
    } else {
      $strSelectedAll = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Anfrageweg: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelWay" id="strSelWay" onchange="this.form.submit()">' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>Alle</option>' .chr(10);
    
    foreach ($arrResult5 as $arrReqWay) {
      $strSelected = '';

      if ($arrReqWay['Anfrageweg__c'] == $_REQUEST['strSelWay']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option value="' .$arrReqWay['Anfrageweg__c'] .'"' .$strSelected .'>' .$arrReqWay['Anfrageweg__c'] .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
    
  $strSql6 = 'SELECT `ct_id`, `ct_name` FROM `cms_text` WHERE `ct_type` = 0 ';
  if (($_REQUEST['strSelWay'] != '') && ($_REQUEST['strSelWay'] != 'all')) {
    $strSql6.= 'AND `ct_category` = "' .$_REQUEST['strSelWay'] .'" ';
  }  
  $strSql6.= 'ORDER BY `ct_name`';
  $arrResult6 = MySQLStatic::Query($strSql6);
  if (count($arrResult6) > 0) {
    
    if ($_REQUEST['strSelText'] == '') {
      $strClass = ' class="red"';
    } else {
      $strClass = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Textvorlage: </td>' .chr(10);
    $strOutput.= '    <td><select' .$strClass .' name="strSelText" id="strSelText" onchange="this.form.submit()">' .chr(10);
    $strOutput.= '    <option value="">Bitte auswählen</option>' .chr(10);
    
    foreach ($arrResult6 as $arrText) {
      $strSelected = '';

      if ($arrText['ct_id'] == $_REQUEST['strSelText']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option class="black" value="' .$arrText['ct_id'] .'"' .$strSelected .'>' .$arrText['ct_name'] .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
  
  $arrRueckSel = array('rot' => 'offen', 'gruen' => 'vollständig');

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Rücklauf: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelRuec" id="strSelRuec">' .chr(10);
  $strOutput.= '    <option value=""></option>' .chr(10);
  
  foreach ($arrRueckSel as $strKey => $strText) {
    $strSelected = '';

    if ($strKey == $_REQUEST['strSelRuec']) {
      $strSelected = ' selected="selected"';
    } 
    $strOutput.= '    <option value="' .$strKey .'"' .$strSelected .'>' .$strText .'</option>' .chr(10);
  }

  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td></td>' .chr(10);
  $strOutput.= '    <td><input type="submit" value="anzeigen"></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);
    
}

if (!isset($_REQUEST['strSelWay'])) {
  $_REQUEST['strSelWay'] = 'all';
}

if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {
  
  $arrAnfrageListeAdd = array();
  
  $arrPeriod = explode('_', $_REQUEST['strSelDate']);

  $strSql3 = 'SELECT * FROM `Account` WHERE `Abweichender_Anfrageprozess__c` = "Anfrage nach BNR" AND `SF42_isInformationProvider__c` = "true" ';
  if (isset($_REQUEST['strSelIp']) && ($_REQUEST['strSelIp'] != '')) {
    $strSql3.= 'AND `Id` = "' .$_REQUEST['strSelIp'] .'" ';
  }
  $strSql3.= 'ORDER BY `Name`';
  $arrKKList = MySQLStatic::Query($strSql3);

  if ($_SESSION['id'] == 3) {
    //echo $strSql3; die();
  }

  $arrPpList = [];
  $strSql = 'SELECT `Id`, `SF42_Comany_ID__c` FROM `Account` WHERE `SF42_Comany_ID__c` != ""';
  $arrSql = MySQLStatic::Query($strSql);

  if (count($arrSql) > 0) {
    foreach ($arrSql as $int => $arrIp) {
      $arrPpList[$arrIp['SF42_Comany_ID__c']] = $arrIp;
    }
  }

  //Get all CATCH ALL Anfragestellen
  $strSqlAllCa = 'SELECT * FROM `Anfragestelle__c` WHERE `CATCH_ALL__c` = "true"';
  $arrSqlAllCa = MySQLStatic::Query($strSqlAllCa);

  $arrCatchAllList = [];
  if (count($arrSqlAllCa) > 0) {
    foreach ($arrSqlAllCa as $intKey => $arrAnfragestelle) {
      $arrCatchAllList[$arrAnfragestelle['Information_Provider__c']] = $arrAnfragestelle;
    }
  }
  
  foreach ($arrKKList as $intAcc => $arrKK) {
    
    //echo $arrKK['Name'] .'<br />' .chr(10);

    $start = microtime(true);   //ZEIT

    $arrCatchAll = isset($arrCatchAllList[$arrKK['Id']]) ? [0 => $arrCatchAllList[$arrKK['Id']]] : [];

    if ($arrIpAmpel[$arrCatchAll[0]['Id']] != 'rot') {
      $arrIpAmpel[$arrCatchAll[0]['Id']] = 'gruen';
    }

    if ($_SERVER['REMOTE_ADDR'] == '193.174.158.110') {
      //echo $strSqlCA .'<br />' .chr(10);
    }
    
    //print_r($arrCatchAll);die();
    
    if ((count($arrCatchAll) > 0) && ($arrCatchAll[0]['Abweichender_Anfrageprozess__c'] == 'Anfrage nach BNR')) {

      if ($_SERVER['REMOTE_ADDR'] == '193.174.158.110') {
        //echo $arrKK['Name'] .' -> ' .$arrCatchAll[0]['Name'] .'<br />' .chr(10);
      }
      
      $strSql6 = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Beitragsmonat__c` = "' .$arrPeriod[0] .'" AND `Beitragsjahr__c` = "' .$arrPeriod[1] .'" ';
      $strSql6.= 'AND (`SF42_EventStatus__c` != "abgelehnt / Dublette") AND (`SF42_EventStatus__c` != "zurückgestellt von IZS") AND (`SF42_EventStatus__c` != "OK") ';
      $strSql6.= 'AND `SF42_informationProvider__c` = "' .$arrKK['Id'] .'" GROUP BY `Betriebsnummer_ZA__c`';
      $arrEventList = MySQLStatic::Query($strSql6);
      
      //echo count($arrEventList); die();

      foreach ($arrEventList as $intEv => $arrEvent) {

        //$strSqlAc = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c`="' .$arrEvent['Betriebsnummer_ZA__c'] .'"';
        //$arrAc = MySQLStatic::Query($strSqlAc);

        $arrAc[0]['Id'] = $arrPpList[$arrEvent['Betriebsnummer_ZA__c']]['Id'];

        $strSql8 = 'SELECT `Id`, `Anfragestelle__c` FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrAc[0]['Id'] .'" AND `Information_Provider__c` = "' .$arrKK['Id'] .'"';

        //echo $strSql8 .chr(10);

        $arrResult8 = MySQLStatic::Query($strSql8);

        
        //keinem Team zugeordnet
        if (count($arrResult8) == 0) {
          
          $arrAnfrageListeAdd[] = array(
            'evid' => $arrEvent['evid'],
            'EventName' => $arrEvent['Name'],
            'SF42_Premium_Payer__c' => $arrEvent['SF42_Premium_Payer__c'],
            'Betriebsnummer_ZA__c' => $arrEvent['Betriebsnummer_ZA__c'],
            'Id' => $arrCatchAll[0]['Id'], 
            'Anfrageweg__c' => $arrCatchAll[0]['Anfrageweg__c'], 
            'Name' => $arrCatchAll[0]['Name'], 
            'SF42_EventStatus__c' => $arrEvent['SF42_EventStatus__c'], 
            'Rueckmeldung_am__c' => $arrEvent['Rueckmeldung_am__c'],
            'IP' => $arrEvent['SF42_informationProvider__c']
          );

          $arrInformationProvider[$arrCatchAll[0]['Id']] = $arrCatchAll[0]['Name'];
          $arrRequestWay[$arrCatchAll[0]['Id']] = $arrCatchAll[0]['Anfrageweg__c'];
        }

      }

      
    }
    

    $end = microtime(true);     //ZEIT
    $laufzeit = $end - $start;  //ZEIT
    //echo "Laufzeit: ".$laufzeit." Sekunden!" .'<br />' .chr(10); //ZEIT
    //die();
      
    
  }
  
  $strSqlNeu = '
  SELECT `evid`, `SF42_IZSEvent__c`.`Name` AS `EventName`, `SF42_IZSEvent__c`.`SF42_Premium_Payer__c`, `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c`, `Anfragestelle__c`.Id AS `Id`, Anfrageweg__c, `Anfragestelle__c`.Name AS `Name`, 
  `SF42_EventStatus__c`, `Rueckmeldung_am__c`, `SF42_IZSEvent__c`.`Id` AS `EventId`, `Anfragestelle__c`.`Information_Provider__c` AS `IP` 
  FROM `Dezentrale_Anfrage_KK__c` 
  INNER JOIN `Account` ON `Dezentrale_Anfrage_KK__c`.Premium_Payer__c = `Account`.Id 
  INNER JOIN `Account` AS `Account2` ON `Dezentrale_Anfrage_KK__c`.Information_Provider__c = `Account2`.Id 
  INNER JOIN `SF42_IZSEvent__c` ON ((`SF42_IZSEvent__c`.`SF42_informationProvider__c` = `Dezentrale_Anfrage_KK__c`.Information_Provider__c) AND (`Account`.SF42_Comany_Id__c = `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c`))
  INNER JOIN `Anfragestelle__c` ON `Anfragestelle__c`.Id = `Dezentrale_Anfrage_KK__c`.Anfragestelle__c 
  WHERE `Beitragsmonat__c` = "' .$arrPeriod[0] .'" AND (`SF42_EventStatus__c` != "abgelehnt / Dublette") AND (`SF42_EventStatus__c` != "zurückgestellt von IZS") AND (`SF42_EventStatus__c` != "OK") 
  AND `SF42_IZSEvent__c`.`Beitragsjahr__c` = "' .$arrPeriod[1] .'" AND ((`Anfragestelle__c`.`Abweichender_Anfrageprozess__c` = "Anfrage nach BNR") AND (`Anfragestelle__c`.`Information_Provider__c` != "001300000109k5wAAA")) '; // NICHT DAK = Übergangslösung bis Anfragestellen gelöscht werden können

  if (isset($_REQUEST['strSelIp']) && ($_REQUEST['strSelIp'] != '')) {
    $strSqlNeu.= 'AND `SF42_IZSEvent__c`.`SF42_informationProvider__c` = "' .$_REQUEST['strSelIp'] .'" ';
  }

  $strSqlNeu.= 'ORDER BY `Name`, `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c`, `SF42_IZSEvent__c`.`SF42_Premium_Payer__c`
  '; // (OR (`SF42_EventStatus__c` = "to enquire")) AND 
  
  $arrAnfrageListe = MySQLStatic::Query($strSqlNeu);
  
  $arrAnfrageListe = array_merge($arrAnfrageListe, $arrAnfrageListeAdd);
  
  /*
  if (is_array($arrAnfrageListe) && (count($arrAnfrageListe) > 0)) {
    foreach ($arrAnfrageListe as $arrAList) {
      
      if ($arrIpAmpel[$arrAList['Id']] == '') {
        $arrIpAmpel[$arrAList['Id']] = 'gruen';
      }

      if (($arrIpAmpel[$arrAList['Id']] == 'gruen') && (($arrAList['SF42_EventStatus__c'] == 'enquired') && (($arrAList['Rueckmeldung_am__c'] == '0000-00-00') || ($arrAList['Rueckmeldung_am__c'] == '')))) {
        $arrIpAmpel[$arrAList['Id']] = 'rot';
      } 
      
      $arrInformationProvider[$arrAList['Id']] = $arrAList['Name'];
      $arrRequestWay[$arrAList['Id']] = $arrAList['Anfrageweg__c'];
              
    }
  }
  */

  //print_r($arrAnfrageListe);

  $arrCreateUrlList = array();
 
  
  if (count($arrAnfrageListe) > 0) {

    $strSql = 'SELECT `ct_id`, `ct_ip` FROM `cms_text` WHERE `ct_type` = 0 AND `ct_request` =  "bnr" AND `ct_process` = "request"';
    $arrRes = MySQLStatic::Query($strSql);

    $arrText = array();
    if (count($arrRes) > 0) {
      foreach ($arrRes as $intKey => $arrTextRaw) {
        $arrText[$arrTextRaw['ct_ip']] = $arrTextRaw['ct_id'];
      }
    }

    //print_r($arrText);
    
    $strOutputTable.= '<table class="" id="5">' .chr(10);
    $strOutputTable.= '  <thead>' .chr(10);
    $strOutputTable.= '    <tr>' .chr(10);
  	$strOutputTable.= '      <th class="">Event Id</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Information Provider</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Premium Payer</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Rücklauf</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Anschreiben</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Vollmacht</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">erzeugt am</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Anfrageliste</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">erzeugt am</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">xlsx</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Anfrageweg</th>' .chr(10); //
  	$strOutputTable.= '      <th class="">versandt am</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Anlieferung bis</th>' .chr(10); //class="header"
    $strOutputTable.= '    </tr>' .chr(10);
    $strOutputTable.= '  </thead>' .chr(10);
    $strOutputTable.= '  <tbody>' .chr(10);
    
    $arrVollmachtLink = array();
    $arrAnfrageLink   = array();
    $arrSendLink      = array();
    
    $arrCountEntries = 0;

    $arrBackList = [];
    $strSqlB = 'SELECT `cb_id`, `cb_date`, `cb_account_id` FROM `cms_back` WHERE `cb_month` = "' .$arrPeriod[0] .'" AND `cb_year` = "' .$arrPeriod[1] .'"';
    $arrBack = MySQLStatic::Query($strSqlB);
    if (count($arrBack) > 0) {
      foreach ($arrBack as $intKey => $arrBackRaw) {
        $arrBackList[$arrBackRaw['cb_account_id']] = $arrBackRaw;
      }
    }

    $arrOrgList = [];
    $strSql7 = 'SELECT `Id`, `Nur_neue_Vollmachten__c`, `Originalvollmacht_ben_tigt__c` FROM `Account`'; // WHERE `Id` ="' .$strId .'"
    $arrOrgReq  = MySQLStatic::Query($strSql7);
    if (count($arrOrgReq) > 0) {
      foreach ($arrOrgReq as $intKey => $arrAccountRaw) {
        $strKey = $arrAccountRaw['Id'];
        unset($arrAccountRaw['Id']);
        $arrOrgList[$strKey] = [0 => $arrAccountRaw];
      }
    }

    $arrCertList = [];
    $strSql = 'SELECT `ca_id`, DATE_FORMAT(`ca_created`, "%d.%m.%Y %H:%i:%s") AS `ca_created`, `ca_account_id` FROM `cms_auth` WHERE `ca_month` = "' .$arrPeriod[0] .'" AND `ca_year` = "' .$arrPeriod[1] .'"'; // `ca_account_id` = "' .$strId .'" AND 
    $arrCert = MySQLStatic::Query($strSql);
    if (count($arrCert) > 0) {
      foreach ($arrCert as $intKey => $arrCertRaw) {
        $strKey = $arrCertRaw['ca_account_id'];
        unset($arrCertRaw['ca_account_id']);
        $arrCertList[$strKey] = [0 => $arrCertRaw];
      }
    }

    $arrReqList = [];
    $strSql = 'SELECT `cr_id`, DATE_FORMAT(`cr_created`, "%d.%m.%Y %H:%i:%s") AS `cr_created`, `cr_account_id` FROM `cms_req` WHERE `cr_month` = "' .$arrPeriod[0] .'" AND `cr_year` = "' .$arrPeriod[1] .'"'; // `cr_account_id` = "' .$strId .'" AND 
    $arrReq = MySQLStatic::Query($strSql);
    if (count($arrReq) > 0) {
      foreach ($arrReq as $intKey => $arrReqRaw) {
        $strKey = $arrReqRaw['cr_account_id'];
        unset($arrReqRaw['cr_account_id']);
        $arrReqList[$strKey] = [0 => $arrReqRaw];
      }
    }

    $arrSentList = [];
    $strSql = 'SELECT `cs_id`, DATE_FORMAT(`cs_sent`, "%d.%m.%Y %H:%i:%s") AS `cs_sent`, `cs_account_id` FROM `cms_sent` WHERE `cs_month` = ' .$arrPeriod[0] .' AND `cs_year` = ' .$arrPeriod[1] .''; // `cs_account_id` = "' .$strId .'" AND 
    $arrSent = MySQLStatic::Query($strSql);
    if (count($arrSent) > 0) {
      foreach ($arrSent as $intKey => $arrSentRaw) {
        $strKey = $arrSentRaw['cs_account_id'];
        unset($arrSentRaw['cs_account_id']);
        $arrSentList[$strKey] = [0 => $arrSentRaw];
      }
    }

    foreach ($arrAnfrageListe as $arrEvent) {
      
      //echo '  ' .$strName .'<br />' .chr(10);
      
      if ((isset($_REQUEST['strSelWay']) && ($_REQUEST['strSelWay'] == $arrEvent['Anfrageweg__c'])) || ($_REQUEST['strSelWay'] == 'all')) {
        
        if (($arrEvent['SF42_EventStatus__c'] == 'enquired') && (($arrEvent['Rueckmeldung_am__c'] == '0000-00-00') || ($arrEvent['Rueckmeldung_am__c'] == ''))) {
          $strIpAmpel = 'rot';
        } else {
          $strIpAmpel = 'gruen';
        }

        if ((isset($_REQUEST['strSelRuec']) && ($_REQUEST['strSelRuec'] == $strIpAmpel)) || ($_REQUEST['strSelRuec'] == '')) {


          $arrBack['cb_date'] = '';
          //$strSqlB = 'SELECT `cb_id`, `cb_date` FROM `cms_back` WHERE `cb_month` = "' .$arrPeriod[0] .'" AND `cb_year` = "' .$arrPeriod[1] .'" AND `cb_evid` ="' .$arrEvent['evid'] .'"';
          //$arrBack = MySQLStatic::Query($strSqlB);

          $arrBack = isset($arrBackList[$strId]) ? [0 => $arrBackList[$arrEvent['evid']]] : [];
          
          if ((count($arrBack) > 0) && (@$arrBack[0]['cb_date'] != '0000-00-00')) {
          
            $arrDate = explode('-', $arrBack[0]['cb_date']);
            $arrBack['cb_date'] = $arrDate[2] .'.' .$arrDate[1] .'.' .$arrDate[0];
            $arrBack['cb_id'] = $arrBack[0]['cb_id'];
            
            $intDiff = ((mktime(0, 0, 0, date('m'), date('d'), date('Y')) - mktime(0, 0, 0, $arrDate[1], $arrDate[2], $arrDate[0])) / 86400);
            
            if ($intDiff >= 0) {
              $strClassBack = ' red';
            } else {
              $strClassBack = '';
            }
          
          } else {
            $arrBack['cb_id'] = $arrPeriod[1] .'-' .$arrPeriod[0] .'-' .@$strId .'-' .$arrEvent['evid'];
          }



          $strOutputTable.= '    <tr>' .chr(10);
          $strOutputTable.= '      <td>' .$arrEvent['EventName'] .'</td>' .chr(10);

          $arrIp = $objAnfragestelle->arrGetIpFromAnfragestelle($arrEvent['Id']);

          $strOutputTable.= '      <td><span class="hidden">' .$arrEvent['Name'] .'</span><a href="index_neu.php?ac=contacts&acid=' .$arrIp[0]['acid'] .'" title="Zeige Kontakte" target="_blank">' .$arrEvent['Name'] .'</a></td>' .chr(10);
          $strOutputTable.= '      <td>' .$arrEvent['SF42_Premium_Payer__c'] .'</td>' .chr(10);
          $strOutputTable.= '      <td><img src="/assets/images/sys/ampel_' .$strIpAmpel .'.png" alt="' .$arrEvent['SF42_EventStatus__c'] .'" title="' .$arrEvent['SF42_EventStatus__c'] .' (Rückmeldung: ' .$arrEvent['Rueckmeldung_am__c'] .')"></td>' .chr(10);

          if ($arrEvent['Anfrageweg__c'] == 'E-Mail') {
            $strOutputTable.= '      <td style="text-align: center;"><img src="img/icon_email.png" class="ic_preview_m" id="e_' .$arrEvent['Id'] .'" rel="' .$arrEvent['evid'] .'" /></td>' .chr(10);
          } else {
            $strOutputTable.= '      <td align="center"><img src="img/icon_pdf.png" class="ic_preview" id="p_' .$arrEvent['Id'] .'" /></td>' .chr(10);
          }

          /*
          $strVollmachtLink = 'createVollmacht.neu.php?DezId=' .$arrEvent['Id'] .'&Name=' .rawurlencode($arrEvent['Name']) .'&strSelDate=' .$_REQUEST['strSelDate'] .'&strSelText=' .$_REQUEST['strSelText'] .'&strSelWay=' .$_REQUEST['strSelWay'] .'&strSelRuec=' .$_REQUEST['strSelRuec'] .'&show=C&evid=' .$arrEvent['evid'];
          $arrVollmachtLink[] = $strVollmachtLink;
          */

          //$strSql7 = 'SELECT `Nur_neue_Vollmachten__c`, `Originalvollmacht_ben_tigt__c` FROM `Account` WHERE `Id` ="' .$arrIp[0]['acid'] .'"';
          //$arrOrg  = MySQLStatic::Query($strSql7);

          $arrOrg = $arrOrgList[$arrIp[0]['acid']] ?? [];

          $strVollmachtLink = 'createVollmacht.neu.php?DezId=' .$arrEvent['Id'] .'&Name=' .rawurlencode($arrEvent['Name']) .'&strSelDate=' .$_REQUEST['strSelDate'] .'&strSelText=' .$_REQUEST['strSelText'] .'&strSelWay=' .$_REQUEST['strSelWay'] .'&strSelRuec=' .$_REQUEST['strSelRuec'] .'&show=C&evid=' .$arrEvent['evid'];
          $strVollmachtLinkNeu = 'createVollmacht.neu.php?DezId=' .$arrEvent['Id'] .'&Name=' .rawurlencode($arrEvent['Name']) .'&strSelDate=' .$_REQUEST['strSelDate'] .'&strSelText=' .$_REQUEST['strSelText'] .'&strSelWay=' .$_REQUEST['strSelWay'] .'&strSelRuec=' .$_REQUEST['strSelRuec'] .'&show=C&evid=' .$arrEvent['evid'] .'&new=1';

          $arrVollmachtLink[] = $strVollmachtLink;
          
          $arrDiff = null;
          
          /*
          if ($arrOrg[0]['Nur_neue_Vollmachten__c'] == 'true') {
          	$boolDiff = $objEvent->boolEventIsNew($arrEvent['EventId'], $arrPeriod[0], $arrPeriod[1]);
            
            $strOutputTable.= '      <td><a href="' .$strVollmachtLink .'">alle</a> / ';
            if (count($arrDiff) > 0) { 
              $strOutputTable.= '<a href="' .$strVollmachtLinkNeu .'">neue (' .count($arrDiff) .')</a>';
              $arrVollmachtLink[] = $strVollmachtLinkNeu;
            } else {
              $strOutputTable.= 'neue (0)';
            }
            $strOutputTable.= '</td>' .chr(10);
            
          } else {
            $strOutputTable.= '      <td><a href="' .$strVollmachtLink .'">erzeugen</a></td>' .chr(10);
          }
          */
          
          $strOutputTable.= '      <td><a href="' .$strVollmachtLink .'">erzeugen</a></td>' .chr(10);
  
          //$strSql = 'SELECT `ca_id`, DATE_FORMAT(`ca_created`, "%d.%m.%Y %H:%i:%s") AS `ca_created` FROM `cms_auth` WHERE `ca_account_id` = "' .$arrEvent['Id'] .'" AND `ca_month` = "' .$arrPeriod[0] .'" AND `ca_year` = "' .$arrPeriod[1] .'" AND `ca_evid` = "' .$arrEvent['evid'] .'"';
          //$arrCert = MySQLStatic::Query($strSql);

          $arrCert = $arrCertList[$arrEvent['Id']] ?? [];
          
          if (count($arrCert) > 0) {
            $arrDate = explode(' ', $arrCert[0]['ca_created']);
            $strFileName = 'pdf/vollmacht/' .$arrEvent['Id'] .'_' .$arrEvent['evid'] .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_vollmacht_sv.pdf?' .substr(md5(uniqid(mt_rand(), true)), 0, 4);
            $strOutputTable.= '      <td><a href="' .$strFileName .'" target="_blank" alt="' .$arrCert[0]['ca_created'] .'" title="' .$arrCert[0]['ca_created'] .'" id="v_' .$arrEvent['Id'] .'">' .$arrDate[0] .'</a></td>' .chr(10);
          } else {
            $strOutputTable.= '      <td></td>' .chr(10);
          }
  
  
          $strAnfrageLink = 'createAnfrageliste.php?DezId=' .$arrEvent['Id'] .'&Name=' .rawurlencode($arrEvent['Name']) .'&strSelDate=&strSelDate=' .$_REQUEST['strSelDate'] .'&strSelText=' .$_REQUEST['strSelText'] .'&strSelWay=' .$_REQUEST['strSelWay'] .'&strSelRuec=' .$_REQUEST['strSelRuec'] .'&show=D&evid=' .$arrEvent['evid'] .'&clid=' .$_SESSION['id'];
          $arrAnfrageLink[] = $strAnfrageLink;
  
          $strOutputTable.= '      <td><a href="' .$strAnfrageLink .'">erzeugen</a></td>' .chr(10);
  
          //$strSql = 'SELECT `cr_id`, DATE_FORMAT(`cr_created`, "%d.%m.%Y %H:%i:%s") AS `cr_created` FROM `cms_req` WHERE `cr_account_id` = "' .$arrEvent['Id'] .'" AND `cr_month` = "' .$arrPeriod[0] .'" AND `cr_year` = "' .$arrPeriod[1] .'" AND `cr_evid` = "' .$arrEvent['evid'] .'"';
          //$arrReq = MySQLStatic::Query($strSql);

          $arrReq = $arrReqList[$arrEvent['Id']] ?? [];
          
          if (count($arrReq) > 0) {
            $arrDate = explode(' ', $arrReq[0]['cr_created']);
            $strFileName = 'pdf/anfrage/' .$arrEvent['Id'] .'_' .$arrEvent['evid'] .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_anfrage_sv.pdf';
            $strOutputTable.= '      <td><a href="' .$strFileName .'?' .substr(md5(uniqid(mt_rand(), true)), 0, 4) .'" target="_blank" alt="' .$arrReq[0]['cr_created'] .'" title="' .$arrReq[0]['cr_created'] .'" id="a_' .$arrEvent['Id'] .'">' .$arrDate[0] .'</a></td>' .chr(10);
            
            $strFileNameXlsx = str_replace('.pdf', '.xlsx', $strFileName);
            if (file_exists($strFileNameXlsx)) {
              $strOutputTable.= '      <td><a href="' .$strFileNameXlsx .'?' .substr(md5(uniqid(mt_rand(), true)), 0, 4) .'" target="_blank" alt="' .$arrReq[0]['cr_created'] .'" title="' .$arrReq[0]['cr_created'] .'" id="xl_' .$strId .'"><img src="img/icon_xls.png" id="xi_' .$strId .'" /></a></td>' .chr(10);
            } else {
              $strOutputTable.= '      <td></td>' .chr(10);
            }
          } else {
            $strOutputTable.= '      <td></td>' .chr(10);
            $strOutputTable.= '      <td></td>' .chr(10);
          }
  
          if ($arrEvent['Anfrageweg__c'] == 'E-Mail') {
            $strOutputTable.= '      <td><span id="e_' .$arrEvent['Id'] .'" class="send" rel="' .$arrEvent['evid'] .'">' .$arrEvent['Anfrageweg__c'] .'</span></td>' .chr(10);
            
            //$strSql = 'SELECT `cs_id`, DATE_FORMAT(`cs_sent`, "%d.%m.%Y %H:%i:%s") AS `cs_sent` FROM `cms_sent` WHERE `cs_account_id` = "' .$arrEvent['Id'] .'" AND `cs_month` = ' .$arrPeriod[0] .' AND `cs_year` = ' .$arrPeriod[1] .' AND `cs_evid` = "' .$arrEvent['evid'] .'"';
            //$arrSent = MySQLStatic::Query($strSql);

            $arrSent = $arrSentList[$arrEvent['Id']] ?? [];

            if ($_REQUEST['strSelText'] != '') {
              $strSelText = $_REQUEST['strSelText'];
            } else {

              if (isset($arrText[$arrEvent['IP']])) {
                $strSelText = $arrText[$arrEvent['IP']];
              } else {
                $strSelText = $arrText[''];
              }

            }

            $arrGet = array(
              'ctId' => $strSelText, 
              'strSelWay' => $_REQUEST['strSelWay'], 
              'accId' => $arrEvent['Id'],  //Account ID
              'strSelDate' => $_REQUEST['strSelDate'], 
              'strSelText' => $strSelText, 
              'strSelRuec' => $_REQUEST['strSelRuec'], 
              'type' => $_REQUEST['strSelWay'], 
              'do' => 'sent',
              'evid' => $arrEvent['evid'],
              'dez' => 1                
            );

            $arrSendLink[] = 'createPreview.php?' .http_build_query($arrGet);
            
            if (count($arrSent) > 0) {
              $arrDate = explode(' ', $arrSent[0]['cs_sent']);
              $strOutputTable.= '      <td><span class="hidden">' .$arrDate[0] .'</span><a id="sd_' .$arrEvent['Id'] .'" alt="' .$arrSent[0]['cs_sent'] .'" title="' .$arrSent[0]['cs_sent'] .'">' .$arrDate[0] .'</a></td>' .chr(10);
            } else {
              //$strOutputTable.= '      <td>' .$arrEvent['Id'] .' -> ' .$strSelText .'</td>' .chr(10);
              $strOutputTable.= '      <td></td>' .chr(10);

            }
  
          } elseif ($arrEvent['Anfrageweg__c'] == 'Fax') {
            $strOutputTable.= '      <td><span id="f_' .$arrEvent['Id'] .'" class="send" rel="' .$arrEvent['evid'] .'">' .$arrEvent['Anfrageweg__c'] .'</span></td>' .chr(10);
            
            //$strSql = 'SELECT `cs_id`, DATE_FORMAT(`cs_sent`, "%d.%m.%Y %H:%i:%s") AS `cs_sent` FROM `cms_sent` WHERE `cs_account_id` = "' .$arrEvent['Id'] .'" AND `cs_month` = ' .$arrPeriod[0] .' AND `cs_year` = ' .$arrPeriod[1] .' AND `cs_evid` = "' .$arrEvent['evid'] .'"';
            //$arrSent = MySQLStatic::Query($strSql);

            $arrSent = $arrSentList[$arrEvent['Id']] ?? [];
            
            if (count($arrSent) > 0) {
              $arrDate = explode(' ', $arrSent[0]['cs_sent']);
              $strOutputTable.= '      <td><span class="hidden">' .$arrDate[0] .'</span><a id="sd_' .$arrEvent['Id'] .'" alt="' .$arrSent[0]['cs_sent'] .'" title="' .$arrSent[0]['cs_sent'] .'">' .$arrDate[0] .'</a></td>' .chr(10);
            } else {
              $strOutputTable.= '      <td></td>' .chr(10);
            }
  
          } elseif ($arrEvent['Anfrageweg__c'] == 'Post') {
            $strOutputTable.= '      <td><span id="b_' .$arrEvent['Id'] .'" class="send" rel="' .$arrEvent['evid'] .'">' .$arrEvent['Anfrageweg__c'] .'</span></td>' .chr(10);
            
            //$strSql = 'SELECT `cs_id`, DATE_FORMAT(`cs_sent`, "%d.%m.%Y %H:%i:%s") AS `cs_sent` FROM `cms_sent` WHERE `cs_account_id` = "' .$arrEvent['Id'] .'" AND `cs_month` = ' .$arrPeriod[0] .' AND `cs_year` = ' .$arrPeriod[1] .' AND `cs_evid` = "' .$arrEvent['evid'] .'"';
            //$arrSent = MySQLStatic::Query($strSql);

            $arrSent = $arrSentList[$arrEvent['Id']] ?? [];
            
            if (count($arrSent) > 0) {
              $arrDate = explode(' ', $arrSent[0]['cs_sent']);
              $strOutputTable.= '      <td><span class="hidden">' .$arrDate[0] .'</span><a id="sd_' .$arrEvent['Id'] .'" alt="' .$arrSent[0]['cs_sent'] .'" title="' .$arrSent[0]['cs_sent'] .'">' .$arrDate[0] .'</a></td>' .chr(10);
            } else {
              $strOutputTable.= '      <td></td>' .chr(10);
            }
  
          } else {
            $strOutputTable.= '      <td><span id="b_' .$arrEvent['Id'] .'" class="send" rel="' .$arrEvent['evid'] .'">' .$arrEvent['Anfrageweg__c'] .'</span></td>' .chr(10);
            $strOutputTable.= '      <td></td>' .chr(10);
          }
          
          $strOutputTable.= '      <td><span id="h_date_' .$arrBack['cb_id']  .'" class="hidden">' .$arrBack['cb_date'] .'</span><input type="text" id="date_' .$arrBack['cb_id']  .'" class="date update_field' .$strClassBack .' selD" name="cb_date" value="' .$arrBack['cb_date'] .'" autocomplete="off"></td>' .chr(10);
          
          $strOutputTable.= '    </tr>' .chr(10);
          
          $arrCountEntries++;
        
        }
        
      }
      
    }
    
    $strOutputTable.= '  </tbody>' .chr(10);
    $strOutputTable.= '</table>' .chr(10);
    
    $strOutput.= '<div class="clearfix" style="height: 48px; width: 1680px;">' .chr(10);
    $strOutput.= '  <div class="form-left">' .chr(10);
    $strOutput.= '    <h3>' .$arrCountEntries .' Ergebnisse gefunden:</h3>' .chr(10);
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' .chr(10);

    if ($_REQUEST['startType'] == 'auth') {
      $strOutput.= '<span class="buttons">Dokumente werden erzeugt...</span>' .chr(10);
    } else {
      $strOutput.= '<form action="" method="post" class="buttons" style="display: inline;">' .chr(10);
      $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);
      $strOutput.= '<input type="hidden" name="startType" value="auth">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelDate" value="' .$_REQUEST['strSelDate'] .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelWay" value="' .$_REQUEST['strSelWay'] .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelText" value="' .$_REQUEST['strSelText'] .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="startCron" value="' .$strHashVollmacht .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelRuec" value="' .$_REQUEST['strSelRuec'] .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelIp" value="' .$_REQUEST['strSelIp'] .'">' .chr(10);
      $strOutput.= '<button id="doc-create" class="ui-button ui-state-default ui-corner-all">Dokumente erzeugen</button>' .chr(10);
      $strOutput.= '</form> ' .chr(10);
    }
    if (isset($_REQUEST['sendEmail']) && ($_REQUEST['sendEmail'] == 1)) {
      $strOutput.= '<span class="buttons">Dokumente werden versendet...</span>' .chr(10);
    } else {      
      $strOutput.= '<form action="" method="post" class="buttons" style="display: inline;">' .chr(10);
      $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);
      $strOutput.= '<input type="hidden" name="sendEmail" value="1">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelDate" value="' .$_REQUEST['strSelDate'] .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelWay" value="' .$_REQUEST['strSelWay'] .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelText" value="' .$_REQUEST['strSelText'] .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="startCron" value="' .$strHashVollmacht .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelRuec" value="' .$_REQUEST['strSelRuec'] .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelIp" value="' .$_REQUEST['strSelIp'] .'">' .chr(10);
      $strOutput.= '<button id="doc-send" class="ui-button ui-state-default ui-corner-all">Dokumente versenden</button>' .chr(10);
      $strOutput.= '</form> ' .chr(10);
    }

    $strOutput.= '    <form method="post" action="_get_csv.php" style="display: inline; float: right; padding-left: 10px;">';
    $strOutput.= '    <input type="hidden" name="strTable" value="' .str_replace('"', "'", $strOutputTable) .'">' .chr(10);
    $strOutput.= '    <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' .chr(10);
    $strOutput.= '    </form>';
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '</div>' .chr(10);


    $strOutput.= $strOutputTable;
   
    
    $strOutput.= "
<script>
$('#5').fixheadertable({ 
   colratio    : [80, 270, 270, 80, 100, 110, 110, 110, 110, 50, 110, 110, 110], 
   height      : 500, 
   width       : 1650, 
   zebra       : true, 
   sortable    : true,
   sortType    : ['string', 'string', 'string', 'string', 'string', 'string', 'date', 'string', 'date', 'string', 'string', 'date', 'date'],
   sortedColId : 1, 
   dateFormat  : 'd.m.Y'
});
</script>
";
          
  } else {
    
    $strOutput.= '<p>Keine Events im gewählten Zeitraum vorhanden.</p>' .chr(10);
    
  }

}


$strOutput.= '

<style>
td {
  padding-bottom: 5px;
}

#dialog_prev textarea {
  margin-bottom: 0px;
}
</style>

<div id="dialog_prev" title="Vorschau">
</div>

<script type="text/javascript">
	
$(document).ready(function() {

	$("#dialog_prev").dialog({
		bgiframe: true,
		autoOpen: false,
		resizable: false,
		height: 500,
		width: 590,
		modal: true,
		buttons: {
			\'OK\': function() {

		    $(this).dialog(\'close\');

			}
		},
		close: function() {

		}
  });
	
});	

</script>

  ';

  
$strSql = 'DELETE FROM `_cron_url` WHERE `cu_status` = 0 AND `cu_time` < DATE_ADD(CURDATE(), INTERVAL -1 DAY)';
$arrRow = MySQLStatic::Query($strSql);
  
$strSql = 'DELETE FROM `_cron_url` WHERE `cu_status` = 0 AND `cu_url` LIKE "%createPreview%"';
$arrRow = MySQLStatic::Query($strSql);

if (is_array($arrVollmachtLink) && (count($arrVollmachtLink) > 0)) {
  
  foreach ($arrVollmachtLink as $intId => $strLink) {
    
    $strSql = 'INSERT INTO `_cron_url` (`cu_id`, `cu_hash`, `cu_url`, `cu_time`, `cu_status`) VALUES (NULL, "' .$strHashVollmacht .'", "' .$strLink .'", NOW(), 0)';
    $intCt_id = MySQLStatic::Insert($strSql);
    
    $strSql = 'INSERT INTO `_cron_url` (`cu_id`, `cu_hash`, `cu_url`, `cu_time`, `cu_status`) VALUES (NULL, "' .$strHashVollmacht .'", "' .$arrAnfrageLink[$intId] .'", NOW(), 0)';
    $intCt_id = MySQLStatic::Insert($strSql);

    if (isset($arrSendLink[$intId]) && ($arrSendLink[$intId] != '')) {
      $strSql = 'INSERT INTO `_cron_url` (`cu_id`, `cu_hash`, `cu_url`, `cu_time`, `cu_status`) VALUES (NULL, "' .$strHashVollmacht .'", "' .$arrSendLink[$intId] .'", NOW(), 0)';
      $intCt_id = MySQLStatic::Insert($strSql);
    }

  }
  
}

/*

accId: "a0X30000008PLrHEAW"
ctId: "2021"
dez: 1
do: "sent"
evid: "584609"
strSelDate: "12_2018"
strSelRuec: ""
strSelText: "2021"
strSelWay: "all"
type: "E-Mail"

createPreview.php?ctId=2021
&strSelWay=E-Mail
&accId=a0X3000000DKHOTEA5
&strSelDate=12_2018
&strSelText=2021
&strSelRuec=
&type=E-Mail
&do=sent

print_r($arrSendLink);
print_r($arrVollmachtLink);
print_r($arrAnfrageLink);

*/

?>