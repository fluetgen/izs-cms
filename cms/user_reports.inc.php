<?php

session_start();

$intStundensatz = 13;
$intMaxUmsatz   = 450;

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

require_once('inc/refactor.inc.php');

$intUser = $_SESSION['id'];

$arrHeader = array(
  'user: ' .$intUser,
  'Accept: application/json'
);

if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
  $arrHeader[] = 'test: true';
}

if (isset($_REQUEST['strSelMonth'])) {
    $arrPart  = explode('-', $_REQUEST['strSelMonth']);
    $intMonth = (int) $arrPart[0];
    $intYear  = $arrPart[1];
} else {
    $intMonth = date('n');
    $intYear  = date('Y');
}


function secondsToTime($seconds) {

  $strSign = '';
  if ($seconds < 0) {
    $strSign = '-';
    $seconds = 0 - $seconds;
  }

  $hours = floor($seconds / (60 * 60));

  $divisor_for_minutes = $seconds % (60 * 60);
  $minutes = floor($divisor_for_minutes / 60);

  $divisor_for_seconds = $divisor_for_minutes % 60;
  $seconds = ceil($divisor_for_seconds);

  $obj = array(
      "h" => (int) $hours,
      "m" => (int) $minutes,
      "s" => (int) $seconds,
      "sign" => $strSign
   );

  return $obj;
}

function formatTime ($arrTime = array()) {

  return sprintf('%s%02d:%02d', $arrTime['sign'], $arrTime['h'], $arrTime['m']); //sprintf('%02d:%02d:%02d', $arrTime['h'], $arrTime['m'], $arrTime['s']);

}



$strOutput = '';
$strOutput.= '<h1>Benutzer - Berichte</h1>' .chr(10);


$strOutput.= '<form method="post" action="" style="padding: 20px 0 20px 0;" id="monthSel">' .chr(10);
$strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);

$strOutput.= '<table>' .chr(10);
$strOutput.= '<tbody>' .chr(10);

$strOutput.= '  <tr>' .chr(10);
$strOutput.= '    <td style="width:110px;">Monat: </td>' .chr(10);
$strOutput.= '    <td><select name="strSelMonth" id="strSelMonth">' .chr(10);

$intShowMonth = 12;

for ($intCount = 0; ($intCount < $intShowMonth); $intCount++) {

    $strMonth = date(('m-Y'), mktime(0, 0, 0, (date('n') - $intCount), 1, date('Y')));

    $strSelected = '';

    if ($strMonth == $_REQUEST['strSelMonth']) {
    $strSelected = ' selected="selected"';
    } 

    $strOutput.= '  <option value="' .$strMonth .'"' .$strSelected .'>' .$strMonth .'</option>' .chr(10);

}

$strOutput.= '    </select></td>' .chr(10);
$strOutput.= '  </tr>' .chr(10);


$strSql = 'SELECT `cu_clock_id` AS `id`, `cu_name` AS `name`, `izs_clock_user_info`.* FROM `izs_clock_user_info` INNER JOIN `izs_clock_user` ON `cu_id` = `ci_cu_id` WHERE `cu_active` = 1 AND `cu_role` != "owner" ORDER BY CAST(`ci_company` AS CHAR), `ci_type`, `cu_name`';
$arrUserList = MySQLStatic::Query($strSql);

$arrDone = array();

$strOutput.= '  <tr>' .chr(10);
$strOutput.= '    <td style="width:110px;">Firma: </td>' .chr(10);
$strOutput.= '    <td><select name="strSelCompany" id="strSelCompany">' .chr(10);

$intShowMonth = 12;

foreach ($arrUserList as $intKey => $arrUser) {

    $strCompany = $arrUser['ci_company'] .'|' .$arrUser['ci_type'];

    if (($intKey == 0) && ($_REQUEST['strSelCompany'] == '')) {
      $_REQUEST['strSelCompany'] = $strCompany;
    }

    $strSelected = '';

    if ($strCompany == $_REQUEST['strSelCompany']) {
      $strSelected = ' selected="selected"';
    } 
    
    if (!in_array($strCompany, $arrDone)) {

      $strHeadline = $arrUser['ci_company'];
      if ($arrUser['ci_type'] != '') {
        $strHeadline.= ' - ' .$arrUser['ci_type'];
      }

      $strOutput.= '  <option value="' .$strCompany .'"' .$strSelected .'>' .$strHeadline .'</option>' .chr(10);
      $arrDone[] = $strCompany;
    }

}

$strOutput.= '    </select></td>' .chr(10);
$strOutput.= '  </tr>' .chr(10);

$strOutput.= '</tbody>' .chr(10);



/*
$strOutput.= '  <tfoot>' .chr(10);
$strOutput.= '  <tr>' .chr(10);
$strOutput.= '    <td align="right" colspan="2"><input type="submit" name="Suche" value="Suche" /></td>' .chr(10);
$strOutput.= '  </tr>' .chr(10);
$strOutput.= '  </tfoot>' .chr(10);
*/
$strOutput.= '</table>' .chr(10);
$strOutput.= '</form>' .chr(10);


if (count($arrUserList) > 0) {

  $strOutput.= '<table class="" id="5">' .chr(10);
  $strOutput.= '
  <thead>
    <tr>
      <th>Name</th>
      <th>Übersicht</th>
    </tr>
  </thead>' .chr(10);
  //$strOutput.= '<tbody>' .chr(10);

  $intUser = 1;
  reset($arrUserList);

  foreach ($arrUserList as $intKey => $arrUser) {

    $strKey = $arrUser['ci_company'] .'|' .$arrUser['ci_type'];

    if ($strKey != $_REQUEST['strSelCompany']) {
      continue;
      //echo $strKey .' != ' .$_REQUEST['strSelCompany'] .chr(10);
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>' .$arrUser['name'] .'</td>' .chr(10);
    $strOutput.= '    <td><a href="javascript://" class="overview" rel="' .$arrUser['id'] .'">Download</a></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);

    $intUser++;

  }

    $strOutput.= '</table>' .chr(10);

    $strOutput.= '<p><a href="javascript://" class="summery">Deteillübersicht</a><br />
    <a href="javascript://" class="cash">Auszahlungsbeträge</a></p>' .chr(10); 

    $strOutput.= "
    <script>
    $('#5').fixheadertable({ 
     colratio    : [180, 80], 
     height      : " .($intUser * 24) .", 
     width       : 263, 
     zebra       : true, 
     resizeCol   : true,
     sortable    : true,
     sortType    : ['hidden', ''],
     sortedColId : 0, 
     dateFormat  : 'Y-m'
    });

    $('#strSelMonth').change(function(){
      $('#monthSel').submit();
    });

    $('#strSelCompany').change(function(){
      $('#monthSel').submit();
    });

    $('.overview').click(function() {
      var m = $('#strSelMonth').val();
      var c = $('#strSelCompany').val();
      var u = $(this).attr('rel');
      url = 'inc/user_report.ajax.php?type=overview&user=' + u + '&month=' + m + '&company=' + c;
      window.location.href = url;
    });

    $('.summery').click(function() {
      var m = $('#strSelMonth').val();
      var c = $('#strSelCompany').val();
      url = 'inc/user_report.ajax.php?type=summery&month=' + m + '&company=' + c;
      window.location.href = url;
    });

    $('.cash').click(function() {
      var m = $('#strSelMonth').val();
      var c = $('#strSelCompany').val();
      url = 'inc/user_report.ajax.php?type=cash&month=' + m + '&company=' + c;
      window.location.href = url;
    });

    </script>
    " .chr(10);


}

?>