<?php

session_start();

$start = microtime(true);

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

require_once('../assets/classes/class.mysql.php');
require_once('classes/class.informationprovider.php');

$objIP = new InformationProvider;

$boolCronSet = false;

$arrOrgName = array(
	'true' => 'ja',
	'false' => 'nein'
);

$end = microtime(true);
$durationInMs = ($end-$start) * 1000;
echo '<!-- Abchnitt 01: ' .round($durationInMs) .'ms -->' .chr(10);
$start = microtime(true);

if (isset($_REQUEST['startCron']) && ($_REQUEST['startCron'] != '')) {

  $strSql = 'UPDATE `_cron_url` SET `cu_status` = 1 WHERE `cu_status` = 0 AND `cu_hash` = "' .$_REQUEST['startCron'] .'"';
  $arrRow = MySQLStatic::Update($strSql);
  $boolCronSet = true;
  
}

$strHashVollmacht = md5(uniqid(rand(), true));
$strHashAnfrage = md5(uniqid(rand(), true));

$strOutput = '';
$arrRequestWay = array();
$arrDecentral = array();

$strOutput.= '<h1>Anfrage zentral</h1>' .chr(10);

//Schnittstellen
$strSchnittstellen = '';
$strSqlS = 'SELECT  `Id` FROM  `Account` WHERE  `Anfrageliste_XLS__c` =  "XLS + Vollmacht"';
$arrResS = MySQLStatic::Query($strSqlS);
if (count($arrResS) > 0) {
  $arrSchnitt = array();
  foreach ($arrResS as $intKey => $arrDataset) {
    $arrSchnitt[] = $arrDataset['Id'];
  }
  $strSchnittstellen = '"' .implode('", "', $arrSchnitt) .'"';
}

$strSql0 = "SELECT `Beitragsmonat__c`, `Beitragsjahr__c` FROM `cron_izs_sv_month` ORDER BY `Beitragsjahr__c` DESC, `Beitragsmonat__c` DESC";
$arrResult0 = MySQLStatic::Query($strSql0);
if (count($arrResult0) > 0) {
  
  $strOutput.= '<form method="post" action="">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);

  $strOutput.= '<table>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Zeitraum: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelDate" id="strSelDate" onchange="this.form.submit()">' .chr(10);
  
  $intPeriod = 0;

  foreach ($arrResult0 as $arrPeriod0) {
    $strSelected = '';
    $strValue = $arrPeriod0['Beitragsmonat__c'] .'_' .$arrPeriod0['Beitragsjahr__c'];
    
    if (empty($_REQUEST['strSelDate']) && ($intPeriod == 0)) {
      $_REQUEST['strSelDate'] = $strValue;
      //$_REQUEST['send'] = 1;
    }
    
    if ($strValue == $_REQUEST['strSelDate']) {
      $strSelected = ' selected="selected"';
    } 
    $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .str_pad($arrPeriod0['Beitragsmonat__c'], 2 ,'0', STR_PAD_LEFT) .'-' .$arrPeriod0['Beitragsjahr__c'] .'</option>' .chr(10);
    $intPeriod++;
  }
  
  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $arrResult5 = [
    ['SF42_Anfrageweg__c' => "E-Mail"],
    ['SF42_Anfrageweg__c' => "Fax"],
    ['SF42_Anfrageweg__c' => "Post"],
    ['SF42_Anfrageweg__c' => "Webmailer"]
  ];

  //$strSql5 = 'SELECT `SF42_Anfrageweg__c` FROM `Account` WHERE `SF42_Anfrageweg__c` != "" GROUP BY `SF42_Anfrageweg__c` ORDER BY `SF42_Anfrageweg__c`';
  //$arrResult5 = MySQLStatic::Query($strSql5);
  if (count($arrResult5) > 0) {
    
    if (($_REQUEST['strSelWay'] == 'all') || ($_REQUEST['strSelWay'] == '')) {
      $strSelectedAll = ' selected="selected"';
      $_REQUEST['strSelWay'] = 'all';
    } else {
      $strSelectedAll = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Anfrageweg: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelWay" id="strSelWay" onchange="this.form.submit()">' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>Alle</option>' .chr(10);

    foreach ($arrResult5 as $arrReqWay) {
      $strSelected = '';
      
      if ($arrReqWay['SF42_Anfrageweg__c'] == $_REQUEST['strSelWay']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option value="' .$arrReqWay['SF42_Anfrageweg__c'] .'"' .$strSelected .'>' .$arrReqWay['SF42_Anfrageweg__c'] .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
    
  $strSql6 = 'SELECT `ct_id`, `ct_name` FROM `cms_text` WHERE `ct_type` = 0 ';
  if (($_REQUEST['strSelWay'] != '') && ($_REQUEST['strSelWay'] != 'all')) {
    $strSql6.= 'AND `ct_category` = "' .$_REQUEST['strSelWay'] .'" ';
  }  
  $strSql6.= 'ORDER BY `ct_name`';
  $arrResult6 = MySQLStatic::Query($strSql6);
  if (count($arrResult6) > 0) {
    
    if ($_REQUEST['strSelText'] == '') {
      $strClass = ' class="red"';
    } else {
      $strClass = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Textvorlage: </td>' .chr(10);
    $strOutput.= '    <td><select' .$strClass .' name="strSelText" id="strSelText" onchange="this.form.submit()">' .chr(10);
    $strOutput.= '    <option value="">Bitte auswählen</option>' .chr(10);
    
    foreach ($arrResult6 as $arrText) {
      $strSelected = '';

      if ($arrText['ct_id'] == $_REQUEST['strSelText']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option class="black" value="' .$arrText['ct_id'] .'"' .$strSelected .'>' .$arrText['ct_name'] .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
  
  $arrRueckSel = array('gelb' => 'offen', 'gruen' => 'vollständig');

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Rücklauf: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelRuec" id="strSelRuec">' .chr(10);
  $strOutput.= '    <option value=""></option>' .chr(10);
  
  foreach ($arrRueckSel as $strKey => $strText) {
    $strSelected = '';

    if ($strKey == $_REQUEST['strSelRuec']) {
      $strSelected = ' selected="selected"';
    } 
    $strOutput.= '    <option value="' .$strKey .'"' .$strSelected .'>' .$strText .'</option>' .chr(10);
  }

  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);
  
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td></td>' .chr(10);
  $strOutput.= '    <td><input type="submit" value="anzeigen"></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);
    
}

$end = microtime(true);

$durationInMs = ($end-$start) * 1000;
echo '<!-- Abchnitt 02: ' .round($durationInMs) .'ms -->' .chr(10);
$start = microtime(true);


if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {
  
  $arrPeriod = explode('_', $_REQUEST['strSelDate']);

  $start = microtime(true);   //ZEIT
  
  $arrGroups = array();
  $arrInformationProvider = array();
  $arrIpAmpel = array();
  $arrEventStatus = array();
  $arrAngefragtStatus = array();
  $arrKlaerungStatus = array();
  
  //echo 'COUNT: ' .count($arrResult) .' Periods:<br />' .chr(10);

  
  if (1) {

    //foreach ($arrResult as $arrPeriod) {

      //echo chr(10) .chr(10) .'<br /><br />' .$arrPeriod['Name'] .' | ' .chr(10);
      
      $strSql2 = 'SELECT `SF42_IZSEvent__c`.* FROM `SF42_IZSEvent__c` WHERE `Beitragsmonat__c` = "' .$arrPeriod[0] .'" AND `Beitragsjahr__c` = "' .$arrPeriod[1] .'" ';
      $strSql2.= 'AND `SF42_EventStatus__c` NOT IN ("abgelehnt / Dublette", "zurückgestellt von IZS") ';
      if ($strSchnittstellen != '') { //SF42_informationProvider__c
        $strSql2.= 'AND (`SF42_informationProvider__c` NOT IN (' .$strSchnittstellen .')) ';
      }
      //$strSql2.= 'ORDER BY `SF42_informationProvider__c` ';
      
      $arrResult2 = MySQLStatic::Query($strSql2);
      
echo '<!-- SQL 01: ' .$strSql2 .' -->' .chr(10);
$end = microtime(true);
$durationInMs = ($end-$start) * 1000;
echo '<!-- Abchnitt 03: ' .round($durationInMs) .'ms -->' .chr(10);
$start = microtime(true);
      
      //echo count($arrResult2) .' Events:' .chr(10);
      
      if (count($arrResult2) > 0) {

        $arrIpList = [];
        $strSql = 'SELECT * FROM `Account` WHERE `RecordTypeId` = "01230000001Ao72AAC" AND `SF42_isInformationProvider__c` = "true"';
        $arrSql = MySQLStatic::Query($strSql);

        if (count($arrSql) > 0) {
          foreach ($arrSql as $int => $arrIp) {
            $arrIpList[$arrIp['Id']] = $arrIp;
          }
        }
        
        foreach ($arrResult2 as $arrEvent) {
          //echo chr(10) .'<br />  ' .$arrEvent['Name'] .' -> ' .$arrEvent['SF42_informationProvider__c'] .' | ';
          
          /*
          $strSql3 = 'SELECT * FROM `Account` WHERE `Id` = "' .$arrEvent['SF42_informationProvider__c'] .'"';
          $arrResult3 = MySQLStatic::Query($strSql3);
          */

          $arrResult3[0] = $arrIpList[$arrEvent['SF42_informationProvider__c']] ?? [];
          
          if ((count($arrResult3) > 0) && ($arrResult3[0]['Abweichender_Anfrageprozess__c'] == "")) {
            
            if ($arrIpAmpel[$arrResult3[0]['Id']] != 'gelb') {
              $arrIpAmpel[$arrResult3[0]['Id']] = 'gruen';
            }

            foreach ($arrResult3 as $arrAccount) {

              if (($arrIpAmpel[$arrAccount['Id']] == 'gruen') && (($arrEvent['SF42_EventStatus__c'] == 'enquired') && (($arrEvent['Rueckmeldung_am__c'] == '0000-00-00') || ($arrEvent['Rueckmeldung_am__c'] == '')))) {
                $arrIpAmpel[$arrAccount['Id']] = 'gelb';
              }
              
              if ($arrAngefragtStatus[$arrEvent['SF42_informationProvider__c']] == '') $arrAngefragtStatus[$arrEvent['SF42_informationProvider__c']] = 0; 
              if ($arrKlaerungStatus[$arrEvent['SF42_informationProvider__c']] == '') $arrKlaerungStatus[$arrEvent['SF42_informationProvider__c']] = 0; 

              if ($arrEvent['SF42_EventStatus__c'] == 'enquired') {
                $arrAngefragtStatus[$arrEvent['SF42_informationProvider__c']]++;
                if ($arrEvent['Status_Klaerung__c'] == 'in Klärung') {
                  $arrKlaerungStatus[$arrEvent['SF42_informationProvider__c']]++;
                }
              }

              if ((($arrEvent['SF42_EventStatus__c'] == 'enquired') && (($arrEvent['Rueckmeldung_am__c'] == '0000-00-00') || ($arrEvent['Rueckmeldung_am__c'] == '')))) {
                if (!isset($arrEventStatus[$arrEvent['SF42_informationProvider__c']][0])) {
                  $arrEventStatus[$arrEvent['SF42_informationProvider__c']][0] = 0;
                }
                $arrEventStatus[$arrEvent['SF42_informationProvider__c']][0]++;
              } else {
                if (!isset($arrEventStatus[$arrEvent['SF42_informationProvider__c']][1])) {
                  $arrEventStatus[$arrEvent['SF42_informationProvider__c']][1] = 0;
                }
                //echo 'Name: ' .$arrAccount['Name'] .' | Status: ' .$arrEvent['SF42_EventStatus__c'] .' | Rückmeldung am: ' .$arrEvent['Rueckmeldung_am__c'] .chr(10);
                $arrEventStatus[$arrEvent['SF42_informationProvider__c']][1]++;
              }

              if (($arrEvent['SF42_EventStatus__c'] == 'enquired') || ($arrEvent['SF42_EventStatus__c'] == 'to enquire')) {
                $arrInformationProvider[$arrAccount['Id']] = $arrAccount['Name'];
                $arrRequestWay[$arrAccount['Id']] = $arrAccount['SF42_Anfrageweg__c'];
              }


            }

          }
          
        }
        
      }
      

      
      
    //}
  
  }

$end = microtime(true);
$durationInMs = ($end-$start) * 1000;
echo '<!-- Abchnitt 04: ' .round($durationInMs) .'ms -->' .chr(10);
$start = microtime(true);  

  //print_r($arrEventStatus);  

  //print_r($arrInformationProvider);
  //print_r($arrDecentral);
  
  if (count($arrInformationProvider) > 0) {
    natcasesort($arrInformationProvider);
    
    $strOutputTable = '';
    
    $strOutputTable.= '<table class="" id="5">' .chr(10);
    $strOutputTable.= '  <thead>' .chr(10);
    $strOutputTable.= '    <tr>' .chr(10);
  	$strOutputTable.= '      <th class="">Information Provider</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Rücklauf</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Anschreiben</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Vollmacht</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">erzeugt am</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Orig. nötig</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Anfrageliste</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">erzeugt am</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">xlsx</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Anfrageweg</th>' .chr(10); //
  	$strOutputTable.= '      <th class="">versandt am</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">nur Klärungsfälle</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Anlieferung bis</th>' .chr(10); //class="header"
    $strOutputTable.= '    </tr>' .chr(10);
    $strOutputTable.= '  </thead>' .chr(10);
    $strOutputTable.= '  <tbody>' .chr(10);
    
    if (!isset($_REQUEST['strSelWay'])) {
      $_REQUEST['strSelWay'] = 'all';
    }
    
    $arrPeriod = explode('_', $_REQUEST['strSelDate']);
    
    $arrVollmachtLink = array();
    $arrAnfrageLink   = array();
    $arrVollVersand   = array();
    $arrAnfrVersand   = array();
      
    $intCountEntries = 0;

    $arrBackList = [];
    $strSqlB = 'SELECT `cb_id`, `cb_date`, `cb_account_id` FROM `cms_back` WHERE `cb_month` = "' .$arrPeriod[0] .'" AND `cb_year` = "' .$arrPeriod[1] .'"';
    $arrBack = MySQLStatic::Query($strSqlB);
    if (count($arrBack) > 0) {
      foreach ($arrBack as $intKey => $arrBackRaw) {
        $arrBackList[$arrBackRaw['cb_account_id']] = $arrBackRaw;
      }
    }

    $arrOrgList = [];
    $strSql7 = 'SELECT `Id`, `Nur_neue_Vollmachten__c`, `Originalvollmacht_ben_tigt__c` FROM `Account`'; // WHERE `Id` ="' .$strId .'"
    $arrOrgReq  = MySQLStatic::Query($strSql7);
    if (count($arrOrgReq) > 0) {
      foreach ($arrOrgReq as $intKey => $arrAccountRaw) {
        $strKey = $arrAccountRaw['Id'];
        unset($arrAccountRaw['Id']);
        $arrOrgList[$strKey] = [0 => $arrAccountRaw];
      }
    }

    $arrCertList = [];
    $strSql = 'SELECT `ca_id`, DATE_FORMAT(`ca_created`, "%d.%m.%Y %H:%i:%s") AS `ca_created`, `ca_account_id` FROM `cms_auth` WHERE `ca_month` = "' .$arrPeriod[0] .'" AND `ca_year` = "' .$arrPeriod[1] .'"'; // `ca_account_id` = "' .$strId .'" AND 
    $arrCert = MySQLStatic::Query($strSql);
    if (count($arrCert) > 0) {
      foreach ($arrCert as $intKey => $arrCertRaw) {
        $strKey = $arrCertRaw['ca_account_id'];
        unset($arrCertRaw['ca_account_id']);
        $arrCertList[$strKey] = [0 => $arrCertRaw];
      }
    }

    $arrReqList = [];
    $strSql = 'SELECT `cr_id`, DATE_FORMAT(`cr_created`, "%d.%m.%Y %H:%i:%s") AS `cr_created`, `cr_account_id` FROM `cms_req` WHERE `cr_month` = "' .$arrPeriod[0] .'" AND `cr_year` = "' .$arrPeriod[1] .'"'; // `cr_account_id` = "' .$strId .'" AND 
    $arrReq = MySQLStatic::Query($strSql);
    if (count($arrReq) > 0) {
      foreach ($arrReq as $intKey => $arrReqRaw) {
        $strKey = $arrReqRaw['cr_account_id'];
        unset($arrReqRaw['cr_account_id']);
        $arrReqList[$strKey] = [0 => $arrReqRaw];
      }
    }

    $arrSentList = [];
    $strSql = 'SELECT `cs_id`, DATE_FORMAT(`cs_sent`, "%d.%m.%Y %H:%i:%s") AS `cs_sent`, `cs_account_id` FROM `cms_sent` WHERE `cs_month` = ' .$arrPeriod[0] .' AND `cs_year` = ' .$arrPeriod[1] .''; // `cs_account_id` = "' .$strId .'" AND 
    $arrSent = MySQLStatic::Query($strSql);
    if (count($arrSent) > 0) {
      foreach ($arrSent as $intKey => $arrSentRaw) {
        $strKey = $arrSentRaw['cs_account_id'];
        unset($arrSentRaw['cs_account_id']);
        $arrSentList[$strKey] = [0 => $arrSentRaw];
      }
    }

    foreach ($arrInformationProvider as $strId => $strName) {
      
      //echo '  ' .$strName .'<br />' .chr(10);
      
      if ((isset($_REQUEST['strSelWay']) && ($_REQUEST['strSelWay'] == $arrRequestWay[$strId])) || ($_REQUEST['strSelWay'] == 'all')) {
        
        if ((isset($_REQUEST['strSelRuec']) && ($_REQUEST['strSelRuec'] == $arrIpAmpel[$strId])) || ($_REQUEST['strSelRuec'] == '')) {
          
          //SELECT `cb_date` FROM `cms_back` WHERE `cb_month` = "2017" AND `cb_year` = "2" AND `cb_account_id` ="001300000109k6PAAQ"
          $arrBack['cb_date'] = '';
          //$strSqlB = 'SELECT `cb_id`, `cb_date`, `cb_account_id` FROM `cms_back` WHERE `cb_month` = "' .$arrPeriod[0] .'" AND `cb_year` = "' .$arrPeriod[1] .'" AND `cb_account_id` ="' .$strId .'"';
          //$arrBack = MySQLStatic::Query($strSqlB);

          $arrBack = isset($arrBackList[$strId]) ? [0 => $arrBackList[$strId]] : [];

          if ((count($arrBack) > 0) && (@$arrBack[0]['cb_date'] != '0000-00-00')) {
          
            $arrDate = explode('-', $arrBack[0]['cb_date']);
            $arrBack['cb_date'] = $arrDate[2] .'.' .$arrDate[1] .'.' .$arrDate[0];
            $arrBack['cb_id'] = $arrBack[0]['cb_id'];
            
            $intDiff = ((mktime(0, 0, 0, date('m'), date('d'), date('Y')) - mktime(0, 0, 0, $arrDate[1], $arrDate[2], $arrDate[0])) / 86400);
            
            if ($intDiff >= 0) {
              $strClassBack = ' red';
            } else {
              $strClassBack = '';
            }
          
          } else {
            $arrBack['cb_id'] = $arrPeriod[1] .'-' .$arrPeriod[0] .'-' .$strId .'-';
          }
          

          $strOutputTable.= '    <tr>' .chr(10);
          $strOutputTable.= '      <td><span class="hidden">' .$strName .'</span><a href="index_neu.php?ac=contacts&acid=' .$strId .'" title="Zeige Kontakte" target="_blank">' .$strName .'</a></td>' .chr(10);

          if (!isset($arrEventStatus[$strId][0])) {
            $arrEventStatus[$strId][0] = 0;
          }
          if (!isset($arrEventStatus[$strId][1])) {
            $arrEventStatus[$strId][1] = 0;
          }

          $strTitle = 'gesamt: ' .($arrEventStatus[$strId][1] + $arrEventStatus[$strId][0]);
          $strTitle.= ' | gruen: ' .$arrEventStatus[$strId][1];
          $strTitle.= ' | rot: ' .$arrEventStatus[$strId][0];
          
          if (($arrEventStatus[$strId][1] > 0) && ($arrEventStatus[$strId][0] == 0)) {
            $strAmpelStatus = 'gruen';
          } else if (($arrEventStatus[$strId][1] == 0) && ($arrEventStatus[$strId][0] > 0)) {
            $strAmpelStatus = 'rot';
          } else {
            $strAmpelStatus = 'gelb';
          }
          
          $strOutputTable.= '      <td><span class="hidden">' .$strAmpelStatus .'</span><img src="/assets/images/sys/ampel_' .$strAmpelStatus .'.png" title="' .$strTitle .'"></td>' .chr(10);
          //$strOutputTarrAngefragtStatusable.= '      <td><span class="hidden">' .$arrIpAmpel[$strId] .'</span><img src="/assets/images/sys/ampel_' .$arrIpAmpel[$strId] .'.png"></td>' .chr(10);
          
          if ($arrRequestWay[$strId] == 'E-Mail') {
            $strOutputTable.= '      <td style="text-align: center;"><img src="img/icon_email.png" class="ic_preview_m" id="z_' .$strId .'" rel="' .$intEvId .'" /></td>' .chr(10);
          } else {
            $strOutputTable.= '      <td style="text-align: center;"><img src="img/icon_pdf.png" class="ic_preview" id="p_' .$strId .'" /></td>' .chr(10);
          }
          
          //$strSql7 = 'SELECT `Nur_neue_Vollmachten__c`, `Originalvollmacht_ben_tigt__c` FROM `Account` WHERE `Id` ="' .$strId .'"';
          //$arrOrg  = MySQLStatic::Query($strSql7);

          $arrOrg = $arrOrgList[$strId] ?? [];

          $strVollmachtLink = 'createVollmacht.neu.php?AccountId=' .$strId .'&Name=' .rawurlencode($strName) .'&strSelDate=' .$_REQUEST['strSelDate'] .'&strSelText=' .$_REQUEST['strSelText'] .'&show=C&strSelWay=' .$_REQUEST['strSelWay'] .'&strSelRuec=' .$_REQUEST['strSelRuec'] .'';
          $strVollmachtLinkNeu = 'createVollmacht.neu.php?AccountId=' .$strId .'&Name=' .rawurlencode($strName) .'&strSelDate=' .$_REQUEST['strSelDate'] .'&strSelText=' .$_REQUEST['strSelText'] .'&show=C&strSelWay=' .$_REQUEST['strSelWay'] .'&strSelRuec=' .$_REQUEST['strSelRuec'] .'&new=1';

          $arrVollmachtLink[] = $strVollmachtLink;
          
          $arrDiff = null;
          
          if ($arrOrg[0]['Nur_neue_Vollmachten__c'] == 'true') {
          	$arrDiff = $objIP->arrGetNewVollmachtList($strId, $arrPeriod[0], $arrPeriod[1]);
            
            $strOutputTable.= '      <td><a href="' .$strVollmachtLink .'">alle</a> / ';
            if (count($arrDiff) > 0) { 
              $strOutputTable.= '<a href="' .$strVollmachtLinkNeu .'">neue (' .count($arrDiff) .')</a>';
              $arrVollmachtLink[] = $strVollmachtLinkNeu;
            } else {
              $strOutputTable.= 'neue (0)';
            }
            $strOutputTable.= '</td>' .chr(10);
            
          } else {
            $strOutputTable.= '      <td><a href="' .$strVollmachtLink .'">erzeugen</a></td>' .chr(10);
          }
  
          //$strSql = 'SELECT `ca_id`, DATE_FORMAT(`ca_created`, "%d.%m.%Y %H:%i:%s") AS `ca_created` FROM `cms_auth` WHERE `ca_account_id` = "' .$strId .'" AND `ca_month` = "' .$arrPeriod[0] .'" AND `ca_year` = "' .$arrPeriod[1] .'"';
          //$arrCert = MySQLStatic::Query($strSql);

          $arrCert = $arrCertList[$strId] ?? [];
          
          if (count($arrCert) > 0) {
            $arrDate = explode(' ', $arrCert[0]['ca_created']);
            $strFileName = 'pdf/vollmacht/' .$strId .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_vollmacht_sv.pdf?' .substr(md5(uniqid(mt_rand(), true)), 0, 4);
            $strOutputTable.= '      <td><a href="' .$strFileName .'" target="_blank" alt="' .$arrCert[0]['ca_created'] .'" title="' .$arrCert[0]['ca_created'] .'" id="v_' .$strId .'">' .$arrDate[0] .'</a></td>' .chr(10);
          } else {
            $strOutputTable.= '      <td></td>' .chr(10);
          }
          
          if ($arrOrg[0]['Originalvollmacht_ben_tigt__c'] == 'true') {
            
          	if (is_null($arrDiff)) {
          	  $arrDiff = $objIP->arrGetNewVollmachtList($strId, $arrPeriod[0], $arrPeriod[1]);
            }

            if (count($arrDiff) > 0) {
              $strOutputTable.= '      <td><a href="index_neu.php?ac=vollmacht&acid=' .$strId .'&m=' .$arrPeriod[0] .'&y=' .$arrPeriod[1] .'" title="Zeige Original-Vollmachten" target="_blank">' .$arrOrgName[$arrOrg[0]['Originalvollmacht_ben_tigt__c']] .' (' .count($arrDiff) .')</a></td>' .chr(10);
            } else {
              $strOutputTable.= '      <td>' .$arrOrgName[$arrOrg[0]['Originalvollmacht_ben_tigt__c']] .' (0)</td>' .chr(10);
            }
            
          } else {
            $strOutputTable.= '      <td>' .$arrOrgName[$arrOrg[0]['Originalvollmacht_ben_tigt__c']] .'</td>' .chr(10);
          }
          
          
          $strAnfrageLink = 'createAnfrageliste.php?AccountId=' .$strId .'&Name=' .rawurlencode($strName) .'&strSelDate=' .$_REQUEST['strSelDate'] .'&strSelText=' .$_REQUEST['strSelText'] .'&strSelWay=' .$_REQUEST['strSelWay'] .'&strSelRuec=' .$_REQUEST['strSelRuec'] .'&show=D&clid=' .$_SESSION['id'];
          $arrAnfrageLink[] = $strAnfrageLink;
  
          $strOutputTable.= '      <td><a href="' .$strAnfrageLink .'">erzeugen</a></td>' .chr(10);
  
          //$strSql = 'SELECT `cr_id`, DATE_FORMAT(`cr_created`, "%d.%m.%Y %H:%i:%s") AS `cr_created` FROM `cms_req` WHERE `cr_account_id` = "' .$strId .'" AND `cr_month` = "' .$arrPeriod[0] .'" AND `cr_year` = "' .$arrPeriod[1] .'"';
          //$arrReq = MySQLStatic::Query($strSql);

          $arrReq = $arrReqList[$strId] ?? [];
          
          if (count($arrReq) > 0) {
            $arrDate = explode(' ', $arrReq[0]['cr_created']);
            $strFileNameBlank = 'pdf/anfrage/' .$strId .'_' .$arrPeriod[1] .'_' .$arrPeriod[0]  .'_anfrage_sv.pdf';
            $strFileName = $strFileNameBlank .'?' .substr(md5(uniqid(mt_rand(), true)), 0, 4);
            $strOutputTable.= '      <td><a href="' .$strFileName .'" target="_blank" alt="' .$arrReq[0]['cr_created'] .'" title="' .$arrReq[0]['cr_created'] .'" id="a_' .$strId .'">' .$arrDate[0] .'</a></td>' .chr(10);
            
            $strFileNameXlsx = str_replace('.pdf', '.xlsx', $strFileNameBlank);
            if (file_exists($strFileNameXlsx)) {
              $strOutputTable.= '      <td><a href="' .$strFileNameXlsx .'" target="_blank" alt="' .$arrReq[0]['cr_created'] .'" title="' .$arrReq[0]['cr_created'] .'" id="x_' .$strId .'"><img src="img/icon_xls.png" id="x_' .$strId .'" /></a></td>' .chr(10);
            } else {
              $strOutputTable.= '      <td></td>' .chr(10);
            }

          } else {
            $strOutputTable.= '      <td></td>' .chr(10);
            $strOutputTable.= '      <td></td>' .chr(10);
          }
  
          if ($arrRequestWay[$strId] == 'E-Mail') {
            $strOutputTable.= '      <td><span id="e_' .$strId .'" class="send">' .$arrRequestWay[$strId] .'</span></td>' .chr(10);
            
            //$strSql = 'SELECT `cs_id`, DATE_FORMAT(`cs_sent`, "%d.%m.%Y %H:%i:%s") AS `cs_sent` FROM `cms_sent` WHERE `cs_account_id` = "' .$strId .'" AND `cs_month` = ' .$arrPeriod[0] .' AND `cs_year` = ' .$arrPeriod[1] .'';
            //$arrSent = MySQLStatic::Query($strSql);

            $arrSent = $arrSentList[$strId] ?? [];
            
            if (count($arrSent) > 0) {
              $arrDate = explode(' ', $arrSent[0]['cs_sent']);
              $strOutputTable.= '      <td><span class="hidden">' .$arrDate[0] .'</span><a id="sd_' .$strId .'" alt="' .$arrSent[0]['cs_sent'] .'" title="' .$arrSent[0]['cs_sent'] .'">' .$arrDate[0] .'</a></td>' .chr(10);
            } else {
              $strOutputTable.= '      <td></td>' .chr(10);
            }
            
            $arrVollVersand[2][] = $strVollmachtLink;
            $arrAnfrVersand[2][] = $strAnfrageLink;
  
          } elseif ($arrRequestWay[$strId] == 'Fax') {
            $strOutputTable.= '      <td><span id="f_' .$strId .'" class="send">' .$arrRequestWay[$strId] .'</span></td>' .chr(10);
            
            //$strSql = 'SELECT `cs_id`, DATE_FORMAT(`cs_sent`, "%d.%m.%Y %H:%i:%s") AS `cs_sent` FROM `cms_sent` WHERE `cs_account_id` = "' .$strId .'" AND `cs_month` = ' .$arrPeriod[0] .' AND `cs_year` = ' .$arrPeriod[1] .'';
            //$arrSent = MySQLStatic::Query($strSql);

            $arrSent = $arrSentList[$strId] ?? [];
            
            if (count($arrSent) > 0) {
              $arrDate = explode(' ', $arrSent[0]['cs_sent']);
              $strOutputTable.= '      <td><span class="hidden">' .$arrDate[0] .'</span><a id="sd_' .$strId .'" alt="' .$arrSent[0]['cs_sent'] .'" title="' .$arrSent[0]['cs_sent'] .'">' .$arrDate[0] .'</a></td>' .chr(10);
            } else {
              $strOutputTable.= '      <td></td>' .chr(10);
            }
            
            $arrVollVersand[1][] = $strVollmachtLink;
            $arrAnfrVersand[1][] = $strAnfrageLink;
  
          } elseif ($arrRequestWay[$strId] == 'Post') {
            $strOutputTable.= '      <td><span id="b_' .$strId .'" class="send">' .$arrRequestWay[$strId] .'</span></td>' .chr(10);
            
            //$strSql = 'SELECT `cs_id`, DATE_FORMAT(`cs_sent`, "%d.%m.%Y %H:%i:%s") AS `cs_sent` FROM `cms_sent` WHERE `cs_account_id` = "' .$strId .'" AND `cs_month` = ' .$arrPeriod[0] .' AND `cs_year` = ' .$arrPeriod[1] .'';
            //$arrSent = MySQLStatic::Query($strSql);

            $arrSent = $arrSentList[$strId] ?? [];
            
            if (count($arrSent) > 0) {
              $arrDate = explode(' ', $arrSent[0]['cs_sent']);
              $strOutputTable.= '      <td><span class="hidden">' .$arrDate[0] .'</span><a id="sd_' .$strId .'" alt="' .$arrSent[0]['cs_sent'] .'" title="' .$arrSent[0]['cs_sent'] .'">' .$arrDate[0] .'</a></td>' .chr(10);
            } else {
              $strOutputTable.= '      <td></td>' .chr(10);
            }
            
            $arrVollVersand[0][] = $strVollmachtLink;
            $arrAnfrVersand[0][] = $strAnfrageLink;
  
          } else {
            $strOutputTable.= '      <td><span id="b_' .$strId .'" class="send">' .$arrRequestWay[$strId] .'</span></td>' .chr(10);
            $strOutputTable.= '      <td></td>' .chr(10);
          }
          
          $strNurKlaerung = 'nein';
          if (($arrAngefragtStatus[$strId] == $arrKlaerungStatus[$strId]) && ($arrAngefragtStatus[$strId] != 0)) {
            $strNurKlaerung = 'ja (' .$arrKlaerungStatus[$strId] .')';
          }

          $strOutputTable.= '      <td>' .$strNurKlaerung .' <!-- ' .$arrAngefragtStatus[$strId] .'/' .$arrKlaerungStatus[$strId] .' --></td>' .chr(10);
          
          $strOutputTable.= '      <td><span id="h_date_' .$arrBack['cb_id']  .'" class="hidden">' .$arrBack['cb_date'] .'</span><input type="text" id="date_' .$arrBack['cb_id']  .'" class="date update_field' .$strClassBack .' selD" name="cb_date" value="' .$arrBack['cb_date'] .'" autocomplete="off"></td>' .chr(10);
          
          $strOutputTable.= '    </tr>' .chr(10);
          
          $intCountEntries++;
        
        }
        
      }
      
    }
    
    
    $end = microtime(true);
    $durationInMs = ($end-$start) * 1000;
    echo '<!-- Abchnitt 05: ' .round($durationInMs) .'ms -->' .chr(10);
    $start = microtime(true);  

    
    $strOutputTable.= '  </tbody>' .chr(10);
    $strOutputTable.= '</table>' .chr(10);   
    
    $strOutput.= '<div class="clearfix" style="height: 48px; width: 1574px;">' .chr(10);
    $strOutput.= '  <div class="form-left">' .chr(10);
    $strOutput.= '    <h3>' .$intCountEntries .' Ergebnisse gefunden:</h3>' .chr(10);
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' .chr(10);

    if ($_REQUEST['startType'] == 'auth') {
      $strOutput.= '<span class="buttons">Dokumente werden erzeugt...</span>' .chr(10);
    } else {
      $strOutput.= '<form action="" method="post" class="buttons" style="display: inline;">' .chr(10);
      $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);
      $strOutput.= '<input type="hidden" name="startType" value="auth">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelDate" value="' .$_REQUEST['strSelDate'] .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelWay" value="' .$_REQUEST['strSelWay'] .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelText" value="' .$_REQUEST['strSelText'] .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="startCron" value="' .$strHashVollmacht .'">' .chr(10);
      $strOutput.= '<button id="doc-create" class="ui-button ui-state-default ui-corner-all">Dokumente erzeugen</button>' .chr(10);
      $strOutput.= '</form> ' .chr(10);
    }

    $strOutput.= '    <form method="post" action="_get_csv.php" style="display: inline; float: right; padding-left: 10px;">';
    $strOutput.= '    <input type="hidden" name="strTable" value="' .str_replace('"', "'", $strOutputTable) .'">' .chr(10);
    $strOutput.= '    <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' .chr(10);
    $strOutput.= '    </form>';
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '</div>' .chr(10);


    $strOutput.= $strOutputTable;

    $strOutput.= "
<script>
$('#5').fixheadertable({ 
   colratio    : [270, 80, 100, 110, 110, 110, 110, 110, 50, 110, 110, 120, 110], 
   height      : 500, 
   width       : 1530, 
   zebra       : true, 
   sortable    : true,
   sortType    : ['string', 'string', 'string', 'string', 'date', 'string', 'date', 'string', 'string', 'date', 'string', 'string', 'date'],
   sortedColId : 0, 
   dateFormat  : 'd.m.Y'
});
</script>
";

          
  } else {
    
    $strOutput.= '<p>Keine Events im gewählten Zeitraum vorhanden.</p>' .chr(10);
    
  }

}

$strOutput.= '

<style>
td {
  padding-bottom: 5px;
}

#dialog_prev textarea {
  margin-bottom: 0px;
}
</style>

<div id="dialog_prev" title="Vorschau">
</div>

<script type="text/javascript">
	
$(document).ready(function() {

	$("#dialog_prev").dialog({
		bgiframe: true,
		autoOpen: false,
		resizable: false,
		height: 500,
		width: 590,
		modal: true,
		buttons: {
			\'OK\': function() {

		    $(this).dialog(\'close\');

			}
		},
		close: function() {

		}
	});
	
});	

</script>

  ';     

  $end = microtime(true);
  $durationInMs = ($end-$start) * 1000;
  echo '<!-- Abchnitt 06: ' .round($durationInMs) .'ms -->' .chr(10);
  $start = microtime(true);  


  //date<curdate() and date>DATE_ADD(CURDATE(), INTERVAL -1 DAY)
$strSql = 'DELETE FROM `_cron_url` WHERE `cu_status` = 0 AND `cu_time` < DATE_ADD(CURDATE(), INTERVAL -1 DAY)';
$arrRow = MySQLStatic::Query($strSql);

/*

if (is_array($arrVollmachtLink) && (count($arrVollmachtLink) > 0)) {
  
  foreach ($arrVollmachtLink as $intId => $strLink) {
    
    $strSql = 'INSERT INTO `_cron_url` (`cu_id`, `cu_hash`, `cu_url`, `cu_time`, `cu_status`) VALUES (NULL, "' .$strHashVollmacht .'", "' .$strLink .'", NOW(), 0)';
    $intCt_id = MySQLStatic::Insert($strSql);
    
    $strSql = 'INSERT INTO `_cron_url` (`cu_id`, `cu_hash`, `cu_url`, `cu_time`, `cu_status`) VALUES (NULL, "' .$strHashVollmacht .'", "' .$arrAnfrageLink[$intId] .'", NOW(), 0)';
    $intCt_id = MySQLStatic::Insert($strSql);

  }
  
}

*/

for ($intCount = 0; $intCount <= 2; $intCount++) {

  if (is_array($arrVollVersand[$intCount]) && (count($arrVollVersand[$intCount]) > 0)) {
    
    foreach ($arrVollVersand[$intCount] as $intId => $strLink) {
      
      $strSql = 'INSERT INTO `_cron_url` (`cu_id`, `cu_hash`, `cu_url`, `cu_time`, `cu_status`) VALUES (NULL, "' .$strHashVollmacht .'", "' .$strLink .'", NOW(), 0)';
      $intCt_id = MySQLStatic::Insert($strSql);
      
      $strSql = 'INSERT INTO `_cron_url` (`cu_id`, `cu_hash`, `cu_url`, `cu_time`, `cu_status`) VALUES (NULL, "' .$strHashVollmacht .'", "' .$arrAnfrVersand[$intCount][$intId] .'", NOW(), 0)';
      $intCt_id = MySQLStatic::Insert($strSql);

    }
    
  }

}


$end = microtime(true);
$durationInMs = ($end-$start) * 1000;
echo '<!-- Abchnitt 07: ' .round($durationInMs) .'ms -->' .chr(10);
$start = microtime(true);  

/*

print_r($arrVollmachtLink);
print_r($arrAnfrageLink);

*/


?>