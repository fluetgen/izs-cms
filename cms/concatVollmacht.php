<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

ini_set('memory_limit', '2048M');

session_start();

define('FPDF_FONTPATH','fpdf17/font/');
define('TEMP_PATH', 'temp/');

require_once('../assets/classes/class.mysql.php');
require('fpdf17/fpdf.php');
require('fpdf17/fpdi.php');


class concat_pdf extends fpdi {
  var $files = array();
  function concat_pdf($orientation='P',$unit='mm',$format='A4') {
      parent::__construct($orientation,$unit,$format);
  }
  function setFiles($files) {
      $this->files = $files;
  }
  function concat() {
      foreach($this->files AS $file) {
          $pagecount = $this->setSourceFile($file);
          for ($i = 1;  $i <= $pagecount;  $i++) {
               $tplidx = $this->ImportPage($i);
               $this->AddPage();
               $this->useTemplate($tplidx);
          }
      }
  }
}


if (isset($_REQUEST['id'])) {

  if (is_array($_REQUEST['id'])) {
    $strId = '"' .implode('", "', $_REQUEST['id']) .'"';
  } else {
    $strId = '"' .$_REQUEST['id'] .'"';
  }

  $strSql = 'SELECT `izs_file`.* FROM `izs_file` INNER JOIN `Group__c` ON `if_object` = `Group__c`.`Id` ';
  $strSql.= 'WHERE `if_object` IN (' .$strId .') AND `if_type` = "Vollmacht" AND `if_show` = "true" ORDER BY `Group__c`.`Name`, `if_name`';
  $arrResult = MySQLStatic::Query($strSql);

  if (count($arrResult) > 0) {

    $arrJpgList = array();

    foreach ($arrResult as $intKey => $arrVollmacht) {

      $strRelativePath = substr($arrVollmacht['if_url'], 23); // https://www.izs.de/cms/
      if (file_exists($strRelativePath)) {
        $arrJpgList[] = $strRelativePath;
      }

    }

    if (count($arrJpgList) > 0) {

      $pdf = new concat_pdf();
      $pdf->AliasNbPages();

      foreach ($arrJpgList as $intKey => $strFile) {

        $pdf->AddPage();
        $pdf->SetAutoPageBreak(false);
        $pdf->Image($strFile, 0, 0, 210, 297, 'JPG'); 

      }

      $pdf->Output(date('Y.m.d_His - ') .'Neue_Vollmachten.pdf', 'D'); //-' .$arrPeriod[1] .'-' .$arrPeriod[0] .'

    }

  }

}

?> 