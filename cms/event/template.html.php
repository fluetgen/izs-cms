<?php

$strSql1 = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$arrSql[0]['Betriebsnummer_ZA__c'] .'"'; //
$arrSql1 = MySQLStatic::Query($strSql1);

$strSql2 = 'SELECT * FROM `Account` WHERE `Id` = "' .$arrSql[0]['SF42_informationProvider__c'] .'"';
$arrSql2 = MySQLStatic::Query($strSql2);

$strSql4 = 'SELECT * FROM `Account` WHERE `Id` = "' .$arrSql1[0]['Id'] .'"';
$arrSql4 = MySQLStatic::Query($strSql4);

// NOT USED
$strSql3 = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$arrSql2[0]['SF42_Company_Group__c'] .'"';
$arrSql3 = MySQLStatic::Query($strSql3);

$strRegistriert = '';
if ($arrSql4[0]['Registriert_seit__c'] == '0000-00-00') {
    $strRegistriert = '12/2011';
} else {
    $arrPart = explode('-', $arrSql4[0]['Registriert_seit__c']);
    $strRegistriert = $arrPart[1] .'/' .$arrPart[0];
}

/*
$strSql3 = 'SELECT DATE_FORMAT(`cs_sent`, "%d.%m.%Y") AS `cs_sent` FROM `cms_sent` WHERE `cs_account_id` = "' .$arrSql[0]['SF42_informationProvider__c'] .'" AND `cs_month` = ' .$arrSql[0]['SF42_Month__c'] .' AND `cs_year` = ' .$arrSql[0]['SF42_Year__c'] .'';
$arrSql3 = MySQLStatic::Query($strSql3);

$strRequestDate = '';
if (count($arrSql3) > 0) {
    $strRequestDate = $arrSql3[0]['cs_sent'];
}
*/

$intTime = strtotime($arrSqlProof[0]['ed_download_time']);

$strOutput = '<!DOCTYPE html>
<html lang="de">

<head>
    <link rel="stylesheet" type="text/css" href="style_mobile.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
</head>

<body>
    <div class="section_1">
        <div class="section_1_1">
            <img src="img/calendar.svg">
            <span>' .date('d.m.Y', $intTime) .'</span>
        </div>
        <div class="section_1_1">
            <img src="img/wall-clock.svg">
            <span>' .date('H:i:s', $intTime) .' CET' .'</span>
        </div>
        <div class="section_1_1">
            <img src="img/documents.svg">
            <span>' .$arrSql[0]['Name'] .'</span>
        </div>
    </div>
    <div class="section_2">
        <h2>
            DIGITALE SV-KONTOAUSKUNFT
        </h2>
        <div>
            mit integriertem Subsidiärhaftungsschutz*
        </div>
    </div>
    <div class="section_3">
';

if ($arrSql[0]['SF42_EventStatus__c'] == 'OK') {

    if ($arrSql[0]['Stundung__c'] == '') {

        $strOutput.= '        <div class="section_3_top">Das Beitragskonto ist ausgeglichen.</div>' .chr(10);
        $strOutput.= '        <img src="img/checked.svg">' .chr(10);

    } elseif ($arrSql[0]['Stundung__c'] == 'Ratenzahlung') {

        $strOutput.= '        <div class="section_3_top">Das Beitragskonto ist ausgeglichen.</div>' .chr(10);
        $strOutput.= '        <img src="img/Group161.svg">' .chr(10);
        $strOutput.= '        <div class="section_3_red">Es besteht eine Ratenzahlungsvereinbarung.</div>' .chr(10);

    } elseif ($arrSql[0]['Stundung__c'] == 'Stundung') {

        $strOutput.= '        <div class="section_3_top">Das Beitragskonto ist ausgeglichen.</div>' .chr(10);
        $strOutput.= '        <img src="img/Group161.svg">' .chr(10);
        $strOutput.= '        <div class="section_3_red">Es besteht eine Stundungsvereinbarung.</div>' .chr(10);

    } elseif ($arrSql[0]['Stundung__c'] == 'Stundung oder Ratenzahlung') {

        $strOutput.= '        <div class="section_3_top">Das Beitragskonto ist ausgeglichen.</div>' .chr(10);
        $strOutput.= '        <img src="img/Group161.svg">' .chr(10);
        $strOutput.= '        <div class="section_3_red">Es besteht eine Stundungs- oder Ratenzahlungsvereinbarung.</div>' .chr(10);

    }


} else {

    $strOutput.= '        <div class="section_3_top">Das Beitragskonto ist nicht ausgeglichen.</div>' .chr(10);
    $strOutput.= '        <img src="img/cancel.svg">' .chr(10);

}


$strOutput.= '
    </div>
';

if (($arrSql4[0]['UbbFormulierung__c'] != '') && ($arrSql4[0]['UbbSeitMonat__c'] != '') && ($arrSql4[0]['UbbSeitJahr__c'] != '')) {

    $strUbb = 'Nach den uns vorliegenden Informationen wurden seit ' .$arrSql4[0]['UbbSeitMonat__c'] .'/' .$arrSql4[0]['UbbSeitJahr__c'] .' alle SV-Beiträge ' .$arrSql4[0]['UbbFormulierung__c'] .' bezahlt.';
    /*
    Nach den uns vorliegenden Informationen wurden seit 10/2016 alle SV-Beiträge stets fristgerecht bezahlt.
    Nach den uns vorliegenden Informationen wurden seit 12/2021 alle SV-Beiträge überwiegend fristgerecht bezahlt.
    */

    $strOutput.= '
    <div class="section_2" style="padding: 0 20px 0 20px">
        <div>
        ' .$strUbb .'
        </div>
    </div>
    ';

}

/*
$strOutput.= '
    <div class="section_4">
        <div class="section_4_1">
            PERSONALDIENSTLEISTER
        </div>
        <div class="section_4_2">
            ' .$arrSql1[0]['Name'] .'
        </div>
        <span class="section_4_3">
            ' .$arrSql1[0]['BillingStreet'] .' | ' .$arrSql1[0]['BillingPostalCode'] .' ' .$arrSql1[0]['BillingCity'] .'
        </span>
        <div class="section_4_4">
            Betriebsnummer: ' .$arrSql1[0]['Btnr'] .'
        </div>


        <div class="section_4_1">
            KRANKENKASSE
        </div>
        <div class="section_4_2">
            ' .$arrSql2[0]['Name'] .'
        </div>
        <span class="section_4_3">
            ' .$arrSql2[0]['BillingStreet'] .' | ' .$arrSql2[0]['BillingPostalCode'] .' ' .$arrSql2[0]['BillingCity'] .'
        </span>
        <div class="section_4_4">
            Betriebsnummer: ' .$arrSql2[0]['SF42_Comany_ID__c'] .'
        </div>


        <div class="section_4_1">
            REGISTRIERT BEI IZS SEIT
        </div>
        <div class="section_4_4">
            ' .$strRegistriert .'
        </div>


        <div class="section_4_1">
            BEITRAGSMONAT
        </div>
        <div class="section_4_4">
            ' .str_pad($arrSql[0]['SF42_Month__c'], 2, '0', STR_PAD_LEFT) .'-' .$arrSql[0]['SF42_Year__c'] .'
        </div>


        <div class="section_4_1">
            ART DER MITTEILUNG
        </div>
        <div class="section_4_4">
            Direkte Auskunft der Krankenkasse an IZS
        </div>
    </div>
</body>

</html>';
*/

$strOutput = '<!DOCTYPE html>
<html lang="de">

<head>
    <link rel="stylesheet" type="text/css" href="style_mobile.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
</head>

<body>
    <div class="header">
        <div class="logo">
            <a href="https://www.izs-institut.de/"><img src="img/mobile_logo.svg"></a>
        </div>
        <div class="menu_button">
            <img src="img/menu_button.svg">
        </div>
    </div>
    <div class="section_1">
        <div class="section_1_1">
            <img src="img/calendar.svg">
            <span>' .date('d.m.Y', $intTime) .'</span>
        </div>
        <div class="section_1_1">
            <img src="img/wall-clock.svg">
            <span>' .date('H:i:s', $intTime) .' CET' .'</span>
        </div>
        <div class="section_1_1">
            <img src="img/documents.svg">
            <span>' .$arrSql[0]['Name'] .'</span>
        </div>
    </div>
    <div class="section_2">
        <h2>
            DIGITALES SV-ZERTIFIKAT
        </h2>
        <div>
            mit Haftungsschutz für berechtigte Entleiher*
        </div>
    </div>
    <div class="section_3">
    ';

    if ($arrSql[0]['SF42_EventStatus__c'] == 'OK') {
    
        $strOutput.= '        <div class="section_3_top">Das Beitragskonto ist ausgeglichen</div>' .chr(10);
        
        if ($arrSql[0]['Stundung__c'] == '') {
    
            $strOutput.= '        <div class="section_3_red"><img src="img/checked.svg"></div>' .chr(10);
    
        } else {

            $strOutput.= '        <div class="section_3_red"><img src="img/Group161.svg"></div>' .chr(10);

        }
    
        if (($arrSql4[0]['UbbFormulierung__c'] != '') && ($arrSql4[0]['UbbSeitMonat__c'] != '') && ($arrSql4[0]['UbbSeitJahr__c'] != '')) {

            $strUbb = '<div class="section_3_red long">Nach den uns vorliegenden Informationen wurden seit ' .$arrSql4[0]['UbbSeitMonat__c'] .'/' .$arrSql4[0]['UbbSeitJahr__c'] .' alle SV-Beiträge ' .$arrSql4[0]['UbbFormulierung__c'] .' bezahlt.</div>';
        
            $strOutput.= '
                ' .$strUbb .'
            ';
        
        }
    
    } else {
    
        $strOutput.= '        <div class="section_3_top">Das Beitragskonto ist nicht ausgeglichen</div>' .chr(10);
        $strOutput.= '        <div class="section_3_red"><img src="img/cancel.svg"></div>' .chr(10);
    
    }

    if ($arrSql[0]['Stundung__c'] == 'Ratenzahlung') {
            
        $strOutput.= '        <div class="section_3_red">Es besteht eine Ratenzahlungsvereinbarung.</div>' .chr(10);

    } elseif ($arrSql[0]['Stundung__c'] == 'Stundung') {

        $strOutput.= '        <div class="section_3_red">Es besteht eine Stundungsvereinbarung.</div>' .chr(10);

    } elseif ($arrSql[0]['Stundung__c'] == 'Stundung oder Ratenzahlung') {

        $strOutput.= '        <div class="section_3_red">Es besteht eine Stundungs- oder Ratenzahlungsvereinbarung.</div>' .chr(10);

    }

    
    $strOutput.= '
        </div>
        <div class="section_4">
        <div class="section_4_1">
            PERSONALDIENSTLEISTER
        </div>
        <div class="section_4_2">
            ' .$arrSql1[0]['Name'] .'
        </div>
        <span class="section_4_3">
            ' .$arrSql1[0]['BillingStreet'] .' | ' .$arrSql1[0]['BillingPostalCode'] .' ' .$arrSql1[0]['BillingCity'] .'
        </span>
        <div class="section_4_3">
            Betriebsnummer: ' .$arrSql1[0]['SF42_Comany_ID__c'] .'
        </div>
        <div class="section_4_4">
            Registriert seit: ' .$strRegistriert .'
        </div>


        <div class="section_4_1">
            KRANKENKASSE
        </div>
        <div class="section_4_2">
            ' .$arrSql2[0]['Name'] .'
        </div>
        <span class="section_4_3">
            ' .$arrSql2[0]['BillingStreet'] .' | ' .$arrSql2[0]['BillingPostalCode'] .' ' .$arrSql2[0]['BillingCity'] .'
        </span>
        <div class="section_4_4">
            Betriebsnummer: ' .$arrSql2[0]['SF42_Comany_ID__c'] .'
        </div>


        <div class="section_4_1">
            ART DER MITTEILUNG
        </div>
        <div class="section_4_2">
            Direkte Auskunft an IZS
        </div>' .chr(10);

    if ($arrSql[0]['Rueckmeldung_am__c'] != '0000-00-00') {
    $strOutput.= '
        <span class="section_4_3">
            Auskunft erteilt am: ' .date('d.m.Y', strtotime($arrSql[0]['Rueckmeldung_am__c'])) .'
        </span>' .chr(10);
    }

    $strOutput.= '
        <div class="section_4_4">
            Beitragsmonat: ' .str_pad($arrSql[0]['SF42_Month__c'], 2, '0', STR_PAD_LEFT) .'/' .$arrSql[0]['SF42_Year__c'] .'
        </div>

        
    </div>
    <div class="section_5">
        <div class="section_5_1">Integrierter Haftungsschutz*</div>
        <div class="section_5_2">
            <div class="section_5_2_text">bis zu</div>
            <div class="section_5_2_price">5.000 €</div>
        </div>
        <a href="https://antrag.izs.de/haftungsschutz" class="section_5_3">Kostenlos beantragen</a>
    </div>
    <div class="right_section_6">
        <div class="right_section_6_1">
            <b>Bagatellfall-Regelung:</b> Rückstände auf dem Beitragskonto von bis zu 500 € werden nicht als
            Rückstand gewertet.
        </div>
        <div class="right_section_6_1">
            <b>* Integrierter Haftungsschutz</b></br>IZS steht gegenüber berechtigten Entleihunternehmen für die inhaltlich
            und technisch korrekte Verarbeitung von Auskünften von Krankenkassen über den Stand von
            Beitragskonten <b>bis zu einem Betrag in Höhe von EUR 5.000,00 je Zertifikat und Subsidiär-Schadensfall</b>
            ein. Einzelheiten zur Anspruchsberechtigung entnehmen Sie bitte unseren <a href="https://www.izs-institut.de/agb/">AGB</a>.
            Voraussetzung für die Nutzung des Haftungsschutzes ist eine Online-Registrierung, die Sie <a href="https://antrag.izs.de/haftungsschutz">hier</a> bequem in nur einer Minute
            durchführen können, sowie die Annahmeerklärung von IZS.
        </div>
    </div>
</body>

</html>';

?>