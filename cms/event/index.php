<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

require_once('../const.inc.php');
require_once('../../assets/classes/class.mysql.php');
require_once('phpqrcode/classes/phpqrcode.class.php');

//https://www.izs.de/cms/event/Orizon%20GmbH/TUI%20BKK/2022/7/1042492.pdf

//print_r($_REQUEST);

$strRedirectUrl = @$_REQUEST['r'];

if (isset($strRedirectUrl) && ($strRedirectUrl != '')) {

    $arrPart = explode('/', $strRedirectUrl);

    //print_r($arrPart); die();

    if (count($arrPart) == 5) { // CREATE https://www.izs.de/cms/event/Orizon%20GmbH/TUI%20BKK/2022/7/1042492.pdf

        $arrRequest = pathinfo($strRedirectUrl);

        if (isset($arrRequest['filename']) && ($arrRequest['filename'] != '')) {

            $intBeginTime = microtime(true); 

            $intEventId = $arrRequest['filename'];

            $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Id` = "' .$intEventId .'"';
            $arrSql = MySQLStatic::Query($strSql);
            
            if (($arrSql[0]['SF42_OnlineStatus__c'] == 'false') || ($arrSql[0]['RecordTypeId'] != '01230000001Ao75AAC') || ($arrSql[0]['Sperrvermerk__c'] == 'true') || ($arrSql[0]['SF42_PublishingStatus__c'] != 'online')) {
                http_response_code(404);
                //include('my_404.php'); // provide your own HTML for the error page
                die();
            }

            $strReturnType  = 'string';
            $boolStandAlone = true;
            $arrPdfLinkListEvent = array(); //NOT USED
            if (isset($_COOKIE['id']) && ($_COOKIE['id'] == 3)) {
                include ('create.publication.mod.sv.inc.php');
            } else {
                include ('create.publication.mod.sv.inc.php');
            }

            //echo $strOutput;

            if (isset($_REQUEST['d']) && ($_REQUEST['d'] == 't')) {
                
                //echo $strOutput;
                $intDurationTime = microtime(true) - $intBeginTime;
                echo "gesamt: $intDurationTime Sek.";

            } else {

                header ('Content-type:application/pdf');
                    //header ('Content-Disposition:attachment;filename="' .$intEventId .'.pdf"');
                header ('Content-Disposition:inline;filename="' .$intEventId .'.pdf"');
                echo $strOutput;

            }



        } 

    } elseif (count($arrPart) == 1) { // PROOF  https://www.izs.de/cms/event/4c0df93ce6fd34efea1721b2cfb92816

        $strSqlProof = 'SELECT * FROM `izs_event_download` WHERE `ed_hash` = "' .$arrPart[0] .'"';
        $arrSqlProof = MySQLStatic::Query($strSqlProof);

        if (count($arrSqlProof) > 0) {

            $arrSql[0] = unserialize(stripslashes($arrSqlProof[0]['ed_dataset']));

            //print_r($arrSql); die();

            include ('template.html.php');
            echo $strOutput;

        } else {

            http_response_code(404);
            //include('my_404.php'); // provide your own HTML for the error page
            die();

        }


    }


} else {

    http_response_code(404);
    //include('my_404.php'); // provide your own HTML for the error page
    die();

}

?>