<?php

$arrAllowPath = array('svadd');
$uploaddir    = __DIR__;

$strHash = $_REQUEST['hash'];
$strId   = $_REQUEST['id'];

if (isset($_REQUEST['path']) && (in_array($_REQUEST['path'], $arrAllowPath))) {

    if (is_array($_FILES)) {

        //print_r($_FILES);

        if ($_REQUEST['path'] == 'svadd') {

            foreach ($_FILES as $strUpload => $arrUpload) {

                $arrName = pathinfo($_FILES[$strUpload]['name']);

                if ($arrName['extension'] == 'pdf') {

                    $uploadfile = $uploaddir .'/' .$strHash .'.pdf';

                    if (file_exists($uploadfile)) {
                        unlink($uploadfile);
                    }

                    if (move_uploaded_file($_FILES[$strUpload]['tmp_name'], $uploadfile)) {                    

                        $arrItemList[] = array(
                            'name' => $strId .'.pdf',
                            'link' => '/cms/upload/' .$strHash .'.pdf',
                            'state' => 'ok'
                        );

                    } else {

                        $arrItemList[] = array(
                            'name' => $_FILES[$strUpload]['name'],
                            'state' => 'error',
                            'error' => 'Fehler beim Upload!'
                        );

                    }
                    
                } else {

                    $arrItemList[] = array(
                        'name' => $_FILES[$strUpload]['name'],
                        'state' => 'error',
                        'error' => 'Es sind nur PDF Dateien erlaubt!'
                    );

                }

            }

            $arrResponce = array(

                'id' => $strHash,
                'done' => true,
                'items' => $arrItemList
        
            );

        }

        echo json_encode($arrResponce);

    }

} else {

    die();

}


?>