<?php

//CONST
if ((isset($_SERVER['SERVER_NAME']) && (strstr($_SERVER['SERVER_NAME'], 'test.') !== false))) {
  define('APP_PATH', '/data/www/sites/www.izs.de/html/');
  $strIChatUrl  = 'https://qm.smartgroups.io/run';
} else {
  define('APP_PATH', '/data/www/sites/www.izs.de/html/');
  $strIChatUrl  = 'https://izs.smartgroups.io/run';
}

define('CMS_PATH', APP_PATH .'cms/');
define('TEMP_PATH', APP_PATH .'cms/temp/');
define('FPDF_FONTPATH', APP_PATH .'cms/fpdf17/font/');

$arrChangePreview = array ( 
  'abgelehnt / Dublette' => 'abgelehnt / Dublette', 
  'abgelehnt / Frist' => 'abgelehnt / Frist', 
  'accepted' => 'angenommen', 
  'bereit für REVIEW' => 'bereit für REVIEW', 
  'enquired' => 'angefragt', 
  'in progress' => 'in Bearbeitung', 
  'new' => 'neu', 
  'no Feedback' => 'keine Antwort', 
  'no result' => 'NO RESULT', 
  'not assignable' => 'nicht zuordenbar', 
  'not OK' => 'NOT OK', 
  'OK' => 'OK', 
  'gestundet' => 'gestundet',
  'refused' => 'abgelehnt', 
  'to clear' => 'zu klären', 
  'to enquire' => 'anzufragen', 
  'zugeordnet / abgelegt' => 'zugeordnet / abgelegt', 
  'zurückgestellt von IZS' => 'zurückgestellt von IZS'
);


//GRUND
$arrGrund = array (
  //'Auskunft/Anhang fehlt' => 'Auskunft/Anhang fehlt',
  //'Auskunftsverweigerung' => 'Auskunftsverweigerung',
  //'Bagatellfall' => 'Bagatellfall',   
  //'Bagatellfall + Stundung' => 'Bagatellfall + Stundung',   
  'Beitragsnachweis fehlt' => 'Beitragsnachweis fehlt',
  'Beitragsrückstand' => 'Beitragsrückstand',
  //'Beitragsrückstand + Stundung' => 'Beitragsrückstand + Stundung',
  //'Bescheinigung so nicht veröffentlichbar' => 'Bescheinigung so nicht veröffentlichbar',
  //'Fehlender Stempel KK' => 'Fehlender Stempel KK', 
  //'Guthaben' => 'Guthaben',
  'Keine Auskunft von KK' => 'Keine Auskunft von KK',
  'Kein Konto' => 'Kein Konto',
  //'Kein Kreuz' => 'Kein Kreuz',
  'Konto geschlossen' => 'Konto geschlossen', 
  'Ratenvereinbarung' => 'Ratenvereinbarung',
  'Sonstiges' => 'Sonstiges',
  'Stundung' => 'Stundung',
);

$arrGrundColor = array (
  'Beitragsnachweis fehlt' => 'orange',
  'Beitragsrückstand' => 'red',
  'Bescheinigung so nicht veröffentlichbar' => 'yellow',
  'Fehlender Stempel KK' => 'pink', 
  'Guthaben' => 'green',
  'Keine Auskunft von KK' => 'gray',
  'Kein Konto' => 'purple',
  'Konto geschlossen' => 'blue', 
  'Sonstiges' => 'brown'
);


//NÄCHSTER SCHRITT

$arrMeilenstein = array (
  '1.1 Info an ZA: senden' => '1.1 Info an ZA: senden',
  '1.2 Info an ZA: Erinnerung 1' => '1.2 Info an ZA: Erinnerung 1',
  '1.3 Info an ZA: Erinnerung 2' => '1.3 Info an ZA: Erinnerung 2',
  '1.4 Info an ZA: Erinnerung 3' => '1.4 Info an ZA: Erinnerung 3',
  '2.1 Versenden: Anfrageformular an KK' => '2.1 Versenden: Anfrageformular an KK',
  '2.2 Versenden: Vollmacht an KK' => '2.2 Versenden: Vollmacht an KK',
  '3.1 Anrufen: KK' => '3.1 Anrufen: KK',
  '3.2 Anrufen: ZA' => '3.2 Anrufen: ZA',
  '4.1 Warten auf Rückmeldung: von KK' => '4.1 Warten auf Rückmeldung: von KK',
  '4.2 Warten auf Rückmeldung: von ZA' => '4.2 Warten auf Rückmeldung: von ZA'
);



?>
