<?php

function strCreateAlert ($strMessage = '', $strAlertType = 'info', $boolCloseable = false) {
  
  //info, success, danger, warning
  
  if ($strAlertType == '') {
    $strAlertType = 'info';
  }
  
  $strReturn = '';
  
  $strReturn.= '                <div role="alert" class="alert alert-' .$strAlertType .' alert-dismissible">' .chr(10);
  
  if ($boolCloseable) {
    $strReturn.= '                  <button aria-label="Close" data-dismiss="alert" class="close" type="button">' .chr(10);
    $strReturn.= '                    <span aria-hidden="true">×</span>' .chr(10);
    $strReturn.= '                  </button>' .chr(10);
  }
  $strReturn.= '                  ' .$strMessage .chr(10);
  $strReturn.= '                </div>' .chr(10);

  
  return $strReturn;
  
}

function strCreateTableHead ($arrHead = array(), $arrOpt = array()) {
  
  $strReturn = '';

  if (is_array($arrHead) && (count($arrHead) > 0)) {
    
    $strTh = '';
    
    foreach ($arrHead as $intIndex => $strColumName) {

      $strAdd = '';
      if (isset($arrOpt['arrNoSort']) && in_array($intIndex, $arrOpt['arrNoSort'])) {
        $strAdd = '  class="nosort" data-orderable="false"';
      }

      $strTh.= '              <th' .$strAdd .'>' .$strColumName .'</th>' .chr(10);

    }
    
    $strReturn.= '          <thead>' .chr(10);
    $strReturn.= '            <tr>' .chr(10);
    $strReturn.= $strTh;
    $strReturn.= '            </tr>' .chr(10);
    $strReturn.= '          </thead>' .chr(10);
    $strReturn.= '          <tfoot>' .chr(10);
    $strReturn.= '            <tr>' .chr(10);
    $strReturn.= $strTh;
    $strReturn.= '            </tr>' .chr(10);
    $strReturn.= '          </tfoot>' .chr(10);

  }
  
  return $strReturn;
  
}

function strCreateTable ($arrData = array(), $arrHead = array(), $strTableId = '', $boolFirstRowIsHead = true, $boolTableOnly = false, $arrWidth = array(), $arrOpt = array()) {
  
  global $strJsFootCodeRun, $strJsFootScript, $strCssHead;
  
  $strReturn = '';
  
  if ($strTableId == '') {
    $strTableId = uniqid('table_');
  }

  
  if (is_array($arrData) && (count($arrData) > 0)) {
  
    $strReturn.= '          <table id="' . $strTableId .'" class="display table table-striped" cellspacing="0" style="table-layout: auto;">' .chr(10);

    if (is_array($arrHead) && (count($arrHead) > 0)) {
      
      $strReturn.= strCreateTableHead($arrHead, $arrOpt);
    
    }
    
    $strReturn.= '          <tbody>' .chr(10);
    foreach ($arrData as $intRow => $arrRow) {
      if (isset($arrOpt['boolHasContext']) && ($arrOpt['boolHasContext'] === true)) {
        $strReturn.= '            <tr class="EventContext" data-contextid="' .$intRow .'" data-toggle="context">' .chr(10);
      } else {
        $strReturn.= '            <tr>' .chr(10);
      }
      
      foreach ($arrRow as $intCol => $strEntry) {
        $strReturn.= '              <td>' .$strEntry .'</td>' .chr(10);
      }
        
      $strReturn.= '            </tr>' .chr(10);
    }
    $strReturn.= '          </tbody>' .chr(10);


    $strReturn.= '          </table>' .chr(10);
    
    $strJsFootCodeRun.= '    $(\'#' .$strTableId .'\').DataTable( {' .chr(10);
    $strJsFootCodeRun.= '        scrollY:        \'70vh\',' .chr(10);
    $strJsFootCodeRun.= '        scrollCollapse: true,' .chr(10);
    if (count($arrHead) > 0) $strJsFootCodeRun.= '        fixedHeader:    true,' .chr(10);
    $strJsFootCodeRun.= '        autoWith:    false,' .chr(10);
    $strJsFootCodeRun.= '        paging:         false';

    
    $intCountWidth = @count($arrWidth);
    if ($intCountWidth > 0) {
      $strJsFootCodeRun.= ', ' .chr(10);
      
      $strJsFootCodeRun.= '        "aoColumns" :   [' .chr(10);
      foreach ($arrWidth as $intKey => $strValue) {
        if ($intKey < ($intCountWidth - 1)) {
          $strJsFootCodeRun.= '                         { sWidth: "' .$strValue .'" },' .chr(10);
        } else {
          $strJsFootCodeRun.= '                         { sWidth: "' .$strValue .'" }' .chr(10);
        }
      }
      $strJsFootCodeRun.= '                        ]';
 
    }

    //columnDefs: [ { orderable: false, targets: [0] }, { visible: false, targets: [5,12,13] } ],
    
    if (isset($arrOpt['arrSort']) && (count($arrOpt['arrSort']) > 0)) {

      $intCountSort = count($arrOpt['arrSort']);

      $strJsFootCodeRun.= ', ' .chr(10);
      
      $strJsFootCodeRun.= '        "aoColumns" :   [' .chr(10);
      foreach ($arrOpt['arrSort'] as $intKey => $strValue) {

        if ($strValue != 'null') {
          $strValue = '{ ' .$strValue .' }';
        }

        if ($intKey < ($intCountSort - 1)) {
          $strJsFootCodeRun.= '                         ' .$strValue .',' .chr(10);
        } else {
          $strJsFootCodeRun.= '                         ' .$strValue .'' .chr(10);
        }
      }
      $strJsFootCodeRun.= '                        ]';
  
    }

    if (isset($arrOpt['strOrder']) && ($arrOpt['strOrder'] != '')) {
      $strJsFootCodeRun.= ', ' .chr(10);
      $strJsFootCodeRun.= '        ' .$arrOpt['strOrder'] .chr(10);
    }
    
    if ($boolTableOnly) {
      $strJsFootCodeRun.= ', ' .chr(10);
      $strJsFootCodeRun.= '        "sDom":         "t"' .chr(10);
    } else {
      $strJsFootCodeRun.= chr(10);
    }
    
    $strJsFootCodeRun.= '    } );' .chr(10);

    //$strCssHead.= '  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">' .chr(10);
    $strCssHead.= '  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.2/css/jquery.dataTables.css">' .chr(10);
    
    //$strCssHead.= '  <link rel="stylesheet" href="css/jquery.dataTables.min.css">' .chr(10);
    $strCssHead.= chr(10);
    $strCssHead.= '  <style>' .chr(10);
    $strCssHead.= '    .dataTables_scrollFoot { display: none; }' .chr(10);
    $strCssHead.= '    .sc-component-main.sc-size-md { width: 50% !important; }' .chr(10);
    $strCssHead.= '  </style>' .chr(10);
    
      
    //$strJsFootScript.= '  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>' .chr(10);
    $strJsFootScript.= '  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.13.2/js/jquery.dataTables.js"></script>' .chr(10);
    //$strJsFootScript.= '  <script src="js/jquery.dataTables.min.js"></script>' .chr(10);
    
  } else {
  
    $strReturn.= strCreateAlert('Keine Datensätze vorhanden!', 'warning');
  
  }
  
  return $strReturn;
  
}

function strCreateTableHeadOld ($arrHead = array()) {
  
  $strReturn = '';

  if (is_array($arrHead) && (count($arrHead) > 0)) {
    
    $strTh = '';
    
    foreach ($arrHead as $strColumName) {

      $strTh.= '              <th>' .$strColumName .'</th>' .chr(10);

    }
    
    $strReturn.= '          <thead>' .chr(10);
    $strReturn.= '            <tr>' .chr(10);
    $strReturn.= $strTh;
    $strReturn.= '            </tr>' .chr(10);
    $strReturn.= '          </thead>' .chr(10);


  }
  
  return $strReturn;
  
}

function strCreateTableOld ($arrData = array(), $arrHead = array(), $strTableId = '', $boolFirstRowIsHead = true) {
  
  global $strJsFootCodeRun, $strJsFootScript, $strCssHead;
  
  $strReturn = '';
  
  if ($strTableId == '') {
    $strTableId = uniqid('table_');
  }

  
  if (is_array($arrData) && (count($arrData) > 0)) {
  
    $strReturn.= '          <table id="' . $strTableId .'" class="adresstable">' .chr(10);

    if (is_array($arrHead) && (count($arrHead) > 0)) {
      
      $strReturn.= strCreateTableHeadOld($arrHead);
    
    }
    
    //print_r($arrData);
    
    $strReturn.= '          <tbody>' .chr(10);
    foreach ($arrData as $intRow => $arrRow) {
      $strReturn.= '            <tr>' .chr(10);
      
      foreach ($arrRow as $intCol => $strEntry) {
        $strReturn.= '              <td>' .$strEntry .'</td>' .chr(10);
      }
        
      $strReturn.= '            </tr>' .chr(10);
    }
    $strReturn.= '          </tbody>' .chr(10);


    $strReturn.= '          </table>' .chr(10);

    $strJsFootCodeRun.= '    $(\'#' . $strTableId .'123\').fixheadertable({ ' .chr(10);
    $strJsFootCodeRun.= '       colratio    : [70, 200, 200, 200, 200],  ' .chr(10);
    $strJsFootCodeRun.= '       height      : 200,  ' .chr(10);
    $strJsFootCodeRun.= '       width       : 900,  ' .chr(10);
    $strJsFootCodeRun.= '       zebra       : true,  ' .chr(10);
    $strJsFootCodeRun.= '       resizeCol   : true, ' .chr(10);
    $strJsFootCodeRun.= '       sortable    : true, ' .chr(10);
    $strJsFootCodeRun.= '       sortType    : [\'string\', \'string\', \'string\', \'string\', \'string\'], ' .chr(10);
    $strJsFootCodeRun.= '       sortedColId : 1,  ' .chr(10);
    $strJsFootCodeRun.= '       dateFormat  : \'Y-m\' ' .chr(10);
    $strJsFootCodeRun.= '    });' .chr(10);
    
  } else {
  
    $strReturn.= strCreateAlert('Keine Datensätze vorhanden!', 'warning');
  
  }
  
  return $strReturn;
  
}


function strCreateTableSimple ($arrData = array(), $arrHead = array(), $strTableId = '', $boolFirstRowIsHead = true) {
  
  global $strJsFootCodeRun, $strJsFootScript, $strCssHead;
  
  $strReturn = '';
  
  if (is_array($arrData) && (count($arrData) > 0)) {
  
    $strReturn.= '          <table id="' . $strTableId .'" class="adresstable">' .chr(10);

    if (is_array($arrHead) && (count($arrHead) > 0)) {
      
      $strReturn.= strCreateTableHeadOld($arrHead);
    
    }
    
    //print_r($arrData);
    
    $strReturn.= '          <tbody>' .chr(10);
    foreach ($arrData as $intRow => $arrRow) {
      $strReturn.= '            <tr>' .chr(10);
      
      foreach ($arrRow as $intCol => $strEntry) {
        $strReturn.= '              <td>' .$strEntry .'</td>' .chr(10);
      }
        
      $strReturn.= '            </tr>' .chr(10);
    }
    $strReturn.= '          </tbody>' .chr(10);


    $strReturn.= '          </table>' .chr(10);
    
  } else {
  
    $strReturn.= strCreateAlert('Keine Datensätze vorhanden!', 'warning');
  
  }
  
  return $strReturn;
  
}


function strCreatePageTitle ($strHeadline = '', $strSubheadline = '') {

  $strReturn = '';
  
  $strReturn.= '  <div class="page-header">' .chr(10);
  $strReturn.= '    <div class="page-title">' .chr(10);
  $strReturn.= '      <h1>' .$strHeadline .'</h1>' .chr(10);
  
  if ($strSubheadline != '') {
    $strReturn.= '      <h4>' .$strSubheadline .'</h4>' .chr(10);
  }
  
  $strReturn.= '    </div>' .chr(10);
  $strReturn.= '  </div>' .chr(10);
  
  return $strReturn;  

}

function strCreatePageContent ($strContent = '', $boolUsePanel = true) {

  $strReturn = '';
  
  $strReturn.= '    <div class="page-content">' .chr(10);
  
  if ($boolUsePanel) {
    $strReturn.= '      <!-- Panel -->' .chr(10);
    $strReturn.= '      <div class="panel">' .chr(10);
    $strReturn.= '        <div class="panel-body container-fluid">' .chr(10);
  }

  $strReturn.= $strContent;

  if ($boolUsePanel) {
    $strReturn.= '        </div>' .chr(10);
    $strReturn.= '      </div>' .chr(10);
    $strReturn.= '      <!-- End Panel -->' .chr(10);
  }
  
  $strReturn.= '    </div>' .chr(10);
  
  return $strReturn;  

}



?>