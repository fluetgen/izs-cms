<?php

session_start();

$time_start = microtime(true);

require_once('../assets/classes/class.mysql.php');
require_once('../assets/classes/class.database.php');

$uploadDir = 'upload/';
$downloDir = 'server/php/files/';
$strServer = 'https://' .$_SERVER['SERVER_NAME'] .'/cms/' .$downloDir;

$strOutput = '';

if ($_SERVER['REMOTE_ADDR'] == '128.90.181.242') {
  //print_r($_REQUEST);
  //die();
}

if (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'save')) {
  
  $_REQUEST = str_replace('"', '\\"', $_REQUEST);
  
  if ($_REQUEST['sperr'] != 'true') {
    $_REQUEST['sperr'] = 'false';
  }
  
  //print_r($_REQUEST); die();
  
  $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Id` = "' .$_REQUEST['id'] .'"';
  $arrRow = MySQLStatic::Query($strSql);
  
  if ($arrRow[0]['Art_des_Dokuments__c'] != $_REQUEST['artd']) {
    
    $strSql2 = 'INSERT INTO `izs_event_change` 
     (`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (
     NULL, "' .$_REQUEST['id'] .'", ' .$_SESSION['id'] .', "' .$arrRow[0]['Name'] .'", "Art_des_Dokuments__c", "' .$arrRow[0]['Art_des_Dokuments__c'] .'", "' .$_REQUEST['artd'] .'", NOW(), 2)';
    $result2 = MySQLStatic::Query($strSql2);
  
    $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Art_des_Dokuments__c` = "' .$_REQUEST['artd'] .'" WHERE `Id` = "' .$_REQUEST['id'] .'"';
    $resSql = MySQLStatic::Query($strSql);
    
  }
  
  if ($arrRow[0]['Auskunft_von__c'] != $_REQUEST['ausk']) {
    
    $strSql2 = 'INSERT INTO `izs_event_change` 
     (`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (
     NULL, "' .$_REQUEST['id'] .'", ' .$_SESSION['id'] .', "' .$arrRow[0]['Name'] .'", "Auskunft_von__c", "' .$arrRow[0]['Auskunft_von__c'] .'", "' .$_REQUEST['ausk'] .'", NOW(), 2)';
    $result2 = MySQLStatic::Query($strSql2);
  
    $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Auskunft_von__c` = "' .$_REQUEST['ausk'] .'" WHERE `Id` = "' .$_REQUEST['id'] .'"';
    $resSql = MySQLStatic::Query($strSql);
    
  }
  
  if ($arrRow[0]['SF42_EventStatus__c'] != $_REQUEST['estatus']) {
    
    $strSql2 = 'INSERT INTO `izs_event_change` 
     (`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (
     NULL, "' .$_REQUEST['id'] .'", ' .$_SESSION['id'] .', "' .$arrRow[0]['Name'] .'", "SF42_EventStatus__c", "' .$arrRow[0]['SF42_EventStatus__c'] .'", "' .$_REQUEST['estatus'] .'", NOW(), 2)';
    $result2 = MySQLStatic::Query($strSql2);
  
    $strSql = 'UPDATE `SF42_IZSEvent__c` SET `SF42_EventStatus__c` = "' .$_REQUEST['estatus'] .'" WHERE `Id` = "' .$_REQUEST['id'] .'"';
    $resSql = MySQLStatic::Query($strSql);
    
  }
  
  if ($arrRow[0]['Sperrvermerk__c'] != $_REQUEST['sperr']) {
    
    $strSql2 = 'INSERT INTO `izs_event_change` 
     (`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (
     NULL, "' .$_REQUEST['id'] .'", ' .$_SESSION['id'] .', "' .$arrRow[0]['Name'] .'", "Sperrvermerk__c", "' .$arrRow[0]['Sperrvermerk__c'] .'", "' .$_REQUEST['sperr'] .'", NOW(), 2)';
    $result2 = MySQLStatic::Query($strSql2);
  
    $strSql = 'UPDATE `SF42_IZSEvent__c` SET `Sperrvermerk__c` = "' .$_REQUEST['sperr'] .'" WHERE `Id` = "' .$_REQUEST['id'] .'"';
    $resSql = MySQLStatic::Query($strSql);
    
  }
  
  if ($arrRow[0]['SF42_EventComment__c'] != $_REQUEST['comm']) {
    
    $strSql2 = 'INSERT INTO `izs_event_change` 
     (`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (
     NULL, "' .$_REQUEST['id'] .'", ' .$_SESSION['id'] .', "' .$arrRow[0]['Name'] .'", "SF42_EventComment__c", "' .str_replace('"', '\\"', $arrRow[0]['SF42_EventComment__c']) .'", "' .str_replace('"', '\\"', $_REQUEST['comm']) .'", NOW(), 2)';
    $result2 = MySQLStatic::Query($strSql2);
  
    $strSql = 'UPDATE `SF42_IZSEvent__c` SET `SF42_EventComment__c` = "' .str_replace('"', '\\"', $_REQUEST['comm']) .'" WHERE `Id` = "' .$_REQUEST['id'] .'"';
    $resSql = MySQLStatic::Query($strSql);
    
  }
  
  if ($arrRow[0]['SF42_PublishingStatus__c'] != $_REQUEST['pstatus']) {
    
    $strSql2 = 'INSERT INTO `izs_event_change` 
     (`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (
     NULL, "' .$_REQUEST['id'] .'", ' .$_SESSION['id'] .', "' .$arrRow[0]['Name'] .'", "SF42_PublishingStatus__c", "' .$arrRow[0]['SF42_PublishingStatus__c'] .'", "' .$_REQUEST['pstatus'] .'", NOW(), 2)';
    $result2 = MySQLStatic::Query($strSql2);
  
    $strSql = 'UPDATE `SF42_IZSEvent__c` SET `SF42_PublishingStatus__c` = "' .$_REQUEST['pstatus'] .'" WHERE `Id` = "' .$_REQUEST['id'] .'"';
    $resSql = MySQLStatic::Query($strSql);
    
  }

  if ($_REQUEST['temp'] != '') {

    $strSourceFileName = $_REQUEST['temp'] .'.pdf';
    $strDestFileName   = $_REQUEST['id'] .'.pdf';
    
    $strSql2 = 'INSERT INTO `izs_event_change` 
     (`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (
     NULL, "' .$_REQUEST['id'] .'", ' .$_SESSION['id'] .', "' .$arrRow[0]['Name'] .'", "SF42_DocumentUrl__c", "' .$arrRow[0]['SF42_DocumentUrl__c'] .'", "' .$strServer .$strDestFileName .'", NOW(), 2)';
    $result2 = MySQLStatic::Query($strSql2);
  
    $strSql = 'UPDATE `SF42_IZSEvent__c` SET `SF42_DocumentUrl__c` = "' .$strServer .$strDestFileName .'" WHERE `Id` = "' .$_REQUEST['id'] .'"';
    $resSql = MySQLStatic::Query($strSql);

    if (file_exists($downloDir .$strDestFileName)) {
      unlink($downloDir .$strDestFileName);
    }
    
    rename($uploadDir .$strSourceFileName, $downloDir .$strDestFileName);

  }

  //echo '1';

}  
  
if (isset($_REQUEST['id']) && !isset($_REQUEST['ac'])) {
  
  $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$_REQUEST['id'] .'"';
  $arrRow = MySQLStatic::Query($strSql);

  $strSql2 = 'SELECT * FROM `Account` WHERE `Id` = "' .$arrRow[0]['SF42_informationProvider__c'] .'"';
  $arrRow2 = MySQLStatic::Query($strSql2);

  $strSql2a = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$arrRow[0]['Betriebsnummer_ZA__c'] .'"';
  $arrRow2a = MySQLStatic::Query($strSql2a);
  
  $strSql3 = 'SELECT `Anfragestelle__c` FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrRow2a[0]['Id'] .'" AND  `Information_Provider__c` = "' .$arrRow[0]['SF42_informationProvider__c'] .'"';
  $arrRow3 = MySQLStatic::Query($strSql3);
  
  if (count($arrRow3) > 0) {
    
    $strSql4 = 'SELECT `Name` FROM `Anfragestelle__c` WHERE `Id` = "' .$arrRow3[0]['Anfragestelle__c'] .'"';
    $arrRow4 = MySQLStatic::Query($strSql4);
    $strAnfragestelle = $arrRow4[0]['Name'];
    
  } else {
    
    $strAnfragestelle = '';
    
  }
 
  $strOutput.= '<div class="phead">';

  $strOutput.= '<div style="display: inline-block; margin-right: 20px;">';
  $strOutput.= '' .$arrRow2[0]['Name'] .'<br />';
  $strOutput.= '' .$arrRow[0]['Name'] .'<br />';
  $strOutput.= 'BNR: <span class="orange">' .$arrRow[0]['Betriebsnummer_ZA__c'] .'</span>';
  $strOutput.= '</div>';

  $strOutput.= '<div style="display: inline-block;">';
  $strOutput.= '' .$strAnfragestelle .'<br />';
  $strOutput.= 'Monat: ' .$arrRow[0]['Beitragsjahr__c'] .'/' .$arrRow[0]['Beitragsmonat__c'] .'<br />';
  $strOutput.= '' .$arrRow[0]['SF42_Premium_Payer__c'] .'';
  $strOutput.= '</div>';

  $strOutput.= '</div>';

  $strOutput.= '<div style="height: 25px; margin-top: 5px; text-align: center;"><span id="validateTips" style="margin: 0px; display: block;"></span></div>';

  $strOutput.= '<div class="pbody">';

  $strTemp = uniqid();
  
  $strOutput.= '<form id="upl" action="/cms/_save.php" method="POST" enctype="multipart/form-data" class="popup">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);
  $strOutput.= '<input type="hidden" name="Id" value="' .$_REQUEST['id'] .'" id="ev_id">' .chr(10);
  $strOutput.= '<input type="hidden" name="cl_id" value="' .$_SESSION['id'] .'">' .chr(10);
  $strOutput.= '<input type="hidden" name="temp" id="temp" value="' .$strTemp .'">' .chr(10);

  $strOutput.= '<table>' .chr(10);
  $strOutput.= '<tbody>' .chr(10);

  $strRueckmeldungAm = '';
  if (($arrRow[0]['Rueckmeldung_am__c'] != '') && ($arrRow[0]['Rueckmeldung_am__c'] != '0000-00-00')) {
    $strRueckmeldungAm = date('d.m.Y', strtotime($arrRow[0]['Rueckmeldung_am__c']));
  }


  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td style="width: 145px;">Upload: </td>' .chr(10);
  $strOutput.= '    <td style="height:96px">' .chr(10);

  $strOutput.= '      <izs-upload base="https://' .$_SERVER['SERVER_NAME'] .'/cms" action="svadd?hash=' .$strTemp .'&id=' .$_REQUEST['id'] .'"></izs-upload>' .chr(10);
  
  $strOutput.= '    </div>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  /*
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Dokument: </td>' .chr(10);
  $strOutput.= '    <td><iframe id="iframe" src="_files.php?Id=' .$_REQUEST['id'] .'" style="width:372px;height:20px;border:0px;"></iframe></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);
  */

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Rückmeldung am: </td>' .chr(10);
  $strOutput.= '    <td>' .$strRueckmeldungAm .'</td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Art der Rückmeldung: </td>' .chr(10);
  $strOutput.= '    <td>' .$arrRow[0]['Art_der_Rueckmeldung__c'] .'</td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);
    
  $arrDokArt = array (
    'Kontoauskunft' => 'Form',
    'Kontoauszug' => 'KtoA',
    'Unbedenklichkeitsbescheinigung' => 'UBB',
    'Sonstiges' => 'Sonst.'
  );

  if (count($arrDokArt) > 0) {

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Art: </td>' .chr(10);
    $strOutput.= '    <td><select name="Art_des_Dokuments__c" id="Art_des_Dokuments__c">' .chr(10);
    
    foreach ($arrDokArt as $strKey => $strValue) {
      $strSelected = '';
      if ($strKey == $arrRow[0]['Art_des_Dokuments__c']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option value="' .$strKey .'"' .$strSelected .'>' .$strKey .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }

    
  $arrAuskunft = array (
    'Krankenkasse' => 'Krankenkasse',
    'Zeitarbeitsunternehmen' => 'Zeitarbeitsunternehmen',
  );
  
  if (count($arrAuskunft) > 0) {

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Auskunft von: </td>' .chr(10);
    $strOutput.= '    <td><select name="Auskunft_von__c" id="Auskunft_von__c">' .chr(10);
    
    foreach ($arrAuskunft as $strValue) {
      $strSelected = '';
      if ($strValue == $arrRow[0]['Auskunft_von__c']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option value="' .$strValue .'"' .$strSelected .'>' .$strValue .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }

  $arrChangePreview = array ( 
    'abgelehnt / Dublette' => 'abgelehnt / Dublette', 
    'abgelehnt / Frist' => 'abgelehnt / Frist', 
    'accepted' => 'angenommen', 
    'bereit für REVIEW' => 'bereit für REVIEW', 
    'enquired' => 'angefragt', 
    'gestundet' => 'gestundet',
    'in progress' => 'in Bearbeitung', 
    'new' => 'neu', 
    'no Feedback' => 'keine Antwort', 
    'no result' => 'NO RESULT', 
    'not assignable' => 'nicht zuordenbar', 
    'not OK' => 'NOT OK', 
    'OK' => 'OK', 
    'refused' => 'abgelehnt', 
    'to clear' => 'zu klären', 
    'to enquire' => 'anzufragen', 
    'zugeordnet / abgelegt' => 'zugeordnet / abgelegt', 
    'zurückgestellt von IZS' => 'zurückgestellt von IZS'
  );
    
  if (count($arrChangePreview) > 0) {
    
    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Ergebnis: </td>' .chr(10);
    $strOutput.= '    <td><select name="SF42_EventStatus__c" id="SF42_EventStatus__c">' .chr(10);
    
    foreach ($arrChangePreview as $strStatus => $strStatusName) {
      $strSelected = '';
      if ($strStatus == 'OK') { //$arrRow[0]['SF42_EventStatus__c']
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option value="' .$strStatus .'"' .$strSelected .'>' .$strStatusName .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select> <i>(' .$arrChangePreview[$arrRow[0]['SF42_EventStatus__c']] .')</i></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }

  if ($arrRow[0]['Sperrvermerk__c'] == 'true') {
    $strChecked = 'checked="checked" ';
  }
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Sperrvermerk: </td>' .chr(10);
  $strOutput.= '    <td><input type="checkbox" name="Sperrvermerk__c" id="Sperrvermerk__c" value="true" ' .$strChecked .'/></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);


  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Kommentar: </td>' .chr(10);
  $strOutput.= '    <td><textarea name="SF42_EventComment__c" id="SF42_EventComment__c" style="width: 300px;">' .$arrRow[0]['SF42_EventComment__c'] .'</textarea></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);


  $arrVisual = array ( 
    'online' => 'Ja', 
    'private' => 'Nein', 
    'ready' => 'Auf Anfrage'
  );
    
  if (count($arrVisual) > 0) {
    
    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>PDF sichtbar: </td>' .chr(10);
    $strOutput.= '    <td><select name="SF42_PublishingStatus__c" id="SF42_PublishingStatus__c">' .chr(10);
    
    foreach ($arrVisual as $strVisual => $strVisualName) {
      $strSelected = '';
      if ($strVisual == 'online') { //$arrRow[0]['SF42_PublishingStatus__c']
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option value="' .$strVisual .'"' .$strSelected .'>' .$strVisualName .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select> <i>(' .$arrVisual[$arrRow[0]['SF42_PublishingStatus__c']] .')</i></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
  

  $strOutput.= '</tbody>' .chr(10);

  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);

  $strOutput.= '</div>';



echo $strOutput;
  
}

?>