<?php

ini_set('memory_limit', '256M');

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

$lifetime = 86400;
session_start();
setcookie(session_name(), session_id(), time()+$lifetime, '/');

require_once('const.inc.php');

require_once('login.inc.php');
require_once('../assets/classes/class.mysql.php');

$strCustomCss = '';
$strCustomJs  = '';

$boolIsCron = false;

if ($_REQUEST['cron'] != '') {
  echo $_REQUEST['cron']; die();
  if ($_REQUEST['hash'] == md5('Triple2013' .$_REQUEST['cron'])) {
    $boolIsCron = true;
  }
}

if (($boolLogin == false) && (!$boolIsCron)) {
  
  $strUser = '';
  /*
  $strNav = '
  				<div class="item active last"><a class="clickable" href="index.php"><span>Login</span></a></div>' .chr(10);
  */
  $strNav = '
  				<div class="item active last"> </div>' .chr(10);
  
  $strContent = '
						<h1>Login</h1>
            <div id="formLogin" class="box">
            <form class="formLogin clearfix" method="post" action="/cms/index.php">
';

  if ($strReason == 'forced') {
    $strContent.= '<p class="red">Du wurdest automatisch ausgeloggt, weil in den 
      letzten 4 Stunden keine Aktivität <br />Deinerseits stattfand. Bitte melde Dich neu 
      an, um weiterarbeiten zu können.</p>';
  }

  $strContent.= '
            <fieldset>
              <div class="fieldRow clearfix">
                <label for="user">Benutzername</label>
                <div class="last ">
                  <input type="text" value="" id="user" name="user">
                </div>
              </div>
              <div class="fieldRow clearfix">
                <label for="company">Passwort</label>
                <div class="last ">
                  <input type="password" value="" id="pass" name="pass">
                </div>
              </div>
            </fieldset>
            <div class="fieldRowSubmit clearfix">
              <input type="submit" value="Absenden" class="button brown">
            </div>
            
            </form>
            </div>' .chr(10);

} else {
  
  $strSql = 'SELECT `cl_first`, `cl_last`, `cl_admin`, `cl_mail` FROM `cms_login` WHERE `cl_id` = "' .MySQLStatic::esc($_SESSION['id']) .'"';
  $arrResult = MySQLStatic::Query($strSql);
  if (count($arrResult) > 0) {
    $intAdmin = $arrResult[0]['cl_admin'];
    $strName = $arrResult[0]['cl_first'];
    $strUserMail = $arrResult[0]['cl_mail'];
    $strUserFirst = $arrResult[0]['cl_first'];
    $strUserLast  = $arrResult[0]['cl_last'];
    if ($strName != '') {
      $strName.= ' ' .$arrResult[0]['cl_last'];
    } else {
      $strName = $arrResult[0]['cl_last'];
    }
  }
  

  $strSql = 'SELECT `cl_id`, `cl_sg_link_id` FROM `cms_login` WHERE `cl_sg_link_id` != 0';
  $arrSql = MySQLStatic::Query($strSql);
  
  $arrIChatUserMap = array();
  
  if (count($arrSql) > 0) {
    foreach ($arrSql as $intKey => $arrUserMap) {
      $arrIChatUserMap[$arrUserMap['cl_id']] = $arrUserMap['cl_sg_link_id'];
    }
  }
  
  if (isset($arrIChatUserMap[$_SESSION['id']])) {
    $intIChatUserId = $arrIChatUserMap[$_SESSION['id']];
  } else {
    if (in_array($_SESSION['id'], array(45,46))) { //DB, MA
        $intIChatUserId = $strUserMail;
      } else {
        $intIChatUserId = $_SESSION['id'];
      }
  }

$strNav = '';

$strSql0 = "SELECT * FROM `cms_page` WHERE `cp_short` = '" .$_REQUEST['ac'] ."'";
$arrResult0 = MySQLStatic::Query($strSql0);

$strSqlR = "SELECT `cr_access` FROM `cms_right` WHERE `cr_cl_id` = '" .$_SESSION['id'] ."' AND `cr_cp_id` = '" .$arrResult0[0]['cp_id'] ."'";

if (@$_SERVER['HTTP_FKLD'] == 'on') {
    //echo $strSqlR; die();
}

$arrResultR = MySQLStatic::Query($strSqlR);
if (($arrResultR[0]['cr_access'] == 0) || ($arrResultR[0]['cr_access'] == '')) {
  @session_destroy();
  $boolLogin = false;
  header('Location: /cms/index.php');
}

$boolNoCurrent = false;
if ((count($arrResult0) > 0) && ($arrResult0[0]['cp_include'] != '')) {
  
  if (isset($_REQUEST['user']) && isset($_REQUEST['pass'])) {
    include('welcome.inc.php');
    $boolNoCurrent = true;
  } else {
    include($arrResult0[0]['cp_include']);
  }
  
  $strContent = $strOutput;
} else {
  include('welcome.inc.php');
  $boolNoCurrent = true;
  $strContent = $strOutput;
}


$arrPageList = [];
$strSql = "SELECT * FROM `cms_page` ORDER BY `cp_parent`, `cp_ord`";
$arrResult = MySQLStatic::Query($strSql);

if (count($arrResult) > 0) {
  
  foreach ($arrResult as $intKey => $arrPageRaw) {
    $arrPageList[$arrPageRaw['cp_parent']][] = $arrPageRaw;
  }

}

if (count($arrPageList[0]) > 0) {

  $arrPageRightList = [];
  $strSqlR = "SELECT * FROM `cms_right` WHERE `cr_cl_id` = '" .$_SESSION['id'] ."' AND `cr_access` = 1";
  $arrResultR = MySQLStatic::Query($strSqlR);

  if (count($arrResultR) > 0) {
    foreach ($arrResultR as $intKey => $arrRightRaw) {
      $arrPageRightList[$arrRightRaw['cr_cp_id']] = $arrRightRaw;
    }
  }

  $strNav.= '          <div id="acdnmenu">' .chr(10);
  $strNav.= '            <ul>' .chr(10);

  foreach ($arrPageList[0] as $arrPage) {
    if ($_REQUEST['ac'] == $arrPage['cp_short']) {
      $strClass = 'current ';
    } else {
      $strClass = '';
    }
    
    $arrResultR = $arrPageRightList[$arrPage['cp_id']] ?? [];
    
    if (count($arrResultR) > 0) {
      
      if ($arrPage['cp_link'] != '') {
        $strNav.= '                  <li><a href="' .$arrPage['cp_link'] .'" target="' .$arrPage['cp_target'] .'" class="' .$strClass .'">' .$arrPage['cp_name'] .'</a>';
      } elseif ($arrPage['cp_short'] != '') {
        $strNav.= '                  <li><a href="/cms/index.php?ac=' .$arrPage['cp_short'] .'" class="' .$strClass .'">' .$arrPage['cp_name'] .'</a>';
      } else {
        $strNav.= '                  <li>' .$arrPage['cp_name'] .'';
      }
    
    } else {
      continue;
    }

    $arrResult2 = $arrPageList[$arrPage['cp_id']] ?? [];
    
    if (count($arrResult2) > 0) {

      $strNav.= chr(10);
      $strNav.= '                    <ul>' .chr(10);

      foreach ($arrResult2 as $arrPage2) {
        
        if ($_REQUEST['ac'] == $arrPage2['cp_short']) {
          $strClass2 = 'current ';
        } else {
          $strClass2 = '';
        }

        $arrResultR2 = $arrPageRightList[$arrPage2['cp_id']] ?? [];
              
        if ((($arrPage2['cp_admin_only'] && $intAdmin) || (!$arrPage2['cp_admin_only'])) && (count($arrResultR2) > 0)) {
        
          if ($arrPage2['cp_link'] != '') {
            $strNav.= '                  <li><a href="' .$arrPage2['cp_link'] .'" target="' .$arrPage2['cp_target'] .'" class="' .$strClass .'">' .$arrPage2['cp_name'] .'</a>';
          } elseif ($arrPage2['cp_short'] != '') {
            $strNav.= '                      <li><a href="/cms/index.php?ac=' .$arrPage2['cp_short'] .'" class="' .$strClass2 .'">' .$arrPage2['cp_name'] .'</a></li>' .chr(10);
          } else {
            $strNav.= '                      <li>' .$arrPage2['cp_name'] .'</li>' .chr(10);
          }
        
        }

      }

      $strNav.= '                    </ul>' .chr(10);
      $strNav.= '                  </li>' .chr(10);

    } else {
      $strNav.= '</li>' .chr(10);
    }
    
  }
  
  $strNav.= '            </ul>' .chr(10);
  $strNav.= '         </div>' .chr(10);
  
} else {
  header('Location: /cms/index.php?ac=logout');
}

}

if ($_SERVER['HTTPS'] != '') {
  $strProtocol = 'https://';
} else {
  $strProtocol = 'http://';
}


?><!doctype html>
<head>
	<meta name="robots" CONTENT="noindex, nofollow">
	<meta charset="utf-8">

	<link rel="stylesheet" href="/cms/css/reset.css">
	<link rel="stylesheet" href="/cms/css/cms.css">
	<link rel="stylesheet" href="/cms/css/uploadifive.css">
  <link rel="stylesheet" href="/cms/css/accmenu.css">  
  <link rel="stylesheet" href="/cms/css/fixheadertable.css">
  
  <link rel="stylesheet" href="/cms/css/style.css">
  
  <link rel="stylesheet" href="/cms/css/ui.core.css">
  <link rel="stylesheet" href="/cms/css/ui.theme.css">
  <link rel="stylesheet" href="/cms/css/ui.tabs.css">
  <link rel="stylesheet" href="/cms/css/ui.dialog.css">
  <link rel="stylesheet" href="/cms/css/ui.datepicker.css">

  <link rel="stylesheet" href="//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="/cms/css/selectize.default.css" data-theme="default">

<?php
echo $strCustomCss;
?>

<?php 
if ((isset($_REQUEST['conly'])) || ($_REQUEST['det'] != '')) { 
  echo '<style>' .chr(10);
  echo ' #outer2 { margin-left: 20px; }' .chr(10);
  echo '</style>' .chr(10);
}  
?>

<?php if ($_REQUEST['ac'] == 'sync') { ?>
  <script src="//code.jquery.com/jquery.js"></script>
<?php } elseif (isset($_REQUEST['det'])) { ?>
  <script type="text/javascript" src="/cms/js/jquery-1.7.2.min.js"></script>
<?php } elseif (isset($_REQUEST['ac']) && (($_REQUEST['ac'] == 'revi') || ($_REQUEST['ac'] == 'cert'))) { ?>
  <script type="text/javascript" src="/cms/js/jquery-1.7.2.min.js"></script>
<?php } else { ?>
  <script type="text/javascript" src="/cms/js/jquery-1.4.2.min.js"></script>
<?php } ?>

<?php   
  if (isset($intIChatUserId)) {
    $strToken = hash_hmac('sha256', $strIChatUser .$intIChatUserId, $strIChatSecret);  
    echo '<script src="' .$strIChatUrl .'?uid=' .$strIChatUser .$intIChatUserId .'&email=' .$strUserMail .'&forename=' .$strUserFirst .'&surname=' .$strUserLast .'&appid=' .$strIChatApp .'&token=' .$strToken .'&lang="></script>' .chr(10);

    echo '<script>' .chr(10);
    echo '// Register custom component' .chr(10);
    echo 'window.smartchat.registerComponent("MyCustomIndicatorComponent", function(api, el, options) { ' .chr(10);
    echo '    return { ' .chr(10);
    echo '        link: function() { ' .chr(10);
    echo '            var uri = el.data("uri"); ' .chr(10);
    echo '            if (uri) { ' .chr(10);
    echo '                var that = this; ' .chr(10);
    echo '                api.request("context/byuri", { data : { uri : uri }}).then(function(data) { ' .chr(10);
    echo '                    that.context = data; ' .chr(10);
    echo '                    that.refresh(); ' .chr(10);
    echo '                }) ' .chr(10);
    echo '            } ' .chr(10);
    echo '        }, ' .chr(10);
    echo '        refresh: function() { ' .chr(10);
    echo '            var contextId = this.context.id;' .chr(10);
    echo '            return api.request("browse/count/topic/" + contextId, { data : { f : "open_me" }}).then(function(data_open_me) {    ' .chr(10);            
    echo '                return api.request("browse/count/topic/" + contextId, { data : { f : "done" }}).then(function(data_done) { ' .chr(10);
    echo '                    el.removeClass("done");' .chr(10);
    echo '                    el.removeClass("open_me");' .chr(10);
    echo '                    if (data_done.count > 0) { el.addClass("done"); el.attr("title", "Es gibt nur noch erledigte Chats"); }' .chr(10);
    echo '                    if (data_open_me.count > 0) { el.addClass("open_me");  el.attr("title", "Es gibt noch mindestens einen offenen Chat"); }' .chr(10);
    echo '                }) ' .chr(10);
    echo '            }) ' .chr(10);
    echo '        }, ' .chr(10);
    echo '        cmd: function(channel, data) { ' .chr(10);
    echo '            if (this.context && channel == "/topic/updated" && data && data.target == this.context.id) { ' .chr(10);
    echo '                this.refresh(); ' .chr(10);
    echo '            }      ' .chr(10); 
    echo '        } ' .chr(10);
    echo '    } ' .chr(10);
    echo '}) ' .chr(10);
    echo '</script>' .chr(10) .chr(10);

  }

  if (isset($_SERVER['SERVER_NAME']) && (strstr($_SERVER['SERVER_NAME'], 'test.') !== false)) {
    echo '<style>
    #header {
      background: url("/cms/img/bgrd_header_about_red.png") repeat-x scroll left top rgba(0, 0, 0, 0) !important;
    }
</style>';
  }
?>
  
	<script type="text/javascript" src="/cms/js/ui.core.js"></script>
	<script type="text/javascript" src="/cms/js/ui.draggable.js"></script>
	<script type="text/javascript" src="/cms/js/ui.resizable.js"></script>
	<script type="text/javascript" src="/cms/js/ui.dialog.js"></script>
	<script type="text/javascript" src="/cms/js/effects.core.js"></script>
	<script type="text/javascript" src="/cms/js/effects.highlight.js"></script>
	<script type="text/javascript" src="/cms/js/jquery.bgiframe.js"></script>

	<script type="text/javascript" src="/cms/js/ui.tabs.js"></script>
	<script type="text/javascript" src="/cms/js/ui.datepicker.js"></script>
	<script type="text/javascript" src="/cms/js/ui.datepicker-de.js"></script>

  <script src="/cms/js/jquery.uploadifive.js" type="text/javascript"></script>
  <script src="/cms/js/accmenu.js" type="text/javascript"></script>

<?php if ($_REQUEST['ac'] == 'cert') { ?>
  <script src="/cms/js/jquery.fixheadertable-1.7.2.js" type="text/javascript"></script>	
<?php } else { ?>
  <script src="/cms/js/jquery.fixheadertable.js" type="text/javascript"></script>	
<?php } ?>

  <script src="/cms/js/jquery.countdown.js" type="text/javascript"></script>	

  <script src="/cms/js/nicEdit.js" type="text/javascript"></script>
  <script src="/cms/js/script.js" type="text/javascript"></script>

  <script src="/cms/js/standalone/selectize.js"></script>

<?php
  echo $strCustomJs;
?>

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<title><?php if ($strPageTitle != '') { echo $strPageTitle; } else { echo 'CMS :: IZS-Institut für Zahlungssicherheit'; } ?></title>
	
	<base href="<?php echo $strProtocol .$_SERVER['SERVER_NAME']; ?>/cms/">
	
</head>
<body>

<div id="container">

<?php if ((isset($_REQUEST['det'])) && ($_REQUEST['det'] != '')) { ?>
  <div id="ichat">
    <a kng smartchat data-type="inbox">Inbox</a><span kng smartchat data-type="unread" class="badge badge-danger up"></span>
    <a class="newtab" href="<?php echo substr($strIChatUrl, 0, -4); ?>/inbox/unread" target="_blank">Open in new tab</a>
  </div>
<?php } ?>
  
<?php if ((empty($_REQUEST['conly'])) && ($_REQUEST['det'] == '')) { ?>
<header id="header">
  <div id="logo">
    <a href="index.php" title="Startseite des IZS-Institut für Zahlungssicherheit"><img src="/cms/img/izs-institut-zahlungssicherheit.png" alt="Logo IZS-Institut für Zahlungssicherheit" title="<?php echo 'Server: ' .$_SERVER['SERVER_ADDR'] .'' ?>"></a>
  </div>

<?php if ($boolLogin) { ?>
  <div id="search">
    <form action="/cms/index.php?ac=sear" method="post">
      <input type="hidden" name="send" value="1">
      <select name="strSearchType">
        <option value="event">Event</option>
      </select> <input type="text" name="strSearchValue" value="<?php echo $_REQUEST['strSearchValue']; ?>">
    </form>
  </div>

  <div id="ichat">
    <a kng smartchat data-type="inbox">Inbox</a><span kng smartchat data-type="unread" class="badge badge-danger up"></span>
    <a class="newtab" href="<?php echo substr($strIChatUrl, 0, -4); ?>/inbox/unread" target="_blank">Open in new tab</a>
  </div>

<?php } ?>
</header> <!-- end header -->
<?php } ?>

<div id="wrapper">
  
<?php if ((empty($_REQUEST['conly'])) && ($_REQUEST['det'] == '')) { ?>
	<section id="outer1">
		<div class="panel"

  		<nav id="verleiher_nav">
<?php if ($boolNoCurrent) echo str_replace('current', '', $strNav); else echo $strNav; ?>
  		</nav>

		</div>
	</section>

	<div id="outer3">
	</div> <!-- end outer3 -->
<?php } ?>

	<section id="outer2">
		<article>
<?php echo $strContent; ?>
		</article>
	</section>
	
</div><!-- end #wrapper -->

<footer id="footer">
	<div class="content">
	</div>
</footer> <!-- end footer -->

</div>

<script>
<?php echo $strJsFootCodeRun; ?>

<?php if ($boolNoCurrent) {
  
echo '
$( document ).ready(function() {

  setTimeout(function(){
    $("a.link").removeClass("current");
  }, 20);

});
';
  
} ?>
</script>

<script src="//cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js" crossorigin="anonymous" type="text/javascript"></script>

</body>
</html>