<?php

die();

session_start();

require_once('../assets/classes/class.mysql.php');

if ($_REQUEST['ac'] == 'del') {
  foreach (glob('server/php/files/' .$_REQUEST['Id'] .'.*') as $strFilename) {
    unlink($strFilename);
  }
  
  $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Id` = "' .$_REQUEST['Id'] .'"';
  $arrRow = MySQLStatic::Query($strSql);

  $strSql2 = 'INSERT INTO `izs_event_change` 
   (`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (
   NULL, "' .$_REQUEST['Id'] .'", ' .$_SESSION['id'] .', "' .$arrRow[0]['Name'] .'", "SF42_DocumentUrl__c", "' .$arrRow[0]['SF42_DocumentUrl__c'] .'", "", NOW(), 2)';
  $result2 = MySQLStatic::Query($strSql2);

  $strSql = 'UPDATE `SF42_IZSEvent__c` SET `SF42_DocumentUrl__c` = "" WHERE `Id` = "' .$_REQUEST['Id'] .'"';
  $resSql = MySQLStatic::Query($strSql);
  
}

$strServer = '/cms/server/php/files/';

$strOutput = '';
$arrOutput = array();

foreach (glob('server/php/files/' .$_REQUEST['Id'] .'.*') as $strFilename) {
  $arrOutput[] = str_replace('server/php/files/', '', $strFilename);
}


if (count($arrOutput) > 0) {
  $strOutput.= '<table>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);;
  foreach ($arrOutput as $intKey => $strEntry) {
    $strOutput.= '    <td><a href="' .$strServer .$strEntry .'" target="_blank">' .$strEntry .'</a></td>' .chr(10);;
    $strOutput.= '    <td><a href="_files.php?Id=' .$_REQUEST['Id'] .'&ac=del"><img src="img/f_delete.png" /></a></td>' .chr(10);
  }
  $strOutput.= '  </tr>' .chr(10);;
  $strOutput.= '</table>' .chr(10);;
}
if ($strOutput == '') exit();

?><!doctype html>
<head>
	<meta name="robots" CONTENT="noindex, nofollow">
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<link rel="stylesheet" href="/assets/css/reset.css">  
	<link rel="stylesheet" href="/cms/css/style.css" />
	
	<style>
	  *, td {margin: 0; padding: 0; }
	  td { padding-right: 7px; }
	</style>
	
	<title>CMS :: Files</title>
	
</head>
<body>

<?php echo $strOutput; ?>

</body>
</html>