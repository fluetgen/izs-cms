<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

require_once('../assets/classes/class.mysql.php');
require_once('../assets/classes/class.database.php');

require_once('classes/class.user.php');

$objUser = new User;
$arrUserListRaw = $objUser->arrGetUserList();

$arrUserList = array();
if (is_array($arrUserListRaw) && (count($arrUserListRaw) > 0)) {
  foreach ($arrUserListRaw as $intKey => $arrUserRaw) {
    $arrUserList[$arrUserRaw['cl_id']] = $arrUserRaw;
  }
}

$uploadDir = 'server/php/temp/';
$downloDir = 'server/php/files/';
$strServer = 'https://' .$_SERVER['SERVER_NAME'] .'/cms/' .$downloDir;

$boolShowIp = false;

$arrSchnittstellenProvider = array();
$strSql = 'SELECT `Id` FROM `Account` WHERE `Schnittstelle__c` = "true"';
$arrSql = MySQLStatic::Query($strSql);

if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrDataset) {
    $arrSchnittstellenProvider[] = $arrDataset['Id'];
  }
}

//print_r($arrSchnittstellenProvider);

/*
Array
(
    [0] => 001300000109k5mAAA
    [1] => 001300000109k6gAAA
    [2] => 001300000109k7dAAA
    [3] => 001300000109k8MAAQ
    [4] => 001300000109k8VAAQ
)
*/

/*
$arrSchnittstellenProvider = array(
  '001300000109k5mAAA', // ikk classic
  '001300000109k7dAAA', // BKK Pfalz
  '001300000109k8MAAQ', // SBK
  '001300000109k6gAAA'  // AOK RH
);
*/


if (isset($_REQUEST['change']) && ($_REQUEST['change'] == 1)) {

  if (isset($_REQUEST['marked'])) {
    if (is_array($_REQUEST['publish']) && (count($_REQUEST['publish']) > 0)) {
      foreach ($_REQUEST['publish'] as $strId => $intValue) {
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `RecordTypeId` = "01230000001Ao75AAC" WHERE `Id` = "' .$strId .'"';
        $resSql = MySQLStatic::Update($strSql);

        $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Id` = "' .$strId .'"';
        $arrRow = MySQLStatic::Query($strSql);

        $strSql2 = 'INSERT INTO `izs_event_change` 
         (`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (
         NULL, "' .$strId .'", ' .$_SESSION['id'] .', "' .$arrRow[0]['Name'] .'", "RecordTypeId", "01230000001Ao76AAC", "01230000001Ao75AAC", NOW(), 2)';
        $result2 = MySQLStatic::Query($strSql2);

      }
    }
    $_REQUEST['send'] = 1;
  }
  
  if (isset($_REQUEST['all'])) {
    
    $arrPeriod = explode('_', $_REQUEST['strSelDate']);
    
    $strSql = 'SELECT `Id`, `Name`, `SF42_informationProvider__c` FROM `SF42_IZSEvent__c` ';
    $strSql.= 'WHERE (`SF42_EventStatus__c` = "' .$_REQUEST['strSelSta'] .'") ';
    if (($_REQUEST['strSelInf'] != '') && ($_REQUEST['strSelInf'] != 'all')) {
      $strSql.= 'AND (`SF42_informationProvider__c` = "' .$_REQUEST['strSelInf'] .'") ';
    }
    $strSql.= 'AND (`Beitragsmonat__c` = "' .$arrPeriod[0] .'") AND (`Beitragsjahr__c` = "' .$arrPeriod[1] .'") ';
    $strSql.= 'AND `RecordTypeId` = "01230000001Ao76AAC" '; //Review -> angefragt  
    $strSql.= 'ORDER BY `Name` ASC';
    
    $arrResult = MySQLStatic::Query($strSql);
    
    if (is_array($arrResult) && (count($arrResult) > 0)) {

      foreach ($arrResult as $intKey => $strId) {
        
        if (($intAdmin != 1) && ($_SESSION['id'] != 31) && (!in_array($strId['SF42_informationProvider__c'], $arrSchnittstellenProvider))) { 
          $strSql = 'SELECT `cl_id` FROM `izs_event_change` WHERE `ev_id` = "' .$strId['Id'] .'" AND `ec_fild` = "SF42_EventStatus__c" AND `ec_new` = "OK" ORDER BY `ec_time` DESC LIMIT 1';
          //echo $strSql .chr(10);
          $resSql = MySQLStatic::Query($strSql);
          if (count($resSql) > 0) {
            if ($resSql[0]['cl_id'] != $_SESSION['id']) continue;
          } else {
            continue;
          }
        }
        
        $strSql = 'UPDATE `SF42_IZSEvent__c` SET `RecordTypeId` = "01230000001Ao75AAC" WHERE `Id` = "' .$strId['Id'] .'"';
        $resSql = MySQLStatic::Update($strSql);

        $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Id` = "' .$strId['Id'] .'"';
        $arrRow = MySQLStatic::Query($strSql);

        $strSql2 = 'INSERT INTO `izs_event_change` 
         (`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (
         NULL, "' .$strId['Id'] .'", ' .$_SESSION['id'] .', "' .$arrRow[0]['Name'] .'", "RecordTypeId", "01230000001Ao76AAC", "01230000001Ao75AAC", NOW(), 2)';
        $result2 = MySQLStatic::Query($strSql2);

      }
      
    }

  }
  
}


$strOutput = '';
$arrRequestWay = array();
$arrDecentral = array();

$arrRecordType = array (
  'Delivery' => '01230000001Ao73AAC',
  'FollowUp' => '01230000001Ao74AAC',
  'Publication' => '01230000001Ao75AAC',
  'Review' => '01230000001Ao76AAC'
);

$arrDokArt = array (
  'Kontoauskunft' => 'Kontoauskunft',
  'Kontoauszug' => 'Kontoauszug',
  'Unbedenklichkeitsbescheinigung' => 'Unbedenklichkeitsbescheinigung',
  'Sonstiges' => 'Sonstiges'
);

$arrChangePreview = array ( 
  'OK', 
  'gestundet',
  'not OK', 
  'no Feedback',
  'no result'
);

$arrSperr = array (
  'true' => 'ja',
  'false' => 'nein'
);

$arrSichtbar = array (
  'online' => 'ja',
  'private' => 'nein',
  'ready' => 'auf Anfrage'  
);

$strOutput.= '<h1>Zuordnung</h1>' .chr(10);

$strSql0 = "SELECT DISTINCT `Beitragsmonat__c`, `Beitragsjahr__c` FROM `SF42_IZSEvent__c` ORDER BY `Beitragsjahr__c` DESC, `Beitragsmonat__c` DESC";
$arrResult0 = MySQLStatic::Query($strSql0);
if (count($arrResult0) > 0) {
  
  $strOutput.= '<form method="post" action="" onSubmit="if ($(\'#strSelInf\').val() == \'\') { alert(\'Bitte wähle einen Information Provider.\'); return false;}">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);

  $strOutput.= '<table>' .chr(10);
  $strOutput.= '<tbody>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Zeitraum: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelDate" id="strSelDate">' .chr(10);
  
  $intPeriod = 0;

  foreach ($arrResult0 as $arrPeriod0) {
    $strSelected = '';
    $strValue = $arrPeriod0['Beitragsmonat__c'] .'_' .$arrPeriod0['Beitragsjahr__c'];
    
    if (empty($_REQUEST['strSelDate']) && ($intPeriod == 0)) {
      $_REQUEST['strSelDate'] = $strValue;
      //$_REQUEST['send'] = 1;
    }
    
    if ($strValue == $_REQUEST['strSelDate']) {
      $strSelected = ' selected="selected"';
    } 
    $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .str_pad($arrPeriod0['Beitragsmonat__c'], 2 ,'0', STR_PAD_LEFT) .'-' .$arrPeriod0['Beitragsjahr__c'] .'</option>' .chr(10);
    $intPeriod++;
  }
  
  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);


  $arrStatusCss = array ( 
    'OK' => 'thick', 
    'gestundet' => 'question',
    'not OK' => 'warning', 
    'enquired' => 'wait', 
    'no Feedback' => 'question',
    'no result' => 'question'
  );

  $arrStatus = array ( 
    'OK' => 'OK', 
    'gestundet' => 'gestundet',
    'not OK' => 'nicht OK', 
    'enquired' => 'angefragt', 
    'no Feedback' => 'keine Auskunft',
    'no result' => 'no result'
  );
    
  if (count($arrStatus) > 0) {
    
    if ($_REQUEST['strSelSta'] == 'all') {
      $strSelectedAll = ' selected="selected"';
    } elseif ($_REQUEST['strSelSta'] == '') {
      $_REQUEST['strSelSta'] = 'enquired';
    } else {
      $strSelectedAll = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Event-Status: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelSta" id="strSelSta">' .chr(10);
    //$strOutput.= '    <option value="all"' .$strSelectedAll .'>Alle</option>' .chr(10);
    
    foreach ($arrStatus as $strStatus => $strValue) {
      $strSelected = '';
      if ($strStatus == $_REQUEST['strSelSta']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option value="' .$strStatus .'"' .$strSelected .'>' .$strValue .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
    
    
  $strSql6 = 'SELECT DISTINCT `SF42_IZSEvent__c`.`SF42_informationProvider__c` AS `Id`, `Account`.`Name` AS `Name`, `Account`.`Dezentrale_Anfrage__c` ';
  $strSql6.= 'FROM `SF42_IZSEvent__c` INNER JOIN `Account` ';
  $strSql6.= 'ON `SF42_IZSEvent__c`.`SF42_informationProvider__c` = `Account`.`Id` ';
  $strSql6.= 'WHERE `SF42_isInformationProvider__c` = "true" ';
  $strSql6.= 'ORDER BY `Account`.`Name` ASC';
  
  $arrResult6 = MySQLStatic::Query($strSql6);
  if (count($arrResult6) > 0) {

    if ($_REQUEST['strSelInf'] == 'all') {
      $strSelectedAll = ' selected="selected"';
    } else {
      $strSelectedAll = '';
    }

    if ($_REQUEST['strSelInf'] == 'allwo') {
      $strSelectedAllWo = ' selected="selected"';
    } else {
      $strSelectedAllWo = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Information Provider: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelInf" id="strSelInf">' .chr(10);
    $strOutput.= '    <option value="">Bitte auswählen</option>' .chr(10);
    $strOutput.= '    <option value="allwo"' .$strSelectedAllWo .'>- Alle Krankenkassen - ohne Schnittstelle -</option>' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Krankenkassen - mit Schnittstelle - </option>' .chr(10);

    foreach ($arrResult6 as $arrProvider) {
      $strSelected = '';
      if ($arrProvider['Id'] == $_REQUEST['strSelInf']) {
        $strSelected = ' selected="selected"';
      } 
      
      $strAddDez = '';
      
      if ($arrProvider['Dezentrale_Anfrage__c'] == 'true') {
        $strOptClass = ' class="optv"';
        
        $strSqlDez = 'SELECT `Id`, `Name`, `CATCH_ALL__c` FROM `Anfragestelle__c` WHERE `Information_Provider__c`="' .$arrProvider['Id'] .'" ORDER BY `Name`';
        $arrSqlDez = MySQLStatic::Query($strSqlDez);

        //$arrProvider['Id'] = 'da_' .$arrProvider['Id'];
        
        if (count($arrSqlDez) > 0) {
          foreach ($arrSqlDez as $arrDez) {
            
            $strSelectedDez = '';
            if (('d_' .$arrDez['Id']) == $_REQUEST['strSelInf']) {
              $strSelectedDez = ' selected="selected"';
            } 
            
            $strAddDez.= '    <option value="d_' .$arrDez['Id'] .'"' .$strSelectedDez .'>&nbsp;&nbsp;- ' .$arrDez['Name'] .'</option>' .chr(10);
          } 
        }
        
      } else {
        $strOptClass = '';
      }
      
      $strOutput.= '    <option' .$strOptClass .' value="' .$arrProvider['Id'] .'"' .$strSelected .'>' .$arrProvider['Name'] .'</option>' .chr(10);
      $strOutput.= $strAddDez;
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }

    $strOutput.= '</tbody>' .chr(10);

    $strOutput.= '  <tfoot>' .chr(10);
    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td align="right" colspan="2"><input type="submit" name="Suche" value="Suche" /></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
    $strOutput.= '  </tfoot>' .chr(10);


  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);
    
}

if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {

  if (in_array($_REQUEST['strSelSta'], $arrChangePreview) == true) { 
    $boolMore = true;
  } else {
    $boolMore = false;
  }
  
  $arrPeriod = explode('_', $_REQUEST['strSelDate']);
  
  if (strstr($_REQUEST['strSelInf'], 'd_') != false) {
    
    $_REQUEST['strSelInf'] = substr($_REQUEST['strSelInf'], 2);
    $strAnfragestelleId = $_REQUEST['strSelInf'];
    
    $strSqlDez = 'SELECT `Id`, `Name`, `CATCH_ALL__c` FROM `Anfragestelle__c` WHERE `Id`="' .$_REQUEST['strSelInf'] .'"';
    $arrSqlDez = MySQLStatic::Query($strSqlDez);
    
    $strSqlPp = 'SELECT `Premium_Payer__c`, `Information_provider__c` FROM `Dezentrale_Anfrage_KK__c` WHERE `Anfragestelle__c` = "' .$_REQUEST['strSelInf'] .'"';
    $arrSqlPp = MySQLStatic::Query($strSqlPp);

    $arrPayer = array();
    $arrPayerAccount = array();
    $strPremiumPayer = '';
    
    if (count($arrSqlPp) > 0) {
      foreach ($arrSqlPp as $arrPremiumPayer) {
        $arrPayer[] = '"' .$arrPremiumPayer['Premium_Payer__c'] .'"';
        $_REQUEST['strSelInf'] = $arrPremiumPayer['Information_provider__c'];
      }
    
      $strPremiumPayer = implode(',', $arrPayer);
      
      $strSqlPpA = 'SELECT `SF42_Comany_ID__c` FROM `Account` WHERE `Id` IN (' .$strPremiumPayer .')';
      $arrSqlPpA = MySQLStatic::Query($strSqlPpA);

      if (count($arrSqlPpA) > 0) {
        foreach ($arrSqlPpA as $arrPpAccount) {
          $arrPayerAccount[] = '"' .$arrPpAccount['SF42_Comany_ID__c'] .'"';
        }
        $strPpAccount = implode(',', $arrPayerAccount);
      }
      
    }
    
    if ($arrSqlDez[0]['CATCH_ALL__c'] == 'true') {
      
      
      $strSqlKK = 'SELECT * FROM `Anfragestelle__c` WHERE `Id`="' .$strAnfragestelleId .'"';
      $arrKK = MySQLStatic::Query($strSqlKK);
      
      $boolIsCatchAll = true;
      $arrCatchAll = $arrKK[0];
      
      $strSqlKK = 'SELECT * FROM `Account` WHERE `Id`="' .$arrKK[0]['Information_Provider__c'] .'"';
      $arrKK = MySQLStatic::Query($strSqlKK);

      $strSql6 = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Beitragsmonat__c` = "' .$arrPeriod[0] .'" AND `Beitragsjahr__c` = "' .$arrPeriod[1] .'" AND ';
      $strSql6.= '((`SF42_EventStatus__c` = "enquired") OR (`SF42_EventStatus__c` = "to enquire")) ';
      $strSql6.= 'AND `SF42_informationProvider__c` = "' .$arrKK[0]['Id'] .'" GROUP BY `Betriebsnummer_ZA__c`';
      $arrEventList = MySQLStatic::Query($strSql6);
      
      foreach ($arrEventList as $intEv => $arrEvent) {
    
        $strSqlAc = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c`="' .$arrEvent['Betriebsnummer_ZA__c'] .'"';
        $arrAc = MySQLStatic::Query($strSqlAc);

        //PremiumPayer gesuchtem Team zugeordnet
        $strSql7 = 'SELECT * FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrAc[0]['Id'] .'" AND `Information_Provider__c` = "' .$arrKK[0]['Id'] .'" AND `Anfragestelle__c` = "' .$strAnfragestelleId .'"';
        $arrResult7 = MySQLStatic::Query($strSql7);

        if (count($arrResult7) == 0) {
          
          $strSql8 = 'SELECT `Id` FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrAc[0]['Id'] .'" AND `Information_Provider__c` = "' .$arrKK[0]['Id'] .'"';
          $arrResult8 = MySQLStatic::Query($strSql8);
          
          //keinem Team zugeordnet
          if (count($arrResult8) == 0) {
            $arrPayerAccount[] = $arrEvent['Betriebsnummer_ZA__c'];
          }
          
        }
        
      }

      $strPpAccount = implode(',', $arrPayerAccount);
        
      if ($_SERVER['REMOTE_ADDR'] == '88.217.23.84') {
        // echo '|' .print_r($arrPayerAccount, true) . chr(10) .print_r($arrCatchAllPp, true); die();
      }          



      
    }
  
    $strSql = 'SELECT `Id`, `Name`, `SF42_EventStatus__c`, `SF42_Premium_Payer__c`, `Status_Klaerung__c`, ';
    $strSql.= '`Betriebsnummer_IP__c`, `Betriebsnummer_ZA__c`, `SF42_informationProvider__c`, ';
    $strSql.= '`SF42_EventComment__c`, `Art_des_Dokuments__c`, `SF42_PublishingStatus__c`, `Sperrvermerk__c` ';
    $strSql.= 'FROM `SF42_IZSEvent__c` ';
    $strSql.= 'WHERE (`SF42_EventStatus__c` = "' .$_REQUEST['strSelSta'] .'") ';
    if (($_REQUEST['strSelInf'] != '') && ($_REQUEST['strSelInf'] != 'all') && ($_REQUEST['strSelInf'] != 'allwo')) {
      $strSql.= 'AND ((`SF42_informationProvider__c` = "' .$arrKK[0]['Id'] .'") OR (`SF42_informationProvider__c` = "' .$_REQUEST['strSelInf'] .'")) ';
    } 
    if ($_REQUEST['strSelInf'] == 'allwo') {
      $strSql.= 'AND (`SF42_informationProvider__c` NOT IN ("' .implode('", "', $arrSchnittstellenProvider) .'")) ';
    }
    $strSql.= 'AND (`Beitragsmonat__c` = "' .$arrPeriod[0] .'") AND (`Beitragsjahr__c` = "' .$arrPeriod[1] .'") ';
    
    /*
    if ($_REQUEST['strSelSta'] == 'enquired') {
      $strSql.= 'AND `RecordTypeId` = "01230000001Ao76AAC" '; //Review -> angefragt
    } elseif ($_REQUEST['strSelSta'] == 'OK') {
      $strSql.= 'AND `RecordTypeId` = "01230000001Ao75AAC" '; //Publication -> OK
    }
    */
    
    $strSql.= 'AND `RecordTypeId` = "01230000001Ao76AAC" '; //Review -> angefragt  
    
    if (strlen($strPpAccount) > 0) {
      $strSql.= 'AND `Betriebsnummer_ZA__c` IN (' .$strPpAccount .') '; //Betriebsnummer  
    }
    
    $strSql.= 'ORDER BY `Betriebsnummer_ZA__c` ASC';

    if ($_SERVER['REMOTE_ADDR'] == '46.244.171.57') {
      echo $strSql;
    }
    
  } else {
    
    if (strstr($_REQUEST['strSelInf'], 'da_') != false) {
      $_REQUEST['strSelInf'] = substr($_REQUEST['strSelInf'], 3);
      //$boolShowIp = true;
    }
  
    $strSql = 'SELECT `Id`, `Name`, `SF42_EventStatus__c`, `SF42_Premium_Payer__c`, `Status_Klaerung__c`, ';
    $strSql.= '`Betriebsnummer_IP__c`, `Betriebsnummer_ZA__c`, `SF42_informationProvider__c`, ';
    $strSql.= '`SF42_EventComment__c`, `Art_des_Dokuments__c`, `SF42_PublishingStatus__c`, `Sperrvermerk__c`, `SF42_DocumentUrl__c` ';
    $strSql.= 'FROM `SF42_IZSEvent__c` ';
    $strSql.= 'WHERE (`SF42_EventStatus__c` = "' .$_REQUEST['strSelSta'] .'") ';
    if (($_REQUEST['strSelInf'] != '') && ($_REQUEST['strSelInf'] != 'all') && ($_REQUEST['strSelInf'] != 'allwo')) {
      $strSql.= 'AND (`SF42_informationProvider__c` = "' .$_REQUEST['strSelInf'] .'") ';
    }
    if ($_REQUEST['strSelInf'] == 'allwo') {
      $strSql.= 'AND (`SF42_informationProvider__c` NOT IN ("' .implode('", "', $arrSchnittstellenProvider) .'")) ';
    }
    $strSql.= 'AND (`Beitragsmonat__c` = "' .$arrPeriod[0] .'") AND (`Beitragsjahr__c` = "' .$arrPeriod[1] .'") ';
    
    /*
    if ($_REQUEST['strSelSta'] == 'enquired') {
      $strSql.= 'AND `RecordTypeId` = "01230000001Ao76AAC" '; //Review -> angefragt
    } elseif ($_REQUEST['strSelSta'] == 'OK') {
      $strSql.= 'AND `RecordTypeId` = "01230000001Ao75AAC" '; //Publication -> OK
    }
    */
    
    $strSql.= 'AND `RecordTypeId` = "01230000001Ao76AAC" '; //Review -> angefragt  
    
    $strSql.= 'ORDER BY `Betriebsnummer_ZA__c` ASC';
    
  }
  
  $arrResult = MySQLStatic::Query($strSql);
  
  $arrInformationProvider = array();
  
  if (count($arrResult) > 0) {
    
    $arrInformationProvider = $arrResult;
  
  }
  
  //echo count($arrResult);
  //print_r($arrInformationProvider);
  //print_r($arrDecentral);

  //print_r($arrInformationProvider);
  //die();

  
  if (count($arrInformationProvider) > 0) {
    //natcasesort($arrInformationProvider);
    
    $strOutputTable.= '<table class="" id="5">' .chr(10);
    $strOutputTable.= '  <thead>' .chr(10);
    $strOutputTable.= '    <tr>' .chr(10);
    if ($boolMore) {
      $strOutputTable.= '      <th class="{sorter: false}"><input type="checkbox" value="0" name="check_all"></th>' .chr(10); //class="header"
    }
  	$strOutputTable.= '      <th class="{sorter: false}">Status</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header">Event-ID</th>' .chr(10); //class="header"
    if (($_REQUEST['strSelInf'] == 'allwo') || ($_REQUEST['strSelInf'] == 'all') || ($boolShowIp)) {
      $strOutputTable.= '      <th class="header">Infoprovider</th>' .chr(10);
    }
  	$strOutputTable.= '      <th class="header">BNR PP</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="header">Premium Payer</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="header">DL</th>' .chr(10); //class="header"
    //$strOutputTable.= '      <th class="header">DS</th>' .chr(10); //class="header"
    if ($boolMore) {
      $strOutputTable.= '      <th class="header">DokArt</th>' .chr(10); //class="header"
      $strOutputTable.= '      <th class="header">sichtbar</th>' .chr(10); //class="header"
      $strOutputTable.= '      <th class="header">SVermerk</th>' .chr(10); //class="header"
      $strOutputTable.= '      <th class="header">Kommentar</th>' .chr(10); //class="header"
      $strOutputTable.= '      <th class="header">Upload durch</th>' .chr(10); //class="header"
    }
    $strOutputTable.= '    </tr>' .chr(10);
    $strOutputTable.= '  </thead>' .chr(10);
    $strOutputTable.= '  <tbody>' .chr(10);
    
    $arrPeriod = explode('_', $_REQUEST['strSelDate']);
    
    $strNext = '';

    foreach ($arrInformationProvider as $strId => $arrEvent) {

      if ($_SERVER['REMOTE_ADDR'] == '188.193.38.60') {
        //print_r($arrEvent); die();
      }
      
      $strUserUpload = '';
      if (($boolMore)) { // && ($intAdmin != 1)
        $strSql = 'SELECT `cl_id` FROM `izs_event_change` WHERE `ev_id` = "' .$arrEvent['Id'] .'" AND `ec_fild` = "SF42_EventStatus__c" ORDER BY `ec_time` DESC LIMIT 1';
        $resSql = MySQLStatic::Query($strSql);
        if (count($resSql) > 0) {
          //if ($resSql[0]['cl_id'] != $_SESSION['id']) continue;
          $strUserUpload = $arrUserList[$resSql[0]['cl_id']]['cl_first'] .' ' .$arrUserList[$resSql[0]['cl_id']]['cl_last'];
        //} else {
          //continue;
        }
      }
      
      if ($arrEvent['Status_Klaerung__c'] == 'in Klärung') {
        $strTrClass = 'red';
      } else {
        $strTrClass = '';
      }
      
      if ($strNext == 'blubb') {
        $strNext = $arrEvent['Id'];
      }
      
      if (($_REQUEST['done'] != '') && ($_REQUEST['done'] == $arrEvent['Id'])) {
        $strNext = 'blubb';
      }

      $strTrClass = '';
      if ($arrEvent['Status_Klaerung__c'] == 'in Klärung') {
        $strTrClass = 'bgred';
      } elseif (strstr($arrEvent['Status_Klaerung__c'], 'geschlossen') != false) {
        $strTrClass = 'bggreen';
      } else {
        $strTrClass = '';
      }
      
      $strOutputTable.= '    <tr class="' .$strTrClass .'" id="e_' .$arrEvent['Id'] .'">' .chr(10);
      $strTrClass = '';

      if ($boolMore) {

        if ($_SERVER['REMOTE_ADDR'] == '62.245.254.82') {
          //var_dump($intAdmin);
        }

        if (($intAdmin == 1) || ($resSql[0]['cl_id'] == $_SESSION['id']) || ($_SESSION['id'] == 31) || (in_array($arrEvent['SF42_informationProvider__c'], $arrSchnittstellenProvider))) { // ADMIN || EIGENE || SUSI || SCHNITTSTELLEN_DOKUMENTE
          $strOutputTable.= '      <td><input type="checkbox" name="publish[' .$arrEvent['Id'] .']" value="1" id="check_' .$arrEvent['Id'] .'"></td>' .chr(10);
        } else {
          $strOutputTable.= '      <td>&nbsp;</td>' .chr(10);
        }

      }

      $strOutputTable.= '      <td align="center"><img src="/assets/images/sys/status_' .$arrStatusCss[$arrEvent['SF42_EventStatus__c']] .'.png" /></td>' .chr(10);
      $strOutputTable.= '      <td><a class="opupl" rel="' .$arrEvent['Id'] .'"><span class="' .$strTrClass .'">' .$arrEvent['Name'] .'</span></a></td>' .chr(10);
      if (($_REQUEST['strSelInf'] == 'allwo') || ($_REQUEST['strSelInf'] == 'all') || ($boolShowIp)) {
        $strSql = 'SELECT `Name` FROM `Account` WHERE `Id` = "' .$arrEvent['SF42_informationProvider__c'] .'"';
        $arrSql = MySQLStatic::Query($strSql);
        $strOutputTable.= '      <td><span class="' .$strTrClass .'">' .$arrSql[0]['Name'] .'</span></td>' .chr(10);
      }
      
      $strOutputTable.= '      <td><span class="' .$strTrClass .'">' .$arrEvent['Betriebsnummer_ZA__c'] .'</span></td>' .chr(10);
      $strOutputTable.= '      <td><span class="' .$strTrClass .'">' .$arrEvent['SF42_Premium_Payer__c'] .'</span></td>' .chr(10);
      
      if ($arrEvent['SF42_DocumentUrl__c'] != '') {
        $strDL = '<a href="' .$arrEvent['SF42_DocumentUrl__c'] .'" target="_blank"><img src="img/icon_pdf.png" alt="' .$arrEvent['SF42_DocumentUrl__c'] .'" /></a>';
      } else {
        $strDL = '';
      }
      $strOutputTable.= '      <td id="file_e_' .$arrEvent['Id'] .'_l">' .$strDL .'</td>' .chr(10);
      
      if ($boolMore) {

        $strSort = ($resSql[0]['cl_id'] == $_SESSION['id']) ? 'aaa' : $arrUserList[$resSql[0]['cl_id']]['cl_first'];

        $strOutputTable.= '      <td><span class="' .$strTrClass .'">' .$arrDokArt[$arrEvent['Art_des_Dokuments__c']] .'</span></td>' .chr(10);
        $strOutputTable.= '      <td><span class="' .$strTrClass .'">' .$arrSichtbar[$arrEvent['SF42_PublishingStatus__c']] .'</span></td>' .chr(10);
        $strOutputTable.= '      <td><span class="' .$strTrClass .'">' .$arrSperr[$arrEvent['Sperrvermerk__c']] .'</span></td>' .chr(10);
        $strOutputTable.= '      <td><span class="' .$strTrClass .'">' .$arrEvent['SF42_EventComment__c'] .'</span></td>' .chr(10);
        $strOutputTable.= '      <td><span class="hidden">' .$strSort .'</span><span class="' .$strTrClass .'">' .$strUserUpload .'</span></td>' .chr(10);
      
      }
      $strOutputTable.= '    </tr>' .chr(10);
      
    }
      
    
    $strOutputTable.= '  </tbody>' .chr(10);
    $strOutputTable.= '</table>' .chr(10);
    
    if (($_REQUEST['strSelInf'] == 'allwo') || ($_REQUEST['strSelInf'] == 'all') || ($boolShowIp)) {
      $strW = ', 250';
      $strT = ", 'string'";
      $intSum = 690; //1420
    } else {
      $strW = '';
      $strT = "";
      $intSum = 540; //1170
    }

    if ($boolMore) {
      $intTableWidth = 676;
      $intTableHeight = 81;
      $strStyleTable = 'margin-top: 51px;';
    } else {
      $intTableWidth = 166;
      $intTableHeight = 52;
      $strStyleTable = '';
    }
    
    if ($boolMore) {
    
      $strOutput.= '<div class="clearfix" style="height: 54px; width: ' .($intSum + $intTableWidth) .'px;">' .chr(10);
      $strOutput.= '  <div class="form-left">' .chr(10);
      $strOutput.= '    <h3 class="">&nbsp;</h3>' .chr(10);
      $strOutput.= '  </div>' .chr(10);
      $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 20px;">' .chr(10);
      $strOutput.= '    <form method="post" action="_get_csv.php">';
      $strOutput.= '    <input type="hidden" name="strTable" value="' .str_replace('"', "'", $strOutputTable) .'">' .chr(10);
      $strOutput.= '    <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' .chr(10);
      $strOutput.= '    </form>';
      $strOutput.= '  </div>' .chr(10);
      $strOutput.= '</div>' .chr(10);

      $strOutput.= '<form class="events" action="/cms/index.php?ac=revi" method="post">' .chr(10);
      $strOutput.= '<input type="hidden" name="change" value="1">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelDate" value="' .$_REQUEST['strSelDate'] .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelSta" value="' .$_REQUEST['strSelSta'] .'">' .chr(10);
      $strOutput.= '<input type="hidden" name="strSelInf" value="' .$_REQUEST['strSelInf'] .'">' .chr(10);

      $strOutput.= '<div class="clearfix" style="height: 54px; width: ' .($intSum + $intTableWidth) .'px;">' .chr(10);
      $strOutput.= '  <div class="form-left">' .chr(10);
      $strOutput.= '    <h3><span class="sumEvents">' .count($arrInformationProvider) .'</span> Ergebnisse gefunden:</h3>' .chr(10);
      $strOutput.= '  </div>' .chr(10);
      $strOutput.= '  <div class="form-right" style="text-align: right; padding-bottom: 10px;">' .chr(10);

      $strOutput.= '<div class="buttons" style="padding: 10px 0;">veröffentliche <button id="sel-marked" ';
      $strOutput.= 'type="submit" name="marked" value="Nur markierte" class="ui-button ui-state-default ';
      $strOutput.= 'ui-corner-all" style="margin: 0 7px;">Nur markierte</button> <button id="sel-all" type="submit" name="all" ';
      $strOutput.= 'value="Alle" class="ui-button ui-state-default ui-corner-all">Alle</button></div>' .chr(10);

      $strOutput.= '  </div>' .chr(10);
      $strOutput.= '</div>' .chr(10);
      
    } else {
    
      $strOutput.= '<div class="clearfix" style="height: 54px; width: ' .($intSum + $intTableWidth) .'px;">' .chr(10);
      $strOutput.= '  <div class="form-left">' .chr(10);
      $strOutput.= '    <h3><span class="sumEvents">' .count($arrInformationProvider) .'</span> Ergebnisse gefunden:</h3>' .chr(10);
      $strOutput.= '  </div>' .chr(10);
      $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' .chr(10);
      $strOutput.= '    <form method="post" action="_get_csv.php">';
      $strOutput.= '    <input type="hidden" name="strTable" value="' .str_replace('"', "'", $strOutputTable) .'">' .chr(10);
      $strOutput.= '    <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' .chr(10);
      $strOutput.= '    </form>';
      $strOutput.= '  </div>' .chr(10);
      $strOutput.= '</div>' .chr(10);

    }
    
    $strOutput.= $strOutputTable;
    
    if ($boolMore) {
      $strOutput.= "
<script>
$('#5').fixheadertable({ 
   colratio    : [30, 60, 80" .$strW .", 80, 250, 30, 130, 80, 80, 200, 150], 
   height      : 500, 
   width       : " .($intSum + 660) .", 
   zebra       : true, 
   sortable    : true,
   sortType    : ['', 'string', 'string'" .$strT .", 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string'],
   sortedColId : 10, 
   dateFormat  : 'Y-m'
});
</script>
";
    } else {
      $strOutput.= "
<script>
$('#5').fixheadertable({ 
   colratio    : [60, 80" .$strW .", 80, 400, 40], 
   height      : 500, 
   width       : " .($intSum + 150) .", 
   zebra       : true, 
   sortable    : true,
   sortType    : ['string', 'string'" .$strT .", 'string', 'string', 'string'],
   sortedColId : 2, 
   dateFormat  : 'Y-m'
});
</script>
";
 
}
    
    if ($boolMore) {
      $strOutput.= '</form>' .chr(10);
    }

    if (($strNext != '') && ($strNext != 'blubb') && ($_REQUEST['strSelSta'] != 'OK') && ($_SERVER['REQUEST_METHOD'] != 'POST')) {
      //$strOutput.= '<script>$(document).ready(function () { openOffersDialog(\'' .$strNext .'\'); });</script>' .chr(10);
    }

$strOutput.= '

<script src="js/izs-upload.js"></script>

<style>
#queue, #queueM {
	height: 79px;
	overflow: auto;
	margin-bottom: 10px;
	padding: 0 3px 3px;
	width: 365px;

  background-color: #5BB75B;
  background-repeat: repeat-x;
  border-radius: 4px;
  color: #FFFFFF;
}

#validateTips {
  margin: 10px 0 5px;
  width: 590px;
  color: #ff0000;
  font-weight: bold;
}


td {
  padding-bottom: 5px;
}

#dialog_note textarea {
  margin-bottom: 0px;
}

#emp, #empM {
  text-align: center;
  width: 365px;
  display: block;
  padding-top: 30px;
}

.uploadifive-queue-item .close {
    background: url("/cms/img/uploadifive-cancel.png") no-repeat scroll 0 2px rgba(0, 0, 0, 0);
}
</style>

<div id="dialog_note" title="Event bearbeiten">
</div>

	<script type="text/javascript">

	
  $(document).ready(function() {


		var tips = $("#validateTips");

		function updateTips(t) {
			$("#validateTips").text(t).effect("highlight",{color:"#F49595"},1500);
      setTimeout(function() { 
        $("#validateTips").fadeOut("slow");
      }, 1500);
		}

		function checkLength(o,f,min) {
			if ( o.val().length < min ) {
				o.addClass(\'ui-state-error\');
				updateTipsM("Bitte das Feld \'" +f +"\' ausfüllen.");
				return false;
			} else {
				return true;
			}
		}
		
		function checkEnty(o,min) {
			if ( o.val().length < min ) {
				return false;
			} else {
				return true;
			}
		}

';

//$strOutput.= 'console.log(' .$_SESSION['id'] .')';


//if (isset($_SESSION['id']) && ($_SESSION['id'] == 3)) {

  /*
$.LoadingOverlay("show");
$.LoadingOverlay("hide");
  */

  $strOutput.= '
  $("#dialog_note").dialog({
    bgiframe: true,
    autoOpen: false,
    resizable: false,
    height: 650,
    width: 620,
    modal: true,
    buttons: {
      \'OK & Weiter\': function() {

        var done = document.querySelector("izs-upload").value;
        
        if (done == null) {

          updateTips("Bitte lade ein Dokument hoch.");

        } else {
        
          var sp = \'\';
          if ($(\'#Sperrvermerk__c\').is(":checked")) {
            sp = $(\'#Sperrvermerk__c\').val();
          }

          $(".ui-dialog").LoadingOverlay("show");
          
          $.ajax({

            type: "POST",
            url:  "_ajax_review.php",
            //dataType: "html; charset=utf-8", 
            data: { id: $(\'#ev_id\').val(), artd: $(\'#Art_des_Dokuments__c\').val(), 
              ausk: $(\'#Auskunft_von__c\').val(), estatus: $(\'#SF42_EventStatus__c\').val(), 
              sperr: sp, comm: $(\'#SF42_EventComment__c\').val(),
              pstatus: $(\'#SF42_PublishingStatus__c\').val(), ac: \'save\', 
              temp: $(\'#temp\').val() }

          }).done(function() {

            console.log("done (ok & weiter).");

            //$("#dialog_note").html("Hello World");

            var tr = $("#e_" + $(\'#ev_id\').val());
            
            tr.css("display", "none");

            var sum = parseInt($(\'.sumEvents\').text());
            if (sum > 0) {
              $(\'.sumEvents\').text(sum - 1);
            }

            /* UND WEITER */

            var next = tr.nextAll("tr[id*=\'e_\']:visible").not(".bgred").first()[0];

            if (typeof next != \'undefined\') {
              nextId = next.id.split("e_")[1];
              $(next).css(\'font-weight\', \'bold\');

              $(\'#dialog_note\').load(\'_ajax_review.php\', {id: nextId}, function( response, status, xhr ) {
                  $(\'#dialog_note\').dialog(\'open\');
              });
              
              //$(\'#dialog_note\').load(\'_ajax_review.php\', {id: nextId}).dialog(\'open\');

            } else {

              $(\'#dialog_note\').dialog(\'close\');

            }

            $(".ui-dialog").LoadingOverlay("hide");
          
          }).fail(function() {

            console.log("fail (ok & weiter).");

            $(".ui-dialog").LoadingOverlay("hide");
          
          });

          
          
        }

      },
      \'Abbrechen\': function() {
        $(this).dialog(\'close\');
      },
      \'OK\': function() {

        var done = document.querySelector("izs-upload").value;
        
        if (done == null) {

          updateTips("Bitte lade ein Dokument hoch.");

        } else {
        
          var sp = \'\';
          if ($(\'#Sperrvermerk__c\').is(":checked")) {
            sp = $(\'#Sperrvermerk__c\').val();
          }
          
          $.ajax({
          
            type: "POST",
            url:  "_ajax_review.php",
            //dataType: "html; charset=utf-8", 
            data: { id: $(\'#ev_id\').val(), artd: $(\'#Art_des_Dokuments__c\').val(), 
              ausk: $(\'#Auskunft_von__c\').val(), estatus: $(\'#SF42_EventStatus__c\').val(), 
              sperr: sp, comm: $(\'#SF42_EventComment__c\').val(),
              pstatus: $(\'#SF42_PublishingStatus__c\').val(), ac: \'save\', 
              temp: $(\'#temp\').val() }
          
          }).done(function() {

            console.log("done (ok).");

            $("#dialog_note").dialog("close");

            var tr = $("#e_" + $(\'#ev_id\').val());
            
            tr.css("display", "none");

            var sum = parseInt($(\'.sumEvents\').text());
            if (sum > 0) {
              $(\'.sumEvents\').text(sum - 1);
            }

          }).fail(function() {

            console.log("fail (ok).");
          
          });
          
        }

      }
    },
    close: function() {
      tips.text(\' \');
      $(\'tr\').css(\'font-weight\', \'normal\');
    }
  });
';

//} else {

$strOutput.= '
		
	});
	
	</script>

  ';     
       
  } else {
    
    $strOutput.= '<p>Keine Premium Payer im gewählten Zeitraum vorhanden.</p>' .chr(10);
    
  }

}


?>