<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

$strOutput = '';
$strOutput.= '<h1>Benutzerdaten</h1>' .chr(10);

if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {
  
  if ($_REQUEST['cl_user'] == '') {
    $strOutput.= '<p class="bgred" style="display: table;">Fehler! Der Benutzername darf nicht leer sein.</p>' .chr(10);
  } elseif ($_REQUEST['cl_pass'] != $_REQUEST['cl_pass2']) {
    $strOutput.= '<p class="bgred" style="display: table;">Fehler! Die Passwörter stimmen nicht überein.</p>' .chr(10);
  } else {
    
    $strSql = 'UPDATE `cms_login` SET `cl_first` = "' .$_REQUEST['cl_first'] .'", `cl_last` = "' .$_REQUEST['cl_last'] .'", ';
    $strSql.= '`cl_user` = "' .$_REQUEST['cl_user'] .'", `cl_mail` = "' .$_REQUEST['cl_mail'] .'"';
    if ($_REQUEST['cl_pass'] != '') {
      $strSql.= ', `cl_pass` = MD5("' .$_REQUEST['cl_pass'] .'") ';
    }
    $strSql.= 'WHERE `cl_id` = "' .$_SESSION['id'] .'"';
    $result = MySQLStatic::Update($strSql);
    
    $_REQUEST['cl_pass'] = '';
    $_REQUEST['cl_pass2'] = '';
  }
  
}

$strSql = 'SELECT * FROM `cms_login` WHERE `cl_id` = "' .$_SESSION['id'] .'"';
$result = MySQLStatic::Query($strSql);

$strOutput.= '<form method="post" action="">' .chr(10);
$strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);

$strOutput.= '<table>' .chr(10);
$strOutput.= '<tbody>' .chr(10);


$strOutput.= '  <tr>' .chr(10);
$strOutput.= '    <td>Vorname: </td>' .chr(10);
$strOutput.= '    <td><input type="text" name="cl_first" value="' .$result[0]['cl_first'] .'"></td>' .chr(10);
$strOutput.= '  </tr>' .chr(10);

$strOutput.= '  <tr>' .chr(10);
$strOutput.= '    <td>Nachname: </td>' .chr(10);
$strOutput.= '    <td><input type="text" name="cl_last" value="' .$result[0]['cl_last'] .'"></td>' .chr(10);
$strOutput.= '  </tr>' .chr(10);

$strOutput.= '  <tr>' .chr(10);
$strOutput.= '    <td>Benutzername: </td>' .chr(10);
$strOutput.= '    <td><input type="text" name="cl_user" value="' .$result[0]['cl_user'] .'"></td>' .chr(10);
$strOutput.= '  </tr>' .chr(10);

$strOutput.= '  <tr>' .chr(10);
$strOutput.= '    <td>E-Mail: </td>' .chr(10);
$strOutput.= '    <td><input type="text" name="cl_mail" value="' .$result[0]['cl_mail'] .'"></td>' .chr(10);
$strOutput.= '  </tr>' .chr(10);

$strOutput.= '  <tr>' .chr(10);
$strOutput.= '    <td>Passwort: </td>' .chr(10);
$strOutput.= '    <td><input type="password" name="cl_pass" value="' .$_REQUEST['cl_pass'] .'"></td>' .chr(10);
$strOutput.= '  </tr>' .chr(10);

$strOutput.= '  <tr>' .chr(10);
$strOutput.= '    <td>Passwort wiederh.: </td>' .chr(10);
$strOutput.= '    <td><input type="password" name="cl_pass2" value="' .$_REQUEST['cl_pass2'] .'"></td>' .chr(10);
$strOutput.= '  </tr>' .chr(10);

$strOutput.= '</tbody>' .chr(10);

$strOutput.= '  <tfoot>' .chr(10);
$strOutput.= '  <tr>' .chr(10);
$strOutput.= '    <td align="right" colspan="2"><input type="submit" name="Suche" value="Speichern" /></td>' .chr(10);
$strOutput.= '  </tr>' .chr(10);
$strOutput.= '  </tfoot>' .chr(10);

$strOutput.= '</table>' .chr(10);
$strOutput.= '</form>' .chr(10);

?>