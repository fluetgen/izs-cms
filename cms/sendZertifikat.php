<?php

session_start();

require_once('../assets/classes/class.mysql.php');

$arrMonth = array(
  1 => 'Januar',
  2 => 'Februar',
  3 => 'März',
  4 => 'April',
  5 => 'Mai',
  6 => 'Juni',
  7 => 'Juli',
  8 => 'August',
  9 => 'September',
  10 => 'Oktober',
  11 => 'November',
  12 => 'Dezember'
);

$strSql = 'SELECT * FROM `Group__c` WHERE `Id` = "' . $_REQUEST['Id'] . '"';
$arrGroup = MySQLStatic::Query($strSql);

if (($_REQUEST['Id'] == 'a083000000iI0fqAAC') && ($_REQUEST['strSelDate'] == '4_2016')) {
  $strTemplatePath = 'templates/pruefzertifikat_mail.oceal.tpl.html';
} else {

  if (($arrGroup[0]['Gesamtstatus__c'] == 'rot') || (($_REQUEST['strSelDate'] == '9_2021') && ($_REQUEST['Id'] == 'a084S0000004hpiQAA'))) { //Schwartpaul Sonderlocke!!!
    $strTemplatePath = 'templates/pruefzertifikat_mail_nocert.tpl.html';
  } else {
    $strTemplatePath = 'templates/pruefzertifikat_mail.tpl.html';
  }
  
}

$strTemplate = file_get_contents($strTemplatePath);

$_REQUEST['Id'] = rawurldecode($_REQUEST['Id']);

$boolMail = true;

require_once('parseTemplate.inc.php');


$strSql = 'SELECT `E_Mail__c` FROM `Abonnent__c` WHERE `Verleiher_Gruppe__c` = "' . $_REQUEST['Id'] . '" AND `aktiviert__c` = "true"';
$arrAbo = MySQLStatic::Query($strSql);

$intRecipients = count($arrAbo);

if ($intRecipients > 0) {

  $strFrom = 'infoservice@izs-institut.de';
  //IZS-Prüfzertifikat <Monat>/<Jahr>: <COMPANY GROUP: Name für Infoservice
  $strSubject = 'IZS-Prüfzertifikat ' . str_pad($arrPeriod[0], 2, "0", STR_PAD_LEFT) . '.' . $arrPeriod[1] . ': ' . $strCompanyGroup;

  $strBoundary = md5(uniqid(time()));

  $strHeaders = "From: =?UTF-8?B?" . base64_encode('IZS Institut für Zahlungssicherheit') . "?= <" . $strFrom . ">\n";

  //$strHeaders = 'From: ' .$strFrom ."\r\n";
  $strHeaders.= "MIME-Version: 1.0\r\n";
  $strHeaders.= "Content-Type: multipart/mixed; boundary=" . $strBoundary . "\r\n";
  $strHeaders.= "This is a multi-part message in MIME format\r\n";

  $strMime = '--' . $strBoundary . chr(10);
  $strMime .= 'Content-Type: text/html; charset=UTF-8' . chr(10);
  $strMime .= 'Content-Transfer-Encoding: 8bit' . chr(10) . chr(10);

  $strTemplate = htmlspecialchars_decode($strTemplate, ENT_QUOTES);

  //$strBody = $strMime .chunk_split(base64_encode($strTemplate));
  $strBody = $strMime . $strTemplate;
  $strBody .= chr(10) . '--' . $strBoundary . '--';

  foreach ($arrAbo as $arrMail) {

    $strEmail = str_replace('{{Unsubscribe}}', 'https://www.izs-institut.de/api/company/' . $_REQUEST['Id'] . '/infoservice/unsubscribe/?email=' . $arrMail['E_Mail__c'] . '', $strBody);

    mail($arrMail['E_Mail__c'], '=?UTF-8?B?' . base64_encode($strSubject) . '?=', $strEmail, $strHeaders);
    //mail('system@izs-institut.de', '=?UTF-8?B?' . base64_encode($strSubject) . '?=', $strEmail, $strHeaders);

  }

  mail('infoservice@izs-institut.de', '=?UTF-8?B?' . base64_encode($strSubject) . '?=', $strBody, $strHeaders);
}


if (count($arrAbo) > 0) {
  $strSql = 'UPDATE `cms_cert` SET `cc_sent` = NOW(), `cc_recipients` = ' . $intRecipients . ' WHERE `cc_id` = "' . $arrCert[0]['cc_id'] . '"';
  MySQLStatic::Update($strSql);
}

header('Location: /cms/index.php?ac=cert&send=1&strSelText=' . $_REQUEST['strSelText'] . '&strSelDate=' . $arrPeriod[0] . '_' . $arrPeriod[1]);
