<?php

session_start();

include('conf/global.inc.php');

$strOutput = '';
$strOutput.= '<h1>Vorlagen - Events</h1>' .chr(10);
$strOutput.= '<div id="dialog_info" title="Liste der Platzhalter">' .chr(10);

$strHtml.= '<table class="lett" id="tables" style="width: 348px; font-size: 0.8em;">' .chr(10);
$strHtml.= '  <tbody>' .chr(10);

$intCount = 1;

foreach ($arrPlatzhalterListe as $arrPlatzhalter) {
  
  if ($intCount % 2 == 0) {
    $strTdClass = 'ui-state-active';
    $strTrClass = 'ui-state-active';
  } else {
    $strTdClass = 'ui-widget-content';
    $strTrClass = 'ui-widget-content';
  }

  $strHtml.= '    <tr class="' .$strTrClass .'" style="height: 25px;">' .chr(10);
  $strHtml.= '      <td class="' .$strTdClass .'">' .$arrPlatzhalter['name'] .'</td>' .chr(10);
  $strHtml.= '      <td style="width: 100px;">' .$arrPlatzhalter['info'] .'</td>' .chr(10);
  $strHtml.= '    </tr>' .chr(10);
  
  $intCount++;
} 

$strHtml.= '  </tbody>' .chr(10);
$strHtml.= '</table>' .chr(10);

$strOutput.= '<div style="padding: 5px; width: 350px;" class="ui-widget ui-widget-content ui-corner-all">' .chr(10);
$strOutput.= '<div style="border: 1px solid #E78F08;">' .chr(10);
$strOutput.= $strHtml;
$strOutput.= '</div>' .chr(10);
$strOutput.= '</div>' .chr(10);

$strContent = $strOutput;

?><!doctype html>
<head>
	<meta name="robots" CONTENT="noindex, nofollow">
	<meta charset="utf-8">

	<link rel="stylesheet" href="/cms/css/reset.css">
	<link rel="stylesheet" href="/cms/css/cms.css">
  <link rel="stylesheet" href="/cms/css/style.css">
  
  <style>
    #outer2 { margin-left: 10px; }
  </style>
  
  <link rel="stylesheet" href="/cms/css/ui.core.css">
  <link rel="stylesheet" href="/cms/css/ui.theme.css">
  <link rel="stylesheet" href="/cms/css/ui.tabs.css">
  <link rel="stylesheet" href="/cms/css/ui.dialog.css">
  <link rel="stylesheet" href="/cms/css/ui.datepicker.css">

	<title>CMS :: IZS-Institut für Zahlungssicherheit</title>
	
	<base href="https://dev.izs-institut.de/cms/">
	
</head>
<body>

<div id="container">
  

	<section id="outer2">
		<article>
<?php echo $strContent; ?>
		</article>
	</section>
	

</div>

</body>
</html>