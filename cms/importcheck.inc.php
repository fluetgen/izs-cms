<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

include('conf/global.inc.php');
require_once('../assets/classes/class.mysql.php');
require_once('../assets/classes/class.database.php');

$strOutput = '';

$strOutput.= '<h1>Import Check</h1>' .chr(10);

// SUCHE BEGIN 

$strSql0 = "SELECT DISTINCT `Beitragsmonat__c`, `Beitragsjahr__c` FROM `SF42_IZSEvent__c` ORDER BY `Beitragsjahr__c` DESC, `Beitragsmonat__c` DESC";
$arrResult0 = MySQLStatic::Query($strSql0);
if (count($arrResult0) > 0) {
  
  $strOutput.= '<form method="post" action="">' .chr(10);
  $strOutput.= '<input type="hidden" name="send" value="1">' .chr(10);

  $strOutput.= '<table>' .chr(10);
  $strOutput.= '<tbody>' .chr(10);
  $strOutput.= '  <tr>' .chr(10);
  $strOutput.= '    <td>Zeitraum: </td>' .chr(10);
  $strOutput.= '    <td><select name="strSelDate" id="strSelDate">' .chr(10);
  
  $intPeriod = 0;

  foreach ($arrResult0 as $intKey => $arrPeriod0) {
    $strSelected = '';
    $strValue = $arrPeriod0['Beitragsmonat__c'] .'_' .$arrPeriod0['Beitragsjahr__c'];
    
    if (empty($_REQUEST['strSelDate']) && ($intPeriod == 0)) {
      $_REQUEST['strSelDate'] = $strValue;
      //$_REQUEST['send'] = 1;
    }
    
    if ($strValue == $_REQUEST['strSelDate']) {
      $strSelected = ' selected="selected"';
      $strActMonth = $strValue;
      $strLasMonth = $arrResult0[$intKey+1]['Beitragsmonat__c'] .'_' .$arrResult0[$intKey+1]['Beitragsjahr__c'];
    } 
    $strOutput.= '  <option value="' .$strValue .'"' .$strSelected .'>' .str_pad($arrPeriod0['Beitragsmonat__c'], 2 ,'0', STR_PAD_LEFT) .'-' .$arrPeriod0['Beitragsjahr__c'] .'</option>' .chr(10);
    $intPeriod++;
  }
  
  $strOutput.= '    </select></td>' .chr(10);
  $strOutput.= '  </tr>' .chr(10);

  
  $strSql6 = 'SELECT `Name`, `SF42_Comany_ID__c` AS `Btnr`  FROM `Account` WHERE `SF42_use_data__c` = "true" AND `SF42_isPremiumPayer__c` = "true" AND `SF42_Comany_ID__c` != "" ORDER BY `Name`';
  
  $arrResult6 = MySQLStatic::Query($strSql6);
  if (count($arrResult6) > 0) {

    if ($_REQUEST['strSelPay'] == 'all') {
      $strSelectedAll = ' selected="selected"';
    } else {
      $strSelectedAll = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Premium Payer: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelPay" id="strSelPay">' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Premium Payer -</option>' .chr(10);
    
    foreach ($arrResult6 as $intKey => $arrProvider) {
      $strSelected = '';
      if ($arrProvider['Btnr'] == $_REQUEST['strSelPay']) {
        $strSelected = ' selected="selected"';
      } 
      
      if (($strLastName == $arrProvider['Name']) || ($arrResult6[$intKey + 1]['Name'] == $arrProvider['Name'])) {
         $strAdd = ' (' .$arrProvider['Btnr'] .')';
      } else {
        $strAdd = '';
      }
      
      $strOutput.= '    <option value="' .$arrProvider['Btnr'] .'"' .$strSelected .'>' .$arrProvider['Name'] .'' .$strAdd .'</option>' .chr(10);
      $strLastName = $arrProvider['Name'];
      
    }
    
    if ($_REQUEST['strSelPay'] != '') {
      $arrProvider['Btnr'] = $_REQUEST['strSelPay'];
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }

      
  $strSql6 = 'SELECT `Name`, `Id`  FROM `izs_premiumpayer_group` ORDER BY `Name`';
    
  $arrResult6 = MySQLStatic::Query($strSql6);
  if (count($arrResult6) > 0) {

    if ($_REQUEST['strSelPay'] == 'all') {
      $strSelectedAll = ' selected="selected"';
    } else {
      $strSelectedAll = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Company Group: </td>' .chr(10);
    $strOutput.= '    <td><select name="strSelGru" id="strSelGru">' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>- alle Company Groups -</option>' .chr(10);
    
    foreach ($arrResult6 as $intKey => $arrProvider) {
      $strSelected = '';
      if ($arrProvider['Id'] == $_REQUEST['strSelGru']) {
        $strSelected = ' selected="selected"';
      } 
      
      $strOutput.= '    <option value="' .$arrProvider['Id'] .'"' .$strSelected .'>' .$arrProvider['Name'] .'</option>' .chr(10);
      //$strLastName = $arrProvider['Name'];
      
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }
  

  $arrStatusEv = array ( 
    'ja' => 'hat Events', 
    'nein' => 'hat keine Events'
  );

  $arrStatus = $arrChangePreview;
    
  if (count($arrStatusEv) > 0) {
    
    if (($_REQUEST['strEvvSta'] == 'all') || ($_REQUEST['strEvvSta'] == '')) {
      $strSelectedAll = ' selected="selected"';
      $_REQUEST['strEvvSta'] = 'all';
    } else {
      $strSelectedAll = '';
    }

    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td>Events vorhanden: </td>' .chr(10);
    $strOutput.= '    <td><select name="strEvvSta" id="strEvvSta">' .chr(10);
    $strOutput.= '    <option value="all"' .$strSelectedAll .'>Alle</option>' .chr(10);
    
    foreach ($arrStatusEv as $strStatus => $strValue) {
      $strSelected = '';
      if ($strStatus == $_REQUEST['strEvvSta']) {
        $strSelected = ' selected="selected"';
      } 
      $strOutput.= '    <option value="' .$strStatus .'"' .$strSelected .'>' .$strValue .'</option>' .chr(10);
    }
  
    $strOutput.= '    </select></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
  }


    $strOutput.= '</tbody>' .chr(10);

    $strOutput.= '  <tfoot>' .chr(10);
    $strOutput.= '  <tr>' .chr(10);
    $strOutput.= '    <td align="right" colspan="2"><input type="submit" name="Suche" value="Suche" /></td>' .chr(10);
    $strOutput.= '  </tr>' .chr(10);
    $strOutput.= '  </tfoot>' .chr(10);


  $strOutput.= '</table>' .chr(10);
  $strOutput.= '</form>' .chr(10);
    
}

if (isset($_REQUEST['send']) && ($_REQUEST['send'] == 1)) {
  
  $strSql6 = 'SELECT `Id`, `Name`, `SF42_Comany_ID__c` AS `Btnr`, `SF42_Company_Group__c` FROM `Account` WHERE `SF42_use_data__c` = "true" AND `SF42_isPremiumPayer__c` = "true" ';
  if (($_REQUEST['strSelPay'] != 'all') && ($_REQUEST['strSelPay'] != '')) {
    $strSql6.= 'AND `SF42_Comany_ID__c` = "' .$_REQUEST['strSelPay'] .'" ';
  }
    
  if (($_REQUEST['strSelGru'] != 'all') && ($_REQUEST['strSelGru'] != '')) {
    $strSql6.= 'AND (`SF42_Company_Group__c` = "' .$_REQUEST['strSelGru'] .'") ';
  }

  $strSql6.= 'ORDER BY `Name`';
  $arrResult6 = MySQLStatic::Query($strSql6);
  
  if (count($arrResult6) > 0) {
    
    $strOutputTable = '';
    $strOutputTable.= '<table class="check" id="5">' .chr(10);
    $strOutputTable.= '  <thead>' .chr(10);
    $strOutputTable.= '    <tr>' .chr(10);
  	$strOutputTable.= '      <th class="">Premium Payer</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">BNR PP</th>' .chr(10); //class="header"
    $strOutputTable.= '      <th class="">Aktueller Monat</th>' .chr(10);
  	$strOutputTable.= '      <th class="">Vormonat</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Abweichung</th>' .chr(10); //class="header"
  	$strOutputTable.= '      <th class="">Info</th>' .chr(10); //class="header"
    $strOutputTable.= '    </tr>' .chr(10);
    $strOutputTable.= '  </thead>' .chr(10);
    $strOutputTable.= '  <tbody>' .chr(10);
    
    $strExportTable = $strOutputTable;

    $arrPeriod = explode('_', $_REQUEST['strSelDate']);

    $arrAktMonth = array();
    $strSql7 = 'SELECT COUNT(*) AS `anz`, `Betriebsnummer_ZA__c` FROM `SF42_IZSEvent__c` WHERE `Beitragsmonat__c` = "' .$arrPeriod[0] .'" AND `Beitragsjahr__c` = "' .$arrPeriod[1] .'" ';
    if (($_REQUEST['strSelPay'] != 'all') && ($_REQUEST['strSelPay'] != '')) {
      $strSql7.= 'AND `Betriebsnummer_ZA__c` = "' .$arrProvider['Btnr'] .'" ';
    }
    $strSql7.= 'GROUP BY `Betriebsnummer_ZA__c`';
    $arrEventsM1 = MySQLStatic::Query($strSql7);
    if (count($arrEventsM1) > 0) {
      foreach($arrEventsM1 as $arrPPInfo) {
        $arrAktMonth[$arrPPInfo['Betriebsnummer_ZA__c']] = $arrPPInfo['anz'];
      }
    }

    //print_r($arrAktMonth); die();
    
    if ($arrPeriod[0] > 1) {
      $arrPeriod2[0] = $arrPeriod[0] - 1;
      $arrPeriod2[1] = $arrPeriod[1];
    } else {
      $arrPeriod2[0] = 12;
      $arrPeriod2[1] = $arrPeriod[1] - 1;
    }
    
    $arrPreMonth = array();
    $strSql8 = 'SELECT COUNT(*) AS `anz`, `Betriebsnummer_ZA__c` FROM `SF42_IZSEvent__c` WHERE `Beitragsmonat__c` = "' .$arrPeriod2[0] .'" AND `Beitragsjahr__c` = "' .$arrPeriod2[1] .'" ';
    if (($_REQUEST['strSelPay'] != 'all') && ($_REQUEST['strSelPay'] != '')) {
      $strSql8.= 'AND `Betriebsnummer_ZA__c` = "' .$arrProvider['Btnr'] .'" ';
    }
    $strSql8.= 'GROUP BY `Betriebsnummer_ZA__c`';
    $arrEventsM2 = MySQLStatic::Query($strSql8);
    if (count($arrEventsM2) > 0) {
      foreach($arrEventsM2 as $arrPPInfo) {
        $arrPreMonth[$arrPPInfo['Betriebsnummer_ZA__c']] = $arrPPInfo['anz'];
      }
    }

    $arrStatCache = array();

    $arrPeriod[0] = str_pad($arrPeriod[0], 2 ,'0', STR_PAD_LEFT);

    foreach ($arrResult6 as $intKey => $arrProvider) {

      $strFileSql = 'SELECT `is_filename` FROM `import_stats` WHERE is_grid = "' .$arrProvider['SF42_Company_Group__c'] .'" AND `is_date` = "' .$arrPeriod[1] .'-' .$arrPeriod[0] .'-01"';
      $arrFile = MySQLStatic::Query($strFileSql);

      $strHint = '';

      if (is_array($arrFile) && (count($arrFile) > 0)) {
        if (!isset($arrStatCache[$arrProvider['SF42_Company_Group__c']])) {
          $strStatSql = 'SELECT SUM(`is_datasets`) AS `is_datasets`, `is_result_ok`, `is_result_error`, `is_result_doublet_system`, `is_result_doublet_file`, ';
          $strStatSql.= '`is_result_saldo_null`, `is_result_saldo_no`, `is_result_saldo_unknown` FROM `import_stats` WHERE `is_filename` = "' .$arrFile[0]['is_filename'] .'" ';
          $strStatSql.= 'AND `is_date` = "' .$arrPeriod[1] .'-' .$arrPeriod[0] .'-01"';
          $arrFile = MySQLStatic::Query($strStatSql);
          $arrStatCache[$arrProvider['SF42_Company_Group__c']] = $arrFile;
        } else {
          $arrFile = $arrStatCache[$arrProvider['SF42_Company_Group__c']];
        }

        $strHint.= '<b>Statistik für Company Group:</b><br />';
        $strHint.= 'Anzahl Datensätze: ' .$arrFile[0]['is_datasets'] .'<br />';
        $strHint.= 'Ergebnis OK: ' .$arrFile[0]['is_result_ok'] .'<br />';
        $strHint.= 'Ergebnis NOT OK: ' .$arrFile[0]['is_result_error'] .'<br />';
        $strHint.= 'Dubletten im System: ' .$arrFile[0]['is_result_doublet_system'] .'<br />';
        $strHint.= 'Dubletten in der Import-Datei: ' .$arrFile[0]['is_result_doublet_file'] .'<br />';
        $strHint.= 'Saldo Null: ' .$arrFile[0]['is_result_saldo_null'] .'<br />';
        $strHint.= 'Saldo Nein: ' .$arrFile[0]['is_result_saldo_no'] .'<br />';
        $strHint.= 'Saldo Unbekannt: ' .$arrFile[0]['is_result_saldo_unknown'] .'';

      }

      if (empty($arrAktMonth[$arrProvider['Btnr']])) {
        //echo $arrProvider['Btnr'] .'| -> |' .$arrAktMonth[$arrProvider['Btnr']] .'|'.chr(10);
        $arrAktMonth[$arrProvider['Btnr']] = 0;
      }
      if (empty($arrPreMonth[$arrProvider['Btnr']])) {
        $arrPreMonth[$arrProvider['Btnr']] = 0;
      }
      
      $intDiff = ($arrAktMonth[$arrProvider['Btnr']] - $arrPreMonth[$arrProvider['Btnr']]);
      
      /*
      if ($intDiff < 0) {
        $strDiff = '<span class="red">' .$intDiff .'</span>';
      } else {
        $strDiff = $intDiff;
      }
      */
      $strDiff = $intDiff;
      
      if ($arrAktMonth[$arrProvider['Btnr']] == 0) {
        $strTrClass = 'bgred';
      } else {
        $strTrClass = '';
      }
      
      if ((($_REQUEST['strEvvSta'] == 'nein') && ($arrAktMonth[$arrProvider['Btnr']] == 0)) || 
          (($_REQUEST['strEvvSta'] == 'ja') && ($arrAktMonth[$arrProvider['Btnr']] > 0)) || 
          ($_REQUEST['strEvvSta'] == 'all')) {
        $strOutputTable.= '    <tr id="' .$arrProvider['Id'] .'" class="' .$strTrClass .'">' .chr(10);
      	$strOutputTable.= '      <td>' .$arrProvider['Name'] .'</td>' .chr(10); //class="header"
      	$strOutputTable.= '      <td>' .$arrProvider['Btnr'] .'</td>' .chr(10); //class="header"
        $strOutputTable.= '      <td><a href="#" onClick="wopen(\'index.php?ac=sear&strSelDate=' .$strActMonth .'&strSelPay=' .$arrProvider['Btnr'] .'&send=1&conly=1\'); return false;">' .$arrAktMonth[$arrProvider['Btnr']] .'</a></td>' .chr(10);
      	$strOutputTable.= '      <td><a href="#" onClick="wopen(\'index.php?ac=sear&strSelDate=' .$strLasMonth .'&strSelPay=' .$arrProvider['Btnr'] .'&send=1&conly=1\'); return false;">' .$arrPreMonth[$arrProvider['Btnr']] .'</a></td>' .chr(10); //class="header"
      	$strOutputTable.= '      <td>' .$strDiff .'</td>' .chr(10); //class="header"
      	$strOutputTable.= '      <td><img src="img/information.png" class="hint" /><span class="hidden">' .$strHint .'</span></td>' .chr(10); //class="header"
        $strOutputTable.= '    </tr>' .chr(10);

        $strExportTable.= '    <tr id="' .$arrProvider['Id'] .'" class="' .$strTrClass .'">' .chr(10);
      	$strExportTable.= '      <td>' .$arrProvider['Name'] .'</td>' .chr(10); //class="header"
      	$strExportTable.= '      <td>' .$arrProvider['Btnr'] .'</td>' .chr(10); //class="header"
        $strExportTable.= '      <td>' .$arrAktMonth[$arrProvider['Btnr']] .'</td>' .chr(10);
      	$strExportTable.= '      <td>' .$arrPreMonth[$arrProvider['Btnr']] .'</td>' .chr(10); //class="header"
      	$strExportTable.= '      <td>' .$strDiff .'</td>' .chr(10); //class="header"
      	$strExportTable.= '      <td></td>' .chr(10); //class="header"
        $strExportTable.= '    </tr>' .chr(10);
      }
    }
    
    $strOutputTable.= '  </tbody>' .chr(10);
    $strOutputTable.= '</table>' .chr(10);

    $strExportTable.= '  </tbody>' .chr(10);
    $strExportTable.= '</table>' .chr(10);

    
    $strOutput.= '<div class="clearfix" style="height: 48px; width: 860px;">' .chr(10);
    $strOutput.= '  <div class="form-left">' .chr(10);
    $strOutput.= '    <h3>' .count($arrResult6) .' Ergebnisse gefunden:</h3>' .chr(10);
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '  <div class="form-right" style="text-align: right; padding-top: 10px;">' .chr(10);
    $strOutput.= '    <form method="post" action="_get_csv.php">';
    $strOutput.= '    <input type="hidden" name="strTable" value="' .str_replace('"', "'", $strExportTable) .'">' .chr(10);
    $strOutput.= '    <button id="export-csv" class="ui-button ui-state-default ui-corner-all">Export</button>' .chr(10);
    $strOutput.= '    </form>';
    $strOutput.= '  </div>' .chr(10);
    $strOutput.= '</div>' .chr(10);
        
    $strOutput.= $strOutputTable;

    $strOutput.= "
<script>
$('#5').fixheadertable({ 
   colratio    : [400, 80, 120, 90, 90, 40], 
   height      : 500, 
   width       : 845, 
   zebra       : true, 
   sortable    : true,
   sortType    : ['string', 'string', 'integer', 'integer', 'integer', 'string'],
   sortedColId : 0, 
   dateFormat  : 'd.m.Y'
});

</script>
";

  
  } else {
    
    $strOutput.= '<p>Keine Events im gewählten Zeitraum vorhanden.</p>' .chr(10);
    
  }
}


$strOutput.= '';



?>