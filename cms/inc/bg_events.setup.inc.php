<?php


$objBg = new Bg;

/* ----------------------- */

$arrCategory = array (
    '', 
    'E-Mail', 
    'Fax', 
    'Post' 
  );
  
  require_once('../assets/classes/class.mysql.php');
  
  $strOutput = '';

  $strOutput.= '
<style>

#tables {
  width: 610px;
}
table {
  border-collapse: collapse;
  border-spacing: 0;
}
user agent stylesheet
table {
  display: table;
  border-collapse: separate;
  border-spacing: 2px;
  border-color: grey;
}
.ui-widget-content {
  border: 1px solid #dddddd;
  background: #eeeeee url(/cms/images/ui-bg_highlight-soft_100_eeeeee_1x100.png) 50% top repeat-x;
  color: #333333;
}

.ui-widget {
  font-family: Trebuchet MS, Tahoma, Verdana, Arial, sans-serif;
  font-size: 1.1em;
}

.cat {
    float: right;
    padding-right: 10px;
}

.acc-c {
    padding: 4px 10px 4px 30px;
    background-image: url(/cms/img/acc-closed.png);
    background-repeat: no-repeat;
    background-position: 5px 3px;
    cursor: pointer;
}

.acc-o {
    padding: 4px 10px 4px 30px;
    background-image: url(/cms/img/acc-open.png);
    background-repeat: no-repeat;
    background-position: 5px 3px;
    cursor: pointer;
}

table.lett input, table.lett textarea {
    width: 600px;
}

</style>
';

$strJsFootCodeRun.= '

//Auf- und Zuklappen
$(".head").bind(\'click\', function() {
  var dbid = $(this).attr(\'id\').replace(\'sn_\', \'\');
  if ($(\'tr#text_\' + dbid).hasClass(\'hidden\')) {
    $(\'tr#name_\' + dbid).removeClass(\'hidden\');
    $(\'tr#head_\' + dbid).removeClass(\'hidden\');
    $(\'tr#text_\' + dbid).removeClass(\'hidden\');
    $(\'#snf_\' + dbid).removeClass(\'hidden\');
    $(\'#sn_\' + dbid).removeClass(\'acc-c\');
    $(\'#sn_\' + dbid).addClass(\'acc-o\');
    addArea(\'in_text_\' + dbid);
  } else {
    $(\'tr#name_\' + dbid).addClass(\'hidden\');
    $(\'tr#head_\' + dbid).addClass(\'hidden\');
    $(\'tr#text_\' + dbid).addClass(\'hidden\');
    $(\'#snf_\' + dbid).addClass(\'hidden\');
    $(\'#sn_\' + dbid).removeClass(\'acc-o\');
    $(\'#sn_\' + dbid).addClass(\'acc-c\');
    removeArea(\'in_text_\' + dbid);
  }
});

$(\'.head\').bind(\'mouseover\', function() {
    $(this).css(\'cursor\', \'pointer\');
});


function addArea(inst) {
    var area2 = new nicEditor({
      buttonList : [\'save\',\'bold\',\'italic\',\'underline\',\'left\',\'center\',\'right\',\'indent\',\'outdent\',\'xhtml\'], 
      maxHeight : 300, 
      onSave : function(content, id, instance) {
        $.ajax({
          type: "POST",
          url:  "_ajax.php",
          dataType: "html; charset=utf-8", 
          data: { id: id, content: content, type: \'text\' },
          success: function(result) {
            /*
            $.blockUI({ 
                message: \'Vorlage gespeichert.\', 
                fadeIn: 10, 
                fadeOut: 700, 
                timeout: 1500, 
                showOverlay: false, 
                centerY: false, 
                css: { 
                    width: \'350px\', 
                    border: \'none\', 
                    padding: \'5px\', 
                    backgroundColor: \'#000\', 
                    \'-webkit-border-radius\': \'10px\', 
                    \'-moz-border-radius\': \'10px\', 
                    opacity: .6, 
                    color: \'#fff\' 
                } 
            });
            */
          }
        });
      }
    }).panelInstance(inst).addEvent(\'blur\', function() {
      /*
      if (typeof nicEditors.findEditor(inst) != \'undefined\') {
        var content = nicEditors.findEditor(inst).getContent();
          $.ajax({
            type: "POST",
            url:  "_ajax.php",
            data: { id: inst, content: content, type: \'text\' },
            success: function(result) {
              $.blockUI({ 
                  message: \'Vorlage gespeichert.\', 
                  fadeIn: 10, 
                  fadeOut: 700, 
                  timeout: 1500, 
                  showOverlay: false, 
                  centerY: false, 
                  css: { 
                      width: \'350px\', 
                      border: \'none\', 
                      padding: \'5px\', 
                      backgroundColor: \'#000\', 
                      \'-webkit-border-radius\': \'10px\', 
                      \'-moz-border-radius\': \'10px\', 
                      opacity: .6, 
                      color: \'#fff\' 
                  } 
              }); 
            }
          });
      }*/
    });
  }
  function removeArea(inst) {
    if (typeof area2 != \'undefined\') {
      area2.removeInstance(inst);
    }
  }
  
  
';

  $strOutput.= '<div class="panel"><div class="panel-body">' .chr(10);

  $strOutput.= '<div class="clearfix" style="height: 30px;">' .chr(10);
  $strOutput.= '  <div style="float: left; width: 100px;">' .chr(10);
  $strOutput.= '    <span class="add"><img src="/cms/img/f_add.png" alt="Hinzufügen" title="Hinzufügen" border="0" /> hinzufügen</span>' .chr(10);
  $strOutput.= '  </div><div style="float: left; width: 150px;">' .chr(10);
  $strOutput.= '    <span class="showP"><img id="show-info" src="/cms/img/placeholder.png" alt="Liste der Platzhalter" title="Liste der Platzhalter" border="0" /> Platzhalter anzeigen</span>' .chr(10);
  $strOutput.= '  </div>' .chr(10);
  $strOutput.= '</div>' .chr(10);
  
  
  $strSql = 'SELECT * FROM `cms_text` WHERE `ct_type` = 3 ORDER BY `ct_name`';
  $arrResult = MySQLStatic::Query($strSql);
  
  if (count($arrResult) > 0) {
    
    $strOutput.= '<div style="padding: 5px; width: 617px;" class="ui-widget ui-widget-content ui-corner-all">' .chr(10);
    $strOutput.= '<div style="border: 1px solid #E78F08;">' .chr(10);
    $strOutput.= '<table class="lett" id="tables">' .chr(10);
    $strOutput.= '  <thead>' .chr(10);
    $strOutput.= '    <tr>' .chr(10);
      $strOutput.= '      <th class="ui-widget-content ui-state-default">Vorlage</th>' .chr(10); //class="header"
    $strOutput.= '    </tr>' .chr(10);
    $strOutput.= '  </thead>' .chr(10);
    $strOutput.= '  <tbody>' .chr(10);
    
    $intCount = 1;
  
    foreach ($arrResult as $arrText) {
      
      if ($intCount % 2 == 0) {
        $strTdClass = 'ui-state-active';
        $strTrClass = 'ui-state-active';
      } else {
        $strTdClass = 'ui-widget-content';
        $strTrClass = 'ui-widget-content';
      }
          
      $strOutput.= '    <tr class="' .$strTrClass .'">' .chr(10);
      $strOutput.= '      <td class="' .$strTdClass .'" id="h_' .$arrText['ct_id'] .'">' .chr(10);
      $strOutput.= '        <span id="sn_' .$arrText['ct_id'] .'" class="head acc-c">' .$arrText['ct_name'] .'</span>' .chr(10);
      $strOutput.= '        <span class="hidden" id="snf_' .$arrText['ct_id'] .'">' .chr(10);
      $strOutput.= '          <a href="createPreview.php?ctId=' .$arrText['ct_id'] .'&strSelDate=' .date('m\_Y') .'&type=Brief"><img src="/cms/img/f_preview.png" alt="Vorschau" title="Vorschau" border="0" /></a>' .chr(10);
      $strOutput.= '          <img id="fc_' .$arrText['ct_id'] .'" class="copy" src="/cms/img/f_copy.png" alt="Kopieren" title="Kopieren" border="0" />' .chr(10);
      $strOutput.= '          <img id="fd_' .$arrText['ct_id'] .'" class="delete" src="/cms/img/f_delete.png" alt="Löschen" title="Löschen" border="0" />' .chr(10);
      $strOutput.= '        </span>' .chr(10);
  
      $strOutput.= '        <span class="cat"><select id="cat_' .$arrText['ct_id'] .'">' .chr(10);
      
      foreach ($arrCategory as $strCategory) {
        if ($strCategory == $arrText['ct_category']) {
          $strSelected = ' selected="selected"';
        } else {
          $strSelected = '';
        }
        $strOutput.= '          <option value="' .$strCategory .'"' .$strSelected .'>' .$strCategory .'</option>' .chr(10);
      }
      
       
      $strOutput.= '        </select></span>' .chr(10);
  
      $strOutput.= '      </td>' .chr(10);
      $strOutput.= '    </tr>' .chr(10);
  
      $strOutput.= '    <tr class="hidden ' .$strTrClass .'" id="name_' .$arrText['ct_id'] .'">' .chr(10);
      $strOutput.= '      <td class="' .$strTdClass .'"><input type="text" name="in_name[' .$arrText['ct_id'] .']" value="' .$arrText['ct_name'] .'" id="in_name_' .$arrText['ct_id'] .'" class="inname"></td>' .chr(10);
      $strOutput.= '    </tr>' .chr(10);
  
      $strOutput.= '    <tr class="hidden ' .$strTrClass .'" id="head_' .$arrText['ct_id'] .'">' .chr(10);
      $strOutput.= '      <td class="' .$strTdClass .'"><input type="text" name="in_head[' .$arrText['ct_id'] .']" value="' .$arrText['ct_head'] .'" id="in_head_' .$arrText['ct_id'] .'" class="inhead"></td>' .chr(10);
      $strOutput.= '    </tr>' .chr(10);
      
      $strOutput.= '    <tr class="hidden ' .$strTrClass .'" id="text_' .$arrText['ct_id'] .'">' .chr(10);
      $strOutput.= '      <td class="' .$strTdClass .'"><textarea name="in_text[' .$arrText['ct_id'] .']" id="in_text_' .$arrText['ct_id'] .'">' .$arrText['ct_text'] .'</textarea></td>' .chr(10);
      $strOutput.= '    </tr>' .chr(10);
      
      $intCount++;
      
    }
    
    $strOutput.= '  </tbody>' .chr(10);
    $strOutput.= '</table>' .chr(10);
    
          
  } else {
    
    $strOutput.= '<p>Keine Anschreiben vorhanden.</p>' .chr(10);
    
  }

$strOutput.= '</div></div>' .chr(10);

/* ----------------------- */

$strIncOutput.= $strOutput;


?>