<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

session_start();

require_once('../const.inc.php');

require_once('../../assets/classes/class.mysql.php');
require_once('../../assets/classes/class.database.php');

require_once('refactor.inc.php');

$intUser = $_SESSION['id'];

$intEventId   = @$_REQUEST['evid'];
$arrEventList = @$_REQUEST['evlist'];
$strAction    = @$_REQUEST['ac'];
$intResult    = @$_REQUEST['result'];

$strStatus    = @$_REQUEST['status'];
$strGrund     = @$_REQUEST['grund'];
$strSchritt   = @$_REQUEST['schritt'];
$strAp        = @$_REQUEST['ap'];
$strTelefon   = @$_REQUEST['telefon'];
$strWieder    = @$_REQUEST['wieder'];
$strInfo      = @$_REQUEST['info'];

$strList      = @$_REQUEST['list'];

$arrHeader = array(
  'user: ' .$intUser,
  'Accept: application/json'
);

if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
  $arrHeader[] = 'test: true';
}

$boolOutputDone = false;

/*
"Phase" wird auf "Publication" geändert
"Online Status" ändern auf „sichtbar“ (sofern noch nicht geschehen?!)
"Dokument auf Portal sichtbar" ändern auf „ Ja“ (sofern noch nicht geschehen?!)
*/

if ($strAction == 'proof') {

    if (isset($intEventId)) {
        $arrEventList = array($intEventId);
    }

    if ((int) $intResult == 1) {

        foreach ($arrEventList as $intKey => $intEventId) {

            $strSql = 'UPDATE `SF42_IZSEvent__c` SET `RecordTypeId` = "01230000001Ao75AAC" WHERE `Id` = "' .$intEventId .'"';
            $resSql = MySQLStatic::Update($strSql);
    
            $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Id` = "' .$intEventId .'"';
            $arrRow = MySQLStatic::Query($strSql);
    
            $strSql2 = 'INSERT INTO `izs_event_change` 
             (`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES (
             NULL, "' .$intEventId .'", ' .$intUser .', "' .$arrRow[0]['Name'] .'", "RecordTypeId", "01230000001Ao76AAC", "01230000001Ao75AAC", NOW(), 2)';
            $result2 = MySQLStatic::Query($strSql2);

        }

    }

    echo json_encode($arrEventList);
    $boolOutputDone = true;

}

if ($strAction == 'klaerung') {

    if ((int) $intResult == 0) {

        $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Id` = "' .$intEventId .'"';
        $arrRow = MySQLStatic::Query($strSql);

        $strWiederWrite = '0000-00-00';
        if ($strWieder != '') {
            $arrPart = explode('.', $strWieder);
            $strWiederWrite = $arrPart[2] .'-' .$arrPart[1] .'-' .$arrPart[0];
        }

        //print_r($arrPart);

        $arrChangeList = array(
            'RecordTypeId' => '01230000001Ao76AAC',
            'SF42_EventStatus__c' => 'enquired',
            'Status_Klaerung__c' => $strStatus,
            'Grund__c' => $strGrund,
            'Naechster_Meilenstein__c' => $strSchritt,
            'AP_Klaerung' => $strAp,
            'Tel_Klaerung' => $strTelefon,
            'bis_am__c' => $strWiederWrite,
            'Info__c' => $strInfo
        );

        //print_r($arrChangeList); die();

        foreach ($arrChangeList as  $strField => $strValue) {

            if ($arrRow[0][$strField] != $strValue) {
                $strSql = 'UPDATE `SF42_IZSEvent__c` SET `' .$strField .'` = "' .str_replace('"', '\\"', $strValue) .'" WHERE `Id` = "' .$intEventId .'"';
                $resSql = MySQLStatic::Update($strSql);
        
                $strSql2 = 'INSERT INTO `izs_event_change` ';
                $strSql2.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES ( ';
                $strSql2.= 'NULL, "' .$intEventId .'", ' .$intUser .', "' .$arrRow[0]['Name'] .'", "' .$strField .'", "' .str_replace('"', '\\"', $arrRow[0][$strField]) .'", "' .str_replace('"', '\\"', $strValue) .'", NOW(), 2)';
                $result2 = MySQLStatic::Query($strSql2);
            }

        }

    }

} 

if ($strAction == 'chk_klaerung') {

    //echo $strList;
    if ($strList != '') {
        $strList = substr($strList, 1);

        $strSqlList = '"' .str_replace('|', '", "', $strList) .'"';

        $strSql = 'SELECT `Name` FROM `SF42_IZSEvent__c` WHERE `Status_Klaerung__c` = "in Klärung" AND `Id` IN (' .$strSqlList .')';
        $arrSql = MySQLStatic::Query($strSql);

        if (count($arrSql) > 0) {

            $strReturn = '';
            foreach ($arrSql as $intKey => $arrDataset) {
                $strReturn.= $arrDataset['Name'] .', ';
            }
            $strReturn = substr($strReturn, 0, -2);

            echo $strReturn;
            exit;

        }

    }

}


if ($strAction == 'chk_status') {

    //echo $strList;
    if ($strList != '') {
        $strList = substr($strList, 1);

        $strSqlList = '"' .str_replace('|', '", "', $strList) .'"';

        $strSql = 'SELECT `Name` FROM `SF42_IZSEvent__c` WHERE `SF42_EventStatus__c` IN ("not OK", "OK", "zurückgestellt von IZS") AND `Id` IN (' .$strSqlList .')';
        $arrSql = MySQLStatic::Query($strSql);

        if (count($arrSql) > 0) {

            $strReturn = "HINWEIS\n\n";
            $strReturn.= "Nur Vorgänge mit Status = „angefragt“ werden auf „OK“ gesetzt.\n";
            $strReturn.= "Falls Du Vorgänge mit anderer Status angewählt hast, passiert nichts. Du kannst beruhigt weitermachen... :-)";

            echo $strReturn;
            exit;

        }

    }

}

if ($strAction == 'refresh') {

    $arrReturn = array();

    $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Id` = "' .$_REQUEST['evid'] .'"';
    $arrRow = MySQLStatic::Query($strSql);

    $strSql2 = 'SELECT `Id` FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$arrRow[0]['Betriebsnummer_ZA__c'] .'"';
    $arrRow2 = MySQLStatic::Query($strSql2);

    $strSql3 = 'SELECT `Art_Negativmerkmal__c` FROM `Negativmerkmal__c` WHERE `Auskunftsgeber__c` = "' .$arrRow[0]['SF42_informationProvider__c'] .'" AND `AccountId` = "' .$arrRow2[0]['Id'] .'"';
    $arrRow3 = MySQLStatic::Query($strSql3);

    $strNvm = '';
    if (count($arrRow3) > 0) {
        $strNvm = $arrRow3[0]['Art_Negativmerkmal__c'];
    }

    $arrReturn = array(
        'nvm' => $strNvm,
        'grund' => $arrRow[0]['Grund__c']
    );

    echo json_encode($arrReturn);
    exit;

}

if ($strAction == 'undo') {

    $arrReturn = array();

    $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Id` = "' .$intEventId .'"';
    $arrRow = MySQLStatic::Query($strSql);

    $arrChangeList = array(
        'SF42_EventStatus__c' => 'OK',
        'Status_Klaerung__c' => 'geschlossen / positiv',
        'Art_des_Dokuments__c' => 'Dynamische Erzeugung'
    );

    ///*
    foreach ($arrChangeList as  $strField => $strValue) {

        if ($arrRow[0][$strField] != $strValue) {
            $strSql = 'UPDATE `SF42_IZSEvent__c` SET `' .$strField .'` = "' .str_replace('"', '\\"', $strValue) .'" WHERE `Id` = "' .$intEventId .'"';
            $resSql = MySQLStatic::Update($strSql);
    
            $strSql2 = 'INSERT INTO `izs_event_change` ';
            $strSql2.= '(`ec_id`, `ev_id`, `cl_id`, `ev_name`, `ec_fild`, `ec_old`, `ec_new`, `ec_time`, `ec_type`) VALUES ( ';
            $strSql2.= 'NULL, "' .$intEventId .'", ' .$intUser .', "' .$arrRow[0]['Name'] .'", "' .$strField .'", "' .str_replace('"', '\\"', $arrRow[0][$strField]) .'", "' .str_replace('"', '\\"', $strValue) .'", NOW(), 2)';
            $result2 = MySQLStatic::Query($strSql2);
        }

    }
    //*/

    $arrReturn = array(
        'stundung' => $arrRow[0]['Stundung__c']
    );

    echo json_encode($arrReturn);
    exit;

}

if (!$boolOutputDone) {
    echo 1;
}


?>