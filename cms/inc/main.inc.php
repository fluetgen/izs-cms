<?php

session_start();
error_reporting(E_ALL);
ini_set("display_errors", 1); 

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}


$strCssHead = '';
$strJsHead  = '';
$strJsFootScript  = '';
$strJsFootCodeRun = '';

$strPageTitle = '';

$strTopnav = '';
$strSiteMenubar = '';
$strSiteGridmenu = '';

$strOutput = '';

//DESIGN SWITCH
$boolShowHead = true;
$boolShowSide = true;

//CONSTANTS
require_once('const.inc.php');

//FUNCTIONS
require_once('bootstrap.inc.php');
require_once('functions.inc.php');

//CLASSES
require_once('classes/class.contact.php');
require_once('classes/class.informationprovider.php');
require_once('classes/class.premiumpayer.php');
require_once('classes/class.group.php');
require_once('classes/class.anfragestelle.php');
require_once('classes/class.event.php');

require_once('classes/class.stream.php');
require_once('classes/class.multimail.php');

//LOGIN
require_once('login.inc.php');

$boolIsCron = false;

if (@$_REQUEST['cron'] != '') {
  if ($_REQUEST['hash'] == md5('Triple2013' .$_REQUEST['cron'])) {
    $boolIsCron = true;
  }
}

if (($boolLogin == false) && (!$boolIsCron)) {
  header('Location: /cms/index.php?ac=logout');
} else {
  $strSql = 'SELECT `cl_first`, `cl_last`, `cl_admin`, `cl_mail` FROM `cms_login` WHERE `cl_id` = "' .MySQLStatic::esc($_SESSION['id']) .'"';
  $arrResult = MySQLStatic::Query($strSql);
  if (count($arrResult) > 0) {
    $strUserFirst = $arrResult[0]['cl_first'];
    $strUserLast = $arrResult[0]['cl_last'];
    $intAdmin = $arrResult[0]['cl_admin'];
    $strName = $arrResult[0]['cl_first'];
    $strUserMail = $arrResult[0]['cl_mail'];
    if ($strName != '') {
      $strName.= ' ' .$arrResult[0]['cl_last'];
    } else {
      $strName = $arrResult[0]['cl_last'];
    }
  }
}

//CONTENT
//HANDLE ACTION
switch ($_REQUEST['ac']) {
  case 'contacts':
    require_once('inc/contacts.inc.php');
    break;
  case 'contact_details':

  $strCssHead.= '
  <style>
    .tleft > tbody > tr > td, .tleft > tbody > tr > th { border-bottom: 1px solid #e4eaec; }
    .tright > tbody > tr > td, .tleft > tbody > tr > th { border-bottom: 1px solid #e4eaec; }
  </style>' .chr(10);

    require_once('inc/contact_details.inc.php');
    break;
  case 'contact_klaerung':
    require_once('inc/contact_klaerung.inc.php');
    break;
  case 'vollmacht':
    require_once('inc/vollmacht.inc.php');
    break;
  case 'bd_aktualisieren':
    require_once('inc/bd_aktualisieren.inc.php');
    break;
  case 'bg_uebersicht':
    require_once('inc/bg_uebersicht.inc.php');
    break;
  case 'bg_detail':
    require_once('inc/bg_detail.inc.php');
    break;
  case 'bg_events':
    require_once('inc/bg_events.inc.php');
    break;
  case 'invoice':
    require_once('inc/invoice.inc.php');
    break;
  case 'kk_status':
    require_once('inc/kk_status.inc.php');
    break;
  case 'abgleich':
    require_once('inc/ba_abgleich.inc.php');
    break;
  case 'dezkk':
    require_once('inc/dk_setup.inc.php');
    break;
  default:
    require_once('inc/layout.blank.inc.php');
    break;
}

//LAYOUT
if ($boolShowHead) {
  require_once('inc/layout.topnav.inc.php');
} else {

  $strCssHead.= '  <style>body { padding-top: 0px; }</style>' .chr(10);
  
}
  
if ($boolShowSide) {
  require_once('inc/layout.sitemenubar.inc.php');
  require_once('inc/layout.sitegridmenu.inc.php');
} else {
  $strCssHead.= '
  <style>
    .site-menubar-fold .page, .site-menubar-fold .site-footer { margin-left: 0 !important; }
    .site-menubar-unfold .page, .site-menubar-unfold .site-footer { margin-left: 0 !important; }
    table.dataTable thead th, table.dataTable thead td { padding-left: 9px; }
  </style>' .chr(10);
}

$strCssHead.= '
<style>

  body, .nav-tabs > li > a, .table, .form-control  {
    color: #3f3f3f;
  }

  textarea.ta {
    width: 300px;
    height: 50px;
  }
  
  .page-header, .page-content {
      background-color: #f1f4f5 !important;
  }
  
  select option:disabled {
    color: #a3afb7;
    font-style: italic;
  }

  .page-aside-title {
    display: inline-block;
  }

  #resetFilter {
    position: relative;
    top: -24px;
    left: -20px;
    font-size: 10px;
    text-decoration: underline;
    color: #62a8ea;
  }
  
  .red, .red a {
    color: red;
  }

  .layout-boxed, .layout-boxed .site-navbar {
    margin-left: 0px !important;
    margin-right: auto;
    //min-width: 1920px !important;
    //min-width: 2700px !important;
    max-width: 5000px !important;
  }
  .page-content { padding: 20px; }
  .page-header { padding: 20px; }
  table { font-size: 11px; }
  
  .dataTable > tbody > tr.warning > td, .dataTable > tbody > tr.warning > th, .dataTable > tbody > tr > td.warning, .dataTable > tbody > tr > th.warning, .dataTable > tfoot > tr.warning > td, .dataTable > tfoot > tr.warning > th, .dataTable > tfoot > tr > td.warning, .dataTable > tfoot > tr > th.warning, .dataTable > thead > tr.warning > td, .dataTable > thead > tr.warning > th, .dataTable > thead > tr > td.warning, .dataTable > thead > tr > th.warning {
      background-color: #f2a654!important;
  }
  .dataTable-hover > tbody > tr.warning:hover > td, .dataTable-hover > tbody > tr.warning:hover > th, .dataTable-hover > tbody > tr:hover > .warning, .dataTable-hover > tbody > tr > td.warning:hover, .dataTable-hover > tbody > tr > th.warning:hover {
      background-color: #f09a3c!important;
  }

  .dataTable > tbody > tr.danger > td, .dataTable > tbody > tr.danger > th, .dataTable > tbody > tr > td.danger, .dataTable > tbody > tr > th.danger, .dataTable > tfoot > tr.danger > td, .dataTable > tfoot > tr.danger > th, .dataTable > tfoot > tr > td.danger, .dataTable > tfoot > tr > th.danger, .dataTable > thead > tr.danger > td, .dataTable > thead > tr.danger > th, .dataTable > thead > tr > td.danger, .dataTable > thead > tr > th.danger {
      background-color: #f96868!important;
  }
  .dataTable-hover > tbody > tr.danger:hover > td, .dataTable-hover > tbody > tr.danger:hover > th, .dataTable-hover > tbody > tr:hover > .danger, .dataTable-hover > tbody > tr > td.danger:hover, .dataTable-hover > tbody > tr > th.danger:hover {
      background-color: #f96868!important;
  }

.modal-dialog {
    margin: 30px auto;
    width: 900px;
}

.stream {
  height: 400px; 
  overflow-y: auto; 
  position: relative; 
  padding: 10px; 
  border: 1px solid #e4eaec;
}

.streamentry {
  padding: 10px 0; 
  border-bottom: 2px solid #808080;
}

.streamhead, .streamuser {
  font-size: 12px;
}

.modal-dialog.smallw {
  width: 600px;
}

.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    width: 270px;
}

.cVollmacht, .cAnfrage, cVorlage {
    cursor: pointer;
}

.dp {
  width: 110px !important;
}

table {
    font-size: 14px;
}

.modal-header.bg-blue-grey-600 h4, .modal-header.bg-blue-grey-600 span {
  color: #ffffff;
}

.modal-datail {
  padding: 20px;
}

.modal-detail table {
  width: 100%;
}

.modal-footer {
  text-align: left;
}

.right {
  float: right;
}

.bsp {
  padding-bottom: 10px;
}

.tsp {
  padding-top: 10px;
}

#open_btn {
  background: lightgray none repeat scroll 0 0;
  border: 3px dotted gray;
  display: block;
  height: 70px;
  width: 630px;
  color: gray;
  text-align: center;
  vertical-align: middle;
  font-size: 18px;
  padding-top: 18px;
}

.page, .panel {
  display: inline-block;
  min-width: 1200px;
}

.panel {
  min-width: 100%;
}


</style>' .chr(10);

?>