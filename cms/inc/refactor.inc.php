<?php

$strCrmUrl = 'https://crm.izs-institut.de/';

function curl_get ($strUrl, array $arrGet = NULL, array $arrOptions = array(), $arrHeader = array()) { 

    $arrDefault = array( 
        CURLOPT_URL => $strUrl. (strpos($strUrl, '?') === FALSE ? '?' : ''). http_build_query($arrGet), 
        CURLOPT_HEADER => 0, 
        CURLOPT_RETURNTRANSFER => true, 
        CURLOPT_FOLLOWLOCATION => true
        
    ); //CURLOPT_TIMEOUT => 4

    $resCurl = curl_init();
  
    if (count($arrHeader) > 0) {
        curl_setopt ($resCurl, CURLOPT_HTTPHEADER, $arrHeader); 
    }
    
    curl_setopt_array($resCurl, ($arrOptions + $arrDefault)); 
    if(!$mxdResult = curl_exec($resCurl)) { 
        //trigger_error(curl_error($resCurl)); 
    } 
    curl_close($resCurl);
  
    return $mxdResult; 
} 

function curl_post ($strUrl, array $arrPost = NULL, array $arrOptions = array(), $arrHeader = array()) { 

    $arrDefault = array( 
        CURLOPT_POST => TRUE,
        CURLOPT_POSTFIELDS => json_encode($arrPost),
        CURLOPT_RETURNTRANSFER => TRUE, 
        CURLOPT_URL => $strUrl,
        CURLOPT_HEADER => TRUE,
        CURLOPT_SSL_VERIFYPEER => TRUE
    ); 
  
    $resCurl = curl_init(); 
  
    if (count($arrHeader) > 0) {
        curl_setopt ($resCurl, CURLOPT_HTTPHEADER, $arrHeader); 
    }
    
    curl_setopt_array($resCurl, ($arrOptions + $arrDefault)); 
    if(!$mxdResult = curl_exec($resCurl)) { 
        trigger_error(curl_error($resCurl)); 
    } 
    $arrInfo = curl_getinfo($resCurl);
    curl_close($resCurl);
  
    $strHeader = trim(substr($mxdResult, 0, $arrInfo['header_size']));
    $strBody = substr($mxdResult, $arrInfo['header_size']);
  
    return $strBody; 
} 

?>