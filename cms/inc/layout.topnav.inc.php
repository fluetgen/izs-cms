<?php

$strTopnav = '
<style>
#header {
  height: 100px;
  background: url("/cms/img/bgrd_header_about.png") repeat-x scroll left top rgba(0, 0, 0, 0);
}

#logo {
  padding-left: 30px;
  padding-top: 15px;
  float: left;
}

#search {
  float: right;
  margin: 40px 40px 0 0;
}

body {
  padding-top: 38px !important;
}

.layout-boxed {
  padding-top: 0px !important;
}

.page-header-actions {
  top: 150px !important;
}

</style>

<header id="header">
  <div id="logo">
    <a href="index.php" title="Startseite des IZS-Institut für Zahlungssicherheit"><img src="/cms/img/izs-institut-zahlungssicherheit.png" alt="Logo IZS-Institut für Zahlungssicherheit" title="Server: ' .$_SERVER['SERVER_ADDR'] .'"></a>
  </div>

  <div id="search">
    <form action="/cms/index.php?ac=sear" method="post">
      <input type="hidden" name="send" value="1">
      <select name="strSearchType">
        <option value="event">Event</option>
      </select> <input type="text" name="strSearchValue" value="' .@$_REQUEST['strSearchValue'] .'">
    </form>
  </div>

</header> <!-- end header -->
';

?>