<?php

//REQUEST

$strBnr = @$_REQUEST['bnr'];

if ($strBnr != '') {

  // IP
  $strSql = 'SELECT * FROM `izs_informationprovider` WHERE `Btnr` = "' .$strBnr .'"';
  $arrIp  = MySQLStatic::Query($strSql);

  if (count($arrIp) > 0) {
    $_REQUEST['acid'] = $arrIp[0]['Id'];
  } else {

    // PP
    $strSql = 'SELECT * FROM `izs_premiumpayer` WHERE `Btnr` = "' .$strBnr .'"';
    $arrPp  = MySQLStatic::Query($strSql);

    if (count($arrPp) > 0) {
      $_REQUEST['grid'] = $arrIp[0]['Group'];
    } else {

      // PP GROUP
      $strSql = 'SELECT * FROM `izs_premiumpayer_group` WHERE `Btnr` = "' .$strBnr .'"';
      $arrPp  = MySQLStatic::Query($strSql);

      if (count($arrPp) > 0) {
        $_REQUEST['grid'] = $arrIp[0]['Id'];
      } else {

        $strSql = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$strBnr .'"';
        $arrAccount = MySQLStatic::Query($strSql);

        if (count($arrAccount) > 0) {
          $_REQUEST['acid'] = $arrAccount[0]['Id'];
        }

      }

    }

  }

}

$strId   = @$_REQUEST['acid'];
$strGrId = @$_REQUEST['grid'];

$strName = '';
$strDataNamePart = '';
$strForeignId = '';

//DATA
if ($strGrId != '') {

  $strType = 'PP';

  $objGroup = new Group;
  $arrGroup = $objGroup->arrGetGroup($strGrId);
  
  if (count($arrGroup) > 0) {
    $strName = $arrGroup[0]['gr_name'];
  }

  $strDataNamePart = 'Zeitarbeitsunternehmen/';

  $strUri = '' .$strIChatUri .'cms/index_neu.php?ac=contacts&grid=' .$_REQUEST['grid'];

  $strForeignId = $strGrId;

} else {

  $strType = 'IP';

  $objPP = new PremiumPayer;
  $arrPP = $objPP->arrGetPP($strId, false);
  
  $objContact = new Contact;
  $arrContact = $objContact->arrGetContactListFromIP($strId);
  
  $objAnfragestelle = new Anfragestelle;

  $strName = $arrPP[0]['ac_name'];

  if ($arrPP[0]['RecordTypeId'] == '01230000001Ao72AAC') {
    $strDataNamePart = 'Krankenkassen/';
  } elseif ($arrPP[0]['RecordTypeId'] == '01230000001DKGtAAO') {
    $strDataNamePart = 'Berufsgenossenschaften/';
  } elseif ($arrPP[0]['RecordTypeId'] == '01230000001BvebAAC') {
    $strDataNamePart = 'Entleiher/';
  }

  $strUri = '' .$strIChatUri .'cms/index_neu.php?ac=contacts&acid=' .$_REQUEST['acid'];

  $strForeignId = $_REQUEST['acid'];

}

$strContext = '';
$strSql = 'SELECT * FROM `ks_link` WHERE `kl_type` = "smartgroup" AND `kl_obj_id` = "' .MySQLStatic::esc($strForeignId) .'"';
$arrResult = MySQLStatic::Query($strSql);

if (count($arrResult) > 0) {
  $strContext = $arrResult[0]['kl_foreign_id'];
}

//SETTINGS

$boolShowHead = false;
$boolShowSide = false;

$strPageTitle = $strName;

//CONTENT

$strCssHead.= '
<style>
.nav-tabs > li > a {
  -moz-border-bottom-colors: none;
  -moz-border-left-colors: none;
  -moz-border-right-colors: none;
  -moz-border-top-colors: none;
  background-color: #f3f7f9;
  border-color: #f3f7f9 #f3f7f9 transparent;
  border-image: none;
  border-style: solid;
  border-width: 1px;
  color: #37474f;
}

.pane-body {
  overflow-x: hidden;
  overflow-y: auto;
}

.border-0 {
  border: 0!important;
}

iframe {
  width: 100%;
  height: 1000px;
}

</style>
';

$strOutput = '
  <!-- Page -->
  <div class="animsition container-fluid">
';

//$strOutput.= strCreatePageTitle('Kontakte', $arrIP[0]['ac_name']);

$arrTableHead = array(
  'Anrede', 
  'Name', 
  'Vorname', 
  'Telefon', 
  'Email',
  'Anfragestelle',
  //'Aktiv',
  'Info'
);

$arrContactList = array();

if ($strType == 'IP') {
  foreach ($arrContact as $intRow => $arrRow) {

    $strAnfragestelle = '';
    if (count( $objAnfragestelle->arrGetAnfragestelle($arrRow['co_anfrage'])) > 0) {
      $strAnfragestelle = $objAnfragestelle->arrGetAnfragestelle($arrRow['co_anfrage'])[0]['as_name'];
    }
  
    $arrContactList[] = array(
      'Anrede'  => $arrRow['co_salut'],
      'Name'    => '<span class="hidden">' .$arrRow['co_last'] .'</span><a href="index_neu.php?ac=contact_details&coid=' .$arrRow['coid'] .'" target="_blank">' .$arrRow['co_last'] .'</a>',
      'Vorname' => $arrRow['co_first'],
      'Telefon' => $arrRow['co_phone'],
      'Email'   => $arrRow['co_mail'],
      'Anfragestelle' => $strAnfragestelle,
      //'Aktiv' => '',
      'Info' => $arrRow['co_info']
    );
  }
} else {
  ;
}


$strDataName = $strIChatPath .'/' .$strDataNamePart .$strName;


$strContactTable = strCreateTable($arrContactList, $arrTableHead);

$strOutput.= '
 <!-- Page -->
  <div class="animsition container-fluid">

  <div class="page-header">
    <div class="page-title">
      <h1>' .$strPageTitle  .'</h1>
    </div>
  </div>
    <div class="page-content">


              <!-- Example Tabs -->
              <div class="example-wrap">
                <div class="nav-tabs-horizontal">
                  <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                    <li role="presentation"><a data-toggle="tab" href="#exampleTabsOne" aria-controls="exampleTabsOne"
                      role="tab">Kontakte</a></li>
                    <li class="active" role="presentation"><a data-toggle="tab" href="#exampleTabsTwo" aria-controls="exampleTabsTwo"
                      role="tab"><span>Chats</span><span class="badge up badge-danger" kng smartchat data-type="unread" data-uri="' .$strUri .'"></span></a></li>
                  </ul>

                    
<div class="panel">
                      
<div class="panel-body">
<div class="row">
<div class="col-lg-12 padding-bottom-20">

                  <div class="tab-content">
                    <div class="tab-pane" id="exampleTabsOne" role="tabpanel">
';

$strOutput.= $strContactTable;

$strOutput.= '
                    </div>
                    <div class="tab-pane active" id="exampleTabsTwo" role="tabpanel">
                      <!-- <div kng smartchat data-type="topics" data-name="' .$strDataName .'"></div> -->
';

if ($strContext != '') {
  $strOutput.= '                      <div><iframe src="' .substr($strIChatUrl, 0, -4) .'/context/' .$strContext .'/topics" class="border-0 pane-body" params="[object Object]" style="margin-top: 0px; margin-left: 0px;"></iframe></div>';
} else {
  $strOutput.= '                      Keine Smartgroup verlinkt! Bitte wähle eine Smartgroup im crm aus.';
}

$strOutput.= '
                    </div>
                  </div>

</div>
</div>
</div>

</div>



                </div>
              </div>
              <!-- End Example Tabs -->
              
    </div>


  </div>
  <!-- End Page -->
  ';

?>