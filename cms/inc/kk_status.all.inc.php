<?php

$arrFilter = array();

$strStatus = '';
if (!isset($_REQUEST['filter_sb'])) {
  $strStatus = '2';
} elseif ($_REQUEST['filter_sb'] != '') {
  $arrPart = explode('_', $_REQUEST['filter_sb']);
  $strStatus = $arrPart[1];
}

$strRueck = '';
if (!isset($_REQUEST['filter_ru'])) {
  $strRueck = '0';
} elseif ($_REQUEST['filter_ru'] != '') {
  $arrPart = explode('_', $_REQUEST['filter_ru']);
  $strRueck = $arrPart[1];
}

$strStartStatus = '';
if ($strStatus != '') {
  $strStartStatus = 'sb_' .$strStatus;
}

$strStartRueck = '';
if ($strRueck != '') {
  $strStartRueck = 'ru_' .$strRueck;
}

$objEvent     = new Event;

$arrFilter[] = array(
    'strKey' => 'SF42_Month__c',
    'strType' => '=',
    'strValue' => $intMonth
);
$arrFilter[] = array(
    'strKey' => 'SF42_Year__c',
    'strType' => '=',
    'strValue' => $intYear
);

$arrEventListAll = $objEvent->arrGetEventList($arrFilter);

$arrEventStatus = array(
  'OK', 
  'not OK', 
  'enquired', 
  'zurückgestellt von IZS'
);

if ((isset($_REQUEST['filter_sb']) && ($_REQUEST['filter_sb'] != '')) || (!isset($_REQUEST['filter_sb']))) {
  $arrFilter[] = array(
    'strKey' => 'SF42_EventStatus__c',
    'strType' => '=',
    'strValue' => $arrEventStatus[$strStatus]
  );
}

if (isset($_REQUEST['filter_ip']) && ($_REQUEST['filter_ip'] != '')) {
  $arrPart = explode('_', $_REQUEST['filter_ip']);
  $strIpId = $arrPart[1];
  $arrFilter[] = array(
    'strKey' => 'SF42_informationProvider__c',
    'strType' => '=',
    'strValue' => $strIpId
  );  
}

$arrEventList = $objEvent->arrGetEventList($arrFilter);


$arrTableHead = array(
    'Status',
    'Monat',
    'Event-ID',
    'Information Provider',
    'Company Group',
    'Btnr PP',
    'Premium Payer',
    'Rückmeldung am',
    'Art der Rückmeldung',
    'Klärung',
    "Status Klärung",
    'Wiedervorlage',
    'Dokument'
);

$arrEventStatusShow = array(
  'OK', 
  'NOT OK', 
  'angefragt', 
  'zurückgestellt von IZS'
);

$arrRuecklauf = array(
  'Rücklauf vollständig', 
  'Rücklauf anteilig', 
  'kein Rücklauf'
);

for ($intCount = 0; $intCount < 6; $intCount++) {
  $intTime = mktime(0, 0, 0, date('n') - $intCount, 1, date('Y'));
  $arrFilterSection['y'][date('Y', $intTime) .'_' .date('n', $intTime)] = date('Y', $intTime) .'-' .date('n', $intTime);
}

foreach ($arrEventStatusShow as $strKey => $strValue) {
  $arrFilterSection['sb'][$strKey] = $strValue;
}

foreach ($arrRuecklauf as $strKey => $strValue) {
  $arrFilterSection['ru'][$strKey] = $strValue;
}

foreach ($arrIpList as $strKey => $strValue) {
  $arrFilterSection['ip'][$strKey] = $strValue;
}

$intToday = mktime(0, 0, 0, date('n'), date('j'), date('Y'));
$arrAmpel = array();
foreach ($arrEventListAll as $intKey => $arrEvent) {

  if ((strtotime($arrEvent['ev_rueck_am']) <= $intToday) && ($arrEvent['ev_rueck_am'] != '0000-00-00')) {
    $strAmpel = 'gruen';
  } else {
    $strAmpel = 'rot';
  }

  if (empty($arrAmpel[$arrEvent['ev_ip_acid']])) {
    $arrAmpel[$arrEvent['ev_ip_acid']] = array();
  }
  if (empty($arrAmpel[$arrEvent['ev_ip_acid']][$strAmpel])) {
    $arrAmpel[$arrEvent['ev_ip_acid']][$strAmpel] = 0;
  }
  $arrAmpel[$arrEvent['ev_ip_acid']][$strAmpel]++;
}

foreach ($arrAmpel as $strKkId => $arrKkStatus) {
  if (count($arrKkStatus) == 2) {
    $arrAmpel[$strKkId] = 'Rücklauf anteilig';
  } else {
    if (isset($arrKkStatus['gruen'])) {
      $arrAmpel[$strKkId] = 'Rücklauf vollständig';
    } else {
      $arrAmpel[$strKkId] = 'kein Rücklauf';
    }
  }
}

//print_r($arrAmpel);

$arrList = array();
foreach ($arrEventList as $intRow => $arrEvent) {
  
  $strClassAdd = '';

  //print_r($arrEvent); die();

  if (($strRueck == '') || (($arrRuecklauf[$strRueck] == $arrAmpel[$arrEvent['ev_ip_acid']]))) {

    $strMeldestelle = @$arrPpBtnrList[$arrEvent['ev_ac_betriebsnummer']];
    
    $strClassAdd.= ' y_' .$arrEvent['ev_year'] .'_' .$arrEvent['ev_month'];
    
    $intSb = array_search($arrEvent['ev_event_status'], $arrEventStatus);
    $strClassAdd.= ' sb_' .$intSb;

    $intRu = array_search($arrAmpel[$arrEvent['ev_ip_acid']], $arrRuecklauf);
    $strClassAdd.= ' ru_' .$intRu;

    $strClassAdd.= ' ip_' .$arrEvent['ev_ip_acid'];

    $strRueckAm = '';
    if ($arrEvent['ev_rueck_am'] != '0000-00-00') {
      $strRueckAm = date('d.m.Y', strtotime($arrEvent['ev_rueck_am']));
    }

    $strWieder = '';
    if ($arrEvent['ev_bis'] != '0000-00-00') {
      $strWieder = date('d.m.Y', strtotime($arrEvent['ev_bis']));
    }

    $strDoc = '';
    if ($arrEvent['ev_doc'] != '') {
      $strDoc = '<a href="' .$arrEvent['ev_doc'] .'" target="_blank"><img src="img/icon_pdf.png" width="14" height="16"></a>';
    }

    $strInformationProvider = '';
    if (isset($arrDezAnfrage[$arrEvent['ev_ip_acid']])) {
      if (isset($arrDezAnfrage[$arrEvent['ev_ip_acid']][$arrPpIdList[$arrEvent['ev_ac_betriebsnummer']]])) {
        $strInformationProvider = '<a href="index_neu.php?ac=contacts&acid=' .$arrEvent['ev_ip_acid'] .'" target="_blank">' .$arrDezAnfrage[$arrEvent['ev_ip_acid']][$arrPpIdList[$arrEvent['ev_ac_betriebsnummer']]] .'</a>';
      } else {
        $strInformationProvider = '<a href="index_neu.php?ac=contacts&acid=' .$arrEvent['ev_ip_acid'] .'" target="_blank">' .$arrCatchAllList[$arrEvent['ev_ip_acid']] .'</a>';
      }
    } else {
      $strInformationProvider = '<a href="index_neu.php?ac=contacts&acid=' .$arrEvent['ev_ip_acid'] .'" target="_blank">' .$arrIpList[$arrEvent['ev_ip_acid']] .'</a>';
    }
    
    $arrList[] = array(
      'Status' => '' .$arrEventStatusShow[$intSb] .'</a><span class="hidden hfilter' .$strClassAdd .'"></span>',
      'Monat' => $arrEvent['ev_year'] .'-' .$arrEvent['ev_month'],
      //'ID' => '<a href="/cms/index_neu.php?ac=kk_detail&evid=' .$arrEvent['evid'] .'" target="_blank">' .$arrEvent['ev_name'] .'</a><span class="hidden hfilter' .$strClassAdd .'"></span>',
      'ID' => '<a href="/cms/index.php?ac=sear&det=' .$arrEvent['evid'] .'" target="_blank" style="white-space:nowrap;">' .$arrEvent['ev_name'] .'</span><span class="hidden hfilter' .$strClassAdd .'"></span>',
      'Information Provider' => $strInformationProvider,
      'Company Group' => '<a href="index_neu.php?ac=contacts&grid=' .$arrEvent['ev_grid'] .'" target="_blank">' .$arrPpGrpList[$arrEvent['ev_grid']] .'</a>',
      'Btnr PP' => $arrEvent['ev_ac_betriebsnummer'],
      'Premium Payer' => $strMeldestelle,
      'Rückmeldung am' => $strRueckAm,
      'Art der Rückmeldung' => $arrEvent['ev_rueckmeldung_art'],
      'Klärung' => '<span class="hidden">' .$arrEvent['ev_grund'] .'</span>' .$arrEvent['ev_grund'],
      "Status Klärung" => '<span class="hidden">' .$arrEvent['ev_status'] .'</span>' .$arrEvent['ev_status'],
      'Wiedervorlage' => $strWieder,
      'Dokument' => $strDoc
    );

    //if ($intRow == 99) break;

  }

}

$arrOpt = array(
  'boolHasContext' => false
);

$strBgTable = strCreateTable($arrList, $arrTableHead, '', false, true, array(), $arrOpt);

$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Events gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strBgTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->



  <form method="post" action="_get_csv.php" id="export">
    <input type="hidden" name="strTable" id="strTable" value="">
  </form>
  
  
  
';


?>