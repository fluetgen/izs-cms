<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

session_start();

require_once('../const.inc.php');

require_once('../../assets/classes/class.mysql.php');
require_once('../../assets/classes/class.database.php');

require_once('refactor.inc.php');

$intUser = $_SESSION['id'];

$arrHeader = array(
  'user: ' .$intUser,
  'Accept: application/json'
);

if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
  $arrHeader[] = 'test: true';
}


if (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'createBill') ) {

    $arrHeader[] = 'apikey: 9b0d3b1c-27f2-4e40-8e4d-d735a50dbbee';

    $arrTemp = explode('_', $_REQUEST['id']);

    $intId   = $arrTemp[0];
    $intType = $_REQUEST['type'];

    $intMonth = $_REQUEST['month'];
    $intYear  = $_REQUEST['year'];

    $arrBill = array(
        'customer_id' => $intId,
        'type' => $intType,
    );

    $strSql  = 'SELECT `ie_pp_id` FROM `izs_easy_premiumpayer` WHERE `ie_easy_id` = "' .$intId .'"';
    $arrSql  = MySQLStatic::Query($strSql);
    $strPpId = $arrSql[0]['ie_pp_id'];

    $strSql  = 'SELECT * FROM `izs_vertrag` WHERE `Id` = "' .$strPpId .'" AND `Aktiv__c` = "true"';
    $arrSql  = MySQLStatic::Query($strSql);
    $strContractId = $arrSql[0]['Id'];

    if ($intUser == 3) {
        //echo $strSql; 
        //print_r($arrSql);
        //die();
    }

    $strSql  = 'SELECT * FROM `Vertrag__c` WHERE `Id` = "' .$arrSql[0]['Vertrag'] .'"';
    $arrContract  = MySQLStatic::Query($strSql);

    if ($intType == 3) { // Einmalige Einrichtungsgebühr

        $arrBill['month'] = $intMonth;
        $arrBill['year']  = $intYear;

        //Item
        $arrBill['ammount']['setup'] = $arrContract[0]['Einrichtungsgebuehr__c'];
        //Text
        $arrBill['firstDue'] = strtotime($arrContract[0]['Vertragsbeginn__c']);
        $arrBill['iban'] = $arrContract[0]['IBAN__c'];
        //Prexix
        $arrBill['contractBegin'] = strtotime($arrContract[0]['Auftrag_vom__c']);
    }

    if ($intType == 4) { // Dauerrechnung
        //Item
        $arrBill['svDatasets'] = $arrContract[0]['Anzahl_Datens_tze_Quartal__c'];
        $arrBill['ammount']['processing'] = $arrContract[0]['Summe_Leistungen_DV_pro_Monat__c'];
        $arrBill['firstDue'] = strtotime($arrContract[0]['Vertragsbeginn__c']);

        $arrBill['month'] = $intMonth;
        $arrBill['year']  = $intYear;

        //Text
        $arrBill['dueDates'] = $arrContract[0]['Abzurechnen_in_Monat__c'];
        $arrBill['iban'] = $arrContract[0]['IBAN__c'];
        //Prexix
        $arrBill['contractBegin'] = strtotime($arrContract[0]['Auftrag_vom__c']);
    }

    if ($intType == 5) { // Nachberechnung SV

        $arrBill['month'] = $intMonth;
        $arrBill['year']  = $intYear;

        $strDetail = substr($_REQUEST['detail'], 1);
        $arrDetail = array();

        $arrPart = explode('_', $strDetail);

        if (count($arrPart) > 0) {
            for ($intCount = 0; $intCount < count($arrPart); $intCount++) {
                $arrDetail[$arrPart[$intCount]] = floor($arrPart[$intCount + 1]);
                $intCount++;
            }
    
        }

        $intDiff = $_REQUEST['diff'];
        $intReal = $_REQUEST['real'];

        //Item
        $arrBill['svOver'] = $intDiff;
        $arrBill['ammount']['svOver'] = $arrContract[0]['Preis_pro_Datensatz_ber_Inklusivvolumen__c'];
        //Text
        ;
        //Prexix
        $arrBill['contractBegin'] = strtotime($arrContract[0]['Auftrag_vom__c']);
        $arrBill['recalcPeriod'] = $_REQUEST['month'] .';' .$_REQUEST['year'] .';' .$_REQUEST['mcount'];
        $arrBill['recalcDelivery'] = $intReal;
        $arrBill['recalcContract'] = $intReal - $intDiff;
        $arrBill['recalcDiff'] = $intDiff;
        $arrBill['recalcDetail'] = $arrDetail;
        $arrBill['firstDue'] = '02.' .$arrBill['month'] .'.' .$arrBill['year'];

        //print_r($arrBill); die();

    }

    if ($intType == 8) { // Nachberechnung Klärungsfälle

        $arrMonthName = array(
            1 => 'Januar',
            2 => 'Februar',
            3 => 'März',
            4 => 'April',
            5 => 'Mai',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'August',
            9 => 'September',
           10 => 'Oktober',
           11 => 'November',
           12 => 'Dezember'
       );

        $arrBill['month'] = $intMonth;
        $arrBill['year']  = $intYear;

        $strDetail = substr($_REQUEST['detail'], 1);
        $arrDetail = array();

        $arrPart = explode('_', $strDetail);

        if (count($arrPart) > 0) {
            for ($intCount = 0; $intCount < count($arrPart); $intCount++) {
                $arrDetail[$arrPart[$intCount]] = floor($arrPart[$intCount + 1]);
                $intCount++;
            }
    
        }

        //print_r($arrDetail);

        //Item
        $arrBill['clearing'] = $_REQUEST['diff'];
        $arrBill['ammount']['clearing'] = $arrContract[0]['Kosten_Kl_rungsfall__c'];
        //Text
        ;
        //Prexix
        $arrBill['contractBegin'] = strtotime($arrContract[0]['Auftrag_vom__c']);
        $arrBill['recalcClearing'] = $arrContract[0]['Anzahl_inkl_Kl_rungsf_lle__c'];

        $intPerMonth = 

        $intCount = 3;
        $intYearDisplay = $intYear;
        foreach ($arrDetail as $intMonth => $intTotal) {

            $intDiff = $intTotal - $arrBill['recalcClearing'];

            if (($intCount != 3) && ($intMonth == 12)) $intYearDisplay--;
            $arrBill['recalcDetail']['month0' .$intCount] = $arrMonthName[$intMonth] .' ' .$intYearDisplay;
            $arrBill['recalcDetail']['clear0' .$intCount] = $intTotal;
            $arrBill['recalcDetail']['diff0' .$intCount]  = ($intDiff > 0) ? $intDiff : 0;

            $intCount --;
        }

        $arrBill['recalcPeriod'] = $_REQUEST['month'] .';' .$_REQUEST['year'] .';' .count($arrDetail);

        //$arrBill['recalcDetail'] = $arrDetail;

    }

    $arrBill['clearingLogic'] = $arrContract[0]['Logik_Klaerung__c'];

    $arrPost = array(
        'arrBill' => $arrBill
    );

    if ($intUser == 3) {
        //print_r($arrPost); die();
    }

    $arrRes = curl_post('http://api.izs-institut.de/api/easy/documents/', $arrPost, array(), $arrHeader);
    $arrCreate = json_decode(json_decode($arrRes, true), true);


    if ($intUser == 3) {
        //print_r($arrRes);
    }

    /*
    print_r($arrPost);
    print_r($arrRes);
    print_r($arrCreate);
    */

    //print_r($arrPost); die();

    //print_r($arrRes); die();
    
    echo 1; 
    exit;

}


if (isset($_REQUEST['ac']) && (($_REQUEST['ac'] == 'test') || ($_REQUEST['ac'] == 'send'))) {

    $arrHeader[] = 'apikey: 9b0d3b1c-27f2-4e40-8e4d-d735a50dbbee';

    $arrGet = array(
      'format' => 'xlsx',
      'templ'  => $_REQUEST['templ'],
      'list'   => $_REQUEST['list']
    );

    if ($_REQUEST['templ'] != 'AOKNDS') {

        $arrRes = curl_get('http://api.izs-institut.de/api/sv/pp/', $arrGet, array(), $arrHeader);
        $arrEventList = json_decode($arrRes, true);
    
        $strName = 'Neue_Meldestellen_' .$_REQUEST['templ'] .'_' .date('Ym');
        
        $strFileName = CMS_PATH .'files/inform/' .$strName .'.xlsx';
        
        if (file_exists($strFileName)) {
            unlink($strFileName);
        }
        
        file_put_contents($strFileName, $arrRes);

        $strSql = 'SELECT * FROM `cms_text` WHERE `ct_type` = 4 AND `ct_ip` = "' .$_REQUEST['ct_ip'] .'"';
        $arrMailTemplate = MySQLStatic::Query($strSql);

        $strSql = 'SELECT * FROM `Account` WHERE `Id` = "' .$_REQUEST['ct_ip'] .'"';
        $arrAccount = MySQLStatic::Query($strSql);

        $strSql = 'SELECT * FROM `cms_login` WHERE `cl_id` = "' .$intUser .'"';
        $arrUser = MySQLStatic::Query($strSql);

        $strFrom = 'info@izs-institut.de';

        if ($_REQUEST['ac'] == 'test') {
            $to = $arrUser[0]['cl_mail'];
            $bcc = '';
        } else {
            $to = $arrAccount[0]['Neue_Meldestelle_Email__c'];
            $bcc = 'info@izs-institut.de';
            //$to = $arrUser[0]['cl_mail'];
        }

        $cc = '';

        $files = array();
        $files[] = array($strFileName, $strName .'.xlsx'); 

        $subject = $arrMailTemplate[0]['ct_head'];
        $message = str_replace('</p>', "\n", $arrMailTemplate[0]['ct_text']);
        $message_html = $arrMailTemplate[0]['ct_text'];

        multi_attach_mail($to, $cc, $bcc, $files, $subject, $message, $message_html);

        if ($_REQUEST['ac'] != 'test') {
            foreach ($_REQUEST['list'] as $intKey => $strPPId) {
                $strSql = 'INSERT INTO  `izs_inform_ip` (`ii_id`, `ii_ip`, `ii_pp`, `ii_date`, `ii_cl_id`) ';
                $strSql.= 'VALUES (NULL, "' .$_REQUEST['ct_ip'] .'", "' .$strPPId .'", NOW(), "' .$intUser .'");';
                $intInsert = MySQLStatic::Insert($strSql);
            }
        }

    } else {

        $strSql = 'SELECT * FROM `cms_text` WHERE `ct_type` = 4 AND `ct_ip` = "' .$_REQUEST['ct_ip'] .'"';
        $arrMailTemplate = MySQLStatic::Query($strSql);

        $strSql = 'SELECT * FROM `Account` WHERE `Id` = "' .$_REQUEST['ct_ip'] .'"';
        $arrAccount = MySQLStatic::Query($strSql);

        $strSql = 'SELECT * FROM `cms_login` WHERE `cl_id` = "' .$intUser .'"';
        $arrUser = MySQLStatic::Query($strSql);

        $strFrom = 'info@izs-institut.de';

        if ($_REQUEST['ac'] == 'test') {
            $to = $arrUser[0]['cl_mail'];
        } else {
            $to = $arrAccount[0]['Neue_Meldestelle_Email__c'];
            //$to = $arrUser[0]['cl_mail'];
        }

        $cc = '';
        $bcc = 'info@izs-institut.de';

        $files = array();
        
        foreach ($_REQUEST['list'] as $intKey => $strPPId) {

            $strSql = 'SELECT * FROM `Account` WHERE `Id` = "' .$strPPId .'"';
            $arrAccount = MySQLStatic::Query($strSql);

            $subject = str_replace('{BNR}', $arrAccount[0]['SF42_Comany_ID__c'], $arrMailTemplate[0]['ct_head']);
            $strText = str_replace('{BNR}', $arrAccount[0]['SF42_Comany_ID__c'], $arrMailTemplate[0]['ct_text']);
            $strText = str_replace('{ACCOUNTNAME}', $arrAccount[0]['Name'], $strText);

            $message = str_replace('</p>', "\n", $strText);
            $message_html = $strText;
    
            multi_attach_mail($to, $cc, $bcc, $files, $subject, $message, $message_html);

            if ($_REQUEST['ac'] != 'test') {

                //$strDate = '"2019-07-16 13:00:00"';
                $strDate = 'NOW()';

                $strSql = 'INSERT INTO  `izs_inform_ip` (`ii_id`, `ii_ip`, `ii_pp`, `ii_date`, `ii_cl_id`) ';
                $strSql.= 'VALUES (NULL, "' .$_REQUEST['ct_ip'] .'", "' .$strPPId .'", ' .$strDate .', "' .$intUser .'");';
                $intInsert = MySQLStatic::Insert($strSql);
            }

        }


    }



/*


*/    

    echo 1;

}


?>