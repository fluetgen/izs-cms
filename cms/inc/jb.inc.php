<?php

//CLASSES
//require_once('classes/class.bg.php');


//REQUEST
$strFu      = @$_REQUEST['fu'];

//DATA
//$objBg   = new Bg;

//SETTINGS

$boolShowHead = false;
$boolShowSide = false;

$strPageTitle = 'Jahresbericht';


//FUNCTIONS
$strAllRequest = '';
$strDetailRequest = '';
$strDownloadRequest = '';


$arrFilterSection = array();

$strIncOutput = '';

switch ($strFu) {

  case 'all':
    $strAllRequest = 'active';
    require_once('jb.all.inc.php');
    break;

  case 'detail':
    $strDetailRequest = 'active';
    require_once('jb.detail.inc.php');
    break;
  
  default:
    $strAllRequest = 'active';
    require_once('jb.all.inc.php');
    break;

}

//print_r($arrFilterSection);


//CONTENT

$strOutput = '

<style>
.page { width: 100%; !important }
.panel-actions {
  position: absolute;
  top: -80px;
}
</style>

  <!-- Page -->
  <div class="page animsition">
  
  
    <div class="page-aside">
      <div class="page-aside-switch">
        <i class="icon wb-chevron-left" aria-hidden="true"></i>
        <i class="icon wb-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner" style="position: fixed; width: 250px;">
        <section class="page-aside-section" style="width: 250px;">
          <h5 class="page-aside-title">Filter </h5><span id="resetFilter"></span>
          
          <form style="padding-left: 30px;">
';

$arrUseFilter = array(

    'pp' => 'Premium PDLs',  
    'ld' => 'Liefert Daten',
    'ac' => 'Aktiver Vertrag',
  
  );

  foreach ($arrUseFilter as $strKey => $strSelectName) {

    if (isset($arrFilterSection[$strKey]) && (count($arrFilterSection[$strKey]) > 0)) {
    
      asort($arrFilterSection[$strKey], SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL);
      
      $strOutput.= '            <div class="form-group form-material row">' .chr(10);
      $strOutput.= '              <div class="col-sm-10">' .chr(10);
      $strOutput.= '                <label class="control-label" for="filter_' .$strKey .'">' .$strSelectName .'</label>' .chr(10);
      $strOutput.= '                <select class="form-control filter" id="filter_' .$strKey .'" name="filter_' .$strKey .'" placeholder="' .$strSelectName .'">' .chr(10);
      $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
      foreach ($arrFilterSection[$strKey] as $intKey => $strValue) {
        $strOutput.= '                  <option value="' .$strKey .'_' .$intKey .'">' .$strValue .'</option>' .chr(10);
      }
      $strOutput.= '                </select>' .chr(10);
      $strOutput.= '              </div>' .chr(10);
      $strOutput.= '            </div>' .chr(10);
    }
  
  }

$strOutput.= '          
          </form>

        </section>
      </div>
    </div>
    
    
    
    <div class="page-main">
      <div class="page-header">
        <h1 class="page-title">' .$strPageTitle .'</h1>
';

if ($intAdmin == 1) {
$strOutput.= '
        <div style="z-index: 1;" class="page-header-actions">

          <div class="panel-actions">

            <div class="dropdown">
              <button type="button" class="btn btn-info dropdown-toggle pull-right" id="exampleAlignmentDropdown"
              data-toggle="dropdown" aria-expanded="true">
                Optionen
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="exampleAlignmentDropdown" role="menu">
                <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="doExport">Export CSV</a></li>' .chr(10);

$strOutput.= '              </ul>
            </div>

          </div>

        </div>
';
}

$strOutput.= '      
        <ul class="nav nav-tabs nav-tabs-line">
        <li class="' .$strAllRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=sp&fu=all">Premium PDLs</a></li>
        <!-- <li class="' .$strDetailRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=sp&fu=detail">Zertifikate</a></li> -->
        </ul>
      
      </div>
      <div class="page-content">
  
';

$strOutput.= $strIncOutput;

$strOutput.= '

      </div> <!-- End page-content -->

    </div> <!-- End page-main -->

  </div>
  <!-- End Page -->
  

';


$strOutput.= '

<form method="post" action="_get_csv.php" id="export">
  <input type="hidden" name="strTable" id="strTable" value="">
  <input type="hidden" name="encHd" id="encHd" value="">
</form>

';


$strJsFootScript.= '  <script src="js/script_neu.js"></script>' .chr(10);

$strJsFootCodeRun.= '
  
  $("#doExport").on("click", function(e) {

    var ct = $("table").length; 
    var table_hd = $($("table")[ct-3]).html();
    var table_cth = $("table.table:eq(1) tr:visible");
    var table_ct = "";
    
    if (table_cth.length > 0) {
      $.each(table_cth, function(k, v) {
        if (k > 1) table_ct += "<tr>" + $(v).html() + "</tr>";
      });
    }

    //console.log(table_hd);
    
    $("#strTable").val("<table>" + table_hd + table_ct + "</table>");
    $("#encHd").val(1);
    $("#useHidden").val(0);
    $("#export").submit();
    
  });

  $("#doClear").on("click", function(e) {

    $.post("inc/svdaten.ajax.php", { 
      ac: "clear"
    }).done(function(data) {
      location.reload();
    });

  });

  $("#resetFilter").on("click", function(e) {
  
    $("table.display tbody tr").show();
  
    $("select.filter option").each(function(i) {
      $(this).css("color", "#3f3f3f");
    });
  
    $("ul.select2-results__options li").css("color", "#3f3f3f");
    
    $("#DocCount").text($("table.display tbody tr:visible").length);
    $("#resetFilter").text("");
    
    $("select").val("");
      
  });
  
  $(".due").parent().parent().addClass("red");
  
  $("#resetFilter").hover(function() {
    $(this).css("cursor","pointer");
  });
  
  var $eventSelect = $(".filter");
  $eventSelect.on("select2:opening", function (e) { 
    var id = $(this).prop("id");
    setTimeout(function(){
      $("#select2-" + id +"-results li").each(function(i) {
        var str = $(this).prop("id").toString();
        if (str != "") {
          var tr = $("tr:visible td span.hfilter." + str.split("-")[4]);
          if (tr.length > 0) {
            $(this).css("color", "#3f3f3f");
          } else {
            $(this).css("color", "#a3afb7");
          }
        }
      });
    }, 1);
  });
  
  
  $(".filter").on("change", function(e) {
    
    var clstr = "span.hfilter";
    var fiid  = $(this).attr("id");
    var sval  = $(this).val();
    
    //if (uf !== null) {
    //  clstr = uf;
    //  uf = null;
    //} else {
      $("select.filter").each(function(i) {
        if ($(this).val() != "") clstr+= "." + $(this).val();
      });
    //}
    
    $("span.hfilter").parent().parent().hide();
    $(clstr).parent().parent().show();
  
    window.clstr = clstr;
    
    var res = [];
    
    $("table.display tbody tr:visible td:first-child span").each(function(i) {
      res = res.concat($(this).attr("class").split(" ").filter(function (item) {
        return res.indexOf(item) < 0;
      }));
      return res;
    });
    
    $("#DocCount").text($("table.display tbody tr:visible").length);
    $("#resetFilter").text("alle zurücksetzen");
  
    $(".concon:hidden:checked").prop("checked", false);
  
  });
  
  var url = new URL(location.href);
  var uf = url.searchParams.get("filter");
  if (uf !== null) {
  
    var fipa = uf.split(".");
    if (fipa.length > 2) {
      for (var i = 2; i < fipa.length; i++) {
        var v = fipa[i].split("_");
        $("#filter_" + v[0]).val(fipa[i]).trigger("change");
        console.log("#filter_" + v[0] + " -> " + fipa[i]);
      }
    }
  
  }
  
  setTimeout(function() { 
    if (uf === null) {
      //$("#filter_y").val("y_' .(date('Y') - 1) .'").trigger("change");
    }
  }, 500);  
  
    

';

?>