<?php

$arrFilter = array();

$arrEventList = $objBgEvent->arrGetBgEventList($arrFilter, true);

$arrBgNameList = array();
if (count($arrBgListEvent) > 0) {
  foreach ($arrBgListEvent as $intKey => $arrBg) {
    $arrBgNameList[$arrBg['acid']] = $arrBg['ac_name'];
  }
}

$arrMeldestellen = $objBg->arrGetMeldestellen();

$arrMeldestellenNameList = array();
if (count($arrMeldestellen) > 0) {
  foreach ($arrMeldestellen as $intKey => $arrMeldestelle) {
    $arrMeldestellenNameList[$arrMeldestelle['acid']] = $arrMeldestelle['ac_name'];
  }
}

$arrMeldestelleGruppe = [];
$strSql = 'SELECT * FROM `izs_bg_meldestelle`';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrDataset) {
    $arrMeldestelleGruppe[$arrDataset['Id']] = $arrDataset;
  }
}

$arrEscalation = [];
$strSql = 'SELECT * FROM `Group__c` WHERE `Eskalation__c` = "ja"';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrDataset) {
    $arrEscalation[$arrDataset['Id']] = $arrDataset;
  }
}

$arrTableHead = array(
  'Esc',
  'Jahr',
  'ID',
  'Chat',
  'Berufsgenossenschaft',
  'Meldestelle',
  'Status Bearbeitung',
  'Ergebnis',
  'Befristet bis',
  'DokArt',
  'Dokument',
  'Sichtbarkeit des Dokuments',
  'Sperrvermerk',
  'Bemerkung sichtbar',
  'Interne Statusinfo',
  'Status Klärung'
);

$arrStatusBearbeitung = $objBgEvent->arrGetSelection('be_status_bearbeitung');
$arrErgebnis = $objBgEvent->arrGetSelection('be_ergebnis');
$arrStatusKlaerung = $objBgEvent->arrGetSelection('be_klaerung_status');

$arrBefristet = array(
  'noch nicht fällig',
  'fällig',
  'ohne Befristung'
);

$arrList = array();
foreach ($arrEventList as $intRow => $arrEvent) {
  
  $strBesfistet = '';
  if ($arrEvent['be_befristet'] != '0000-00-00') {
    $strBesfistet = '<span class="hidden">' .strConvertDate($arrEvent['be_befristet'], 'Y-m-d', 'Ymd') .'</span>' .strConvertDate($arrEvent['be_befristet'], 'Y-m-d', 'd.m.Y');
  }
  
  $strClassAdd = '';
  
  $strClassAdd.= ' sp_' .$arrEvent['be_sperrvermerk'];
  $strSperrvermerk = 'nein';
  if ($arrEvent['be_sperrvermerk'] != 0) {
    $strSperrvermerk = 'ja';
  }
  if (!isset($arrFilterSection['sp'][$strSperrvermerk])) {
    $arrFilterSection['sp'][$arrEvent['be_sperrvermerk']] = $strSperrvermerk;
  }
  
  $strMeldestelle = @$arrMeldestellenNameList[$arrEvent['be_meldestelle']];
  if ($strMeldestelle == '') {
    $arrMeldestelle = $objPp->arrGetPP($arrEvent['be_meldestelle'], false);
    $strMeldestelle = $arrMeldestelle[0]['ac_name'];
  }
  
  $strDokumentArt = '';
  if (($arrEvent['be_dokument_art'] == '') || ($arrEvent['be_dokument_art'] == 'Unbedenklichkeitsbescheinigung (UBB)')) {
    $strDokumentArt = 'UBB';
  }
  
  $strClassAdd.= ' y_' .$arrEvent['be_jahr'];
  if (!isset($arrFilterSection['y'][$arrEvent['be_jahr']])) {
    $arrFilterSection['y'][$arrEvent['be_jahr']] = $arrEvent['be_jahr'];
  }

  $strClassAdd.= ' bg_' .$arrEvent['be_bg'];
  if (!isset($arrFilterSection['bg'][$arrEvent['be_bg']])) {
    $arrFilterSection['bg'][$arrEvent['be_bg']] = $arrBGList[$arrEvent['be_bg']];
  }
  
  $strClassAdd.= ' pp_' .$arrEvent['be_meldestelle'];
  if (!isset($arrFilterSection['pp'][$arrEvent['be_meldestelle']])) {
    $arrFilterSection['pp'][$arrEvent['be_meldestelle']] = $strMeldestelle;
  }
  
  $intSb = array_search($arrEvent['be_status_bearbeitung'], $arrStatusBearbeitung);
  $strClassAdd.= ' sb_' .$intSb;
  if (!isset($arrFilterSection['sb'][$intSb]) && ($intSb !== false)) {
    $arrFilterSection['sb'][$intSb] = $arrEvent['be_status_bearbeitung'];
  }
  
  $intEr = array_search($arrEvent['be_ergebnis'], $arrErgebnis);
  $strClassAdd.= ' er_' .$intEr;
  if (!isset($arrFilterSection['er'][$intEr]) && ($intEr !== false)) {
    $arrFilterSection['er'][$intEr] = $arrEvent['be_ergebnis'];
  }
  
  $intBefristet = 2;
  if ($arrEvent['be_befristet'] != '0000-00-00') { 
    //if (strtotime($arrEvent['be_befristet']) < strtotime('+ 14 days')) {
    if (strtotime($arrEvent['be_befristet']) <= strtotime('now')) {
      
      /*
      $arrFi[0] = array('strKey' => 'be_bg', 'strType' => '=', 'strValue' => $arrEvent['be_bg']);
      $arrFi[1] = array('strKey' => 'be_meldestelle', 'strType' => '=', 'strValue' => $arrEvent['be_meldestelle']);
      $arrFi[2] = array('strKey' => 'be_befristet', 'strType' => '>', 'strValue' => $arrEvent['be_befristet']);
      
      $arrFollowingEv = $objBgEvent->arrGetBgEventList($arrFi);
      
      if (count($arrFollowingEv) > 0) {
        $intBefristet = 2;
      } else {
        $intBefristet = 1;
        $strClassAdd.= ' due';
      }
      */

      $intBefristet = 1;
      $strClassAdd.= ' due';
      
    } else {
      $intBefristet = 0;
    }
  }
  $strClassAdd.= ' bb_' .$intBefristet;
  if (!isset($arrFilterSection['bb'][$intBefristet])) {
    $arrFilterSection['bb'][$intBefristet] = $arrBefristet[$intBefristet];
  }

  $intSichtbar = 0;
  if ($arrEvent['be_sichtbar'] == 'sichtbar') { 
    $intSichtbar = 1;
  }
  $strClassAdd.= ' si_' .$intSichtbar;
  if (!isset($arrFilterSection['si'][$intSichtbar])) {
    $arrFilterSection['si'][$intSichtbar] = $arrEvent['be_sichtbar'];
  }

  $strGroupId = $arrMeldestelleGruppe[$arrEvent['be_meldestelle']]['Group'] ?? '';

  if (isset($arrEscalation[$strGroupId])) { // 'a083000000D4hUMAAZ' https://izs.smartgroups.io/frontend/main/topic/topic-97f48d16-eb79-48c7-b450-6085ab217700

    //$arrEscalation[$strGroupId] = $arrEscalation['a083000000D4hUMAAZ'];

    preg_match('/topic-[\w-]*/', $arrEscalation[$strGroupId]['Eskalation_Link__c'], $arrLink);
    $strSgLink = $arrLink[0] ?? '';

    $strKey = 'ja';
    $strEsc = '<a href="javascript:void(0)" alt="' .$arrEscalation[$strGroupId]['Eskalation_Grund__c'] .'" title="' .$arrEscalation[$strGroupId]['Eskalation_Grund__c'] .'" onclick="openSG(\'' .$strSgLink .'\')" style="color:red;"><i class="icon fa fa-warning" aria-hidden="true"></i></a>';
  } else {
    $strKey = 'nein';
    $strEsc = '';
  }  

  $intSk = array_search($arrEvent['be_klaerung_status'], $arrStatusKlaerung);
  $strClassAdd.= ' sk_' .$intSk;
  if (!isset($arrFilterSection['sk'][$intSk]) && ($intSk !== false)) {
    $arrFilterSection['sk'][$intSk] = $arrEvent['be_klaerung_status'];
  }

  $strChatId = $objBgEvent->getSgId($arrEvent['beid']);
  
  $arrList[$arrEvent['beid']] = array(
    'esc' => $strEsc,
    'Jahr' => $arrEvent['be_jahr'],
    'ID' => '<a href="/cms/index_neu.php?ac=bg_detail&beid=' .$arrEvent['beid'] .'" target="_blank">' .$arrEvent['be_name'] .'</a><span class="hidden hfilter' .$strClassAdd .'"></span>',
    'chat' => '<a href="javascript:void(0);" onclick="openSG(\'' .$strChatId .'\');"><img src="https://izs.smartgroups.io/img/logo/logo_32.png" height="20"></a>',
    'Berufsgenossenschaft' => '<a href="/cms/index_neu.php?ac=contacts&acid=' .$arrEvent['be_bg'] .'" target="_blank">' .$arrBGList[$arrEvent['be_bg']] .'</a>',
    'Meldestelle' => $strMeldestelle .' ',
    'Status' => $arrEvent['be_status_bearbeitung'],
    'Ergebnis' => $arrEvent['be_ergebnis'],
    'Befristet bis' => $strBesfistet,
    'DokArt' => $strDokumentArt,
    'Dokument' => '<a href="pdf/bg/' .$arrEvent['be_link'] .'" target="_blank">' .$arrEvent['be_link'] .'</a>',
    'Sichtbarkeit des Dokuments' => $arrEvent['be_sichtbar'],
    'Sperrvermerk' => $strSperrvermerk,
    'Bemerkung sichtbar' => $arrEvent['be_bemerkung'],
    'Interne Statusinfo' => $arrEvent['be_info_intern'],
    'Status Klärung' => $arrEvent['be_klaerung_status']
  );

}

//print_r($arrList);
$arrOpt = array(
  'boolHasContext' => true
);

$strBgTable = strCreateTable($arrList, $arrTableHead, '', false, true, array(), $arrOpt);

$strIncOutput.= ' 

<script>
function openSG(topicId) {
  if (topicId != "") {
    setTimeout(()=>smartchat.cmd("topic.show", {entity: {id: topicId}}), 150);
  }
}
</script>

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Events für Berufsgenossenschaften gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strBgTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';


$strJsFootCodeRun.= '

  var url = new URL(location.href);
  var uf = url.searchParams.get("filter");

  setTimeout(function() { 
    if (uf === null) {
      $("#filter_y").val("y_' .(date('Y') - 1) .'").trigger("change");
    }
  }, 500);

';

?>