<?php

//REQUEST

$strContact = $_REQUEST['coid'];

//DATA

$objContact = new Contact;
$arrContact = $objContact->arrGetContact($strContact);

$arrIpPp = $objContact->arrGetIPPpFromContact($strContact);

$objPP = new PremiumPayer;
$arrPP = $objPP->arrGetPP($arrContact[0]['co_acid'], false);

$objEvent = new Event;
$objGroup = new Group;

$objAnfragestelle = new Anfragestelle;


//SETTINGS

$boolShowHead = false;
$boolShowSide = false;

$strName = strConcatName($arrContact[0]['co_first'], $arrContact[0]['co_last']);

$strPageTitle = $strName .' - Kontakt-Details ';

//CONTENT

$strOutput = '
  <!-- Page -->
  <div class="animsition container-fluid">
';

$strOutput.= strCreatePageTitle('Kontakt-Details', $strName);

$strOutput.= '
    <div class="page-content">
';


$strOutput.= ' 

    <div class="panel">
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-4">
          
            <table class="table table-striped">
               <colgroup>
                 <col width="50%">
                 <col width="50%">
               </colgroup>
               <thead>
              </thead>
              <tbody>
                <tr>
                  <th class="text-nowrap">Anrede</th>
                  <td>' .$arrContact[0]['co_salut'] .'</td>
                </tr>
                <tr>
                  <th class="text-nowrap">Name</th>
                  <td>' .$strName .'</td>
                </tr>
                <tr>
                  <th class="text-nowrap">Information Provider</th>
                  <td>' .$arrPP[0]['ac_name'] .'</td>
                </tr>
                <tr>
                  <th class="text-nowrap">Funktion</th>
                  <td>' .$arrContact[0]['co_function'] .'</td>
                </tr>
              </tbody>
            </table>
    
          </div>  <!-- COL1 -->
    
          <div class="col-lg-4">
          
            <table class="table table-striped">
               <colgroup>
                 <col width="50%">
                 <col width="50%">
               </colgroup>
              <thead>
              </thead>
              <tbody>
                <tr>
                  <th class="text-nowrap">Telefon</th>
                  <td>' .$arrContact[0]['co_phone'] .'</td>
                </tr>
                <tr>
                  <th class="text-nowrap">weitere Telefon-Nr.</th>
                  <td>' .$arrContact[0]['co_phone_other'] .'</td>
                </tr>
                <tr>
                  <th class="text-nowrap">E-Mail</th>
                  <td>' .$arrContact[0]['co_mail'] .'</td>
                </tr>
                <tr>
                  <th class="text-nowrap">Fax</th>
                  <td>' .$arrContact[0]['co_fax'] .'</td>
                </tr>
              </tbody>
            </table>
    
          </div>  <!-- COL2 -->
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

if ($arrPP[0]['RecordTypeId'] == '01230000001Ao72AAC') {
  $strDataNamePart = 'Krankenkassen/';
} elseif ($arrPP[0]['RecordTypeId'] == '01230000001DKGtAAO') {
  $strDataNamePart = 'Berufsgenossenschaften/';
}

if (count($arrIpPp) > 0)  {
  
$arrTableHead = array(
  'Information Provider', 
  'Premium Payer', 
  'Betriebsnummer PP'
);

$arrPpList = array();
$arrContactList = array();
foreach ($arrIpPp as $intRow => $arrRow) {
  $arrPP = $objPP->arrGetPP($arrRow['ppid']);
  
  $arrPpList[] = $arrRow['ppid'];
  $arrContactList[] = array(
    'Information Provider'  => $arrPP[0]['ac_name'], 
    'Premium Payer'         => $arrPP[0]['ac_name'], 
    'Betriebsnummer PP'     => $arrPP[0]['ac_betriebsnummer']
  );
}

$strContactTable = strCreateTable($arrContactList, $arrTableHead, '', false, true);

$strOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title">Kontakte IP zu PP</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-8">
          
';

$strOutput.= $strContactTable;

$strOutput.= ' 
    
          </div>  <!-- COL1 -->
    
          <div class="col-lg-66">
    
          </div>  <!-- COL2 -->
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

if ($strDataNamePart != 'Berufsgenossenschaften/') { //BG

$arrEventList = array();
if (count($arrPpList) > 0) {
  foreach ($arrPpList as $intKey => $strPp) {
    $arrEventList = array_merge($arrEventList, $objEvent->arrGetEventListFromIpPpInKlaerung($arrPP[0]['acid'], $strPp));
  }
}

$arrTableHead = array(
  'Monat', 
  'Event-ID', 
  'Premium Payer', 
  'BNR PP', 
  'Company Group', 
  'Anfragestelle', 
  'Status', 
  'Grund', 
  'Nächster Schritt', 
  'Wiedervorlage', 
  'Info', 
  'AP Klärung', 
  'Tel. Klärung'
);

$arrEventListFormatted = array();

foreach ($arrEventList as $intRow => $arrRow) {
  $arrGroup = $objGroup->arrGetGroup($arrRow['ev_grid']);
  $arrPp = $objPP->arrGetPPFromBetriebsnummer($arrRow['ev_ac_betriebsnummer']);
  $arrAnfragestelle = $objAnfragestelle->arrGetAnfragestelleFromEvent($arrRow['evid']);
  
  $arrEventListFormatted[] = array(
    'Monat' => $arrRow['ev_year'] .'/' .str_pad($arrRow['ev_month'], 2, '0', STR_PAD_LEFT), 
    'Event-ID' => $arrRow['ev_name'], 
    'Premium Payer' => $arrPp[0]['ac_name'], 
    'BNR PP' => $arrRow['ev_ac_betriebsnummer'], 
    'Company Group' => $arrGroup[0]['gr_name'], 
    'Anfragestelle' => $arrAnfragestelle[0]['as_name'], 
    'Status' => $arrRow['ev_status'], 
    'Grund' => $arrRow['ev_grund'], 
    'Nächster Schritt' => $arrRow['ev_meilenstein'], 
    'Wiedervorlage' => strFormatDate($arrRow['ev_bis'], 'd.m.Y'), 
    'Info' => $arrRow['ev_info'], 
    'AP Klärung' => $arrRow['ev_ap'], 
    'Tel. Klärung' => $arrRow['ev_tel']
  );
  
}

$strContactTable = strCreateTable($arrEventListFormatted, $arrTableHead, '', false, true);

$strOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title">Offene Klärungsfälle</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strOutput.= $strContactTable;

$strOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

} //NOT BG

}

$strOutput.= '
    </div>
  </div>
  <!-- End Page -->
';

?>