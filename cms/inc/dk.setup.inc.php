<?php

//CLASSES
//require_once('classes/class.bg.php');


//REQUEST
$strAction   = @$_REQUEST['ac'];
$strFunction = @$_REQUEST['fu'];

if ($strFunction == '') {
  $strFunction = 'setup';
}

if (@$_REQUEST['seldate'] != '') {
  
  if (strstr($_REQUEST['seldate'], '-') !== false) {
    $intTime = strtotime($_REQUEST['seldate'] .'-01');
  } else {
    $intTime = strtotime($_REQUEST['seldate'] .'-01-01');
  }

  $intTime = mktime(0, 0, 0, date('n', $intTime), date('j', $intTime), date('Y', $intTime));

  $intYear = date('Y', $intTime);  
  $intMonth = date('m', $intTime);  
  $intQuarter = date('m', $intTime);  

} else {

  $intYear = date('Y', strtotime("first day of last month"));
  $intMonth = date('m', strtotime("first day of last month"));
  $intQuarter = date('m', strtotime("first day of last month"));

}
//DATA
//$objBg   = new Bg;

//SETTINGS

$boolShowHead = false;
$boolShowSide = false;


//FUNCTIONS
$strDezKKRequest = '';

$arrFilterSection = array();

$strIncOutput = '';

switch ($strFunction) {
  case 'setup':
    $strDezKKRequest = 'active';
    
    if (isset($_REQUEST['id'])) {
      require_once('temp.edit.inc.php');
    } else {
      require_once('temp.list.inc.php');
    }
    
  break;
  
  case 'list':
    $strDezKKList = 'active';
    require_once('dk.list.inc.php');
  break;

  default:
    $strDezKKRequest = 'active';
    require_once('temp.list.inc.php');
    break;
}

//print_r($arrFilterSection);


//CONTENT

$strOutput = '
  <!-- Page -->
  <div class="page animsition">
  
  <!--
    <div class="page-aside">
      <div class="page-aside-switch">
        <i class="icon wb-chevron-left" aria-hidden="true"></i>
        <i class="icon wb-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner" style="position: fixed; width: 250px;">
        <section class="page-aside-section" style="width: 250px;">
          <h5 class="page-aside-title">Filter </h5><span id="resetFilter"></span>

';

$strOutput.= '          

        </section>
      </div>
    </div>
    
    -->
    
    <div class="page-main">
      <div class="page-header">
        <h1 class="page-title">' .$strPageTitle .'</h1>
';


$strOutput.= '      

<!--
        <ul class="nav nav-tabs nav-tabs-line">
          <li class="' .$strDezKKRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=dezkk&fu=setup">Setup</a></li>
        </ul>
-->

      </div>
      <div class="page-content">
  
';

$strOutput.= $strIncOutput;

$strOutput.= '

      </div> <!-- End page-content -->

    </div> <!-- End page-main -->

  </div>
  <!-- End Page -->
  

';


$strOutput.= '

<form method="post" action="_get_csv.php" id="export">
  <input type="hidden" name="strTable" id="strTable" value="">
  <input type="hidden" name="encHd" id="encHd" value="">
</form>

';


$strJsFootScript.= '  <script src="js/script_neu.js"></script>' .chr(10);

$strJsFootCodeRun.= '
  
  $("#doExport").on("click", function(e) {

    var ct = $("table").length; 
    var table_hd = $($("table")[ct-3]).html();
    var table_cth = $("table.table:eq(1) tr:visible");
    var table_ct = "";
    
    if (table_cth.length > 0) {
      $.each(table_cth, function(k, v) {
        table_ct += "<tr>" + $(v).html() + "</tr>";
      });
    }
    
    $("#strTable").val("<table>" + table_hd + table_ct + "</table>");
    $("#encHd").val(1);
    $("#export").submit();
    
  });  
';

?>