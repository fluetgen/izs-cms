<?php

$strSql = 'SELECT COUNT( * ) AS  `Anz`, `au_as_id`, `au_pp_group`, `au_name`, `as_include_name` FROM `izs_aue_erlaubnis` ';
$strSql.= 'INNER JOIN `izs_aue_search` ON ((`au_as_id` = `as_id`) AND (`au_pp_group` = `as_pp_group`)) ';
$strSql.= 'WHERE `au_pp_group` != "" GROUP BY `au_as_id` ORDER BY `au_name`';
$arrAUeListRaw = MySQLStatic::Query($strSql);
$intCountResult = count($arrAUeListRaw);

$arrAUeList = array();
if ($intCountResult > 0) {

  foreach ($arrAUeListRaw as $intKey => $arrAUe) {
    $arrAUeList[$arrAUe['au_as_id']] = array(
      'id'     => $arrAUe['au_as_id'],
      'group'  => $arrAUe['au_pp_group'],
      'name'   => $arrAUe['au_name'],
      'search' => $arrAUe['as_include_name'],
      'count'  => $arrAUe['Anz']
    );
  }

}

$arrTableHead = array(
  'Anzahl',
  'Prüfung ab',
  'Bezeichnung System',
  'Suchbegriff',
  'Bezeichnung BA',
);

$strSql = 'SELECT * FROM `izs_premiumpayer_group` LEFT JOIN `izs_aue_search` ON `izs_premiumpayer_group`.`Id` = `as_pp_group`';
$arrPpListRaw = MySQLStatic::Query($strSql);
$intResult = count($arrPpListRaw);

$strSelect = '';

$arrList = array();
foreach ($arrPpListRaw as $intRow => $arrGroup) {

  if ($arrGroup['as_valid_from'] == '') {
    $strFrom = '-';
    $strFromSort = -1;
  } else if ($arrGroup['as_valid_from'] == '0000-00-00') {
    $strFrom = 'initial';
    $strFromSort = 0;
  } else {
    $strFrom = date('d.m.Y', strtotime($arrGroup['as_valid_from']));
    $strFromSort = strtotime($arrGroup['as_valid_from']);
  }

  if (isset($arrAUeList[$arrGroup['as_id']])) {
  
    $arrList[] = array(
      'anzahl' => $arrAUeList[$arrGroup['as_id']]['count'] .'',
      'from' => '<span class="hidden">' .$strFromSort .'</span>' .$strFrom,
      'name1'  => $arrGroup['Name'],
      'search' => $arrAUeList[$arrGroup['as_id']]['search'],
      'name2'  => $arrAUeList[$arrGroup['as_id']]['name']
    );

  } else {

    $arrList[] = array(
      'anzahl' => 0,
      'from' => '<span class="hidden">' .$strFromSort .'</span>' .$strFrom,
      'name1'  => $arrGroup['Name'],
      'search' => '-',
      'name2'  => '-'
    );

  }

}


$strTable = strCreateTable($arrList, $arrTableHead, '', false, true);

$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Zeitarbeitsunternehmen gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strSelect;

$strIncOutput.= $strTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

$strJsFootCodeRun.= '

$("span.warning").parent().parent().addClass("warning");
$("span.danger").parent().parent().addClass("danger");

$("#seldate").on("change", function (e) {
  this.form.submit();
});

';


?>