<?php

$arrFilter = array();

$arrFilter[] = array(
  'strKey' => 'be_status_bearbeitung',
  'strType' => '=',
  'strValue' => 'angefragt'
);


$arrEventList = $objBgEvent->arrGetBgEventList($arrFilter);

$arrBgNameList = array();
if (count($arrBgListEvent) > 0) {
  foreach ($arrBgListEvent as $intKey => $arrBg) {
    $arrBgNameList[$arrBg['acid']] = $arrBg['ac_name'];
  }
}

$arrMeldestellen = $objBg->arrGetMeldestellen();

$arrMeldestellenNameList = array();

if (count($arrMeldestellen) > 0) {
  foreach ($arrMeldestellen as $intKey => $arrMeldestelle) {
    $arrMeldestellenNameList[$arrMeldestelle['acid']] = $arrMeldestelle['ac_name'];
    $arrMeldestelleGruppe[$arrMeldestelle['acid']] = $arrMeldestelle['ac_cgid'];
  }
}

$arrMeldestelleGruppe = [];
$strSql = 'SELECT * FROM `izs_bg_meldestelle`';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrDataset) {
    $arrMeldestelleGruppe[$arrDataset['Id']] = $arrDataset;
  }
}

$arrEscalation = [];
$strSql = 'SELECT * FROM `Group__c` WHERE `Eskalation__c` = "ja"';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrDataset) {
    $arrEscalation[$arrDataset['Id']] = $arrDataset;
  }
}

$strSql = 'SELECT `cl_ks_id` FROM `cms_login` WHERE `cl_id` = "' .$_SESSION['id'] .'"';
$arrSql = MySQLStatic::Query($strSql);

if (count($arrSql) > 0) {
  $strCrmUser = $arrSql[0]['cl_ks_id'];
}

$arrTableHead = array(
  'Esc',
  '<input type="checkbox" value="0" name="check_all_r">',
  'ID',
  'Chat',
  'Berufsgenossenschaft',
  'Meldestelle',
  'Unternehmensnummer',
  'Rückmeldung am',
  'Status Klärung',
  '',
  '',
  //'DokTyp',
  //'Ergebnis',
  //'Befristet bis'
);

$arrStatusBearbeitung = $objBgEvent->arrGetSelection('be_status_bearbeitung');
$arrErgebnis = $objBgEvent->arrGetSelection('be_ergebnis');
$arrStatusKlaerung = $objBgEvent->arrGetSelection('be_klaerung_status');

$arrBefristet = array(
  'noch nicht fällig',
  'fällig',
  'ohne Befristung'
);

$arrRueckmeldung = array(
  'nicht erhalten',
  'erhalten'
);

$arrList = array();
foreach ($arrEventList as $intRow => $arrEvent) {
  
  $strOption = ' disabled="disabled"';

  $strDatumRueckmeldung = '';
  $strDatumRueckmeldungHidden = '';
  if ($arrEvent['be_datum_rueckmeldung'] != '0000-00-00') {
    $strDatumRueckmeldung = strConvertDate($arrEvent['be_datum_rueckmeldung'], 'Y-m-d', 'd.m.Y');
    $strDatumRueckmeldungHidden = strConvertDate($arrEvent['be_datum_rueckmeldung'], 'Y-m-d', 'Ymd');
    $strOption = '';
  }
  
  $strBesfistet = '';
  $strDatumFrist = '';
  $strDatumFristHidden = '';
  if ($arrEvent['be_befristet'] != '0000-00-00') {
    $strDatumFrist = strConvertDate($arrEvent['be_befristet'], 'Y-m-d', 'd.m.Y');
    $strDatumFristHidden = strConvertDate($arrEvent['be_befristet'], 'Y-m-d', 'd.m.Y');
  }
  $strBesfistet = '
                  <div class="input-group">
                    <span class="hidden" id="frh_' .$arrEvent['beid'] .'">' .$strDatumFristHidden .'</span>
                    <span class="input-group-addon">
                      <i class="icon wb-calendar" aria-hidden="true"></i>
                    </span>
                    <input id="fr_' .$arrEvent['beid'] .'" type="text" class="form-control dp fr" data-plugin="datepicker" value="' .$strDatumFrist .'" data-date-format="dd.mm.yyyy"' .$strOption .'>
                  </div>
  ';

  $strErgebnis = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_ergebnis');
  $strErgebnis.= strGetFormElement($arrSelValues, $arrEvent['be_ergebnis'], 'select', 'er_' .$arrEvent['beid'], true, $strOption .' class="ergebnis"');

  $strRueckmeldung = '
                  <div class="input-group">
                    <span class="hidden" id="ruh_' .$arrEvent['beid'] .'">' .$strDatumRueckmeldungHidden .'</span>
                    <span class="input-group-addon">
                      <i class="icon wb-calendar today" aria-hidden="true" id="icr_' .$arrEvent['beid'] .'"></i>
                    </span>
                    <input id="ru_' .$arrEvent['beid'] .'" type="text" class="form-control dp ru" data-plugin="datepicker" value="' .$strDatumRueckmeldung .'" data-date-format="dd.mm.yyyy">
                  </div>
  ';

  $strDokumentArt = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_dokument_art');
  if ($arrEvent['be_dokument_art'] == '') {
    $arrEvent['be_dokument_art'] = 'UBB';
  }
  $strDokumentArt.= strGetFormElement($arrSelValues, $arrEvent['be_dokument_art'], 'select', 'da_' .$arrEvent['beid'], true, $strOption .' class="dart"');

  $strMitglNummer = $objBgEvent->strGetMitgliedsnummer($arrEvent['be_bg'], $arrEvent['be_meldestelle']);
  $strUnterNummer = $objBgEvent->strGetUnternehmensnummer($arrEvent['be_bg'], $arrEvent['be_meldestelle']);

  $strClassAdd = '';
  
  $strClassAdd.= ' y_' .$arrEvent['be_jahr'];
  if (!isset($arrFilterSection['y'][$arrEvent['be_jahr']])) {
    $arrFilterSection['y'][$arrEvent['be_jahr']] = $arrEvent['be_jahr'];
  }

  $strClassAdd.= ' bg_' .$arrEvent['be_bg'];
  if (!isset($arrFilterSection['bg'][$arrEvent['be_bg']])) {
    $arrFilterSection['bg'][$arrEvent['be_bg']] = $arrBGList[$arrEvent['be_bg']];
  }
  
  $strMeldestelle = @$arrMeldestellenNameList[$arrEvent['be_meldestelle']];
  if ($strMeldestelle == '') {
    $arrMeldestelle = $objPp->arrGetPP($arrEvent['be_meldestelle'], false);
    $strMeldestelle = $arrMeldestelle[0]['ac_name'];
  }
  
  $strClassAdd.= ' pp_' .$arrEvent['be_meldestelle'];
  if (!isset($arrFilterSection['pp'][$arrEvent['be_meldestelle']])) {
    $arrFilterSection['pp'][$arrEvent['be_meldestelle']] = $strMeldestelle;
  }
  //echo $arrEvent['be_meldestelle'] .' -> ';
  //echo 
  if (isset($arrMeldestelleGruppe[$arrEvent['be_meldestelle']])) {
    $strGroupId = $arrMeldestelleGruppe[$arrEvent['be_meldestelle']]['Group'];
  } else {
    continue;
  }
  
  //echo '<br>' .chr(10);

  if (isset($arrEscalation[$strGroupId])) { // 'a083000000D4hUMAAZ' https://izs.smartgroups.io/frontend/main/topic/topic-97f48d16-eb79-48c7-b450-6085ab217700

    //$arrEscalation[$strGroupId] = $arrEscalation['a083000000D4hUMAAZ'];

    preg_match('/topic-[\w-]*/', $arrEscalation[$strGroupId]['Eskalation_Link__c'], $arrLink);
    $strSgLink = $arrLink[0] ?? '';

    $strKey = 'ja';
    $strEsc = '<a href="javascript:void(0)" alt="' .$arrEscalation[$strGroupId]['Eskalation_Grund__c'] .'" title="' .$arrEscalation[$strGroupId]['Eskalation_Grund__c'] .'" onclick="openSG(\'' .$strSgLink .'\')" style="color:red;"><i class="icon fa fa-warning" aria-hidden="true"></i></a>';
  } else {
    $strKey = 'nein';
    $strEsc = '';
  }
  $strClassAdd.= ' es_' .$strKey;
  if (!isset($arrFilterSection['es'][$strKey])) {
    $arrFilterSection['es'][$strKey] = 'es_' .$strKey;
  }


  $strChatId = $objBgEvent->getSgId($arrEvent['beid']);
  
  $arrList[] = array(
    'esc' => $strEsc,
    'box' => '<input type="checkbox" name="klae[' .$arrEvent['beid'] .']" value="1" class="klae" rel="' .$arrEvent['beid'] .'" id="check_' .$arrEvent['beid'] .'">',
    'ID' => '<a href="/cms/index_neu.php?ac=bg_detail&beid=' .$arrEvent['beid'] .'" target="_blank">' .$arrEvent['be_name'] .'</a><span class="hidden hfilter' .$strClassAdd .'">',
    'chat' => '<a href="javascript:void(0);" onclick="openSG(\'' .$strChatId .'\');"><img src="https://izs.smartgroups.io/img/logo/logo_32.png" height="20"></a>',
    //'Berufsgenossenschaft' => $arrBgNameList[$arrEvent['be_bg']],
    'Berufsgenossenschaft' => '<a href="/cms/index_neu.php?ac=contacts&acid=' .$arrEvent['be_bg'] .'" target="_blank">' .$arrBGList[$arrEvent['be_bg']] .'</a>',
    'Meldestelle' => $strMeldestelle .' ',
    'Unternehmensnummer (BG)' => $strUnterNummer,
    'Rückmeldung am' => $strRueckmeldung,
    'Status Klärung' => $arrEvent['be_klaerung_status'],
    'OK' => '<button type="" class="btn right proof" id="be_' .$arrEvent['beid'] .'_ok">OK </button>',
    'Klärungsfall' => '<button type="" class="btn wrong proof" id="be_' .$arrEvent['beid'] .'_k">Klärungsfall </button>'
    //'Art des Dokuments' => $strDokumentArt,
    //'Ergebnis' => $strErgebnis,
    //'Befristet bis' => $strBesfistet
  );

}

$arrWidth = array('3%', '3%', '5%', '3%', '23%', '23%', '10%', '9%', '8%', '3%', '10%'); //, '10%', '7%', '7%'

$arrOpt = array(
  'arrSort' => array(
    'null', '"bSortable": false', 'null', '"bSortable": false', 'null', 'null', 'null', 'null', 'null', '"bSortable": false', '"bSortable": false'
  ),
  'strOrder' => '"order": [[ 2, "asc" ]]'
);

$strBgTable = strCreateTable($arrList, $arrTableHead, '', false, true, $arrWidth, $arrOpt);

$strIncOutput.= ' 

    <script>
    function openSG(topicId) {
      if (topicId != "") {
        setTimeout(()=>smartchat.cmd("topic.show", {entity: {id: topicId}}), 150);
      }
    }

    function handle() {
      var strValues = "";
      $(".klae").each (function() {
        if($(this).is(":checked")) {
          strValues+= "|" + $(this).attr(\'rel\');
        }
      });
      strValues = strValues.substr(1);
      return strValues;
    }

    </script>

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Events für Berufsgenossenschaften gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strBgTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
 

    <!-- Modal -->
    <div class="modal fade" id="MassOKModal" aria-hidden="true" aria-labelledby="MassOKModal"
    role="dialog" tabindex="-1">
      <div class="modal-dialog modal-center">
        <input type="hidden" name="beid_k" id="beid_k" value="">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">x</span>
            </button>
            <h4 class="modal-title">OK erfassen</h4>
          </div>
          <div class="modal-body">
            <p>&nbsp;</p>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>
    <!-- End Modal -->
    
    <!-- Modal -->
    <div class="modal fade" id="MassKlaerung" aria-hidden="true" aria-labelledby="MassKlaerung"
    role="dialog" tabindex="-1">
      <div class="modal-dialog modal-center">
        <input type="hidden" name="beid_k" id="beid_k" value="">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">x</span>
            </button>
            <h4 class="modal-title">Klärungsfall</h4>
          </div>
          <div class="modal-body">
            <p>&nbsp;</p>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>
    <!-- End Modal -->
    
    <!-- Modal -->
    <div class="modal fade" id="OK_Erfassen" aria-hidden="true" aria-labelledby="OK_Erfassen"
    role="dialog" tabindex="-1">
      <div class="modal-dialog modal-center">
        <input type="hidden" name="beid_ok" id="beid_ok" value="">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">x</span>
            </button>
            <h4 class="modal-title">OK erfassen</h4>
          </div>
          <div class="modal-body">
            <p>&nbsp;</p>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>
    <!-- End Modal -->


    <!-- Modal -->
    <div class="modal fade" id="Klaerung" aria-hidden="true" aria-labelledby="Klaerung"
    role="dialog" tabindex="-1">
      <div class="modal-dialog modal-center">
        <input type="hidden" name="beid_k" id="beid_k" value="">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">x</span>
            </button>
            <h4 class="modal-title">Klärungsfall erfassen</h4>
          </div>
          <div class="modal-body">
            <p>&nbsp;</p>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>
    <!-- End Modal -->


    <!-- Modal -->
    <div class="modal fade modal-danger" id="ModalDanger" aria-hidden="true"
    aria-labelledby="ModalDanger" role="dialog" tabindex="-1">
      <div class="modal-dialog modal-center smallw">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title">Achtung</h4>
          </div>
          <div class="modal-body">
            <p id="errMessage">Text</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>
    <!-- End Modal -->
    
';

$strJsFootScript.= '  <script src="js/script_neu.js"></script>' .chr(10);

/*
wi_    "`be_wiedervorlage`", 
gr_    "`be_klaerung_grund`", 
ns_    "`be_klaerung_schritt`", 
ap_    "`be_klaerung_ap`", 
at_    "`be_klaerung_telefon`"
*/

$strJsFootCodeRun.= '

// Fill modal with content from link href
$("#MassKlaerung").on("shown.bs.modal", function(e) {

    $(this).find(".modal-content").load("action/modal.php?ac=massKlae", function () {

      $(".modal .dp").datepicker();

    });
    
});

$("#MassOKModal").on("shown.bs.modal", function(e) {

  $(this).find(".modal-content").load("action/modal.php?ac=massOK", function () {

    $(".modal .dp").datepicker();

  });
  
});

$("input[name=\'check_all_r\']").click(function() {
  $("input[name^=\"klae\"]").prop("checked", $(this).prop("checked"));
});

$(document).on("click", "#okMass", function(){

  var checked = $("input[name^=\"klae\"]:checked");

  var beidMass = new Array();

  $.each(checked, function () {
    beidMass.push($(this).prop("id").split("check_")[1]);
  });

  var arrFields = {
    be_datum_rueckmeldung: $("#dr_all").val(),
    be_ergebnis:           $("#er_all").val(),
    be_stundung:           $("#st_all").val(),
    be_befristet:          $("#be_all").val(),
    be_bearbeiter:         "' .$strCrmUser .'",
    be_status_bearbeitung: "Endkontrolle"
  };

  $.ajax({
    type: "POST",
    url:  "action/ajax.php",
    dataType: "text", 
    data: { ac: "okClickMass", beidMass: beidMass, values: arrFields },
    success: function(result) {
      var jsonData = $.parseJSON(result);
      
      if (jsonData["Status"] == "OK") {

        var arrFields = jsonData["fields"];
        
        $("#MassOKModal").modal("hide");

        var tr = $("input[name^=\"klae\"]:checked").parent().parent();

        if (arrFields["be_datum_rueckmeldung"] != "") {
          if (arrFields["be_datum_rueckmeldung"] != "00.00.0000") tr.find("td:nth-child(8) .ru").val(arrFields["be_datum_rueckmeldung"]);
          else tr.find("td:nth-child(8) .ru").val("");
        }

        if (arrFields["be_klaerung_status"] != "") {
          if (arrFields["be_klaerung_status"] != "Alle Einträge löschen") {
            tr.find("td:nth-child(9)").html(arrFields["be_klaerung_status"]);
            tr.find("td:nth-child(3) span").removeClass("sk_0").removeClass("sk_1").removeClass("sk_2");
            if (arrFields["be_klaerung_status"] == "in Klärung") { tr.find("td:nth-child(3) span").addClass("sk_0"); }
            if (arrFields["be_klaerung_status"] == "geschlossen - negativ") { tr.find("td:nth-child(3) span").addClass("sk_2"); }
            if (arrFields["be_klaerung_status"] == "geschlossen - positiv") { tr.find("td:nth-child(3) span").addClass("sk_1"); }
          }
          else {
            tr.find("td:nth-child(9)").html("");
            tr.find("td:nth-child(3) span").removeClass("sk_0").removeClass("sk_1").removeClass("sk_2");
          }
        }

        tr.hide();

        
        //$("input[name^=\"klae\"]:checked").parent().parent().hide();
        $("input[name^=\"klae\"]:checked").prop("checked", false);

        
      }
      
      if (jsonData["Status"] == "FAIL") {

        $("#ModalDanger").modal("show");
        $("#ModalDanger #errMessage").html(jsonData["Reason"]);

      }

    }

  });


});

$(document).on("click", "#svKlaerungMass", function(){
  
  var checked = $("input[name^=\"klae\"]:checked");

  var beidMass = new Array();

  $.each(checked, function () {
    beidMass.push($(this).prop("id").split("check_")[1]);
  });

  var arrFields = {
    be_datum_rueckmeldung: $("#dr_all").val(),
    be_klaerung_status:    $("#sk_all").val(),
    be_klaerung_grund:     $("#gr_all").val(),
    be_klaerung_schritt:   $("#ns_all").val(),
    be_rueckstand:         $("#rr_all").val(),
    be_klaerung_ap:        $("#ap_all").val(),
    be_klaerung_telefon:   $("#at_all").val(),
    be_info_intern:        $("#ii_all").val(),
    be_wiedervorlage:      $("#wi_all").val(),
    be_stundung:           $("#st_all").val(),
    be_meldepflicht:       $("#me_all").val(),
    be_bearbeiter:         $("#us_all").val(),
  };

  $.ajax({
    type: "POST",
    url:  "action/ajax.php",
    dataType: "text", 
    data: { ac: "svKlaerungClickMass", beidMass: beidMass, values: arrFields },
    success: function(result) {
      var jsonData = $.parseJSON(result);
      
      if (jsonData["Status"] == "OK") {

        var arrFields = jsonData["fields"];
        
        $("#MassKlaerung").modal("hide");

        var tr = $("input[name^=\"klae\"]:checked").parent().parent();

        if (arrFields["be_klaerung_status"] != "Alle Einträge löschen") { 

          if (arrFields["be_klaerung_grund"] != "") {
          }
          if (arrFields["be_klaerung_status"] != "") {
            if (arrFields["be_klaerung_status"] != "Alle Einträge löschen") {
              tr.find("td:nth-child(9)").html(arrFields["be_klaerung_status"]);
              tr.find("td:nth-child(3) span").removeClass("sk_0").removeClass("sk_1").removeClass("sk_2");
              if (arrFields["be_klaerung_status"] == "in Klärung") { tr.find("td:nth-child(3) span").addClass("sk_0"); }
              if (arrFields["be_klaerung_status"] == "geschlossen - negativ") { tr.find("td:nth-child(3) span").addClass("sk_2"); }
              if (arrFields["be_klaerung_status"] == "geschlossen - positiv") { tr.find("td:nth-child(3) span").addClass("sk_1"); }
            }
            else {
              tr.find("td:nth-child(9)").html("");
              tr.find("td:nth-child(3) span").removeClass("sk_0").removeClass("sk_1").removeClass("sk_2");
            }
          }
          if (arrFields["be_wiedervorlage"] != "") {
          }
          if (arrFields["be_klaerung_schritt"] != "") {
          }
          if (arrFields["be_info_intern"] != "") {
          }
          if (arrFields["be_klaerung_ap"] != "") {
          }
          if (arrFields["be_klaerung_telefon"] != "") {
          }
          if (arrFields["be_bearbeiter"] != "") {
          } else {
          }
          if (arrFields["be_datum_rueckmeldung"] != "") {
            if (arrFields["be_datum_rueckmeldung"] != "00.00.0000") tr.find("td:nth-child(8) .ru").val(arrFields["be_datum_rueckmeldung"]);
            else tr.find("td:nth-child(8) .ru").val("");
          }

          if (arrFields["be_klaerung_status"] != $("#filter_sk option:selected").text()) {
            if (arrFields["be_klaerung_status"] != "") {
              //tr.hide();
              $("#DocCount").text($("#DocCount").text() - $("input[name^=\"klae\"]:checked").length); //
            }
          }

        } else {
          //tr.hide();
          $("#DocCount").text($("#DocCount").text() - $("input[name^=\"klae\"]:checked").length); //
        }
        
        //$("input[name^=\"klae\"]:checked").parent().parent().hide();
        $("input[name^=\"klae\"]:checked").prop("checked", false);
            
      }
      
      if (jsonData["Status"] == "FAIL") {

        $("#ModalDanger").modal("show");
        $("#ModalDanger #errMessage").html(jsonData["Reason"]);

      }

    }

  });

});


    $(document).on("click", "#massKlae", function() {

      var checked = $("input[name^=\"klae\"]:checked");

      if (checked.length > 0) {

        $("#MassKlaerung").modal("show");

      } else {

        $("#ModalDanger").modal("show");
        $("#ModalDanger #errMessage").html("Bitte wähle mindestens ein Event aus.");

      }

    });

    $(document).on("click", "#massOk", function() {

      var checked = $("input[name^=\"klae\"]:checked");

      if (checked.length > 0) {

        $("#MassOKModal").modal("show");

      } else {

        $("#ModalDanger").modal("show");
        $("#ModalDanger #errMessage").html("Bitte wähle mindestens ein Event aus.");

      }

    });

    $(document).on("click", ".proof", function() {

      var id = $(this).attr("id");
      var part = id.split("_");

      if (part[2] == "ok") {

        $("#beid_ok").val(part[1]);
        $("#OK_Erfassen").modal("show");

      } else {

        $("#beid_k").val(part[1]);
        $("#Klaerung").modal("show");

      }

    });

    // Fill modal with content from link href
    $("#OK_Erfassen").on("shown.bs.modal", function(e) {
        var beid = $("#beid_ok").val();
        $(this).find(".modal-content").load("action/modal.php?ac=ok_erfassen&beid=" + beid, function () {

          $(".modal .dp").datepicker();

        });
        
    });


    // Fill modal with content from link href
    $("#Klaerung").on("shown.bs.modal", function(e) {
        var beid = $("#beid_k").val();
        $(this).find(".modal-content").load("action/modal.php?ac=klaerung&beid=" + beid, function () {

          $(".modal .dp").datepicker();
          $("#us_" + beid).val("' .$strCrmUser .'");
          $("#sk_" + beid).val("in Klärung");

        });        
    });



    $(document).on("dragenter", "#open_btn", function() {
        $.FileDialog({
          multiple: false,
          dropheight: 300,
        }).on("files.bs.filedialog", function(ev) {
          
          $(".loading").css("display", "block");
          
          var files = ev.files;

          var data = new FormData();
          for (var i = 0; i < files.length; i++) {
              data.append(files[i].name, files[i]);
          }
          $.ajax({
              url: "action/upload.php",
              type: "POST",
              data: data,
              contentType: false,
              processData: false,
              success: function (result) {
                  //$(location).attr("href", "locations_upload.php?grp=7&fi=" + result);
              },
              error: function (err) {
                  alert(err.statusText)
              }
          });
        }).on("cancel.bs.filedialog", function(ev) {
            //$("#output").html("Cancelled!");
        });
    });

    $(document).on("dragleave", "#fdfd", function() {
      console.log("leave");
    });

    $(document).on("mouseout", "#fdfd", function() {
      console.log("out");
    });

  //$(document).on("click", ".dp", function(){
  //  $(this).datepicker();
  //});

  //MassOKModal


  $(document).on("click", "#Erfassen_ok", function(){  
  
    var beid = $("#beid_ok").val();

    var arrFields = {
      be_datum_rueckmeldung: $("#dr_" + beid).val(),
      be_ergebnis:           $("#er_" + beid).val(),
      be_stundung:           $("#st_" + beid).val(),
      be_befristet:          $("#be_" + beid).val(),
      be_bearbeiter:         "' .$strCrmUser .'",
      be_status_bearbeitung: "Endkontrolle",
      be_meldepflicht:       $("#me_" + beid).val()
    };

    $.ajax({
      type: "POST",
      url:  "action/ajax.php",
      dataType: "text", 
      data: { ac: "okClick", beid: beid, values: arrFields },
      success: function(result) {
        var jsonData = $.parseJSON(result);
        
        if (jsonData["Status"] == "OK") {
          
        	$("#OK_Erfassen").modal("hide");
        	$("#be_" + beid + "_ok").parent().parent().hide()
              
        }
        
        if (jsonData["Status"] == "FAIL") {

          $("#ModalDanger").modal("show");
          $("#ModalDanger #errMessage").html(jsonData["Reason"]);

        }

      }

    });

  });



  $(document).on("click", "#svKlaerung", function(){
  
    var beid = $("#beid_k").val()

    var arrFields = {
      be_rueckstand:         $("#rr_" + beid).val(),
      be_wiedervorlage:      $("#wi_" + beid).val(),
      be_klaerung_grund:     $("#gr_" + beid).val(),
      be_klaerung_schritt:   $("#ns_" + beid).val(),
      be_klaerung_ap:        $("#ap_" + beid).val(),
      be_klaerung_telefon:   $("#at_" + beid).val(),
      be_info_intern:        $("#ii_" + beid).val(),
      be_datum_rueckmeldung: $("#dr_" + beid).val(),
      be_klaerung_status:    "in Klärung",
      be_stundung:           $("#st_" + beid).val(),
      be_meldepflicht:       $("#me_" + beid).val(),
      be_bearbeiter:         $("#us_" + beid).val()
    };

    $.ajax({
      type: "POST",
      url:  "action/ajax.php",
      dataType: "text", 
      data: { ac: "svKlaerungClick", beid: beid, values: arrFields },
      success: function(result) {
        var jsonData = $.parseJSON(result);
        
        if (jsonData["Status"] == "OK") {
          
        	$("#Klaerung").modal("hide");
        	$("#be_" + beid + "_k").parent().parent().hide()
              
        }
        
        if (jsonData["Status"] == "FAIL") {

          $("#ModalDanger").modal("show");
          $("#ModalDanger #errMessage").html(jsonData["Reason"]);

        }

      }

    });

  });





  $(".today").click(function(){
    var id = $(this).attr("id");
    var parts = id.split("_");
    var idd = parts[1];
    
    $("#ru_" + idd).val("' .date('d.m.Y') .'");
    $("#ru_" + idd).attr("value", "' .date('d.m.Y') .'");
    $("#ruh_" + idd).html("' .date('Ymd') .'");
    $("#ru_" + idd).trigger("blur");
  });

  $(".today").bind("mouseover", function() {
      $(this).css("cursor", "pointer");
  });
  
  $(".ergebnis").change(function(){
    var id = $(this).attr("id");
    var parts = id.split("_");
    var idd = parts[1];
    
    $.ajax({
      type: "POST",
      url:  "action/ajax.php",
      dataType: "text", 
      data: { ac: "bgErChange", beid: idd, value: $(this).val() },
      success: function(result) {
        var jsonData = $.parseJSON(result);
        
        if (jsonData["Status"] == "OK") {
          

          if (jsonData["Action"]) {
            
            $("#beid").val(jsonData["beid"]);
            $("#" + jsonData["Name"]).modal("show");

          }
              
        }
        
        if (jsonData["Status"] == "FAIL") {

          $("#ModalDanger").modal("show");
          $("#ModalDanger #errMessage").html(jsonData["Reason"]);

        }

      }
    });

  });

  
  $(".dart").change(function(){
    var id = $(this).attr("id");
    var parts = id.split("_");
    var idd = parts[1];
    
    $.ajax({
      type: "POST",
      url:  "action/ajax.php",
      dataType: "text", 
      data: { ac: "bgDaChange", beid: idd, value: $(this).val() },
      success: function(result) {
        var jsonData = $.parseJSON(result);
        
        if (jsonData["Status"] == "OK") {
          

          if (jsonData["Action"]) {
            
            $("#beid").val(jsonData["beid"]);
            $("#" + jsonData["Name"]).modal("show");

          }
              
        }
        
        if (jsonData["Status"] == "FAIL") {

          $("#ModalDanger").modal("show");
          $("#ModalDanger #errMessage").html(jsonData["Reason"]);

        }

      }
    });

  });

  
  $(".fr").blur(function(){
    var id = $(this).attr("id");
    var parts = id.split("_");
    var idd = parts[1];
    
    setTimeout(function(){
      var fr_date = $("#fr_" +idd).val();
      $.ajax({
        type: "POST",
        url:  "action/ajax.php",
        dataType: "text", 
        data: { ac: "bgFrChange", beid: idd, value: fr_date },
        success: function(result) {
          var jsonData = $.parseJSON(result);
          
          if (jsonData["Status"] == "OK") {
                
          }
          
          if (jsonData["Status"] == "FAIL") {
  
            $("#ModalDanger").modal("show");
            $("#ModalDanger #errMessage").html(jsonData["Reason"]);
  
          }
  
        }
      });
    }, 500);


  });

  
  $(".ru").blur(function(){
    var id = $(this).attr("id");
    var parts = id.split("_");
    var idd = parts[1];
    
    setTimeout(function(){
      var ru_date = $("#ru_" +idd).val();
      $.ajax({
        type: "POST",
        url:  "action/ajax.php",
        dataType: "text", 
        data: { ac: "bgRuChange", beid: idd, value: ru_date },
        success: function(result) {
          var jsonData = $.parseJSON(result);
          
          if (jsonData["Status"] == "OK") {
            
            if (jsonData["value"] != "") {
              $("#er_" +idd).prop("disabled", false);
              $("#fr_" +idd).prop("disabled", false);
              $("#da_" +idd).prop("disabled", false);
            } else {
              $("#er_" +idd).prop("disabled", true);
              $("#fr_" +idd).prop("disabled", true);
              $("#da_" +idd).prop("disabled", true);
            }
                
          }
          
          if (jsonData["Status"] == "FAIL") {
  
            $("#ModalDanger").modal("show");
            $("#ModalDanger #errMessage").html(jsonData["Reason"]);
  
          }
  
        }
      });
    }, 500);


  });

';


?>