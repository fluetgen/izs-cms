<?php

function arrGetBearbeiter ($intTask = 0, $strField = 'Name') {

  $arrReturn = array();

  $strSql = 'SELECT `User`.`Id`, `User`.`Name`, `User`.`Alias`, `UserTask`.* FROM `UserTask` INNER JOIN `User` ON `User`.`Id` = `User_Id` WHERE `ut_ta_id` = "' .$intTask .'" ORDER BY `User`.`' .$strField .'` ';
  $arrResult = MySQLStatic::Query($strSql);
  $intCountResult = count($arrResult);
  
  if ($intCountResult > 0) {
    foreach ($arrResult as $intKey => $arrUser) {
      $arrReturn[$arrUser['Id']] = $arrUser[$strField];
    }
  }

    return $arrReturn;

}

$arrBearbeiterList = arrGetBearbeiter(3, 'Alias');

$arrListSvAnfrage = [];
$arrListSvAnfrage[] = '';

if (count($arrBearbeiterList) > 0) {
  foreach ($arrBearbeiterList as $strKey => $strAlias) {
    $arrListSvAnfrage[] = $strAlias;
  }
}


$arrStatus = array(
    'Offen',
    'Anteilig erhalten',
    'Abgelegt in Company Group Ordner',
    'Freigabe zum Import',
    'Freigabe zum Import - siehe Chat',
    'Fertig zum Import',
    'Fertig zum Import - siehe Chat',
    'Problem bei Anlieferung',
    'Problem bei Verarbeitung',
    'Problem bei Import',
    'Importiert',
    'Neuanbindung - Anfrage erst im nächsten Monat',
    'SV zurückgestellt / ZA in Klärung - siehe Chat',
    'Vierteljährliche Anfrage - siehe Chat'
  );
  
  $arrKlaerung = array(
    'true' => 'Ja',
    'false' => 'Nein'
  );
  
  $arrEdit = $arrListSvAnfrage;
  
  $arrProove = $arrListSvAnfrage;
  
  $arrFormat = array(
    '',
    'CSV / XLS',
    'PDF / EL',
    'ZIP',
    'Sonstiges'
  );
  
  $arModell = array(
    'Premium',
    'Light',
    'Sponsored',
    'Sponsored PDL', 
    'Sponsored ENT'
  );
  
  $arrSoftware = array(
    'Addison Lohn & Gehalt',
    'Agenda',
    'Anderer Anbieter',
    'BAB',
    'Dakota',
    'DATEV',
    'E+S',
    'Eurodata',
    'Exact',
    'Landwehr',
    'Lexware Dakota',
    'Lohnwerk KG',
    'NAVISION Napa',
    'PDS',
    'ProPers',
    'ProSoft AÜOffice',
    'SAGE Personalwirtschaft',
    'SAP',
    'SBS',
    'VEDA',
    'Zvoove'
  );
  
  $arrTableHead = array(
    '<input type="checkbox" id="selall">',
    'Esc',
    'Company Group',
    'Mitgliedsmodell',
    'Offene Vorgänge',
    'Offene Chats',
    'Prio',
    'Kündigung zum',
    'Registriert seit',
    'Status',
    'In Klärung',
    'Bearbeitet von',
    'Geprüft von',
    'Datenformat',
    'Software',
    'Hinweis Bearbeitung',
    //'Hinweis Upload',
    'Kommentar'
  );
  

?>