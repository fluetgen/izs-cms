<?php

//CLASSES
require_once('classes/class.event.php');
require_once('classes/class.informationprovider.php');
require_once('classes/class.premiumpayer.php');

//REQUEST
$strYear  = @$_REQUEST['filter_y'];
$strFu    = @$_REQUEST['fu'];

if (isset($strYear)) {
  $arrDate = explode('_', $strYear);
  $intYear  = $arrDate[1];
  $intMonth = $arrDate[2];
}

if (empty($intYear)) {
    $intYear = date('Y');
    if (date('n') == 1) {
        $intYear--;
    }
}
if (empty($intMonth)) {
    if (date('n') == 1) {
        $intMonth = 12;
    } else {
        $intMonth = date('n') - 1;
    }
}

$strStartDate = $intYear .'_' .$intMonth;

//DATA
$objIp        = new InformationProvider;
$arrIpList    = array();
$arrIpDezList = array();
$arrIpListRaw = $objIp->arrInformationProviderList();

if (count($arrIpListRaw) > 0) { // ALL IPs WITH "Dezentrale Anfrage"
    foreach ($arrIpListRaw as $intKey => $arrIp) {
        $arrIpList[$arrIp['Id']] = $arrIp['Name'];
        if ($arrIp['Anfrage'] == 'Dezentrale Anfrage') {
          $arrIpDezList[$arrIp['Id']] = $arrIp['Name'];
        }
    }
}

$arrDezAnfrage    = array();
$arrDezAnfrageRaw = $objIp->arrDezentraleAnfrage();

if (count($arrDezAnfrageRaw) > 0) {
    foreach ($arrDezAnfrageRaw as $intKey => $arrEv) {
      if (!isset($arrDezAnfrage[$arrEv['Information_Provider__c']])) {
        $arrDezAnfrage[$arrEv['Information_Provider__c']] = array();
      }
      $arrDezAnfrage[$arrEv['Information_Provider__c']][$arrEv['Premium_Payer__c']] = $arrEv['Name'];
    }
}

$objPp         = new PremiumPayer;
$arrPpBtnrList = array();
$arrPpIdList   = array();
$arrPpListRaw  = $objPp->arrGetPremiumPayerList();

if (count($arrPpListRaw) > 0) {
    foreach ($arrPpListRaw as $intKey => $arrPp) {
        $arrPpBtnrList[$arrPp['Btnr']] = $arrPp['Name'];
        $arrPpIdList[$arrPp['Btnr']]   = $arrPp['Id'];
    }
}

$objPp        = new PremiumPayer;
$arrPpGrpList = array();
$arrPpGrpListRaw = $objPp->arrGetPremiumPayerGroupList();

if (count($arrPpGrpListRaw) > 0) {
    foreach ($arrPpGrpListRaw as $intKey => $arrPp) {
        $arrPpGrpList[$arrPp['Id']] = $arrPp['Name'];
    }
}

$objEvent        = new Event;
$arrCatchAllList = array();
$arrCatchAllRaw = $objEvent->arrGetCatchAll();

if (count($arrCatchAllRaw) > 0) {
    foreach ($arrCatchAllRaw as $intKey => $arrIp) {
        $arrCatchAllList[$arrIp['Information_Provider__c']] = $arrIp['Name'];
    }
}

if (isset($_SERVER['HTTP_FKLD'])) {
  //print_r($arrCatchAllList);
}

//SETTINGS

$boolShowHead = false;
$boolShowSide = false;

$strPageTitle = 'Bearbeitungs-Status KK';


//FUNCTIONS
$strActiveRequest = '';
$strRuecklaufRequest = '';
$strKlaerungRequest = '';
$strZuordnungRequest = '';
$strEndkontrolleRequest = '';
$strAllRequest    = '';
$strDueRequest    = '';

$arrFilterSection = array();

$strIncOutput = '';

switch ($strFu) {
  case 'all':
    $strAllRequest = 'active';
    require_once('kk_status.all.inc.php');
    break;
  default:
    $strAllRequest = 'active';
    require_once('kk_status.all.inc.php');
    break;
}

//print_r($arrFilterSection);


//CONTENT

$strOutput = '
  <!-- Page -->
  <div class="page animsition">
  
    <div class="page-aside">
      <div class="page-aside-switch">
        <i class="icon wb-chevron-left" aria-hidden="true"></i>
        <i class="icon wb-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner" style="position: fixed; width: 250px;">
        <section class="page-aside-section" style="width: 250px;">
          <h5 class="page-aside-title">Filter </h5><span id="resetFilter"></span>
          
          <form style="padding-left: 30px;" id="form_filter" action="index_neu.php?ac=' .$_REQUEST['ac'] .'" method="post">
';

if (isset($arrFilterSection['y']) && (count($arrFilterSection['y']) > 0)) {
  
  arsort($arrFilterSection['y']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_y">Monat</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_y" name="filter_y" placeholder="Jahr" data-plugin="select2">' .chr(10);
  //$strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['y'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="y_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['pp']) && (count($arrFilterSection['pp']) > 0)) {
  
  natcasesort($arrFilterSection['pp']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_pp">Meldestelle</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_pp" name="filter_pp" placeholder="Meldestelle" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['pp'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="pp_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['ru']) && (count($arrFilterSection['ru']) > 0)) {
  
  arsort($arrFilterSection['ru']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_ru">Rückmeldung</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_ru" name="filter_ru" placeholder="Rückmeldung" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['ru'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="ru_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['sb']) && (count($arrFilterSection['sb']) > 0)) {
  
  natcasesort($arrFilterSection['sb']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_sb">Status Bearbeitung</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_sb" name="filter_sb" placeholder="Status Bearbeitung" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['sb'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="sb_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}


if (isset($arrFilterSection['ip']) && (count($arrFilterSection['ip']) > 0)) {
  
  natcasesort($arrFilterSection['ip']);
  
  $strOutput.= '            <div class="form-group form-material row" style="padding-top: 20px;">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_ip">Information Provider</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_ip" name="filter_ip" placeholder="Information Provider" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['ip'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="ip_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['er']) && (count($arrFilterSection['er']) > 0)) {
  
  natcasesort($arrFilterSection['er']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_er">Ergebnis</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_er" name="filter_er" placeholder="Ergebnis" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['er'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="er_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['bb']) && (count($arrFilterSection['bb']) > 0)) {
  
  natcasesort($arrFilterSection['bb']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_bb">Befristet bis</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_bb" name="filter_bb" placeholder="Befristet bis" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['bb'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="bb_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['sp']) && (count($arrFilterSection['sp']) > 0)) {
  
  natcasesort($arrFilterSection['sp']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_sp">Sperrvermerk</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_sp" name="filter_sp" placeholder="Sperrvermerk" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['sp'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="sp_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['si']) && (count($arrFilterSection['si']) > 0)) {
  
  natcasesort($arrFilterSection['si']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_si">Sichtbarkeit des Dokuments</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_si" name="filter_si" placeholder="Sichtbarkeit des Dokuments" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['si'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="si_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['sk']) && (count($arrFilterSection['sk']) > 0)) {
  
  natcasesort($arrFilterSection['sk']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_sk">Status Klärung</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_sk" name="filter_sk" placeholder="Status Klärung" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  $strOutput.= '                  <option value="sk_">kein Status</option>' .chr(10);
  foreach ($arrFilterSection['sk'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="sk_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['gk']) && (count($arrFilterSection['gk']) > 0)) {
  
  natcasesort($arrFilterSection['gk']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_gk">Grund für Klärung</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_gk" name="filter_gk" placeholder="Grund Klärung" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  $strOutput.= '                  <option value="gk_">kein Grund</option>' .chr(10);
  foreach ($arrFilterSection['gk'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="gk_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}


if (isset($arrFilterSection['ns']) && (count($arrFilterSection['ns']) > 0)) {
  
  natcasesort($arrFilterSection['ns']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_ns">Nächster Schritt</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_ns" name="filter_ns" placeholder="Nächster Schritt" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['ns'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="ns_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}


if (isset($arrFilterSection['wi']) && (count($arrFilterSection['wi']) > 0)) {
  
  natcasesort($arrFilterSection['wi']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_wi">Wiedervorlage</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_wi" name="filter_wi" placeholder="Wiedervorlage" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['wi'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="wi_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

$strOutput.= '          
          </form>

        </section>
      </div>
    </div>
    <div class="page-main">
      <div class="page-header">
        <h1 class="page-title">' .$strPageTitle .'</h1>
';

$strOutput.= '
        <div style="z-index: 1;" class="page-header-actions">

          <div class="panel-actions">

            <div class="dropdown">
              <button type="button" class="btn btn-info dropdown-toggle pull-right" id="exampleAlignmentDropdown"
              data-toggle="dropdown" aria-expanded="true">
                Optionen
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="exampleAlignmentDropdown" role="menu">
                <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="doExport">Export CSV</a></li>' .chr(10);
$strOutput.= '              </ul>
            </div>

          </div>

        </div>
';


$strOutput.= '      
        <ul class="nav nav-tabs nav-tabs-line">
          <li class="' .$strAllRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bg_events&fu=all">Alle</a></li>
        </ul>
      
      </div>
      <div class="page-content">
  
';

$strOutput.= $strIncOutput;

$strOutput.= '

      </div> <!-- End page-content -->

    </div> <!-- End page-main -->

  </div>
  <!-- End Page -->
  
';


$strJsFootScript.= '  <script src="js/script_neu.js"></script>' .chr(10);

$strJsFootCodeRun.= '

  $("#doExport").on("click", function(e) {
  
    var table_hd = $($("table")[0]).html();
    var table_cth = $("table.table:eq(1) tr:visible");
    var table_ct = "";
    
    if (table_cth.length > 0) {
      $.each(table_cth, function(k, v) {
        table_ct += "<tr>" + $(v).html() + "</tr>";
      });
    }
    
    $("#strTable").val("<table>" + table_hd + table_ct + "</table>");
    $("#export").submit();
    
  });  

  $("#resetFilter").on("click", function(e) {
  
    $("table.display tbody tr").show();

    $("select.filter option").each(function(i) {
      $(this).css("color", "#3f3f3f");
    });

    $("ul.select2-results__options li").css("color", "#3f3f3f");
    
    $("#DocCount").text($("table.display tbody tr:visible").length);
    $("#resetFilter").text("");
    
    $("select").val("");
      
  });

  $("#resetFilter").hover(function() {
    $(this).css("cursor","pointer");
  });
  
  var $eventSelect = $(".filter");
  $eventSelect.on("select2:opening", function (e) { 
    var id = $(this).prop("id");
    setTimeout(function(){
      $("#select2-" + id +"-results li").each(function(i) {
        var str = $(this).prop("id").toString();
        if (str != "") {
          var tr = $("tr:visible td span.hfilter." + str.split("-")[4]);
          var tr = $("tr td span.hfilter." + str.split("-")[4]);
          if (tr.length > 0) {
            $(this).css("color", "#3f3f3f");
          } else {
            $(this).css("color", "#a3afb7");
          }
        }
      });
    }, 1);
  });


  $(".filter:not(#filter_y,#filter_sb,#filter_ru)").on("change", function(e) {
    
    var clstr = "span.hfilter";
    var fiid  = $(this).attr("id");
    var sval  = $(this).val();
    
    //if (uf !== null) {
    //  clstr = uf;
    //  uf = null;
    //} else {
      $("select.filter").each(function(i) {
        if ($(this).val() != "") clstr+= "." + $(this).val();
      });
    //}
    
    $("span.hfilter").parent().parent().hide();
    $(clstr).parent().parent().show();

    window.clstr = clstr;
    
    var res = [];
    
    $("table.display tbody tr:visible td:first-child span").each(function(i) {
      res = res.concat($(this).attr("class").split(" ").filter(function (item) {
        return res.indexOf(item) < 0;
      }));
      return res;
    });
    
    $("#DocCount").text($("table.display tbody tr:visible").length);
    $("#resetFilter").text("alle zurücksetzen");

  });

  var url = new URL(location.href);
  var uf = url.searchParams.get("filter");
  if (uf !== null) {

    var fipa = uf.split(".");
    if (fipa.length > 2) {
      for (var i = 2; i < fipa.length; i++) {
        var v = fipa[i].split("_");
        $("#filter_" + v[0]).val(fipa[i]).trigger("change");
        console.log("#filter_" + v[0] + " -> " + fipa[i]);
      }
    }

  }

  setTimeout(function() { 
    if (uf === null) {
      $("#filter_y").val("y_' .$strStartDate .'").trigger("change");
      $("#filter_sb").val("' .$strStartStatus .'").trigger("change");
      $("#filter_ru").val("' .$strStartRueck .'").trigger("change");
';

if (isset($_REQUEST['filter_ip']) && ($_REQUEST['filter_ip'] != '')) {
  $strJsFootCodeRun.= '      $("#filter_ip").val("' .$_REQUEST['filter_ip'] .'").trigger("change");' .chr(10);
}


$strJsFootCodeRun.= '
    }
  }, 500);

  setTimeout(function() { 
    $("#filter_y").on("change", function(e) {
      //console.log("submit"); //
      $("#form_filter").submit();
    });
  }, 600);

  setTimeout(function() { 
    $("#filter_sb").on("change", function(e) {
      //console.log("submit"); //
      $("#form_filter").submit();
    });
  }, 600);

  setTimeout(function() { 
    $("#filter_ru").on("change", function(e) {
      //console.log("submit"); //
      $("#form_filter").submit();
    });
  }, 600);

  // ------
  (function() {
    $(\'.EventContext\').contextmenu({
      target: \'#ContextMenu\',
      before: function(e) {
        //console.log($(e.target).parent().data("contextid"));
        e.preventDefault();
        return true;
      },
      onItem: function (context, e) {
        var action = $(e.target).data("action");
        var id = $(context).data("contextid");
        if (action == 3) {
          $("#deleteid").val(id);
          $("#modalDeleteBgEvent").modal("show");
        }
        if (action == 2) {
          $("#duplicateid").val(id);
          $("#modalDuplicateBgEvent").modal("show");
        }
        if (action == 1) {
          $("#editid").val(id);
          $("#modalEditBgEvent").modal("show");
        }
        console.log(action + " - " + id);
      }
    });
  })();


';

?>