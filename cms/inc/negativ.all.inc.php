<?php

//CLASSES

$arrTraffic = array(
    '' => '',
    'green'  => '<span class="hidden">3</span><span class="label bg-color-green"><i class="fa fa-check-circle-o"></i></span>',
    'yellow' => '<span class="hidden">2</span><span class="label bg-color-orange"><i class="fa fa-dot-circle-o"></i></span>',
    'red'    => '<span class="hidden">1</span><span class="label bg-color-red"><i class="fa fa-ban"></i>',
    'gray'   => '<span class="hidden">0</span><span class="label bg-color-lighten"><i class="fa fa-circle-o"></i></span>'
);

$arrTrafficGerman = array(
  ''       => 'kein Status',
  'green'  => 'grün',
  'yellow' => 'gelb',
  'red'    => 'rot',
  'gray'   => 'grau'
);

$arrEscGerman = array(
  0 => 'nein',
  1 => 'ja'
);

$arrTrafficKey = array(
  ''       => '',
  'green'  => '3',
  'yellow' => '2',
  'red'    => '1',
  'gray'   => '0'
);


//DATA

$strUrl = 'https://www.izs-institut.de/api/ver/?esc=1';
$strJson = file_get_contents($strUrl);
$arrRequest = json_decode($strJson, true);
$arrNvmList = @$arrRequest['content']['company'];

$arrEscRequest = array();
if (is_array($arrNvmList) && (count($arrNvmList) > 0)) {
    foreach ($arrNvmList as $intKey => $arrEscRaw) {
        $arrEscRequest[$arrEscRaw['id']] = $arrEscRaw;
    }
}


$strUrl = 'https://www.izs-institut.de/api/negativ/?active=1'; //to=' .date('Y-m-d') .'
$strJson = file_get_contents($strUrl);
$arrRequest = json_decode($strJson, true);
$arrNvmList = $arrRequest['content']['negativ'];

//print_r($arrNvmList); die();

//$arrNvmDeteilList = array();

//print_r($arrNvmList);

$arrVerRequest = array();
if (count($arrNvmList) > 0) {

    $arrTableHeadNvm = array(
      '',
      '',
      'Meldestelle',
      'Typ',
      'Auskunftsgeber', 
      'Btnr/MitglNr',
      'Merkmal', 
      'Beginn',
      'Ende',
      'Info'
    );

    $arrFilter = [];

    foreach ($arrNvmList as $intKey => $arrNegativRaw) {

        //print_r($arrNegativRaw); break;

        $strChat = '';
        if ($arrNegativRaw['chat'] != '') {
          $strChat = '<a href="' .$arrNegativRaw['chat'] .'" target="_blank" style="margin-right: 5px;"><img src="img/smartgroups.png" height="20""></a>';
        }

        $strBox = '';
        if ($arrNegativRaw['box'] != '') {
          $strBox = '<a href="' .$arrNegativRaw['box'] .'" target="_blank"><img src="img/boxcom.png" height="20"></a>';
        }

        if (!in_array($arrNegativRaw['verid'], $arrVerRequest)) {
            $arrVerRequest[] = $arrNegativRaw['verid'];
        }
        
        $arrNvmDeteilList[] = array(
          $strChat,
          $strBox,
          '<span class="hidden">' .$arrNegativRaw['ver'] .' - ' .$arrNegativRaw['mel'] .'</span><span class="nvm_detail ver_' .$arrNegativRaw['verid'] .'">' .$arrNegativRaw['mel'] .'</span>',
          $arrNegativRaw['iptype'],
          $arrNegativRaw['ip'],
          ($arrNegativRaw['iptype'] == 'Berufsgenossenschaft') ? $arrNegativRaw['mgnr'] : $arrNegativRaw['btnr'],
          $arrNegativRaw['ntype'],
          date('d.m.Y', $arrNegativRaw['from']),
          ($arrNegativRaw['to'] != 0) ? date('d.m.Y', $arrNegativRaw['to']) : '',
          $arrNegativRaw['info'],
        ); 

        if (!isset($arrFilter[$arrNegativRaw['verid']]['art'])) {
          $arrFilter[$arrNegativRaw['verid']]['art'] = [];
        }
        if (!isset($arrFilter[$arrNegativRaw['verid']]['typ'])) {
          $arrFilter[$arrNegativRaw['verid']]['typ'] = [];
        }
        
        if ((!in_array($arrNegativRaw['iptype'], $arrFilter[$arrNegativRaw['verid']]['art'])) && (!in_array($arrNArt[$arrNegativRaw['iptype']], $arrFilter[$arrNegativRaw['verid']]['art']))) {
          $arrFilter[$arrNegativRaw['verid']]['art'][] = $arrNArt[$arrNegativRaw['iptype']];
        }
        
        if ((!in_array($arrNegativRaw['ntype'], $arrFilter[$arrNegativRaw['verid']]['typ'])) && (!in_array($arrNTyp[$arrNegativRaw['ntype']], $arrFilter[$arrNegativRaw['verid']]['typ']))) {
          $arrFilter[$arrNegativRaw['verid']]['typ'][] =  $arrNTyp[$arrNegativRaw['ntype']];
        }

    }
}

$arrPostData = http_build_query(array('id' => $arrVerRequest));
$arrOpt = array(
    'http' => array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/x-www-form-urlencoded',
        'content' => $arrPostData
    )
);
$resContext = stream_context_create($arrOpt);
$strJson    = file_get_contents('https://www.izs-institut.de/api/ver/', false, $resContext);
$arrRequest = json_decode($strJson, true);
$arrVerListRaw = $arrRequest['content']['company'];

//print_r($arrVerListRaw); die();

$boolShowHead = true;
$boolShowSide = false;

$strPageTitle = 'Aktuelle Negativmerkmale / Eskalationsfälle';


$arrTableHead = array(
  'Esc',
  'Gesamt',
  'Name', 
  'Doku', 
  'SV',
  'BG',
  //'Hinweis öffentliches Portal',
  'Hinweis IZS Portal'
);

$arrVerList = array();
$arrDocListExport = array();
$arrEscDone = array();

$arrJs = array();

foreach ($arrVerListRaw as $intRow => $arrVer) {

  if ($_SESSION['id'] == 3) {
    //print_r($arrDocument);
  }

    $strEscalation = '';
    if (isset($arrEscRequest[$arrVer['id']])) {
      $arrVer+= $arrEscRequest[$arrVer['id']];
      $strEscalation = '<a target="_blank" href="' .$arrVer['esc_link'] .'" title="' .$arrVer['esc_reason'] .'" alt="' .$arrVer['esc_reason'] .'" style="color:orangered;"><i class="fa fa-exclamation-triangle"></i></a>';
      $arrEscDone[] = $arrVer['id'];
    }

    $strFilterKeySv = 'sv' .$arrTrafficKey[$arrVer['status_sv']];
    if (!isset($arrFilterSection['rs'][$strFilterKeySv])) {
      $arrFilterSection['rs'][$strFilterKeySv] = $arrTrafficGerman[$arrVer['status_sv']];
    } 
    
    $strFilterKeyBg = 'bg' .$arrTrafficKey[$arrVer['status_bg']];
    if (!isset($arrFilterSection['rb'][$strFilterKeyBg])) {
      $arrFilterSection['rb'][$strFilterKeyBg] = $arrTrafficGerman[$arrVer['status_bg']];
    } 

    $strFilterKeyDc = 'dc' .$arrTrafficKey[$arrVer['status_doc']];
    if (!isset($arrFilterSection['rd'][$strFilterKeyDc])) {
      $arrFilterSection['rd'][$strFilterKeyDc] = $arrTrafficGerman[$arrVer['status_doc']];
    } 
    
    $strFilterKeyEsc = 'es' .($strEscalation != '');
    if (!isset($arrFilterSection['es'][$strFilterKeyEsc])) {
      $arrFilterSection['es'][$strFilterKeyEsc] = $arrEscGerman[($strEscalation != '')];
    } 

    $strFilter = $strFilterKeySv .' ' .$strFilterKeyBg .' ' .$strFilterKeyDc .' ' .$strFilterKeyEsc .' ' .implode(' ', $arrFilter[$arrVer['id']]['art']) .' ' .implode(' ', $arrFilter[$arrVer['id']]['typ']);

    $arrVerList[] = array(
        $strEscalation,
        $arrTraffic[$arrVer['status_all']],
        '<span class="negativ" style="cursor: pointer;" rel="ic_' .$arrVer['id'] .'">' .$arrVer['name'] .'</span>' .'<span class="hidden hfilter ' .$strFilter .'"></span>',
        $arrTraffic[$arrVer['status_doc']],
        $arrTraffic[$arrVer['status_sv']],
        $arrTraffic[$arrVer['status_bg']],
        //$arrVer['note_public'],
        $arrVer['note_lender'],
    );

}  

//print_r($arrEscRequest); die();

if (count($arrEscRequest) > 0) {
  foreach ($arrEscRequest as $intRow => $arrVer) {

      if (in_array($arrVer['id'], $arrEscDone)) continue;

      $strFilterKeyEsc = 'es1';
      if (!isset($arrFilterSection['es'][$strFilterKeyEsc])) {
        $arrFilterSection['es'][$strFilterKeyEsc] = $arrEscGerman[1];
      } 
  
      $arrVerList[] = array(
          '<a target="_blank" href="' .$arrVer['esc_link'] .'" title="' .$arrVer['esc_reason'] .'" alt="' .$arrVer['esc_reason'] .'" style="color:orangered;"><i class="fa fa-exclamation-triangle"></i></a>',
          $arrTraffic[$arrVer['status_all']],
          $arrVer['name'] .'<span class="hidden hfilter ' .$strFilterKeyEsc .'"></span>',
          $arrTraffic[$arrVer['status_doc']],
          $arrTraffic[$arrVer['status_sv']],
          $arrTraffic[$arrVer['status_bg']],
          //$arrVer['note_public'],
          $arrVer['note_lender'],
      );
  }  
}

//print_r($arrVerList); die();

$arrOpt = array(
    'boolHasContext' => true,
    //'arrNoSort' => array(0),
    'strOrder' => '"order": [[ 2, "asc" ]]'
);

$strDocTable = strCreateTable($arrVerList, $arrTableHead, '', false, true, array(), $arrOpt);

if (true) {

}


$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading col-lg-12">
        <h3 class="panel-title"><span id="DocCount">' .count($arrVerList) .'</span> Negativmerkmale / Eskalationsfälle gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strDocTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

if (count($arrNvmDeteilList) > 0) {

  $arrOpt = array(
    'boolHasContext' => true,
    'arrNoSort' => array(0, 1),
    'strOrder' => '"order": [[ 2, "asc" ]]'
  );

  $strNvmTable = strCreateTable($arrNvmDeteilList, $arrTableHeadNvm, '', false, true, array(), $arrOpt);

  $strIncOutput.= '
                          <!-- Modal -->
                          <div class="modal fade" id="modalNegativ" aria-hidden="true" aria-labelledby="ChangelogSidebar"
                          role="dialog" tabindex="-1">
                            <div class="modal-dialog modal-center" style="width: 1350px;">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">x</span>
                                  </button>
                                  <h4 class="modal-title nvm">Negativmerkmale</h4>
                                </div>
                                <div class="modal-body">
                                  ' .$strNvmTable .'
                                </div>
                                <div class="modal-footer">
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- End Modal -->
  ';

}


$strJsFootCodeRun.= '

$(".negativ").on("click", function(e) {

  var obj = $(this);
  var hid = obj.attr("rel");

  var id = hid.split("_")[1];

  $("h4.nvm").append(" - " + $(this).text());
  $(".nvm_detail").parent().parent().hide();
  $(".ver_" + id).parent().parent().show();

  $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();

  $("#modalNegativ").modal("show");
});

$(document).on("shown.bs.modal", function (e) {
  $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
});

$(document).on("hidden.bs.modal", function (e) {
  $(".nvm_detail").parent().parent().show();
  $("h4.nvm").text("Negativmerkmale");
});


$("#doXlsxExport").on("click", function(e) {

  var header = new Array();
  var list   = new Array();

  $.each($(".modal-body table[id^=\"table_\"] thead th"), function () {
      //if ($(this).text() != "") header.push($(this).text());
      header.push($(this).text());
  });

  $.each($(".modal-body table[id^=\"table_\"] tbody tr"), function () {
      var tdarr = new Array();
      $.each($(this).find("td"), function () {
          //if ($(this).children(":first").length == 0) tdarr.push($(this).text());
          var text = $(this).text();
          if (text != "") {
            tdarr.push(text);
          } else {
            tdarr.push($(this).children().attr("href"));
          }
      });
      list.push(tdarr);
  });

  //console.log(header);
  //console.log(list);

  var fn = "' .str_replace(' ', '_', str_replace('/', '_', $strPageTitle)) .'_" + $.format.date(new Date(), "yyyyMMdd_Hmmss") + ".xlsx";

  ///*
  $.post("inc/negativ.ajax.php", { 
      ac: "export", list: list, header: header, format: "xlsx", name: fn
    }).done(function(data) {
      location.href="/cms/files/temp/" + fn;
    }
  );
  //*/
  
});

';

$strJsFootCodeRun.= "

";


?>