<?php

//REQUEST


//DATA

$objContact = new Contact;

$objIP = new InformationProvider;
$objPP = new PremiumPayer;

$objEvent = new Event;
$objGroup = new Group;

$objAnfragestelle = new Anfragestelle;


//SETTINGS

$boolShowHead = false;
$boolShowSide = false;

$strName = 'Offene Klärungsfälle';

$strPageTitle = $strName;

//CONTENT

$strOutput = '
  <!-- Page -->
  <div class="animsition container-fluid">
';

$strOutput.= strCreatePageTitle($strName);

$strOutput.= '
    <div class="page-content">
';


$arrEventList = $objEvent->arrGetListKlaerung();

$arrTableHead = array(
  'AP IP-> PP',
  'Anrede', 
  'AP Tel.',
  'Monat', 
  'Event-ID', 
  'Premium Payer', 
  'BNR PP', 
  'Company Group', 
  'Anfragestelle', 
  'Status', 
  'Grund', 
  'Nächster Schritt', 
  'Wiedervorlage', 
  'Info', 
  'AP Klärung', 
  'Tel. Klärung'
);

$arrEventListFormatted = array();

foreach ($arrEventList as $intRow => $arrRow) {
  $arrPp = $objPP->arrGetPPFromBetriebsnummer($arrRow['ev_ac_betriebsnummer']);
  
  $arrContact = $objContact->arrGetContactListFromIpPp($arrRow['ev_ip_acid'], $arrPp[0]['acid']);
  
  if (count($arrContact) > 0) {

    $arrGroup = $objGroup->arrGetGroup($arrRow['ev_grid']);
    $arrAnfragestelle = $objAnfragestelle->arrGetAnfragestelleFromEvent($arrRow['evid']);
    
    if (count($arrAnfragestelle) == 0) {
      $arrAnfragestelle[0]['as_name'] = '';
    }
    
    $strName = strConcatName($arrContact[0]['co_first'], $arrContact[0]['co_last'], $arrContact[0]['co_title']);
  
    $arrEventListFormatted[] = array(
      'AP IP-> PP' => $strName, 
      'Anrede' => $arrContact[0]['co_salut'], 
      'AP Tel' => $arrContact[0]['co_phone'], 
      'Monat' => $arrRow['ev_year'] .'/' .str_pad($arrRow['ev_month'], 2, '0', STR_PAD_LEFT), 
      'Event-ID' => $arrRow['ev_name'], 
      'Premium Payer' => $arrPp[0]['ac_name'], 
      'BNR PP' => $arrRow['ev_ac_betriebsnummer'], 
      'Company Group' => $arrGroup[0]['gr_name'], 
      'Anfragestelle' => $arrAnfragestelle[0]['as_name'], 
      'Status' => $arrRow['ev_status'], 
      'Grund' => $arrRow['ev_grund'], 
      'Nächster Schritt' => $arrRow['ev_meilenstein'], 
      'Wiedervorlage' => strFormatDate($arrRow['ev_bis'], 'd.m.Y'), 
      'Info' => $arrRow['ev_info'], 
      'AP Klärung' => $arrRow['ev_ap'], 
      'Tel. Klärung' => $arrRow['ev_tel']
    );
  
  }
}

$strContactTable = strCreateTable($arrEventListFormatted, $arrTableHead, '', false, true);

$strOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title">Offene Klärungsfälle</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strOutput.= $strContactTable;

$strOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';


$strOutput.= '
    </div>
  </div>
  <!-- End Page -->
';

?>