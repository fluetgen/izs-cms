<?php

$arrFilter = array();

$arrFilter[] = array(
  'strKey' => 'be_status_bearbeitung',
  'strType' => '=',
  'strValue' => 'erhalten - verarbeitbar'
);

$arrEventList = $objBgEvent->arrGetBgEventList($arrFilter);

$arrFilter = array();

$arrFilter[] = array(
  'strKey' => 'be_datum_rueckmeldung',
  'strType' => '!=',
  'strValue' => '0000-00-00'
);

$arrFilter[] = array(
  'strKey' => 'be_status_bearbeitung',
  'strType' => '=',
  'strValue' => 'angefragt'
);

$arrEventList = $objBgEvent->arrGetBgEventList($arrFilter);

if (@$_SERVER['HTTP_FKLD'] == 'on') {
  //print_r($arrEventList2);
}

$arrBgNameList = array();
if (count($arrBgListEvent) > 0) {
  foreach ($arrBgListEvent as $intKey => $arrBg) {
    $arrBgNameList[$arrBg['acid']] = $arrBg['ac_name'];
  }
}

$arrMeldestellen = $objBg->arrGetAllMeldestellen();

$arrMeldestellenNameList = array();
if (count($arrMeldestellen) > 0) {
  foreach ($arrMeldestellen as $intKey => $arrMeldestelle) {
    $arrMeldestellenNameList[$arrMeldestelle['Id']] = $arrMeldestelle['Name'];
  }
}


$arrTableHead = array(
  'ID',
  'Berufsgenossenschaft',
  'Meldestelle',
  'DokTyp',
  'Aktion'
);


$arrStatusBearbeitung = $objBgEvent->arrGetSelection('be_status_bearbeitung');
$arrErgebnis = $objBgEvent->arrGetSelection('be_ergebnis');
$arrStatusKlaerung = $objBgEvent->arrGetSelection('be_klaerung_status');
$arrGrundKlaerung = $objBgEvent->arrGetSelection('be_klaerung_grund');

$arrBefristet = array(
  'noch nicht fällig',
  'fällig',
  'ohne Befristung'
);

$arrRueckmeldung = array(
  'nicht erhalten',
  'erhalten'
);

$arrList = array();
foreach ($arrEventList as $intRow => $arrEvent) {
  
  $strBesfistet = '';
  if ($arrEvent['be_befristet'] != '0000-00-00') {
    $strBesfistet = strConvertDate($arrEvent['be_befristet'], 'Y-m-d', 'd.m.Y');
  }

  $strRueckmeldung = '';
  if ($arrEvent['be_datum_rueckmeldung'] != '0000-00-00') {
    $strRueckmeldung = strConvertDate($arrEvent['be_datum_rueckmeldung'], 'Y-m-d', 'd.m.Y');
  }
  
  $strSperrvermerk = 'nein';
  if ($arrEvent['be_sperrvermerk'] != 0) {
    $strSperrvermerk = 'ja';
  }

  $strDokumentArt = '';
  if (($arrEvent['be_dokument_art'] == '') || ($arrEvent['be_dokument_art'] == 'Unbedenklichkeitsbescheinigung (UBB)')){
    $strDokumentArt = 'UBB';
  }
    
   $strClassAdd = '';
  
  $strClassAdd.= ' y_' .$arrEvent['be_jahr'];
  if (!isset($arrFilterSection['y'][$arrEvent['be_jahr']])) {
    $arrFilterSection['y'][$arrEvent['be_jahr']] = $arrEvent['be_jahr'];
  }

  $strClassAdd.= ' bg_' .$arrEvent['be_bg'];
  if (!isset($arrFilterSection['bg'][$arrEvent['be_bg']])) {
    $arrFilterSection['bg'][$arrEvent['be_bg']] = $arrBGList[$arrEvent['be_bg']];
  }
  
  $strMeldestelle = @$arrMeldestellenNameList[$arrEvent['be_meldestelle']];
  if ($strMeldestelle == '') {
    $arrMeldestelle = $objPp->arrGetPP($arrEvent['be_meldestelle'], false);
    $strMeldestelle = $arrMeldestelle[0]['ac_name'];
  }
  
  $strClassAdd.= ' pp_' .$arrEvent['be_meldestelle'];
  if (!isset($arrFilterSection['pp'][$arrEvent['be_meldestelle']])) {
    $arrFilterSection['pp'][$arrEvent['be_meldestelle']] = $strMeldestelle;
  }
  
  $intSb = array_search($arrEvent['be_status_bearbeitung'], $arrStatusBearbeitung);
  $strClassAdd.= ' sb_' .$intSb;
  if (!isset($arrFilterSection['sb'][$intSb]) && ($intSb !== false)) {
    //$arrFilterSection['sb'][$intSb] = $arrEvent['be_status_bearbeitung'];
  }
  
  $intEr = array_search($arrEvent['be_ergebnis'], $arrErgebnis);
  $strClassAdd.= ' er_' .$intEr;
  if (!isset($arrFilterSection['er'][$intEr]) && ($intEr !== false)) {
    $arrFilterSection['er'][$intEr] = $arrEvent['be_ergebnis'];
  }
  
  $intBefristet = 2;
  if ($arrEvent['be_befristet'] != '0000-00-00') { 
    if (strtotime($arrEvent['be_befristet']) < strtotime(date('Y-m-d'))) {
      $intBefristet = 1;
    } else {
      $intBefristet = 0;
    }
  }
  $strClassAdd.= ' bb_' .$intBefristet;
  if (!isset($arrFilterSection['bb'][$intBefristet])) {
    $arrFilterSection['bb'][$intBefristet] = $arrBefristet[$intBefristet];
  }
   
  $intSk = array_search($arrEvent['be_klaerung_status'], $arrStatusKlaerung);
  $strClassAdd.= ' sk_' .$intSk;
  if (!isset($arrFilterSection['sk'][$intSk]) && ($intSk !== false)) {
    //$arrFilterSection['sk'][$intSk] = $arrEvent['be_klaerung_status'];
  }
   
  $intGk = array_search($arrEvent['be_klaerung_grund'], $arrGrundKlaerung);
  $strClassAdd.= ' gk_' .$intGk;
  if (!isset($arrFilterSection['gk'][$intGk]) && ($intGk !== false)) {
    //$arrFilterSection['gk'][$intGk] = $arrEvent['be_klaerung_grund'];
  }
  
  $intRueckmeldung = 0;
  if ($arrEvent['be_datum_rueckmeldung'] != '0000-00-00') { 
    $intRueckmeldung = 1;
  }
  $strClassAdd.= ' ru_' .$intRueckmeldung;
  if (!isset($arrFilterSection['ru'][$intRueckmeldung])) {
    //$arrFilterSection['ru'][$intRueckmeldung] = $arrRueckmeldung[$intRueckmeldung];
  }

  $arrList[] = array(//
    'ID' => '<a href="/cms/index_neu.php?ac=bg_detail&beid=' .$arrEvent['beid'] .'" target="_blank">' .$arrEvent['be_name'] .'</a><span class="hidden hfilter' .$strClassAdd .'">',
    //'Berufsgenossenschaft' => $arrBgNameList[$arrEvent['be_bg']],
    'Berufsgenossenschaft' => '<a href="/cms/index_neu.php?ac=contacts&acid=' .$arrEvent['be_bg'] .'" target="_blank">' .$arrBGList[$arrEvent['be_bg']] .'</a>',
    'Meldestelle' => $strMeldestelle,
    'DokArt' => $strDokumentArt,
    'Aktion' => '<a href="javascript: void(0);" class="zu" rel="' .$arrEvent['beid'] .'" id="zu_' .$arrEvent['beid'] .'">Dokument zuordnen</a>'
  );

}

$strBgTable = strCreateTable($arrList, $arrTableHead, '', false, true);

$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Events für Berufsgenossenschaften gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strBgTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
    


                        <!-- Modal -->
                        <div class="modal fade" id="Auskunft" aria-hidden="true" aria-labelledby="Auskunft"
                        role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-center">
                            <input type="hidden" name="beid" id="beid" value="">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">x</span>
                                </button>
                                <h4 class="modal-title">Auskunft erfassen</h4>
                              </div>
                              <div class="modal-body">
                                <p>Body.</p>
                              </div>
                              <div class="modal-footer">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Modal -->

                        <!-- Modal -->
                        <div class="modal fade modal-danger" id="ModalDanger" aria-hidden="true"
                        aria-labelledby="ModalDanger" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-center smallw">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Achtung</h4>
                              </div>
                              <div class="modal-body">
                                <p id="errMessage">Text</p>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Modal -->
    


';


//$strJsFootScript.= '  <script src="js/script_neu.js"></script>' .chr(10);

/*
wi_    "`be_wiedervorlage`", 
gr_    "`be_klaerung_grund`", 
ns_    "`be_klaerung_schritt`", 
ap_    "`be_klaerung_ap`", 
at_    "`be_klaerung_telefon`"
*/

$strJsFootCodeRun.= '

    $("#Auskunft").on("shown.bs.modal", function() {
      //$(".dp").datepicker();
    });

    $(".zu").click(function(){
      var id = $(this).attr("rel");
      $("#beid").val(id);
      $("#Auskunft").modal("show");
    });

    $("#Auskunft").on("click", ".dp", function () { 
      //$(this).datepicker();
    });

    $(document).on("dragenter", "#open_btn", function() {
        $.FileDialog({
          multiple: false,
          dropheight: 300,
        }).on("files.bs.filedialog", function(ev) {
          
          $(".loading").css("display", "block");
          
          var files = ev.files;

          var data = new FormData();
          for (var i = 0; i < files.length; i++) {
              data.append(files[i].name, files[i]);
          }
          $.ajax({
              url: "action/upload.php",
              type: "POST",
              data: data,
              contentType: false,
              processData: false,
              success: function (result) {

                $("#fileList").html(result);
                $("#be_link").val(result);

              },
              error: function (err) {
                  alert(err.statusText)
              }
          });
        }).on("cancel.bs.filedialog", function(ev) {
            //$("#output").html("Cancelled!");
        });
    });

    $(document).on("dragleave", "#fdfd", function() {
      console.log("leave");
    });

    $(document).on("mouseout", "#fdfd", function() {
      console.log("out");
    });

  //$(document).on("click", ".dp", function(){
  //  $(this).datepicker();
  //});

  $(document).on("click", "#svAuskunft", function(){
  
    var beid = $("#beid").val();
    var spv = 0;
    
    if ($("#sp_" + beid).is(":checked")) {
      spv = 1;
    }

    var arrFields = {
      be_auskunftsgeber:   $("#ag_" + beid).val(),
      be_befristet:        $("#be_" + beid).val(),
      be_sperrvermerk:     spv,
      be_bemerkung:        $("#bs_" + beid).val(),
      be_sichtbar:         $("#si_" + beid).val(),
      be_link:             $("#be_link").val(),
      be_ergebnis:         $("#er_" + beid).val(),
      be_dokument_art:     $("#da_" + beid).val(),
    };

    $.ajax({
      type: "POST",
      url:  "action/ajax.php",
      dataType: "text", 
      data: { ac: "svAuskunftClick", beid: beid, values: arrFields },
      success: function(result) {
        var jsonData = $.parseJSON(result);
        
        if (jsonData["Status"] == "OK") {
          

        	$("#Auskunft").modal("hide");
        	$("#zu_" + jsonData["beid"]).parent().parent().hide();
          $("#DocCount").html(parseInt($("#DocCount").html()) - 1);

        	//location.reload();
              
        }
        
        if (jsonData["Status"] == "FAIL") {

          $("#ModalDanger").modal("show");
          $("#ModalDanger #errMessage").html(jsonData["Reason"]);

        }

      }

    });

  });

  $("#Auskunft").on("show.bs.modal", function(e) {
      var beid = $("#beid").val();
      $(this).find(".modal-content").load("action/modal.php?ac=auskunft&beid=" + beid);
  });


';


?>