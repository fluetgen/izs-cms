<?php

$strSql = 'SELECT `Group__c`.`Id` AS `GId`, `Group__c`.`Name` AS `GName`, `Vertrag__c`.*  FROM `Vertrag__c` INNER JOIN `Group__c` ON `Company_Group__c` = `Group__c`.`Id` WHERE `Aktiv__c` = "true" ORDER BY `GName`';
$arrGroupList = MySQLStatic::Query($strSql);
$intCountResult = count($arrGroupList);


$arrTableHead = array(
  'Personaldienstleister',
  'Mitgliedsmodell',
  'tatsächlich angeliefert',
  'vereinbart',
  'Abweichung',
  'in %'
);

$arrSumReal = array();
$strSql = 'SELECT SUM(`is_datasets`) AS `sum`, `import_stats`.* FROM `import_stats` WHERE `is_date` = "' .($intYear .'-' .$intMonth .'-01') .'" GROUP BY `is_ilid`';
$arrEventSum = MySQLStatic::Query($strSql);
$intEventSum = count($arrEventSum);
if ($intEventSum > 0) {
  foreach ($arrEventSum as $intKey => $arrEventSum) {

    if ($arrEventSum['is_grid'] == '') {
      $strSqlGr = 'SELECT `is_grid` FROM `import_stats` WHERE `is_grid` != "" AND `is_ilid` = "' .$arrEventSum['is_ilid'] .'"';
      $arrSqlGr = MySQLStatic::Query($strSqlGr);
      if (count($arrSqlGr) > 0) {
        $arrEventSum['is_grid'] = $arrSqlGr[0]['is_grid'];
      }
    }

    if (isset($arrSumReal[$arrEventSum['is_grid']])) {
      $arrSumReal[$arrEventSum['is_grid']]+= $arrEventSum['sum'];
    } else {
      $arrSumReal[$arrEventSum['is_grid']] = $arrEventSum['sum'];
    }
  }
}

$strSelect = '';
$strSql = 'SELECT DISTINCT(`is_date`) AS `is_date` FROM `import_stats` ORDER BY `is_date` DESC';
$arrAvailMonthList = MySQLStatic::Query($strSql);
$intMonthSum = count($arrAvailMonthList);
if ($intMonthSum > 0) {
  $strSelect.= '<form action="/cms/index_neu.php?ac=invoice" method="post"><label for="seldate" style="margin-right: 20px;">Anlieferungsmonat</label><select name="seldate" id="seldate">' .chr(10);
  foreach ($arrAvailMonthList as $intKey => $strMonth) {
    
    $intDate = strtotime($strMonth['is_date']);
    //$strNextMonth = date('Y-m', mktime(0, 0, 0, date('n', $intDate) + 1, date('j', $intDate), date('Y', $intDate)));
    
    $strSelected = '';
    if (date('Y-m', $intDate) == @$_REQUEST['seldate']) {
      $strSelected = ' selected="selected"';  
    }
    
    $strSelect.= '<option value="' .date('Y-m', $intDate) .'"' .$strSelected .'>' .date('Y-m', $intDate) .'</option>' .chr(10);
  }  
  $strSelect.= '</select></form>' .chr(10);
}


$arrList = array();
foreach ($arrGroupList as $intRow => $arrGroup) {
  
  /*
  preg_match("/(\d{1,2})/", $arrGroup['Abrechnungszeitraum__c'], $arrMonth);
  $intMonth = $arrMonth[0];
  */
  
  $intCountContract = ($arrGroup['Anzahl_Datens_tze_Quartal__c'] / 3);
  $intCountReal = @$arrSumReal[$arrGroup['Company_Group__c']];
  $intDiff = $intCountReal - round($intCountContract);
  if (round($intCountContract) != 0) { 
    $intPercent = round(($intDiff / round($intCountContract)) * 100);
  } else {
    $intPercent = 0;
  }
  
  $strAddClass = '';
  if ($intPercent > 0) {
    if ($intPercent < 10) $strAddClass = 'warning';
    else $strAddClass = 'danger';
  }
  
  $arrList[] = array(
    'ppayer'   => $arrGroup['GName'] .'<span class="hidden ' .$strAddClass .'"></span>',
    'model'    => $arrModelList[$arrGroup['GId']],
    'count'    => $intCountReal,
    'contract' => round($intCountContract), 
    'diff'     => $intDiff, 
    'percent'  => $intPercent .'%'
  );
}

$strTable = strCreateTable($arrList, $arrTableHead, '', false, true);

$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Verträge gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strSelect;

$strIncOutput.= $strTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

$strJsFootCodeRun.= '

$("span.warning").parent().parent().addClass("warning");
$("span.danger").parent().parent().addClass("danger");

$("#seldate").on("change", function (e) {
  this.form.submit();
});

';


?>