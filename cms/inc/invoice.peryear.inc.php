<?php

$strSql = 'SELECT `Group__c`.`Id`  AS `GId`, `Group__c`.`Name` AS `GName`, `Vertrag__c`.*  FROM `Vertrag__c` INNER JOIN `Group__c` ON `Company_Group__c` = `Group__c`.`Id` WHERE `Aktiv__c` = "true" ORDER BY `GName`';
$arrGroupList = MySQLStatic::Query($strSql);
$intCountResult = count($arrGroupList);


$arrTableHead = array(
  '',
  'Mitgliedsmodell',
  'Jan',
  'Febr',
  'März',
  'Apr',
  'Mai',
  'Juni',
  'Juli',
  'Aug',
  'Sept',
  'Okt',
  'Nov',
  'Dez'
);

function arrGetDeliveredEvents ($intGrId = 0, $strMonth = '', $intMonthCount = 0) {
  
  $arrReturn = array(
    'intCount' => 0,
    'intMissing' => ($intMonthCount + 1)
  );
  
  $intDate = strtotime($strMonth);
  $strMonthStop = date('Y-m', mktime(0, 0, 0, date('n', $intDate) - $intMonthCount, date('j', $intDate), date('Y', $intDate))) .'-01';

  
  $arrSumReal = array();
  $strSql = 'SELECT `is_ilid` FROM `import_stats` WHERE `is_date` <= "' .$strMonth .'" AND `is_date` >= "' .$strMonthStop .'" AND `is_grid` = "' .$intGrId .'" GROUP BY `is_ilid`';
  $arrFileList = MySQLStatic::Query($strSql);
  $intFileList = count($arrFileList);
  
  $arrFileRequest = array();
  if ($intFileList > 0) {
    foreach ($arrFileList as $intKey => $arrFile) {
      $arrFileRequest[] = $arrFile['is_ilid'];
    }
  } 
  
  $strFileRequest = implode(', ', $arrFileRequest);
  
  if ($strFileRequest != '') {
  
    $strSql = 'SELECT SUM(`is_datasets`) AS `sum`, `is_date` FROM `import_stats` WHERE `is_ilid` IN (' .$strFileRequest .') GROUP BY `is_date`';
    $arrDatasetCount = MySQLStatic::Query($strSql);
    $intDatasetCount = count($arrDatasetCount);
    
    $arrReturn['intMissing'] = ($intMonthCount + 1) - $intDatasetCount;
    
    foreach ($arrDatasetCount as $intKey => $arrMonth) {
      $arrReturn['intCount']+= $arrMonth['sum'];
    }
  
  }
  
  return $arrReturn;
}

$strSelect = '';
$strSql = 'SELECT YEAR(`is_date`) AS `year` FROM `import_stats` GROUP BY `year` ORDER BY `year` DESC';
$arrAvailMonthList = MySQLStatic::Query($strSql);
$intMonthSum = count($arrAvailMonthList);
if ($intMonthSum > 0) {

  if (!isset($_REQUEST['seldate'])) {
    $_REQUEST['seldate'] = date('Y');
  } 

  $strSelect.= '<form action="/cms/index_neu.php?ac=invoice&fu=peryear" method="post"><label for="seldate" style="margin-right: 20px;">Jahr</label><select name="seldate" id="seldate">' .chr(10);
  foreach ($arrAvailMonthList as $intKey => $arrMonth) {
    
    $strSelected = '';
    if ($arrMonth['year'] == @$_REQUEST['seldate']) {
      $strSelected = ' selected="selected"';
      $intYear = $_REQUEST['seldate'];
    }
    
    $strSelect.= '<option value="' .$arrMonth['year'] .'"' .$strSelected .'>' .$arrMonth['year'] .'</option>' .chr(10);
  }  
  $strSelect.= '</select></form>' .chr(10);
}


$arrList = array();
foreach ($arrGroupList as $intRow => $arrGroup) {

  preg_match("/(\d{1,2})/", $arrGroup['Abrechnungszeitraum__c'], $arrMonth);
  $intMonthCount = $arrMonth[0];
  
  $intCountContract = $arrGroup['Anzahl_Datens_tze_Quartal__c'];
  
  $arrMonth    = array();
  $arrMonth[-1] = $arrGroup['GName'];
  $arrMonth[0] = $arrModelList[$arrGroup['GId']];
  
  for ($intMonth = 1; $intMonth <= 12; $intMonth++) {
    
    $intDate = strtotime($intYear .'-' .$intMonth .'-01');
    $strDate = date('Y-m', mktime(0, 0, 0, date('n', $intDate), date('j', $intDate), date('Y', $intDate))) .'-01';
  
    $arrCountReal = arrGetDeliveredEvents($arrGroup['Company_Group__c'], $strDate, 0);
    $intCountReal = $arrCountReal['intCount'];
    
    $intDiff = $intCountReal - round($intCountContract * $intMonthCount / 3);
    if (round($intCountContract) != 0) { 
      $intPercent = round(($intDiff / round($intCountContract)) * 100);
    } else {
      $intPercent = 0;
    }
    
    $strAddClass = '';
    if ($intPercent > 0) {
      if ($intPercent < 10) $strAddClass.= 'warning';
      else $strAddClass.= 'danger';
    }
    
    if ($intCountReal == 0) $intCountReal = '';
    
    $arrMonth[$intMonth] = $intCountReal;
  
  }
  
  $arrList[] = $arrMonth;
  
}

//print_r($arrList); die();

$strTable = strCreateTable($arrList, $arrTableHead, '', false, true);

$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Verträge gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strSelect;

$strIncOutput.= $strTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

$strJsFootCodeRun.= '

$("span.warning").parent().parent().addClass("warning");
$("span.danger").parent().parent().addClass("danger");

$("#seldate").on("change", function (e) {
  this.form.submit();
});

';


?>