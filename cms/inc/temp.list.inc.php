<?php

//REQUEST

$intType  = @$_REQUEST['ty'];

if (!isset($_REQUEST['fu'])) {
  $_REQUEST['fu'] = '';
}

$strName = '';
$strDataNamePart = '';

$strSql = 'SELECT * FROM `cms_text` WHERE `ct_type` = "' .$intType .'"';
$arrTemplateList = MySQLStatic::Query($strSql);

//SETTINGS

$boolShowHead = false;
$boolShowSide = false;

$strPageTitle = 'E-Mail Vorlagen';

//CONTENT

$strCssHead.= '
<style>
.nav-tabs > li > a {
  -moz-border-bottom-colors: none;
  -moz-border-left-colors: none;
  -moz-border-right-colors: none;
  -moz-border-top-colors: none;
  background-color: #f3f7f9;
  border-color: #f3f7f9 #f3f7f9 transparent;
  border-image: none;
  border-style: solid;
  border-width: 1px;
  color: #37474f;
}
</style>
';

$strOutput = '
  <!-- Page -->
  <div class="animsition container-fluid">
';

//$strOutput.= strCreatePageTitle('Kontakte', $arrIP[0]['ac_name']);

$arrTableHead = array(
    'Name',
    'Betreff',
    'Anfrageweg'
);

if (in_array($intType, array(1,4))) {
  unset($arrTableHead[2]);
}

$arrList = array();
foreach ($arrTemplateList as $intRow => $arrTemplate) {
    
  $arrTemp = array(
    'name'    => '<a href="index_neu.php?ac=' .$_REQUEST['ac'] .'&fu=' .$_REQUEST['fu'] .'&ty=' .$arrTemplate['ct_type'] .'&id=' .$arrTemplate['ct_id'] .'&clid=' .$_SESSION['id'] .'">' .$arrTemplate['ct_name'] .'</a>', // .'<span class="hidden ' .$strAddClass .'"></span>',
    'subject' => $arrTemplate['ct_head']
  );

  if (!in_array($intType, array(1,4))) {
    $arrTemp['request'] = $arrTemplate['ct_category'];
  }

  $arrList[] = $arrTemp;

}

$strTable = strCreateTable($arrList, $arrTableHead, '', false, true);

$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Vorlagen gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strTable;

$strIncOutput.= '         <button type="button" class="btn btn-primary" id="add" style="margin-top: 20px;">Hinzufügen</button>
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

$strJsFootCodeRun.= '
  
  $("#add").on("click", function(e) {

    location.href="index_neu.php?ac=' .$_REQUEST['ac'] .'&fu=' .$_REQUEST['fu'] .'&ty=' .$intType .'&id=&clid=' .$_SESSION['id'] .'";
    
  });  

';


?>