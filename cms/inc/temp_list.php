<?php

//REQUEST

$intType  = @$_REQUEST['ty'];

$strName = '';
$strDataNamePart = '';

$strSql = 'SELECT * FROM `cms_text` WHERE `ct_type` = "' .$intType .'"';
$arrTemplateList = MySQLStatic::Query($strSql);

//SETTINGS

$boolShowHead = false;
$boolShowSide = false;

$strPageTitle = '$strName';

//CONTENT

$strCssHead.= '
<style>
.nav-tabs > li > a {
  -moz-border-bottom-colors: none;
  -moz-border-left-colors: none;
  -moz-border-right-colors: none;
  -moz-border-top-colors: none;
  background-color: #f3f7f9;
  border-color: #f3f7f9 #f3f7f9 transparent;
  border-image: none;
  border-style: solid;
  border-width: 1px;
  color: #37474f;
}
</style>
';

$strOutput = '
  <!-- Page -->
  <div class="animsition container-fluid">
';

//$strOutput.= strCreatePageTitle('Kontakte', $arrIP[0]['ac_name']);

$arrTableHead = array(
    'Personaldienstleister',
    'tatsächlich angeliefert',
    'vereinbart',
    'Abweichung',
    'in %'
);

$arrList = array();
foreach ($arrTemplateList as $intRow => $arrTemplate) {
  
  /*
  preg_match("/(\d{1,2})/", $arrGroup['Abrechnungszeitraum__c'], $arrMonth);
  $intMonth = $arrMonth[0];
  */
  
  $intCountContract = ($arrGroup['Anzahl_Datens_tze_Quartal__c'] / 3);
  $intCountReal = @$arrSumReal[$arrGroup['Company_Group__c']];
  $intDiff = $intCountReal - round($intCountContract);
  if (round($intCountContract) != 0) { 
    $intPercent = round(($intDiff / round($intCountContract)) * 100);
  } else {
    $intPercent = 0;
  }
  
  $strAddClass = '';
  if ($intPercent > 0) {
    if ($intPercent < 10) $strAddClass = 'warning';
    else $strAddClass = 'danger';
  }
  
  $arrList[] = array(
    'ppayer'   => $arrGroup['GName'] .'<span class="hidden ' .$strAddClass .'"></span>',
    'count'    => $intCountReal,
    'contract' => round($intCountContract), 
    'diff'     => $intDiff, 
    'percent'  => $intPercent .'%'
  );
}

$strTable = strCreateTable($arrList, $arrTableHead, '', false, true);

$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Vorlagen gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

?>