<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


if (!isset($_REQUEST['grid'])) {
  $_REQUEST['grid'] = '';
}

$arrTableHead = array(
  'Zeitarbeitsunternehmen',
  'Liefert Daten',
  'Erzeugt am',
  'Erzeugt von',
  'Zurückgerufen',
  'Zurückgerufen am',
  'Zurückgerufen von',
  'Aktion'
);


$arrUserList = [];
$strSqlU = 'SELECT * FROM `cms_login`';
$arrSqlU = MySQLStatic::Query($strSqlU);
if (isset($arrSqlU) && (count($arrSqlU) > 0)) {
  foreach ($arrSqlU as $intRow => $arrUserRaw) {
    $arrUserList[$arrUserRaw['cl_id']] = $arrUserRaw['cl_first'] . ' ' .$arrUserRaw['cl_last'];
  }
}

$strSelect = '';
$strSql = 'SELECT `Group__c`.* FROM `Group__c` INNER JOIN `Vertrag__c` ON `Group__c`.`Id` = `Vertrag__c`.`Company_Group__c` WHERE `SF42_Group_Type__c` = "Lender" GROUP BY `Vertrag__c`.`Company_Group__c` ORDER BY `Group__c`.`Name`';
$arrPpListRaw = MySQLStatic::Query($strSql);
$intResult = count($arrPpListRaw);

if ($intResult > 0) {

  $strSelect.= '<form action="/cms/index_neu.php" method="GET">' .chr(10);

  $strSelect.= '<input type="hidden" name="ac" value="sp" />' .chr(10);
  $strSelect.= '<input type="hidden" name="fu" value="detail" />' .chr(10);

  $strSelect.= '<label for="grid" style="margin-right: 20px;">PDL</label>' .chr(10);
  $strSelect.= '<select name="grid" id="grid" onchange="this.form.submit()">' .chr(10);
  foreach ($arrPpListRaw as $intKey => $arrPp) {
    
    $strSelected = '';
    if ($arrPp['Id'] == $_REQUEST['grid']) {
      $strSelected = ' selected="selected"';
    }
    
    $strSelect.= '<option value="' .$arrPp['Id'] .'"' .$strSelected .'>' .$arrPp['Name'] .'</option>' .chr(10);
  }  
  $strSelect.= '</select></form>' .chr(10);

}

$strLink = '';

$strSql = 'SELECT * FROM `izs_subsidiaerprotect` INNER JOIN `Group__c` ON `Group__c`.`Id` = `pr_group_id` WHERE `pr_group_id` = "' .$_REQUEST['grid'] .'" ORDER BY `pr_group_id`';
$arrSql = MySQLStatic::Query($strSql);

if (isset($arrSql) && (count($arrSql) > 0)) {

  $arrList = array();
  foreach ($arrSql as $intRow => $arrGroup) {

      $strClassAdd = '';

      //Filter Zeitarbeitsfirma
      $strClassAdd.= ' pp_' .$arrGroup['Id'];
      if (!isset($arrFilterSection['pp'][$arrGroup['Id']])) {
          $arrFilterSection['pp'][$arrGroup['Id']] = $arrGroup['Name'];
      }

      //Filter Liefert Daten
      if ($arrGroup['Liefert_Daten__c'] == 'true') { 
          $strPr = 'Ja';
      } else {
          $strPr = 'Nein';
      }
      $strClassAdd.= ' ld_' .$strPr;
      if (!isset($arrFilterSection['po'][$strPr]) && ($strPr !== false)) {
          $arrFilterSection['ld'][$strPr] = $strPr;
      }

      $strName = str_replace('/', '-', $arrGroup['Name']);
      $strName = str_replace('&', '-', $strName);
      $strUrl = 'https://www.izs.de/cms/protect/' .$strName .'/' .$arrGroup['Id'] .'.pdf';

      $strLink = '<a href="' .$strUrl .'">' .$strUrl .'</a>';

      $strAct = '<span class="hidden">' .strtotime($arrGroup['pr_time']) .'"</span>' .'' .date('d.m.Y H:i:s', strtotime($arrGroup['pr_time'])) .'';

      $strRevTime = '';
      if ($arrGroup['pr_revoked'] == 1) {
        $strRevTime = date('d.m.Y H:i:s', strtotime($arrGroup['pr_revoked_time']));
      }


      $arrList[] = array(
        'name' => '<span class="ct_' .$arrGroup['pr_id'] .'">' .$arrGroup['Name'] .'</span>' .'<span class="hidden hfilter ' .$strClassAdd .'"></span>',
        'delivery'  => ($arrGroup['Liefert_Daten__c'] == 'true') ? 'Ja' : 'Nein',
        'created'   => $strAct,
        'cruser'   => ($arrUserList[$arrGroup['pr_cl_id']] ?? ''),
        'revoked'   => ($arrGroup['pr_revoked'] == 0) ? 'Nein' : 'Ja',
        'retime'   => '<span class="hidden">' .strtotime($arrGroup['pr_revoked_time']) .'"</span>' .$strRevTime,
        'reuser'   => ($arrGroup['pr_revoked'] == 0) ? '' : $arrUserList[$arrGroup['pr_revoked_by']],
        'action' => '<a class="rCert" id="cc_' .$arrGroup['pr_id'] .'" style="cursor: pointer;">' .(($arrGroup['pr_revoked'] == 0) ? 'zurückrufen' : 'aktivieren') .'</a>'
      );

  }

  $arrOpt = array(
      'arrSort' => array(
        'null', 'null', '"type": "hidden"', 'null', 'null', '"type": "hidden"', 'null', 'null'
      ),
      'strOrder' => '"order": [[ 2, "desc" ]]'
  );

  $strTable = strCreateTable($arrList, $arrTableHead, '', false, true, [], $arrOpt);

} else {

  $strTable = '<p>&nbsp;</p><p><strong>Bitte wähle einen PDL.</strong></p>';

}

$strIncOutput.= ' 

    <div class="panel">
';

if (isset($arrSql) && (count($arrSql) > 0)) {
$strIncOutput.= ' 
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Zertifiket' .((count($arrList) != 1) ? 'e' : '') .' gefunden</h3>
      </header>
';
}
      $strIncOutput.= ' 
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strSelect;

$strIncOutput.= '<p>Permalink: <a href="">' .$strLink .'</a></p>';

$strIncOutput.= $strTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

$strJsFootCodeRun.= '

$("span.warning").parent().parent().addClass("warning");
$("span.danger").parent().parent().addClass("danger");

$("#seldate").on("change", function (e) {
  this.form.submit();
});

';

$strJsFootCodeRun.= "

$('.rCert').on('click', function (e) {

  var id   = $(this).attr('id').split('_')[1];

  $.post('/cms/inc/sp.ajax.inc.php', {
    ac: 'revoke', id: id, clid: " .$_COOKIE['id'] ."
  }).done(function(data) {
    
    var data = data.split('|')
    
    $('.ct_' + id).parent().parent().find('td:nth-child(7)').html(data[0]);
    $('.ct_' + id).parent().parent().find('td:nth-child(6)').html(data[1]);

    if ($('#cc_' + id).text() == 'zurückrufen') {
      $('.ct_' + id).parent().parent().find('td:nth-child(5)').html('Ja');
      $('#cc_' + id).text('aktivieren');
    } else {
      $('.ct_' + id).parent().parent().find('td:nth-child(5)').html('Nein');
      $('#cc_' + id).text('zurückrufen');
    }

  });

}).bind('mouseover', function() {
  $(this).css('cursor', 'pointer');
});


";


?>