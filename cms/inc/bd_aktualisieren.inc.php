<?php

//CLASSES
require_once('classes/class.bg.php');
require_once('classes/class.bgevent.php');
require_once('classes/class.auth.php');
require_once('classes/class.request.php');
require_once('classes/class.sendrequest.php');

require_once('classes/class.premiumpayer.php');


//REQUEST
$strFu   = @$_REQUEST['fu'];
$strSf   = @$_REQUEST['sf'];


//DATA
//$objBg   = new Bg;
//$arrEv = $objBgEvent->arrGetOpenEventList($intYear);

//$objBgEvent->boolCreateEventsFromYear(2016);
//die();

if (@$_SERVER['HTTP_FKLD'] == 'on') {
  //print_r($arrEv);
}


//SETTINGS

$boolShowHead = true;
$boolShowSide = false;

$strPageTitle = 'Basisdokumentation';


//FUNCTIONS
$strAllRequest     = '';
$strReqRequest     = '';
$strProoveRequest  = '';
$strBackRequest    = '';
$strEscRequest     = '';
$strInfoRequest    = '';
$strNoreactRequest = '';


$arrFilterSection = array();

$strIncOutput = '';

switch ($strFu) {
  case 'all':
    $strAllRequest = 'active';
    require_once('bd_aktualisieren.all.inc.php');
    break;
  case 'request':
    $strReqRequest = 'active';
    require_once('bd_aktualisieren.all.inc.php');
    break;
  case 'proove':
    $strProoveRequest = 'active';
    require_once('bd_aktualisieren.all.inc.php');
    break;
  case 'back':
    $strBackRequest = 'active';
    require_once('bd_aktualisieren.all.inc.php');
    break;
  case 'esc':
    $strEscRequest = 'active';
    require_once('bd_aktualisieren.all.inc.php');
    break;

  case 'noreact':
    $strNoreactRequest = 'active';
    require_once('bd_aktualisieren.all.inc.php');
    break;
  
  case 'info':
    $strInfoRequest = 'active';
    require_once('bd_aktualisieren.info.inc.php');
    break;
  default:
    $strAllRequest = 'active';
    $strFu = 'all';
    require_once('bd_aktualisieren.all.inc.php');
    break;
}

//print_r($arrFilterSection);


//CONTENT

$strOutput = '

  <style>
  .page { width: 100%; !important }
  .panel-actions {
    position: absolute;
    top: -80px;
    width: 250px;
  }
  .nosort {
    background-image: none !important;
  }
  </style>

  <!-- Page -->
  <div class="page animsition">
  
    <div class="page-aside">
      <div class="page-aside-switch">
        <i class="icon wb-chevron-left" aria-hidden="true"></i>
        <i class="icon wb-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner" style="/* position: fixed; width: 250px; */">
        <section class="page-aside-section" style="width: 229px;">
          <h5 class="page-aside-title">Filter </h5><span id="resetFilter"></span>
          
          <form style="padding-left: 30px;">
';

if (isset($arrFilterSection['pp']) && (count($arrFilterSection['pp']) > 0)) {
  
  natcasesort($arrFilterSection['pp']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_pp">CompanyGroup</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_pp" name="filter_pp" placeholder="CompanyGroup" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['pp'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="pp_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['ac']) && (count($arrFilterSection['ac']) > 0)) {
  
  natcasesort($arrFilterSection['ac']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_ac">Meldestelle</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_ac" name="filter_ac" placeholder="CompanyGroup" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['ac'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="ac_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['ty']) && (count($arrFilterSection['ty']) > 0)) {
  
  natcasesort($arrFilterSection['ty']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_ty">Typ</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_ty" name="filter_ty" placeholder="Typ" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['ty'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="ty_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['en']) && (count($arrFilterSection['en']) > 0)) {
  
  natcasesort($arrFilterSection['en']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_en">Entleiher</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_en" name="filter_en" placeholder="Typ" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['en'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="en_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['up']) && (count($arrFilterSection['up']) > 0)) {
  
  ksort($arrFilterSection['up']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_up">Update-Status</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_up" name="filter_up" placeholder="Update-Status" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['up'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="up_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['ns']) && (count($arrFilterSection['ns']) > 0)) {
  
  natcasesort($arrFilterSection['ns']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_ns">Nächste Aktion</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_ns" name="filter_ns" placeholder="Nächste Aktion" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['ns'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="ns_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['er']) && (count($arrFilterSection['er']) > 0)) {
  
  natcasesort($arrFilterSection['er']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_er">Erhalten</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_er" name="filter_er" placeholder="Erhalten" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['er'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="er_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

/*
if (isset($arrFilterSection['ru']) && (count($arrFilterSection['ru']) > 0)) {
  
  arsort($arrFilterSection['ru']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_ru">Rückmeldung</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_ru" name="filter_ru" placeholder="Rückmeldung" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['ru'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="ru_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['sb']) && (count($arrFilterSection['sb']) > 0)) {
  
  natcasesort($arrFilterSection['sb']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_sb">Status Bearbeitung</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_sb" name="filter_sb" placeholder="Status Bearbeitung" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['sb'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="sb_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['er']) && (count($arrFilterSection['er']) > 0)) {
  
  natcasesort($arrFilterSection['er']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_er">Ergebnis</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_er" name="filter_er" placeholder="Ergebnis" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['er'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="er_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['bb']) && (count($arrFilterSection['bb']) > 0)) {
  
  natcasesort($arrFilterSection['bb']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_bb">Befristet bis</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_bb" name="filter_bb" placeholder="Befristet bis" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['bb'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="bb_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['sp']) && (count($arrFilterSection['sp']) > 0)) {
  
  natcasesort($arrFilterSection['sp']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_sp">Sperrvermerk</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_sp" name="filter_sp" placeholder="Sperrvermerk" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['sp'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="sp_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['si']) && (count($arrFilterSection['si']) > 0)) {
  
  natcasesort($arrFilterSection['si']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_si">Sichtbarkeit des Dokuments</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_si" name="filter_si" placeholder="Sichtbarkeit des Dokuments" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['si'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="si_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['sk']) && (count($arrFilterSection['sk']) > 0)) {
  
  natcasesort($arrFilterSection['sk']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_sk">Status Klärung</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_sk" name="filter_sk" placeholder="Status Klärung" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  $strOutput.= '                  <option value="sk_">kein Status</option>' .chr(10);
  foreach ($arrFilterSection['sk'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="sk_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['gk']) && (count($arrFilterSection['gk']) > 0)) {
  
  natcasesort($arrFilterSection['gk']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_gk">Grund für Klärung</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_gk" name="filter_gk" placeholder="Grund Klärung" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  $strOutput.= '                  <option value="gk_">kein Grund</option>' .chr(10);
  foreach ($arrFilterSection['gk'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="gk_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}


if (isset($arrFilterSection['ns']) && (count($arrFilterSection['ns']) > 0)) {
  
  natcasesort($arrFilterSection['ns']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_ns">Nächster Schritt</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_ns" name="filter_ns" placeholder="Nächster Schritt" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['ns'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="ns_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}


if (isset($arrFilterSection['wi']) && (count($arrFilterSection['wi']) > 0)) {
  
  natcasesort($arrFilterSection['wi']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_wi">Wiedervorlage</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_wi" name="filter_wi" placeholder="Wiedervorlage" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['wi'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="wi_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}
*/

$strOutput.= '          
          </form>

        </section>
      </div>
    </div>
    <div class="page-main">
      <div class="page-header">
        <h1 class="page-title">' .$strPageTitle .'</h1>
';

$strOutput.= '
        <div style="z-index: 1;" class="page-header-actions">

          <div class="panel-actions">
';

if (@$_REQUEST['fu'] != 'info') {
$strOutput.= '
          <div style="width: 80px; float: left; border-right: 30px;">
          <button type="button" class="btn btn-info" id="massFunction" aria-expanded="true">
            Massenfunktion
          </button>
        </div>
';
}

$strOutput.= '
            <div class="dropdown" style="float: right;">
              <button type="button" class="btn btn-info dropdown-toggle " id="exampleAlignmentDropdown"
              data-toggle="dropdown" aria-expanded="true">
                Optionen
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="exampleAlignmentDropdown" role="menu">
                <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="doExport">Export CSV</a></li>
              </ul>
            </div>

            </div>

        </div>
';


$strOutput.= '      
        <ul class="nav nav-tabs nav-tabs-line" style="margin-bottom: 20px;">
          <li class="' .$strAllRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bd_aktualisieren&fu=all">Alle</a></li>
          <li class="' .$strReqRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bd_aktualisieren&fu=request">Anzufragen</a></li>
          <li class="' .$strProoveRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bd_aktualisieren&fu=proove">Anfrage prüfen</a></li>
          <li class="' .$strNoreactRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bd_aktualisieren&fu=noreact">Keine Reaktion</a></li>
          <li class="' .$strBackRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bd_aktualisieren&fu=back">Anfrage zurückgestellt</a></li>
          <li class="' .$strEscRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bd_aktualisieren&fu=esc">Eskalation</a></li>
          <li class="' .$strInfoRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bd_aktualisieren&fu=info">Info an Entleiher</a></li>
        </ul>
';


$strOutput.= '  

      </div>
      <div class="page-content">
  
';

$strOutput.= $strIncOutput;

$strOutput.= '

      </div> <!-- End page-content -->

    </div> <!-- End page-main -->

  </div>
  <!-- End Page -->
  

  <!-- Modal -->
  <div class="modal fade" id="DetailModal" aria-hidden="true" aria-labelledby="ChangelogSidebar"
  role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Changelog</h4>
        </div>
        <div class="modal-body">
          <p>Body.</p>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
  <!-- End Modal -->
  
  <!-- Modal -->
  <div class="modal fade" id="Archive" aria-hidden="true" aria-labelledby="TaskSidebar"
  role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center smallw">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Ablegen</h4>
        </div>
        <div class="modal-body">
          <p>Bitte wähle den Ablegegrund:</p>

          <select class="selectpicker" name="ArchiveAction">
            <option>Keine Reaktion</option>
            <option>Kein Ersatzdokument erforderlich</option>
          </select>
          
        </div>
        <div class="modal-footer">
        
          <input type="hidden" name="ddid" value="" id="ddid">

          <button data-dismiss="modal" class="btn btn-default" type="button">Abbbrechen</button>
          <button id="doArchiv" class="btn btn-primary" type="button">Ablegen</button>
        
        </div>
      </div>
    </div>
  </div>
  <!-- End Modal -->

  <!-- Modal -->
  <div class="modal fade modal-danger" id="ModalDanger" aria-hidden="true"
  aria-labelledby="ModalDanger" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center smallw">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title">Achtung</h4>
        </div>
        <div class="modal-body">
          <p id="errMessage">Text</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
        </div>
      </div>
    </div>
  </div>
  <!-- End Modal -->

  <!-- Modal -->
  <div class="modal fade modal-info" id="modalMassFunction" aria-hidden="true"
  aria-labelledby="modalMassFunction" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title">Massenfunktion</h4>
        </div>
        <div class="modal-body">

          <div class="form-group form-material ">
            <spam class="msg"> </span>
          </div> <!-- form-group -->

        </div>
        <div class="modal-footer">
          <button class="btn btn-primary margin-top-5 pull-right" type="submit" id="modalMassFunctionSubmit">ausführen</button>
          <button data-dismiss="modal" class="btn btn-default btn-default margin-top-5 margin-right-10 pull-right" type="button">Close</button>
        </div>
      </div>
    </form>

    </div>
  </div>
  <!-- End Modal -->


<form method="post" action="_get_csv.php" id="export">
<input type="hidden" name="strTable" id="strTable" value="">
</form>

';


$strJsFootScript.= '  <script src="js/script_neu.js"></script>' .chr(10);

$strJsFootCodeRun.= '

$("span.warning").parent().parent().addClass("warning");
$("span.info").parent().parent().addClass("info");
$("span.danger").parent().parent().addClass("danger");

$("#resetFilter").on("click", function(e) {
  
  $("table.display tbody tr").show();

  $("select.filter option").each(function(i) {
    $(this).css("color", "#3f3f3f");
  });

  $("ul.select2-results__options li").css("color", "#3f3f3f");
  
  $("#DocCount").text($("table.display tbody tr:visible").length);
  $("#resetFilter").text("");
  
  $("select").val("");
    
});

$(".due").parent().parent().addClass("red");

$("#resetFilter").hover(function() {
  $(this).css("cursor","pointer");
});

var $eventSelect = $(".filter");
$eventSelect.on("select2:opening", function (e) { 
  var id = $(this).prop("id");
  setTimeout(function(){
    $("#select2-" + id +"-results li").each(function(i) {
      var str = $(this).prop("id").toString();
      if (str != "") {
        var tr = $("tr:visible td span.hfilter." + str.split("-")[4]);
        if (tr.length > 0) {
          $(this).css("color", "#3f3f3f");
        } else {
          $(this).css("color", "#a3afb7");
        }
      }
    });
  }, 1);
});


$(".filter").on("change", function(e) {
  
  var clstr = "span.hfilter";
  var fiid  = $(this).attr("id");
  var sval  = $(this).val();
  
  //if (uf !== null) {
  //  clstr = uf;
  //  uf = null;
  //} else {
    $("select.filter").each(function(i) {
      if ($(this).val() != "") clstr+= "." + $(this).val();
    });
  //}
  
  $("span.hfilter").parent().parent().hide();
  $(clstr).parent().parent().show();

  window.clstr = clstr;
  
  var res = [];
  
  $("table.display tbody tr:visible td:first-child span").each(function(i) {
    res = res.concat($(this).attr("class").split(" ").filter(function (item) {
      return res.indexOf(item) < 0;
    }));
    return res;
  });
  
  $("#DocCount").text($("table.display tbody tr:visible").length);
  $("#resetFilter").text("alle zurücksetzen");

  $(".docdoc:hidden:checked").prop("checked", false);

});

var url = new URL(location.href);
var uf = url.searchParams.get("filter");
if (uf !== null) {

  var fipa = uf.split(".");
  if (fipa.length > 2) {
    for (var i = 2; i < fipa.length; i++) {
      var v = fipa[i].split("_");
      $("#filter_" + v[0]).val(fipa[i]).trigger("change");
      console.log("#filter_" + v[0] + " -> " + fipa[i]);
    }
  }

}

setTimeout(function() { 
  if (uf === null) {
    //$("#filter_y").val("y_' .(date('Y') - 1) .'").trigger("change");
  }
}, 500);  

  
      $("[id^=\'dd_\']").on("change", function(e) {
      
        var checked = $(this).prop("checked");
        
        if (checked) {
          $(this).parent().parent().css("font-style", "italic");
          $(this).parent().find("span").text("order_1");
        } else {
          $(this).parent().parent().css("font-style", "normal");
          $(this).parent().find("span").text("order_0");
        }

        $(this).parent().parent().parent().parent().DataTable().rows().invalidate("dom");
      
        $.post("action/additional.ajax.php", { 
            ac: "addValue", table: "Dokument_Dokumentation__c", id: $(this).prop("id"), key: "received", value: checked
          }).done(function(data) {
            ;
          }
        );
        
      });  

  
      $("#doExport").on("click", function(e) {
        
        $("input[type=checkbox]").each(function() { 
          $(this).attr("csvchecked", $(this).is(":checked")); 
        });
        
        var table_hd = $($("table")[0]).html();
        var table_cth = $("table.table:eq(1) tr:visible");
        var table_ct = "";
        
        if (table_cth.length > 0) {
          $.each(table_cth, function(k, v) {
            table_ct += "<tr>" + $(v).html() + "</tr>";
          });
        }
        
        $("#strTable").val("<table>" + table_hd + table_ct + "</table>");
        $("#export").submit();
        
      });  

      // Fill modal with content from link href
      $("#DetailModal").on("show.bs.modal", function(e) {
          var link = $(e.relatedTarget);
          $(this).find(".modal-content").load(link.attr("href"));
      });
      
 	    //$("selectpicker").selectpicker();

      $("#doArchiv").click(function(){

        var text = $(".selectpicker option:selected").val()
        var id = $("#ddid").val();

        $(this).parent().parent().fadeTo("slow", 0.00);

        $.ajax({
          type: "POST",
          url:  "action/ajax.php",
          dataType: "text", 
          data: { ac: "ddArchivClick", ddid: id, aaction: text },
          success: function(result) {
            jsonData = $.parseJSON(result);
            
            $("#" + jsonData["ddid"]).parent().parent().fadeTo("slow", 1.00);
            
            if (jsonData["Status"] == "OK") {
              
              if (jsonData["Action"] == "restart") {

                $(\'#st_\' + jsonData[\'ddid\']).html(jsonData[\'UStatus\']);
                $(\'#da_\' + jsonData[\'ddid\']).html(jsonData[\'UTime\']);
                $(\'#dah_\' + jsonData[\'ddid\']).html(jsonData[\'UTimeH\']);

                $(\'#es_\' + jsonData[\'ddid\']).html("");
                $(\'#esh_\' + jsonData[\'ddid\']).html("");
                
                $(\'#\' + jsonData[\'ddid\']).html(jsonData[\'UStatusNew\']);
                $(\'#\' + jsonData[\'ddid\']).prop(\'title\', jsonData[\'UStatusNew\']);

              } 
              
              if (jsonData["Action"] == "archive") {
              
                var table = $("table[id*=table_]").DataTable();
              
                $("#" + jsonData["ddid"]).parent().parent().hide();
                $("#" + jsonData["ddid"]).parent().parent().addClass("selected");
                table.row(".selected").remove().draw(false);
                
                $("#DocCount").text(Number($("#DocCount").text()) - 1);
              
              }
                  
            }
            
            if (jsonData["Status"] == "FAIL") {
              alert(jsonData["Reason"]);
            }
    
          }
        });

       	$("#Archive").modal("hide");
       	//console.log(text);
      });
';

?>