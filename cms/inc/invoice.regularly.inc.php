<?php

//require_once ('../../modules/serial/classes/serial.class.php');

//var_dump(file_exists('../../../crm'));

$strMailSubject = 'Ausübung der vertraglich vereinbarten Preiserhöhung';

$strMailBody = 'Sehr geehrte Damen und Herren,

wir möchten Sie hiermit informieren, dass wir zum ' .date('d.m.Y', strtotime('first day of next month')) .' die vertraglich vereinbarte Preiserhöhung gemäß Ziffer 3.1 unserer AGBs "{AGB}" umsetzen werden.

Die beiliegende Aufstellung zeigt Ihnen die jeweiligen betragsmäßigen Anpassungen ab dem ' .date('d.m.Y', strtotime('first day of next month')) .'. Alle Preise verstehen sich netto zzgl. gesetzlicher Mehrwertsteuer.

Beiliegend erhalten Sie zudem unsere aktualisierte Dauerrechnung, ein separater Postversand erfolgt nicht mehr.

Bei Fragen sind wir jederzeit gerne telefonisch unter Tel. 089 122 237 770 oder per E-Mail (info@izs-institut.de) für Sie erreichbar.

Mit besten Grüßen
Ihr IZS-Team


P.S.:
Ihre IZS-Mitgliedschaft wird in Kürze noch wertvoller, denn schon bald schalten wir unseren neuen "IZS MARKETPLACE" frei – eine neue Online-Plattform, auf dem Entleiher gezielt nach geographisch und fachlich passenden, IZS-zertifizierten Personaldienstleistern suchen und diese ansprechen können, um Verleiher, mit denen sie bisher arbeiten und die noch nicht IZS-zertifiziert sind, zu ersetzen. Sie werden in Kürze eine exklusive Einladung dazu erhalten. Als geschätzter IZS-Bestandskunde können Sie sich auf dem neuen IZS MARKETPLACE natürlich kostenlos listen lassen im Rahmen der Markteinführungsphase. Sie werden sehen: Noch nie war es einfacher und bequemer, mehr Umsatz zu machen. Ihre IZS-Mitgliedschaft macht den entscheidenden Unterschied.';

$strSelect = '';

if (empty($_REQUEST['seldate_m'])) {
  $_REQUEST['seldate_m'] = date('m');
  $_REQUEST['seldate_y'] = date('Y');
}

$strSelect.= '<form action="/cms/index_neu.php?ac=invoice&fu=regularly" method="post">'; 
$strSelect.= '<label for="seldate_y" style="margin-right: 20px;">Abrechnungsmonat</label>';

$strSelect.= '<select name="seldate_m" id="seldate_m">' .chr(10);
for ($intMonth = 1; $intMonth <= 12; $intMonth++) {
  $strSelected = '';
  if ($intMonth == @$_REQUEST['seldate_m']) {
    $strSelected = ' selected="selected"';
  }
  $strSelect.= '<option value="' .$intMonth  .'"' .$strSelected .'>' .$intMonth .'</option>' .chr(10);
}  
$strSelect.= '</select>';

$strSelect.= ' - ';

$strSelect.= '<select name="seldate_y" id="seldate_y">' .chr(10);
for ($intYear = 0; $intYear <= 7; $intYear++) {
  $strSelected = '';

  $intYearDisplay = (date('Y') + $intYear - 2);

  if ($intYearDisplay == @$_REQUEST['seldate_y']) {
    $intYearSelected = $intYearDisplay;
    $strSelected = ' selected="selected"';
  }
  $strSelect.= '<option value="' .$intYearDisplay  .'"' .$strSelected .'>' .$intYearDisplay .'</option>' .chr(10);
}  
$strSelect.= '</select>' .chr(10);

$strSelect.= ' <button type="submit" label="Los">Los</button></form>' .chr(10);

$_REQUEST['seldate'] = $_REQUEST['seldate_y'] .'-' .sprintf('%02d', $_REQUEST['seldate_m']);
$intMonth = $_REQUEST['seldate_m'];

$arrEntList = array();
$strSql = 'SELECT * FROM `Account` WHERE `RecordTypeId` = "01230000001BvebAAC"';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrEnt) {
    $arrEntList[$arrEnt['Id']] = $arrEnt['Name'];
  }
}


$strSql = 'SELECT `Group__c`.`Name` AS `GName`, `Group__c`.`Id` AS `GId`, `Betriebsnummer__c`, `Vertrag__c`.*  FROM `Vertrag__c` INNER JOIN `Group__c` ON `Company_Group__c` = `Group__c`.`Id` WHERE `Aktiv__c` = "true" ORDER BY `GName`';
$arrGroupList = MySQLStatic::Query($strSql);

//print_r($arrGroupList);

$intCountResult = count($arrGroupList);

$arrGroupListPerMonth = array();
if ($intCountResult > 0) {
  foreach ($arrGroupList as $intRow => $arrGroup) {
    $arrMonthList = explode(';', $arrGroup['Abzurechnen_in_Monat__c']);
    if (in_array($intMonth, $arrMonthList)) {
      $arrGroupListPerMonth[] = $arrGroup;
    }
  }
}

$arrGroupList = $arrGroupListPerMonth;

//print_r($arrGroupList); die();

$arrTableHead = array(
  'Company Group',
  'Mitgliedsmodell',
  'Zahler',
  'Zahler ab',
  'Kündigung wirksam ab',
  'Vertragsbeginn',
  'Zahlungsart',
  'Einmalige Einrichtung',
  'Preis pro Datensatz über Inklusivvolumen',
  'Pauschale DV p.M.',
  'Nettobetrag DV',
  'Nettobetrag Einrichtung',
  'Anmerkung'
);

$arrList  = array();
$floatSumFlat = 0.0;
$floatSumSetup = 0.0;

foreach ($arrGroupList as $intRow => $arrGroup) {

  $strNote = '';

  /*
  $strSqlDone = 'SELECT `ec_type`, UNIX_TIMESTAMP(`ec_time`) as `ec_time`, `ec_cl_id` FROM `izs_easy_created` WHERE `ec_group_id` = "' .$arrGroup['GId'] .'" AND `ec_month` = "' .$_REQUEST['seldate_m'] .'" AND `ec_year` = "' .$_REQUEST['seldate_y'] .'"';
  $arrSqlDone = MySQLStatic::Query($strSqlDone);

  $arrSqlDone = Serial::createIndexedList($arrSqlDone, 'ec_type');
  */

  //print_r($arrSqlDone); die();

  $strAddClass = '';
  if (substr($arrGroup['Vertragsbeginn__c'], 0, -3) == $_REQUEST['seldate']) {
    $strAddClass.= 'warning';

    $strSql = 'SELECT *  FROM `izs_vertrag` WHERE `Id` = "' .$arrGroup['GId'] .'" AND `Vertragsbeginn__c` < "' .$arrGroup['Vertragsbeginn__c'] .'" ORDER BY `Name`';
    $arrGroupList = MySQLStatic::Query($strSql);
    $intCountResult = count($arrGroupList);
    if ($intCountResult >= 1) {
      $strNote = '<b>Achtung Vertragswechsel</b>';
    }

  }  

  $strInvSetup = '';
  if ($strAddClass != '') {

    $strAddFunc = '';
    if (isset($arrMapping[$arrGroup['GId']])) {
      $strDone = '';
      $strAddFunc = ' <i class="fa fa-money' .$strDone .' setup" aria-hidden="true" rel="' .$arrMapping[$arrGroup['GId']] .'"></i>';

      $intDocNumber = getDocumentNumber(3, $arrMapping[$arrGroup['GId']], $intMonth, $intYearSelected);
      if ($intDocNumber != 0) {
        $strAddFunc.= ' <a href="inc/invoice.download.php?id=' .$intDocNumber .'" target="_blank"><i class="fa fa-file-pdf-o duration" aria-hidden="true" style="padding-left: 3px; color: black;"></i></a>';
      }

    } else {
      //$strAddFunc.= ' ' .(isset($arrMapping[$arrGroup['GId']]) ? 'true' : 'false');
      //die('here');

      //print_r($arrMapping); die();

    }

    $strInvSetup = number_format($arrGroup['Einrichtungsgebuehr__c'], 2, ',', '.') .' EUR' .$strAddFunc;
    $floatSumSetup+= $arrGroup['Einrichtungsgebuehr__c'];
  }


  if ($arrGroup['Einrichtungsgebuehr__c'] == '') {
    $arrGroup['Einrichtungsgebuehr__c'] = 0;
  }

  $strInvType = $arrGroup['Zahlungsart__c'];
  if ($strInvType == 'Überweisung') {
    $strInvType = '<span class="text-warning">' .$arrGroup['Zahlungsart__c'] .'</span>';
  }

  $strAddFunc = '';
  if ($strAddClass != '') {
    /*
    if (isset($arrMapping[$arrGroup['GId']])) {
      $strDone = '';
      if (in_array(4, $arrSqlDone)) {

      }
    */
      //$strAddFunc = ' <i class="fa fa-money' .$strDone .' duration" aria-hidden="true" rel="' .@$arrMapping[$arrGroup['GId']] .'"></i>';
    //}
  }

  $strDone = '';




  $strAddFunc = ' <i class="fa fa-money' .$strDone .' duration" aria-hidden="true" rel="' .@$arrMapping[$arrGroup['GId']] .'_' .$_REQUEST['seldate_m'] .'_' .$_REQUEST['seldate_y'] .'"></i>';
  
  if (isset($arrMapping[$arrGroup['GId']])) {
    $intDocNumber = getDocumentNumber(4, $arrMapping[$arrGroup['GId']], $intMonth, $intYearSelected);

    if ($intDocNumber != 0) {
      $strAddFunc.= ' <a href="inc/invoice.download.php?id=' .$intDocNumber .'" target="_blank"><i class="fa fa-file-pdf-o duration" aria-hidden="true" style="padding-left: 3px; color: black;"></i></a>';
    }
  }


  $strPreis = 'SELECT * FROM `temp_preiserhoehung` WHERE `Nr` = "' .$arrGroup['Name'] .'"';
  $arrPreis = MySQLStatic::Query($strPreis);

  if (count($arrPreis) > 0) {

    $strContact = 'SELECT `Email` FROM `Contact` WHERE `Ansprechpartner_fuer__c` LIKE "%Rechnungswesen%" AND `Email` != "" AND `GroupId` = "' .$arrGroup['GId'] .'" AND `Aktiv__c` = "true"';
    $arrContact = MySQLStatic::Query($strContact);

    $strTo = '';
    if (count($arrContact) > 0) {
      $arrTo = array();
      foreach ($arrContact as $intCount => $strEmail) {
        $arrTo[] = $strEmail['Email'];
      }
      $strTo = implode(',', $arrTo);
    }

    $strBody = str_replace('"', '%22', $strMailBody);
    $strBody = str_replace("\n", '%0D%0A', $strBody);
    $strBody = str_replace("{AGB}", $arrGroup['AGB_Version__c'], $strBody);

    $strAddFunc.= ' <a target="_blank" href="mailto:' .$strTo .'?bcc=rechnung@izs-institut.de&subject=' .$strMailSubject .'&body=' .$strBody .'" title="Anschreiben Preiserhöhung"><i class="fa fa-envelope-o" aria-hidden="true" style="padding-left: 3px; color: black;"></i></a>';

  }

  $strOver = '';
  $strOverFrom = '';
  if ($arrGroup['BillingOver'] != '') {

    $strOver = $arrEntList[$arrGroup['BillingOver']];
    $strOverFrom = date('d.m.Y', strtotime($arrGroup['BillingOverFrom']));

  }

  $arrList[] = array(
    'ppayer'   => $arrGroup['GName'] .'<span class="hidden ' .$strAddClass .'"></span>',
    'model'    => $arrModelList[$arrGroup['GId']],
    'payer'    => $strOver,
    'payfrom'  => $strOverFrom,
    'cancel'   => strFormatDate($arrGroup['Kuendigung_wirksam_ab__c'], 'd.m.Y', 'Y-m-d'),
    'since'    => strFormatDate($arrGroup['Vertragsbeginn__c'], 'd.m.Y', 'Y-m-d'),
    'invtype'  => $strInvType,
    'setup'    => number_format($arrGroup['Einrichtungsgebuehr__c'], 2, ',', '.') .' EUR',
    'ppds'     => number_format($arrGroup['Preis_pro_Datensatz_ber_Inklusivvolumen__c'], 2, ',', '.') .' EUR', 
    'flat'     => number_format((float) $arrGroup['Summe_Leistungen_DV_pro_Monat__c'], 2, ',', '.') .' EUR', 
    'invdv'    => number_format((float) $arrGroup['Pauschale_DV_Quartal__c'], 2, ',', '.') .' EUR' .$strAddFunc,
    'invsetup' => $strInvSetup,
    'note'     => $strNote
  );

  $floatSumFlat+= (float) $arrGroup['Pauschale_DV_Quartal__c'];

  /*
  if ($intDiff > 0) {
    $floatSum+= $intDiff * $arrGroup['Preis_pro_Datensatz_ber_Inklusivvolumen__c'];
  }
  */

}

$strTable = strCreateTable($arrList, $arrTableHead, '', false, true);

$arrCol = array();
$arrCol[] = array(
  'Summe DV',
  number_format($floatSumFlat, 2, ',', '.') .' EUR'
);
$arrCol[] = array(
  'Summe Einrichtung',
  number_format($floatSumSetup, 2, ',', '.') .' EUR',
);
$arrCol[] = array(
  'Gesamtsumme',
  '<b>' .number_format($floatSumFlat+ $floatSumSetup, 2, ',', '.') .' EUR' .'</b>'
);

$strSumTable = strCreateTableSimple($arrCol, array(), 'regSumTable', false, true);

$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Verträge gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strSelect;

$strIncOutput.= $strSumTable;

/*
$strIncOutput.= '<p>' .number_format($floatSumFlat, 2, ',', '.') .' EUR' .'<br />';
$strIncOutput.= '' .number_format($floatSumSetup, 2, ',', '.') .' EUR' .'</p>';
$strIncOutput.= '' .number_format($floatSumFlat+ $floatSumSetup, 2, ',', '.') .' EUR' .'</p>';
*/

$strIncOutput.= $strTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

$strJsFootCodeRun.= '

$("span.warning").parent().parent().addClass("warning");
$("span.danger").parent().parent().addClass("danger");

$("#seldate").on("change", function (e) {
  this.form.submit();
});

$("td:nth-child(6)").css("text-align",  "center");
$("td:nth-child(6)").css("white-space", "nowrap");

$("td:nth-child(7)").css("text-align",  "center");
$("td:nth-child(7)").css("white-space", "nowrap");

$("td:nth-child(9)").css("text-align",  "right");
$("td:nth-child(9)").css("white-space", "nowrap");

$("td:nth-child(10)").css("text-align",  "right");
$("td:nth-child(10)").css("white-space", "nowrap");

$("td:nth-child(11)").css("text-align",  "right");
$("td:nth-child(11)").css("white-space", "nowrap");

$(".fa-money").on("click", function (e) {

  var ty, tys, obj;

  obj = $(this);

  if ($(this).hasClass("setup")) {
    ty = 3;
    tys = "Einrichtungsgebühr";
  }
  if ($(this).hasClass("duration")) {
    ty = 4;
    tys = "Dauerrechnung";
  }

  $(this).addClass("done");

  ///*

  $.post("inc/invoice.ajax.php", { 
    ac: "createBill", id: $(this).attr("rel"), type: ty, month: ' .(int) $intMonth .', year: ' .$intYearSelected .'
    }).done(function(data) {
      if (data == 1) {
        console.log(data);
        toastr.options = { "closeButton": true, "positionClass": "toast-top-full-width", "newestOnTop": true,   "showMethod": "fadeIn", "hideMethod": "fadeOut" }
        toastr.success(\'Die Rechnung "\' + tys + \'" wurde an easyBill gesendet.\');
        obj.hide();
      } else {
        toastr.options = { "closeButton": true, "positionClass": "toast-top-full-width", "newestOnTop": true,   "showMethod": "fadeIn", "hideMethod": "fadeOut" }
        toastr.error(\'Die Rechnung konnte nicht gesendet werden. Es sind nur 10 Zugriffe pro Minute möglich. Versuchen Sie es später noch einmal.\');
      }
    }
  );
  
  //*/

});

';


?>