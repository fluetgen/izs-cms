<?php

$arrFilter = array();

$arrFilter[] = array(
  'strKey' => 'be_status_bearbeitung',
  'strType' => '=',
  'strValue' => 'Endkontrolle'
);


$arrEventList = $objBgEvent->arrGetBgEventList($arrFilter);

//print_r($arrEventList); die();

$arrBgNameList = array();
if (count($arrBgListEvent) > 0) {
  foreach ($arrBgListEvent as $intKey => $arrBg) {
    $arrBgNameList[$arrBg['acid']] = $arrBg['ac_name'];
  }
}

$arrMeldestellen = $objBg->arrGetAllMeldestellen();

$arrMeldestellenNameList = array();
if (count($arrMeldestellen) > 0) {
  foreach ($arrMeldestellen as $intKey => $arrMeldestelle) {
    $arrMeldestellenNameList[$arrMeldestelle['Id']] = $arrMeldestelle['Name'];
  }
}

$arrMeldestelleGruppe = [];
$strSql = 'SELECT * FROM `izs_bg_meldestelle`';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrDataset) {
    $arrMeldestelleGruppe[$arrDataset['Id']] = $arrDataset;
  }
}

$arrEscalation = [];
$strSql = 'SELECT * FROM `Group__c` WHERE `Eskalation__c` = "ja"';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrDataset) {
    $arrEscalation[$arrDataset['Id']] = $arrDataset;
  }
}

$arrListNvm = [];
$strSql = 'SELECT * FROM `Negativmerkmal__c` WHERE `Auskunftsgeber_Typ__c` = "01230000001DKGtAAO" AND `Aktiv__c` = "true"';
$arrSql = MySQLStatic::Query($strSql);

if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrNvm) {
    $arrListNvm[$arrNvm['AccountId']] = $arrNvm;
  }
}

$arrNegativAll = [];
$strSql = 'SELECT * FROM `ks_dropdown` WHERE `kd_kf_id` = 851 AND `kd_criteria` = "01230000001DKGtAAO" ORDER BY `kd_value`';
$arrSql = MySQLStatic::Query($strSql);

if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrNvmAll) {
    $arrNegativAll[] = $arrNvmAll['kd_value'];
  }
}

$strSql = 'SELECT `cl_ks_id` FROM `cms_login` WHERE `cl_id` = "' .$_SESSION['id'] .'"';
$arrSql = MySQLStatic::Query($strSql);

if (count($arrSql) > 0) {
  $strCrmUser = $arrSql[0]['cl_ks_id'];
}

$arrTableHead = array(
  'Esc',
  '',
  'ID',
  'Chat',
  'Berufsgenossenschaft',
  'Meldestelle',
  'Unternehmensnummer',
  '',
  '',
  'Rückmeldung am',
  'Befristet bis',
  'Ergebnis',
  'Sundung / Ratenzahlung',
  'Meldepflicht erfüllt',
  'Negativmerkmal CRM',
  'Klärung Status',
  'Klärung Grund',
  'Status-Info',
  'Bearbeiter',
);

$arrStatusBearbeitung = $objBgEvent->arrGetSelection('be_status_bearbeitung');
$arrErgebnis = $objBgEvent->arrGetSelection('be_ergebnis');
$arrStatusKlaerung = $objBgEvent->arrGetSelection('be_klaerung_status');
$arrBearbeiter = $objBgEvent->arrGetSelection('be_bearbeiter');
$arrStundung = $objBgEvent->arrGetSelection('be_stundung');
$arrMeldepflicht = $objBgEvent->arrGetSelection('be_meldepflicht');

$arrBefristet = array(
  'noch nicht fällig',
  'fällig',
  'ohne Befristung'
);

$arrRueckmeldung = array(
  'nicht erhalten',
  'erhalten'
);

$arrList = array();
foreach ($arrEventList as $intRow => $arrEvent) {

  //print_r($arrBearbeiter); die();

  $strClassAdd = '';

  $strKey = array_search($arrEvent['be_bearbeiter'], $arrBearbeiter);
  $strClassAdd.= ' us_' .$strKey;
  if ((!isset($arrFilterSection['us'][$strKey])) && ($strKey != '')) {
    $arrFilterSection['us'][$strKey] = $arrEvent['be_bearbeiter'];
  }

  $strBesfistet = '';
  if ($arrEvent['be_befristet'] != '0000-00-00') {
    $strBesfistet = strConvertDate($arrEvent['be_befristet'], 'Y-m-d', 'd.m.Y');
  }
  
  $strClassAdd.= ' sp_' .$arrEvent['be_sperrvermerk'];
  $strSperrvermerk = 'nein';
  if ($arrEvent['be_sperrvermerk'] != 0) {
    $strSperrvermerk = 'ja';
  }

  $strDokumentArt = '';
  if (($arrEvent['be_dokument_art'] == '') || ($arrEvent['be_dokument_art'] == 'Unbedenklichkeitsbescheinigung (UBB)')){
    $strDokumentArt = 'UBB';
  }
  
  $strClassAdd.= ' y_' .$arrEvent['be_jahr'];
  if (!isset($arrFilterSection['y'][$arrEvent['be_jahr']])) {
    $arrFilterSection['y'][$arrEvent['be_jahr']] = $arrEvent['be_jahr'];
  }

  $strClassAdd.= ' bg_' .$arrEvent['be_bg'];
  if (!isset($arrFilterSection['bg'][$arrEvent['be_bg']])) {
    $arrFilterSection['bg'][$arrEvent['be_bg']] = $arrBGList[$arrEvent['be_bg']];
  }
  
  $strMeldestelle = @$arrMeldestellenNameList[$arrEvent['be_meldestelle']];
  if ($strMeldestelle == '') {
    $arrMeldestelle = $objPp->arrGetPP($arrEvent['be_meldestelle'], false);
    $strMeldestelle = $arrMeldestelle[0]['ac_name'];
  }
    
  $strClassAdd.= ' pp_' .$arrEvent['be_meldestelle'];
  if (!isset($arrFilterSection['pp'][$arrEvent['be_meldestelle']])) {
    $arrFilterSection['pp'][$arrEvent['be_meldestelle']] = $strMeldestelle;
  }
  
  $intSb = array_search($arrEvent['be_status_bearbeitung'], $arrStatusBearbeitung);
  $strClassAdd.= ' sb_' .$intSb;
  if (!isset($arrFilterSection['sb'][$intSb]) && ($intSb !== false)) {
    //$arrFilterSection['sb'][$intSb] = $arrEvent['be_status_bearbeitung'];
  }
  
  $intEr = array_search($arrEvent['be_ergebnis'], $arrErgebnis);
  $strClassAdd.= ' er_' .$intEr;
  if (!isset($arrFilterSection['er'][$intEr]) && ($intEr !== false)) {
    $arrFilterSection['er'][$intEr] = $arrEvent['be_ergebnis'];
  }
  
  $intBefristet = 2;
  $strBesfistet = '';
  if ($arrEvent['be_befristet'] != '0000-00-00') { 
    $strBesfistet = strConvertDate($arrEvent['be_befristet'], 'Y-m-d', 'd.m.Y');
    if (strtotime($arrEvent['be_befristet']) < strtotime(date('Y-m-d'))) {
      $intBefristet = 1;
    } else {
      $intBefristet = 0;
    }
  }
  $strClassAdd.= ' bb_' .$intBefristet;
  if (!isset($arrFilterSection['bb'][$intBefristet])) {
    $arrFilterSection['bb'][$intBefristet] = $arrBefristet[$intBefristet];
  }
  
  $intRueckmeldung = 0;
  $strRueckmeldung = '';
  if ($arrEvent['be_datum_rueckmeldung'] != '0000-00-00') { 
    $strRueckmeldung = strConvertDate($arrEvent['be_datum_rueckmeldung'], 'Y-m-d', 'd.m.Y');
    $intRueckmeldung = 1;
  }
  
  $strGroupId = $arrMeldestelleGruppe[$arrEvent['be_meldestelle']]['Group'];

  if (isset($arrEscalation[$strGroupId])) { // 'a083000000D4hUMAAZ' https://izs.smartgroups.io/frontend/main/topic/topic-97f48d16-eb79-48c7-b450-6085ab217700

    //$arrEscalation[$strGroupId] = $arrEscalation['a083000000D4hUMAAZ'];

    preg_match('/topic-[\w-]*/', $arrEscalation[$strGroupId]['Eskalation_Link__c'], $arrLink);
    $strSgLink = $arrLink[0] ?? '';

    $strKey = 'ja';
    $strEsc = '<a href="javascript:void(0)" alt="' .$arrEscalation[$strGroupId]['Eskalation_Grund__c'] .'" title="' .$arrEscalation[$strGroupId]['Eskalation_Grund__c'] .'" onclick="openSG(\'' .$strSgLink .'\')" style="color:red;"><i class="icon fa fa-warning" aria-hidden="true"></i></a>';
  } else {
    $strKey = 'nein';
    $strEsc = '';
  }
  $strClassAdd.= ' es_' .$strKey;
  if (!isset($arrFilterSection['es'][$strKey])) {
    $arrFilterSection['es'][$strKey] = 'es_' .$strKey;
  }

  $intNs = array_search($arrEvent['be_stundung'], $arrStundung);
  $strClassAdd.= ' rs_' .$intNs;
  if (!isset($arrFilterSection['rs'][$intNs]) && ($intNs !== false)) {
    $arrFilterSection['rs'][$intNs] = $arrEvent['be_stundung'];
  }

  $intSk = array_search($arrEvent['be_meldepflicht'], $arrMeldepflicht);
  $strClassAdd.= ' mp_' .$intSk;
  if (!isset($arrFilterSection['mp'][$intSk]) && ($intSk !== false)) {
    $arrFilterSection['mp'][$intSk] = $arrEvent['be_meldepflicht'];
  }
     
  $intSk = array_search($arrEvent['be_klaerung_status'], $arrStatusKlaerung);
  $strClassAdd.= ' sk_' .$intSk;
  if (!isset($arrFilterSection['sk'][$intSk]) && ($intSk !== false)) {
    $arrFilterSection['sk'][$intSk] = $arrEvent['be_klaerung_status'];
  }

  $intRueckmeldung = 0;
  if ($arrEvent['be_datum_rueckmeldung'] != '0000-00-00') { 
    $intRueckmeldung = 1;
  }
  $strClassAdd.= ' st_' .$intRueckmeldung;
  if (!isset($arrFilterSection['st'][$intRueckmeldung])) {
    $arrFilterSection['st'][$intRueckmeldung] = $arrRueckmeldung[$intRueckmeldung];
  }

  if ($arrEvent['be_ergebnis'] == 'OK') {
    $strFalsch = '<button type="" class="btn wrong proof" id="be_' .$arrEvent['beid'] .'_k">Falsch </button>';
  } else {
    $strFalsch = '';
  }

  $strNegativ = '';
  if (isset($arrListNvm[$arrEvent['be_meldestelle']])) {
    $strNegativ = $arrListNvm[$arrEvent['be_meldestelle']]['Art_Negativmerkmal__c'];

    $intNs = array_search($arrListNvm[$arrEvent['be_meldestelle']]['Art_Negativmerkmal__c'], $arrNegativAll);
    $strClassAdd.= ' nm_' .$intNs;

    $arrFilterSection['nm'][$intNs] = $arrListNvm[$arrEvent['be_meldestelle']]['Art_Negativmerkmal__c'];
  }


  $strChatId = $objBgEvent->getSgId($arrEvent['beid']); //

  $arrList[] = array(
    'esc' => $strEsc,
    'box' => '<input type="checkbox" name="sel_' .$arrEvent['beid'] .'" id="sel_' .$arrEvent['beid'] .'" value="1">',
    'ID' => '<a href="/cms/index_neu.php?ac=bg_detail&beid=' .$arrEvent['beid'] .'" target="_blank">' .$arrEvent['be_name'] .'</a><span class="hidden hfilter' .$strClassAdd .'">',
    'chat' => '<a class="sg" href="https://izs.smartgroups.io/topic/' .$strChatId .'" onclick="openSG(\'' .$strChatId .'\');"><img src="https://izs.smartgroups.io/img/logo/logo_32.png" height="20"></a>',
    'Berufsgenossenschaft' => '<a href="/cms/index_neu.php?ac=contacts&acid=' .$arrEvent['be_bg'] .'" target="_blank">' .$arrBGList[$arrEvent['be_bg']] .'</a>',
    'Meldestelle' => $strMeldestelle,
    'companyno' => $arrEvent['be_unternehmensnummer'],
    'action1' => '<button type="" class="btn right proof" id="be_' .$arrEvent['beid'] .'_ok">Richtig </button>', 
    'action2' => $strFalsch,
    'rueck' => $strRueckmeldung,
    'Befristet' => $strBesfistet,
    'Ergebnis' => $arrEvent['be_ergebnis'],
    'Stundung' => $arrEvent['be_stundung'],
    'Meldepflicht' => $arrEvent['be_meldepflicht'],
    'negativ' => $strNegativ,
    'kStatus' => $arrEvent['be_klaerung_status'],
    'kGrund' => $arrEvent['be_klaerung_grund'],
    'Info' => $arrEvent['be_info_intern'],
    'Bearbeiter' => $arrBearbeiter[$arrEvent['be_bearbeiter']] ?? '',
    //'Aktion' => '<a href="javascript: void(0);" class="release" rel="' .$arrEvent['beid'] .'" id="re_' .$arrEvent['beid'] .'">veröffentlichen</a>'
  );

}

$arrOpt = array(
  'strOrder' => '"order": [[ 2, "asc" ]]'
);

$strBgTable = strCreateTable($arrList, $arrTableHead, '', false, true, [], $arrOpt);

$strIncOutput.= ' 

    <script>
    function openSG(topicId) {
      if (topicId != "") {
        setTimeout(()=>smartchat.cmd("topic.show", {entity: {id: topicId}}), 150);
      }
    }
    </script>

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Events für Berufsgenossenschaften gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strBgTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
    
    
    <!-- Modal -->
    <div class="modal fade" id="Klaerung" aria-hidden="true" aria-labelledby="Klaerung"
    role="dialog" tabindex="-1">
      <div class="modal-dialog modal-center">
        <input type="hidden" name="beid_k" id="beid_k" value="">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">x</span>
            </button>
            <h4 class="modal-title">Klärungsfall erfassen</h4>
          </div>
          <div class="modal-body">
            <p>&nbsp;</p>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>
    <!-- End Modal -->


                        <!-- Modal -->
                        <div class="modal fade modal-danger" id="ModalDanger" aria-hidden="true"
                        aria-labelledby="ModalDanger" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-center smallw">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Achtung</h4>
                              </div>
                              <div class="modal-body">
                                <p id="errMessage">Text</p>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Modal -->
    


';


$strJsFootScript.= '  <script src="js/script_neu.js"></script>' .chr(10);

$strJsFootCodeRun.= '


$(document).on("click", ".proof", function() {

  var id = $(this).attr("id");
  var part = id.split("_");

  if (part[2] == "ok") {

    $.ajax({
      type: "POST",
      url:  "action/ajax.php",
      dataType: "text", 
      data: { ac: "bgRelease", beid: part[1] },
      success: function(result) {
        var jsonData = $.parseJSON(result);
        
        if (jsonData["Status"] == "OK") {
          
          $("#be_" + jsonData["beid"] + "_ok").parent().parent().hide();
          $("#DocCount").html(parseInt($("#DocCount").html()) - 1);
              
        }
        
        if (jsonData["Status"] == "FAIL") {

          $("#ModalDanger").modal("show");
          $("#ModalDanger #errMessage").html(jsonData["Reason"]);

        }

      }

    });

  } else {

    $("#beid_k").val(part[1]);
    $("#Klaerung").modal("show");

  }

});

// Fill modal with content from link href
$("#Klaerung").on("shown.bs.modal", function(e) {
    var beid = $("#beid_k").val();
    $(this).find(".modal-content").load("action/modal.php?ac=klaerung&beid=" + beid, function () {

      $(".modal .dp").datepicker();

    });        
});

$(document).on("click", "#svKlaerung", function(){
  
  var beid = $("#beid_k").val()

  var arrFields = {
    be_rueckstand:         $("#rr_" + beid).val(),
    be_wiedervorlage:      $("#wi_" + beid).val(),
    be_klaerung_grund:     $("#gr_" + beid).val(),
    be_klaerung_schritt:   $("#ns_" + beid).val(),
    be_klaerung_ap:        $("#ap_" + beid).val(),
    be_klaerung_telefon:   $("#at_" + beid).val(),
    be_info_intern:        $("#ii_" + beid).val(),
    be_datum_rueckmeldung: $("#dr_" + beid).val(),
    be_klaerung_status:    $("#sk_" + beid).val(),
    be_stundung:           $("#st_" + beid).val(),
    be_meldepflicht:       $("#me_" + beid).val(),
    be_bearbeiter:         $("#us_" + beid).val(),
    be_status_bearbeitung: "erhalten - Klärungsfall"
  };

  $.ajax({
    type: "POST",
    url:  "action/ajax.php",
    dataType: "text", 
    data: { ac: "svKlaerungClick", beid: beid, values: arrFields },
    success: function(result) {
      var jsonData = $.parseJSON(result);
      
      if (jsonData["Status"] == "OK") {
        
        $("#Klaerung").modal("hide");
        $("#be_" + beid + "_k").parent().parent().hide()
            
      }
      
      if (jsonData["Status"] == "FAIL") {

        $("#ModalDanger").modal("show");
        $("#ModalDanger #errMessage").html(jsonData["Reason"]);

      }

    }

  });

});




    $(".release").click(function(){
      var beid = $(this).attr("rel");

      $.ajax({
        type: "POST",
        url:  "action/ajax.php",
        dataType: "text", 
        data: { ac: "bgRelease", beid: beid },
        success: function(result) {
          var jsonData = $.parseJSON(result);
          
          if (jsonData["Status"] == "OK") {
            
            $("#re_" + jsonData["beid"]).parent().parent().hide();
            $("#DocCount").html(parseInt($("#DocCount").html()) - 1);
          	//$("#Auskunft").modal("hide");
          	//location.reload();
                
          }
          
          if (jsonData["Status"] == "FAIL") {
  
            $("#ModalDanger").modal("show");
            $("#ModalDanger #errMessage").html(jsonData["Reason"]);
  
          }
  
        }
  
      });

      
    });

    $(".sg").on( "click", function( event ) {
      event.preventDefault();
    });

';

?>