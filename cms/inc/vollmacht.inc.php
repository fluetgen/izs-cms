<?php

//REQUEST

$strIP    = $_REQUEST['acid'];
$strMonth = $_REQUEST['m'];
$strYear  = $_REQUEST['y'];

//DATA

$objIP = new InformationProvider;
$arrIP = $objIP->arrGetIP($strIP);

$arrDiff = array();
if ($arrIP[0]['ac_org'] == 'true') {
	$arrDiff = $objIP->arrGetNewVollmachtList($strIP, $strMonth, $strYear);
}

//print_r($arrDiff); die();

//SETTINGS

$boolShowHead = false;
$boolShowSide = false;

$strPageTitle = $arrIP[0]['ac_name'] .' - Vollmachten ';

//CONTENT

$strOutput = '
  <!-- Page -->
  <div class="animsition container-fluid">
';

$strOutput.= strCreatePageTitle('Vollmachten', $arrIP[0]['ac_name']);

$arrTableHead = array(
  'Betriebsnummer', 
  'PremiumPayer', 
  'CompanyGroup' 
);

$arrDiffList = array();
foreach ($arrDiff as $intRow => $strBtnr) {
  
  $objPP = new PremiumPayer;
  $arrPP = $objPP->arrGetPPFromBetriebsnummer($strBtnr);
  
  $objGroup = new Group;
  $arrGroup = $objGroup->arrGetGroup($arrPP[0]['ac_cgid']);
  
  $arrDiffList[] = array(
    'Betriebsnummer'  => $strBtnr,
    'PremiumPayer'    => $arrPP[0]['ac_name'],
    'CompanyGroup' => $arrGroup[0]['gr_name']
  );
}

$strVollmachtTable = strCreateTable($arrDiffList, $arrTableHead, '', false, true);

$strOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title">Original-Vollmacht nötig</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strOutput.= $strVollmachtTable;

$strOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';


$strOutput.= '
  </div>
  <!-- End Page -->
';

?>