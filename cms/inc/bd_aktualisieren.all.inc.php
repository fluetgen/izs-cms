<?php

//CLASSES
require_once('classes/class.document.php');
require_once(APP_PATH .'cms/classes/class.stream.php');
require_once(APP_PATH .'cms/classes/class.additional.php');

require_once(APP_PATH .'cms/classes/class.escalation.php');

require_once(APP_PATH .'cms/classes/class.account.php');

//DATA
$objAccount = new Account;
$arrAccListRaw = $objAccount->arrGetAccountList();

$arrAccList = array();
if (count($arrAccListRaw) > 0) {
  foreach ($arrAccListRaw as $intKey => $arrAcc) {
    $arrAccList[$arrAcc['acid']] = $arrAcc['ac_name'];
  }
}

if ($_SESSION['id'] == 3) {
  //print_r($arrAccList);
}

$objDocument = new Document;
$arrDocumentList = $objDocument->arrGetDocumentUpdateList();
$arrUpdateStatus = $objDocument->arrGetUpdateStatus();
$arrNextStatus   = $objDocument->arrGetNextStatus();
$intMaxUpdStatus = $objDocument->intGetMaxUpdateStatus();

$objDocument = new Document;
$objAdd      = new Additional;

$objEsc      = new Escalation;
$arrEscList  = $objEsc->getEscalationList();

//REQUEST
if (($strFu == 'request') && (count($arrDocumentList) > 0)) {
  $arrFilteredList = array();
  foreach($arrDocumentList as $intKey => $arrDocument) {
      if (!in_array($arrDocument['dd_ustatus'], array('Anfrage zurückgestellt', 'Anfrage prüfen', 'Keine Reaktion'))) {
        $arrFilteredList[] = $arrDocument;
      }
  }
  $arrDocumentList = $arrFilteredList;
}

if (($strFu == 'proove') && (count($arrDocumentList) > 0)) {
    $arrFilteredList = array();
    foreach($arrDocumentList as $intKey => $arrDocument) {
        if ($arrDocument['dd_ustatus'] == 'Anfrage prüfen') $arrFilteredList[] = $arrDocument;
    }
    $arrDocumentList = $arrFilteredList;
}

if (($strFu == 'back') && (count($arrDocumentList) > 0)) {
  $arrFilteredList = array();
  foreach($arrDocumentList as $intKey => $arrDocument) {
      if ($arrDocument['dd_ustatus'] == 'Anfrage zurückgestellt') $arrFilteredList[] = $arrDocument;
  }
  $arrDocumentList = $arrFilteredList;
}

if (($strFu == 'noreact') && (count($arrDocumentList) > 0)) {
  $arrFilteredList = array();
  foreach($arrDocumentList as $intKey => $arrDocument) {
      if ($arrDocument['dd_ustatus'] == 'Keine Reaktion') $arrFilteredList[] = $arrDocument;
  }
  $arrDocumentList = $arrFilteredList;
}

if ($strFu == 'esc') {
    $arrFilteredList = array();
    $arrDocumentList = $arrFilteredList;
}

//SETTINGS

$arrNextStatus = $objDocument->arrGetNextStatus();
$arrNextStatusFlip = array_flip($arrNextStatus);

$arrUpdStaAll = $objDocument->arrGetUpdateStatus();

$arrErhalten = array(
    'false' => 'nein',
    'true'  => 'ja'
);

$boolShowHead = true;
$boolShowSide = false;

$strPageTitle = 'Basisdokumentation aktualisieren';


$arrTableHead = array(
  '<input type="checkbox" id="selall">',
  'Esc',
  'Event-ID', 
  'Erhalten',
  'CompanyGroup', 
  'Meldestelle',
  'Typ',
  'Ablauf',
  'Update-Status',
  'am',
  'Frist',
  'Nächste Aktion',
  'Info intern',
  'Info extern',
  'Ampel-Status',
);

if ($strFu == 'noreact') {
  $arrTableHead[0] = '';
}

$arrGroupList = array();
$arrDTypeList = $objDocument->arrGetDTypeUpdate();

$arrDocList = array();
$arrDocListExport = array();
foreach ($arrDocumentList as $intRow => $arrDocument) {

  if ($_SESSION['id'] == 3) {
    //print_r($arrDocument);
  }

  $strClassAdd = '';
  
  $arrReceived = $objAdd->arrGetElement('Dokument_Dokumentation__c', $arrDocument['ddid'], 'received');
  
  if (count($arrReceived) > 0) {
    $arrDocument['dd_received'] = $arrReceived[0]['ad_value'];
  }
  
  if (!in_array($arrDocument['dd_grid'], $arrGroupList)) {
    $objGroup = new Group;
    $arrGroup = $objGroup->arrGetGroup($arrDocument['dd_grid']);
    $strGroup = $arrGroup[0]['gr_name'];
    $arrGroupList[$arrDocument['dd_grid']] = $strGroup;
  } else {
    $strGroup = $arrGroupList[$arrDocument['dd_grid']];
  }
  
  $arrDocStatus = $objDocument->arrGetDocumentStatus($arrDocument['ddid']);
  
  $strAm = '';
  $strEscDate = '';
  $strDeadlineClass = '';
  $strAddClassCb = '';

  if ((count($arrDocStatus) > 0) && ($arrDocument['dd_ustatus'] != '')) {
    $strAm = strConvertDate($arrDocStatus[0]['bc_time'], 'Y-m-d H:i:s', 'd.m.Y');
    $strEscDate = strAddWorkingDays((strConvertDate($strAm, 'd.m.Y', 'U')), $objDocument->intGetEscalation($arrDocument['dd_ustatus']));
    if (strConvertDate($strEscDate, 'd.m.Y', 'U') <= strConvertDate(date('d.m.Y'), 'd.m.Y', 'U')) {
      $strDeadlineClass = 'red-600';
      $strAddClassCb = ' due';
    }
  }
  
  $strStatus = $objDocument->strGetNewUpdateAction($arrDocument['dd_ustatus']);
  
  //erneut versenden
  if ($arrDocument['dd_ustatus'] == $arrUpdateStatus[5]) {
    $strStatus = $arrNextStatus[5];
    $strEscDate = '';
  }

  $strUpdateStatus = $arrDocument['dd_ustatus'];
  if (substr($strUpdateStatus, 1, 1) == '.') {
    $strUpdateStatus.= ' versandt';
  } 
  
  $strAddClass = '';

  $strNextAction = '';
  $strCheckBox = '<input type="checkbox" id="dok_' .$arrDocument['ddid'] .'" class="docdoc' .$strAddClassCb .'">';
  if ($strStatus != '') {
    $strNextAction = '<span class="hidden">order_' .$arrNextStatusFlip[$strStatus] .'</span><a title="' .$strStatus .'" id="' .$arrDocument['ddid'] .'" class="bgaction">' .$strStatus .'</a>';
  } elseif ($arrDocument['dd_ustatus'] == $arrUpdateStatus[7]) {
    $strAddClass = 'warning';
    $strCheckBox = '';
  } elseif ($arrDocument['dd_ustatus'] == $arrUpdateStatus[8]) {
    $strAddClass = 'info';
    $strCheckBox = '';
  }
  if ($strFu == 'noreact') {
    $strCheckBox   = '';
    $strNextAction = '';
  }

  $strClassAdd.= ' pp_' .$arrDocument['dd_grid'];
  if (!isset($arrFilterSection['pp'][$arrDocument['dd_grid']])) {
    $arrFilterSection['pp'][$arrDocument['dd_grid']] = $strGroup;
  }

  $intSt = array_search($arrDocument['dd_ustatus'], $arrUpdStaAll);
  $strClassAdd.= ' up_' .$intSt;
  if (!isset($arrFilterSection['up'][$intSt]) && ($intSt !== false)) {
    if ($strUpdateStatus == '') $strUpdateStatusSelect = '- ohne -';
    else $strUpdateStatusSelect = $strUpdateStatus;
    $arrFilterSection['up'][$intSt] = $strUpdateStatusSelect;
  }

  $strTy = $arrDocument['dd_rtype'];
  $strClassAdd.= ' ty_' .$strTy;
  if (!isset($arrFilterSection['ty'][$strTy]) && ($strTy !== false)) {
    $arrFilterSection['ty'][$strTy] = $arrDTypeList[$arrDocument['dd_rtype']];
  }

  $intNs = array_search($strStatus, $arrNextStatus);
  $strClassAdd.= ' ns_' .$intNs;
  if (!isset($arrFilterSection['ns'][$intNs]) && ($intNs !== false)) {
    if ($strStatus == '') $strStatusSelect = '- ohne -';
    else $strStatusSelect = $strStatus;
    $arrFilterSection['ns'][$intNs] = $strStatusSelect;
  }

  $strEr = $arrDocument['dd_received'];
  $strClassAdd.= ' er_' .$strEr;
  if (!isset($arrFilterSection['er'][$strEr]) && ($strEr !== false)) {
    $arrFilterSection['er'][$strEr] = $arrErhalten[$arrDocument['dd_received']];
  }

  $strEsc = '';
  if (isset($arrEscList[$arrDocument['dd_grid']])) {
    $strEsc = '<a target="_blank" href="' .$arrEscList[$arrDocument['dd_grid']]['Eskalation_Link__c'] .'" title="' .$arrEscList[$arrDocument['dd_grid']]['Eskalation_Grund__c'] .'" alt="' .$arrEscList[$arrDocument['dd_grid']]['Eskalation_Grund__c'] .'" style="color:orangered;"><i class="fa fa-exclamation-triangle"></i></a>';
  }

  $strAccName = isset($arrAccList[$arrDocument['dd_acid']]) ? $arrAccList[$arrDocument['dd_acid']] : '';

  if ($strAccName != '') {
    $strClassAdd.= ' ac_' .$arrDocument['dd_acid'];
    if (!isset($arrFilterSection['ac'][$arrDocument['dd_acid']])) {
      $arrFilterSection['ac'][$arrDocument['dd_acid']] = $strAccName;
    }
  }

  $strReceived = '';
  if ($arrDocument['dd_received'] == 'true') {
    $strReceived = ' checked="checked"';
    
    $arrDocList[] = array(
      $strCheckBox,
      $strEsc,
      'Event-ID' => '<i>' .'<a title="' .$arrDocument['dd_name'] .'" data-toggle="modal" data-target="#DetailModal" href="action/ajax.php?ac=bd_detail&ddid=' .$arrDocument['ddid'] .'">' .$arrDocument['dd_name'] .'</a>' .'</i>'  .'</a><span class="hidden hfilter' .$strClassAdd .' ' .$strAddClass .'"></span>', 
      'erhalten' => '<span class="hidden">order_1</span><i><input type="checkbox" id="dd_' .$arrDocument['ddid'] .'" value="1"' .$strReceived .'></i>',
      'CompanyGroup' => '<i>' .$strGroup .'</i>', 
      '' .$strAccName,
      'TypISSET(' => '<i>' .$arrDTypeList[$arrDocument['dd_rtype']] .'' .'</i>',
      'Ablauf' => '<i>' .'<span class="hidden">' .strConvertDate($arrDocument['dd_end'], 'Y-m-d', 'Ymd') .'</span>' .strConvertDate($arrDocument['dd_end'], 'Y-m-d', 'd.m.Y') .'' .'</i>',
      'Update-Status' => '<i>' .'<span title="' .$strUpdateStatus .'" id="st_' .$arrDocument['ddid'] .'">' .$strUpdateStatus .'</span>' .'</i>',
      'am' => '<i>' .'<span class="hidden" id="dah_' .$arrDocument['ddid'] .'">' .strConvertDate($strAm, 'd.m.Y', 'Ymd') .'</span><span id="da_' .$arrDocument['ddid'] .'">' .$strAm .'</span>' .'</i>',
      'Frist' => '<i>' .'<span class="hidden" id="esh_' .$arrDocument['ddid'] .'">' .strConvertDate($strEscDate, 'd.m.Y', 'Ymd') .'</span><span class="' .$strDeadlineClass .'" id="es_' .$arrDocument['ddid'] .'">' .$strEscDate .'</span>' .'</i>',
      'Nächste Aktion' => '<i>' .$strNextAction .'</i>',
      'Info intern' => '<i>' .str_replace(chr(10), '<br />', $arrDocument['dd_info_intern']) .'</i>',
      'Info extern' => '<i>' .str_replace(chr(10), '<br />', $arrDocument['dd_ivisible']) .'</i>',
      'ampel' => $arrDocument['bd_status'],
    );
  
  } else {
    
    $arrDocList[] = array(
      $strCheckBox,
      $strEsc,
      'Event-ID' => '<a title="' .$arrDocument['dd_name'] .'" data-toggle="modal" data-target="#DetailModal" href="action/ajax.php?ac=bd_detail&ddid=' .$arrDocument['ddid'] .'">' .$arrDocument['dd_name'] .'</a>'  .'</a><span class="hidden hfilter' .$strClassAdd .' ' .$strAddClass .'"></span>', // .'<span class="hidden ' .$strAddClass .'"></span>'
      'erhalten' => '<span class="hidden">order_0</span><input type="checkbox" id="dd_' .$arrDocument['ddid'] .'" value="ja"' .$strReceived .'>',
      'CompanyGroup' => $strGroup, 
      '' .$strAccName,
      'Typ' => $arrDTypeList[$arrDocument['dd_rtype']] .'',
      'Ablauf' => '<span class="hidden">' .strConvertDate($arrDocument['dd_end'], 'Y-m-d', 'Ymd') .'</span>' .strConvertDate($arrDocument['dd_end'], 'Y-m-d', 'd.m.Y') .'',
      'Update-Status' => '<span title="' .$strUpdateStatus .'" id="st_' .$arrDocument['ddid'] .'">' .$strUpdateStatus .'</span>',
      'am' => '<span class="hidden" id="dah_' .$arrDocument['ddid'] .'">' .strConvertDate($strAm, 'd.m.Y', 'Ymd') .'</span><span id="da_' .$arrDocument['ddid'] .'">' .$strAm .'</span>',
      'Frist' => '<span class="hidden" id="esh_' .$arrDocument['ddid'] .'">' .strConvertDate($strEscDate, 'd.m.Y', 'Ymd') .'</span><span class="' .$strDeadlineClass .'" id="es_' .$arrDocument['ddid'] .'">' .$strEscDate .'</span>',
      'Nächste Aktion' => $strNextAction,
      'Info intern' => str_replace(chr(10), '<br />', $arrDocument['dd_info_intern']),
      'Info extern' => '<span id="infoe_' .$arrDocument['ddid'] .'"><i>' .str_replace(chr(10), '<br />', $arrDocument['dd_ivisible']) .'</i></span>',
      'ampel' => $arrDocument['bd_status'],
    );
  
  }
  
}

$arrOpt = array(
    'boolHasContext' => true,
    'arrNoSort' => array(0),
    'strOrder' => '"order": [[ 5, "desc" ]]'
);

$strDocTable = strCreateTable($arrDocList, $arrTableHead, '', false, true, array(), $arrOpt);


$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading col-lg-12">
        <h3 class="panel-title"><span id="DocCount">' .count($arrDocList) .'</span> Dokumente abgelaufen</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strDocTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

$strJsFootCodeRun.= '

$("#massFunction").on("click", function(e) {
  $("#modalMassFunctionSubmit").show();
  $("#modalMassFunction").modal("show");
});

$("#modalMassFunctionSubmit").on("click", function(e) {

  $(".docdoc:checked").each(function () { 
    var ddid = $(this).attr("id").split("dok_")[1];
    var data = { "ddid" : ddid };
    var tr   = $(this).parent().parent().hide();
    $.post( "action/ajax.php", { ac: "ddStatusClickMass", data: data, url: "action/ajax.php" } );
    $("#DocCount").text(parseInt($("#DocCount").text()) - 1)
  });

  $("#modalMassFunctionSubmit").hide();
  $(".msg").text("Die gewählten Aktionen werden ausgeführt.");

});

$("#modalMassFunction").on("show.bs.modal", function(e) {

  var noc = $(".docdoc:checked").length;
  var textMessage = "";

  if (noc == 0) {
    textMessage = "Bitte wähle ein Dokument aus.";
    $("#modalMassFunctionSubmit").hide();
  } else {
    if (noc == 1) textMessage = "Willst Du wirklich " + noc + " Aktion ausführen?";
    else textMessage = "Willst Du wirklich " + noc + " Aktionen ausführen?";
  }

  $(".msg").text(textMessage);

});
';

$strJsFootCodeRun.= "

$('#selall').click(function (event) {
  if (this.checked) {
      $('.docdoc').each(function () { //loop through each checkbox
          $(this).prop('checked', true); //check 
      });
  } else {
      $('.docdoc').each(function () { //loop through each checkbox
          $(this).prop('checked', false); //uncheck              
      });
  }
});

";


?>