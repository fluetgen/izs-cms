<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

session_start();

require_once('../const.inc.php');

require_once('../../assets/classes/class.mysql.php');
require_once('../../assets/classes/class.database.php');

require_once('refactor.inc.php');

$intUser   = $_SESSION['id'];
$strAction = $_REQUEST['ac'];

$arrHeader = array(
  'user: ' .$intUser,
  'apikey: 9b0d3b1c-27f2-4e40-8e4d-d735a50dbbee',
  'Accept: application/json'
);

if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
  $arrHeader[] = 'test: true';
}



if ($strAction == 'create') {

    $arrWorkData = array(
        'case_bnr_pdl_companygroup'  => @$_REQUEST['cg'],
        'case_bnr_nr'                => @$_REQUEST['bnr'],
        'case_bnr_clarificationtype' => @$_REQUEST['ty'],
    );

    $arrRes = curl_post('http://api.izs-institut.de/api/work/create', $arrWorkData, array(), $arrHeader);
    $arrWork = json_decode($arrRes, true);

    echo 1;

}




?>