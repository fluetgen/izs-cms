<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

session_start();

require_once('../const.inc.php');
require_once('../../assets/classes/class.mysql.php');
require_once('refactor.inc.php');

$intId   = $_REQUEST['id'];

$strToken = 'triple2013';

if (isset($_SESSION['id']) && isset($_SESSION['token'])) {
  if ($_SESSION['token'] != MD5($strToken .'_' .$_SESSION['id'])) {
    $boolLogin = false;
    exit;
  } else {
    $boolLogin = true;
  }
} else {
  $boolLogin = false;
  exit;
}

$arrHeader = array(
  'user: ' .$_SESSION['id'],
  'Accept: application/json'
);

$arrHeader[] = 'apikey: 9b0d3b1c-27f2-4e40-8e4d-d735a50dbbee';

$arrRes = curl_get('http://api.izs-institut.de/api/easy/documents/' .$intId .'', array(), array(), $arrHeader);
$strName = json_decode($arrRes, true)['number'] .'.pdf';

$binRes = curl_get('http://api.izs-institut.de/api/easy/documents/' .$intId .'/pdf', array(), array(), $arrHeader);

header('Content-Type: application/pdf');
header('Content-Length: '.strlen( $binRes ));
header('Content-disposition: inline; filename="' . $strName . '"');
header('Cache-Control: public, must-revalidate, max-age=0');
header('Pragma: public');

echo $binRes;


?>