<?php

//CLASSES
require_once('classes/class.document.php');
require_once(APP_PATH .'cms/classes/class.stream.php');
require_once(APP_PATH .'cms/classes/class.additional.php');


//REQUEST


//DATA

$objDocument = new Document;
$arrDocumentList = $objDocument->arrGetDocumentUpdateList();
$arrUpdateStatus = $objDocument->arrGetUpdateStatus();
$arrNextStatus   = $objDocument->arrGetNextStatus();
$intMaxUpdStatus = $objDocument->intGetMaxUpdateStatus();

$objDocument = new Document;
$objAdd      = new Additional;

//SETTINGS

$arrNextStatus = $objDocument->arrGetNextStatus();
$arrNextStatusFlip = array_flip($arrNextStatus);

$boolShowHead = true;
$boolShowSide = false;

$strPageTitle = 'Basisdokumentation aktualisieren';

//CONTENT

$strOutput = '
  <!-- Page -->
  <div class="animsition container-fluid">
';

$strOutput.= strCreatePageTitle($strPageTitle);


$strOutput.= '
        <div style="z-index: 1;" class="page-header-actions">

          <div class="panel-actions">

            <div class="dropdown">
              <button type="button" class="btn btn-info dropdown-toggle pull-right" id="exampleAlignmentDropdown"
              data-toggle="dropdown" aria-expanded="true">
                Optionen
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="exampleAlignmentDropdown" role="menu">
                <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="doExport">Export CSV</a></li>
              </ul>
            </div>

          </div>

        </div>
';


$arrTableHead = array(
  'Event-ID', 
  'CompanyGroup', 
  'Typ',
  'Ablauf',
  'Update-Status',
  'am',
  'Frist',
  'Nächste Aktion',
  'Info intern',
  'Info extern',
  'Ampel-Status',
  'Erhalten'
);

$arrGroupList = array();
$arrDTypeList = $objDocument->arrGetDTypeUpdate();

$arrDocList = array();
$arrDocListExport = array();
foreach ($arrDocumentList as $intRow => $arrDocument) {

  if ($_SESSION['id'] == 3) {
    //print_r($arrDocument);
  }
  
  $arrReceived = $objAdd->arrGetElement('Dokument_Dokumentation__c', $arrDocument['ddid'], 'received');
  
  if (count($arrReceived) > 0) {
    $arrDocument['dd_received'] = $arrReceived[0]['ad_value'];
  }
  
  if (!in_array($arrDocument['dd_grid'], $arrGroupList)) {
    $objGroup = new Group;
    $arrGroup = $objGroup->arrGetGroup($arrDocument['dd_grid']);
    $strGroup = $arrGroup[0]['gr_name'];
    $arrGroupList[$arrDocument['dd_grid']] = $strGroup;
  } else {
    $strGroup = $arrGroupList[$arrDocument['dd_grid']];
  }
  
  $arrDocStatus = $objDocument->arrGetDocumentStatus($arrDocument['ddid']);
  
  $strAm = '';
  $strEscDate = '';
  $strDeadlineClass = '';
  if ((count($arrDocStatus) > 0) && ($arrDocument['dd_ustatus'] != '')) {
    $strAm = strConvertDate($arrDocStatus[0]['bc_time'], 'Y-m-d H:i:s', 'd.m.Y');
    $strEscDate = strAddWorkingDays((strConvertDate($strAm, 'd.m.Y', 'U')), $objDocument->intGetEscalation($arrDocument['dd_ustatus']));
    if (strConvertDate($strEscDate, 'd.m.Y', 'U') <= strConvertDate(date('d.m.Y'), 'd.m.Y', 'U')) {
      $strDeadlineClass = 'red-600';
    }
  }
  
  $strStatus = $objDocument->strGetNewUpdateAction($arrDocument['dd_ustatus']);
  
  //erneut versenden
  if ($arrDocument['dd_ustatus'] == $arrUpdateStatus[5]) {
    $strStatus = $arrNextStatus[5];
    $strEscDate = '';
  }

  $strUpdateStatus = $arrDocument['dd_ustatus'];
  if (substr($strUpdateStatus, 1, 1) == '.') {
    $strUpdateStatus.= ' versandt';
  } 
  
  $strAddClass = '';

  $strNextAction = '';
  if ($strStatus != '') {
    $strNextAction = '<span class="hidden">order_' .$arrNextStatusFlip[$strStatus] .'</span><a title="' .$strStatus .'" id="' .$arrDocument['ddid'] .'" class="bgaction">' .$strStatus .'</a>';
  } else {
    $strAddClass = 'warning';
  }
  
  $strReceived = '';
  if ($arrDocument['dd_received'] == 'true') {
    $strReceived = ' checked="checked"';
    
    $arrDocList[] = array(
      'Event-ID' => '<i>' .'<a title="' .$arrDocument['dd_name'] .'" data-toggle="modal" data-target="#DetailModal" href="action/ajax.php?ac=bd_detail&ddid=' .$arrDocument['ddid'] .'">' .$arrDocument['dd_name'] .'</a>' .'</i>'  .'<span class="hidden ' .$strAddClass .'"></span>', 
      'CompanyGroup' => '<i>' .$strGroup .'</i>', 
      'Typ' => '<i>' .$arrDTypeList[$arrDocument['dd_rtype']] .'' .'</i>',
      'Ablauf' => '<i>' .'<span class="hidden">' .strConvertDate($arrDocument['dd_end'], 'Y-m-d', 'Ymd') .'</span>' .strConvertDate($arrDocument['dd_end'], 'Y-m-d', 'd.m.Y') .'' .'</i>',
      'Update-Status' => '<i>' .'<span title="' .$strUpdateStatus .'" id="st_' .$arrDocument['ddid'] .'">' .$strUpdateStatus .'</span>' .'</i>',
      'am' => '<i>' .'<span class="hidden" id="dah_' .$arrDocument['ddid'] .'">' .strConvertDate($strAm, 'd.m.Y', 'Ymd') .'</span><span id="da_' .$arrDocument['ddid'] .'">' .$strAm .'</span>' .'</i>',
      'Frist' => '<i>' .'<span class="hidden" id="esh_' .$arrDocument['ddid'] .'">' .strConvertDate($strEscDate, 'd.m.Y', 'Ymd') .'</span><span class="' .$strDeadlineClass .'" id="es_' .$arrDocument['ddid'] .'">' .$strEscDate .'</span>' .'</i>',
      'Nächste Aktion' => '<i>' .$strNextAction .'</i>',
      'Info intern' => '<i>' .str_replace(chr(10), '<br />', $arrDocument['dd_info_intern']) .'</i>',
      'Info extern' => '<i>' .str_replace(chr(10), '<br />', $arrDocument['dd_ivisible']) .'</i>',
      'ampel' => $arrDocument['bd_status'],
      'erhalten' => '<span class="hidden">order_1</span><i><input type="checkbox" id="dd_' .$arrDocument['ddid'] .'" value="1"' .$strReceived .'></i>'
    );
  
  } else {
    
    $arrDocList[] = array(
      'Event-ID' => '<a title="' .$arrDocument['dd_name'] .'" data-toggle="modal" data-target="#DetailModal" href="action/ajax.php?ac=bd_detail&ddid=' .$arrDocument['ddid'] .'">' .$arrDocument['dd_name'] .'</a>'  .'<span class="hidden ' .$strAddClass .'"></span>', 
      'CompanyGroup' => $strGroup, 
      'Typ' => $arrDTypeList[$arrDocument['dd_rtype']] .'',
      'Ablauf' => '<span class="hidden">' .strConvertDate($arrDocument['dd_end'], 'Y-m-d', 'Ymd') .'</span>' .strConvertDate($arrDocument['dd_end'], 'Y-m-d', 'd.m.Y') .'',
      'Update-Status' => '<span title="' .$strUpdateStatus .'" id="st_' .$arrDocument['ddid'] .'">' .$strUpdateStatus .'</span>',
      'am' => '<span class="hidden" id="dah_' .$arrDocument['ddid'] .'">' .strConvertDate($strAm, 'd.m.Y', 'Ymd') .'</span><span id="da_' .$arrDocument['ddid'] .'">' .$strAm .'</span>',
      'Frist' => '<span class="hidden" id="esh_' .$arrDocument['ddid'] .'">' .strConvertDate($strEscDate, 'd.m.Y', 'Ymd') .'</span><span class="' .$strDeadlineClass .'" id="es_' .$arrDocument['ddid'] .'">' .$strEscDate .'</span>',
      'Nächste Aktion' => $strNextAction,
      'Info intern' => str_replace(chr(10), '<br />', $arrDocument['dd_info_intern']),
      'Info extern' => '<i>' .str_replace(chr(10), '<br />', $arrDocument['dd_ivisible']) .'</i>',
      'ampel' => $arrDocument['bd_status'],
      'erhalten' => '<span class="hidden">order_0</span><input type="checkbox" id="dd_' .$arrDocument['ddid'] .'" value="ja"' .$strReceived .'>'
    );
  
  }
  
}

$strDocTable = strCreateTable($arrDocList, $arrTableHead, '', false, true);

/*
              <div class="page-header">
                <h1 class="page-title">Page Title</h1>
                <div class="page-header-actions">
                  <button type="button" class="btn btn-sm btn-primary btn-round">
                    <span class="text hidden-xs">Settings</span>
                    <i class="icon md-chevron-right" aria-hidden="true"></i>
                  </button>
                </div>
              </div>

*/

$strOutput.= ' 

    <div class="panel">
      <header class="panel-heading col-lg-12">
        <h3 class="panel-title"><span id="DocCount">' .count($arrDocList) .'</span> Dokumente abgelaufen</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strOutput.= $strDocTable;

$strOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';


$strOutput.= '
  </div>
  <!-- End Page -->
';

$strOutput.= '
                        <!-- Modal -->
                        <div class="modal fade" id="DetailModal" aria-hidden="true" aria-labelledby="ChangelogSidebar"
                        role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-center">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">x</span>
                                </button>
                                <h4 class="modal-title">Changelog</h4>
                              </div>
                              <div class="modal-body">
                                <p>Body.</p>
                              </div>
                              <div class="modal-footer">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Modal -->
                        
                        <!-- Modal -->
                        <div class="modal fade" id="Archive" aria-hidden="true" aria-labelledby="TaskSidebar"
                        role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-center smallw">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">x</span>
                                </button>
                                <h4 class="modal-title">Ablegen</h4>
                              </div>
                              <div class="modal-body">
                                <p>Bitte wähle den Ablegegrund:</p>

                                <select class="selectpicker" name="ArchiveAction">
                                  <option>Keine Reaktion</option>
                                  <option>Kein Ersatzdokument erforderlich</option>
                                </select>
                                
                              </div>
                              <div class="modal-footer">
                              
                                <input type="hidden" name="ddid" value="" id="ddid">

                                <button data-dismiss="modal" class="btn btn-default" type="button">Abbbrechen</button>
                                <button id="doArchiv" class="btn btn-primary" type="button">Ablegen</button>
                              
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Modal -->

                        <!-- Modal -->
                        <div class="modal fade modal-danger" id="ModalDanger" aria-hidden="true"
                        aria-labelledby="ModalDanger" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-center smallw">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Achtung</h4>
                              </div>
                              <div class="modal-body">
                                <p id="errMessage">Text</p>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Modal -->


<form method="post" action="_get_csv.php" id="export">
  <input type="hidden" name="strTable" id="strTable" value="">
</form>


';

$strJsFootScript.= '  <script src="js/script_neu.js"></script>' .chr(10);

$strJsFootCodeRun.= '

$("span.warning").parent().parent().addClass("warning");
$("span.danger").parent().parent().addClass("danger");

  
      $("[id^=\'dd_\']").on("change", function(e) {
      
        var checked = $(this).prop("checked");
        
        if (checked) {
          $(this).parent().parent().css("font-style", "italic");
          $(this).parent().find("span").text("order_1");
        } else {
          $(this).parent().parent().css("font-style", "normal");
          $(this).parent().find("span").text("order_0");
        }

        $(this).parent().parent().parent().parent().DataTable().rows().invalidate("dom");
      
        $.post("action/additional.ajax.php", { 
            ac: "addValue", table: "Dokument_Dokumentation__c", id: $(this).prop("id"), key: "received", value: checked
          }).done(function(data) {
            ;
          }
        );
        
      });  

  
      $("#doExport").on("click", function(e) {
        
        $("input[type=checkbox]").each(function() { 
          $(this).attr("csvchecked", $(this).is(":checked")); 
        });
        
        var table_hd = $($("table")[0]).html();
        var table_cth = $("table.table:eq(1) tr:visible");
        var table_ct = "";
        
        if (table_cth.length > 0) {
          $.each(table_cth, function(k, v) {
            table_ct += "<tr>" + $(v).html() + "</tr>";
          });
        }
        
        $("#strTable").val("<table>" + table_hd + table_ct + "</table>");
        $("#export").submit();
        
      });  

      // Fill modal with content from link href
      $("#DetailModal").on("show.bs.modal", function(e) {
          var link = $(e.relatedTarget);
          $(this).find(".modal-content").load(link.attr("href"));
      });
      
 	    //$("selectpicker").selectpicker();

      $("#doArchiv").click(function(){

        var text = $(".selectpicker option:selected").val()
        var id = $("#ddid").val();

        $(this).parent().parent().fadeTo("slow", 0.00);

        $.ajax({
          type: "POST",
          url:  "action/ajax.php",
          dataType: "text", 
          data: { ac: "ddArchivClick", ddid: id, aaction: text },
          success: function(result) {
            jsonData = $.parseJSON(result);
            
            $("#" + jsonData["ddid"]).parent().parent().fadeTo("slow", 1.00);
            
            if (jsonData["Status"] == "OK") {
              
              if (jsonData["Action"] == "restart") {

                $(\'#st_\' + jsonData[\'ddid\']).html(jsonData[\'UStatus\']);
                $(\'#da_\' + jsonData[\'ddid\']).html(jsonData[\'UTime\']);
                $(\'#dah_\' + jsonData[\'ddid\']).html(jsonData[\'UTimeH\']);

                $(\'#es_\' + jsonData[\'ddid\']).html("");
                $(\'#esh_\' + jsonData[\'ddid\']).html("");
                
                $(\'#\' + jsonData[\'ddid\']).html(jsonData[\'UStatusNew\']);
                $(\'#\' + jsonData[\'ddid\']).prop(\'title\', jsonData[\'UStatusNew\']);

              } 
              
              if (jsonData["Action"] == "archive") {
              
                var table = $("table[id*=table_]").DataTable();
              
                $("#" + jsonData["ddid"]).parent().parent().hide();
                $("#" + jsonData["ddid"]).parent().parent().addClass("selected");
                table.row(".selected").remove().draw(false);
                
                $("#DocCount").text(Number($("#DocCount").text()) - 1);
              
              }
                  
            }
            
            if (jsonData["Status"] == "FAIL") {
              alert(jsonData["Reason"]);
            }
    
          }
        });

       	$("#Archive").modal("hide");
       	//console.log(text);
      });
';

?>