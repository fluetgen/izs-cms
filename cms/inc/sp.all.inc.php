<?php

$arrContractList = [];
$strSql = "SELECT * FROM `Vertrag__c`";
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
    foreach ($arrSql as $intKey => $arrContract) {
        if ($arrContract['Aktiv__c'] == 'true') {
          $arrContractList[$arrContract['Company_Group__c']] = 1;
        } elseif (!isset($arrContractList[$arrContract['Company_Group__c']])) {
          $arrContractList[$arrContract['Company_Group__c']] = 0;
        }
    }
}

if ($_COOKIE['id'] == 3) {
  //print_r($arrContractList);
}

$arrCertList = [];
$strSql = "SELECT COUNT(*) AS `Anz`, `pr_group_id` FROM `izs_subsidiaerprotect` GROUP BY `pr_group_id` ORDER BY `pr_group_id`";
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
    foreach ($arrSql as $intKey => $arrCount) {
        $arrCertList[$arrCount['pr_group_id']] = $arrCount['Anz'];
    }
}


$arrCertDateList = [];
$strSql = 'SELECT `izs_subsidiaerprotect`.* FROM `izs_subsidiaerprotect`, ';
$strSql.= '(SELECT `pr_group_id`, MAX(`pr_time`) AS `pr_time` FROM `izs_subsidiaerprotect` GROUP BY `pr_group_id`) AS `max_time` ';
$strSql.= 'WHERE `izs_subsidiaerprotect`.`pr_group_id` = `max_time`.`pr_group_id` ';
$strSql.= 'AND `izs_subsidiaerprotect`.`pr_time` = `max_time`.`pr_time`';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
    foreach ($arrSql as $intKey => $arrMax) {
        $arrCertDateList[$arrMax['pr_group_id']] = strtotime($arrMax['pr_time']);
    }
}

$arrModelList = [];
$strSql = 'SELECT `Id`, `Mitgliedsmodell` FROM `izs_mitgliedsmodell` WHERE `Aktiv__c` = "true"';
$arrSql = MySQLStatic::Query($strSql);

if (count($arrSql) > 0) {
  foreach ($arrSql as $intkKey => $arrModel) {
    $arrModelList[$arrModel['Id']] = $arrModel['Mitgliedsmodell'];
  }
}


$arrTableHead = array(
  'Anzahl',
  'Premium PDL',
  //'Modell',
  'Aktuelles Zertifikat',
  'Liefert Daten',
  'Aktiver Vertrag',
  'Perma-Link zum Zertifikat',
  'Aktion',
  
);

$strSql = 'SELECT * FROM `Group__c` WHERE `SF42_Group_Type__c` = "Lender" ORDER BY `Name`';
$arrPpListRaw = MySQLStatic::Query($strSql);
$intResult = count($arrPpListRaw);

$strSelect = '';

$arrList = array();
foreach ($arrPpListRaw as $intRow => $arrGroup) {

    if (!isset($arrContractList[$arrGroup['Id']])) continue; 

    $strModel = $arrModelList[$arrGroup['Id']] ?? '';
    if ((in_array($strModel, ['Sponsored', 'Sponsored PDL', 'Sponsored ENT', 'Light']))) {
      continue;
    }

    $strClassAdd = '';

    //Filter Zeitarbeitsfirma
    $strClassAdd.= ' pp_' .$arrGroup['Id'];
    if (!isset($arrFilterSection['pp'][$arrGroup['Id']])) {
        $arrFilterSection['pp'][$arrGroup['Id']] = $arrGroup['Name'];
    }

    //Filter Liefert Daten
    if ($arrGroup['Liefert_Daten__c'] == 'true') { 
        $strPr = 'Ja';
    } else {
        $strPr = 'Nein';
    }
    $strClassAdd.= ' ld_' .$strPr;

    if (!isset($arrFilterSection['ld'][$strPr]) && ($strPr !== false)) {
        $arrFilterSection['ld'][$strPr] = $strPr;
    }

    //Filter Aktiver Vertrag
    if ($arrContractList[$arrGroup['Id']] == '1') { 
        $strPr = 'Ja';
    } else {
        $strPr = 'Nein';
    }
    $strClassAdd.= ' ac_' .$strPr;
    
    if (!isset($arrFilterSection['ac'][$strPr]) && ($strPr !== false)) {
        $arrFilterSection['ac'][$strPr] = $strPr;
    }

    $intAnzahl = (($arrCertList[$arrGroup['Id']]) ?? '0');

    if ($intAnzahl > 0) {

        $strName = str_replace('/', '-', $arrGroup['Name']);
        $strName = str_replace('&', '-', $strName);
        $strUrl = 'https://www.izs.de/cms/protect/' .$strName .'/' .$arrGroup['Id'] .'.pdf';

        $strAct = '<span class="hidden">' .$arrCertDateList[$arrGroup['Id']] .'</span>' .'' .date('d.m.Y H:i:s', $arrCertDateList[$arrGroup['Id']]) .'';

        $strLinkCert = '<a href="' .$strUrl .'" target="_blank">' .$strUrl .'</a>';

    } else {

        $strAct = '<span class="hidden">0</span>';
        $strLinkCert = '';

    }

    /*
    $strModel = $arrModelList[$arrGroup['Id']] ?? '';

    if ($strModel != 'Premium') {
      $strSql = 'DELETE FROM `izs_subsidiaerprotect` WHERE `pr_group_id` = "' .$arrGroup['Id'] .'"';
      $arrDel = MySQLStatic::Query($strSql);
    }
    */

    $arrList[] = array(
      'anzahl' => '<a class="ct_' .$arrGroup['Id'] .'" href="/cms/index_neu.php?ac=sp&fu=detail&grid=' .$arrGroup['Id'] .'">' .$intAnzahl .'</a>',
      'name' => $arrGroup['Name'] .'<span class="hidden hfilter ' .$strClassAdd .'"></span>',
      //'model' => $strModel,
      'act' => $strAct .'',
      'delivery'  => ($arrGroup['Liefert_Daten__c'] == 'true') ? 'Ja' : 'Nein',
      'active' => ($arrContractList[$arrGroup['Id']] == 1) ? 'Ja' : 'Nein',
      'link' => $strLinkCert,
      'cert' => '<a class="cCert" id="cc_' .$arrGroup['Id'] .'" style="cursor: pointer;">erzeugen</a>',
    );

}

$arrOpt = array(
    'arrSort' => array(
      'null', 'null', '"type": "hidden"', 'null', 'null', 'null', 'null' //, 'null'
    ),
    'strOrder' => '"order": [[ 1, "asc" ]]'
);

$strTable = strCreateTable($arrList, $arrTableHead, '', false, true, [], $arrOpt);

$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Premium PDLs</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strSelect;

$strIncOutput.= $strTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

$strJsFootCodeRun.= '

$("span.warning").parent().parent().addClass("warning");
$("span.danger").parent().parent().addClass("danger");

$("#seldate").on("change", function (e) {
  this.form.submit();
});

';

$strJsFootCodeRun.= "

$('.cCert').on('click', function (e) {

    var id   = $(this).attr('id').split('_')[1];
  
    $.post('/cms/inc/sp.ajax.inc.php', {
      ac: 'create', id: id, clid: " .$_COOKIE['id'] ."
    }).done(function(data) {
      var part = data.split('|');
      $('.ct_' + id).text(parseInt($('.ct_' + id).text()) + 1);
      $('.ct_' + id).parent().parent().find('td:nth-child(3)').html(part[0]);
      $('.ct_' + id).parent().parent().find('td:nth-child(6)').html(part[1]);
    });
  
  }).bind('mouseover', function() {
    $(this).css('cursor', 'pointer');
  });

";


?>