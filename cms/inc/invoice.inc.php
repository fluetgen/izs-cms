<?php

//CLASSES
require_once ('refactor.inc.php');

function getDocumentNumber ($intProcess = 0, $intCustomer = 0, $intMonth = 0, $intYear = 0) {

  $strSql = 'SELECT * FROM `izs_process_log` WHERE `pl_pr_id` = "' .$intProcess .'" ';
  $strSql.= 'AND `pl_account` = "' .$intCustomer .'" AND `pl_month` = "' .$intMonth .'" ';
  $strSql.= 'AND `pl_year` = "' .$intYear .'" ORDER BY `pl_time` DESC LIMIT 1';
  $arrSql = MySQLStatic::Query($strSql);

  if (count($arrSql) > 0) {
    return $arrSql[0]['pl_result'];
  } else {
    return 0;
  }

}

$arrModelList = [];
$strSql = 'SELECT `Id`, `Mitgliedsmodell` FROM `izs_mitgliedsmodell` WHERE `Aktiv__c` = "true"';
$arrSql = MySQLStatic::Query($strSql);

if (count($arrSql) > 0) {
  foreach ($arrSql as $intkKey => $arrModel) {
    $arrModelList[$arrModel['Id']] = $arrModel['Mitgliedsmodell'];
  }
}


$arrMapping = array();

$strSql = 'SELECT * FROM `izs_easy_premiumpayer`;';
$arrSql = MySQLStatic::Query($strSql);


if (is_array($arrSql) && (count($arrSql) > 0)) {
  foreach ($arrSql as $intKey => $arrMappingRaw) {
    $arrMapping[$arrMappingRaw['ie_pp_id']] = $arrMappingRaw['ie_easy_id'];
  }
}

$strCssHead.= chr(10) .'
<style>
.fa-money {
  font-size: 110%;
  color: green;
  cursor: pointer;
  padding-left: 2px;
}
.done {
  color: red !important;
}
</style>' .chr(10) .chr(10);


//REQUEST
$intMonth   = @$_REQUEST['month'];
$intQuarter = @$_REQUEST['quarter'];
$intYear    = @$_REQUEST['year'];
$strFu      = @$_REQUEST['fu'];

if (@$_REQUEST['seldate'] != '') {
  
  if (strstr($_REQUEST['seldate'], '-') !== false) {
    $intTime = strtotime($_REQUEST['seldate'] .'-01');
  } else {
    $intTime = strtotime($_REQUEST['seldate'] .'-01-01');
  }

  $intTime = mktime(0, 0, 0, date('n', $intTime), date('j', $intTime), date('Y', $intTime));

  $intYear = date('Y', $intTime);  
  $intMonth = date('m', $intTime);  
  $intQuarter = date('m', $intTime);  

} else {

  $intYear = date('Y', strtotime("first day of last month"));
  $intMonth = date('m', strtotime("first day of last month"));
  $intQuarter = date('m', strtotime("first day of last month"));

}
//DATA
//$objBg   = new Bg;

//SETTINGS

$boolShowHead = false;
$boolShowSide = false;

$strPageTitle = 'Abrechnung angelieferter Events';


//FUNCTIONS
$strPerMonthRequest = '';
$strPerQuarterRequest = '';
$strPerYearRequest = '';
$strRegularlyRequest = '';
$strClearingRequest = '';

$arrFilterSection = array();

$strIncOutput = '';

switch ($strFu) {
  case 'permonth':
    $strPerMonthRequest = 'active';
    require_once('invoice.permonth.inc.php');
    break;
  case 'perquarter':
    $strPerQuarterRequest = 'active';
    require_once('invoice.perquarter.inc.php');
    break;
  case 'peryear':
    $strPerYearRequest = 'active';
    require_once('invoice.peryear.inc.php');
    break;
  case 'regularly':
    $strRegularlyRequest = 'active';
    require_once('invoice.regularly.inc.php');
    break;
  case 'clearing':
    $strClearingRequest = 'active';
    require_once('invoice.clearing.inc.php');
    break;
  default:
    $strPerMonthRequest = 'active';
    require_once('invoice.permonth.inc.php');
    break;
}

//print_r($arrFilterSection);


//CONTENT

$strOutput = '
  <!-- Page -->
  <div class="page animsition">
  
  <!--
    <div class="page-aside">
      <div class="page-aside-switch">
        <i class="icon wb-chevron-left" aria-hidden="true"></i>
        <i class="icon wb-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner" style="position: fixed; width: 250px;">
        <section class="page-aside-section" style="width: 250px;">
          <h5 class="page-aside-title">Filter </h5><span id="resetFilter"></span>
          
          <form style="padding-left: 30px;">
';

if (isset($arrFilterSection['y']) && (count($arrFilterSection['y']) > 0)) {
  
  arsort($arrFilterSection['y']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_y">Jahr</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_y" name="filter_y" placeholder="Jahr">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['y'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="y_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

$strOutput.= '          
          </form>

        </section>
      </div>
    </div>
    
    -->
    
    <div class="page-main">
      <div class="page-header">
        <h1 class="page-title">' .$strPageTitle .'</h1>
';

//if ($intAdmin == 1) {
$strOutput.= '
        <div style="z-index: 1;" class="page-header-actions">

          <div class="panel-actions">

            <div class="dropdown">
              <button type="button" class="btn btn-info dropdown-toggle pull-right" id="exampleAlignmentDropdown"
              data-toggle="dropdown" aria-expanded="true">
                Optionen
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="exampleAlignmentDropdown" role="menu">
                <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="doExport">Export CSV</a></li>' .chr(10);

$strOutput.= '              </ul>
            </div>

          </div>

        </div>
';
//}

$strOutput.= '      
        <ul class="nav nav-tabs nav-tabs-line">
          <li class="' .$strPerMonthRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=invoice&fu=permonth">Alle aktiven Veräge</a></li>
          <li class="' .$strRegularlyRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=invoice&fu=regularly">Abrechnung</a></li>
          <li class="' .$strPerQuarterRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=invoice&fu=perquarter">Nachberechnung SV</a></li>
          <li class="' .$strClearingRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=invoice&fu=clearing">Nachberechnung Klärungsfälle</a></li>
          <li class="' .$strPerYearRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=invoice&fu=peryear">Übersicht (Jahr)</a></li>
        </ul>
      
      </div>
      <div class="page-content">
  
';

$strOutput.= $strIncOutput;

$strOutput.= '

      </div> <!-- End page-content -->

    </div> <!-- End page-main -->

  </div>
  <!-- End Page -->
  

';


$strOutput.= '

<form method="post" action="_get_csv.php" id="export">
  <input type="hidden" name="strTable" id="strTable" value="">
  <input type="hidden" name="encHd" id="encHd" value="">
</form>

';


$strJsFootScript.= '  <script src="js/script_neu.js"></script>' .chr(10);

$strJsFootCodeRun.= '
  
  $("#doExport").on("click", function(e) {

    var ct = $("table").length; 
    var table_hd = $($("table")[ct-3]).html();
    var table_cth = $("table.table:eq(1) tr:visible");
    var table_ct = "";
    
    if (table_cth.length > 0) {
      $.each(table_cth, function(k, v) {
        table_ct += "<tr>" + $(v).html() + "</tr>";
      });
    }
    
    $("#strTable").val("<table>" + table_hd + table_ct + "</table>");
    $("#encHd").val(1);
    $("#export").submit();
    
  });  
';

?>