<?php

function prettyfyText ($strText = '') {

  $strText = str_replace('<br>', chr(10), $strText);
  $strText = str_replace('<br />', chr(10), $strText);
  $strText = str_replace('<br/>', chr(10), $strText);

  $strText = str_replace('<p>', chr(10) .chr(10), $strText);

  $strText = str_replace('<li>', chr(10), $strText);

  return strip_tags($strText);

}

function prettyfyTextKeep ($strText = '') {

  return strip_tags($strText, '<p><br><ul><li><div>');

}


$arrEscalation = [];
$strSql = 'SELECT * FROM `Group__c` WHERE `Eskalation__c` = "ja"';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrDataset) {
    $arrEscalation[$arrDataset['Id']] = $arrDataset;
  }
}

$arrOpenWorkflowList = [];
$strSql = 'SELECT * FROM `Vorgang` WHERE `case_type` = "Betriebsnummer-Klärung" AND `case_bnr_status` NOT IN ("Ersetzt", "Abgebrochen")';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrDataset) {
    if (isset($arrOpenWorkflowList[$arrDataset['case_bnr_pdl_companygroup']])) {
      $arrOpenWorkflowList[$arrDataset['case_bnr_pdl_companygroup']]++;
    } else {
      $arrOpenWorkflowList[$arrDataset['case_bnr_pdl_companygroup']] = 1;
    }
  }
}

$arrCmsSgMapping = [];
$strSql = 'SELECT `kl_obj_id`, `kl_foreign_id` FROM `ks_link` WHERE `kl_type` = "smartgroup"';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrDataset) {
    $arrCmsSgMapping[$arrDataset['kl_foreign_id']] = $arrDataset['kl_obj_id'];
  }
}

$arrOpenChatList = [];
$strSql = 'SELECT * FROM `izs_open_topic`';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrDataset) {
    if (isset($arrCmsSgMapping[$arrDataset['ot_topic']])) {
      $arrOpenChatList[$arrCmsSgMapping[$arrDataset['ot_topic']]] = array(
        'sg'    => $arrDataset['ot_topic'],
        'count' => $arrDataset['ot_item_count'],
        'cms'   => $arrCmsSgMapping[$arrDataset['ot_topic']]
      );
    }
  }
}

//print_r($arrOpenChatList);


//Fetch Status Data
$strSql = 'SELECT `izs_sv_sent`.*, UNIX_TIMESTAMP(`vs_date`) AS `vs_date` FROM `izs_sv_sent` WHERE `vs_field` = "status" ';
if ($intReset > 0) {
  $strSql.= 'AND UNIX_TIMESTAMP(`vs_date`) > "' .$intReset .'" ';
}
$strSql.= 'AND `vs_date` = (SELECT MAX(`izs_sv_sent2`.`vs_date`) FROM `izs_sv_sent` `izs_sv_sent2` WHERE ((`izs_sv_sent`.`vs_parent_id` = `izs_sv_sent2`.`vs_parent_id`) AND `izs_sv_sent2`.`vs_field` = "status" ';
if ($intReset > 0) {
  $strSql.= 'AND (UNIX_TIMESTAMP(`izs_sv_sent2`.`vs_date`) > "' .$intReset .'") ';
}
$strSql.= '))';
$arrSent = MySQLStatic::Query($strSql);

//print_r($arrSent); die();

$arrStatusData = array();
if (count($arrSent) > 0) {
  foreach ($arrSent as $intCount => $arrSent) {
    $arrStatusData[$arrSent['vs_parent_id']] = $arrSent;
  }
}

//print_r($arrStatusData); die();

//Fetch Edit Data
$strSql = 'SELECT `izs_sv_sent`.*, UNIX_TIMESTAMP(`vs_date`) AS `vs_date` FROM `izs_sv_sent` WHERE `vs_field` = "edit" ';
if ($intReset > 0) {
  $strSql.= 'AND UNIX_TIMESTAMP(`vs_date`) > "' .$intReset .'" ';
}
$strSql.= 'AND `vs_date` = (SELECT MAX(`izs_sv_sent2`.`vs_date`) FROM `izs_sv_sent` `izs_sv_sent2` WHERE ((`izs_sv_sent`.`vs_parent_id` = `izs_sv_sent2`.`vs_parent_id`) AND `izs_sv_sent2`.`vs_field` = "edit" ';
if ($intReset > 0) {
  $strSql.= 'AND (UNIX_TIMESTAMP(`izs_sv_sent2`.`vs_date`) > "' .$intReset .'") ';
}
$strSql.= '))';
$arrSent = MySQLStatic::Query($strSql);

//print_r($arrSent); die();

$arrEditData = array();
if (count($arrSent) > 0) {
  foreach ($arrSent as $intCount => $arrSent) {
    $arrEditData[$arrSent['vs_parent_id']] = $arrSent;
  }
}

//Fetch Proove Data
$strSql = 'SELECT `izs_sv_sent`.*, UNIX_TIMESTAMP(`vs_date`) AS `vs_date` FROM `izs_sv_sent` WHERE `vs_field` = "proove" ';
if ($intReset > 0) {
  $strSql.= 'AND UNIX_TIMESTAMP(`vs_date`) > "' .$intReset .'" ';
}
$strSql.= 'AND `vs_date` = (SELECT MAX(`izs_sv_sent2`.`vs_date`) FROM `izs_sv_sent` `izs_sv_sent2` WHERE ((`izs_sv_sent`.`vs_parent_id` = `izs_sv_sent2`.`vs_parent_id`) AND `izs_sv_sent2`.`vs_field` = "proove" ';
if ($intReset > 0) {
  $strSql.= 'AND (UNIX_TIMESTAMP(`izs_sv_sent2`.`vs_date`) > "' .$intReset .'") ';
}
$strSql.= '))';
$arrSent = MySQLStatic::Query($strSql);

//print_r($arrSent); die();

$arrProoveData = array();
if (count($arrSent) > 0) {
  foreach ($arrSent as $intCount => $arrSent) {
    $arrProoveData[$arrSent['vs_parent_id']] = $arrSent;
  }
}

$arrVertragList = [];
//$strSql = 'SELECT DATE_FORMAT(`Kuendigung_wirksam_ab__c`, "%d.%m.%Y") AS `Kuendigung_wirksam_ab__c`, `Id`, `Mitgliedsmodell` FROM `izs_vertrag` WHERE `Aktiv__c` = "true"'; // `Id` = "' .$arrGroup['Id'] .'" AND `Kuendigung_wirksam_ab__c` != "0000-00-00" AND 
$strSql = 'SELECT DATE_FORMAT(`Kuendigung_wirksam_ab__c`, "%d.%m.%Y") AS `Kuendigung_wirksam_ab__c`, `Company_Group__c`, `Mitgliedsmodell`, `Pruefmanate_bei_abweichender_Anfrage__c` FROM `Vertrag__c` WHERE `Aktiv__c` = "true"'; 
$arrVertragListRaw = MySQLStatic::Query($strSql);
if (count($arrVertragListRaw) > 0) {
  foreach ($arrVertragListRaw as $intKey => $arrVertrag) {

    $arrVertragList[$arrVertrag['Company_Group__c']] = [
      'Kuendigung_wirksam_ab__c' => $arrVertrag['Kuendigung_wirksam_ab__c'],
      'Mitgliedsmodell' => $arrVertrag['Mitgliedsmodell'],
      'Anfragemonat' => ($arrVertrag['Pruefmanate_bei_abweichender_Anfrage__c'] != '') ? explode(';', $arrVertrag['Pruefmanate_bei_abweichender_Anfrage__c']) : ''
    ];

  }
}

if ($_COOKIE['id'] == 3) {
  //print_r($arrVertragList); die();
}

$strSql = 'SELECT `Id`, `Name`, "" AS `Status`, `InKlaerung__c`, "" AS `BearbeitetVon`, "" AS `GeprueftVon`, `Datenformat__c`, ';
$strSql.= '`SVSoftware__c`, `HinweisBearbeitung__c`, `HinweisUpload__c`, `Kommentar__c`, `SvPrio__c`, `Registriert_bei_IZS_seit__c` FROM `Group__c` ';
$strSql.= 'WHERE `Liefert_Daten__c` = "true" AND `Id` != "a083A00000lqnERQAY" ORDER BY `Name` ';
$arrPpListRaw = MySQLStatic::Query($strSql);
$intResult = count($arrPpListRaw);

$strSelect = '';

$arrAvailMonthList = [
  date('y-m', mktime(10, 0, 0, date('n') -2, 1, date('Y'))),
  date('y-m', mktime(10, 0, 0, date('n') -1, 1, date('Y'))),
  date('y-m', mktime(10, 0, 0, date('n') -0, 1, date('Y'))),
  date('y-m', mktime(10, 0, 0, date('n') +1, 1, date('Y'))),
  date('y-m', mktime(10, 0, 0, date('n') +2, 1, date('Y'))),
];

//print_r($arrAvailMonthList); die();

if (!isset($_REQUEST['seldate'])) {
  $_REQUEST['seldate'] = date('y-m');
}

$strSelect.= '<form action="/cms/index_neu.php?ac=svdaten" method="post"><label for="seldate" style="margin-right: 20px;">Anlieferungsmonat</label><select name="seldate" id="seldate">' .chr(10);
foreach ($arrAvailMonthList as $strMonth) {
  
  $strSelected = '';
  if ($strMonth == $_REQUEST['seldate']) {
    $strSelected = ' selected="selected"';  
  }
  
  $strSelect.= '<option value="' .$strMonth .'"' .$strSelected .'>' .$strMonth .'</option>' .chr(10);
}  
$strSelect.= '</select></form>' .chr(10);

$strSearchMonth = date('n', strtotime($_REQUEST['seldate'] .'-01'));

$arrList = array();
foreach ($arrPpListRaw as $intRow => $arrGroup) {

    $strClassAdd = '';
    $strAddClass = '';

    if (isset($arrVertragList[$arrGroup['Id']]['Anfragemonat']) && (is_array($arrVertragList[$arrGroup['Id']]['Anfragemonat']) === true)) {
      if (!in_array($strSearchMonth, $arrVertragList[$arrGroup['Id']]['Anfragemonat'])) continue;
    } else {
      continue;
    }

    if ($arrGroup['Registriert_bei_IZS_seit__c'] == '0000-00-00') {
      $arrGroup['Registriert_bei_IZS_seit__c'] == '';
    } else {
      $arrGroup['Registriert_bei_IZS_seit__c'] = date('d.m.Y', strtotime($arrGroup['Registriert_bei_IZS_seit__c']));
    }

    $arrGroup['KuendigungZum__c'] = '';
    //$strSql = 'SELECT DATE_FORMAT(`Kuendigung_wirksam_ab__c`, "%d.%m.%Y") AS `Kuendigung_wirksam_ab__c` FROM `izs_vertrag` WHERE `Id` = "' .$arrGroup['Id'] .'" AND `Kuendigung_wirksam_ab__c` != "0000-00-00" AND `Aktiv__c` = "true"';
    //$arrVertrag = MySQLStatic::Query($strSql);
    if (isset($arrVertragList[$arrGroup['Id']]['Kuendigung_wirksam_ab__c']) && ($arrVertragList[$arrGroup['Id']]['Kuendigung_wirksam_ab__c'] != '00.00.0000')) {
      $arrGroup['KuendigungZum__c'] = $arrVertragList[$arrGroup['Id']]['Kuendigung_wirksam_ab__c'];
    }

    //STATUS
    $strStKey   = 0;
    $strStValue = 'Offen';
    $strStatusSelect = '<span class="hidden keep">*KEY*</span><select class="otf hf" name="status" rel="st">';
    foreach ($arrStatus as $intKey => $strStatus) {
      if (isset($arrStatusData[$arrGroup['Id']]) && ($arrStatusData[$arrGroup['Id']]['vs_value'] == $strStatus)) { 
        $strSelected = ' selected = "selected"'; 
        $strStKey   = $intKey;
        $strStValue = $strStatus;
      }
      else $strSelected = '';
      $strStatusSelect.= '<option value="' .$intKey .'"' .$strSelected .'>' .$strStatus .'</option>';
    }
    $strStatusSelect.= '</select>';
    $strStatusSelect = str_replace('*KEY*', $strStValue, $strStatusSelect);

    //KLÄRUNG
    $strKlaerungSelect = ($arrGroup['InKlaerung__c'] == 'true') ? 'Ja' : 'Nein';

    //BEARBEITET
    $strEditSelect = '<select class="otf hf" name="edit" rel="ed">';
    foreach ($arrEdit as $intCount => $strEdit) {
      if (isset($arrEditData[$arrGroup['Id']]) && ($arrEditData[$arrGroup['Id']]['vs_value'] == $strEdit)) { $strSelected = ' selected = "selected"'; }
      else $strSelected = '';
      $strEditSelect.= '<option value="' .$strEdit .'"' .$strSelected .'>' .$strEdit .'</option>';
    }
    $strEditSelect.= '</select>';

    //GEPRÜFT
    $strProoveSelect = '<select class="otf hf" name="proove" rel="pr">';
    foreach ($arrProove as $intCount => $strProove) {
      if (isset($arrProoveData[$arrGroup['Id']]) && ($arrProoveData[$arrGroup['Id']]['vs_value'] == $strProove)) { $strSelected = ' selected = "selected"'; }
      else $strSelected = '';
      $strProoveSelect.= '<option value="' .$strProove .'"' .$strSelected .'>' .$strProove .'</option>';
    }
    $strProoveSelect.= '</select>';

    //DATENFORMAT
    $strDataSelect = '<span class="hidden">' .$arrGroup['Datenformat__c'] .'</span>' .$arrGroup['Datenformat__c'];

    //SOFTWARE
    $strSoftwareSelect = '<span class="hidden">' .$arrGroup['SVSoftware__c'] .'</span>' .$arrGroup['SVSoftware__c'];


    //Filter Prio
    if ($arrGroup['SvPrio__c'] == 'true') { 
      $strPr = 'Ja';
    } else {
      $strPr = 'Nein';
    }
    $strClassAdd.= ' po_' .$strPr;
    if (!isset($arrFilterSection['po'][$strPr]) && ($strPr !== false)) {
      $arrFilterSection['po'][$strPr] = $strPr;
    }

    //Filter Kündigung
    $strCa = ($arrGroup['KuendigungZum__c'] != '') ? 'Ja' : 'Nein';
    $strClassAdd.= ' ca_' .$strCa;
    if (!isset($arrFilterSection['ca'][$strCa]) && ($strCa !== false)) {
      $arrFilterSection['ca'][$strCa] = $strCa;
    }

    //Filter Datenformat
    $strSelectKey = 'mm';
    $strKey = array_search($arrVertragList[$arrGroup['Id']]['Mitgliedsmodell'] ?? '', $arModell);
    $strValue = $arrVertragList[$arrGroup['Id']]['Mitgliedsmodell'] ?? '';
    $strClassAdd.= ' ' .$strSelectKey .'_' .$strKey;
    if (!isset($arrFilterSection[$strSelectKey][$strKey])) {
      $arrFilterSection[$strSelectKey][$strKey] = $strValue;
    }

    //Filter Zeitarbeitsfirma
    $strClassAdd.= ' pp_' .$arrGroup['Id'];
    if (!isset($arrFilterSection['pp'][$arrGroup['Id']])) {
      $arrFilterSection['pp'][$arrGroup['Id']] = $arrGroup['Name'];
    }

    //Filter Status
    $strClassAdd.= ' st_' .$strStKey;
    if (!isset($arrFilterSection['st'][$strStKey])) {
      $arrFilterSection['st'][$strStKey] = $strStValue;
    }

    //Filter Klärung
    $strClKey = $arrGroup['InKlaerung__c'];
    $strClassAdd.= ' cl_' .$strClKey;
    if (!isset($arrFilterSection['cl'][$strClKey])) {
      $arrFilterSection['cl'][$strClKey] = $arrKlaerung[$arrGroup['InKlaerung__c']];
    }

    //Filter Bearbeitet
    $strSelectKey = 'ed';
    if (isset($arrEditData[$arrGroup['Id']]['vs_value'])) {
      $strKey = $arrEditData[$arrGroup['Id']]['vs_value'];
      $strValue = $arrEditData[$arrGroup['Id']]['vs_value'];
    } else {
      $strKey = '';
      $strValue = '';
    }
    $strClassAdd.= ' ' .$strSelectKey .'_' .$strKey;
    if (!isset($arrFilterSection[$strSelectKey][$strKey])) {
      $arrFilterSection[$strSelectKey][$strKey] = $strValue;
    }
    $strValueBearbeitet = $strValue;

    //Filter Geprüft
    $strSelectKey = 'pr';
    if (isset($arrProoveData[$arrGroup['Id']]['vs_value'])) {
      $strKey = $arrProoveData[$arrGroup['Id']]['vs_value'];
      $strValue = $arrProoveData[$arrGroup['Id']]['vs_value'];
    } else {
      $strKey = '';
      $strValue = '';
    }
    $strClassAdd.= ' ' .$strSelectKey .'_' .$strKey;
    if (!isset($arrFilterSection[$strSelectKey][$strKey])) {
      $arrFilterSection[$strSelectKey][$strKey] = $strValue;
    }
    $strValueGeprueft = $strValue;

    //Filter Datenformat
    $strSelectKey = 'da';
    $strKey = array_search($arrGroup['Datenformat__c'], $arrFormat);
    $strValue = $arrGroup['Datenformat__c'];
    $strClassAdd.= ' ' .$strSelectKey .'_' .$strKey;
    if (!isset($arrFilterSection[$strSelectKey][$strKey])) {
      $arrFilterSection[$strSelectKey][$strKey] = $strValue;
    }

    //Filter Software
    $strSelectKey = 'so';
    $strKey = array_search($arrGroup['SVSoftware__c'], $arrSoftware);
    $strValue = $arrGroup['SVSoftware__c'];
    $strClassAdd.= ' ' .$strSelectKey .'_' .$strKey;
    if (!isset($arrFilterSection[$strSelectKey][$strKey])) {
      $arrFilterSection[$strSelectKey][$strKey] = $strValue;
    }
    
    $strSelectKey = 'ow';
    $strKey = 0;
    $strValue = 'Nein';
    $strOpen = '';
    if (isset($arrOpenWorkflowList[$arrGroup['Id']])) {
      $strOpen = '<a href="https://crm.izs-institut.de/ver/' .$arrGroup['Id'] .'/vorgaenge" target="_blank">' .$arrOpenWorkflowList[$arrGroup['Id']] .'</a>';
      $strKey = 1;
      $strValue = 'Ja';
    }
    $strClassAdd.= ' ' .$strSelectKey .'_' .$strKey;
    $arrFilterSection[$strSelectKey][$strKey] = $strValue;

    $strSelectKey = 'oc';
    $strKey = 0;
    $strValue = 'Nein';
    $strChatList = '';
    if (isset($arrOpenChatList[$arrGroup['Id']])) {
      $strChatList = '<a href="https://izs.smartgroups.io/context/' .$arrOpenChatList[$arrGroup['Id']]['sg'] .'/topics" target="_blank"><img src="https://izs.smartgroups.io/img/logo/logo_32.png" height="20"></a>';
      $strKey = 1;
      $strValue = 'Ja';
    }
    $strClassAdd.= ' ' .$strSelectKey .'_' .$strKey;
    $arrFilterSection[$strSelectKey][$strKey] = $strValue;

    $strGroupId = $arrGroup['Id'];

    if (isset($arrEscalation[$strGroupId])) { // 'a083000000D4hUMAAZ' https://izs.smartgroups.io/frontend/main/topic/topic-97f48d16-eb79-48c7-b450-6085ab217700

      preg_match('/topic-[\w-]*/', $arrEscalation[$strGroupId]['Eskalation_Link__c'], $arrLink);
      $strSgLink = $arrLink[0] ?? '';
  
      $strEsc = '<a href="javascript:void(0)" alt="' .$arrEscalation[$strGroupId]['Eskalation_Grund__c'] .'" title="' .$arrEscalation[$strGroupId]['Eskalation_Grund__c'] .'" onclick="openSG(\'' .$strSgLink .'\')" style="color:red;"><i class="icon fa fa-warning" aria-hidden="true"></i></a>';
  
    } else {
  
      $strEsc = '';
  
    }

    $arrList[] = array(
        'Selected' => '<input type="checkbox" id="grp_' .$arrGroup['Id'] .'" class="grpgrp">',
        'esc' => $strEsc,
        'Company Group' => '<a href="https://crm.izs-institut.de/ver/' .$arrGroup['Id'] .'/stammdaten?tab=SV-Daten" target="_blank">' .$arrGroup['Name'] .'</a><span class="hidden hfilter' .$strClassAdd .' ' .$strAddClass .'"></span>',
        'Model' => $arrVertragList[$arrGroup['Id']]['Mitgliedsmodell'] ?? '',
        'open' => $strOpen,
        'chats' => $strChatList,
        'Prio' => '<span class="hidden">' .$arrGroup['SvPrio__c'] .'</span>' .(($arrGroup['SvPrio__c'] == 'true') ? 'Ja' : 'Nein'),
        'Kündigung zum' => '<span class="hidden">' .strtotime($arrGroup['KuendigungZum__c']) .'</span>' .$arrGroup['KuendigungZum__c'],
        'Registriert' => '<span class="hidden">' .strtotime($arrGroup['Registriert_bei_IZS_seit__c']) .'</span>' .$arrGroup['Registriert_bei_IZS_seit__c'],
        'Status' => $strStatusSelect,
        'In Klärung' => $strKlaerungSelect,
        'Bearbeitet von' => '<span class="hidden keep">' .$strValueBearbeitet .'</span>' .$strEditSelect,
        'Geprüft von' => '<span class="hidden keep">' .$strValueGeprueft .'</span>' .$strProoveSelect,
        'Datenformat' => $strDataSelect,
        'Software' => $strSoftwareSelect,
        'Hinweis Bearbeitung' => prettyfyTextKeep($arrGroup['HinweisBearbeitung__c']), //'' .prettyfyText($arrGroup['HinweisBearbeitung__c']) .'',
        //'Hinweis Upload' => prettyfyTextKeep($arrGroup['HinweisUpload__c']), //'' .prettyfyText($arrGroup['HinweisUpload__c']) .'',
        'Kommentar' => prettyfyTextKeep($arrGroup['Kommentar__c']), //'' .prettyfyText($arrGroup['Kommentar__c']) .'',
      //'from' => '<span class="hidden">' .$strFromSort .'</span>' .$strFrom,
    );

}

$arrOpt = array(
  'boolHasContext' => true,
  'arrNoSort' => array(0),
  'strOrder' => '"order": [[ 2, "asc" ]]'
);

$strTable = strCreateTable($arrList, $arrTableHead, '', false, true, array(), $arrOpt);

$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Zeitarbeitsunternehmen</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
';

$strIncOutput.= $strSelect;

$strIncOutput.= $strTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

$strJsFootCodeRun.= '

$("span.warning").parent().parent().addClass("warning");
$("span.danger").parent().parent().addClass("danger");

$("#seldate").on("change", function (e) {
  this.form.submit();
});

';

$strJsFootCodeRun.= "

$('#selall').click(function (event) {
  if (this.checked) {
      $('.grpgrp').each(function () { //loop through each checkbox
          $(this).prop('checked', true); //check 
      });
  } else {
      $('.grpgrp').each(function () { //loop through each checkbox
          $(this).prop('checked', false); //uncheck              
      });
  }
});


$('.otf').change(function (event) {
  var name = $(this).attr('name');
  var value = $(this).val();
  var id = $(this).parent().parent().find('input.grpgrp').prop('id').split('_')[1];
  var filter = $(this).parent().parent().find('span.hfilter');
  var classList = filter.attr('class').split(/\s+/);
  var sel = $(this);
  
  if (sel.prop('nodeName') == 'SELECT') {
    $(this).parent().find('span').text($(this).find('option:selected').text());

    var rel = $(this).attr('rel');

    $.each(classList, function(index, item) {
      if (item.match('^' + rel + '_')) {
        filter.removeClass(item);
        filter.addClass(rel + '_' + sel.children('option:selected').val());
      }
    });
  
  }

  $.post('inc/svdaten.ajax.php', { 
    ac: 'change', id: id, name: name, value: value
  });
  
  var tablelist = $('table');
  var dt = '';

  $.each(tablelist, function(index, item) {
    if (item.id.match('^table_')) {
      dt = item.id;
    }
  });

  var table = $('#' + dt).DataTable();
  
  table.rows().every( function () {
      var d = this.data();
      d.counter++; // update data source for the row
      this.invalidate(); // invalidate the data DataTables has cached for this row
  } );
  
  // Draw once all updates are done
  //table.draw();

  colorTable();

});

function colorTable () {

  $('span.hfilter').each(function(i) {

    var classList = $(this).attr('class').split(/\s+/);
    var tr = $(this).parent().parent();

    $.each(classList, function(index, item) {
      if (item.match('^st_')) {
        var idx = item.split('_')[1];
        if (!tr.hasClass('status_' + idx)) {
          var former = tr.attr('class').split(/\s+/);
          $.each(former, function(ix, itm) {
            if (itm.match('^status_')) tr.removeClass(itm);
          });
          tr.addClass('status_' + idx);
        }
      }
    });

  });


}

colorTable();

";


?>