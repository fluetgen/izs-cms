<?php

//REQUEST

$intType  = @$_REQUEST['ty'];
$intId    = @$_REQUEST['id'];

$arrAnfrageweg = array(
  '' => '- ohne -',
  'E-Mail' => 'E-Mail',
  'Fax' => 'Fax',
  'Post' => 'Post'
);

$strMessage = '';

if (isset($_POST) && (count($_POST) > 0)) {

  if (isset($_REQUEST['id']) && ($_REQUEST['id'] != '')) {

    if (!isset($_REQUEST['ct_category'])) {
      $_REQUEST['ct_category'] = '';
    }

    $strSql = 'UPDATE `cms_text` SET `ct_name` = "' .$_REQUEST['ct_name'] .'", `ct_head` = "' .$_REQUEST['ct_head'] .'", ';
    $strSql.= '`ct_text` = "' .str_replace('"', '\\"', $_REQUEST['ct_text']) .'", `ct_type` = "' .$_REQUEST['ty'] .'", `ct_category` = "' .$_REQUEST['ct_category'] .'" WHERE `ct_id` = "' .$intId .'"';
    $arrRes = MySQLStatic::Query($strSql);

    $strMessage = 'Vorlage erfolgreich bearbeitet.';


  } else {
    
    if (!isset($_REQUEST['ct_category'])) {
      $_REQUEST['ct_category'] = '';
    }

    $strSql = 'INSERT INTO `cms_text` (`ct_id`, `ct_name`, `ct_head`, `ct_text`, `ct_type`, `ct_category`) ';
    $strSql.= 'VALUES (NULL, "' .$_REQUEST['ct_name'] .'", "' .$_REQUEST['ct_head'] .'", "' .str_replace('"', '\\"', $_REQUEST['ct_text']) .'", "' .$_REQUEST['ty'] .'", "' .$_REQUEST['ct_category'] .'")';
    $intId  = MySQLStatic::Insert($strSql);

    $strMessage = 'Vorlage erfolgreich hinzugefügt.';

  }
  

}

if ($intId != '') {
    $strSql = 'SELECT * FROM `cms_text` WHERE `ct_id` = "' .$intId .'"';
    $arrTemplate = MySQLStatic::Query($strSql);

    $strPageTitle = 'E-Mail Vorlage bearbeiten';

} else {

    $arrTemplate[0]['ct_id'] = '';
    $arrTemplate[0]['ct_name'] = '';
    $arrTemplate[0]['ct_head'] = '';
    $arrTemplate[0]['ct_text'] = '';
    $arrTemplate[0]['ct_category'] = '';

    $strPageTitle = 'E-Mail Vorlage hinzufügen';

}


//SETTINGS

$boolShowHead = false;
$boolShowSide = false;


//CONTENT

$strCssHead.= '
<style>
.nav-tabs > li > a {
  -moz-border-bottom-colors: none;
  -moz-border-left-colors: none;
  -moz-border-right-colors: none;
  -moz-border-top-colors: none;
  background-color: #f3f7f9;
  border-color: #f3f7f9 #f3f7f9 transparent;
  border-image: none;
  border-style: solid;
  border-width: 1px;
  color: #37474f;
}
</style>
';

$strOutput = '
  <!-- Page -->
  <div class="animsition container-fluid">
';

//$strOutput.= strCreatePageTitle('Kontakte', $arrIP[0]['ac_name']);


$strTable = '
<form action="index_neu.php?ac=' .$_REQUEST['ac'] .'&fu=' .$_REQUEST['fu'] .'&ty=' .$intType .'&id=' .$arrTemplate[0]['ct_id'] .'&clid=' .$_SESSION['id'] .'" method="post" class="tab-wizard wizard-circle wizard clearfix" role="application" id="tempform"><div class="content clearfix">

    <input type="hidden" id="ct_text" name="ct_text" value="">

    <section id="steps-uid-0-p-0" role="tabpanel" aria-labelledby="steps-uid-0-h-0" class="body current" aria-hidden="false">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="ct_name">Name</label>
                    <input type="text" class="form-control" id="ct_name" name="ct_name" value="' .$arrTemplate[0]['ct_name'] .'"> </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="ct_head">Betreff</label>
                    <input type="email" class="form-control" id="ct_head" name="ct_head" value="' .$arrTemplate[0]['ct_head'] .'"> </div>
            </div>

        </div>
';

if (!in_array($intType, array(1,4))) {
$strTable.= '
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="ct_category">Anfrageweg</label>
                    <select class="custom-select form-control" id="ct_category" name="ct_category">
';

foreach ($arrAnfrageweg as $strValue => $strOption) {

  $strSelected = '';
  if ($arrTemplate[0]['ct_category'] == $strValue) {
    $strSelected = ' selected="selected"';
  }

  $strTable.= '                        <option value="' .$strValue. '"' .$strSelected .'>' .$strOption. '</option>' .chr(10);
}

$strTable.= '
                    </select>
                </div>
            </div>

        </div>
';
}

$strTable.= '
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="ct_text">Text</label>
                    <div id="editor"></div>
                </div>
            </div>

        </div>
        
        <div class="row">
          <div class="col-md-6">
            <button type="button" class="btn btn-default" id="back">Zurück</button> 
            <button type="button" class="btn btn-primary" id="save">Speichern</button>
          </div>
        </div>

    </section>

</div></form>

';

$strIncOutput.= ' 

    <div class="panel">

      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

if ($strMessage != '') {

$strIncOutput.= '

                <div class="alert dark alert-success alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                  ' .$strMessage .'
                </div>

';

}


$strIncOutput.= $strTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

$strCssHead.= '
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jodit/3.1.92/jodit.min.css">
';
$strJsHead.= '
<script src="//cdnjs.cloudflare.com/ajax/libs/jodit/3.1.92/jodit.min.js"></script>
';

$strJsFootCodeRun.= '
  
  $("#back").on("click", function(e) {

    location.href="index_neu.php?ac=' .$_REQUEST['ac'] .'&ty=' .$intType .'&clid=' .$_SESSION['id'] .'&fu=' .$_REQUEST['fu'] .'";
    
  });  

  $("#save").on("click", function(e) {
    
    $("#ct_text").val(editor.value);
    $("#tempform").submit();

  });

  var editor = new Jodit("#editor", {
    "spellcheck": false,
    "language": "de",
    "toolbarAdaptive": false,
    "showCharsCounter": false,
    "showWordsCounter": false,
    "showXPathInStatusbar": false,
  
    "askBeforePasteHTML": false,
    "askBeforePasteFromWord": false,
    "defaultActionOnPaste": "insert_only_text",
  
    "buttons": "source,|,bold,underline,italic,|,,ul,|,,|,|,align,undo,redo,indent,outdent,fullsize"
  
  });    

  editor.value = \'' .str_replace("\n", '', str_replace("\r\n", '', $arrTemplate[0]['ct_text'])) .'\';

';

//editor.value = \'' .str_replace("\r\n", "\\n", $arrTemplate[0]['ct_text']) .'\';

?>