<?php

include_once ('const.inc.php');

//REQUEST

$strMonth      = @$_REQUEST['svmonth'];
$strIpId       = @$_REQUEST['strSelInf'];
$strResult     = @$_REQUEST['svresult'];
$strStundung   = @$_REQUEST['svstundung'];
$strRueckValue = @$_REQUEST['svrueck'];
$strKlaerung   = @$_REQUEST['svklaerung'];

$strSql = 'SELECT * FROM `cms_login` WHERE `cl_id` = "' .$_SESSION['id'] .'"';
$arrUser = MySQLStatic::Query($strSql);

$strSql = 'SELECT * FROM `izs_mel` WHERE `Btnr` != ""';
$arrMelRaw = MySQLStatic::Query($strSql);

$arrMelList = array();
$arrMelListId = array();
foreach ($arrMelRaw as $intKey => $arrMel) {
  $arrMelList[$arrMel['Btnr']] = $arrMel['Name'];
  $arrMelListId[$arrMel['Btnr']] = $arrMel['Id'];
}

$arrNegativMerkmal = array();
$boolNegativMerkmal = false;
$strNvm = 'SELECT `PP`.`SF42_Comany_ID__c` AS `PpBnr`, `KK`.`SF42_Comany_ID__c` AS `KkBnr`, `Negativmerkmal__c`.* FROM `Negativmerkmal__c` INNER JOIN `Account` AS `PP` ON `AccountId` = `PP`.`Id` INNER JOIN `Account` AS `KK` ON `Auskunftsgeber__c` = `KK`.`Id` WHERE `Negativmerkmal__c`.`Aktiv__c` = "true" AND `Auskunftsgeber_Typ__c` = "01230000001Ao72AAC"';
$arrNvm = MySQLStatic::Query($strNvm);

if (count($arrNvm) > 0) {
  foreach ($arrNvm as $intKey => $arrNDataset) {
    $strKey = $arrNDataset['PpBnr'] .'_' .$arrNDataset['KkBnr'];
    $arrNegativMerkmal[$strKey] = $arrNDataset['Art_Negativmerkmal__c'];
  }
}

$arrEscalation = [];
$strSql = 'SELECT * FROM `Group__c` WHERE `Eskalation__c` = "ja"';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrDataset) {
    $arrEscalation[$arrDataset['Id']] = $arrDataset;
  }
}

//print_r($arrNegativMerkmal); die();


$strSql = 'SELECT * FROM `izs_month_sv`';
$arrMonthList = MySQLStatic::Query($strSql);

$strSelect = '

    <div class="form-group">
        <label class="col-sm-2 control-label">Zeitraum</label>
        <div class="col-sm-2" style="padding-left: 0px;">
            <select class="custom-select form-control" id="svmonth" name="svmonth">
';

$strFirst = '';
foreach ($arrMonthList as $intKey => $arrMonth) {

    if ($strFirst == '') {
        $strFirst = $arrMonth['Month'];
    }

    $strSelected = '';
    if ($strMonth == $arrMonth['Month']) {
        $strSelected = ' selected="selected"';
    }

    $strSelect.= '                        <option value="' .$arrMonth['Month']. '"' .$strSelected .'>' .$arrMonth['Month']. '</option>' .chr(10);
}

if (is_null($strMonth)) {
    $strMonth = $strFirst;
}

$strSelect.= '
            </select>
        </div>
    </div>

';

/*
$strSql = 'SELECT * FROM `izs_informationprovider`';
$arrIpList = MySQLStatic::Query($strSql);

$strSelect.= '

    <div class="form-group">
        <label class="col-sm-2 control-label">Information Provider</label>
        <div class="col-sm-4" style="padding-left: 0px;">
            <select class="custom-select form-control" id="svip" name="svip">
';

$strSelectedAll = '';
if (is_null($strIpId)) {
  $strSelectedAll = ' selected="selected"';
  $strIpId = '';
}

$strSelect.= '                        <option value=""' .$strSelectedAll .'>- Alle anzeigen -</option>' .chr(10);

$strFirst = '';
$arrAllIp = array();
foreach ($arrIpList as $intKey => $arrIp) {

    $arrAllIp[$arrIp['Id']] = $arrIp['Name'];

    if ($strFirst == '') {
        $strFirst = $arrIp['Id'];
    }

    $strSelected = '';
    if ($strIpId == $arrIp['Id']) {
        $strSelected = ' selected="selected"';
    }

    $strSelect.= '                        <option value="' .$arrIp['Id']. '"' .$strSelected .'>' .$arrIp['Name']. '</option>' .chr(10);
}

$strSelect.= '
            </select>
        </div>
    </div>

';
*/

// *************************************** //

 
$strSql6 = 'SELECT DISTINCT `SF42_IZSEvent__c`.`SF42_informationProvider__c` AS `Id`, `Account`.`Name` AS `Name`, `Account`.`Dezentrale_Anfrage__c` ';
$strSql6.= 'FROM `SF42_IZSEvent__c` INNER JOIN `Account` ';
$strSql6.= 'ON `SF42_IZSEvent__c`.`SF42_informationProvider__c` = `Account`.`Id` ';
$strSql6.= 'WHERE `SF42_isInformationProvider__c` = "true" ';
$strSql6.= 'ORDER BY `Account`.`Name` ASC';

$arrResult6 = MySQLStatic::Query($strSql6);
if (count($arrResult6) > 0) {

  if (@$_REQUEST['strSelInf'] == 'all') {
    $strSelectedAll = ' selected="selected"';
  } else {
    $strSelectedAll = '';
  }

  $strSelect.= '
    <div class="form-group">
      <label class="col-sm-2 control-label">Information Provider</label>
      <div class="col-sm-4" style="padding-left: 0px;">
        <select class="custom-select form-control" id="strSelInf" name="strSelInf">' .chr(10);
  
  $strSelect.= '    <option value="all"' .$strSelectedAll .'>- alle Information Provider -</option>' .chr(10);
  
  foreach ($arrResult6 as $arrProvider) {
    $strSelected = '';
    if ($arrProvider['Id'] == @$_REQUEST['strSelInf']) {
      $strSelected = ' selected="selected"';
    } 
    
    $strAddDez = '';
    
    if ($arrProvider['Dezentrale_Anfrage__c'] == 'true') {
      $strOptClass = ' class="optv"';
      
      $strSqlDez = 'SELECT `Id`, `Name`, `CATCH_ALL__c` FROM `Anfragestelle__c` WHERE `Information_Provider__c`="' .$arrProvider['Id'] .'" ORDER BY `Name`';
      $arrSqlDez = MySQLStatic::Query($strSqlDez);

      //$arrProvider['Id'] = 'da_' .$arrProvider['Id'];
      
      if (count($arrSqlDez) > 0) {
        foreach ($arrSqlDez as $arrDez) {
          
          $strSelectedDez = '';
          if (('d_' .$arrDez['Id']) == @$_REQUEST['strSelInf']) {
            $strSelectedDez = ' selected="selected"';
          } 
          $strAddDez.= '    <option value="d_' .$arrDez['Id'] .'"' .$strSelectedDez .'>&nbsp;&nbsp;- ' .$arrDez['Name'] .'</option>' .chr(10);

        } 
      }
      
    } else {
      $strOptClass = '';
    }
    
    $strSelect.= '    <option' .$strOptClass .' value="' .$arrProvider['Id'] .'"' .$strSelected .'>' .$arrProvider['Name'] .'</option>' .chr(10);

    $arrAllIp[$arrProvider['Id']] = $arrProvider['Name'];

    $strSelect.= $strAddDez;
  }

  $strSelect.= '
      </select>
    </div>
  </div>
';

}


// *************************************** //


$arrResultList = array(
  'OK' => 'OK',
  'enquired' => 'enquired'
);

$strSelect.= '

    <div class="form-group">
        <label class="col-sm-2 control-label">Ergebnis</label>
        <div class="col-sm-4" style="padding-left: 0px;">
            <select class="custom-select form-control" id="svresult" name="svresult">
';

$strSelectedAll = '';
if (is_null($strResult)) {
  $strSelectedAll = ' selected="selected"';
  $strResult = '';
}

$strSelect.= '                        <option value=""' .$strSelectedAll .'>- Alle anzeigen -</option>' .chr(10);

$strFirst = '';
foreach ($arrResultList as $strKey => $strResultName) {

    $strSelected = '';
    if ($strResult == $strKey) {
        $strSelected = ' selected="selected"';
    }

    $strSelect.= '                        <option value="' .$strKey .'"' .$strSelected .'>' .$strResultName .'</option>' .chr(10);
}

$strSelect.= '
            </select>
        </div>
    </div>

';

$arrStundungList = array(
  'Stundung' => 'Stundung',
	'Ratenzahlung' => 'Ratenzahlung',
	'Stundung oder Ratenzahlung' => 'Stundung oder Ratenzahlung'
);

$strSelect.= '

    <div class="form-group">
        <label class="col-sm-2 control-label">Stundung/Ratenzahlung</label>
        <div class="col-sm-4" style="padding-left: 0px;">
            <select class="custom-select form-control" id="svstundung" name="svstundung">
';

$strSelectedAll = '';
if (is_null($strStundung)) {
  $strSelectedAll = ' selected="selected"';
  $strStundung = '';
}

$strSelect.= '                        <option value=""' .$strSelectedAll .'>- Alle anzeigen -</option>' .chr(10);

$strFirst = '';
foreach ($arrStundungList as $strKey => $strStundungName) {

    $strSelected = '';
    if ($strStundung == $strStundungName) {
        $strSelected = ' selected="selected"';
    }

    $strSelect.= '                        <option value="' .$strStundungName .'"' .$strSelected .'>' .$strStundungName .'</option>' .chr(10);
}

$strSelect.= '
            </select>
        </div>
    </div>

';

$arrKlaerungList = array(
  'in Klärung' => 'in Klärung',
	'geschlossen / positiv' => 'geschlossen / positiv',
	'geschlossen / negativ' => 'geschlossen / negativ'
);

$strSelect.= '

    <div class="form-group">
        <label class="col-sm-2 control-label">Status Klärung</label>
        <div class="col-sm-4" style="padding-left: 0px;">
            <select class="custom-select form-control" id="svklaerung" name="svklaerung">
            ">
';

$strSelectedAll = '';
if (is_null($strKlaerung)) {
  $strSelectedAll = ' selected="selected"';
  $strKlaerung = '';
}

$strSelect.= '                        <option value=""' .$strSelectedAll .'>- Alle anzeigen -</option>' .chr(10);

$strFirst = '';
foreach ($arrKlaerungList as $strKey => $strKlaerungName) {

    $strSelected = '';
    if ($strKlaerung == $strKlaerungName) {
        $strSelected = ' selected="selected"';
    }

    $strSelect.= '                        <option value="' .$strKlaerungName .'"' .$strSelected .'>' .$strKlaerungName .'</option>' .chr(10);
}

$strSelect.= '
            </select>
        </div>
    </div>

';

if ($strRueckValue != '') {
  $strRueck = date('d.m.Y', strtotime($strRueckValue));
} else {
  $strRueck = '';
}

$strSelect.= '
    <div class="form-group">
        <label class="col-sm-2 control-label">Rückmeldung am</label>
        <div class="input-group col-sm-4" style="padding-left: 0px;">
        <!-- 
          <span class="input-group-addon">
              <i class="icon wb-calendar today" aria-hidden="true"></i>
          </span>
        -->
          <input id="svrueck" name="svrueck" type="text" class="form-control dp" data-plugin="datepicker" value="' .$strRueck .'" data-date-format="dd.mm.yyyy">
          <!-- -->
        </div>
    </div>

    ';


$strSend = '

    <div class="form-group">
        <label class="col-sm-2 control-label">
        </label>
        <div class="col-sm-2" style="padding-left: 0px;">
            <button type="submit" class="btn btn-primary">Suchen </button>
        </div>
    </div>

';


$arrMonth = explode('-', $strMonth);


if (strstr(@$_REQUEST['strSelInf'], 'd_') != false) {
    
  $_REQUEST['strSelInf'] = substr($_REQUEST['strSelInf'], 2);
  $strAnfragestelleId = $_REQUEST['strSelInf'];
  
  $strSqlDez = 'SELECT `Id`, `Name`, `CATCH_ALL__c` FROM `Anfragestelle__c` WHERE `Id`="' .$_REQUEST['strSelInf'] .'"';
  $arrSqlDez = MySQLStatic::Query($strSqlDez);
  
  $strSqlPp = 'SELECT `Premium_Payer__c`, `Information_provider__c` FROM `Dezentrale_Anfrage_KK__c` WHERE `Anfragestelle__c` = "' .$_REQUEST['strSelInf'] .'"';
  $arrSqlPp = MySQLStatic::Query($strSqlPp);

  $arrPayer = array();
  $arrPayerAccount = array();
  $strPremiumPayer = '';
  
  if (count($arrSqlPp) > 0) {
    foreach ($arrSqlPp as $arrPremiumPayer) {
      $arrPayer[] = '"' .$arrPremiumPayer['Premium_Payer__c'] .'"';
      $_REQUEST['strSelInf'] = $arrPremiumPayer['Information_provider__c'];
    }
  
    $strPremiumPayer = implode(',', $arrPayer);
    
    $strSqlPpA = 'SELECT `SF42_Comany_ID__c` FROM `Account` WHERE `Id` IN (' .$strPremiumPayer .')';
    $arrSqlPpA = MySQLStatic::Query($strSqlPpA);

    if (count($arrSqlPpA) > 0) {
      foreach ($arrSqlPpA as $arrPpAccount) {
        $arrPayerAccount[] = '"' .$arrPpAccount['SF42_Comany_ID__c'] .'"';
      }
      $strPpAccount = implode(',', $arrPayerAccount);
    }
    
  }
  
  if ($arrSqlDez[0]['CATCH_ALL__c'] == 'true') {
    
    $strSqlKK = 'SELECT * FROM `Anfragestelle__c` WHERE `Id`="' .$strAnfragestelleId .'"';
    $arrKK = MySQLStatic::Query($strSqlKK);
    
    $boolIsCatchAll = true;
    $arrCatchAll = $arrKK[0];
    
    $strSqlKK = 'SELECT * FROM `Account` WHERE `Id`="' .$arrKK[0]['Information_Provider__c'] .'"';
    $arrKK = MySQLStatic::Query($strSqlKK);

    $strSql6 = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `Beitragsmonat__c` = "' .$arrMonth[0] .'" AND `Beitragsjahr__c` = "' .$arrMonth[1] .'" ';
    
    if ($strKlaerung != '') {
      $strSql6.= 'AND ((`Status_Klaerung__c` = "' .$strKlaerung .'") AND (`SF42_EventStatus__c` =  "enquired")) ';
    }

    if ($strResult != '') {
      if ($strResult == 'enquired') {
        $strSql6.= 'AND ((`SF42_EventStatus__c` =  "enquired") AND (`Status_Klaerung__c` != "")) ';
      } else {
        $strSql6.= 'AND (`SF42_EventStatus__c` =  "' .$strResult .'") ';
      }
    }

    if (($strKlaerung == '') && ($strResult == '')) {
      $strSql6.= 'AND (((`SF42_EventStatus__c` =  "enquired") AND (`Status_Klaerung__c` != "")) OR (`SF42_EventStatus__c` =  "OK")) ';
    }
  
    if ($strStundung != '') {
      $strSql6.= 'AND `Stundung__c` = "' .$strStundung .'" ';
    }
  
      
    if (($_REQUEST['strSelInf'] != '') && ($_REQUEST['strSelInf'] != 'all')) {
      $strSql6.= 'AND ((`SF42_IZSEvent__c`.`SF42_informationProvider__c` = "' .@$arrKK[0]['Id'] .'") OR (`SF42_IZSEvent__c`.`SF42_informationProvider__c` = "' .$_REQUEST['strSelInf'] .'")) ';
    }
  
    if ($strRueckValue != '') {
      $strSql6.= 'AND `Rueckmeldung_am__c` = "' .date('Y-m-d', strtotime($strRueckValue)) .'" ';
    }
  
    
    if (strlen($strPpAccount) > 0) {
      //$strSql6.= 'AND `Betriebsnummer_ZA__c` IN (' .$strPpAccount .') '; //Betriebsnummer  
    }
  
    //echo 
    $strSql6.= 'AND `SF42_IZSEvent__c`.`RecordTypeId` = "01230000001Ao76AAC" AND `Art_des_Dokuments__c` = "Dynamische Erzeugung" ';
  

    $strSql6.= ' GROUP BY `Betriebsnummer_ZA__c` ';
    
    //echo $strSql6;
    
    $arrEventList = MySQLStatic::Query($strSql6);
    
    foreach ($arrEventList as $intEv => $arrEvent) {
  
      //if ($arrEvent['Name'] == 'E-186305') echo $arrEvent['Name'] .chr(10);

      $strSqlAc = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c`="' .$arrEvent['Betriebsnummer_ZA__c'] .'"';
      //if ($arrEvent['Name'] == 'E-186305') echo $strSqlAc .chr(10);
      $arrAc = MySQLStatic::Query($strSqlAc);

      //PremiumPayer gesuchtem Team zugeordnet
      $strSql7 = 'SELECT * FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrAc[0]['Id'] .'" AND `Information_Provider__c` = "' .$arrKK[0]['Id'] .'" AND `Anfragestelle__c` = "' .$strAnfragestelleId .'"';
      //if ($arrEvent['Name'] == 'E-186305') echo $strSqlAc .chr(10);
      $arrResult7 = MySQLStatic::Query($strSql7);
      
      //if ($arrEvent['Name'] == 'E-186305') echo '(count($arrResult7) == 0): ' .count($arrResult7) .chr(10);

      if (count($arrResult7) == 0) {
        
        $strSql8 = 'SELECT `Id` FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrAc[0]['Id'] .'" AND `Information_Provider__c` = "' .$arrKK[0]['Id'] .'"';
        //if ($arrEvent['Name'] == 'E-186305') echo $strSql8 .chr(10);
        $arrResult8 = MySQLStatic::Query($strSql8);
      
        //if ($arrEvent['Name'] == 'E-186305') echo '(count($arrResult8) == 0): ' .count($arrResult8) .chr(10);
        
        //keinem Team zugeordnet
        if (count($arrResult8) == 0) {
          $arrPayerAccount[] = $arrEvent['Betriebsnummer_ZA__c'];
          //if ($arrEvent['Name'] == 'E-186305') echo $arrEvent['Betriebsnummer_ZA__c'] .chr(10);
        }
        
      }
      
    }

    $strPpAccount = implode(',', $arrPayerAccount);
      
    if ($_SERVER['REMOTE_ADDR'] == '88.217.15.113') {
      // echo '|' .print_r($arrPayerAccount, true) . chr(10) .print_r($arrCatchAllPp, true); die();
    }          



    
  }

  $strSql = 'SELECT `SF42_IZSEvent__c`.*, `Account`.`Dezentrale_Anfrage__c` AS `Dezentrale_Anfrage__c`, `Account`.`Name` AS `IpName`, `Acc`.`Id` AS `PpId` ';
  $strSql.= 'FROM `SF42_IZSEvent__c` ';
  $strSql.= 'INNER JOIN `Account` ON `Account`.`Id` = `SF42_IZSEvent__c`.`SF42_informationProvider__c` ';
  $strSql.= 'INNER JOIN `Account` AS `Acc` ON `Acc`.`SF42_Comany_ID__c` = `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c` ';
  $strSql.= 'WHERE 1 ';
  
  if ($strKlaerung != '') {
    $strSql.= 'AND ((`Status_Klaerung__c` = "' .$strKlaerung .'") AND (`SF42_EventStatus__c` =  "enquired")) ';
  }

  if ($strResult != '') {
    if ($strResult == 'enquired') {
      $strSql.= 'AND ((`SF42_EventStatus__c` =  "enquired") AND (`Status_Klaerung__c` != "")) ';
    } else {
      $strSql.= 'AND (`SF42_EventStatus__c` =  "' .$strResult .'") ';
    }
  }

  if (($strKlaerung == '') && ($strResult == '')) {
    $strSql.= 'AND (((`SF42_EventStatus__c` =  "enquired") AND (`Status_Klaerung__c` != "")) OR (`SF42_EventStatus__c` =  "OK")) ';
  }

  if ($strStundung != '') {
    $strSql.= 'AND `Stundung__c` = "' .$strStundung .'" ';
  }

    
  if (($_REQUEST['strSelInf'] != '') && ($_REQUEST['strSelInf'] != 'all')) {
    $strSql.= 'AND ((`SF42_IZSEvent__c`.`SF42_informationProvider__c` = "' .@$arrKK[0]['Id'] .'") OR (`SF42_IZSEvent__c`.`SF42_informationProvider__c` = "' .$_REQUEST['strSelInf'] .'")) ';
  }
  $strSql.= 'AND (`Beitragsmonat__c` = "' .(int) $arrMonth[0] .'") AND (`Beitragsjahr__c` = "' .(int) $arrMonth[1] .'") ';


  if ($strRueckValue != '') {
    $strSql.= 'AND `Rueckmeldung_am__c` = "' .date('Y-m-d', strtotime($strRueckValue)) .'" ';
  }

  
  if (strlen($strPpAccount) > 0) {
    $strSql.= 'AND `Betriebsnummer_ZA__c` IN (' .$strPpAccount .') '; //Betriebsnummer  
  }

  //echo 
  $strSql.= 'AND `SF42_IZSEvent__c`.`RecordTypeId` = "01230000001Ao76AAC" AND `Art_des_Dokuments__c` = "Dynamische Erzeugung" ';
  $strSql.= 'ORDER BY `Betriebsnummer_ZA__c` ASC';

  $arrEventListRaw = MySQLStatic::Query($strSql);

  if ((isset($_REQUEST['sent'])) && ($_REQUEST['sent'] == 1)) {
    $arrEventList = $arrEventListRaw;
  } else {
    $arrEventList = array();
  }

  
} else {

  $strSql = 'SELECT `SF42_IZSEvent__c`.*, `Account`.`Dezentrale_Anfrage__c` AS `Dezentrale_Anfrage__c`, `Account`.`Name` AS `IpName`, `Acc`.`Id` AS `PpId` ';
  $strSql.= 'FROM `SF42_IZSEvent__c` ';
  $strSql.= 'INNER JOIN `Account` ON `Account`.`Id` = `SF42_IZSEvent__c`.`SF42_informationProvider__c` ';
  $strSql.= 'INNER JOIN `Account` AS `Acc` ON `Acc`.`SF42_Comany_ID__c` = `SF42_IZSEvent__c`.`Betriebsnummer_ZA__c` ';
  $strSql.= 'WHERE 1 ';
  $strSql.= 'AND `SF42_Month__c` = "' .(int) $arrMonth[0] .'" AND `SF42_Year__c` = "' .$arrMonth[1] .'" ';

  if (($strIpId != '') && ($strIpId != 'all')) {
    $strSql.= 'AND `SF42_IZSEvent__c`.`SF42_informationProvider__c` = "' .$strIpId .'" ';
  }

  if ($strKlaerung != '') {
    $strSql.= 'AND ((`Status_Klaerung__c` = "' .$strKlaerung .'") AND (`SF42_EventStatus__c` =  "enquired")) ';
  }

  if ($strResult != '') {
    if ($strResult == 'enquired') {
      $strSql.= 'AND ((`SF42_EventStatus__c` =  "enquired") AND (`Status_Klaerung__c` != "")) ';
    } else {
      $strSql.= 'AND (`SF42_EventStatus__c` =  "' .$strResult .'") ';
    }
  }

  if (($strKlaerung == '') && ($strResult == '')) {
    $strSql.= 'AND (((`SF42_EventStatus__c` =  "enquired") AND (`Status_Klaerung__c` != "")) OR (`SF42_EventStatus__c` =  "OK")) ';
  }

  if ($strStundung != '') {
    $strSql.= 'AND `Stundung__c` = "' .$strStundung .'" ';
  }

  if ($strRueckValue != '') {
    $strSql.= 'AND `Rueckmeldung_am__c` = "' .date('Y-m-d', strtotime($strRueckValue)) .'" ';
  }

  $strSql.= 'AND `SF42_IZSEvent__c`.`RecordTypeId` = "01230000001Ao76AAC" AND `Art_des_Dokuments__c` = "Dynamische Erzeugung" ';
  $arrEventListRaw = MySQLStatic::Query($strSql);

  if ((isset($_REQUEST['sent'])) && ($_REQUEST['sent'] == 1)) {
    $arrEventList = $arrEventListRaw;
  } else {
    $arrEventList = array();
  }

}

//print_r($arrEventList); die();

///*
if (count($arrEventList) > 0) {
    
  foreach ($arrEventList as $intKey => $arrEvent) {
    
    $boolSecond = false;
    
    if ($arrEvent['Dezentrale_Anfrage__c'] == 'true') { //DEZENTRAL
    
      $strSql4 = 'SELECT `Anfragestelle__c`  FROM `Dezentrale_Anfrage_KK__c` WHERE `Premium_Payer__c` = "' .$arrEvent['PpId'] .'" AND `Information_Provider__c` = "' .$arrEvent['SF42_informationProvider__c'] .'"';
      $arrResult4 = MySQLStatic::Query($strSql4);

      if (count($arrResult4) > 0) {
      
        $strSql5 = 'SELECT `Id`, `Name` FROM `Anfragestelle__c` WHERE `Id` = "' .$arrResult4[0]['Anfragestelle__c'] .'"';
        $arrResult5 = MySQLStatic::Query($strSql5);

        $arrEventList[$intKey]['AnfrageId'] = $arrResult5[0]['Id'];
        $arrEventList[$intKey]['AnfrageName'] = $arrResult5[0]['Name'];
        $arrEventList[$intKey]['AnfrageType'] = 'd';
      
      } else { //CATCH ALL
      
        $strSql5b = 'SELECT `Id`, `Name` FROM `Anfragestelle__c` WHERE `Information_Provider__c` = "' .$arrEvent['SF42_informationProvider__c'] .'" AND `CATCH_ALL__c` = "true"';
        $arrResult5b = MySQLStatic::Query($strSql5b);

        $arrEventList[$intKey]['AnfrageId'] = $arrResult5b[0]['Id'];
        $arrEventList[$intKey]['AnfrageName'] = $arrResult5b[0]['Name'];
        $arrEventList[$intKey]['AnfrageType'] = 'c';
        
      }
    
    } else { // ZENTRAL
      
      $arrEventList[$intKey]['AnfrageId'] = $arrEvent['SF42_informationProvider__c'];
      $arrEventList[$intKey]['AnfrageName'] = $arrEvent['IpName'];
      $arrEventList[$intKey]['AnfrageType'] = 'z';

    }
    
    //$arrEventList[] = $arrEvent;
    
  } 

}
//*/

//print_r($arrResult); die();


//SETTINGS

$boolShowHead = false;
$boolShowSide = false;

$strPageTitle = 'Prüf-Dashboard';

//CONTENT

$strCssHead.= '
<style>
.nav-tabs > li > a {
  -moz-border-bottom-colors: none;
  -moz-border-left-colors: none;
  -moz-border-right-colors: none;
  -moz-border-top-colors: none;
  background-color: #f3f7f9;
  border-color: #f3f7f9 #f3f7f9 transparent;
  border-image: none;
  border-style: solid;
  border-width: 1px;
  color: #37474f;
}
.page {
  min-width: 2200px !important;
}
</style>
';

$strOutput = '
  <!-- Page -->
  <div class="animsition container-fluid">
';

//[] - Ergebnis - Esc - Negativmerkmal (CRM) - EventID - Meldestelle - Anfragestelle - BNR - Falsch/Richtig - Rückmeldung am - Klärung Status - Klärung Grund 

$arrTableHead = array(
    '<input type="checkbox" name="check_all">',
    'Ergebnis',
    'Esc',
    'Negativmerkmal (CRM)',
    'Event Id', 
    'Meldestelle', 
    'Anfragestelle',
    'BNR', 
    ' ', 
    ' ', 
    'Rückm. am', 
    'Klärung Status',
    'Klärung Grund',
    'Stundung / Ratenzahlung',
);


$arrList = array();
foreach ($arrEventList as $intRow => $arrEvent) {

  //print_r($arrEvent); die();

  $strRueckAm = '';
  if ($arrEvent['Rueckmeldung_am__c'] != '0000-00-00') {
    $strRueckAm = date('d.m.Y', strtotime($arrEvent['Rueckmeldung_am__c']));
  }

  if ($arrEvent['SF42_EventStatus__c'] == 'OK') {
    $strBtnRight = '<button type="" class="btn right proof" id="p_' .$arrEvent['evid'] .'_r">Richtig </button>';
    $strBtnWrong = '<button type="" class="btn wrong proof" id="p_' .$arrEvent['evid'] .'_w">Falsch </button>';
  } else {
    $strBtnRight = '';
    $strBtnWrong = '';
  }

  if ($arrEvent['Status_Klaerung__c'] == 'in Klärung') {
    $strBtnRight = '';
    $strBtnWrong = '<button type="" class="btn wrong proof" id="p_' .$arrEvent['evid'] .'_k">Falsch </button>';
  }

  $strNvm = '';
  $strKey = $arrEvent['Betriebsnummer_ZA__c'] .'_' .$arrEvent['Betriebsnummer_IP__c'];
  if (isset($arrNegativMerkmal[$strKey])) {
    $strNvm = $arrNegativMerkmal[$strKey];
  }

  $strMeldestelle = '<a href="http://crm.izs-institut.de/mel/' .$arrMelListId[$arrEvent['Betriebsnummer_ZA__c']] .'" target="_blank">' .$arrMelList[$arrEvent['Betriebsnummer_ZA__c']] .'</a>';

  $strBox = '';
  if ($arrEvent['SF42_EventStatus__c'] == 'OK') {
    $strBox = '<input type="checkbox" name="ev_' .$arrEvent['Id'] .'" class="selevent">';
  }

  $strGroupId = $arrEvent['group_id'];

  if (isset($arrEscalation[$strGroupId])) { // 'a083000000D4hUMAAZ' https://izs.smartgroups.io/frontend/main/topic/topic-97f48d16-eb79-48c7-b450-6085ab217700

    preg_match('/topic-[\w-]*/', $arrEscalation[$strGroupId]['Eskalation_Link__c'], $arrLink);
    $strSgLink = $arrLink[0] ?? '';

    $strEsc = '<a href="javascript:void(0)" alt="' .$arrEscalation[$strGroupId]['Eskalation_Grund__c'] .'" title="' .$arrEscalation[$strGroupId]['Eskalation_Grund__c'] .'" onclick="openSG(\'' .$strSgLink .'\')" style="color:red;"><i class="icon fa fa-warning" aria-hidden="true"></i></a>';

  } else {

    $strEsc = '';

  }
    
  $arrTemp = array(
    'box' => $strBox,
    'res' => '<span class="res_' .$arrResultList[$arrEvent['SF42_EventStatus__c']] .'">' .$arrResultList[$arrEvent['SF42_EventStatus__c']] .'</span>',
    'esc' => $strEsc,
    'Nvm' => $strNvm,
    'evid' => '<a href="/cms/index.php?ac=sear&det=' .$arrEvent['evid'] .'" target="_blank">' .$arrEvent['Name'] .'</a>', 
    'mel' => $strMeldestelle, 
    'ip' => $arrEvent['AnfrageName'], 
    'bnr' => '<span id="e_' .$arrEvent['Id'] .'">' .$arrEvent['Betriebsnummer_ZA__c'] .'</span>',
    'wrong' => $strBtnWrong, 
    'right' => $strBtnRight,
    'rueck' => $strRueckAm,
    'kstatus' => $arrEvent['Status_Klaerung__c'],
    'kgrund' => $arrEvent['Grund__c'],
    'stund' => '<span class="red">' .$arrEvent['Stundung__c'] .'</span>', 
  );

  $arrList[] = $arrTemp;

}

// '"bSortable": false', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null'
$arrOpt = array(
    'arrSort' => array(
        '"bSortable": false', 'null', 'null', 'null', 'null', 'null', 'null', 'null', '"bSortable": false', '"bSortable": false', 'null', 'null', 'null', 'null'
    ),
    'strOrder' => '"order": [[ 5, "asc" ]]'
);


$strTable = strCreateTable($arrList, $arrTableHead, '', false, true, null, $arrOpt);

$strIncOutput.= ' 

<style>
/*!
 * Datepicker for Bootstrap
 *
 * Copyright 2012 Stefan Petre
 * Licensed under the Apache License v2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 */
.datepicker {
  top: 0;
  left: 0;
  padding: 4px;
  margin-top: 1px;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  border-radius: 4px;
  /*.dow {
    border-top: 1px solid #ddd !important;
  }*/

}
.datepicker:before {
  content: \'\';
  display: inline-block;
  border-left: 7px solid transparent;
  border-right: 7px solid transparent;
  border-bottom: 7px solid #ccc;
  border-bottom-color: rgba(0, 0, 0, 0.2);
  position: absolute;
  top: -7px;
  left: 6px;
}
.datepicker:after {
  content: \'\';
  display: inline-block;
  border-left: 6px solid transparent;
  border-right: 6px solid transparent;
  border-bottom: 6px solid #ffffff;
  position: absolute;
  top: -6px;
  left: 7px;
}
.datepicker > div {
  display: none;
}
.datepicker table {
  width: 100%;
  margin: 0;
}
.datepicker td,
.datepicker th {
  text-align: center;
  width: 20px;
  height: 20px;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  border-radius: 4px;
}
.datepicker th {
    background-color: rgba(255,243,230,.8);
}
.datepicker td.day:hover {
  background: #eeeeee;
  cursor: pointer;
}
.datepicker td.day.disabled {
  color: #eeeeee;
}
.datepicker td.old,
.datepicker td.new {
  color: #999999;
}
.datepicker td.active,
.datepicker td.active:hover {
  color: #ffffff;
  background-color: #006dcc;
  background-image: -moz-linear-gradient(top, #0088cc, #0044cc);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#0088cc), to(#0044cc));
  background-image: -webkit-linear-gradient(top, #0088cc, #0044cc);
  background-image: -o-linear-gradient(top, #0088cc, #0044cc);
  background-image: linear-gradient(to bottom, #0088cc, #0044cc);
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#ff0088cc\', endColorstr=\'#ff0044cc\', GradientType=0);
  border-color: #0044cc #0044cc #002a80;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  *background-color: #0044cc;
  /* Darken IE7 buttons by default so they stand out more given they won\'t have borders */

  filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
  color: #fff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
}
.datepicker td.active:hover,
.datepicker td.active:hover:hover,
.datepicker td.active:focus,
.datepicker td.active:hover:focus,
.datepicker td.active:active,
.datepicker td.active:hover:active,
.datepicker td.active.active,
.datepicker td.active:hover.active,
.datepicker td.active.disabled,
.datepicker td.active:hover.disabled,
.datepicker td.active[disabled],
.datepicker td.active:hover[disabled] {
  color: #ffffff;
  background-color: #0044cc;
  *background-color: #003bb3;
}
.datepicker td.active:active,
.datepicker td.active:hover:active,
.datepicker td.active.active,
.datepicker td.active:hover.active {
  background-color: #003399 \9;
}
.datepicker td span {
  display: block;
  width: 47px;
  height: 54px;
  line-height: 54px;
  float: left;
  margin: 2px;
  cursor: pointer;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  border-radius: 4px;
}
.datepicker td span:hover {
  background: #eeeeee;
}
.datepicker td span.active {
  color: #ffffff;
  background-color: #006dcc;
  background-image: -moz-linear-gradient(top, #0088cc, #0044cc);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#0088cc), to(#0044cc));
  background-image: -webkit-linear-gradient(top, #0088cc, #0044cc);
  background-image: -o-linear-gradient(top, #0088cc, #0044cc);
  background-image: linear-gradient(to bottom, #0088cc, #0044cc);
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#ff0088cc\', endColorstr=\'#ff0044cc\', GradientType=0);
  border-color: #0044cc #0044cc #002a80;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  *background-color: #0044cc;
  /* Darken IE7 buttons by default so they stand out more given they won\'t have borders */

  filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
  color: #fff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
}
.datepicker td span.active:hover,
.datepicker td span.active:focus,
.datepicker td span.active:active,
.datepicker td span.active.active,
.datepicker td span.active.disabled,
.datepicker td span.active[disabled] {
  color: #ffffff;
  background-color: #0044cc;
  *background-color: #003bb3;
}
.datepicker td span.active:active,
.datepicker td span.active.active {
  background-color: #003399 \9;
}
.datepicker td span.old {
  color: #999999;
}
.datepicker th.switch {
  width: 145px;
}
.datepicker th.next,
.datepicker th.prev {
  font-size: 21px;
}
.datepicker thead tr:first-child th {
  cursor: pointer;
}
.datepicker thead tr:first-child th:hover {
  background: #eeeeee;
}
.input-append.date .add-on i,
.input-prepend.date .add-on i {
  display: block;
  cursor: pointer;
  width: 16px;
  height: 16px;
}

.datepicker-days table tr:second-child td {
  background-color: #f2a654;
}

.today {
  background-color: #d3d3d3;
}

</style>

    <div class="panel">

      <div class="panel-body">
      <!--
      <div style="z-index: 1; top: 190px !important;" class="page-header-actions">

        <div class="panel-actions">

            <div class="dropdown">
            <button type="button" class="btn btn-info dropdown-toggle pull-right" id="exampleAlignmentDropdown"
            data-toggle="dropdown" aria-expanded="true">
                Optionen
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="exampleAlignmentDropdown" role="menu">
                <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="doXlsxExport">Export XLSX</a></li>
            </ul>
            </div>

        </div>
      </div>
      -->
  
        <div class="row">
          <div class="col-lg-12">
          
';
$strIncOutput.= '<form class="form-horizontal" action="/cms/index_neu.php?ac=' .$_REQUEST['ac'] .'&clid=' .$_SESSION['id'] .'" id="search" method="post">' .chr(10);
$strIncOutput.= '<input type="hidden" name="sent" id="sent" value="1">' .chr(10);
$strIncOutput.= $strSelect;
$strIncOutput.= $strSend;
$strIncOutput.= '</form>' .chr(10);

$strIncOutput.= '
      <header class="panel-heading">
      <h3 class="panel-title" style="padding-left: 0px; width: 250px; float: left;"><span id="DocCount">' .count($arrList) .'</span> Events gefunden</h3>

    <div style="float: left; display: none; margin-top: 14px;" id="massOkButton">
      <button class="btn right" type="button" id="massOkClick"><span class="ctCheck"></span> Event(s) auf "Richtig" setzen</button>
    </div>

    </header>

    <script>
    function openSG(topicId) {
      if (topicId != "") {
        setTimeout(()=>smartchat.cmd("topic.show", {entity: {id: topicId}}), 150);
      }
    }
    </script>

' .chr(10);
$strIncOutput.= $strTable;

$strIncOutput.= '
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->


    <!-- Modal -->
    <div class="modal fade modal-info" id="modalKlaerung" aria-hidden="true"
    aria-labelledby="modalKlaerung" role="dialog" tabindex="-1">
      <form id="klform">
      <input type="hidden" name="klevid" id="klevid" value="" />
      <div class="modal-dialog modal-center" style="width: 700px" >
  
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title">Neuen Klärungsfall anlegen</h4>
          </div>
          <div class="modal-body">
  
            <div class="form-group form-material ">
              <label class="col-sm-3 control-label">Status</label>
              <div class="col-sm-6" style="padding-left: 0px;">
                <input type="hidden" name="klstatus" id="klstatus" value="in Klärung" />
                <span class="red"><strong>in Klärung</strong></span>
              </div>
              <div class="col-sm-3" style="padding-left: 0px; height:36px"></div>
            </div>
            <div class="form-group form-material ">
              <label class="col-sm-3 control-label">Grund</label>
              <div class="col-sm-6" style="padding-left: 0px;">
                <select id="klgrund" name="klgrund" style="height: 25px;">
                  <option value=""></option>
';

///*
$arrGrund = array(
  'Beitragsnachweis fehlt',
	'Beitragsrückstand',
	'Keine Auskunft von KK',
	'Konto geschlossen',
	'Kein Konto',
	'Ratenvereinbarung',
	'Stundung',
	'Sonstiges'
);
//*/

foreach ($arrGrund as $strKey => $strGrundName) {
    $strIncOutput.= '                  <option value="' .$strGrundName .'">' .$strGrundName .'</option>' .chr(10);
}

$strIncOutput.= '
                </select>
              </div>
              <div class="col-sm-3" style="padding-left: 0px; height:36px"></div>
            </div>
            <div class="form-group form-material ">
              <label class="col-sm-3 control-label">Nächster Schritt</label>
              <div class="col-sm-6" style="padding-left: 0px;">
                <select name="klschritt" id="klschritt" style="height: 25px;">
                  <option value=""></option>
';

              foreach ($arrMeilenstein as $strKey => $strName) {
                $strIncOutput.= '                  <option value="' .$strKey .'">' .$strName .'</option>';
              }

$strIncOutput.= '
                </select>
              </div>
              <div class="col-sm-3" style="padding-left: 0px; height:36px"></div>
            </div>
            <div class="form-group form-material ">
              <label class="col-sm-3 control-label">Ansprechpartner</label>
              <div class="col-sm-6" style="padding-left: 0px;">
                <input type="text" name="klap" id="klap" style="height: 25px; width: 300px;">
              </div>
              <div class="col-sm-3" style="padding-left: 0px; height:36px"></div>
            </div>

            <div class="form-group form-material ">
              <label class="col-sm-3 control-label">Telefon</label>
              <div class="col-sm-6" style="padding-left: 0px;">
                <input type="text" name="kltelefon" id="kltelefon" style="height: 25px; width: 300px;">
              </div>
              <div class="col-sm-3" style="padding-left: 0px; height:36px"></div>
            </div>

            <div class="form-group form-material ">
              <label class="col-sm-3 control-label">Wiedervorlage</label>
              <div class="col-sm-6" style="padding-left: 0px;">
                <input name="klwieder" id="klwieder" type="text" class="dp" data-plugin="datepicker" data-date-format="dd.mm.yyyy">
              </div>
              <div class="col-sm-3" style="padding-left: 0px; height:36px"></div>
            </div>

            <div class="form-group form-material ">
              <label class="col-sm-3 control-label">Status-Info</label>
              <div class="col-sm-6" style="padding-left: 0px;">
                <textarea name="klinfo" id="klinfo" style="width: 300px"></textarea>
              </div>
              <div class="col-sm-3" style="padding-left: 0px; height:72px"></div>
            </div>

          </div>
          <div class="modal-footer">
            <button class="btn btn-primary margin-top-5 pull-right" type="button" id="modalKlaerungSubmit">anlegen</button>
            <button data-dismiss="modal" class="btn btn-default btn-default margin-top-5 margin-right-10 pull-right" type="button">Abbrechen</button>
          </div>
        </div>
      </form>
  
      </div>
    </div>
    <!-- End Modal -->



    <!-- Modal -->
    <div class="modal fade modal-info" id="modalKlaerungUndo" aria-hidden="true"
    aria-labelledby="modalKlaerungUndo" role="dialog" tabindex="-1">
      <form id="kluform">

      <input type="hidden" name="kluevid" id="kluevid" value="" />
      <input type="hidden" name="klukstatus" id="klukstatus" value="geschlossen / positiv" />
      <input type="hidden" name="klustatus" id="klustatus" value="OK" />
      <input type="hidden" name="kluart" id="kluart" value="Dynamische Erzeugung" />

      <div class="modal-dialog modal-center" style="width: 700px" >
  
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title">Einen Klärungsfall positiv auflösen</h4>
          </div>
          <div class="modal-body">
            
            <div class="form-group form-material " style="padding-left: 0px; height:16px">
              <div class="col-sm-10" style="padding-left: 0px;">Der folgende Vorfall ist derzeit noch ein Klärungsfall:</div>
              <button class="btn btn-default pull-right" type="button" id="refreshUndo">Refresh</button>
            </div>
  
            <div class="form-group form-material ">
              <label class="col-sm-4 control-label">Event-Id</label>
              <div class="col-sm-6" style="padding-left: 0px;">
                <span id="klu_name">E-12345</span>
              </div>
              <div class="col-sm-2" style="padding-left: 0px; height:36px"></div>
            </div>
            <div class="form-group form-material ">
              <label class="col-sm-4 control-label">Personaldienstleister</label>
              <div class="col-sm-6" style="padding-left: 0px;">
                <span id="klu_ppname">Name PP</span>
              </div>
              <div class="col-sm-2" style="padding-left: 0px; height:36px"></div>
            </div>
            <div class="form-group form-material ">
              <label class="col-sm-4 control-label">BNR</label>
              <div class="col-sm-6" style="padding-left: 0px;">
                <span id="klu_bnr">0987654321</span>
              </div>
              <div class="col-sm-2" style="padding-left: 0px; height:36px"></div>
            </div>
            <div class="form-group form-material ">
              <label class="col-sm-4 control-label">Negativmerkmal (CRM)</label>
              <div class="col-sm-6" style="padding-left: 0px;">
                <span id="klu_nvm">NVM</span>
              </div>
              <div class="col-sm-2" style="padding-left: 0px; height:36px"></div>
            </div>

            <div class="form-group form-material ">
              <label class="col-sm-4 control-label">Klärung / Grund</label>
              <div class="col-sm-6" style="padding-left: 0px;">
                <span id="klu_grund">Keine Ahnung</span>
              </div>
              <div class="col-sm-2" style="padding-left: 0px; height:36px"></div>
            </div>
            
            <div class="form-group form-material ">
              <div class="col-sm-12" style="padding-left: 0px;">Du möchtest den Klärungsfall nun positiv auflösen.<br />
              Das Ergebnis wird dabei auf "OK" gesetzt. Eine Freigabe erfolgt noch nicht.</div>
            </div>

          </div>
          <div class="modal-footer">
            <button class="btn btn-primary margin-top-5 pull-right" type="button" id="modalKlaerungUndoSubmit">Auflösen</button>
            <button data-dismiss="modal" class="btn btn-default btn-default margin-top-5 margin-right-10 pull-right" type="button">Abbrechen</button>
          </div>
        </div>
      </form>
  
      </div>
    </div>
    <!-- End Modal -->

    <!-- Modal -->
    <div class="modal fade modal-info" id="modalMassProof" aria-hidden="true"
    aria-labelledby="modalMassProof" role="dialog" tabindex="-1">

      <div class="modal-dialog modal-center" style="width: 700px" >
  
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title">Ausgewählte Events mit "Richtig" freigeben</h4>
          </div>
          <div class="modal-body">
            
            <div id="modalContent">
              <p>Alle Ausgewählten Events (<span class="ctCheck"></span>) wurden mit "Richtig" freigegeben.</p>
            </div>

          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default btn-default margin-top-5 margin-right-10 pull-right" type="button">OK</button>
          </div>

        </div>

      </div>
    </div>
    <!-- End Modal -->

';

$strJsFootCodeRun.= '

$("#massOkClick").click(function(){

  var list = new Array();

  $.each($(".selevent:checked"), function () {
    var selId = $(this).attr("name").split("_");
    list.push(selId[1]);
  });

  $("#modalMassProof").modal("show");

  $.post("inc/svproof.ajax.php", { 
    ac: "proof", evlist: list, result: 1
  }, "json").done(function(data) {

    var data = $.parseJSON(data);

    $.each(data, function( index, value ) {
      $("[name=\'ev_" + value +"\']").parent().parent().hide();
      $("#DocCount").text(parseInt($("#DocCount").text()) - 1);
      $("[name=\'ev_" + value +"\']").remove();
    });

    $("#massOkButton").hide();

  });

});

function showButton() {
  var checked = $(".selevent").filter(":checked").length;
  $(".ctCheck").text(checked);
  if (checked > 0) {
    $("#massOkButton").show();
  } else {
    $("#massOkButton").hide();
  }
}

$(".selevent").change(function(){
  showButton();
});

$("input[name=\'check_all\']").click(function(){
    var cblist = $("input[name^=\"ev_\"]");
    cblist.prop("checked", $("input[name=\'check_all\']").prop("checked"));
    showButton();
});

$("#doXlsxExport").on("click", function(e) {

    var header = new Array();
    var list   = new Array();

    $.each($("table[id^=\"table_\"] thead th"), function () {
        if ($(this).text() != "") header.push($(this).text());
    });

    $.each($("table[id^=\"table_\"] tbody tr"), function () {
        var tdarr = new Array();
        $.each($(this).find("td"), function () {
            if ($(this).children(":first").length == 0) tdarr.push($(this).text());
        });
        list.push(tdarr);
    });

    var fn = "' .str_replace(' ', '_', $strPageTitle) .'_" + $.format.date(new Date(), "yyyyMMdd_Hmmss") + ".xlsx";

    $.post("inc/dk.ajax.php", { 
        ac: "export", list: list, header: header, format: "xlsx", name: fn
      }).done(function(data) {
        location.href="/cms/files/temp/" + fn;
      }
    );
    
});

$("#modalKlaerungSubmit").on("click", function(e) {

  var evid    = $("#klevid").val();
  var status  = $("#klstatus").val();
  var grund   = $("#klgrund").val();
  var schritt = $("#klschritt").val();
  var ap      = $("#klap").val();
  var telefon = $("#kltelefon").val();
  var wieder  = $("#klwieder").val();
  var info    = $("#klinfo").val();

  $.post("inc/svproof.ajax.php", { 
    ac: "klaerung", evid: evid, result: 0, status: status, grund: grund, schritt: schritt, ap: ap, telefon: telefon, wieder: wieder, info: info
  }).done(function(data) {
    $("#modalKlaerung").modal("hide");
    $("#p_" + $("#klevid").val() + "_w").parent().parent().hide();
    $("#DocCount").text(parseInt($("#DocCount").text()) - 1);
  });

});

$(document).on("click", ".proof", function(e) {

    var elem = $(this);
    var part = $(this).attr("id").split("_");
    var type = part[2];
    var evid = part[1];

    if (type == "r") { 

      $.post("inc/svproof.ajax.php", { 
        ac: "proof", evid: evid, result: 1
      }).done(function(data) {
        elem.parent().parent().hide();
        $("#DocCount").text(parseInt($("#DocCount").text()) - 1);
      });

    } else if (type == "k") { // Klärung

      $("#kluform").trigger("reset");
      $("#kluevid").val(evid);

      var pa = elem.parent().parent().children();

      $("#klu_name").html(pa.eq(4).html());
      $("#klu_ppname").html(pa.eq(5).html());
      $("#klu_bnr").html(pa.eq(7).text());

      $("#refreshUndo").trigger("click");

      $("#kluevid").val(evid);
      $("klukstatus").val(evid);

      $("#modalKlaerungUndo").modal("show");

    } else {

      $("#klform").trigger("reset");;
      $("#klevid").val(evid);
      $("#modalKlaerung").modal("show");

    }

});

$("#refreshUndo").on("click", function(e) {

  $.post("inc/svproof.ajax.php", { 
    ac: "refresh", evid: $("#kluevid").val()
  }, "json").done(function(data) {
    var data = $.parseJSON(data);
    $("#klu_nvm").html(data.nvm);
    $("#klu_grund").html(data.grund);
  });

});

$("#modalKlaerungUndoSubmit").on("click", function(e) {

  $.post("inc/svproof.ajax.php", { 
    ac: "undo", evid: $("#kluevid").val()
  }, "json").done(function(data) {
    data = $.parseJSON(data);

    var tr = $("#e_" +$("#kluevid").val()).parent().parent();
    
    tr.children().eq(0).html(\'<input type="checkbox" name="ev_\' + $("#kluevid").val() + \'" class="selevent">\');
    tr.children().eq(1).html(\'<span class="res_OK">OK</span>\');
    tr.children().eq(13).html(\'<span class="red">\' + data.stundung + \'<\/span>\');
    $("#p_" + $("#kluevid").val() + "_k").attr("id", "p_" + $("#kluevid").val() + "_w");
    tr.children().eq(11).html(\'geschlossen / positiv\');
    tr.children().eq(9).html(\'<button type="" class="btn right proof" id="p_\' + $("#kluevid").val() + \'_r">Richtig <\/button>\');

    $("#modalKlaerungUndo").modal("hide");

  });

});

';

?>