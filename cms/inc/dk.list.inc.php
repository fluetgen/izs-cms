<?php

//REQUEST

$strCtIP = @$_REQUEST['ct_ip'];

if (isset($_REQUEST['dateStart']) && ($_REQUEST['dateStart'] != '')) {
    $arrPartList = explode('.', $_REQUEST['dateStart']);
    $strStart = $arrPartList[2] .'-' .$arrPartList[1] .'-' .$arrPartList[0];
} else {
    $strStart = date('Y-m-d', mktime(0,0,0,(date('m')-1),15));
}

if (isset($_REQUEST['dateStop']) && ($_REQUEST['dateStop'] != '')) {
    $arrPartList = explode('.', $_REQUEST['dateStop']);
    $strStop = $arrPartList[2] .'-' .$arrPartList[1] .'-' .$arrPartList[0];
} else {
    $strStop = date('Y-m-16');
}

$strSql = 'SELECT * FROM `cms_login` WHERE `cl_id` = "' .$_SESSION['id'] .'"';
$arrUser = MySQLStatic::Query($strSql);

$strSql = 'SELECT * FROM `izs_premiumpayer_group`';
$arrPPListRaw = MySQLStatic::Query($strSql);

$arrPPList = array();
foreach ($arrPPListRaw as $intKey => $arrPP) {
    $arrPPList[$arrPP['Id']] = $arrPP['Name'];
}


$strSql = 'SELECT * FROM `Account` WHERE `SF42_isPremiumPayer__c` = "true" AND `SF42_use_data__c` = "true" ';
$strSql.= 'AND `Registriert_seit__c` > "' .$strStart .'" AND `Registriert_seit__c` < "' .$strStop .'"';
$arrAccountList = MySQLStatic::Query($strSql);

$strDateStart = '

    <div class="form-group">
        <label class="col-sm-1 control-label">Von</label>
        <div class="input-group col-sm-9">
        <span class="input-group-addon">
            <i class="icon wb-calendar today" aria-hidden="true"></i>
        </span>
        <input id="dateStart" name="dateStart" type="text" class="form-control dp" data-plugin="datepicker" value="' .date('d.m.Y', strtotime($strStart)) .'" data-date-format="dd.mm.yyyy">
        </div>
    </div>

';
$strDateStop  = '

    <div class="form-group">
        <label class="col-sm-1 control-label">Bis</label>
        <div class="input-group col-sm-9">
        <span class="input-group-addon">
            <i class="icon wb-calendar today" aria-hidden="true"></i>
        </span>
        <input id="dateStop" name="dateStop" type="text" class="form-control dp" data-plugin="datepicker" value="' .date('d.m.Y', strtotime($strStop)) .'" 
        data-date-format="dd.mm.yyyy">
        </div>
    </div>

';

$strSql = 'SELECT * FROM  `Account` WHERE  `RecordTypeId` = "01230000001Ao72AAC" AND  `Neue_Meldestelle_Zuordnung__c` != "" ORDER BY `Name`';
$arrRecipientList = MySQLStatic::Query($strSql);



$strSelect = '

    <div class="form-group">
        <label class="col-sm-1 control-label">Krankenkasse</label>
        <div class="col-sm-2" style="padding-left: 0px;">
            <select class="custom-select form-control" id="ct_ip" name="ct_ip">
';

$strFirst = '';
foreach ($arrRecipientList as $strValue => $arrRecipient) {

    if ($strFirst == '') {
        $strFirst = $arrRecipient['Id'];
    }

    $strSelected = '';
    if ($strCtIP == $arrRecipient['Id']) {
        $strSelected = ' selected="selected"';
    }

    $strSelect.= '                        <option value="' .$arrRecipient['Id']. '"' .$strSelected .'>' .$arrRecipient['Name']. '</option>' .chr(10);
}

if (is_null($strCtIP)) {
    $strCtIP = $strFirst;
}

$strSelect.= '
            </select>
        </div>
        <div class="col-sm-2" style="padding-left: 0px;">
            <button type="submit" class="btn btn-primary">Suchen </button>
        </div>

    </div>

';

$strSend = '

    <div class="form-group">
        <label class="col-sm-1 control-label">
        </label>
        <div class="col-sm-2" style="padding-left: 0px;">
            <button type="button" id="test" class="btn btn-default">Testen </button>
            &nbsp;&nbsp;<button type="button" id="send" class="btn btn-primary">Senden </button>
        </div>
    </div>

';


//SETTINGS

$boolShowHead = false;
$boolShowSide = false;

$strPageTitle = 'Dezentrale Zuordnung KK';

//CONTENT

$strCssHead.= '
<style>
.nav-tabs > li > a {
  -moz-border-bottom-colors: none;
  -moz-border-left-colors: none;
  -moz-border-right-colors: none;
  -moz-border-top-colors: none;
  background-color: #f3f7f9;
  border-color: #f3f7f9 #f3f7f9 transparent;
  border-image: none;
  border-style: solid;
  border-width: 1px;
  color: #37474f;
}
</style>
';

$strOutput = '
  <!-- Page -->
  <div class="animsition container-fluid">
';

//$strOutput.= strCreatePageTitle('Kontakte', $arrIP[0]['ac_name']);

$arrTableHead = array(
    '<input type="checkbox" name="check_all">',
    'versendet am',
    'Accountname', 
    'Betriebsnummer', 
    'Firmengruppe', 
    'Registriert seit', 
    'Straße', 
    'PLZ', 
    'Ort'
);

$arrList = array();
foreach ($arrAccountList as $intRow => $arrAccount) {

  $strSql = 'SELECT * FROM `izs_inform_ip` WHERE `ii_ip` = "' .$strCtIP .'" AND `ii_pp` = "' .$arrAccount['Id'] .'" ORDER BY `ii_date` DESC';
  $arrInform = MySQLStatic::Query($strSql);
  
  $strInform = '';
  if (count($arrInform) > 0) {
    $strInform = date('d.m.Y H:i:s', strtotime($arrInform[0]['ii_date']));
  }
    
  $arrTemp = array(
    'box' => '<input type="checkbox" name="acc_' .$arrAccount['Id'] .'">',
    'sent' => $strInform,
    'acc' => $arrAccount['Name'], 
    'btnr' => $arrAccount['SF42_Comany_ID__c'], 
    'group' => $arrPPList[$arrAccount['SF42_Company_Group__c']], 
    'reg' => date('d.m.Y', strtotime($arrAccount['Registriert_seit__c'])), 
    'street' => $arrAccount['BillingStreet'], 
    'zip' => $arrAccount['BillingPostalCode'], 
    'city' => $arrAccount['BillingCity']
  );

  $arrList[] = $arrTemp;

}

$arrOpt = array(
    'arrSort' => array(
        '"bSortable": false', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null'
    ),
    'strOrder' => '"order": [[ 2, "asc" ]]'
);


$strTable = strCreateTable($arrList, $arrTableHead, '', false, true, null, $arrOpt);

$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Accounts gefunden</h3>
      </header>
      <div class="panel-body">

      <div style="z-index: 1; top: 190px !important;" class="page-header-actions">

        <div class="panel-actions">

            <div class="dropdown">
            <button type="button" class="btn btn-info dropdown-toggle pull-right" id="exampleAlignmentDropdown"
            data-toggle="dropdown" aria-expanded="true">
                Optionen
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="exampleAlignmentDropdown" role="menu">
                <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="doXlsxExport">Export XLSX</a></li>
            </ul>
            </div>

        </div>

      </div>
  
        <div class="row">
          <div class="col-lg-12">
          
';
$strIncOutput.= '<form class="form-horizontal" action="/cms/index_neu.php?ac=' .$_REQUEST['ac'] .'&fu=' .$_REQUEST['fu'] .'&clid=' .$_SESSION['id'] .'" id="search" method="post">' .chr(10);
$strIncOutput.= $strDateStart;
$strIncOutput.= $strDateStop;
$strIncOutput.= $strSelect;
$strIncOutput.= '</form>' .chr(10);

$strIncOutput.= '<form class="form-horizontal">' .chr(10);
$strIncOutput.= $strSend;
$strIncOutput.= '</form>' .chr(10);

$strIncOutput.= $strTable;

$strIncOutput.= '
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

$strJsFootCodeRun.= '
$("input[name=\'check_all\']").click(function(){
    var cblist = $("input[name^=\"acc\"]");
    cblist.prop("checked", $("input[name=\'check_all\']").prop("checked"));
});

$("#test").on("click", function(e) {

    var templList = new Array();
    templList["001300000109k6gAAA"] = "AOKRH";
    templList["001300000109k5wAAA"] = "DAK";
    templList["001300000109k7aAAA"] = "AOKRPS";
    templList["001300000109k6OAAQ"] = "AOKNDS";


    var list = [];
    $.each($("input[name^=\"acc\"]:checked"), function(){            
        list.push($(this).prop("name").split("_")[1]);
    });

    //console.log(list.join(", "));

    $.post("inc/dk.ajax.php", { 
        ac: "test", list: list, templ: templList[$("#ct_ip").val()], ct_ip: $("#ct_ip").val()
      }).done(function(data) {
        toastr.options = { "closeButton": true, "positionClass": "toast-top-full-width", "newestOnTop": true,   "showMethod": "fadeIn", "hideMethod": "fadeOut" }
        toastr.success(\'Die Testnachricht(en) wurde(n) an "' .$arrUser[0]['cl_mail'] .'" versendet.\');
      }
    );
    
});  

$("#send").on("click", function(e) {

    var templList = new Array();
    templList["001300000109k6gAAA"] = "AOKRH";
    templList["001300000109k5wAAA"] = "DAK";
    templList["001300000109k7aAAA"] = "AOKRPS";
    templList["001300000109k6OAAQ"] = "AOKNDS";


    var list = [];
    $.each($("input[name^=\"acc\"]:checked"), function(){            
        list.push($(this).prop("name").split("_")[1]);
    });

    //console.log(list.join(", "));

    $.post("inc/dk.ajax.php", { 
        ac: "send", list: list, templ: templList[$("#ct_ip").val()], ct_ip: $("#ct_ip").val()
      }).done(function(data) {
        $("#search").submit();
      }
    );
    
});

$("#doXlsxExport").on("click", function(e) {

    var header = new Array();
    var list   = new Array();

    $.each($("table[id^=\"table_\"] thead th"), function () {
        if ($(this).text() != "") header.push($(this).text());
    });

    $.each($("table[id^=\"table_\"] tbody tr"), function () {
        var tdarr = new Array();
        $.each($(this).find("td"), function () {
            if ($(this).children(":first").length == 0) tdarr.push($(this).text());
        });
        list.push(tdarr);
    });

    var fn = "' .str_replace(' ', '_', $strPageTitle) .'_" + $.format.date(new Date(), "yyyyMMdd_Hmmss") + ".xlsx";

    $.post("inc/dk.ajax.php", { 
        ac: "export", list: list, header: header, format: "xlsx", name: fn
      }).done(function(data) {
        location.href="/cms/files/temp/" + fn;
      }
    );
    
});

';

?>