<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

session_start();

require_once('../const.inc.php');

require_once('../../assets/classes/class.mysql.php');
require_once('../../assets/classes/class.database.php');

require_once('refactor.inc.php');

$intUser = $_SESSION['id'];

$arrHeader = array(
  'user: ' .$intUser,
  'Accept: application/json'
);

if ((strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) || (strstr(__FILE__, 'test') !== false)) {
  $arrHeader[] = 'test: true';
}


if (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'export') ) {

    $arrHeader[] = 'apikey: 9b0d3b1c-27f2-4e40-8e4d-d735a50dbbee';

    $arrList = $_REQUEST['list'];
    $arrHead = $_REQUEST['header'];
    $strName = $_REQUEST['name'];

    $arrHead[0] = 'smartgroup';
    $arrHead[1] = 'box';

    $arrPost = array(
        'format' => 'xlsx',
        'header'  => $arrHead,
        'list'   => $arrList
    );

    $arrRes = curl_post('http://api.izs-institut.de/api/func/export/', $arrPost, array(), $arrHeader);
    $arrDownload = json_decode($arrRes, true);

    $strName = $strName;
    
    $strFileName = CMS_PATH .'files/temp/' .$strName;
    
    if (file_exists($strFileName)) {
        unlink($strFileName);
    }
    
    file_put_contents($strFileName, $arrRes);

}

?>