<?php

//CLASSES
require_once('classes/class.document.php');
require_once(APP_PATH .'cms/classes/class.stream.php');
require_once(APP_PATH .'cms/classes/class.additional.php');

require_once(APP_PATH .'cms/classes/class.ent.php');
require_once(APP_PATH .'cms/classes/class.escalation.php');

require_once(APP_PATH .'cms/classes/class.account.php');

$objEnt = new Ent;

//DATA

$objAccount = new Account;
$arrAccListRaw = $objAccount->arrGetAccountList();

$arrAccList = array();
if (count($arrAccListRaw) > 0) {
  foreach ($arrAccListRaw as $intKey => $arrAcc) {
    $arrAccList[$arrAcc['acid']] = $arrAcc['ac_name'];
  }
}

$arrEntRaw = $objEnt->arrGetElementList('', true);

$arrEnt = array();
if (count($arrEntRaw) > 0) {
    foreach ($arrEntRaw as $int => $arrEntleiher) {
        $arrEnt[$arrEntleiher['ent_id']] = $arrEntleiher;
    }
}

$arrRelationRaw = $objEnt->arrGetRelation();

$arrRelation = array();
if (count($arrRelationRaw) > 0) {
    foreach ($arrRelationRaw as $int => $arrRel) {
        if (isset($arrEnt[$arrRel['ent_id']])) {
            $arrRelation[$arrRel['ver_id']][$arrRel['ent_id']] = $arrEnt[$arrRel['ent_id']];
        }
    }
}

//print_r($arrRelation);
/*

[a08300000072KieAAE] => Array (
    [0014S000002GBlMQAW] => Array (
        [ent_id] => 0014S000002GBlMQAW
        [ent_name] => Nagel Inhouse Logistics Services GmbH
        [ent_info_when] => Erteilung;Entzug;Adressänderungen;Umfirmierungen
        [ent_info] => 
        [sl_db_aue] => true
        [sl_db_steuer] => true
        [sl_db_haftpflicht] => true
        [sl_db_register] => false
        [sl_db_tarif] => true
        [sl_db_zert] => true
    )
)

*/

$objDocument = new Document;
$arrDocumentListRaw = $objDocument->arrGetDocumentInformList();
$arrUpdateStatus    = $objDocument->arrGetUpdateStatus();
$arrNextStatus      = $objDocument->arrGetNextStatus();
$intMaxUpdStatus    = $objDocument->intGetMaxUpdateStatus();

$objDocument = new Document;
$objAdd      = new Additional;

$objEsc      = new Escalation;
$arrEscList  = $objEsc->getEscalationList();

//print_r($arrDocumentListRaw);
//echo count($arrDocumentListRaw) .'||';

/*
Array
(
    [ddid] => a0f3A000008yDAvQAM
    [dd_deleted] => false
    [dd_name] => Doku-0005230
    [dd_rtype] => 01230000001DMTkAAO
    [dd_grid] => a083A00000nEwjzQAC
    [dd_link] => https://izs-institut.box.com/s/narw9re8zjvvuvcxxopxypg7uqaajd78
    [dd_status] => aktuell
    [dd_dtype] => SCP:2011
    [dd_cert_center] => TÜV Rheinland
    [dd_verband] => 
    [dd_cert_company] => 001300000109nsyAAA
    [dd_date] => 2018-11-30
    [dd_begin] => 2018-11-30
    [dd_end] => 2021-11-13
    [dd_versicherer] => 
    [dd_company] => 
    [dd_street] => 
    [dd_zip] => 
    [dd_city] => 
    [dd_active] => true
    [dd_info] => 
    [dd_check] => 0000-00-00
    [dd_ustatus] => Anfrage zurückgestellt
    [dd_ivisible] => Neues Dokument angefordert.
    [dd_info_intern] => Nachricht Von Herr Thiel am 11.11.21
Die Zertifizierung findet kurzfristig statt.
Keine Erinnerung Senden
WDV 15.12.21: https://izs.smartgroups.io/frontend/main/topic/topic-5fd1b5d6-ac4d-4c7e-a8d6-bf6639a08c72
    [dd_received] => false
    [bd_status] => gelb
)
*/

$arrDocumentList = array(); //arrDocumentListRaw
if (count($arrDocumentListRaw) > 0) {
    foreach ($arrDocumentListRaw as $intKey => $arrDocument) {

        //print_r($arrDocument);

        if (isset($arrRelation[$arrDocument['dd_grid']])) {

          //print_r($arrRelation[$arrDocument['dd_grid']]); 

            foreach ($arrRelation[$arrDocument['dd_grid']] as $strVerId => $arrContract) {

              //print_r($arrContract); die();
              $arrDocument['contract'] = $arrContract;

                if (($arrContract['sl_db_aue'] == 'true') && ($arrDocument['dd_rtype'] == '01230000001DMTVAA4')) { //AÜ-Erlaubnis
                    $arrDocumentList[] = $arrDocument;
                    continue;
                }

                if (($arrContract['sl_db_steuer'] == 'true') && ($arrDocument['dd_rtype'] == '0123A000001e7ChQAI')) { //Bescheinigung Finanzamt
                    $arrDocumentList[] = $arrDocument;
                    continue;
                }

                if (($arrContract['sl_db_haftpflicht'] == 'true') && ($arrDocument['dd_rtype'] == '01230000001DMTaAAO')) { //Versicherungen
                    $arrDocumentList[] = $arrDocument;
                    continue;
                }

                if (($arrContract['sl_db_register'] == 'true') && ($arrDocument['dd_rtype'] == '01230000001DMTQAA4')) { //Registerauszüge
                    $arrDocumentList[] = $arrDocument;
                    continue;
                }

                if (($arrContract['sl_db_tarif'] == 'true') && ($arrDocument['dd_rtype'] == '01230000001DMTfAAO')) { //Mitgliedschaften
                    $arrDocumentList[] = $arrDocument;
                    continue;
                }

                if (($arrContract['sl_db_zert'] == 'true') && ($arrDocument['dd_rtype'] == '01230000001DMTkAAO')) { //Zertifizierungen
                    $arrDocumentList[] = $arrDocument;
                    continue;
                }

            }

            //print_r($arrRelation[$arrDocument['dd_grid']]);
        }
        //print_r();

    }
    //die();
}

//$arrDocumentList = $arrDocumentListRaw;
//echo count($arrDocumentList) .'||';
//print_r($arrDocumentList); die();
//die();

//REQUEST
if (($strFu == 'request') && (count($arrDocumentList) > 0)) {
  $arrFilteredList = array();
  foreach($arrDocumentList as $intKey => $arrDocument) {
      if (!in_array($arrDocument['dd_ustatus'], array('Anfrage zurückgestellt', 'Anfrage prüfen'))) {
        $arrFilteredList[] = $arrDocument;
      }
  }
  $arrDocumentList = $arrFilteredList;
}

if (($strFu == 'proove') && (count($arrDocumentList) > 0)) {
    $arrFilteredList = array();
    foreach($arrDocumentList as $intKey => $arrDocument) {
        if ($arrDocument['dd_ustatus'] == 'Anfrage prüfen') $arrFilteredList[] = $arrDocument;
    }
    $arrDocumentList = $arrFilteredList;
}

if (($strFu == 'back') && (count($arrDocumentList) > 0)) {
  $arrFilteredList = array();
  foreach($arrDocumentList as $intKey => $arrDocument) {
      if ($arrDocument['dd_ustatus'] == 'Anfrage zurückgestellt') $arrFilteredList[] = $arrDocument;
  }
  $arrDocumentList = $arrFilteredList;
}

if ($strFu == 'esc') {
    $arrFilteredList = array();
    $arrDocumentList = $arrFilteredList;
}

//SETTINGS

$arrNextStatus = $objDocument->arrGetNextStatus();
$arrNextStatusFlip = array_flip($arrNextStatus);

$arrUpdStaAll = $objDocument->arrGetUpdateStatus();

$arrErhalten = array(
    'false' => 'nein',
    'true'  => 'ja'
);

$boolShowHead = true;
$boolShowSide = false;

$strPageTitle = 'Basisdokumentation aktualisieren';


$arrTableHead = array(
  //'<input type="checkbox" id="selall">',
  'Esc',
  'Event-ID', 
  'erhalten',
  'CompanyGroup', 
  'Meldestelle',
  'Typ',
  'Ablauf',
  'Update-Status',
  'am',
  'Frist',
  'Entleiher',
  'Nächste Aktion',
  'Info intern',
  'Info extern',
  'Ampel-Status',
);

$arrGroupList = array();
$arrDTypeList = $objDocument->arrGetDTypeUpdate();

$objStream = new Stream('basic');

$arrDocList = array();
$arrDocListExport = array();
foreach ($arrDocumentList as $intRow => $arrDocument) {

  $arrStreamRaw = $objStream->arrGetStream($arrDocument['ddid']);
  if (count($arrStreamRaw) > 0) {
    $boolBreak = false;
    foreach ($arrStreamRaw as $intKey => $arrStream) {
      if ($arrStream['bc_type'] == 7) {
        $arrStreamContent = unserialize($arrStream['bc_new']);
        if ((isset($arrStreamContent['Entleiher'])) && ($arrStreamContent['Entleiher'] == $arrDocument['contract']['ent_id'])) {
          $boolBreak = true;
          break;
        } elseif (!isset($arrStreamContent['Entleiher'])) {
          $boolBreak = true;
          break;
        }
      }
    }
    if ($boolBreak) continue;
        
  }

  if ($_SESSION['id'] == 3) {
    //print_r($arrDocument);
  }

  $strClassAdd = '';
  
  //$arrReceived = $objAdd->arrGetElement('Dokument_Dokumentation__c', $arrDocument['ddid'], 'received');
  
  //if (count($arrReceived) > 0) {
  //  $arrDocument['dd_received'] = $arrReceived[0]['ad_value'];
  //}
  
  if (!in_array($arrDocument['dd_grid'], $arrGroupList)) {
    $objGroup = new Group;
    $arrGroup = $objGroup->arrGetGroup($arrDocument['dd_grid']);
    $strGroup = $arrGroup[0]['gr_name'];
    $arrGroupList[$arrDocument['dd_grid']] = $strGroup;
  } else {
    $strGroup = $arrGroupList[$arrDocument['dd_grid']];
  }
  
  $arrDocStatus = $objDocument->arrGetDocumentStatus($arrDocument['ddid']);
  
  $strAm = '';
  $strEscDate = '';
  $strDeadlineClass = '';
  $strAddClassCb = '';

  
  if ((count($arrDocStatus) > 0) && ($arrDocument['dd_ustatus'] != '')) {
    $strAm = strConvertDate($arrDocStatus[0]['bc_time'], 'Y-m-d H:i:s', 'd.m.Y');
    $strEscDate = strAddWorkingDays((strConvertDate($strAm, 'd.m.Y', 'U')), $objDocument->intGetEscalation($arrDocument['dd_ustatus']));
    if (strConvertDate($strEscDate, 'd.m.Y', 'U') <= strConvertDate(date('d.m.Y'), 'd.m.Y', 'U')) {
      $strDeadlineClass = 'red-600';
      $strAddClassCb = ' due';
    }
  }
  
  
  //$strStatus = $objDocument->strGetNewUpdateAction($arrDocument['dd_ustatus']);
  
  //erneut versenden
  if ($arrDocument['dd_ustatus'] == $arrUpdateStatus[5]) {
    $strStatus = $arrNextStatus[5];
    $strEscDate = '';
  }

  $strUpdateStatus = $arrDocument['dd_ustatus'];
  if (substr($strUpdateStatus, 1, 1) == '.') {
    $strUpdateStatus.= ' versandt';
  } 
  
  $strAddClass = '';

  $strNextAction = '<a title="Info an Entleiher senden" id="' .$arrDocument['ddid'] .'" class="bginfo">Info an Entleiher senden</a>';
  /*
  //$strCheckBox = '<input type="checkbox" id="dok_' .$arrDocument['ddid'] .'" class="docdoc' .$strAddClassCb .'">';
  if ($strStatus != '') {
    $strNextAction = '<span class="hidden">order_' .$arrNextStatusFlip[$strStatus] .'</span><a title="' .$strStatus .'" id="' .$arrDocument['ddid'] .'" class="bgaction">' .$strStatus .'</a>';
  } elseif ($arrDocument['dd_ustatus'] == $arrUpdateStatus[7]) {
    //$strAddClass = 'warning';
    //$strCheckBox = '';
  } elseif ($arrDocument['dd_ustatus'] == $arrUpdateStatus[8]) {
    //$strAddClass = 'info';
    //$strCheckBox = '';
  }
  */

  $strClassAdd.= ' pp_' .$arrDocument['dd_grid'];
  if (!isset($arrFilterSection['pp'][$arrDocument['dd_grid']])) {
    $arrFilterSection['pp'][$arrDocument['dd_grid']] = $strGroup;
  }

  $intSt = array_search($arrDocument['dd_ustatus'], $arrUpdStaAll);
  $strClassAdd.= ' up_' .$intSt;
  if (!isset($arrFilterSection['up'][$intSt]) && ($intSt !== false)) {
    if ($strUpdateStatus == '') $strUpdateStatusSelect = '- ohne -';
    else $strUpdateStatusSelect = $strUpdateStatus;
    $arrFilterSection['up'][$intSt] = $strUpdateStatusSelect;
  }

  $strTy = $arrDocument['dd_rtype'];
  $strClassAdd.= ' ty_' .$strTy;
  if (!isset($arrFilterSection['ty'][$strTy]) && ($strTy !== false)) {
    $arrFilterSection['ty'][$strTy] = $arrDTypeList[$arrDocument['dd_rtype']];
  }

  /*
  $intNs = array_search($strStatus, $arrNextStatus);
  $strClassAdd.= ' ns_' .$intNs;
  if (!isset($arrFilterSection['ns'][$intNs]) && ($intNs !== false)) {
    if ($strStatus == '') $strStatusSelect = '- ohne -';
    else $strStatusSelect = $strStatus;
    $arrFilterSection['ns'][$intNs] = $strStatusSelect;
  }
  */

  /*
  $strEr = $arrDocument['dd_received'];
  $strClassAdd.= ' er_' .$strEr;
  if (!isset($arrFilterSection['er'][$strEr]) && ($strEr !== false)) {
    $arrFilterSection['er'][$strEr] = $arrErhalten[$arrDocument['dd_received']];
  }
  */

  $strEsc = '';
  if (isset($arrEscList[$arrDocument['dd_grid']])) {
    $strEsc = '<a target="_blank" href="' .$arrEscList[$arrDocument['dd_grid']]['Eskalation_Link__c'] .'" title="' .$arrEscList[$arrDocument['dd_grid']]['Eskalation_Grund__c'] .'" alt="' .$arrEscList[$arrDocument['dd_grid']]['Eskalation_Grund__c'] .'" style="color:orangered;"><i class="fa fa-exclamation-triangle"></i></a>';
  }

    $strEntleiher = '';
    
    if ($arrDocument['contract']['ent_name'] != '') {
      $strEntleiher = $arrDocument['contract']['ent_name'];

      $strEnt = $arrDocument['contract']['ent_id'];
      $strClassAdd.= ' en_' .$strEnt;
      if (!isset($arrFilterSection['en'][$strEnt])) {
        $arrFilterSection['en'][$strEnt] = $strEntleiher;
      }
        
    }

    $arrReceived = $objAdd->arrGetElement('Dokument_Dokumentation__c', $arrDocument['ddid'], 'received');
  
    if (count($arrReceived) > 0) {
      $arrDocument['dd_received'] = $arrReceived[0]['ad_value'];
    }

    $strReceived = '';
    if ($arrDocument['dd_received'] == 'true') {
      $strReceived = ' checked="checked"';
    }

    $strEr = $arrDocument['dd_received'];
    $strClassAdd.= ' er_' .$strEr;
    if (!isset($arrFilterSection['er'][$strEr]) && ($strEr !== false)) {
      $arrFilterSection['er'][$strEr] = $arrErhalten[$arrDocument['dd_received']];
    }

    $strAccName = isset($arrAccList[$arrDocument['dd_acid']]) ? $arrAccList[$arrDocument['dd_acid']] : '';

    if ($strAccName != '') {
      $strClassAdd.= ' ac_' .$arrDocument['dd_acid'];
      if (!isset($arrFilterSection['ac'][$arrDocument['dd_acid']])) {
        $arrFilterSection['ac'][$arrDocument['dd_acid']] = $strAccName;
      }
    }    

    if ($arrDocument['dd_received'] == 'true') {
      $arrDocList[] = array(
        //$strCheckBox,
        $strEsc,
        'Event-ID' => '<a title="' .$arrDocument['dd_name'] .'" data-toggle="modal" data-target="#DetailModal" href="action/ajax.php?ac=bd_detail&ddid=' .$arrDocument['ddid'] .'"><i>' .$arrDocument['dd_name'] .'</i></a>'  .'</a><span class="hidden hfilter' .$strClassAdd .' ' .$strAddClass .'"></span>', // .'<span class="hidden ' .$strAddClass .'"></span>'
        'erhalten' => '<span class="hidden">order_1</span><input type="checkbox" id="dd_' .$arrDocument['ddid'] .'" value="1"' .$strReceived .' disabled="disabled">',
        'CompanyGroup' => '<i>' .$strGroup .'</i>', 
        'Meldestelle' => '<i>' .$strAccName .'</i>',
        'Typ' => '<i>' .$arrDTypeList[$arrDocument['dd_rtype']] .'</i>',
        'Ablauf' => '<span class="hidden">' .strConvertDate($arrDocument['dd_end'], 'Y-m-d', 'Ymd') .'</span><i>' .strConvertDate($arrDocument['dd_end'], 'Y-m-d', 'd.m.Y') .'</i>',
        'Update-Status' => '<span title="' .$strUpdateStatus .'" id="st_' .$arrDocument['ddid'] .'"><i>' .$strUpdateStatus .'</i></span>',
        'am' => '<span class="hidden" id="dah_' .$arrDocument['ddid'] .'">' .strConvertDate($strAm, 'd.m.Y', 'Ymd') .'</span><span id="da_' .$arrDocument['ddid'] .'"><i>' .$strAm .'</i></span>',
        'Frist' => '<span class="hidden" id="esh_' .$arrDocument['ddid'] .'">' .strConvertDate($strEscDate, 'd.m.Y', 'Ymd') .'</span><span class="' .$strDeadlineClass .'" id="es_' .$arrDocument['ddid'] .'"><i>' .$strEscDate .'</i></span>',
        'Entleiher' => '<i>' .$strEntleiher .'</i><span class="hidden inform">' .$strEnt .'</span>',
        'Nächste Aktion' => '<i>' .$strNextAction .'</i>',
        'Info intern' => '<i>' .str_replace(chr(10), '<br />', $arrDocument['dd_info_intern']) .'</i>',
        'Info extern' => '<span id="infoe_' .$arrDocument['ddid'] .'"><i>' .str_replace(chr(10), '<br />', $arrDocument['dd_ivisible']) .'</i></span>',
        'ampel' => '<i>' .$arrDocument['bd_status'] .'</i>',
      );
  } else {
    $arrDocList[] = array(
      //$strCheckBox,
      $strEsc,
      'Event-ID' => '<a title="' .$arrDocument['dd_name'] .'" data-toggle="modal" data-target="#DetailModal" href="action/ajax.php?ac=bd_detail&ddid=' .$arrDocument['ddid'] .'">' .$arrDocument['dd_name'] .'</a>'  .'</a><span class="hidden hfilter' .$strClassAdd .' ' .$strAddClass .'"></span>', // .'<span class="hidden ' .$strAddClass .'"></span>'
      'erhalten' => '<span class="hidden">order_1</span><i><input type="checkbox" id="dd_' .$arrDocument['ddid'] .'" value="1"' .$strReceived .' disabled="disabled"></i>',
      'CompanyGroup' => $strGroup, 
      'Meldestelle' => '' .$strAccName,
      'Typ' => $arrDTypeList[$arrDocument['dd_rtype']] .'',
      'Ablauf' => '<span class="hidden">' .strConvertDate($arrDocument['dd_end'], 'Y-m-d', 'Ymd') .'</span>' .strConvertDate($arrDocument['dd_end'], 'Y-m-d', 'd.m.Y') .'',
      'Update-Status' => '<span title="' .$strUpdateStatus .'" id="st_' .$arrDocument['ddid'] .'">' .$strUpdateStatus .'</span>',
      'am' => '<span class="hidden" id="dah_' .$arrDocument['ddid'] .'">' .strConvertDate($strAm, 'd.m.Y', 'Ymd') .'</span><span id="da_' .$arrDocument['ddid'] .'">' .$strAm .'</span>',
      'Frist' => '<span class="hidden" id="esh_' .$arrDocument['ddid'] .'">' .strConvertDate($strEscDate, 'd.m.Y', 'Ymd') .'</span><span class="' .$strDeadlineClass .'" id="es_' .$arrDocument['ddid'] .'">' .$strEscDate .'</span>',
      'Entleiher' => $strEntleiher .'<span class="hidden inform">' .$strEnt .'</span>',
      'Nächste Aktion' => $strNextAction,
      'Info intern' => str_replace(chr(10), '<br />', $arrDocument['dd_info_intern']),
      'Info extern' => '<span id="infoe_' .$arrDocument['ddid'] .'"><i>' .str_replace(chr(10), '<br />', $arrDocument['dd_ivisible']) .'</i></span>',
      'ampel' => $arrDocument['bd_status'],
    );
}
  
  //}
  
}

$arrOpt = array(
    'boolHasContext' => true,
    'arrNoSort' => array(0),
    'strOrder' => '"order": [[ 5, "desc" ]]'
);

$strDocTable = strCreateTable($arrDocList, $arrTableHead, '', false, true, array(), $arrOpt);


$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading col-lg-12">
        <h3 class="panel-title"><span id="DocCount">' .count($arrDocList) .'</span> Dokumente abgelaufen</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strDocTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

$strJsFootCodeRun.= '

$("#massFunction").on("click", function(e) {
  $("#modalMassFunctionSubmit").show();
  $("#modalMassFunction").modal("show");
});

$("#modalMassFunctionSubmit").on("click", function(e) {

  $(".docdoc:checked").each(function () { 
    var ddid = $(this).attr("id").split("dok_")[1];
    var data = { "ddid" : ddid };
    var tr   = $(this).parent().parent().hide();
    $.post( "action/ajax.php", { ac: "ddStatusClickMass", data: data, url: "action/ajax.php" } );
    $("#DocCount").text(parseInt($("#DocCount").text()) - 1)
  });

  $("#modalMassFunctionSubmit").hide();
  $(".msg").text("Die gewählten Aktionen werden ausgeführt.");

});

$("#modalMassFunction").on("show.bs.modal", function(e) {

  var noc = $(".docdoc:checked").length;
  var textMessage = "";

  if (noc == 0) {
    textMessage = "Bitte wähle ein Dokument aus.";
    $("#modalMassFunctionSubmit").hide();
  } else {
    if (noc == 1) textMessage = "Wills Du wirklich " + noc + " Aktion ausführen?";
    else textMessage = "Wills Du wirklich " + noc + " Aktionen ausführen?";
  }

  $(".msg").text(textMessage);

});
';

$strJsFootCodeRun.= "

$('#selall').click(function (event) {
  if (this.checked) {
      $('.docdoc.due').each(function () { //loop through each checkbox
          $(this).prop('checked', true); //check 
      });
  } else {
      $('.docdoc').each(function () { //loop through each checkbox
          $(this).prop('checked', false); //uncheck              
      });
  }
});

";


?>