<?php

$arrTableHead = array(
    '<input type="checkbox" id="selall">',
    'Letzte Aktion',
    'Erhalten',
    'Meldestelle',
    'Zeitarbeitsunternehmen',
    'Kündigung zum',
    'Registriert seit',
    'Vorname',
    'Nachname',
    'Email',
);



//Fetch Received Data
$strSql = 'SELECT `izs_sv_received`.*, UNIX_TIMESTAMP(`vr_date`) AS `vr_date` FROM `izs_sv_received` WHERE `vr_field` = "received" ';
if ($intReset > 0) {
  $strSql.= 'AND UNIX_TIMESTAMP(`vr_date`) > "' .$intReset .'" ';
}
$strSql.= 'AND `vr_date` = (SELECT MAX(`izs_sv_received2`.`vr_date`) FROM `izs_sv_received` `izs_sv_received2` WHERE ((`izs_sv_received`.`vr_contact_id` = `izs_sv_received2`.`vr_contact_id`) AND `izs_sv_received2`.`vr_field` = "received" ';
if ($intReset > 0) {
  $strSql.= 'AND (UNIX_TIMESTAMP(`izs_sv_received2`.`vr_date`) > "' .$intReset .'") ';
}
$strSql.= '))';
$arrReceivedRaw = MySQLStatic::Query($strSql);

$arrReceived = array();
if (count($arrReceivedRaw) > 0) {
  foreach ($arrReceivedRaw as $intCount => $arrDataReceived) {
    $arrReceived[$arrDataReceived['vr_contact_id']] = $arrDataReceived;
  }
}

//Fetch Sent Data
$strSql = 'SELECT `izs_sv_sent`.*, UNIX_TIMESTAMP(`vs_date`) AS `vs_date` FROM `izs_sv_sent` WHERE `vs_field` = "ct_id" ';
if ($intReset > 0) {
  $strSql.= 'AND UNIX_TIMESTAMP(`vs_date`) > "' .$intReset .'" ';
}
$strSql.= 'AND `vs_date` = (SELECT MAX(`izs_sv_sent2`.`vs_date`) FROM `izs_sv_sent` `izs_sv_sent2` WHERE ((`izs_sv_sent`.`vs_parent_id` = `izs_sv_sent2`.`vs_parent_id`) AND `izs_sv_sent2`.`vs_field` = "ct_id" ';
if ($intReset > 0) {
  $strSql.= 'AND (UNIX_TIMESTAMP(`izs_sv_sent2`.`vs_date`) > "' .$intReset .'") ';
}
$strSql.= '))';
$arrSent = MySQLStatic::Query($strSql);

//print_r($arrSent); die();

$arrDone = array();
if (count($arrSent) > 0) {
  foreach ($arrSent as $intCount => $arrMailSent) {
    $arrDone[$arrMailSent['vs_parent_id']] = $arrMailSent;
  }
}

//print_r($arrDone);

$strSql = 'SELECT "" AS `Status`, `Group__c`.`Id` AS `groId`, `Contact`.`Id` AS `conId`, `Account`.`Name` AS `AccountName`, ';
$strSql.= '`Group__c`.`Name` AS `GroupName`, `KuendigungZum__c`, `Contact`.`FirstName`, `Contact`.`LastName`, `Contact`.`Email`, `Registriert_bei_IZS_seit__c` ';
$strSql.= 'FROM `Group__c` ';
$strSql.= 'INNER JOIN `Contact` ON `Contact`.`GroupId` = `Group__c`.`Id`  ';
$strSql.= 'LEFT JOIN `Account` ON `Contact`.`AccountId` = `Account`.`Id`  ';
$strSql.= 'WHERE (`Group__c`.`Liefert_Daten__c` = "true") AND (`Contact`.`Aktiv__c` = "true") ';
$strSql.= 'AND (((FIND_IN_SET("Datenanlieferung", REPLACE(`Contact`.`Ansprechpartner_fuer__c`, ";", ",")) > 0)))';
$arrPpListRaw = MySQLStatic::Query($strSql);
$intResult = count($arrPpListRaw);

$arrTemplateList = array();
$strSelect = '';
$strSql = 'SELECT `ct_id`, `ct_name` FROM `cms_text` WHERE `ct_type` = 5 ORDER BY `ct_order`';
$arrAvailTemplateList = MySQLStatic::Query($strSql);
$intTemplateSum = count($arrAvailTemplateList);
if ($intTemplateSum > 0) {
  $strSelect.= '<form action="" method="post" id="tempSelect">';
  $strSelect.= '<input type="hidden" name="receiver" id="receiver" value="" />' .chr(10);
  $strSelect.= '<label for="ct_id" style="margin-right: 20px;">Vorlage</label><select name="ct_id" id="ct_id">' .chr(10);
  $strSelect.= '<option value="">Bitte auswählen</option>' .chr(10);
  foreach ($arrAvailTemplateList as $intCount => $arrTemplate) {

    $arrTemplateList[$arrTemplate['ct_id']] = $arrTemplate['ct_name'];

    $strSelected = '';
    if ($arrTemplate['ct_id'] == @$_REQUEST['ct_id']) {
      $strSelected = ' selected="selected"';  
    }

    $strSelect.= '<option value="' .$arrTemplate['ct_id'] .'"' .$strSelected .'>' .$arrTemplate['ct_name'] .'</option>' .chr(10);
  }  
  $strSelect.= '</select>&nbsp;<input type="button" id="versenden" value="versenden" class="btn btn-success" style="padding-top: 0px; padding-bottom: 0px;" /></form>' .chr(10);
}

$arrList = array();
foreach ($arrPpListRaw as $intRow => $arrGroup) {

  //echo $arrDone[$arrGroup['conId']]['vs_ct_id']; die();

  $strClassAdd = '';
  $strAddClass = '';

  if ($arrGroup['Registriert_bei_IZS_seit__c'] == '0000-00-00') {
    $arrGroup['Registriert_bei_IZS_seit__c'] == '';
  } else {
    $arrGroup['Registriert_bei_IZS_seit__c'] = date('d.m.Y', strtotime($arrGroup['Registriert_bei_IZS_seit__c']));
  }

  $arrGroup['KuendigungZum__c'] = '';
  $strSql = 'SELECT DATE_FORMAT(`Kuendigung_wirksam_ab__c`, "%d.%m.%Y") AS `Kuendigung_wirksam_ab__c` FROM `izs_vertrag` WHERE `Id` = "' .$arrGroup['groId'] .'" AND `Kuendigung_wirksam_ab__c` != "0000-00-00" AND `Aktiv__c` = "true"';
  $arrVertrag = MySQLStatic::Query($strSql);
  if (count($arrVertrag) > 0) {
    $arrGroup['KuendigungZum__c'] = $arrVertrag[0]['Kuendigung_wirksam_ab__c'];
  }

  $strCa = ($arrGroup['KuendigungZum__c'] != '') ? 'Ja' : 'Nein';
  $strClassAdd.= ' ca_' .$strCa;
  if (!isset($arrFilterSection['ca'][$strCa]) && ($strCa !== false)) {
    $arrFilterSection['ca'][$strCa] = $strCa;
  }

  $strClassAdd.= ' pp_' .$arrGroup['groId'];
  if (!isset($arrFilterSection['pp'][$arrGroup['groId']])) {
    $arrFilterSection['pp'][$arrGroup['groId']] = $arrGroup['GroupName'];
  }

  $strAction = '';
  if (isset($arrDone[$arrGroup['conId']])) {
    $arrData = $arrDone[$arrGroup['conId']];
    $strAction = '"' .$arrTemplateList[$arrDone[$arrGroup['conId']]['vs_value']] .'" gesendet';
    $strLaValue = $strAction;
    $strAction.= ' <i class="fa fa-info-circle" aria-hidden="true" data-content="von ' .$arrUserList[$arrData['vs_cl_id']]['cl_first'] .' ' .$arrUserList[$arrData['vs_cl_id']]['cl_last'] .'<br/>am: ' .date('d.m.Y H:i:s', $arrData['vs_date']) .'" data-trigger="hover" data-toggle="popover" data-html="true"></i>';
    $strLaKey = $arrDone[$arrGroup['conId']]['vs_value'];
  } else {
    $strLaKey = 0;
    $strLaValue = 'Keine';
  }

  $strClassAdd.= ' la_' .$strLaKey;
  if (!isset($arrFilterSection['la'][$strLaKey])) {
    $arrFilterSection['la'][$strLaKey] = $strLaValue;
  }

  $strChecked = '';
  $strLaKey   = 'Nein';
  $strLaValue = 'Nein';
  if (isset($arrReceived[$arrGroup['conId']])) {
    if ($arrReceived[$arrGroup['conId']]['vr_value'] == 'true') {
      $strChecked = 'checked';
      $strLaKey   = 'Ja';
      $strLaValue = 'Ja';
    }
  }

  $strClassAdd.= ' re_' .$strLaKey;
  if (!isset($arrFilterSection['re'][$strLaKey])) {
    $arrFilterSection['re'][$strLaKey] = $strLaValue;
  }

    $arrList[] = array(
        'Selected' => '<input type="checkbox" id="con_' .$arrGroup['conId'] .'" class="concon">',
        'Action' => '' .$strAction .'<span class="hidden hfilter' .$strClassAdd .' ' .$strAddClass .'"></span>',
        'Erhalten' => '<input type="checkbox" id="rec_' .$arrGroup['conId'] .'" class="received" ' .$strChecked .'>',
        'Account' => $arrGroup['AccountName'],
        'Firmengruppe' => $arrGroup['GroupName'],
        'Kündigung zum' => '<span class="hidden">' .strtotime($arrGroup['KuendigungZum__c']) .'</span>' .$arrGroup['KuendigungZum__c'],
        'Registriert' => '<span class="hidden">' .strtotime($arrGroup['Registriert_bei_IZS_seit__c']) .'</span>' .$arrGroup['Registriert_bei_IZS_seit__c'],
        'Vorname' => $arrGroup['FirstName'],
        'Nachname' => $arrGroup['LastName'],
        'Email' => $arrGroup['Email'],
      //'from' => '<span class="hidden">' .$strFromSort .'</span>' .$strFrom,
    );

}

$arrOpt = array(
  'boolHasContext' => true,
  'arrNoSort' => array(0),
  'strOrder' => '"order": [[ 4, "asc" ], [ 3, "asc" ], [ 8, "asc" ]]'
);

$strTable = strCreateTable($arrList, $arrTableHead, '', false, true, array(), $arrOpt);

$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Ansprechpartner gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strSelect;

$strIncOutput.= $strTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

$strJsFootCodeRun.= '

$("span.warning").parent().parent().addClass("warning");
$("span.danger").parent().parent().addClass("danger");

$("#seldate").on("change", function (e) {
  this.form.submit();
});

';


$strJsFootCodeRun.= "

$('#versenden').click(function (event) {

  var countChecked = $('.concon:checked').length;

  if ($('#ct_id')[0].selectedIndex <= 0) {
    alert('Bitte wähle eine Vorlage aus.');
    return false;
  } else if (countChecked == 0) {
    alert('Bitte wähle mindestens einen Empfänger aus.');
    return false;
  } else {
    
    $('#receiver').val('');

    var rcnt = 0;

    $('.concon:checked').each(function () { //loop through each checkbox
      var id = $(this).prop('id');
      var parts = id.split('_');
      $('#receiver').val($('#receiver').val() + parts[1] + ';');
      rcnt++;
    });

    $.post('/cms/inc/svdaten.ajax.php', {
      receiver: $('#receiver').val(), ct_id: $('#ct_id').val(), ac: ''
    }).done(function() {
      toastr.options = { 'closeButton': true, 'positionClass': 'toast-top-full-width', 'newestOnTop': true,   'showMethod': 'fadeIn', 'hideMethod': 'fadeOut' }
      toastr.success(rcnt + ' E-Mails versendet.');
    });


    //$('#tempSelect').submit();

  }

});


$('#selall').click(function (event) {

  if (this.checked) {
      $('.concon:visible').each(function () { //loop through each checkbox
          $(this).prop('checked', true); //check 
      });
  } else {
      $('.concon:visible').each(function () { //loop through each checkbox
          $(this).prop('checked', false); //uncheck              
      });
  }

});

$('.received').click(function (event) {

  var id = $(this).prop('id');
  var parts = id.split('_');

  $.post('inc/svdaten.ajax.php', { 
    ac: 'received', contact_id: parts[1], value: $(this).prop('checked')
  });

});

";


?>