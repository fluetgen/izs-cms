<?php

//CLASSES
require_once('classes/class.bgevent.php');
require_once('classes/class.premiumpayer.php');


//REQUEST
$strBg = @$_REQUEST['acid'];
$strFu = @$_REQUEST['fu'];

//DATA

$objBg = new Bg;
$arrBgList = $objBg->arrGetBgList();

$objBgEvent = new BgEvent;

$objPp = new PremiumPayer;

//SETTINGS

$boolShowHead = false;
$boolShowSide = false;

$strPageTitle = 'Berufsgenossenschaften';

//FUNCTIONS

if (isset($strBg) && ($strBg != '')) {

  $arrBg = $objBg->arrGetBg($strBg);

  if ($strFu == 'export_meldestellen') {
    
    $arrAc = $objBg->arrGetMeldestellen($strBg, '', true);
    
    //Print_R($arrAc);
    
    $strKdNrAdd = '';
    if(strpos($arrBg[0]['ac_name'], "VBG") !== false) {
      $strKdNrAdd = ' VBG';
    }
    
    if (count($arrAc) > 0) {
      
      $arrAcList = array();
      $arrNumberList = array();
      foreach ($arrAc as $intKey => $arrMeldestelle) {
        $arrAcList[] = $arrMeldestelle['acid_meldestelle'];
        $arrNumberList[$arrMeldestelle['acid_meldestelle']] = $arrMeldestelle['ac_bg_number'];
      }
      
      //print_r($arrAcList);
      
      $arrPpList = $objPp->arrGetPPFromIdList($arrAcList);
      
      if (count($arrPpList) > 0) {

        $arrTable = array();
        $arrTable[] = array(
          'Kundennummer' .$strKdNrAdd, 
          'Name des Unternehmens',
          'Straße',
          'Postleitzahl',
          'Ort'
        );
        
        foreach ($arrPpList as $intCount => $arrPp) {
          $arrTable[] = array(
            $arrNumberList[$arrPp['acid']], 
            $arrPp['ac_name'],
            $arrPp['ac_billing_street'],
            $arrPp['ac_billing_zip'],
            $arrPp['ac_billing_city']
          );
        }

        array_to_csv_download($arrTable, 'Meldestellen - ' .$arrBg[0]['ac_name'] .'.csv');
        
      }
      
    }

    
  }

  if ($strFu == 'export_anfrage') {
    
    $arrRequestList = $objBgEvent->arrGetOpenEventList(date('Y') - 1, $strBg);
    
    //print_r($arrRequestList);
    
    if (count($arrRequestList) > 0) {

      $arrTable = array();
      $arrTable[] = array(
        'Jahr', 
        'Status', 
        'Meldestelle',
        'Mitgliedsnummer',
        'Berufsgenossenschaft'
      );
      
      foreach ($arrRequestList as $intCount => $arrEvList) {
        $arrTable[] = array(
          $arrEvList['be_jahr'],
          $arrEvList['be_status_bearbeitung'],
          $arrEvList['ac_name'],
          $arrEvList['Mitgliedsnummer_BG__c'],
          $arrBg[0]['ac_name'],
        );
      }
      
    }
    
    array_to_csv_download($arrTable, 'Anfrageliste - ' .$arrBg[0]['ac_name'] .'.csv');
    
    /*
    
    $arrAc = $objBg->arrGetMeldestellen($strBg);
    
    
    //Print_R($arrAc);
    
    $arrBg = $objBg->arrGetBg($strBg);
    
    $strKdNrAdd = '';
    if(strpos($arrBg[0]['ac_name'], "VBG") !== false) {
      $strKdNrAdd = ' VBG';
    }
    
    if (count($arrAc) > 0) {
      
      $arrAcList = array();
      $arrNumberList = array();
      foreach ($arrAc as $intKey => $arrMeldestelle) {
        $arrAcList[] = $arrMeldestelle['acid_meldestelle'];
        $arrNumberList[$arrMeldestelle['acid_meldestelle']] = $arrMeldestelle['ac_bg_number'];
      }
      
      //print_r($arrAcList);
      
      $arrPpList = $objPp->arrGetPPFromIdList($arrAcList);
      
      if (count($arrPpList) > 0) {

        $arrTable = array();
        $arrTable[] = array(
          'Kundennummer' .$strKdNrAdd, 
          'Name des Unternehmens',
          'Straße',
          'Postleitzahl',
          'Ort'
        );
        
        foreach ($arrPpList as $intCount => $arrPp) {
          $arrTable[] = array(
            $arrNumberList[$arrPp['acid']], 
            $arrPp['ac_name'],
            $arrPp['ac_billing_street'],
            $arrPp['ac_billing_zip'],
            $arrPp['ac_billing_city']
          );
        }

        array_to_csv_download($arrTable, 'Meldestellen - ' .$arrBg[0]['ac_name'] .'.csv');
        
      }
      
    }
    
    */

    
  }

}

//CONTENT

$strOutput = '
  <!-- Page -->
  <div class="animsition container-fluid">
';

$strOutput.= strCreatePageTitle($strPageTitle);

$arrTableHead = array(
  'Name',
  'Meldestellen',
  'Anfragelisten'
);

$arrList = array();
foreach ($arrBgList as $intRow => $arrDocument) {
  
  $arrList[] = array(
    'Berufsgenossenschaft' => '<a href="/cms/index_neu.php?ac=contacts&acid=' .$arrDocument['acid'] .'" target="_blank">' .$arrDocument['ac_name'] .'</a>',
    'Meldestellen' => '<a href="?ac=bg_uebersicht&fu=export_meldestellen&acid=' .$arrDocument['acid'] .'">' .'export CSV' .'</a>',
    'Anfragelisten' => '<a href="?ac=bg_uebersicht&fu=export_anfrage&acid=' .$arrDocument['acid'] .'">' .'export CSV' .'</a>'
  );
}

$strBgTable = strCreateTable($arrList, $arrTableHead, '', false, true);

$strOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Berufsgenossenschaften gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strOutput.= $strBgTable;

$strOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';


$strOutput.= '
  </div>
  <!-- End Page -->
';


?>