<?php

//CLASSES

$arrTraffic = array(
    'green'  => '<span class="hidden">3</span><span class="label bg-color-green"><i class="fa fa-check-circle-o"></i></span>',
    'yellow' => '<span class="hidden">2</span><span class="label bg-color-orange"><i class="fa fa-dot-circle-o"></i></span>',
    'red'    => '<span class="hidden">1</span><span class="label bg-color-red"><i class="fa fa-ban"></i>',
    'gray'   => '<span class="hidden">0</span><span class="label bg-color-lighten"><i class="fa fa-circle-o"></i></span>'
);

//DATA

$strUrl = 'https://www.izs-institut.de/api/negativ/?to=' .date('Y-m-d') .'&active=1';
$strJson = file_get_contents($strUrl);
$arrRequest = json_decode($strJson, true);
$arrNvmList = $arrRequest['content']['negativ'];

$arrVerRequest = array();
if (count($arrNvmList) > 0) {
    foreach ($arrNvmList as $intKey => $arrNegativRaw) {
        if (!in_array($arrNegativRaw['verid'], $arrVerRequest)) {
            $arrVerRequest[] = $arrNegativRaw['verid'];
        }
    }
}

$arrPostData = http_build_query(array('id' => $arrVerRequest));
$arrOpt = array(
    'http' => array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/x-www-form-urlencoded',
        'content' => $arrPostData
    )
);
$resContext = stream_context_create($arrOpt);
$strJson    = file_get_contents('https://www.izs-institut.de/api/ver/', false, $resContext);
$arrRequest = json_decode($strJson, true);
$arrVerListRaw = $arrRequest['content']['company'];


$boolShowHead = true;
$boolShowSide = false;

$strPageTitle = 'Aktuelle Negativmerkmale';


$arrTableHead = array(
  'Gesamt',
  'Name', 
  'Doku', 
  'SV',
  'BG',
  'Hinweis öffentliches Portal',
  'Hinweis Entleiherportal'
);

$arrVerList = array();
$arrDocListExport = array();
foreach ($arrVerListRaw as $intRow => $arrVer) {

  if ($_SESSION['id'] == 3) {
    //print_r($arrDocument);
  }

  $strClassAdd = '';

    
    $arrVerList[] = array(
        $arrTraffic[$arrVer['status_all']],
        $arrVer['name'],
        $arrTraffic[$arrVer['status_doc']],
        $arrTraffic[$arrVer['status_sv']],
        $arrTraffic[$arrVer['status_bg']],
        $arrVer['note_public'],
        $arrVer['note_lender'],
    );
}  

$arrOpt = array(
    'boolHasContext' => true,
    //'arrNoSort' => array(0),
    'strOrder' => '"order": [[ 1, "asc" ]]'
);

$strDocTable = strCreateTable($arrVerList, $arrTableHead, '', false, true, array(), $arrOpt);


$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading col-lg-12">
        <h3 class="panel-title"><span id="DocCount">' .count($arrVerListRaw) .'</span> Negativmerkmale gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strDocTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

$strJsFootCodeRun.= '

$("#massFunction").on("click", function(e) {
  $("#modalMassFunctionSubmit").show();
  $("#modalMassFunction").modal("show");
});

$("#modalMassFunctionSubmit").on("click", function(e) {

  $(".docdoc:checked").each(function () { 
    var ddid = $(this).attr("id").split("dok_")[1];
    var data = { "ddid" : ddid };
    var tr   = $(this).parent().parent().hide();
    $.post( "action/ajax.php", { ac: "ddStatusClickMass", data: data, url: "action/ajax.php" } );
    $("#DocCount").text(parseInt($("#DocCount").text()) - 1)
  });

  $("#modalMassFunctionSubmit").hide();
  $(".msg").text("Die gewählten Aktionen werden ausgeführt.");

});

$("#modalMassFunction").on("show.bs.modal", function(e) {

  var noc = $(".docdoc:checked").length;
  var textMessage = "";

  if (noc == 0) {
    textMessage = "Bitte wähle ein Dokument aus.";
    $("#modalMassFunctionSubmit").hide();
  } else {
    if (noc == 1) textMessage = "Wills Du wirklich " + noc + " Aktion ausführen?";
    else textMessage = "Wills Du wirklich " + noc + " Aktionen ausführen?";
  }

  $(".msg").text(textMessage);

});
';

$strJsFootCodeRun.= "

$('#selall').click(function (event) {
  if (this.checked) {
      $('.docdoc.due').each(function () { //loop through each checkbox
          $(this).prop('checked', true); //check 
      });
  } else {
      $('.docdoc').each(function () { //loop through each checkbox
          $(this).prop('checked', false); //uncheck              
      });
  }
});

";


?>