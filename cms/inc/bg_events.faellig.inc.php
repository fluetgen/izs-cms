<?php

require_once(APP_PATH .'cms/classes/class.escalation.php');

$objEsc      = new Escalation;
$arrEscList  = $objEsc->getEscalationList();

$arrHasEsc = array();
$strSql = 'SELECT * FROM `ks_accgrp`';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrDataset) {
    if (isset($arrEscList[$arrDataset['GId']])) {
      $arrHasEsc[$arrDataset['AId']] = $arrEscList[$arrDataset['GId']];
    }
  }
}

$arrFilter = array();


$arrFilter[] = array(
  'strKey' => 'be_ergebnis',
  'strType' => 'LIKE',
  'strValue' => 'OK%'
);

$arrFilter[] = array(
  'strKey' => 'be_status_bearbeitung',
  'strType' => '!=',
  'strValue' => 'zurückgestellt von IZS'
);

$arrFilter[] = array(
  'boolQuote' => false,
  'strKey' => 'DATE_SUB(`be_befristet`, INTERVAL 14 DAY)',
  'strType' => '<=',
  'strValue' => 'CURDATE()'
);

$objBg = new Bg;

//DATE_SUB(`be_befristet`, INTERVAL 14 DAY) = CURDATE()

/*
Alle Events mit Status = OK + Due Date </= Heute + die KEIN "Folge-Event" haben
Alle Events mit Status = anzufragen, angefragt, erhalten - Klärungsfall + Due Date = leer
*/

$arrEventList = $objBgEvent->arrGetBgEventList($arrFilter);

//print_r($arrEventList); die();

$arrEventListNoFollow = array();
if (count($arrEventList) > 0) {
  
  foreach ($arrEventList as $intKey => $arrEvent) {

      //if ($arrEvent['beid'] == 5185) { print_r($arrEvent); }
  
      $arrFi[0] = array('strKey' => 'be_bg', 'strType' => '=', 'strValue' => $arrEvent['be_bg']);
      $arrFi[1] = array('strKey' => 'be_meldestelle', 'strType' => '=', 'strValue' => $arrEvent['be_meldestelle']);
      $arrFi[2] = array('strKey' => 'be_befristet', 'strType' => '>', 'strValue' => $arrEvent['be_befristet']);
      
      $arrFollowingEv = $objBgEvent->arrGetBgEventList($arrFi);

      //if ($arrEvent['beid'] == 5185) { echo 'Follow1:'; print_r($arrFollowingEv); }
      
      if (count($arrFollowingEv) > 0) {
        ; //$intBefristet = 2;
      } else {

        if (($objBg->boolConIsActive($arrEvent['be_meldestelle'], $arrEvent['be_bg'], $arrEvent['be_befristet'])) && 1) {

          $arrFi[0] = array('strKey' => 'be_bg', 'strType' => '=', 'strValue' => $arrEvent['be_bg']);
          $arrFi[1] = array('strKey' => 'be_meldestelle', 'strType' => '=', 'strValue' => $arrEvent['be_meldestelle']);
          $arrFi[2] = array('strKey' => 'be_befristet', 'strType' => '=', 'strValue' => '0000-00-00');
          //$arrFi[3] = array('strKey' => 'be_jahr', 'strType' => '>=', 'strValue' => $arrEvent['be_jahr']);
          
          $arrFollowingEv2 = $objBgEvent->arrGetBgEventList($arrFi);

          //if ($arrEvent['beid'] == 5185) { echo 'Follow2:'; print_r($arrFollowingEv2); }
          
          if (count($arrFollowingEv2) == 0) {
            $arrEventListNoFollow[] = $arrEvent;
          }
        
        }
        /*
        $intBefristet = 1;
        $strClassAdd.= ' due';
        */
      } 
      
  }

}

$arrEventList = $arrEventListNoFollow;

//print_r($arrFollowingEv2); die();

if (count($arrEventList) > 0) {

  foreach ($arrEventList as $intKey => $arrEvent) {

    $strSql = 'SELECT `Suchergebnisse_sv_ab__c` FROM `Group__c` INNER JOIN `Account` ON `Group__c`.`Id` = `Account`.`SF42_Company_Group__c` ';
    $strSql.= 'WHERE `Suchergebnisse_sv_ab__c` != "0000-00-00" AND `Account`.`Id` = "' .MySQLStatic::esc($arrEvent['be_meldestelle']) .'" ';
    $arrSql = MySQLStatic::Query($strSql);

    if (count($arrSql) > 0) {
      if (strtotime($arrEvent['be_befristet']) < strtotime($arrSql[0]['Suchergebnisse_sv_ab__c'])) {
        unset($arrEventList[$intKey]);
      }
    }

  }

  $arrEventList = array_values($arrEventList);

}


if ($_SESSION['id'] == 3) {
  //print_r($arrEventList);
}


$arrFilter = array();

$arrFilter[] = array(
  'boolQuote' => false,
  'strKey' => 'be_status_bearbeitung',
  'strType' => 'IN',
  'strValue' => '("anzufragen", "angefragt", "erhalten - Klärungsfall")'
);

$arrFilter[] = array(
  'strKey' => 'be_befristet',
  'strType' => '=',
  'strValue' => '0000-00-00'
);


$arrEventListOpen = $objBgEvent->arrGetBgEventList($arrFilter);


$arrEventList = array_merge($arrEventList, $arrEventListOpen);


$arrBgNameList = array();
if (count($arrBgListEvent) > 0) {
  foreach ($arrBgListEvent as $intKey => $arrBg) {
    $arrBgNameList[$arrBg['acid']] = $arrBg['ac_name'];
  }
}

$arrMeldestellen = $objBg->arrGetMeldestellen();

$arrMeldestellenNameList = array();
if (count($arrMeldestellen) > 0) {
  foreach ($arrMeldestellen as $intKey => $arrMeldestelle) {
    $arrMeldestellenNameList[$arrMeldestelle['acid']] = $arrMeldestelle['ac_name'];
  }
}


$arrListNvm = [];
$strSql = 'SELECT * FROM `Negativmerkmal__c` WHERE `Auskunftsgeber_Typ__c` = "01230000001DKGtAAO" AND `Aktiv__c` = "true"';
$arrSql = MySQLStatic::Query($strSql);

if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrNvm) {
    $arrListNvm[$arrNvm['AccountId']] = $arrNvm;
  }
}
$arrNegativAll = [];
$strSql = 'SELECT * FROM `ks_dropdown` WHERE `kd_kf_id` = 851 AND `kd_criteria` = "01230000001DKGtAAO" ORDER BY `kd_value`';
$arrSql = MySQLStatic::Query($strSql);

if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrNvmAll) {
    $arrNegativAll[] = $arrNvmAll['kd_value'];
  }
}

$arrTableHead = array(
  'Esc',
  'ID',
  'Chat',
  'Berufsgenossenschaft',
  'Meldestelle',
  'Status Bearbeitung',
  'Ergebnis',
  'Befristet bis',
  
  //'Dokument',
  //'Sichtbarkeit des Dokuments',
  //'Sperrvermerk',

  'Sundung / Ratenzahlung',
  'Meldepflicht erfüllt',
  'Negativmerkmal CRM',

  'Bemerkung sichtbar',
  'Interne Statusinfo',
  'Status Klärung'
);

$arrStatusBearbeitung = $objBgEvent->arrGetSelection('be_status_bearbeitung');
$arrErgebnis = $objBgEvent->arrGetSelection('be_ergebnis');
$arrStatusKlaerung = $objBgEvent->arrGetSelection('be_klaerung_status');
$arrStundung = $objBgEvent->arrGetSelection('be_stundung');
$arrMeldepflicht = $objBgEvent->arrGetSelection('be_meldepflicht');

$arrBefristet = array(
  'noch nicht fällig',
  'fällig',
  'ohne Befristung'
);

$arrList = array();
foreach ($arrEventList as $intRow => $arrEvent) {

  $strEsc = '';
  if (isset($arrHasEsc[$arrEvent['be_meldestelle']])) {
    $strEsc = '<a target="_blank" href="' .$arrHasEsc[$arrEvent['be_meldestelle']]['Eskalation_Link__c'] .'" title="' .$arrHasEsc[$arrEvent['be_meldestelle']]['Eskalation_Grund__c'] .'" alt="' .$arrHasEsc[$arrEvent['be_meldestelle']]['Eskalation_Grund__c'] .'" style="color:orangered;"><i class="fa fa-exclamation-triangle"></i></a>';
  }
  
  $strBesfistet = '';
  if ($arrEvent['be_befristet'] != '0000-00-00') {
    $strBesfistet = '<span class="hidden">' .strConvertDate($arrEvent['be_befristet'], 'Y-m-d', 'Ymd') .'</span>' .strConvertDate($arrEvent['be_befristet'], 'Y-m-d', 'd.m.Y');
  }
  
  $strClassAdd = '';
  
  $strClassAdd.= ' sp_' .$arrEvent['be_sperrvermerk'];
  $strSperrvermerk = 'nein';
  if ($arrEvent['be_sperrvermerk'] != 0) {
    $strSperrvermerk = 'ja';
  }
  /*
  if (!isset($arrFilterSection['sp'][$strSperrvermerk])) {
    $arrFilterSection['sp'][$arrEvent['be_sperrvermerk']] = $strSperrvermerk;
  }
  */
  
  $strMeldestelle = @$arrMeldestellenNameList[$arrEvent['be_meldestelle']];
  if ($strMeldestelle == '') {
    $arrMeldestelle = $objPp->arrGetPP($arrEvent['be_meldestelle'], false);
    $strMeldestelle = $arrMeldestelle[0]['ac_name'];
  }
  
  $strDokumentArt = '';
  if (($arrEvent['be_dokument_art'] == '') || ($arrEvent['be_dokument_art'] == 'Unbedenklichkeitsbescheinigung (UBB)')) {
    $strDokumentArt = 'UBB';
  }
  
  $strClassAdd.= ' y_' .$arrEvent['be_jahr'];
  if (!isset($arrFilterSection['y'][$arrEvent['be_jahr']])) {
    $arrFilterSection['y'][$arrEvent['be_jahr']] = $arrEvent['be_jahr'];
  }

  $strClassAdd.= ' bg_' .$arrEvent['be_bg'];
  if (!isset($arrFilterSection['bg'][$arrEvent['be_bg']])) {
    $arrFilterSection['bg'][$arrEvent['be_bg']] = $arrBGList[$arrEvent['be_bg']];
  }
  
  $strClassAdd.= ' pp_' .$arrEvent['be_meldestelle'];
  if (!isset($arrFilterSection['pp'][$arrEvent['be_meldestelle']])) {
    $arrFilterSection['pp'][$arrEvent['be_meldestelle']] = $strMeldestelle;
  }
  
  $intSb = array_search($arrEvent['be_status_bearbeitung'], $arrStatusBearbeitung);
  $strClassAdd.= ' sb_' .$intSb;
  if (!isset($arrFilterSection['sb'][$intSb]) && ($intSb !== false)) {
    $arrFilterSection['sb'][$intSb] = $arrEvent['be_status_bearbeitung'];
  }
  
  $intEr = array_search($arrEvent['be_ergebnis'], $arrErgebnis);
  $strClassAdd.= ' er_' .$intEr;
  if (!isset($arrFilterSection['er'][$intEr]) && ($intEr !== false)) {
    $arrFilterSection['er'][$intEr] = $arrEvent['be_ergebnis'];
  }
  
  $intBefristet = 2;
  if ($arrEvent['be_befristet'] != '0000-00-00') { 
    //if (strtotime($arrEvent['be_befristet']) < strtotime('+ 14 days')) {
    if (strtotime($arrEvent['be_befristet']) <= strtotime('now')) {
      
      /*
      $arrFi[0] = array('strKey' => 'be_bg', 'strType' => '=', 'strValue' => $arrEvent['be_bg']);
      $arrFi[1] = array('strKey' => 'be_meldestelle', 'strType' => '=', 'strValue' => $arrEvent['be_meldestelle']);
      $arrFi[2] = array('strKey' => 'be_befristet', 'strType' => '>', 'strValue' => $arrEvent['be_befristet']);
      
      $arrFollowingEv = $objBgEvent->arrGetBgEventList($arrFi);
      
      if (count($arrFollowingEv) > 0) {
        $intBefristet = 2;
      } else {
        $intBefristet = 1;
        $strClassAdd.= ' due';
      }
      */

      $intBefristet = 1;
      $strClassAdd.= ' due';
      
    } else {
      $intBefristet = 0;
    }
  }
  $strClassAdd.= ' bb_' .$intBefristet;
  if (!isset($arrFilterSection['bb'][$intBefristet])) {
    $arrFilterSection['bb'][$intBefristet] = $arrBefristet[$intBefristet];
  }
  
  $intSichtbar = 0;
  if ($arrEvent['be_sichtbar'] == 'sichtbar') { 
    $intSichtbar = 1;
  }
  /*
  $strClassAdd.= ' si_' .$intSichtbar;
  if (!isset($arrFilterSection['si'][$intSichtbar])) {
    $arrFilterSection['si'][$intSichtbar] = $arrEvent['be_sichtbar'];
  }
  
  $intSk = array_search($arrEvent['be_klaerung_status'], $arrStatusKlaerung);
  $strClassAdd.= ' sk_' .$intSk;
  if (!isset($arrFilterSection['sk'][$intSk]) && ($intSk !== false)) {
    $arrFilterSection['sk'][$intSk] = $arrEvent['be_klaerung_status'];
  }
  */


  $intNs = array_search($arrEvent['be_stundung'], $arrStundung);
  $strClassAdd.= ' rs_' .$intNs;
  if (!isset($arrFilterSection['rs'][$intNs]) && ($intNs !== false)) {
    $arrFilterSection['rs'][$intNs] = $arrEvent['be_stundung'];
  }

  $intSk = array_search($arrEvent['be_meldepflicht'], $arrMeldepflicht);
  $strClassAdd.= ' mp_' .$intSk;
  if (!isset($arrFilterSection['mp'][$intSk]) && ($intSk !== false)) {
    $arrFilterSection['mp'][$intSk] = $arrEvent['be_meldepflicht'];
  }

  $strNegativ = '';
  if (isset($arrListNvm[$arrEvent['be_meldestelle']])) {
    $strNegativ = $arrListNvm[$arrEvent['be_meldestelle']]['Art_Negativmerkmal__c'];

    $intNs = array_search($arrListNvm[$arrEvent['be_meldestelle']]['Art_Negativmerkmal__c'], $arrNegativAll);
    $strClassAdd.= ' nm_' .$intNs;

    $arrFilterSection['nm'][$intNs] = $arrListNvm[$arrEvent['be_meldestelle']]['Art_Negativmerkmal__c'];
  }
   

  $strChatId = $objBgEvent->getSgId($arrEvent['beid']);
  
  $arrList[$arrEvent['beid']] = array(
    '' => $strEsc,
    'ID' => '<a href="/cms/index_neu.php?ac=bg_detail&beid=' .$arrEvent['beid'] .'" target="_blank">' .$arrEvent['be_name'] .'</a><span class="hidden hfilter' .$strClassAdd .'"></span>',
    'chat' => '<a href="javascript:void(0);" onclick="openSG(\'' .$strChatId .'\');"><img src="https://izs.smartgroups.io/img/logo/logo_32.png" height="20"></a>',
    'Berufsgenossenschaft' => '<a href="/cms/index_neu.php?ac=contacts&acid=' .$arrEvent['be_bg'] .'" target="_blank">' .$arrBGList[$arrEvent['be_bg']] .'</a>',
    'Meldestelle' => $strMeldestelle .' ',
    'Status' => $arrEvent['be_status_bearbeitung'],
    'Ergebnis' => $arrEvent['be_ergebnis'],
    'Befristet bis' => $strBesfistet,
    
    //'Dokument' => '<a href="pdf/bg/' .$arrEvent['be_link'] .'" target="_blank">' .$arrEvent['be_link'] .'</a>',
    //'Sichtbarkeit des Dokuments' => $arrEvent['be_sichtbar'],
    //'Sperrvermerk' => $strSperrvermerk,

    'Stundung' => $arrEvent['be_stundung'],
    'Meldepflicht' => $arrEvent['be_meldepflicht'],
    'negativ' => $strNegativ,

    'Bemerkung sichtbar' => $arrEvent['be_bemerkung'],
    'Interne Statusinfo' => $arrEvent['be_info_intern'],
    'Status Klärung' => $arrEvent['be_klaerung_status']
  );

}

//print_r($arrList);
$arrOpt = array(
  'boolHasContext' => true
);

$strBgTable = strCreateTable($arrList, $arrTableHead, '', false, true, array(), $arrOpt);

$strIncOutput.= ' 

<script>
function openSG(topicId) {
  if (topicId != "") {
    setTimeout(()=>smartchat.cmd("topic.show", {entity: {id: topicId}}), 150);
  }
}
</script>

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Events für Berufsgenossenschaften gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strBgTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';


?>