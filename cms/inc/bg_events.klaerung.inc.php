<?php


$arrFilter = array();

$arrFilter[] = array(
  'strKey' => 'be_klaerung_status',
  'strType' => 'IN',
  'strValue' => '("in Klärung", "geschlossen - positiv", "geschlossen - negativ")',
  'boolQuote' => false
);


$arrEventList = $objBgEvent->arrGetBgEventList($arrFilter);

$arrBgNameList = array();
if (count($arrBgListEvent) > 0) {
  foreach ($arrBgListEvent as $intKey => $arrBg) {
    $arrBgNameList[$arrBg['acid']] = $arrBg['ac_name'];
  }
}

$arrMeldestellen = $objBg->arrGetAllMeldestellen();

$arrMeldestellenNameList = array();
if (count($arrMeldestellen) > 0) {
  foreach ($arrMeldestellen as $intKey => $arrMeldestelle) {
    $arrMeldestellenNameList[$arrMeldestelle['Id']] = $arrMeldestelle['Name'];
  }
}

$arrMeldestelleGruppe = [];
$strSql = 'SELECT `Id`, `SF42_Company_Group__c` AS `Group` FROM `Account`';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrDataset) {
    $arrMeldestelleGruppe[$arrDataset['Id']] = $arrDataset;
  }
}

$arrEscalation = [];
$strSql = 'SELECT * FROM `Group__c` WHERE `Eskalation__c` = "ja"';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrDataset) {
    $arrEscalation[$arrDataset['Id']] = $arrDataset;
  }
}

$arrTableHead = array(
  'Esc',
  '<input type="checkbox" value="0" name="check_all_r">',
  'ID',
  'Chat',
  'Berufsgenossenschaft',
  'Meldestelle',
  'Unternehmensnummer',
  'Grund für Klärung',
  'Status Klärung',
  'Wiedervorlage',
  'Nächster Schritt',
  'Interne Statusinfo',
  'Ansprechpartner Klärung',
  'Telefon',
  'Berarbeiter:in'
);


$arrStatusBearbeitung = $objBgEvent->arrGetSelection('be_status_bearbeitung');
$arrErgebnis = $objBgEvent->arrGetSelection('be_ergebnis');
$arrStatusKlaerung = $objBgEvent->arrGetSelection('be_klaerung_status');
$arrGrundKlaerung = $objBgEvent->arrGetSelection('be_klaerung_grund');
$arrNaechsterSchritt = $objBgEvent->arrGetSelection('be_klaerung_schritt');
$arrBearbeiter = $objBgEvent->arrGetSelection('be_bearbeiter');
$arrStundung = $objBgEvent->arrGetSelection('be_stundung');
$arrMeldepflicht = $objBgEvent->arrGetSelection('be_meldepflicht');

$arrBefristet = array(
  'noch nicht fällig',
  'fällig',
  'ohne Befristung'
);

$arrWieder = array(
  'noch nicht fällig',
  'heute',
  'überfällig',
  'ohne Datum',
);

$arrRueckmeldung = array(
  'nicht erhalten',
  'erhalten'
);

$arrList = array();
$intCount = 0;
foreach ($arrEventList as $intRow => $arrEvent) {
  
  $strBesfistet = '';
  if ($arrEvent['be_befristet'] != '0000-00-00') {
    $strBesfistet = strConvertDate($arrEvent['be_befristet'], 'Y-m-d', 'd.m.Y');
  }

  $strRueckmeldung = '';
  if ($arrEvent['be_datum_rueckmeldung'] != '0000-00-00') {
    $strRueckmeldung = strConvertDate($arrEvent['be_datum_rueckmeldung'], 'Y-m-d', 'd.m.Y');
  }
  
  $strSperrvermerk = 'nein';
  if ($arrEvent['be_sperrvermerk'] != 0) {
    $strSperrvermerk = 'ja';
  }
  
  $strDokumentArt = '';
  if (($arrEvent['be_dokument_art'] == '') || ($arrEvent['be_dokument_art'] == 'Unbedenklichkeitsbescheinigung (UBB)')){
    $strDokumentArt = 'UBB';
  }
  
  $strClassAdd = '';


  $strKey = $arrEvent['be_bearbeiter'];
  $strClassAdd.= ' us_' .$strKey;
  if ((!isset($arrFilterSection['us'][$strKey])) && ($strKey != '')) {
    $arrFilterSection['us'][$strKey] = $arrBearbeiter[$arrEvent['be_bearbeiter']];
  }

  $strClassAdd.= ' y_' .$arrEvent['be_jahr'];
  if (!isset($arrFilterSection['y'][$arrEvent['be_jahr']])) {
    $arrFilterSection['y'][$arrEvent['be_jahr']] = $arrEvent['be_jahr'];
  }

  $strClassAdd.= ' bg_' .$arrEvent['be_bg'];
  if (!isset($arrFilterSection['bg'][$arrEvent['be_bg']])) {
    $arrFilterSection['bg'][$arrEvent['be_bg']] = $arrBGList[$arrEvent['be_bg']];
  }
  
  $strMeldestelle = @$arrMeldestellenNameList[$arrEvent['be_meldestelle']];
  if ($strMeldestelle == '') {
    $arrMeldestelle = $objPp->arrGetPP($arrEvent['be_meldestelle'], false);
    $strMeldestelle = $arrMeldestelle[0]['ac_name'];
  }
  
  $strClassAdd.= ' pp_' .$arrEvent['be_meldestelle'];
  if (!isset($arrFilterSection['pp'][$arrEvent['be_meldestelle']])) {
    $arrFilterSection['pp'][$arrEvent['be_meldestelle']] = $strMeldestelle;
  }
  
  $intWieder = 3;
  if ($arrEvent['be_wiedervorlage'] != '0000-00-00') { 
    if (strtotime($arrEvent['be_wiedervorlage']) < strtotime(date('Y-m-d'))) {
      $intWieder = 2;
      $strClassAdd.= ' due';
    } elseif (strtotime($arrEvent['be_wiedervorlage']) == strtotime(date('Y-m-d'))) {
      $intWieder = 1;
    } else {
      $intWieder = 0;
    }
  }
  $strClassAdd.= ' wi_' .$intWieder;
  if (!isset($arrFilterSection['wi'][$intWieder])) {
    $arrFilterSection['wi'][$intWieder] = $arrWieder[$intWieder];
  }
   
  $intGk = array_search($arrEvent['be_klaerung_grund'], $arrGrundKlaerung);
  $strClassAdd.= ' gk_' .$intGk;
  if (!isset($arrFilterSection['gk'][$intGk]) && ($intGk !== false)) {
    $arrFilterSection['gk'][$intGk] = $arrEvent['be_klaerung_grund'];
  }
  
  $intRueckmeldung = 0;
  if ($arrEvent['be_datum_rueckmeldung'] != '0000-00-00') { 
    $intRueckmeldung = 1;
  }
  $strClassAdd.= ' ru_' .$intRueckmeldung;
  if (!isset($arrFilterSection['ru'][$intRueckmeldung])) {
    //$arrFilterSection['ru'][$intRueckmeldung] = $arrRueckmeldung[$intRueckmeldung];
  }


  $strWieder = '';
  if ($arrEvent['be_wiedervorlage'] != '0000-00-00') {
    $strWieder = '<span class="hidden">' .strConvertDate($arrEvent['be_wiedervorlage'], 'Y-m-d', 'Ymd') .'</span>' .strConvertDate($arrEvent['be_wiedervorlage'], 'Y-m-d', 'd.m.Y');
  }
  
  //print_r($arrNaechsterSchritt); die();

  $intNs = array_search($arrEvent['be_klaerung_schritt'], $arrNaechsterSchritt);
  $strClassAdd.= ' ns_' .$intNs;
  if (!isset($arrFilterSection['ns'][$intNs]) && ($intNs !== false)) {
    $arrFilterSection['ns'][$intNs] = $arrEvent['be_klaerung_schritt'];
  }

  //print_r($arrStundung); die();

  $intNs = array_search($arrEvent['be_stundung'], $arrStundung);
  $strClassAdd.= ' rs_' .$intNs;
  if (!isset($arrFilterSection['rs'][$intNs]) && ($intNs !== false)) {
    $arrFilterSection['rs'][$intNs] = $arrEvent['be_stundung'];
  }

  $strGroupId = $arrMeldestelleGruppe[$arrEvent['be_meldestelle']]['Group'];

  if (isset($arrEscalation[$strGroupId])) { // 'a083000000D4hUMAAZ' https://izs.smartgroups.io/frontend/main/topic/topic-97f48d16-eb79-48c7-b450-6085ab217700

    //$arrEscalation[$strGroupId] = $arrEscalation['a083000000D4hUMAAZ'];

    preg_match('/topic-[\w-]*/', $arrEscalation[$strGroupId]['Eskalation_Link__c'], $arrLink);
    $strSgLink = $arrLink[0] ?? '';

    $strKey = 'ja';
    $strEsc = '<a href="javascript:void(0)" alt="' .$arrEscalation[$strGroupId]['Eskalation_Grund__c'] .'" title="' .$arrEscalation[$strGroupId]['Eskalation_Grund__c'] .'" onclick="openSG(\'' .$strSgLink .'\')" style="color:red;"><i class="icon fa fa-warning" aria-hidden="true"></i></a>';
  } else {
    $strKey = 'nein';
    $strEsc = '';
  }
  $strClassAdd.= ' es_' .$strKey;
  if (!isset($arrFilterSection['es'][$strKey])) {
    $arrFilterSection['es'][$strKey] = 'es_' .$strKey;
  }
   
  $intSk = array_search($arrEvent['be_klaerung_status'], $arrStatusKlaerung);
  $strClassAdd.= ' sk_' .$intSk;
  if (!isset($arrFilterSection['sk'][$intSk]) && ($intSk !== false)) {
    $arrFilterSection['sk'][$intSk] = $arrEvent['be_klaerung_status'];
  }

  if ($arrEvent['be_klaerung_status'] == 'in Klärung') {
    $intCount++;
  }

  $intSk = array_search($arrEvent['be_meldepflicht'], $arrMeldepflicht);
  $strClassAdd.= ' mp_' .$intSk;
  if (!isset($arrFilterSection['mp'][$intSk]) && ($intSk !== false)) {
    $arrFilterSection['mp'][$intSk] = $arrEvent['be_meldepflicht'];
  }

  $strChatId = $objBgEvent->getSgId($arrEvent['beid']);
  
  $arrList[] = array(
    'esc' => $strEsc,
    'box' => '<input type="checkbox" name="klae[' .$arrEvent['beid'] .']" value="1" class="klae" rel="' .$arrEvent['beid'] .'" id="check_' .$arrEvent['beid'] .'">',
    'ID' => '<a href="/cms/index_neu.php?ac=bg_detail&beid=' .$arrEvent['beid'] .'" target="_blank">' .$arrEvent['be_name'] .'</a><span class="hidden hfilter' .$strClassAdd .'">',
    'chat' => '<a href="javascript:void(0);" onclick="openSG(\'' .$strChatId .'\');"><img src="https://izs.smartgroups.io/img/logo/logo_32.png" height="20"></a>',
    'Berufsgenossenschaft' => '<a href="/cms/index_neu.php?ac=contacts&acid=' .$arrEvent['be_bg'] .'" target="_blank">' .$arrBGList[$arrEvent['be_bg']] .'</a>',
    'Meldestelle' => $strMeldestelle,
    'CompanyNo' => $arrEvent['be_unternehmensnummer'],
    'Grund' => $arrEvent['be_klaerung_grund'],
    'StatusK' => $arrEvent['be_klaerung_status'],
    'Wiedervorlage' => $strWieder,
    'Nächster Schritt' => $arrEvent['be_klaerung_schritt'],
    'Interne Statusinfo' => wordwrap($arrEvent['be_info_intern'], 60, "<br />\n" ),
    'Ansprechpartner Klärung' => $arrEvent['be_klaerung_ap'],
    'Telefon' => $arrEvent['be_klaerung_telefon'],
    'Bearbeiter'  => $arrBearbeiter[$arrEvent['be_bearbeiter']] ?? ''
  );

}

$arrOpt = array(
  'arrSort' => array(
    'null', '"bSortable": false', 'null', '"bSortable": false', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null'
  ),
  'strOrder' => '"order": [[ 2, "asc" ]]'
);

/*  */

$strBgTable = strCreateTable($arrList, $arrTableHead, '', false, true, [], $arrOpt);

$strIncOutput.= ' 

    <script>
    function openSG(topicId) {
      if (topicId != "") {
        setTimeout(()=>smartchat.cmd("topic.show", {entity: {id: topicId}}), 150);
      }
    }

    function handle() {
      var strValues = "";
      $(".klae").each (function() {
        if($(this).is(":checked")) {
          strValues+= "|" + $(this).attr(\'rel\');
        }
      });
      strValues = strValues.substr(1);
      return strValues;
    }

    </script>

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .$intCount .'</span> Events für Berufsgenossenschaften gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strBgTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->


    <!-- Modal -->
    <div class="modal fade" id="MassKlaerung" aria-hidden="true" aria-labelledby="MassKlaerung"
    role="dialog" tabindex="-1">
      <div class="modal-dialog modal-center">
        <input type="hidden" name="beid_k" id="beid_k" value="">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">x</span>
            </button>
            <h4 class="modal-title">Massenfunktion Klärungsfall</h4>
          </div>
          <div class="modal-body">
            <p>&nbsp;</p>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>
    <!-- End Modal -->


    <!-- Modal -->
    <div class="modal fade modal-danger" id="ModalDanger" aria-hidden="true"
    aria-labelledby="ModalDanger" role="dialog" tabindex="-1">
      <div class="modal-dialog modal-center smallw">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title">Achtung</h4>
          </div>
          <div class="modal-body">
            <p id="errMessage">Text</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>
    <!-- End Modal -->
    
';


$strJsFootCodeRun.= '


$(document).on("click", "#svKlaerungMass", function(){
  
  var checked = $("input[name^=\"klae\"]:checked");

  var beidMass = new Array();

  $.each(checked, function () {
    beidMass.push($(this).prop("id").split("check_")[1]);
  });

  var arrFields = {
    be_datum_rueckmeldung: $("#dr_all").val(),
    be_klaerung_status:    $("#sk_all").val(),
    be_klaerung_grund:     $("#gr_all").val(),
    be_klaerung_schritt:   $("#ns_all").val(),
    be_rueckstand:         $("#rr_all").val(),
    be_klaerung_ap:        $("#ap_all").val(),
    be_klaerung_telefon:   $("#at_all").val(),
    be_info_intern:        $("#ii_all").val(),
    be_wiedervorlage:      $("#wi_all").val(),
    be_stundung:           $("#st_all").val(),
    be_meldepflicht:       $("#me_all").val(),
    be_bearbeiter:         $("#us_all").val(),
  };

  $.ajax({
    type: "POST",
    url:  "action/ajax.php",
    dataType: "text", 
    data: { ac: "svKlaerungClickMass", beidMass: beidMass, values: arrFields },
    success: function(result) {
      var jsonData = $.parseJSON(result);
      
      if (jsonData["Status"] == "OK") {

        var arrFields = jsonData["fields"];
        
        $("#MassKlaerung").modal("hide");

        var tr = $("input[name^=\"klae\"]:checked").parent().parent();

        if (arrFields["be_klaerung_status"] != "Alle Einträge löschen") { 

          if (arrFields["be_klaerung_grund"] != "") {
            if (arrFields["be_klaerung_grund"] != "Alle Einträge löschen") tr.find("td:nth-child(8)").html(arrFields["be_klaerung_grund"]);
            else tr.find("td:nth-child(8)").html("");
          }
          if (arrFields["be_klaerung_status"] != "") {
            if (arrFields["be_klaerung_status"] != "Alle Einträge löschen") {
              tr.find("td:nth-child(9)").html(arrFields["be_klaerung_status"]);
              tr.find("td:nth-child(3) span").removeClass("sk_0").removeClass("sk_1").removeClass("sk_2");
              if (arrFields["be_klaerung_status"] == "in Klärung") { tr.find("td:nth-child(3) span").addClass("sk_0"); }
              if (arrFields["be_klaerung_status"] == "geschlossen - negativ") { tr.find("td:nth-child(3) span").addClass("sk_2"); }
              if (arrFields["be_klaerung_status"] == "geschlossen - positiv") { tr.find("td:nth-child(3) span").addClass("sk_1"); }
            }
            else {
              tr.find("td:nth-child(9)").html("");
              tr.find("td:nth-child(3) span").removeClass("sk_0").removeClass("sk_1").removeClass("sk_2");
            }
          }
          if (arrFields["be_wiedervorlage"] != "") {
            if (arrFields["be_wiedervorlage"] != "00.00.0000") tr.find("td:nth-child(10)").html(arrFields["be_wiedervorlage"]);
            else tr.find("td:nth-child(10)").html("");
          }
          if (arrFields["be_klaerung_schritt"] != "") {
            if (arrFields["be_klaerung_schritt"] != "Alle Einträge löschen") tr.find("td:nth-child(11)").html(arrFields["be_klaerung_schritt"]);
            else tr.find("td:nth-child(11)").html("");
          }
          if (arrFields["be_info_intern"] != "") {
            if (arrFields["be_klaerung_schritt"] != "[LÖSCHEN]") tr.find("td:nth-child(12)").html(arrFields["be_info_intern"]);
            else tr.find("td:nth-child(12)").html("");
          }
          if (arrFields["be_klaerung_ap"] != "") {
            if (arrFields["be_klaerung_ap"] != "[LÖSCHEN]") tr.find("td:nth-child(13)").html(arrFields["be_klaerung_ap"]);
            else tr.find("td:nth-child(13)").html("");
          }
          if (arrFields["be_klaerung_telefon"] != "") {
            if (arrFields["be_klaerung_telefon"] != "[LÖSCHEN]") tr.find("td:nth-child(14)").html(arrFields["be_klaerung_telefon"]);
            else tr.find("td:nth-child(14)").html("");
          }
          if (arrFields["be_bearbeiter"] != "") {
            tr.find("td:nth-child(15)").html(arrFields["be_bearbeiter"]);
          } else {
            tr.find("td:nth-child(15)").html("");
          }

          if (arrFields["be_klaerung_status"] != $("#filter_sk option:selected").text()) {
            if (arrFields["be_klaerung_status"] != "") {
              tr.hide();
              $("#DocCount").text($("#DocCount").text() - $("input[name^=\"klae\"]:checked").length); //
            }
          }

        } else {
          tr.hide();
          $("#DocCount").text($("#DocCount").text() - $("input[name^=\"klae\"]:checked").length); //
        }
        
        //$("input[name^=\"klae\"]:checked").parent().parent().hide();
        $("input[name^=\"klae\"]:checked").prop("checked", false);
            
      }
      
      if (jsonData["Status"] == "FAIL") {

        $("#ModalDanger").modal("show");
        $("#ModalDanger #errMessage").html(jsonData["Reason"]);

      }

    }

  });

});

    $(document).on("click", "#massKlae", function() {

      var checked = $("input[name^=\"klae\"]:checked");

      if (checked.length > 0) {

        $("#MassKlaerung").modal("show");

      } else {

        $("#ModalDanger").modal("show");
        $("#ModalDanger #errMessage").html("Bitte wähle mindestens ein Event aus.");

      }

    });


    // Fill modal with content from link href
    $("#MassKlaerung").on("shown.bs.modal", function(e) {

        $(this).find(".modal-content").load("action/modal.php?ac=massKlae", function () {

          $(".modal .dp").datepicker();

        });
        
    });

    $("input[name=\'check_all_r\']").click(function() {
      $("input[name^=\"klae\"]").prop("checked", $(this).prop("checked"));
    });


    $("#filter_sk").val("sk_0").change();
    $(".hfilter.sk_1").parent().parent().hide();
    $(".hfilter.sk_2").parent().parent().hide();
    $(".hfilter.sk_").parent().parent().hide();


';

?>