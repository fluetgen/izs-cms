<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

session_start();

require_once('../const.inc.php');

require_once('../../assets/classes/class.mysql.php');
require_once('../../assets/classes/class.database.php');

require_once('refactor.inc.php');

$intUser = $_SESSION['id'];

$arrHeader = array(
  'user: ' .$intUser,
  'Accept: application/json'
);


if (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'create') ) {

    $strSqlGr = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$_REQUEST['gr'] .'"';
    $arrSqlGr = MySQLStatic::Query($strSqlGr);

    $arrData = array(
        'name' => $arrSqlGr[0]['Name'],
        'year' => $_REQUEST['year'],
        'street' => $arrSqlGr[0]['Strasse__c'],
        'zip' => $arrSqlGr[0]['PLZ__c'],
        'city' => $arrSqlGr[0]['Ort__c'],
        'bnr' => $arrSqlGr[0]['Betriebsnummer__c'],
    );

    if (isset($_REQUEST['id']) && ($_REQUEST['id'] != 0)) {
        $strSql = 'UPDATE `izs_jahresbericht` SET `pr_data` = "' .addslashes(serialize($arrData)) .'", `pr_cl_id` = "' .$_REQUEST['clid'] .'", `pr_time` = NOW() WHERE `pr_id` = "' .$_REQUEST['id'] .'"';
        $arrSql = MySQLStatic::Update($strSql);
        $intId = $_REQUEST['id'];
    } else {
        $strSql = 'INSERT INTO `izs_jahresbericht` (`pr_id`, `pr_group_id`, `pr_data`, `pr_year`, `pr_cl_id`, `pr_time`) VALUES (NULL, "' .$_REQUEST['gr'] .'", "' .addslashes(serialize($arrData)) .'", "' .$_REQUEST['year'] .'",  "' .$_REQUEST['clid'] .'",  NOW())';
        $intSql = MySQLStatic::Insert($strSql);
        $intId = $intSql;
    }

    $strName = str_replace('/', '-', $arrSqlGr[0]['Name']);
    $strName = str_replace('&', '-', $strName);
    $strUrl = 'https://www.izs.de/cms/jahresbericht/' .$_REQUEST['gr'] .'/' .$strName .'/' .$_REQUEST['year'] .'.pdf';

    $strLink = '<a href="' .$strUrl .'" target="_blank">' .$strUrl .'</a>';

    $strDelete = '<a class="dCert" id="dc_' .$intId .'" style="cursor: pointer;">löschen</a>';

    echo $_REQUEST['gr'] .'|' .date('d.m.Y H:i:s') .'|' .$strLink .'|' .$strDelete;


} elseif (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'delete')) {

    $strSql = 'SELECT * FROM `izs_jahresbericht` WHERE `pr_id` = "' .$_REQUEST['id'] .'"';
    $arrSql = MySQLStatic::Query($strSql);

    $strSqlD = 'DELETE FROM `izs_jahresbericht` WHERE `pr_id` = "' .$_REQUEST['id'] .'"';
    $arrSqlD = MySQLStatic::Query($strSqlD);

    echo $arrSql[0]['pr_group_id'] .'|||';

}


?>