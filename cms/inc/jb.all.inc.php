<?php

$arrContractList = [];
$strSql = "SELECT * FROM `Vertrag__c`";
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
    foreach ($arrSql as $intKey => $arrContract) {
        if ($arrContract['Aktiv__c'] == 'true') {
          $arrContractList[$arrContract['Company_Group__c']] = 1;
        } elseif (!isset($arrContractList[$arrContract['Company_Group__c']])) {
          $arrContractList[$arrContract['Company_Group__c']] = 0;
        }
    }
}

if ($_COOKIE['id'] == 3) {
  //print_r($arrContractList);
}

$intYear = $_REQUEST['selyear'] ?? '2024';

$arrCertList = [];
$strSql = 'SELECT `pr_id`, `pr_group_id` FROM `izs_jahresbericht` WHERE `pr_year` = "' .$intYear .'" ORDER BY `pr_group_id`';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
    foreach ($arrSql as $intKey => $arrCount) {
        $arrCertList[$arrCount['pr_group_id']] = $arrCount['pr_id'];
    }
}

$arrCertDateList = [];
$strSql = 'SELECT `pr_id`, `pr_group_id`, `pr_time` FROM `izs_jahresbericht` WHERE `pr_year` = "' .$intYear .'" ORDER BY `pr_group_id`';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
    foreach ($arrSql as $intKey => $arrMax) {
        $arrCertDateList[$arrMax['pr_group_id']] = strtotime($arrMax['pr_time']);
    }
}


$arrTableHead = array(
  'Premium PDL',
  'Aktueller Bericht',
  'Liefert Daten',
  'Aktiver Vertrag',
  'Registriert seit',
  'Perma-Link zum Jahresbericht',
  'Erzeugen',
  'Löschen'
);

$strSql = 'SELECT * FROM `Group__c` WHERE `SF42_Group_Type__c` = "Lender" ORDER BY `Name`';
$arrPpListRaw = MySQLStatic::Query($strSql);
$intResult = count($arrPpListRaw);


$strSelect = '';

$arrYear = array(2023, 2024);

if (date('Y') > 2024) {
  for ($i = 1; $i <= (date('Y') - 2024); $i++) $arrYear[] = 2024 + $i;
}

if (!isset($_REQUEST['selyear'])) $_REQUEST['selyear'] = $arrYear[0];

if (count($arrYear) > 0) {
  $strSelect.= '<form action="/cms/index_neu.php?ac=jb&fu=all" method="post"><label for="selyear" style="margin-right: 20px;">Jahr</label><select name="selyear" id="selyear">' .chr(10);
  foreach ($arrYear as $intYearSel) {

    $strSelected = '';
    if ($intYearSel == @$intYear) {
      $strSelected = ' selected="selected"';
      $strYear = (string) $intYearSel;
    }
    
    $strSelect.= '<option value="' .$intYearSel .'"' .$strSelected .'>' .$intYearSel .'</option>' .chr(10);
  }  
  $strSelect.= '</select></form>' .chr(10);
}

$arrModelList = [];
$strSql = 'SELECT `Id`, `Mitgliedsmodell` FROM `izs_mitgliedsmodell` WHERE `Aktiv__c` = "true"';
$arrSql = MySQLStatic::Query($strSql);

if (count($arrSql) > 0) {
  foreach ($arrSql as $intkKey => $arrModel) {
    $arrModelList[$arrModel['Id']] = $arrModel['Mitgliedsmodell'];
  }
}

$arrList = array();
foreach ($arrPpListRaw as $intRow => $arrGroup) {

    if (!isset($arrContractList[$arrGroup['Id']])) continue; 

    if (isset($arrModelList[$arrGroup['Id']]) && (in_array($arrModelList[$arrGroup['Id']], ['Sponsored', 'Sponsored PDL', 'Sponsored ENT', 'Light']))) {
      continue;
    }

    $strClassAdd = '';

    //Filter Zeitarbeitsfirma
    $strClassAdd.= ' pp_' .$arrGroup['Id'];
    if (!isset($arrFilterSection['pp'][$arrGroup['Id']])) {
        $arrFilterSection['pp'][$arrGroup['Id']] = $arrGroup['Name'];
    }

    //Filter Liefert Daten
    if ($arrGroup['Liefert_Daten__c'] == 'true') { 
        $strPr = 'Ja';
    } else {
        $strPr = 'Nein';
    }
    $strClassAdd.= ' ld_' .$strPr;

    if (!isset($arrFilterSection['ld'][$strPr]) && ($strPr !== false)) {
        $arrFilterSection['ld'][$strPr] = $strPr;
    }

    //Filter Aktiver Vertrag
    if ($arrContractList[$arrGroup['Id']] == '1') { 
        $strPr = 'Ja';
    } else {
        $strPr = 'Nein';
    }
    $strClassAdd.= ' ac_' .$strPr;
    
    if (!isset($arrFilterSection['ac'][$strPr]) && ($strPr !== false)) {
        $arrFilterSection['ac'][$strPr] = $strPr;
    }

    if (isset($arrCertList[$arrGroup['Id']])) {

        $intCertId = $arrCertList[$arrGroup['Id']];

        $strName = str_replace('/', '-', $arrGroup['Name']);
        $strName = str_replace('&', '-', $strName);
        $strUrl = 'https://www.izs.de/cms/jahresbericht/' .$arrGroup['Id'] .'/' .$strName .'/' .$strYear .'.pdf';

        $strAct = '<span class="hidden">' .$arrCertDateList[$arrGroup['Id']] .'</span>' .'' .date('d.m.Y H:i:s', $arrCertDateList[$arrGroup['Id']]) .'';

        $strLinkCert = '<a href="' .$strUrl .'" target="_blank">' .$strUrl .'</a>';

        $strDelete = '<a class="dCert" id="dc_' .$intCertId .'" style="cursor: pointer;">löschen</a>';

    } else {

        $strAct = '<span class="hidden">0</span>';
        $strLinkCert = '';

        $strDelete = '';

        $intCertId = 0;

    }

    if (isset($arrModelList[$arrGroup['Id']]) && (in_array($arrModelList[$arrGroup['Id']], ['Sponsored', 'Light']))) {
      $strCreate = 'nicht möglich';
    } else {
      $strCreate = '<a class="cCert" id="cc_' .$intCertId .'" style="cursor: pointer;" rel="' .$arrGroup['Id'] .'">erzeugen</a>';
    }
    
    $arrList[] = array(
      'name' => $arrGroup['Name'] .'<span class="gr_' .$arrGroup['Id'] .' hidden hfilter ' .$strClassAdd .'"></span>',
      'act' => $strAct .'',
      'delivery'  => ($arrGroup['Liefert_Daten__c'] == 'true') ? 'Ja' : 'Nein',
      'active' => ($arrContractList[$arrGroup['Id']] == 1) ? 'Ja' : 'Nein',
      //'model' => $arrModelList[$arrGroup['Id']] ?? '',
      'regsince' => '<span class="hidden">' .strtotime($arrGroup['Registriert_bei_IZS_seit__c']) .'</span>' .(($arrGroup['Registriert_bei_IZS_seit__c'] != '0000-00-00') ? date('d.m.Y', strtotime($arrGroup['Registriert_bei_IZS_seit__c'])) : ''),
      'link' => $strLinkCert,
      'add' => $strCreate,
      'del' => $strDelete
    );

}

$arrOpt = array(
    'arrSort' => array(
      'null', '"type": "hidden"', 'null', 'null', '"type": "hidden"', 'null', 'null', 'null'
    ),
    'strOrder' => '"order": [[ 0, "asc" ]]'
);

$strTable = strCreateTable($arrList, $arrTableHead, '', false, true, [], $arrOpt);

$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Premium PDLs gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strSelect;

$strIncOutput.= $strTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

$strJsFootCodeRun.= '

$("span.warning").parent().parent().addClass("warning");
$("span.danger").parent().parent().addClass("danger");

$("#seldate").on("change", function (e) {
  this.form.submit();
});

';

$strJsFootCodeRun.= "

$('.cCert').on('click', function (e) {

    var id   = $(this).attr('id').split('_')[1];
    var rel  = $(this).attr('rel');
    var year = $('#selyear').val();
  
    $.post('/cms/inc/jb.ajax.inc.php', {
      ac: 'create', id: id, clid: " .$_COOKIE['id'] .", gr: rel, year: year
    }).done(function(data) {
      var part = data.split('|');
      $('.gr_' + part[0]).parent().parent().find('td:nth-child(2)').html(part[1]);
      $('.gr_' + part[0]).parent().parent().find('td:nth-child(7)').html(part[2]);
      $('.gr_' + part[0]).parent().parent().find('td:nth-child(9)').html(part[3]);
    });
  
  }).bind('mouseover', function() {
    $(this).css('cursor', 'pointer');
  });

  $(document).on('click', '.dCert', function (e) {

    var id   = $(this).attr('id').split('_')[1];
  
    $.post('/cms/inc/jb.ajax.inc.php', {
      ac: 'delete', id: id, clid: " .$_COOKIE['id'] ."
    }).done(function(data) {
      var part = data.split('|');
      $('.gr_' + part[0]).parent().parent().find('td:nth-child(2)').html(part[1]);
      $('.gr_' + part[0]).parent().parent().find('td:nth-child(6)').html(part[2]);
      $('.gr_' + part[0]).parent().parent().find('td:nth-child(8)').html(part[3]);
    });
  
  }).bind('mouseover', function() {
    $(this).css('cursor', 'pointer');
  });

  $('#selyear').on('change', function (e) {
    this.form.submit();
  });

";


?>