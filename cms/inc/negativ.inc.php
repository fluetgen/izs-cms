<?php

//CLASSES
require_once('classes/class.bg.php');
require_once('classes/class.bgevent.php');
require_once('classes/class.auth.php');
require_once('classes/class.request.php');
require_once('classes/class.sendrequest.php');

require_once('classes/class.premiumpayer.php');


//REQUEST
$strFu   = @$_REQUEST['fu'];
$strSf   = @$_REQUEST['sf'];


//DATA
//$objBg   = new Bg;
//$arrEv = $objBgEvent->arrGetOpenEventList($intYear);

//$objBgEvent->boolCreateEventsFromYear(2016);
//die();

if (@$_SERVER['HTTP_FKLD'] == 'on') {
  //print_r($arrEv);
}


//SETTINGS

$boolShowHead = true;
$boolShowSide = false;

$strPageTitle = 'Basisdokumentation';


$arrNTyp = array(
  'Fehlende SV-Datenlieferung' => 't1',
  'Steuerstraftaten' => 't2',
  'Betriebsprüfung' => 't3',
  'Ratenzahlung' => 't4',
  'Stundung' => 't5',
  'Nachtragsbeitragsbescheid' => 't6'
);

$arrNArt = array(
  'Zeitarbeitsfirma' => 'a1',
  'Finanzamt' => 'a2',
  'Berufsgenossenschaft' => 'a3',
  'Krankenkasse' => 'a4'
);


//FUNCTIONS
$strAllRequest = '';
$strReqRequest = '';
$strProoveRequest = '';
$strBackRequest = '';
$strEscRequest = '';


$arrFilterSection = array();

$strIncOutput = '';

switch ($strFu) {
  case 'all':
    $strAllRequest = 'active';
    require_once('negativ.all.inc.php');
    break;
  default:
    $strAllRequest = 'active';
    $strFu = 'all';
    require_once('negativ.all.inc.php');
    break;
}

//print_r($arrFilterSection);


//CONTENT

$strOutput = '

  <style>
  .page { width: 100%; !important }
  .panel-actions {
    position: absolute;
    top: -80px;
    width: 250px;
  }
  .nosort {
    background-image: none !important;
  }
  </style>

  <!-- Page -->
  <div class="page animsition">
  
    <div class="page-aside">
      <div class="page-aside-switch">
        <i class="icon wb-chevron-left" aria-hidden="true"></i>
        <i class="icon wb-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner" style="/* position: fixed; width: 250px; */">
        <section class="page-aside-section" style="width: 229px;">

          <h5 class="page-aside-title">Filter </h5><span id="resetFilter"></span>
          
          <form style="padding-left: 30px;">
';

if (isset($arrFilterSection['rs']) && (count($arrFilterSection['rs']) > 0)) {
  
  natcasesort($arrFilterSection['rs']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_rs">Rating SV</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_rs" name="filter_rs" placeholder="CompanyGroup" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['rs'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['rb']) && (count($arrFilterSection['rb']) > 0)) {
  
  natcasesort($arrFilterSection['rb']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_rb">Rating BG</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_rb" name="filter_rb" placeholder="Typ" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['rb'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['rd']) && (count($arrFilterSection['rd']) > 0)) {
  
  ksort($arrFilterSection['rd']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_rd">Rating Doku</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_rd" name="filter_rd" placeholder="Update-Status" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['rd'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['es']) && (count($arrFilterSection['es']) > 0)) {
  
  natcasesort($arrFilterSection['es']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_es">Eskalation</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_es" name="filter_es" placeholder="Nächste Aktion" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['es'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrNArt) && (count($arrNArt) > 0)) {
  
  natcasesort($arrNArt);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_a">Art</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_a" name="filter_a" placeholder="Nächste Aktion" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrNArt as $strValue => $strKey) {
    $strOutput.= '                  <option value="' .$strKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrNTyp) && (count($arrNTyp) > 0)) {
  
  natcasesort($arrNTyp);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_t">Typ</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_t" name="filter_t" placeholder="Nächste Aktion" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrNTyp as $strValue => $strKey) {
    $strOutput.= '                  <option value="' .$strKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}


$strOutput.= '          
          </form>

        </section>
      </div>
    </div>
    <div class="page-main">
      <div class="page-header">
        <h1 class="page-title">' .$strPageTitle .'</h1>
';

$strOutput.= '
        <div style="z-index: 1;" class="page-header-actions">

        <div class="panel-actions">


            <div class="dropdown" style="float: right;">
              <button type="button" class="btn btn-info dropdown-toggle " id="exampleAlignmentDropdown"
              data-toggle="dropdown" aria-expanded="true">
                Optionen
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="exampleAlignmentDropdown" role="menu">
                <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="doXlsxExport">Alle Negativmerkmale exportieren</a></li>
              </ul>
            </div>

            </div>

            </div>
';


$strOutput.= '      
        <ul class="nav nav-tabs nav-tabs-line" style="margin-bottom: 20px;">
          <li class="' .$strAllRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=negativ&fu=all">Alle</a></li>
          <!--
          <li class="' .$strReqRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bd_aktualisieren&fu=request">Anzufragen</a></li>
          <li class="' .$strProoveRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bd_aktualisieren&fu=proove">Anfrage prüfen</a></li>
          <li class="' .$strBackRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bd_aktualisieren&fu=back">Anfrage zurückgestellt</a></li>
          <li class="' .$strEscRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bd_aktualisieren&fu=esc">Eskalation</a></li>
          -->
        </ul>
';


$strOutput.= '  

      </div>
      <div class="page-content">
  
';

$strOutput.= $strIncOutput;

$strOutput.= '

      </div> <!-- End page-content -->

    </div> <!-- End page-main -->

  </div>
  <!-- End Page -->


<form method="post" action="_get_csv.php" id="export">
<input type="hidden" name="strTable" id="strTable" value="">
</form>

';


$strJsFootScript.= '  <script src="js/script_neu.js"></script>' .chr(10);

$strJsFootCodeRun.= '

$("span.warning").parent().parent().addClass("warning");
$("span.info").parent().parent().addClass("info");
$("span.danger").parent().parent().addClass("danger");

$("#resetFilter").on("click", function(e) {
  
  $("table.display tbody tr").show();

  $("select.filter option").each(function(i) {
    $(this).css("color", "#3f3f3f");
  });

  $("ul.select2-results__options li").css("color", "#3f3f3f");
  
  $("#DocCount").text($("table.display tbody tr:visible").length);
  $("#resetFilter").text("");
  
  $("select").val("");
    
});

$(".due").parent().parent().addClass("red");

$("#resetFilter").hover(function() {
  $(this).css("cursor","pointer");
});

var $eventSelect = $(".filter");
$eventSelect.on("select2:opening", function (e) { 
  var id = $(this).prop("id");
  setTimeout(function(){
    $("#select2-" + id +"-results li").each(function(i) {
      var str = $(this).prop("id").toString();
      if (str != "") {
        var tr = $("tr:visible td span.hfilter." + str.split("-")[4]);
        if (tr.length > 0) {
          $(this).css("color", "#3f3f3f");
        } else {
          $(this).css("color", "#a3afb7");
        }
      }
    });
  }, 1);
});


$(".filter").on("change", function(e) {
  
  var clstr = "span.hfilter";
  var fiid  = $(this).attr("id");
  var sval  = $(this).val();
  
  //if (uf !== null) {
  //  clstr = uf;
  //  uf = null;
  //} else {
    $("select.filter").each(function(i) {
      if ($(this).val() != "") clstr+= "." + $(this).val();
    });
  //}
  
  $("span.hfilter").parent().parent().hide();
  $(clstr).parent().parent().show();

  window.clstr = clstr;
  
  var res = [];
  
  $("table.display tbody tr:visible td:first-child span").each(function(i) {
    res = res.concat($(this).attr("class").split(" ").filter(function (item) {
      return res.indexOf(item) < 0;
    }));
    return res;
  });
  
  $("#DocCount").text($("table.display tbody tr:visible").length);
  $("#resetFilter").text("alle zurücksetzen");

  $(".docdoc:hidden:checked").prop("checked", false);

});

var url = new URL(location.href);
var uf = url.searchParams.get("filter");
if (uf !== null) {

  var fipa = uf.split(".");
  if (fipa.length > 2) {
    for (var i = 2; i < fipa.length; i++) {
      var v = fipa[i].split("_");
      $("#filter_" + v[0]).val(fipa[i]).trigger("change");
      console.log("#filter_" + v[0] + " -> " + fipa[i]);
    }
  }

}

setTimeout(function() { 
  if (uf === null) {
    //$("#filter_y").val("y_' .(date('Y') - 1) .'").trigger("change");
  }
}, 500);  

  
      $("[id^=\'dd_\']").on("change", function(e) {
      
        var checked = $(this).prop("checked");
        
        if (checked) {
          $(this).parent().parent().css("font-style", "italic");
          $(this).parent().find("span").text("order_1");
        } else {
          $(this).parent().parent().css("font-style", "normal");
          $(this).parent().find("span").text("order_0");
        }

        $(this).parent().parent().parent().parent().DataTable().rows().invalidate("dom");
      
        $.post("action/additional.ajax.php", { 
            ac: "addValue", table: "Dokument_Dokumentation__c", id: $(this).prop("id"), key: "received", value: checked
          }).done(function(data) {
            ;
          }
        );
        
      });  

  
      $("#doExport").on("click", function(e) {
        
        $("input[type=checkbox]").each(function() { 
          $(this).attr("csvchecked", $(this).is(":checked")); 
        });
        
        var table_hd = $($("table")[0]).html();
        var table_cth = $("table.table:eq(1) tr:visible");
        var table_ct = "";
        
        if (table_cth.length > 0) {
          $.each(table_cth, function(k, v) {
            table_ct += "<tr>" + $(v).html() + "</tr>";
          });
        }
        
        $("#strTable").val("<table>" + table_hd + table_ct + "</table>");
        $("#export").submit();
        
      });  

      // Fill modal with content from link href
      $("#DetailModal").on("show.bs.modal", function(e) {
          var link = $(e.relatedTarget);
          $(this).find(".modal-content").load(link.attr("href"));
      });
      
 	    //$("selectpicker").selectpicker();

      $("#doArchiv").click(function(){

        var text = $(".selectpicker option:selected").val()
        var id = $("#ddid").val();

        $(this).parent().parent().fadeTo("slow", 0.00);

        $.ajax({
          type: "POST",
          url:  "action/ajax.php",
          dataType: "text", 
          data: { ac: "ddArchivClick", ddid: id, aaction: text },
          success: function(result) {
            jsonData = $.parseJSON(result);
            
            $("#" + jsonData["ddid"]).parent().parent().fadeTo("slow", 1.00);
            
            if (jsonData["Status"] == "OK") {
              
              if (jsonData["Action"] == "restart") {

                $(\'#st_\' + jsonData[\'ddid\']).html(jsonData[\'UStatus\']);
                $(\'#da_\' + jsonData[\'ddid\']).html(jsonData[\'UTime\']);
                $(\'#dah_\' + jsonData[\'ddid\']).html(jsonData[\'UTimeH\']);

                $(\'#es_\' + jsonData[\'ddid\']).html("");
                $(\'#esh_\' + jsonData[\'ddid\']).html("");
                
                $(\'#\' + jsonData[\'ddid\']).html(jsonData[\'UStatusNew\']);
                $(\'#\' + jsonData[\'ddid\']).prop(\'title\', jsonData[\'UStatusNew\']);

              } 
              
              if (jsonData["Action"] == "archive") {
              
                var table = $("table[id*=table_]").DataTable();
              
                $("#" + jsonData["ddid"]).parent().parent().hide();
                $("#" + jsonData["ddid"]).parent().parent().addClass("selected");
                table.row(".selected").remove().draw(false);
                
                $("#DocCount").text(Number($("#DocCount").text()) - 1);
              
              }
                  
            }
            
            if (jsonData["Status"] == "FAIL") {
              alert(jsonData["Reason"]);
            }
    
          }
        });

       	$("#Archive").modal("hide");
       	//console.log(text);
      });
';

?>