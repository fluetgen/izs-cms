<?php

$strSelect = '';
$strSql = 'SELECT DISTINCT(`is_date`) AS `is_date` FROM `import_stats` ORDER BY `is_date` DESC';
$arrAvailMonthList = MySQLStatic::Query($strSql);

$intMonthSum = count($arrAvailMonthList);
if ($intMonthSum > 0) {
  $strSelect.= '<form action="/cms/index_neu.php?ac=invoice&fu=clearing" method="post"><label for="seldate" style="margin-right: 20px;">Abrechnungsmonat</label><select name="seldate" id="seldate">' .chr(10);
  foreach ($arrAvailMonthList as $intKey => $strMonth) {
    
    $intDate = strtotime($strMonth['is_date']);
    $intDate = date('U', mktime(0, 0, 0, date('n', $intDate) + 1, date('j', $intDate), date('Y', $intDate)));
    
    if (empty($_REQUEST['seldate']) && $intKey == 0) {
      $_REQUEST['seldate'] = date('Y-m', $intDate);
    }
    $strSelected = '';
    if (date('Y-m', $intDate) == @$_REQUEST['seldate']) {
      $strSelected = ' selected="selected"';
      $intMonth = date('m', $intDate);
      $intYear = date('Y', $intDate);
    }
    
    $strSelect.= '<option value="' .date('Y-m', $intDate) .'"' .$strSelected .'>' .date('Y-m', $intDate) .'</option>' .chr(10);
  }  
  $strSelect.= '</select></form>' .chr(10);


  $arrPart = explode('-', $_REQUEST['seldate']);
  $intBillMonth = mktime(16, 0, 0, $arrPart[1], 0, $arrPart[0]);
  if ($intBillMonth > time()) {
    $strSelect.= '<p><span class="red">Der ausgewälte Monat kann frühestens am ' .date('d.m.Y', $intBillMonth) .' abgerechnet werden.</span></p>' .chr(10);
  }


}


$strSql = 'SELECT `Group__c`.`Id` AS `GId`, `Group__c`.`Name` AS `GName`, `Betriebsnummer__c`, `Vertrag__c`.*  FROM `Vertrag__c` INNER JOIN `Group__c` ON `Company_Group__c` = `Group__c`.`Id` WHERE `Aktiv__c` = "true" AND `Anzahl_inkl_Kl_rungsf_lle__c` != 0 ORDER BY `GName`';
$arrGroupList = MySQLStatic::Query($strSql);
$intCountResult = count($arrGroupList);

$arrGroupListPerMonth = array();
if ($intCountResult > 0) {
  foreach ($arrGroupList as $intRow => $arrGroup) {
    $arrMonthList = explode(';', $arrGroup['Abzurechnen_in_Monat__c']);
    if (in_array($intMonth, $arrMonthList)) {
      $arrGroupListPerMonth[] = $arrGroup;
    }
  }
}

$arrGroupList = $arrGroupListPerMonth;

//print_r($arrGroupListPerMonth);

$arrEntList = array();
$strSql = 'SELECT * FROM `Account` WHERE `RecordTypeId` = "01230000001BvebAAC"';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrEnt) {
    $arrEntList[$arrEnt['Id']] = $arrEnt['Name'];
  }
}

$arrTableHead = array(
  'Personaldienstleister',
  'Mitgliedsmodell',
  'Zahler',
  'Zahler ab',
  'Btnr',
  'V-Beginn',
  'Abrechnung',
  'Klärungsfälle',
  'Summe KV',
  'vereinbart',
  'Logik',
  'Abweichung',
  'Preis/KV',
  'Summe',
  'Anmerkung'
);

function arrGetDeliveredEvents ($strGrId = '', $strMonth = '', $intMonthCount = 0) {

  //echo $intMonthCount;
  
  $arrReturn = array(
    'intCount' => 0,
    'intMissing' => ($intMonthCount + 1)
  );
  
  $intDate = strtotime($strMonth);
  $strMonthStop = date('Y-m', mktime(0, 0, 0, date('n', $intDate) - $intMonthCount - 1, date('j', $intDate), date('Y', $intDate))) .'-01';


  $arrClearingPerMonth = array();
  $arrSqlMonth = array();
  for ($intCount = 0; $intCount <= $intMonthCount; $intCount++) {
    $strMonth = date('Y / n', mktime(0, 0, 0, date('n', $intDate) - $intCount - 1, date('j', $intDate), date('Y', $intDate)));
    $arrSqlMonth[] = $strMonth;
    $arrClearingPerMonth[$strMonth] = 0;
  }

  //krsort($arrClearingPerMonth);

  $strSqlMonth = '("' .implode('", "', $arrSqlMonth) .'")';

  $arrSumReal = array();
  $strSql = 'SELECT COUNT(*) AS `Anz`, `SF42_EventPeriod__c`, `SF42_Month__c`, `SF42_Year__c` FROM `SF42_IZSEvent__c` WHERE `Status_Klaerung__c` != "" AND `Grund__c` = "Beitragsrückstand" AND `SF42_EventPeriod__c` IN ' .$strSqlMonth .' AND `group_id` = "' .$strGrId .'" GROUP BY `SF42_EventPeriod__c` ORDER BY `SF42_Year__c`, `SF42_Month__c`';
  $arrClearingList = MySQLStatic::Query($strSql);

  if (is_array($arrClearingList) && (count($arrClearingList) > 0)) {
    foreach ($arrClearingList as $intCount => $arrClearing) {
        $arrClearingPerMonth[$arrClearing['SF42_EventPeriod__c']] = $arrClearing['Anz'];
    }
  }
  
  return $arrClearingPerMonth;
}

$arrList  = array();
$floatSum = 0.0;
$intMonthCount = 0;

foreach ($arrGroupList as $intRow => $arrGroup) {

  //print_r($arrGroup);

  if ($arrGroup['Abrechnungszeitraum__c'] == '') continue; 


  $strSql = 'SELECT `Erstanfrage_Monat`, `Erstanfrage_Jahr` FROM `Group__c` WHERE `Id`= "' .$arrGroup['Company_Group__c'] .'" AND (`Erstanfrage_Monat` != "" AND `Erstanfrage_Jahr` != "")';
  $arrSqlContract = MySQLStatic::Query($strSql);
  if (count($arrSqlContract) == 0) {
    $arrSqlContract[0]['Erstanfrage_Monat'] = 11;
    $arrSqlContract[0]['Erstanfrage_Jahr'] = 2011;
  } 
  
  preg_match("/(\d{1,2})/", $arrGroup['Abrechnungszeitraum__c'], $arrMonth);
  $intMonthCount = $arrMonth[0];

  // 
  
  $intCountContract = $arrGroup['Anzahl_inkl_Kl_rungsf_lle__c'];
  if ($intCountContract == 0) {
    $intCountContract = 'inklusive';
  }
  
  $strMonthDiff = '';
  $strCountReal = '';

  $arrCountReal = arrGetDeliveredEvents($arrGroup['Company_Group__c'], $intYear .'-' .$intMonth .'-01', $intMonthCount -1);

  $intCountRealMonth = 0;

  if (count($arrCountReal) > 0) {
    foreach ($arrCountReal as $strPeriod => $intClearing) {
      $arrPart = explode(' / ', $strPeriod);

      if (strtotime($arrPart[0] .'-' . $arrPart[1] .'-01') >= strtotime($arrSqlContract[0]['Erstanfrage_Jahr'] .'-' .$arrSqlContract[0]['Erstanfrage_Monat'] .'-01')) {
        $strCountReal.= sprintf('%02d', $arrPart[1]) .'-' .$arrPart[0] .' => ' .$intClearing .', ' .'<br />';
        $strMonthDiff.= '_' .(int) $arrPart[1] .'_' .$intClearing;
        $intCountRealMonth++;
      }
      
    }
    $strCountReal = substr($strCountReal, 0, -8);
  }

  if ($strCountReal == '') {
    continue;
  }

  if ($arrGroup['Logik_Klaerung__c'] == 'monatlich') {

    $intCountContract = $intCountContract / 3;

    $intSumKv = 0;
    $intDiff  = 0;
    foreach ($arrCountReal as $strKey => $intCountClearing) {
      if ($intCountClearing > ($arrGroup['Anzahl_inkl_Kl_rungsf_lle__c'] / 3)) {
          $intDiff+= ($intCountClearing - ($arrGroup['Anzahl_inkl_Kl_rungsf_lle__c'] / 3));
      }
      $intSumKv+= $intCountClearing;
    }

  } else {

    $intSumKv = 0;
    $intDiff  = 0;
    foreach ($arrCountReal as $strKey => $intCountClearing) {
      $intSumKv+= $intCountClearing;
    }
    if ($intSumKv > $arrGroup['Anzahl_inkl_Kl_rungsf_lle__c']) {
      $intDiff = ($intSumKv - $arrGroup['Anzahl_inkl_Kl_rungsf_lle__c']);
    }

  }
  
  $intCountReal = $intSumKv;


  $arrCountReal['intMissing'] = 0;

  if ($intCountRealMonth > 0) { 
    $arrCountReal['intMissing'] = 3 - $intCountRealMonth;
  }

  $strNote = '';
  if ($arrCountReal['intMissing'] > 0) {
    $strNote = '<i class="icon fa fa-warning" aria-hidden="true"></i> ' .$arrCountReal['intMissing'] .' Abrechnungsmonat fehlt';
  } 
  if ($arrCountReal['intMissing'] > 1) {
    $strNote = '<i class="icon fa fa-warning" aria-hidden="true"></i> ' .$arrCountReal['intMissing'] .' Abrechnungsmonate fehlen';
  }
  
  $strAddClass = '';
  if (($intDiff > 0) && ($arrGroup['Anzahl_inkl_Kl_rungsf_lle__c'] > 0)) {
    $strAddClass.= 'danger';
  } elseif ($arrGroup['Anzahl_inkl_Kl_rungsf_lle__c'] > 0) {
    //$strAddClass.= 'warning';
  }

  //print_r($arrMapping);

  $strSum = '0,00 &euro;';
  if (($intDiff > 0) && ($arrGroup['Anzahl_inkl_Kl_rungsf_lle__c'] > 0)) {
    $intSum = $arrGroup['Kosten_Kl_rungsfall__c'] * $intDiff;
    $strSum = number_format($intSum, 2, ',', '') .' &euro;';
    $strSum.= ' <i class="fa fa-money svover" aria-hidden="true" rel="' .$arrMapping[$arrGroup['GId']] .'_' .$intDiff .'_' .$intCountReal .':' .$strMonthDiff .'"></i>';
  } else {
    $intSum = 0;
  }

  $strDiff = '';
  if (($intDiff > 0) && ($arrGroup['Anzahl_inkl_Kl_rungsf_lle__c'] > 0)) {
    $strDiff = $intDiff;
  } elseif ($arrGroup['Anzahl_inkl_Kl_rungsf_lle__c'] > 0) {
    $strDiff = '0';
  }

  $strOver = '';
  $strOverFrom = '';
  if ($arrGroup['BillingOver'] != '') {

    $strOver = $arrEntList[$arrGroup['BillingOver']];
    $strOverFrom = date('d.m.Y', strtotime($arrGroup['BillingOverFrom']));

  }

  $arrList[] = array(
    'ppayer'   => $arrGroup['GName'] .'<span class="hidden ' .$strAddClass .'"></span>',
    'model'    => $arrModelList[$arrGroup['GId']],
    'payer'    => '',
    'pfrom'    => '',
    'btnr'     => $arrGroup['Betriebsnummer__c'],
    'since'    => strFormatDate($arrGroup['Vertragsbeginn__c'], 'd.m.Y', 'Y-m-d'),
    'invoice'  => $arrGroup['Abrechnungszeitraum__c'],
    'count'    => $strCountReal,
    'sumkv'    => $intSumKv,
    'contract' => $intCountContract, 
    'logic'    => $arrGroup['Logik_Klaerung__c'],
    'diff'     => $strDiff, 
    'price'    => number_format($arrGroup['Kosten_Kl_rungsfall__c'], 2, ',', '.') .' &euro;',
    //'sum'      => '<span class="hidden">' .number_format($arrGroup['Preis_pro_Datensatz_ber_Inklusivvolumen__c'] * $intDiff, 2, '', '') .'</span><span style="white-space: nowrap; text-align: right; display: block;">' .$strSum .'</span>',
    'sum'      => $strSum,
    'note'     => $strNote
  );

  if ($intDiff > 0) {
    $floatSum+= $intSum;
  }

}

$strTable = strCreateTable($arrList, $arrTableHead, '', false, true);

$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Verträge gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strSelect;

$strIncOutput.= '<p>Maximal ' .number_format($floatSum, 2, ',', '.') .' EUR' .' können nachberechnet werden.</p>';

$strIncOutput.= $strTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

$strJsFootCodeRun.= '

$("span.warning").parent().parent().addClass("warning");
$("span.danger").parent().parent().addClass("danger");

$("#seldate").on("change", function (e) {
  this.form.submit();
});

$("td:nth-child(6)").css("text-align",  "center");
$("td:nth-child(7)").css("text-align",  "center");
$("td:nth-child(8)").css("text-align",  "right");
$("td:nth-child(9)").css("text-align",  "right");
$("td:nth-child(10)").css("text-align",  "right");
$("td:nth-child(7)").css("white-space", "nowrap");

$(".fa-money").on("click", function (e) {

  var ty, tys, obj, rel, part;

  obj = $(this);

  ty = 8;
  tys = "Überschreitung Klärungsfälle";

  rel = $(this).attr("rel").split("_");

  part = $(this).attr("rel").split(":");

  $.post("inc/invoice.ajax.php", { 
    ac: "createBill", id: rel[0], real: rel[2], diff: rel[1], month: ' .(int) $intMonth .', mcount: ' .$intMonthCount .', year: ' .$intYear .', type: ty, detail: part[1]
    }).done(function(data) {
      if (data == 1) {
        console.log(data);
        toastr.options = { "closeButton": true, "positionClass": "toast-top-full-width", "newestOnTop": true,   "showMethod": "fadeIn", "hideMethod": "fadeOut" }
        toastr.success(\'Die Rechnung "\' + tys + \'" wurde an easyBill gesendet.\');
        obj.hide();
      } else {
        toastr.options = { "closeButton": true, "positionClass": "toast-top-full-width", "newestOnTop": true,   "showMethod": "fadeIn", "hideMethod": "fadeOut" }
        toastr.error(\'Die Rechnung konnte nicht gesendet werden. Es sind nur 10 Zugriffe pro Minute möglich. Versuchen Sie es später noch einmal.\');
      }
    }
  );
  
});

';


?>