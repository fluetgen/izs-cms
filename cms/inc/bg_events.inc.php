<?php

//CLASSES
require_once('classes/class.bg.php');
require_once('classes/class.bgevent.php');
require_once('classes/class.auth.php');
require_once('classes/class.request.php');
require_once('classes/class.sendrequest.php');

require_once('classes/class.premiumpayer.php');


//REQUEST
$intYear = @$_REQUEST['year'];
$strFu   = @$_REQUEST['fu'];
$strSf   = @$_REQUEST['sf'];

if (empty($intYear)) {
  $intYear = date('Y') - 1;
}


//DATA
$objBg   = new Bg;
$objPp   = new PremiumPayer;
$objAuth = new Auth;
$objReq  = new Request;
$objSendReq  = new SendRequest;

$objBgEvent = new BgEvent;

// 15.10.23 - Es gibt immer wieder Probleme dass bei "Verbindung" nicht die Id der Verbindung, sondern die der Meldestelle drin steht.
// Dieser Hack soll das erstmal beheben, bis der Fehler gefunden wurde.
$strSql = 'UPDATE izs_bg_event AS event
JOIN Verbindung_Meldestelle_BG__c AS verbindung
ON event.be_meldestelle = verbindung.Meldestelle__c AND event.be_bg = verbindung.Berufsgenossenschaft__c
SET event.be_verbindung = verbindung.Id
WHERE event.be_verbindung LIKE "00%";';
$arrRes = MySQLStatic::Query($strSql);


$arrEv = $objBgEvent->arrGetOpenEventList($intYear);

//$objBgEvent->boolCreateEventsFromYear(2016);
//die();

if (@$_SERVER['HTTP_FKLD'] == 'on') {
  //print_r($arrEv);
}

/*
$arrAuthList = $objAuth->arrGetAuthLink('0013000001DuQEhAAN');
print_r($arrAuthList);
die();
*/

$arrBgListEvent = $objBgEvent->arrGetBgListEvent($intYear);

$arrBGList = array();
$arrGetBgList = $objBg->arrGetBgView();
if (is_array($arrGetBgList) && (count($arrGetBgList) > 0)) {
  foreach($arrGetBgList as $intKey => $arrBg) {
    $arrBGList[$arrBg['Id']] = $arrBg['Name'];
  }
}

//print_r($arrBGList);

//SETTINGS

$boolShowHead = true;
$boolShowSide = false;

$strPageTitle = 'Auskünfte von Berufsgenossenschaften';


//FUNCTIONS
$strActiveRequest = '';
$strRuecklaufRequest = '';
$strKlaerungRequest = '';
$strZuordnungRequest = '';
$strEndkontrolleRequest = '';
$strAllRequest    = '';
$strDueRequest    = '';
$strSetupRequest    = '';


$strActiveRequestZ = '';
$strActiveRequestD = '';
$strActiveRequestB = '';

if ((is_null($strSf)) || ($strSf == '') || ($strSf == 'zentral')) {
  $strActiveRequestZ = 'active';
  $strSf = 'zentral';
} elseif ($strSf == 'dezentral') {
  $strActiveRequestD = 'active';
} elseif ($strSf == 'bnr') {
  $strActiveRequestB = 'active';
}


$arrFilterSection = array();

$strIncOutput = '';

switch ($strFu) {
  case 'request':
    $strActiveRequest = 'active';
    require_once('bg_events.request.inc.php');
    break;
  case 'ruecklauf':
    $strRuecklaufRequest = 'active';
    require_once('bg_events.ruecklauf.inc.php');
    break;
  case 'klaerung':
    $strKlaerungRequest = 'active';
    require_once('bg_events.klaerung.inc.php');
    break;
  case 'zuordnung':
    $strZuordnungRequest = 'active';
    require_once('bg_events.zuordnung.inc.php');
    break;
  case 'endkontrolle':
    $strEndkontrolleRequest = 'active';
    require_once('bg_events.endkontrolle.inc.php');
    break;
  case 'all':
    $strAllRequest = 'active';
    require_once('bg_events.all.inc.php');
    break;
  case 'due':
    $strDueRequest = 'active';
    require_once('bg_events.faellig.inc.php');
    break;
  case 'setup':
    $strSetupRequest = 'active';

    $_REQUEST['ty'] = 3;
    
    if (isset($_REQUEST['id'])) {
      require_once('temp.edit.inc.php');
    } else {
      require_once('temp.list.inc.php');
    }

    break;
  default:
    $strActiveRequest = 'active';
    require_once('bg_events.request.inc.php');
    break;
}

//print_r($arrFilterSection);


//CONTENT

$strOutput = '

  <style>
  .page { width: 100%; !important }
  .panel-actions {
    position: absolute;
    top: -80px;
  }
  </style>

  <!-- Page -->
  <div class="page animsition">
  
    <div class="page-aside">
      <div class="page-aside-switch">
        <i class="icon wb-chevron-left" aria-hidden="true"></i>
        <i class="icon wb-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner" style="/* position: fixed; width: 250px; */">
        <section class="page-aside-section" style="width: 229px;">
          <h5 class="page-aside-title">Filter </h5><span id="resetFilter"></span>
          
          <form style="padding-left: 30px;">
';


if (isset($arrFilterSection['us']) && (count($arrFilterSection['us']) > 0)) {

  //print_r($arrFilterSection['us']);
  
  arsort($arrFilterSection['us']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_us">Berabeiter:in</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_y" name="filter_us" placeholder="Berarbeiter:in" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['us'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="us_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['y']) && (count($arrFilterSection['y']) > 0)) {
  
  arsort($arrFilterSection['y']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_y">Jahr</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_y" name="filter_y" placeholder="Jahr" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['y'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="y_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['bg']) && (count($arrFilterSection['bg']) > 0)) {
  
  natcasesort($arrFilterSection['bg']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_bg">Berufsgenossenschaft</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_bg" name="filter_bg" placeholder="Berufsgenossenschaft" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['bg'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="bg_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['pp']) && (count($arrFilterSection['pp']) > 0)) {
  
  natcasesort($arrFilterSection['pp']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_pp">Meldestelle</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_pp" name="filter_pp" placeholder="Meldestelle" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['pp'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="pp_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['ru']) && (count($arrFilterSection['ru']) > 0)) {
  
  arsort($arrFilterSection['ru']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_ru">Rückmeldung</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_ru" name="filter_ru" placeholder="Rückmeldung" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['ru'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="ru_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['sb']) && (count($arrFilterSection['sb']) > 0)) {
  
  natcasesort($arrFilterSection['sb']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_sb">Status Bearbeitung</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_sb" name="filter_sb" placeholder="Status Bearbeitung" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['sb'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="sb_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['er']) && (count($arrFilterSection['er']) > 0)) {
  
  natcasesort($arrFilterSection['er']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_er">Ergebnis</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_er" name="filter_er" placeholder="Ergebnis" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['er'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="er_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['bb']) && (count($arrFilterSection['bb']) > 0)) {
  
  natcasesort($arrFilterSection['bb']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_bb">Befristet bis</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_bb" name="filter_bb" placeholder="Befristet bis" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['bb'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="bb_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['sp']) && (count($arrFilterSection['sp']) > 0)) {
  
  natcasesort($arrFilterSection['sp']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_sp">Sperrvermerk</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_sp" name="filter_sp" placeholder="Sperrvermerk" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['sp'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="sp_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['si']) && (count($arrFilterSection['si']) > 0)) {
  
  natcasesort($arrFilterSection['si']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_si">Sichtbarkeit des Dokuments</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_si" name="filter_si" placeholder="Sichtbarkeit des Dokuments" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['si'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="si_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['sk']) && (count($arrFilterSection['sk']) > 0)) {
  
  natcasesort($arrFilterSection['sk']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_sk">Status Klärung</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_sk" name="filter_sk" placeholder="Status Klärung" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  $strOutput.= '                  <option value="sk_">kein Status</option>' .chr(10);
  foreach ($arrFilterSection['sk'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="sk_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['gk']) && (count($arrFilterSection['gk']) > 0)) {
  
  natcasesort($arrFilterSection['gk']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_gk">Grund für Klärung</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_gk" name="filter_gk" placeholder="Grund Klärung" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  $strOutput.= '                  <option value="gk_">kein Grund</option>' .chr(10);
  foreach ($arrFilterSection['gk'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="gk_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['ns']) && (count($arrFilterSection['ns']) > 0)) {
  
  natcasesort($arrFilterSection['ns']);

  //print_r($arrFilterSection['ns']); die();
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_ns">Nächster Schritt</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_ns" name="filter_ns" placeholder="Nächster Schritt" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['ns'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="ns_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['wi']) && (count($arrFilterSection['wi']) > 0)) {
  
  natcasesort($arrFilterSection['wi']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_wi">Wiedervorlage</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_wi" name="filter_wi" placeholder="Wiedervorlage" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['wi'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="wi_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['rs']) && (count($arrFilterSection['rs']) > 0)) {
  
  natcasesort($arrFilterSection['rs']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_rs">Stundung / Ratenzahlung</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_rs" name="filter_rs" placeholder="Stundung" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['rs'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="rs_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['mp']) && (count($arrFilterSection['mp']) > 0)) {
  
  natcasesort($arrFilterSection['mp']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_mp">Meldepflicht erfüllt</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_mp" name="filter_mp" placeholder="Stundung" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['mp'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="mp_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

if (isset($arrFilterSection['nm']) && (count($arrFilterSection['nm']) > 0)) {
  
  natcasesort($arrFilterSection['nm']);
  
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="filter_mp">Negativmerkmal CMS</label>' .chr(10);
  $strOutput.= '                <select class="form-control filter" id="filter_nm" name="filter_nm" placeholder="Stundung" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  foreach ($arrFilterSection['nm'] as $intKey => $strValue) {
    $strOutput.= '                  <option value="nm_' .$intKey .'">' .$strValue .'</option>' .chr(10);
  }
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
}

$strOutput.= '          
          </form>
';

if (!isset($strFu) || ($strFu == 'request')) {
  $strOutput.= '          <form style="padding-left: 30px;">' .chr(10);
  $strOutput.= '            <div class="form-group form-material row">' .chr(10);
  $strOutput.= '              <div class="col-sm-10">' .chr(10);
  $strOutput.= '                <label class="control-label" for="online">Online-Zugang</label>' .chr(10);
  $strOutput.= '                <select class="form-control" id="online" name="online" data-plugin="select2">' .chr(10);
  $strOutput.= '                  <option value="">Alle anzeigen</option>' .chr(10);
  $strOutput.= '                  <option value="on_yes">Ja</option>' .chr(10);
  $strOutput.= '                  <option value="on_no">Nein</option>' .chr(10);
  $strOutput.= '                </select>' .chr(10);
  $strOutput.= '              </div>' .chr(10);
  $strOutput.= '            </div>' .chr(10);
  $strOutput.= '          </form>' .chr(10);
}

$strOutput.= '          
        </section>
      </div>
    </div>
    <div class="page-main">
      <div class="page-header">
        <h1 class="page-title">' .$strPageTitle .'</h1>
';

$strOutput.= '
        <div style="z-index: 1;" class="page-header-actions">

          <div class="panel-actions">

            <div class="dropdown">
              <button type="button" class="btn btn-info dropdown-toggle pull-right" id="exampleAlignmentDropdown"
              data-toggle="dropdown" aria-expanded="true">
                Optionen
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="exampleAlignmentDropdown" role="menu">
                <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="doExport">Export CSV</a></li>' .chr(10);
if (($intAdmin == 1) || (1)) {
  $strOutput.= '                <li role="presentation"><a href="javascript:void(0)" role="menuitem" data-target="#modalAddBgEvent" data-toggle="modal">Event hinzufügen</a></li>' .chr(10);
}
if (isset($_REQUEST['fu']) && ($_REQUEST['fu'] == 'all')) {
  $strOutput.= '                <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="vbgdoc">Word Anfrageliste</a></li>' .chr(10);
  $strOutput.= '                <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="vbgxls">XLSX Anfrageliste</a></li>' .chr(10);
}
if (isset($_REQUEST['fu']) && ($_REQUEST['fu'] == 'klaerung')) {
  $strOutput.= '                <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="massKlae">Klärungsfall</a></li>' .chr(10);
}
if (isset($_REQUEST['fu']) && ($_REQUEST['fu'] == 'ruecklauf')) {
  //if ($_SESSION['id'] == 2) {
    $strOutput.= '                <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="massOk" style="background: #67d933; color: white;">OK erfassen</a></li>' .chr(10);
    $strOutput.= '                <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="massKlae">Klärungsfall</a></li>' .chr(10);
  //}
}
$strOutput.= '              </ul>
            </div>

          </div>

        </div>
';


$strOutput.= '      
        <ul class="nav nav-tabs nav-tabs-line" style="margin-bottom: 20px;">
          <li class="' .$strActiveRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bg_events&fu=request">Anfrage</a></li>
          <li class="' .$strRuecklaufRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bg_events&fu=ruecklauf">Rücklauf</a></li>
          <li class="' .$strKlaerungRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bg_events&fu=klaerung">Klärung</a></li>
          <!-- <li class="' .$strZuordnungRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bg_events&fu=zuordnung">Zuordnung</a></li> -->
          <li class="' .$strEndkontrolleRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bg_events&fu=endkontrolle">Endkontrolle</a></li>
          <li class="' .$strAllRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bg_events&fu=all">Alle</a></li>
          <li class="' .$strDueRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bg_events&fu=due">Fällig</a></li>
          <li class="' .$strSetupRequest .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bg_events&fu=setup">Setup</a></li>
        </ul>
';

if (($strFu == 'request') || ($strFu == '')) {

$strOutput.= '      

  <ul class="nav nav-tabs nav-tabs-line" style="margin-bottom: 10px;">
    <li class="' .$strActiveRequestZ .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bg_events&fu=request&sf=zentral">Zentral</a></li>
    <li class="' .$strActiveRequestD .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bg_events&fu=request&sf=dezentral">Dezentral</a></li>
    <li class="' .$strActiveRequestB .'"><a data-toggle="tab" class="tab" href="/cms/index_neu.php?ac=bg_events&fu=request&sf=bnr">Nach BNR</a></li>
  </ul>
';

}


$strOutput.= '  

      </div>
      <div class="page-content">
  
';

$strOutput.= $strIncOutput;

$strOutput.= '

      </div> <!-- End page-content -->

    </div> <!-- End page-main -->

  </div>
  <!-- End Page -->
  

  <!-- Modal -->
  <div class="modal fade modal-danger" id="ModalDanger" aria-hidden="true"
  aria-labelledby="ModalDanger" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center smallw">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title">Achtung</h4>
        </div>
        <div class="modal-body">
          <p id="errMessage">Text</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
        </div>
      </div>
    </div>
  </div>
  <!-- End Modal -->
';

$strYearOptions = '';
for ($intCount = 0; $intCount <= 5; $intCount++) {
  $intYear = date('Y') - 3 + $intCount;
  $strSelected = '';
  if ($intYear == (date('Y') - 1)) {
    $strSelected = ' selected="selected"';
  }
  $strYearOptions.= '              <option value="' .$intYear. '"' .$strSelected. '>' .$intYear. '</option>' .chr(10);
}

$strMeldestelleOptions = '';
$strMeldestelleOptions.= '              <option value="">Bitte auswählen</option>' .chr(10);
$arrMeldestellenList = $objBg->arrGetMeldestellen();
if (is_array($arrMeldestellenList) && (count($arrMeldestellenList) > 0)) {
  foreach ($arrMeldestellenList as $intKey => $arrMeldestelle) {
    $strMeldestelleOptions.= '              <option value="' .$arrMeldestelle['acid_meldestelle']. '">' .$arrMeldestelle['ac_name']. '</option>' .chr(10);
  }
}

$strOutput.= '
  <!-- Modal -->
  <div class="modal fade modal-info" id="modalAddBgEvent" aria-hidden="true"
  aria-labelledby="modalAddBgEvent" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center">
    <form id="formBgEventAdd" class="modal-content form-horizontal" autocomplete="off" method="post" action="action/bgevent.ajax.php">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title">Event hinzufügen</h4>
        </div>
        <div class="modal-body">

        <input type="hidden" name="ac" value="formBgEventAddSubmit" />
        <input type="hidden" name="mofilter" class="mofilter" value="" />
        
          <div class="row">
            <div class="form-group form-material ">
              <label class="col-sm-3 control-label">Jahr</label>
              <div class="col-sm-8">
                <select class="form-control" id="be_jahr" name="be_jahr" data-plugin="select2-">
' .$strYearOptions .'
                </select>
              </div>
            </div> <!-- form-group -->
          </div> <!-- row -->

          <div class="row">
            <div class="form-group form-material ">
              <label class="col-sm-3 control-label">Meldestelle</label>
              <div class="col-sm-8">
                <select class="form-control" id="be_meldestelle" name="be_meldestelle" data-plugin="select2-">
' .$strMeldestelleOptions .'
                </select>
              </div>
            </div> <!-- form-group -->
          </div> <!-- row -->

          <div class="row">
            <div class="form-group form-material bg" style="display: none;">
              <label class="col-sm-3 control-label">Berufsgenossenschaft</label>
              <div class="col-sm-8">
                <select class="form-control" id="be_bg" name="be_bg" data-plugin="select2-">
                </select>
              </div>
            </div> <!-- form-group -->
          </div> <!-- row -->

          <div class="row">
            <div class="form-group form-material bg" style="display: none;">
              <label class="col-sm-3 control-label">Beitragsfälligkeit</label>
              <div class="col-sm-8">
                <input name="be_beitragsfaelligkeit" id="be_beitragsfaelligkeit" type="text" class="form-control dp" data-plugin="datepicker" value="" data-date-format="dd.mm.yyyy" style="width: 120px;">
              </div>
            </div> <!-- form-group -->
          </div> <!-- row -->
          
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary margin-top-5 pull-right" type="submit" id="formBgEventAddSubmit">Event hinzufügen</button>
          <button data-dismiss="modal" class="btn btn-default btn-default margin-top-5 margin-right-10 pull-right" type="button">Close</button>
        </div>
      </div>
    </form>

    </div>
  </div>
  <!-- End Modal -->

  <!-- Modal -->
  <div tabindex="-1" role="dialog" aria-labelledby="modalDuplicateBgEventLabel" aria-hidden="false" id="modalDuplicateBgEvent" class="modal fade">
    <div class="modal-dialog modal-center modal-info">
      <form action="action/bgevent.ajax.php" method="post" autocomplete="off" class="modal-content form-horizontal" id="formBgDuplicate">
      <input type="hidden" value="formBgEventDuplicateSubmit" name="ac">
      <input type="hidden" value="" id="duplicateid" name="duplicateid">
      <input type="hidden" value="" class="mofilter" name="mofilter">
      
        <div class="modal-header ">
          <button aria-label="Close" data-dismiss="modal" class="close" type="button">
            <span aria-hidden="true">x</span>
          </button>
          <h4 id="modalDuplicategEventLabel" class="modal-title">Event duplizieren</h4>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button id="formBgEventDuplicateSubmit" type="submit" class="btn btn-primary margin-top-5 pull-right">Event duplizieren</button>
          <button type="button" class="btn btn-default btn-default margin-top-5 margin-right-10 pull-right" data-dismiss="modal">Close</button>
        </div>
          <!-- modal-body -->
      </form>
    </div>
  </div>
  <!-- End Modal -->  

  <!-- Modal -->
  <div tabindex="-1" role="dialog" aria-labelledby="modalEditBgEventLabel" aria-hidden="false" id="modalEditBgEvent" class="modal fade">
    <div class="modal-dialog modal-center modal-info smallw">
      <form action="action/bgevent.ajax.php" method="post" autocomplete="off" class="modal-content form-horizontal" id="formBgEventEdit">
      <input type="hidden" value="formBgEventEditSubmit" name="ac">
      <input type="hidden" value="" id="editid" name="editid">
      <input type="hidden" value="" class="mofilter" name="mofilter">
        <div class="modal-header ">
          <button aria-label="Close" data-dismiss="modal" class="close" type="button">
            <span aria-hidden="true">x</span>
          </button>
          <h4 id="modalEditgEventLabel" class="modal-title">Event bearbeiten</h4>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button id="formBgEventEditSubmit" type="submit" class="btn btn-primary margin-top-5 pull-right">Event bearbeiten</button>
          <button type="button" class="btn btn-default btn-default margin-top-5 margin-right-10 pull-right" data-dismiss="modal">Close</button>
        </div>
         <!-- modal-body -->
      </form>
    </div>
  </div>
  <!-- End Modal -->  

  <!-- Modal -->
  <div class="modal fade modal-info" id="modalDeleteBgEvent" aria-hidden="true"
  aria-labelledby="modalDeleteBgEvent" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center smallw">
    <form id="formBgEventDelete" class="modal-content form-horizontal" autocomplete="off" method="post" action="action/bgevent.ajax.php">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title">Event löschen</h4>
        </div>
        <div class="modal-body">

          <input type="hidden" name="ac" value="formBgEventDeleteSubmit" />
          <input type="hidden" name="deleteid" id="deleteid" value="" />
          <input type="hidden" name="mofilter" class="mofilter" value="" />
          
          <div class="row">
            <div class="form-group form-material ">
              <div class="col-sm-11 margin-left-15">
                <p>Willst Du das BG-Event wirklich endgültig löschen?</p>
                <p>Bitte beachte: Dieser Vorgang kann nicht mehr rückgängig gemacht werden.</p>
              </div>
            </div>
          </div> <!-- row -->
          
        </div>
        <div class="modal-footer">
          <button class="btn btn-default margin-top-5 pull-right" type="submit" id="formBgEventDeleteSubmit">Event löschen</button>
          <button data-dismiss="modal" class="btn btn-primary btn-default margin-top-5 margin-right-10 pull-right" type="button">Close</button>
        </div>
      </div>
    </form>

    </div>
  </div>
  <!-- End Modal -->
  
  <!-- Your custom menu with dropdown-menu as default styling -->
  <div id="ContextMenu">
    <ul class="dropdown-menu" role="menu">
      <li><a tabindex="-1" data-action="1"><i class="icon md-edit" aria-hidden="true" data-action="1"></i> Bearbeiten</a></li>
      <li><a tabindex="-1" data-action="2"><i class="icon md-copy" aria-hidden="true" data-action="2"></i> Duplizieren</a></li>
      <li class="divider"></li>
      <li><a tabindex="-1" data-action="3"><i class="icon md-delete" aria-hidden="true" data-action="3"></i> Löschen</a></li>
    </ul>
  </div>

  <form method="post" action="_get_csv.php" id="export">
  <input type="hidden" name="format" id="format" value="">
  <input type="hidden" name="strTable" id="strTable" value="">
  </form>
  
';


$strJsFootScript.= '  <script src="js/script_neu.js"></script>' .chr(10);

$strJsFootCodeRun.= '

  $("#modalAddBgEvent").on("show.bs.modal", function(e) {
    $(".mofilter").val(window.clstr);
    console.log(window.clstr);
  });

  $("#modalEditBgEvent").on("show.bs.modal", function(e) {
    $(".mofilter").val(window.clstr);
    console.log(window.clstr);
  });

  $("#modalDeleteBgEvent").on("show.bs.modal", function(e) {
    $(".mofilter").val(window.clstr);
    console.log(window.clstr);
  });  

  $("#modalDuplicateBgEvent").on("show.bs.modal", function(e) {
    $(".mofilter").val(window.clstr);
    console.log(window.clstr);
  });  
  
  $("#doExport").on("click", function(e) {
  
    var table_hd = $($("table")[0]).html();
    var table_cth = $("table.table:eq(1) tr:visible");
    var table_ct = "";
    
    if (table_cth.length > 0) {
      $.each(table_cth, function(k, v) {
        table_ct += "<tr>" + $(v).html() + "</tr>";
      });
    }
    
    $("#strTable").val("<table>" + table_hd + table_ct + "</table>");
    $("#format").val("csv");
    $("#export").submit();
    
  });  
  
  $("#vbgdoc").on("click", function(e) {

    var table_hd = $($("table")[0]).html();
    var table_cth = $("table.table:eq(1) tr:visible");
    var table_ct = "";
    
    if (table_cth.length > 0) {
      $.each(table_cth, function(k, v) {
        table_ct += "<tr>" + $(v).html() + "</tr>";
      });
    }
    
    $("#strTable").val("<table>" + table_hd + table_ct + "</table>");
    $("#format").val("docx");
    $("#export").submit();

    /*
    var entryList = $(".dataTable tr:visible td:first-child").text().split("BG-").slice(1);
    if (entryList.length > 0) {
      $.each(entryList, function(k, v) {
        entryList[k] = parseInt(v, 10);
      });
    }

    var table_hd = $($("table")[0]).html();
    var table_cth = $("table.table:eq(1) tr:visible");
    var table_ct = "";
    
    if (table_cth.length > 0) {
      $.each(table_cth, function(k, v) {
        table_ct += "<tr>" + $(v).html() + "</tr>";
      });
    }

    $.ajax({
      url: "_get_csv.php",
      type: \'POST\',
      dataType: \'text\', 
      data: { bg: $( "#filter_bg option:selected" ).text(), strTable: table_ct, eventList: entryList, format: "docx" },
      success: function(result) {

        var jsonData = $.parseJSON(result);

        if (jsonData["Status"] == "OK") {
          
        }
        
        if (jsonData["Status"] == "FAIL") {

        }
        
      }
    });
    */

  });  

  $("#vbgxls").on("click", function(e) {
  
    var table_hd = $($("table")[0]).html();
    var table_cth = $("table.table:eq(1) tr:visible");
    var table_ct = "";
    
    if (table_cth.length > 0) {
      $.each(table_cth, function(k, v) {
        table_ct += "<tr>" + $(v).html() + "</tr>";
      });
    }
    
    $("#strTable").val("<table>" + table_hd + table_ct + "</table>");
    $("#format").val("xlsx");
    $("#export").submit();
    
  });  

  $("#resetFilter").on("click", function(e) {
  
    $("table.display tbody tr").show();

    $("select.filter option").each(function(i) {
      $(this).css("color", "#3f3f3f");
    });

    $("ul.select2-results__options li").css("color", "#3f3f3f");
    
    $("#DocCount").text($("table.display tbody tr:visible").length);
    $("#resetFilter").text("");
    
    $("select").val("");
      
  });

  $(".due").parent().parent().addClass("red");

  $("#resetFilter").hover(function() {
    $(this).css("cursor","pointer");
  });
  
  var $eventSelect = $(".filter");
  $eventSelect.on("select2:opening", function (e) { 
    var id = $(this).prop("id");
    setTimeout(function(){
      $("#select2-" + id +"-results li").each(function(i) {
        var str = $(this).prop("id").toString();
        if (str != "") {
          var tr = $("tr:visible td span.hfilter." + str.split("-")[4]);
          if (tr.length > 0) {
            $(this).css("color", "#3f3f3f");
          } else {
            $(this).css("color", "#a3afb7");
          }
        }
      });
    }, 1);
  });


  $(".filter").on("change", function(e) {
    
    var clstr = "span.hfilter";
    var fiid  = $(this).attr("id");
    var sval  = $(this).val();
    
    //if (uf !== null) {
    //  clstr = uf;
    //  uf = null;
    //} else {
      $("select.filter").each(function(i) {
        if ($(this).val() != "") clstr+= "." + $(this).val();
      });
    //}
    
    $("span.hfilter").parent().parent().hide();
    $(clstr).parent().parent().show();

    $("tr:hidden").each(function() {
        // Alle Checkboxen in den versteckten TRs finden und deaktivieren
        $(this).find(\'input[type="checkbox"]\').prop("checked", false);
    });

    window.clstr = clstr;
    
    var res = [];
    
    $("table.display tbody tr:visible td:first-child span").each(function(i) {
      res = res.concat($(this).attr("class").split(" ").filter(function (item) {
        return res.indexOf(item) < 0;
      }));
      return res;
    });
    
    $("#DocCount").text($("table.display tbody tr:visible").length);
    $("#resetFilter").text("alle zurücksetzen");

  });

  var url = new URL(location.href);
  var uf = url.searchParams.get("filter");
  if (uf !== null) {

    var fipa = uf.split(".");
    if (fipa.length > 2) {
      for (var i = 2; i < fipa.length; i++) {
        var v = fipa[i].split("_");
        $("#filter_" + v[0]).val(fipa[i]).trigger("change");
        console.log("#filter_" + v[0] + " -> " + fipa[i]);
      }
    }

  }

  setTimeout(function() { 
    if (uf === null) {
      //$("#filter_y").val("y_' .(date('Y') - 1) .'").trigger("change");
    }
  }, 500);  

  $("#modalEditBgEvent").on("hidden.bs.modal", function(e) {
      $(this).find(".modal-body").html("");
  });

  $("#modalDuplicateBgEvent").on("hidden.bs.modal", function(e) {
      $(this).find(".modal-body").html("");
  });

  $("#modalDuplicateBgEvent").on("show.bs.modal", function(e) {
      var link = "action/bgevent.ajax.php?ac=getModal&ty=duplicate&beid=" + $("#duplicateid").val();
      $(this).find(".modal-body").load(link);
      $(".mofilter").val(window.clstr);
  });

  $("#modalEditBgEvent").on("show.bs.modal", function(e) {
      var link = "action/bgevent.ajax.php?ac=getModal&ty=edit&beid=" + $("#editid").val();
      $(this).find(".modal-body").load(link);
      $(".mofilter").val(window.clstr);
  });

  (function() {
    $(\'#formBgEventDeleteSubmit\').on(\'click\', function(e) {
      // Prevent form submission
      e.preventDefault();

      var $form = $("#formBgEventDelete");

      console.log($form);

      // Use Ajax to submit form data
      $.ajax({
          url: $form.attr(\'action\'),
          type: \'POST\',
          dataType: \'text\', 
          data: $form.serialize(),
          success: function(result) {
  
            var jsonData = $.parseJSON(result);
            toastr.options = { "closeButton": true, "positionClass": "toast-top-full-width", "newestOnTop": true,   "showMethod": "fadeIn", "hideMethod": "fadeOut" }

            if (jsonData["Status"] == "OK") {
              
              toastr.success(\'Done.\');
              //aktualisieren
              if ((typeof jsonData["Reload"] != "undefined") && (jsonData["Reload"] === true)) {
                setTimeout(function() { location.reload(); }, 1000);
              }

            if (typeof jsonData["Goto"] != "undefined") {
              window.location.replace(jsonData["Goto"]);
            }
            }
            
            if (jsonData["Status"] == "FAIL") {
    
              if (jsonData["Reason"] == "login") {
                toastr.error(\'Your session expired!\');
                setTimeout(function() { handleLogout(); }, 2000);
              } else {
                toastr.error(\'An error occurred!\');
              }
    
            }
            
            $(\'.modal\').trigger(\'hide\');
            $(\'.modal\').modal(\'hide\');

          }
      });
  });  })();


  (function() {
    $(\'#formBgEventEditSubmit\').on(\'click\', function(e) {
      // Prevent form submission
      e.preventDefault();

      var $form = $("#formBgEventEdit");

      // Use Ajax to submit form data
      $.ajax({
          url: "action/bgevent.ajax.php",
          type: \'POST\',
          dataType: \'text\', 
          data: $form.serialize(),
          success: function(result) {
  
            var jsonData = $.parseJSON(result);
            toastr.options = { "closeButton": true, "positionClass": "toast-top-full-width", "newestOnTop": true,   "showMethod": "fadeIn", "hideMethod": "fadeOut" }

            if (jsonData["Status"] == "OK") {
              
              toastr.success(\'Done.\');
              //aktualisieren
              if ((typeof jsonData["Reload"] != "undefined") && (jsonData["Reload"] === true)) {
                if (jsonData["mofilter"] != "undefined") {
                  var url = window.location.href;
                  var reurl = url.split("&filter=")[0];
                  setTimeout(function() { window.location.href = reurl + "&filter=" + jsonData["mofilter"]; }, 1000);
                } else {
                  setTimeout(function() { location.reload(); }, 1000);
                }
              }

            if (typeof jsonData["Goto"] != "undefined") {
              window.location.replace(jsonData["Goto"]);
            }
            }
            
            if (jsonData["Status"] == "FAIL") {
    
              if (jsonData["Reason"] == "login") {
                toastr.error(\'Your session expired!\');
                setTimeout(function() { handleLogout(); }, 2000);
              } else {
                toastr.error(\'An error occurred!\');
              }
    
            }
            
            $(\'.modal\').trigger(\'hide\');
            $(\'.modal\').modal(\'hide\');

          }
      });
  });  })();


  (function() {
    $(\'#formBgEventDuplicateSubmit\').on(\'click\', function(e) {
      // Prevent form submission
      e.preventDefault();

      var $form = $("#formBgDuplicate");

      // Use Ajax to submit form data
      $.ajax({
          url: "action/bgevent.ajax.php",
          type: \'POST\',
          dataType: \'text\', 
          data: $form.serialize(),
          success: function(result) {
  
            var jsonData = $.parseJSON(result);
            toastr.options = { "closeButton": true, "positionClass": "toast-top-full-width", "newestOnTop": true,   "showMethod": "fadeIn", "hideMethod": "fadeOut" }

            if (jsonData["Status"] == "OK") {
              
              toastr.success(\'Done.\');
              //aktualisieren
              if ((typeof jsonData["Reload"] != "undefined") && (jsonData["Reload"] === true)) {
                if (jsonData["mofilter"] != "undefined") {
                  var url = window.location.href;
                  var reurl = url.split("&filter=")[0];
                  setTimeout(function() { window.location.href = reurl + "&filter=" + jsonData["mofilter"]; }, 1000);
                } else {
                  setTimeout(function() { location.reload(); }, 1000);
                }
              }

            if (typeof jsonData["Goto"] != "undefined") {
              window.location.replace(jsonData["Goto"]);
            }
            }
            
            if (jsonData["Status"] == "FAIL") {
    
              if (jsonData["Reason"] == "login") {
                toastr.error(\'Your session expired!\');
                setTimeout(function() { handleLogout(); }, 2000);
              } else {
                toastr.error(\'An error occurred!\');
              }
    
            }
            
            $(\'.modal\').trigger(\'hide\');
            $(\'.modal\').modal(\'hide\');

          }
      });
  });  })();

  (function() {
    $(\'#formBgEventDelete\').formValidation({
      framework: "bootstrap",
      button: {
        selector: \'#formBgEventDeleteSubmit\',
        disabled: \'disabled\',
        excluded: \':disabled\'
      },
      icon: {
        valid: \'fa-check\',
        invalid: \'fa-close\',
        validating: \'fa-refresh\'
      },
      err: {
          container: \'tooltip\'
      },
      fields: {

      }
    }).on(\'success.form.fv\', function(e) {
      // Prevent form submission
      e.preventDefault();

      var $form = $(e.target),
          fv    = $form.data(\'formValidation\');

      // Use Ajax to submit form data
      $.ajax({
          url: $form.attr(\'action\'),
          type: \'POST\',
          dataType: \'text\', 
          data: $form.serialize(),
          success: function(result) {
  
            var jsonData = $.parseJSON(result);
            toastr.options = { "closeButton": true, "positionClass": "toast-top-full-width", "newestOnTop": true,   "showMethod": "fadeIn", "hideMethod": "fadeOut" }

            if (jsonData["Status"] == "OK") {
              
              toastr.success(\'Done.\');
              //aktualisieren
              if ((typeof jsonData["Reload"] != "undefined") && (jsonData["Reload"] === true)) {
                if (jsonData["mofilter"] != "undefined") {
                  var url = window.location.href;
                  var reurl = url.split("&filter=")[0];
                  setTimeout(function() { window.location.href = reurl + "&filter=" + jsonData["mofilter"]; }, 1000);
                } else {
                  setTimeout(function() { location.reload(); }, 1000);
                }
              }

            if (typeof jsonData["Goto"] != "undefined") {
              window.location.replace(jsonData["Goto"]);
            }
            }
            
            if (jsonData["Status"] == "FAIL") {
    
              if (jsonData["Reason"] == "login") {
                toastr.error(\'Your session expired!\');
                setTimeout(function() { handleLogout(); }, 2000);
              } else {
                toastr.error(\'An error occurred!\');
              }
    
            }
            
            $(\'.modal\').trigger(\'hide\');
            $(\'.modal\').modal(\'hide\');

          }
      });
  });  })();


  // ------
  (function() {
    $(\'.EventContext\').contextmenu({
      target: \'#ContextMenu\',
      before: function(e) {
        //console.log($(e.target).parent().data("contextid"));
        e.preventDefault();
        return true;
      },
      onItem: function (context, e) {
        var action = $(e.target).data("action");
        var id = $(context).data("contextid");
        if (action == 3) {
          $("#deleteid").val(id);
          $("#modalDeleteBgEvent").modal("show");
        }
        if (action == 2) {
          $("#duplicateid").val(id);
          $("#modalDuplicateBgEvent").modal("show");
        }
        if (action == 1) {
          $("#editid").val(id);
          $("#modalEditBgEvent").modal("show");
        }
        console.log(action + " - " + id);
      }
    });
  })();


  $("#be_meldestelle").on("change", function(e) {

    if ($("#be_meldestelle").val() != "") {

      $.post("action/bgevent.ajax.php", { 
        ac: "getOptionsBg", be_meldestelle: $("#be_meldestelle").val()
      }).done(function(data) {
        var res =data.split("\n");
        $("#be_bg").find("option").remove();
        $("#be_bg").append(res[0]);
        $("#be_beitragsfaelligkeit").val(res[1]);
      });

      $(".bg").show();

    } else {
      $(".bg").hide();
    }

  });

  $(\'#modalAddBgEvent\').on(\'hide.bs.modal\', function (e) {
    if (e.namespace === \'bs.modal\') {
      rsForm(\'formBgEventAdd\');
    }
  });
    
  // Example Validataion Standard Mode
  // ---------------------------------
  (function() {
    $(\'#formBgEventAdd\').formValidation({
      framework: "bootstrap",
      button: {
        selector: \'#formBgEventAddSubmit\',
        disabled: \'disabled\',
        excluded: \':disabled\'
      },
      icon: {
        valid: \'fa-check\',
        invalid: \'fa-close\',
        validating: \'fa-refresh\'
      },
      err: {
          container: \'tooltip\'
      },
      fields: {

        be_jahr: {
          validators: {
            notEmpty: {
              message: \'This field is required and cannot be empty\'
            }
          }
        },
        be_meldestelle: {
          validators: {
            notEmpty: {
              message: \'This field is required and cannot be empty\'
            }
          }
        },
        be_bg: {
          validators: {
            notEmpty: {
              message: \'This field is required and cannot be empty\'
            }
          }
        }
      }
    }).on(\'success.form.fv\', function(e) {
      // Prevent form submission
      e.preventDefault();

      var $form = $(e.target),
          fv    = $form.data(\'formValidation\');

      // Use Ajax to submit form data
      $.ajax({
          url: $form.attr(\'action\'),
          type: \'POST\',
          dataType: \'text\', 
          data: $form.serialize(),
          success: function(result) {
  
            var jsonData = $.parseJSON(result);
            toastr.options = { "closeButton": true, "positionClass": "toast-top-full-width", "newestOnTop": true,   "showMethod": "fadeIn", "hideMethod": "fadeOut" }

            if (jsonData["Status"] == "OK") {
              
              //schreiben
              //$(\'#Tabs0 table\').DataTable().row.add( jsonData["TableData"] ).draw( false );
              //$(\'#Tabs0 table\').show();
              //$(\'.nav-tabs li:eq(0) a\').tab(\'show\');
              toastr.success(\'Done.\');
              //aktualisieren
              if ((typeof jsonData["Reload"] != "undefined") && (jsonData["Reload"] === true)) {
                if (jsonData["mofilter"] != "undefined") {
                  var url = window.location.href;
                  var reurl = url.split("&filter=")[0];
                  setTimeout(function() { window.location.href = reurl + "&filter=" + jsonData["mofilter"]; }, 1000);
                } else {
                  setTimeout(function() { location.reload(); }, 1000);
                }
              }

            if (typeof jsonData["Goto"] != "undefined") {
              window.location.replace(jsonData["Goto"]);
            }
            }
            
            if (jsonData["Status"] == "FAIL") {
    
              if (jsonData["Reason"] == "login") {
                toastr.error(\'Your session expired!\');
                setTimeout(function() { handleLogout(); }, 2000);
              } else {
                toastr.error(\'An error occurred!\');
              }
    
            }
            
            $(\'.modal\').trigger(\'hide\');
            $(\'.modal\').modal(\'hide\');

          }
      });
  });  })();

';

?>