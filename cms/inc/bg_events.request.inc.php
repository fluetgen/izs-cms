<?php

$strVolageOptionList = '';
$arrListAnschreiben = $objBgEvent->arrGetAnschreibenList(3, 'Anfrage BG');
if (count($arrListAnschreiben) > 0) {
  foreach ($arrListAnschreiben as $intKey => $arrAnschreiben) {
    $strVolageOptionList.= '  <option value="opt_' .$arrAnschreiben['ct_id'] .'">' .$arrAnschreiben['ct_name'] .'</option>' .chr(10);
  }
}

$arrTableHead = array(
  'Berufsgenossenschaft',
  'Meldestelle',
  'Anschreiben',
  'Vollmacht',
  'erzeugt am',
  'Anfrageliste',
  'xlsx',
  'erzeugt am',
  'Anfrageweg',
  'versandt am',
  'nur Klärungsfälle'
);

if ($strSf != 'bnr') { 
  unset($arrTableHead[1]);
}

$boolDebug = true;

//print_r($arrTableHead); die();

$arrFilter = array();


$arrList = array();
foreach ($arrGetBgList as $intRow => $arrBg) {

  if ($_SESSION['id'] == 3) {
    if ($arrBg['Id'] == '0013A00001VOh4UQAT') {
      //$arrBg['Anfrage_zum__c'].= ';02.12.';
    }

    //print_r($arrBg); die();

  } 

  $arrAnfrageList = array();

  if (($strSf == 'zentral') && ($arrBg['Anfrageart'] != '')) {
    continue;
  }
  if (($strSf == 'dezentral') && ($arrBg['Anfrageart'] != 'Dezentrale Anfrage')) {
    continue;
  }
  
  if (($strSf == 'bnr') && ($arrBg['Anfrageart'] != 'Anfrage nach BNR')) {
    continue;
  }

  if ($strSf == 'dezentral') {
    $strSql = 'SELECT * FROM `izs_dezentrale_anfrage` WHERE `Information_Provider__c` = "' .$arrBg['Id'] .'" ';
    $arrResult = MySQLStatic::Query($strSql);

    //print_r($arrResult); die();

    $arrAnfrageList = array();
    foreach ($arrResult as $intKey => $arrDez) {
      if (isset($arrAnfrageList[$arrDez['DezId']])) {
        $arrAnfrageList[$arrDez['DezId']]['Premium_Payer__c'][] = $arrDez['Premium_Payer__c'];
      } else {
        $arrDez['Premium_Payer__c'] = array($arrDez['Premium_Payer__c']);
        $arrAnfrageList[$arrDez['DezId']] = $arrDez;
      }
      
    }

  } elseif ($strSf == 'bnr') {

    $strSql = 'SELECT `Account`.`Name`, `Meldestelle__c` FROM `Verbindung_Meldestelle_BG__c` INNER JOIN `Account` ON `Meldestelle__c` = `Account`.`Id` ';
    $strSql.= 'WHERE `Berufsgenossenschaft__c` = "' .$arrBg['Id'] .'" AND `Verbindung_Meldestelle_BG__c`.`Aktiv__c` = "true"';
    $arrResult = MySQLStatic::Query($strSql);

    if ($_SESSION['id'] == 3) {
      //echo $strSql;
    }

    foreach ($arrResult as $intKey => $arrMeldestelle) {
      $arrAnfrageList[$arrMeldestelle['Meldestelle__c']] = array(
        'Id' => $arrBg['Id'],
        'DezId' => $arrMeldestelle['Meldestelle__c'],
        'Name' => $arrMeldestelle['Name'],
        'Premium_Payer__c' => array($arrMeldestelle['Meldestelle__c']),
        'Information_Provider__c' => $arrBg['Id']
      );
    }

  } else {

    $arrAnfrageList[$arrBg['Id']] = array(
      'Id' => $arrBg['Id'],
      'DezId' => '',
      'Name' => $arrBg['Name'],
      'Premium_Payer__c' => array(),
      'Information_Provider__c' => $arrBg['Id']
    );

  }

  if (($_SESSION['id'] == 3) && ($boolDebug)) {
    //print_r($arrAnfrageList);
    //print_r($arrGetBgList); 
    //die();
  }

  
  foreach ($arrAnfrageList as $strCheckId => $arrAnfragestelle) {

    if ($strSf == 'dezentral') {

      $arrFilter[0] = array(
        'strKey' => 'be_meldestelle',
        'strType' => 'IN',
        'strValue' => '(\'' .implode('\', \'', $arrAnfragestelle['Premium_Payer__c']) .'\')',
        'boolQuote' => false
      );

    } else if ($strSf == 'bnr') {

      $arrFilter[0] = array(
        'strKey' => 'be_meldestelle',
        'strType' => '=',
        'strValue' => $strCheckId
      );

    } else {
  
      $arrFilter[0] = array(
        'strKey' => 'be_bg',
        'strType' => '=',
        'strValue' => $strCheckId
      );

    }

    $arrFilter[1] = array(
      'strKey' => 'be_status_bearbeitung',
      'strType' => '=',
      'strValue' => 'erhalten - Klärungsfall'
    );


    $arrEventListKlaerung = $objBgEvent->arrGetBgEventList($arrFilter);
    
    unset($arrFilter[1]);

    $arrFilter[1] = array(
      'strKey' => 'be_status_bearbeitung',
      'strType' => '=',
      'strValue' => 'anzufragen'
    );

    if (($_SESSION['id'] == 3) && ($boolDebug)) {
      //$arrFilter[1]['strValue'] = 'veröffentlicht';
    }
    
    $arrEventListAnzufragen = $objBgEvent->arrGetBgEventList($arrFilter);
    
    unset($arrFilter[1]);

    $arrFilter[1] = array(
      'strKey' => 'be_status_bearbeitung',
      'strType' => '=',
      'strValue' => 'angefragt'
    );
    
    $arrEventListAngefragt = $objBgEvent->arrGetBgEventList($arrFilter);

    $strSqlBg = 'SELECT `BeitragsjahrBg__c` FROM `izs_bg` WHERE `Id` = "' .$arrBg['Id'] .'"';
    $arrSqlBg = MySQLStatic::Query($strSqlBg);
  
    if (count($arrSqlBg) > 0) {
      $intYear = date('Y') - $arrSqlBg[0]['BeitragsjahrBg__c'];
    } else {
      $strSqlAs = 'SELECT `Information_Provider__c`  FROM `Anfragestelle__c` WHERE `Id` = "' .$arrBg['Id'] .'"';
      $arrSqlAs = MySQLStatic::Query($strSqlAs);
  
      $strSqlBg = 'SELECT `BeitragsjahrBg__c` FROM `izs_bg` WHERE `Id` = "' .$arrSqlAs[0]['Information_Provider__c'] .'"';
      $arrSqlBg = MySQLStatic::Query($strSqlBg);    
    }
  
    $intYear = date('Y') - $arrSqlBg[0]['BeitragsjahrBg__c'];

    $strAdd = '';
    if ($strSf == 'bnr') {
      //$strCheckId .' -> ' .'' .$arrBg['Id'];
      $arrAuthLog = $objAuth->arrGetLog($arrBg['Id'], $intYear, $strCheckId);
      $arrReqLog = $objReq->arrGetLog($arrBg['Id'], $intYear, $strCheckId);
      $arrSendLog = $objSendReq->arrGetLog($arrBg['Id'], $intYear, $strCheckId);
      $strAdd = $arrBg['Id'] .'_';
    } else {
      $arrAuthLog = $objAuth->arrGetLog($strCheckId, $intYear);
      $arrReqLog = $objReq->arrGetLog($strCheckId, $intYear);
      $arrSendLog = $objSendReq->arrGetLog($strCheckId, $intYear);
    }
    
    $strAuthDate = '<span class="hidden" id="cvh_' .$strCheckId .'"></span><a id="cv_' .$strCheckId .'" href="" target="_blank"></a>';
    if (count($arrAuthLog) > 0) {
      $strAuthDate = '<span class="hidden" id="cvh_' .$strCheckId .'">' .strConvertDate($arrAuthLog[0]['ca_created'], 'Y-m-d H:i:s', 'YmdHis') .'</span><a id="cv_' .$strCheckId .'" href="/cms/pdf/vollmacht/bg/' .$strAdd .$strCheckId .'_vollmacht.pdf?' .substr(md5(uniqid(mt_rand(), true)), 0, 4) .'" target="_blank" title="' .strConvertDate($arrAuthLog[0]['ca_created'], 'Y-m-d H:i:s', 'd.m.Y H:i:s') .'">' .strConvertDate($arrAuthLog[0]['ca_created'], 'Y-m-d H:i:s', 'd.m.Y') .'</a>';
    }
    
    $strReqDate = '<span class="hidden" id="cah_' .$strCheckId .'"></span><a id="ca_' .$strCheckId .'" href="" target="_blank"></a>';
    if (count($arrReqLog) > 0) {
      $strReqDate = '<span class="hidden" id="cah_' .$strCheckId .'">' .strConvertDate($arrReqLog[0]['cr_created'], 'Y-m-d H:i:s', 'YmdHis') .'</span><a id="ca_' .$strCheckId .'" href="/cms/pdf/anfrage/bg/' .$strAdd .$strCheckId .'_anfrage.pdf?' .substr(md5(uniqid(mt_rand(), true)), 0, 4) .'" target="_blank" title="' .strConvertDate($arrReqLog[0]['cr_created'], 'Y-m-d H:i:s', 'd.m.Y H:i:s') .'">' .strConvertDate($arrReqLog[0]['cr_created'], 'Y-m-d H:i:s', 'd.m.Y') .'</a>';
    }
    
    //$strSendAction = '<a id="sr_' .$strCheckId .'" class="sAnfrage">auf ANGEFRAGT setzen</a>';
    $strSendDate = '<span class="hidden" id="sdh_' .$strCheckId .'"></span><span id="sd_' .$strCheckId .'"></span>';
    if (count($arrSendLog) > 0) {
      //$strSendAction = 'angefragt';
      $strSendDate = '<span class="hidden" id="sdh_' .$strCheckId .'">' .strConvertDate($arrSendLog[0]['cs_sent'], 'Y-m-d H:i:s', 'YmdHis') .'</span><span id="sd_' .$strCheckId .'" title="' .strConvertDate($arrSendLog[0]['cs_sent'], 'Y-m-d H:i:s', 'd.m.Y H:i:s') .'">' .strConvertDate($arrSendLog[0]['cs_sent'], 'Y-m-d H:i:s', 'd.m.Y') .'</span>';
    }
    //$strSendAction = '';

    $strXlsxDate = '<span class="hidden" id="cah_' .$strCheckId .'"></span><a id="cx_' .$strCheckId .'" href="" target="_blank"></a>';
    if (count($arrReqLog) > 0) {
      $strXlsxDate = '<span class="hidden" id="cah_' .$strCheckId .'">' .strConvertDate($arrReqLog[0]['cr_created'], 'Y-m-d H:i:s', 'YmdHis') .'</span><a id="cx_' .$strCheckId .'" href="/cms/pdf/anfrage/bg/' .$strCheckId .'_anfrage.xlsx?' .substr(md5(uniqid(mt_rand(), true)), 0, 4) .'" target="_blank" title="' .strConvertDate($arrReqLog[0]['cr_created'], 'Y-m-d H:i:s', 'd.m.Y H:i:s') .'">' .strConvertDate($arrReqLog[0]['cr_created'], 'Y-m-d H:i:s', 'd.m.Y') .'</a>';
    }

    //if ((count($arrEventListKlaerung) > 0) || (count($arrEventListFrist)) && (0)) {
    if ((count($arrEventListAnzufragen) > 0) || (count($arrEventListAngefragt) > 0) || (count($arrEventListKlaerung) > 0)) {

      $strRef = '';
      $strBgName = $arrAnfragestelle['Name'];
      if ($strSf == 'bnr') {
        $strBgName = $arrBg['Name'];
        $strRef = $arrBg['Id'];
      }
      
      $arrEntry = array(
        'Berufsgenossenschaft' => '<a href="/cms/index_neu.php?ac=contacts&acid=' .$arrBg['Id'] .'" target="_blank">' .$strBgName .'</a>',
        'Meldestelle' => $arrAnfragestelle['Name'],
        'Anschreiben' => '<i id="d_' .$strCheckId .'" class="icon fa-download cVorlage" aria-hidden="true" ref="' .$strRef .'"></i>',
        'Vollmacht' => '<a id="v_' .$strCheckId .'" class="cVollmacht" ref="' .$strRef .'">erzeugen</a>',
        'erzeugt am 1' => $strAuthDate, 
        'Anfrage' => '<a id="a_' .$strCheckId .'" class="cAnfrage" ref="' .$strRef .'">erzeugen</a>', 
        'xlsx' => $strXlsxDate,
        'erzeugt am 2' => $strReqDate,
        'Anfrageweg' => '<a id="sr_' .$strCheckId .'" class="sAnfrage" ref="' .$strRef .'">' .$arrBg['Anfrageweg'] .'</a>',
        //'Status Bearbeitung' => $strSendAction,
        'versandt am' => $strSendDate,
        'Klärungsfälle' => '(' .count($arrEventListKlaerung) .')'//,
        //'Befristungen' => count($arrEventListFrist)
      );

      if ($strSf != 'bnr') {
        unset($arrEntry['Meldestelle']);
      }

      $arrList[] = $arrEntry;
      
    } 

  }

}

$strBgTable = strCreateTable($arrList, $arrTableHead, '', false, true);

$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Berufsgenossenschaften gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
            <input type="hidden" name="year" id="year" value="' .$intYear .'">
        </div>
        <div class="page-content">
';

if (count($arrList) > 0) {
  $strIncOutput = '<div style="margin-bottom: 10px;">Textvorlage: <select id="vorlage" class="sVorlage">' .chr(10);
  $strIncOutput.= $strVolageOptionList;
  $strIncOutput.= '</select></div>';
}

$strIncOutput.= $strBgTable;

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';



?>