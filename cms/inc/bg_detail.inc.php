<?php

//CLASSES
require_once(APP_PATH .'cms/classes/class.user.php');
require_once(APP_PATH .'assets/classes/class.database.php');

require_once(APP_PATH .'cms/classes/class.stream.php');
require_once(APP_PATH .'cms/classes/class.request.php');
require_once(APP_PATH .'cms/classes/class.sendrequest.php');

require_once(APP_PATH .'cms/functions.inc.php');

require_once(APP_PATH .'cms/classes/class.bgevent.php');
require_once(APP_PATH .'cms/classes/class.bg.php');

require_once(APP_PATH .'cms/classes/class.premiumpayer.php');


//REQUEST
$intBeId = @$_REQUEST['beid'];
$arrPostData = @$_POST;

//DATA

$objBg = new Bg;
$arrBgList = $objBg->arrGetBgList();

$objPp = new PremiumPayer;

//SETTINGS

$boolShowHead = false;
$boolShowSide = false;

//OBJECTS

$objStream   = new Stream('basic');
$objUser     = new User;

$objRequest     = new Request;
$objSendRequest = new SendRequest;

$objBgEvent  = new BgEvent;
$objBg       = new Bg;

$objPp = new PremiumPayer;

$strOutput.= '';
$strNote   = '';

//FUNCTIONS
if (is_array($arrPostData) && (count($arrPostData) > 0)) {
  
  //print_r($arrPostData);


  if ($arrPostData['da_' .$intBeId] == 'Dynamische Erzeugung') {
    $arrPostData['do_' .$intBeId] = '';
    $arrPostData['sp_' .$intBeId] = 0;
    $arrPostData['si_' .$intBeId] = 'sichtbar';
  }
  
  //Datumsformat
  if ($arrPostData['wi_' .$intBeId] == '') {
    $arrPostData['wi_' .$intBeId] = '0000-00-00';
  } else {
    $arrPostData['wi_' .$intBeId] = strConvertDate($arrPostData['wi_' .$intBeId], 'd.m.Y', 'Y-m-d');
  }

  if ($arrPostData['be_' .$intBeId] == '') {
    $arrPostData['be_' .$intBeId] = '0000-00-00';
  } else {
    $arrPostData['be_' .$intBeId] = strConvertDate($arrPostData['be_' .$intBeId], 'd.m.Y', 'Y-m-d');
  }

  if ($arrPostData['dr_' .$intBeId] == '') {
    $arrPostData['dr_' .$intBeId] = '0000-00-00';
  } else {
    $arrPostData['dr_' .$intBeId] = strConvertDate($arrPostData['dr_' .$intBeId], 'd.m.Y', 'Y-m-d');
  }
  
  if ($arrPostData['bf_' .$intBeId] == '') {
    $arrPostData['bf_' .$intBeId] = '0000-00-00';
  } else {
    $arrPostData['bf_' .$intBeId] = strConvertDate($arrPostData['bf_' .$intBeId], 'd.m.Y', 'Y-m-d');
  }

  if (empty($arrPostData['sp_' .$intBeId])) {
    $arrPostData['sp_' .$intBeId] = 0;
  }


  $arrFields = array(
    "be_ergebnis"             => $arrPostData['er_' .$intBeId], 
    "be_auskunftsgeber"       => $arrPostData['ag_' .$intBeId], 
    "be_sperrvermerk"         => $arrPostData['sp_' .$intBeId], 
    "be_befristet"            => $arrPostData['be_' .$intBeId], 
    "be_bemerkung"            => $arrPostData['bs_' .$intBeId], 
    "be_status_bearbeitung"   => $arrPostData['sb_' .$intBeId], 
    "be_sichtbar"             => $arrPostData['si_' .$intBeId], 
    "be_info_intern"          => $arrPostData['ii_' .$intBeId], 
    "be_wiedervorlage"        => $arrPostData['wi_' .$intBeId], 
    "be_datum_rueckmeldung"   => $arrPostData['dr_' .$intBeId], 
    "be_klaerung_status"      => $arrPostData['sk_' .$intBeId], 
    "be_klaerung_grund"       => $arrPostData['gr_' .$intBeId], 
    "be_rueckstand"           => $arrPostData['rr_' .$intBeId], 
    "be_klaerung_schritt"     => $arrPostData['ns_' .$intBeId], 
    "be_klaerung_ap"          => $arrPostData['ap_' .$intBeId], 
    "be_klaerung_telefon"     => $arrPostData['at_' .$intBeId],
    "be_dokument_art"         => $arrPostData['da_' .$intBeId],
    "be_stundung"             => $arrPostData['st_' .$intBeId],
    "be_bearbeiter"           => $arrPostData['us_' .$intBeId],
    "be_meldepflicht"         => $arrPostData['me_' .$intBeId],
    "be_beitragsfaelligkeit"  => $arrPostData['bf_' .$intBeId]
  );
  
  if ($intAdmin) {
    $arrFields['be_jahr'] = $arrPostData['ja_' .$intBeId];
    $arrFields['be_name_meldestelle'] = $arrPostData['mel_' .$intBeId];
    $arrFields['be_mitgliedsnummer']  = $arrPostData['nr_' .$intBeId];
    $arrFields['be_unternehmensnummer']  = $arrPostData['unr_' .$intBeId];
  }
  
  $boolChange = $objBgEvent->boolChangeValueList($arrFields, $intBeId);
  
  $arrBgEvent = $objBgEvent->arrGetBgEvent($intBeId);
  
  $strNote = '
                <div class="alert dark alert-alt alert-success alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  Event erfolgreich gespeichert.
                </div>
  ';
  
}


//CONTENT

$arrUser = array();
$strSql = 'SELECT `cl_id`, `cl_first`, `cl_last` FROM `cms_login` ORDER BY `cl_id`';
$resSql = MySQLStatic::Query($strSql);
if (is_array($resSql) && (count($resSql) > 0)) {
  foreach ($resSql as $intLine => $arrUserData) {
    $arrUser[$arrUserData['cl_id']] = $arrUserData['cl_first'] .' ' .$arrUserData['cl_last'];
  }
}


$arrStreamType = array (
  0 => 'other', 
  1 => 'create', 
  2 => 'update', 
  3 => 'message',
  4 => 'mail',
  5 => 'delete',
  6 => 'duplicate'
);

$arrStreamClass = array (
  1 => 'system',
  2 => 'system', 
  3 => 'notiz',
  4 => 'file',
  5 => 'mail',
  5 => 'system',
  6 => 'system'
);

$arrSystemStream = array (
  'beid' => '<b>duplizierte</b> Event',
  'be_auskunftsgeber' => 'änderte <b>Auskunftsgeber</b>',
  'be_befristet' => 'änderte <b>Befristung</b>',
  'be_bg' => 'änderte <b>Berufsgenossenschaft</b>',
  'be_bemerkung' => 'änderte <b>Bemerkung</b>',
  'be_datum_rueckmeldung' => 'änderte <b>Rückmelde-Datum</b>',
  'be_status_anfrage' => 'änderte <b>Status Anfrage</b>',
  'be_dokument_art' => 'änderte <b>Dokumentart</b>',
  'be_ergebnis' => 'änderte <b>Ergebnis</b>',
  'be_info_intern' => 'änderte <b>interne Info</b>',
  'be_jahr' => 'änderte <b>Jahr</b>',
  'be_klaerung_ap' => 'änderte <b>Ansprechpartner Klärung</b>',
  'be_klaerung_grund' => 'änderte <b>Klärungsgrund</b>',
  'be_klaerung_schritt' => 'änderte <b>Klärung nä. Schritt</b>',
  'be_klaerung_status' => 'änderte <b>Klärungsstatus</b>',
  'be_klaerung_telefon' => 'änderte <b>Klärung-Telefonnummer</b>',
  'be_link' => 'änderte <b>Dokumentenlink</b>',
  'be_meldestelle' => 'änderte <b>Meldestelle</b>',
  'be_rueckstand' => 'änderte <b>Rückstand</b>',
  'be_sichtbar' => 'änderte <b>Sichtbarkeit</b>',
  'be_sperrvermerk' => 'änderte <b>Sperrvermerk</b>',
  'be_status_bearbeitung' => 'änderte <b>Bearbeitsungstatus</b>',
  'be_wiedervorlage' => 'änderte <b>Wiedervorlage-Datum</b>',
  'be_name_meldestelle' => 'änderte den <b>Namen der Meldestelle</b>',
  'be_mitgliedsnummer' => 'änderte die <b>Mitgliedsnummer</b>',
  'be_unternehmensnummer' => 'änderte die <b>Unternehmensnummer</b>',
  'be_name_bg' => 'änderte den <b>Namen der Berufsgenossenschaft</b>',
  'be_verbindung' => 'änderte die <b>Id der Verbindung</b>',
  'be_stundung' => 'änderte <b>Stundung / Ratenzahlung</b>',
  'be_bearbeiter' => 'änderte die/den <b>Bearbeiter:in</b>',
  'be_meldepflicht' => 'änderte <b>Meldepflicht erfüllt</b>',
  'be_beitragsfaelligkeit' => 'änderte die <b>Beitragsfälligkeit</b>',
); 

$strActivityStream = '';
$strActivityStreamAside = '';

$strSql = 'SELECT * FROM `izs_bgevent_change` WHERE `ev_id` = "' .$intBeId .'" ORDER BY `ec_time` DESC, `ec_id` DESC';
$arrSql = MySQLStatic::Query($strSql);

if ($_SESSION['id'] == 3) {
  //echo $strSql;
}

if (count($arrSql) > 0) {

  $intAct = 1;

  foreach ($arrSql as $inKey => $arrActitity) {

    $strActivities = '';
    
    $strClassAdd = ' even';
    if ($intAct % 2 != 0) {
      $strClassAdd = ' odd';
    }

    $intTime = strtotime($arrActitity['ec_time']);

    if (in_array($arrActitity['ec_fild'], array('be_datum_rueckmeldung', 'be_wiedervorlage' ,'be_befristet')) ) {
      if (($arrActitity['ec_new'] != '') && ($arrActitity['ec_new'] != '0000-00-00')) {
        $arrActitity['ec_new'] = date('d.m.Y', strtotime($arrActitity['ec_new']));
      } else {
        $arrActitity['ec_new'] = '';
      }
      if (($arrActitity['ec_old'] != '') && ($arrActitity['ec_old'] != '0000-00-00')) {
        $arrActitity['ec_old'] = date('d.m.Y', strtotime($arrActitity['ec_old']));
      } else {
        $arrActitity['ec_old'] = '';
      }      
    }

    if ($arrActitity['ec_fild'] == 'be_bg') {
      if ($arrActitity['ec_old'] != '') {
        $arrBgActivity = $objBg->arrGetBg($arrActitity['ec_old']);
        $arrActitity['ec_old'] = $arrBgActivity[0]['ac_name'];
      }
      if ($arrActitity['ec_new'] != '') {
        $arrBgActivity = $objBg->arrGetBg($arrActitity['ec_new']);
        $arrActitity['ec_new'] = $arrBgActivity[0]['ac_name'];
      }      
    }

    if ($arrActitity['ec_fild'] == 'be_meldestelle') {
      if ($arrActitity['ec_old'] != '') {
        $arrMeldestelleActivity = $objPp->arrGetPP($arrActitity['ec_old'], false);
        $arrActitity['ec_old'] = $arrMeldestelleActivity[0]['ac_name'];
      }
      if ($arrActitity['ec_new'] != '') {
        $arrMeldestelleActivity = $objPp->arrGetPP($arrActitity['ec_new'], false);
        $arrActitity['ec_new'] = $arrMeldestelleActivity[0]['ac_name'];
      }  
    }


    if ($arrActitity['ec_type'] == 1) {
      $strActivities.= '
      <div class="stream-line ' .$arrStreamType[$arrActitity['ec_type']] .' ' .$arrStreamClass[$arrActitity['ec_type']] .$strClassAdd .'">
        <span class="stream-head">' .@$arrUser[$arrActitity['cl_id']] .' - am ' .date('d.m.Y', $intTime) .' / ' .date('H:i:s', $intTime) .' Uhr</span>
        <span><b>erzeugte</b> Event</span>
      </div>
      ';
    } elseif ($arrActitity['ec_type'] == 5) {

      $arrMessage = unserialize($arrActitity['ec_new']);

      $strActivities.= '
      <div class="stream-line ' .$arrStreamType[$arrActitity['ec_type']] .' ' .$arrStreamClass[$arrActitity['ec_type']] .$strClassAdd .'">
        <span class="stream-head">' .$arrUser[$arrActitity['cl_id']] .' - am ' .date('d.m.Y', $intTime) .' / ' .date('H:i:s', $intTime) .' Uhr</span>
        <span>sendete folgende Nachricht: </span><br />
        <p><strong>' .$arrMessage['subject'] .'</strong></p>
        <p>' .str_replace('salutToReplace', '', $arrMessage['message']) .'</p>
        <p><strong>Empfänger:</strong><br />
        An: ' .$arrMessage['to'] .'<br />
        CC: ' .$arrMessage['cc'] .'<br />
        BCC: ' .$arrMessage['bcc'] .'<br />
      </div>
      ';
    } elseif ($arrActitity['ec_type'] == 6) {

      $arrDuplicate = $objBgEvent->arrGetBgEvent($arrActitity['ec_old']);

      $strActivities.= '
      <div class="stream-line ' .$arrStreamType[$arrActitity['ec_type']] .' ' .$arrStreamClass[$arrActitity['ec_type']] .$strClassAdd .'">
        <span class="stream-head">' .$arrUser[$arrActitity['cl_id']] .' - am ' .date('d.m.Y', $intTime) .' / ' .date('H:i:s', $intTime) .' Uhr</span>
        <span><b>duplizierte</b> Event aus: "' .$arrDuplicate[0]['be_name'] .'"</span>
      </div>
      ';
    } else {
      $strActivities.= '
      <div class="stream-line ' .$arrStreamType[$arrActitity['ec_type']] .' ' .$arrStreamClass[$arrActitity['ec_type']] .$strClassAdd .'">
        <span class="stream-head">' .@$arrUser[@$arrActitity['cl_id']] .' - am ' .date('d.m.Y', $intTime) .' / ' .date('H:i:s', $intTime) .' Uhr</span>
        <span>' .$arrSystemStream[$arrActitity['ec_fild']] .'<br>neu: "' .$arrActitity['ec_new'] .'"<br>alt: "' .$arrActitity['ec_old'] .'"</span>
      </div>
      ';
    }

    $strActivityStream.= $strActivities;

    if (($arrActitity['ec_type'] >= 3) && ($arrActitity['ec_type'] <= 5)) {
      $strActivityStreamAside.= $strActivities;
    }

    $intAct++;

  }

}

$arrEmpty = array();
$arrBgEvent = $objBgEvent->arrGetBgEvent($intBeId);


$boolUiOld = true;
if ($arrBgEvent[0]['be_dokument_art'] == 'Dynamische Erzeugung') {
  $boolUiOld = false;
}




$strMitglNummer = $arrBgEvent[0]['be_mitgliedsnummer'];
$strUnterNummer = $arrBgEvent[0]['be_unternehmensnummer'];
$arrBg = $objBg->arrGetBg($arrBgEvent[0]['be_bg']);
$strMeldestelleName = $arrBgEvent[0]['be_name_meldestelle'];


$strOutput = '
  <!-- Page -->
  <div class="animsition container-fluid">
';

$strPageTitle = 'Event-Details' .' (' .$arrBgEvent[0]['be_name'] .')';
$strSubheadline = '' .$arrBgEvent[0]['be_jahr'] .' - BG: ' .$arrBg[0]['ac_name'] .' - ' .@$strMeldestelleName .' (' .$strMitglNummer .')';
if ($strUnterNummer != '') $strSubheadline.= '<br>Unternehmensnummer: ' .$strUnterNummer;

$strOutput.= strCreatePageTitle($strPageTitle, $strSubheadline);

if (1) {
  
/*
    "`be_datum`", ?
*/

  $strMeldestelle = '';
  $strMeldestelle.= strGetFormElement($arrEmpty, $arrBgEvent[0]['be_name_meldestelle'], 'text', 'mel_' .$intBeId, true, ' class="moretxt"');

  $strMitgliedsnummer = '';
  $strMitgliedsnummer.= strGetFormElement($arrEmpty, $arrBgEvent[0]['be_mitgliedsnummer'], 'text', 'nr_' .$intBeId, true, ' class="moretxt"');

  $strUnternehmensnummer = '';
  $strUnternehmensnummer.= strGetFormElement($arrEmpty, $arrBgEvent[0]['be_unternehmensnummer'], 'text', 'unr_' .$intBeId, true, ' class="moretxt"');

  $strBearbeiter = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_bearbeiter');
  $strBearbeiter.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_bearbeiter'], 'select', 'us_' .$intBeId, true, ' class=""', true);

  $strDokument = '';
  if ($arrBgEvent[0]['be_link'] != '') {
    $strLink = '' .$arrBgEvent[0]['be_link'] .'';
    $strDokument.= '<i aria-hidden="true" class="icon fa-remove" id="deldoc"></i> <a href="pdf/bg/' .$strLink .'" target="_blank">' .$arrBgEvent[0]['be_link'] .'</a>';
  }

  $strDokumentArt = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_dokument_art');
  $strDokumentArt.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_dokument_art'], 'select', 'da_' .$intBeId, true, ' class=""');
  
  
  $strStatusBearbeitung = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_status_bearbeitung');
  $strStatusBearbeitung.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_status_bearbeitung'], 'select', 'sb_' .$intBeId, true, ' class=""');
  
  $strDate = '';
  if (($arrBgEvent[0]['be_datum_rueckmeldung'] != '') && ($arrBgEvent[0]['be_datum_rueckmeldung'] != '0000-00-00')) {
    $strDate = strConvertDate($arrBgEvent[0]['be_datum_rueckmeldung'], 'Y-m-d', 'd.m.Y');
  }
  $strDatumRueck = '';
  $strDatumRueck.= '
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="icon wb-calendar" aria-hidden="true"></i>
                    </span>
                    <input name="dr_' .$intBeId .'" id="dr_' .$intBeId .'" type="text" class="form-control dp" data-plugin="datepicker" value="' .$strDate .'" data-date-format="dd.mm.yyyy" style="width: 120px;">
                  </div>
  ';

  $strDate = '';
  if (($arrBgEvent[0]['be_beitragsfaelligkeit'] != '') && ($arrBgEvent[0]['be_beitragsfaelligkeit'] != '0000-00-00')) {
    $strDate = strConvertDate($arrBgEvent[0]['be_beitragsfaelligkeit'], 'Y-m-d', 'd.m.Y');
  }
  $strDatumFaellig = '';
  $strDatumFaellig.= '
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="icon wb-calendar" aria-hidden="true"></i>
                    </span>
                    <input name="bf_' .$intBeId .'" id="bf_' .$intBeId .'" type="text" class="form-control dp" data-plugin="datepicker" value="' .$strDate .'" data-date-format="dd.mm.yyyy" style="width: 120px;">
                  </div>
  ';

  $strAuskunftsGeber = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_auskunftsgeber');
  $strAuskunftsGeber.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_auskunftsgeber'], 'select', 'ag_' .$intBeId, true, ' class=""');
  
  $strErgebnis = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_ergebnis');

  if ($boolUiOld === false) {
    if (($intKey = array_search('OK (mit RV)', $arrSelValues)) !== false) {
      unset($arrSelValues[$intKey]);
    }
    if (($intKey = array_search('gestundet', $arrSelValues)) !== false) {
      unset($arrSelValues[$intKey]);
    }
  }

  $strErgebnis.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_ergebnis'], 'select', 'er_' .$intBeId, true, ' class=""');

  $strSperrvermerk = '';
  $arrSelValues = array('ja' => 1);
  $strSperrvermerk.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_sperrvermerk'], 'checkbox', 'sp_' .$intBeId, true, ' class=""');

  $strBemerkungSichtbar = '';
  $strBemerkungSichtbar.= strGetFormElement($arrEmpty, $arrBgEvent[0]['be_bemerkung'], 'textarea', 'bs_' .$intBeId, true, ' class=""');
  
  $strSichtbarkeit = '';
  if ($arrBgEvent[0]['be_sichtbar'] == '') {
    //$arrBgEvent[0]['be_sichtbar'] = 'sichtbar';
  }
  $arrSelValues = $objBgEvent->arrGetSelection('be_sichtbar');
  $strSichtbarkeit.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_sichtbar'], 'select', 'si_' .$intBeId, true, ' class=""');
  
  $strFristDate = '';
  if (($arrBgEvent[0]['be_befristet'] != '') && ($arrBgEvent[0]['be_befristet'] != '0000-00-00')) {
    $strFristDate = strConvertDate($arrBgEvent[0]['be_befristet'], 'Y-m-d', 'd.m.Y');
  }
  
  $strBefristet = '';
  $strBefristet.= '
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="icon wb-calendar" aria-hidden="true"></i>
                    </span>
                    <input name="be_' .$intBeId .'" id="be_' .$intBeId .'" type="text" class="form-control dp" data-plugin="datepicker" value="' .$strFristDate .'" data-date-format="dd.mm.yyyy" style="width: 120px;">
                  </div>
  ';
  
  $strStatusKlaerung = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_klaerung_status');
  $strStatusKlaerung.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_klaerung_status'], 'select', 'sk_' .$intBeId, true, ' class=""');
  
  $strRueckstand = '';
  $strRueckstand.= strGetFormElement($arrEmpty, $arrBgEvent[0]['be_rueckstand'], 'text', 'rr_' .$intBeId, true, ' class=""');

  $strGrund = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_klaerung_grund');
  $strGrund.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_klaerung_grund'], 'select', 'gr_' .$intBeId, true, ' class=""');
  
  $strSchritt = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_klaerung_schritt');
  $strSchritt.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_klaerung_schritt'], 'select', 'ns_' .$intBeId, true, ' class=""');

  $strAp = '';
  $strAp.= strGetFormElement($arrEmpty, $arrBgEvent[0]['be_klaerung_ap'], 'text', 'ap_' .$intBeId, true, ' class=""');

  $strTel = '';
  $strTel.= strGetFormElement($arrEmpty, $arrBgEvent[0]['be_klaerung_telefon'], 'text', 'at_' .$intBeId, true, ' class=""');

  $strStundung = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_stundung');
  $strStundung.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_stundung'], 'select', 'st_' .$intBeId, true, ' class=""');

  $strMeldepflicht = '';
  $arrSelValues = $objBgEvent->arrGetSelection('be_meldepflicht');
  $strMeldepflicht.= strGetFormElement($arrSelValues, $arrBgEvent[0]['be_meldepflicht'], 'select', 'me_' .$intBeId, false, ' class=""');

  
  $strValueWiedervorlage = '';
  if (($arrBgEvent[0]['be_wiedervorlage'] != '') && ($arrBgEvent[0]['be_wiedervorlage'] != '0000-00-00')) {
    $strValueWiedervorlage = strConvertDate($arrBgEvent[0]['be_wiedervorlage'], 'Y-m-d', 'd.m.Y');
  }
  $strWieder = '';
  $strWieder.= '
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="icon wb-calendar" aria-hidden="true"></i>
                    </span>
                    <input name="wi_' .$intBeId .'" id="wi_' .$intBeId .'" type="text" class="form-control dp" data-plugin="datepicker" value="' .$strValueWiedervorlage .'" data-date-format="dd.mm.yyyy" style="width: 120px;">
                  </div>
  ';
  
  $strInfoIntern = '';
  $strInfoIntern.= strGetFormElement($arrEmpty, $arrBgEvent[0]['be_info_intern'], 'textarea', 'ii_' .$intBeId, true, ' class=""');
  

  $strDataName = $strIChatPath .'/BG/' .$arrBgEvent[0]['be_jahr'] .'/' .$arrBg[0]['ac_name'] .'/' .$arrBgEvent[0]['be_name'] ; //IZS/BG/2015/VBG - Körperschaft des öffentlichen Rechts/BG-000310


  $strModal = '';
  $strModal.= '

  <div class="col-lg-66" style="margin: 20px 0px; text-align: right;">
    <form id="selectTemplate">
    <input type="hidden" name="beid" value="' .$intBeId .'">
    <select id="ct_id_select" name="ct_id_select">
      <option value=""> - Vorlage auswählen - </option>' .chr(10);

$strSql = 'SELECT * FROM `cms_text` WHERE `ct_type` = "3" ORDER BY `ct_name`';
$resSql = MySQLStatic::Query($strSql);
if (count($resSql) > 0) {
  foreach ($resSql as $intKey => $arrTemplate) {
    $strModal.= '      <option value="' .$arrTemplate['ct_id'] .'">' .$arrTemplate['ct_name'] .'</option>' .chr(10);
  }
}

$strModal.= '
      </select><button style="margin-left: 10px;" id="editMail">E-Mail senden</button>
    </form>
  </div>
  
  <div class="col-lg-66" style="margin-bottom: 20px; float: left;">
  

                  <div class="nav-tabs-horizontal" style="margin-top: 20px;">
                  <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                    <li class="active" role="presentation"><a data-toggle="tab" href="#exampleTabsOne" aria-controls="exampleTabsOne"
                      role="tab">Stammdaten</a></li>
                    <li role="presentation"><a data-toggle="tab" href="#exampleTabsTwo" aria-controls="exampleTabsTwo"
                      role="tab"><span>Chats<span smartchat data-type="MyCustomIndicatorComponent" data-uri="' .$strIChatUri .'cms/index_neu.php?ac=bg_detail&beid=' .$intBeId .'"></span></span></a></li>
                    <li role="presentation"><a data-toggle="tab" href="#exampleTabsThree" aria-controls="exampleTabsThree"
                      role="tab">Activity Stream</a></li>
                  </ul>
                  <div class="tab-content" style="width: 1200px;">
                    <div class="tab-pane active" id="exampleTabsOne" role="tabpanel" style="min-height: 450px; background-color: #fff;">

  <form action="" method="post"> 
    <input type="hidden" name="beid" value="' .$intBeId .'">

    <div class="panel" style="">
      <div class="panel-body">

          
  ' .$strNote .'

        <div class="row">
          <div class="col-lg-8">
          
            <table class="" style="width: 100%">
               <thead>
              </thead>
              <tbody>
                <tr>
                  <th class="text-nowrap"><label for="ii_' .$intBeId .'">Hinweis:</label></th>
                </tr>
                <tr>
                  <td>' .$strInfoIntern .'</td>
                </tr>
              </tbody>
            </table>
    
          </div>  <!-- COL1 -->

          <div class="col-lg-4">
          
            <table class="" style="width: 100%">
              <thead>
              </thead>
              <tbody>
                <tr>
                  <th class="text-nowrap"><label for="wi_' .$intBeId .'">Wiedervorlage:</label></th>
                </tr>
                <tr>
                  <td>' .$strWieder .'</td>
                </tr>
              </tbody>
            </table>
    
          </div>  <!-- COL2 -->
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

if ($intAdmin) {

$strModal.= '
    <div class="panel" style="">
      <header class="panel-heading">
        <h3 class="panel-title">Stammdaten (nur Admin-Zugriff)</h3>
      </header>
      <div class="panel-body">

        <div class="row">
          <div class="col-lg-12">
          
          <table class="table table-striped">
            <colgroup>
              <col width="20%">
              <col width="80%">
            </colgroup>
            <thead>
            </thead>
            <tbody>
';


$strYearOptions = '';
for ($intCount = 0; $intCount <= 5; $intCount++) {
$intYear = date('Y') - 3 + $intCount;
$strSelected = '';
if ($intYear == $arrBgEvent[0]['be_jahr']) {
 $strSelected = ' selected="selected"';
}
$strYearOptions.= '              <option value="' .$intYear. '"' .$strSelected. '>' .$intYear. '</option>' .chr(10);
}

$strModal.= '
              <tr>
                <th class="text-nowrap"><label for="ja_' .$intBeId .'">Jahr:</label></th>
                <td>
                  <select id="ja_' .$intBeId .'" name="ja_' .$intBeId .'">
' .$strYearOptions .'
                  </select>
                </td>
              </tr>

              <tr>
                <th class="text-nowrap"><label for="mel_' .$intBeId .'">Meldestelle:</label></th>
                <td>' .$strMeldestelle .'</td>
              </tr>              
              
              <tr>
                <th class="text-nowrap"><label for="nr_' .$intBeId .'">Mitgliedsnummer:</label></th>
                <td>' .$strMitgliedsnummer .'</td>
              </tr>
              
              <tr>
                <th class="text-nowrap"><label for="nr_' .$intBeId .'">Unternehmensnummer:</label></th>
                <td>' .$strUnternehmensnummer .'</td>
              </tr>

              
              <tr>
                <th class="text-nowrap"><label for="bf_' .$intBeId .'">Beitragsfälligkeit:</label></th>
                <td>' .$strDatumFaellig .'</td>
              </tr>


            </tbody>
          </table>
    
          </div>  <!-- COL1 -->

        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

}

$strModal.= '

<div class="panel">
<header class="panel-heading">
  <h3 class="panel-title">Bearbeiter:in</h3>
</header>
<div class="panel-body">

  <div class="row">
    <div class="col-lg-66">
    
      <table class="table table-striped">
         <colgroup>
           <col width="20%">
           <col width="80%">
         </colgroup>
         <thead>
        </thead>
        <tbody>
          <tr>
            <th class="text-nowrap"><label for="us_' .$intBeId .'">Bearbeiter:in:</label></th>
            <td>' .$strBearbeiter .'</td>
          </tr>
        </tbody>
      </table>

    </div>  <!-- COL1 -->

  </div>  <!-- ROW -->
  
</div>  <!-- PANELBODY -->
</div>  <!-- PANEL -->


    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title">Ergebnis</h3>
      </header>
      <div class="panel-body">

        <div class="row">
          <div class="col-lg-66">
          
            <table class="table table-striped">
               <colgroup>
                 <col width="20%">
                 <col width="80%">
               </colgroup>
               <thead>
              </thead>
              <tbody>
';

if (!$intAdmin) {

  $strModal.= '
  <tr>
    <th class="text-nowrap"><label for="bf_' .$intBeId .'">Beitragsfälligkeit:</label></th>
    <td>' .str_replace(' dp', '', str_replace(' data-plugin="datepicker"', 'readonly="readonly"', $strDatumFaellig)) .'</td>
  </tr>  
  ';

}

$strModal.= '
                <tr>
                  <th class="text-nowrap"><label for="sb_' .$intBeId .'">Status Bearbeitung:</label></th>
                  <td>' .$strStatusBearbeitung .'</td>
                </tr>
                <tr>
                  <th class="text-nowrap"><label for="ag_' .$intBeId .'">Auskunftsgeber:</label></th>
                  <td>' .$strAuskunftsGeber .'</td>
                </tr>
                <tr>
                  <th class="text-nowrap"><label for="be_' .$intBeId .'">Befristet bis:</label></th>
                  <td>' .$strBefristet .'</td>
                </tr>
                <tr>
                  <th class="text-nowrap"><label for="er_' .$intBeId .'">Ergebnis:</label></th>
                  <td>' .$strErgebnis .'</td>
                </tr>
                <tr>
                  <th class="text-nowrap"><label for="st_' .$intBeId .'">Stundung / Ratenzahlung:</label></th>
                  <td>' .$strStundung .'</td>
                </tr>
                <tr>
                  <th class="text-nowrap"><label for="me_' .$intBeId .'" class="">Meldepflicht erfüllt:</label></th>
                  <td>' .$strMeldepflicht .'</td>
                </tr>
';

if ($boolUiOld === true) {
$strModal.= '
                <tr>
                  <th class="text-nowrap"><label for="do_' .$intBeId .'">Dokument:</label></th>
                  <td class="text-nowrap">' .$strDokument .'</td>
                </tr>
';
}

$strModal.= '
                <tr>
                  <th class="text-nowrap"><label for="da_' .$intBeId .'">Art des Dokuments:</label></th>
                  <td>' .$strDokumentArt .'</td>
                </tr>
              </tbody>
            </table>
    
          </div>  <!-- COL1 -->

          <div class="col-lg-66">
          
            <table class="table table-striped">
               <colgroup>
                 <col width="20%">
                 <col width="80%">
               </colgroup>
              <thead>
              </thead>
              <tbody>

                <tr>
                  <th class="text-nowrap"><label for="dr_' .$intBeId .'" class="tsp">Rückmeldung am:</label></th>
                  <td>' .$strDatumRueck .'</td>
                </tr>
';

if ($boolUiOld === true) {
$strModal.= '
                
                <tr>
                  <th class="text-nowrap"><label for="sp_' .$intBeId .'" class="">Sperrvermerk:</label></th>
                  <td>' .$strSperrvermerk .'</td>
                </tr>
                <tr>
                  <th class="text-nowrap"><label for="si_' .$intBeId .'">Sichtbarkeit Dokument:</label></th>
                  <td>' .$strSichtbarkeit .'</td>
                </tr>
';
}

$strModal.= '
                <tr>
                  <th class="text-nowrap"><label for="bs_' .$intBeId .'">Bemerkung (sichtbar):</label></th>
                  <td>' .$strBemerkungSichtbar .'</td>
                </tr>
              </tbody>
            </table>
    
          </div>  <!-- COL2 -->
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title">Klärung</h3>
      </header>
      <div class="panel-body">

        <div class="row">
          <div class="col-lg-66">
          
            <table class="table table-striped">
               <colgroup>
                 <col width="20%">
                 <col width="80%">
               </colgroup>
               <thead>
              </thead>
              <tbody>
                <tr>
                  <th class="text-nowrap"><label for="sk_' .$intBeId .'">Status Klärung:</label></th>
                  <td>' .$strStatusKlaerung .'</td>
                </tr>
                <tr>
                  <th class="text-nowrap"><label for="gr_' .$intBeId .'">Grund für Klärung:</label></th>
                  <td>' .$strGrund .'</td>
                </tr>
                <tr>
                  <th class="text-nowrap"><label for="ns_' .$intBeId .'">Nächster Schritt:</label></th>
                  <td>' .$strSchritt .'</td>
                </tr>
              </tbody>
            </table>
    
          </div>  <!-- COL1 -->

          <div class="col-lg-66">
          
            <table class="table table-striped">
               <colgroup>
                 <col width="20%">
                 <col width="80%">
               </colgroup>
              <thead>
              </thead>
              <tbody>
                <tr>
                  <th class="text-nowrap"><label for="rr_' .$intBeId .'">Rückstand:</label></th>
                  <td>' .$strRueckstand .'</td>
                </tr>
                <tr>
                  <th class="text-nowrap"><label for="ap_' .$intBeId .'">Ansprechpartner:</label></th>
                  <td>' .$strAp .'</td>
                </tr>
                <tr>
                  <th class="text-nowrap"><label for="at_' .$intBeId .'">Telefon:</label></th>
                  <td>' .$strTel .'</td>
                </tr>
              </tbody>
            </table>
    
          </div>  <!-- COL2 -->
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->


    <div class="">
      <button type="submit" class="btn btn-primary pull-right" id="svDetail">Speichern</button>
    </div>


  </form>

  
                    </div>
                    <div class="tab-pane" id="exampleTabsTwo" role="tabpanel" style="min-height: 450px;">
                      <div style="padding: 20px; background-color: #fff; height: 450px; width: 1200px">
                        <div kng smartchat data-type="topics" data-uri="' .$strIChatUri .'cms/index_neu.php?ac=bg_detail&beid=' .$intBeId .'" data-name="' .$strDataName .'"></div>
                      </div>
                    </div>


                    <div class="tab-pane" id="exampleTabsThree" role="tabpanel" style="min-height: 450px;">
                      <div style="padding: 20px; background-color: #fff; height: 800px;">

<script>
function show(type) {
  if (type == "all") {
    $(".stream-line").removeClass(\'hidden\');
  } else {
    $(".stream-line").addClass(\'hidden\');
    $("." + type).removeClass(\'hidden\');
  }
  $(".snav").css("font-weight", "normal");
  $("#s" + type).css("font-weight", "bold");
}	
</script>

                        <div id="stream">

                        <div class="stream-left" style="display: none;">

                        <p><a onclick="show(\'all\')" id="sall" class="snav">Alle</a></p>
                        <a onclick="show(\'notiz\')" id="snotiz" class="snav">Notizen</a><br>
                        <a onclick="show(\'file\')" id="sfile" class="snav">Files</a><br>
                        <a onclick="show(\'mail\')" id="smail" class="snav">Emails</a><br>
                        <a onclick="show(\'system\')" id="ssystem" class="snav">System</a><br>
                
                      </div>
                      <div class="stream-right" style="width: 820px;">
                        
                        <h2>Activities</h2>

                        <div id="stream-list">

' .$strActivityStream .'
                        
                        </div> <!-- Stream-List -->
                      </div> <!-- Stream-Right -->
                    </div>

                        </div>
                      
                      </div>
                    </div>
                  </div>
                </div>

                <div style="width: 300px; background-color: #DCDCDC; height: 40px; float: left; border: 1px solid #808080; margin-top: 24px;">
                <h4 style="padding-left: 10px;">Kommunikation</h4>
                </div>
                <div id="streamAside" style="width: 300px; background-color: white; height: 800px; float: left; overflow: scroll; position: relative; top: 63px; margin-left: -300px; border: 1px solid #808080;">
                
                ' .$strActivityStreamAside .'
                
                </div>

              </div>


  </div>  <!-- ROW-8 -->

';

$strOutput.= $strModal;

}

/* ------------------------- */

$strCssHead.= '
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jodit/3.1.92/jodit.min.css">
<link rel="stylesheet" href="/cms/css/selectize.default.css" data-theme="default">
';
$strJsHead.= '
<script src="//cdnjs.cloudflare.com/ajax/libs/jodit/3.1.92/jodit.min.js"></script>
';

$strJsHead.= '

';

$strOutput.= ' 

';


$strOutput.= '
  </div>
  <!-- End Page -->

  <style>

  .selectize-input {
    width: 100% !important;
  }
  
  .items .name {
    margin-right: 5px;
  }
  
  .salut, .info {
    display: none;
  }

  .caption {
    float: left;
    width: 900px;
    border: 1px solid transparent;
    padding: 2px;
  }

  .label {
    float: left;
    width: 900px;
    border: 1px solid transparent;
    padding: 2px;
    color: #3f3f3f;
    font-family: Roboto,sans-serif;
    font-size: 14px;
    line-height: 1.57142857;
  }

  </style>
  

                        <!-- Modal -->
                        <div class="modal fade modal-danger" id="ModalDanger" aria-hidden="true"
                        aria-labelledby="ModalDanger" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-center smallw">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Achtung</h4>
                              </div>
                              <div class="modal-body">
                                <p id="errMessage">Text</p>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Modal -->

                        <!-- Modal -->
                        <div class="modal fade modal-success" id="ModalSuccess" aria-hidden="true"
                        aria-labelledby="ModalSuccess" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-center smallw">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Erfolg</h4>
                              </div>
                              <div class="modal-body">
                                <p id="successMessage">Text</p>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Modal -->

                        <!-- Modal -->
                        <div tabindex="-1" role="dialog" aria-labelledby="modalEditBgMailLabel" aria-hidden="false" id="modalEditBgMail" class="modal fade">
                          <div class="modal-dialog modal-center modal-info">
                            <form action="" method="post" autocomplete="off" class="modal-content form-horizontal" id="formEditBgMail">
                            <input type="hidden" value="formEditBgMailSubmit" name="ac">
                            <input type="hidden" value="" id="ct_id" name="ct_id">
                              <div class="modal-header ">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button">
                                  <span aria-hidden="true">x</span>
                                </button>
                                <h4 id="modalEditgEventLabel" class="modal-title">E-Mail versenden</h4>
                              </div>
                              <div class="modal-body" style="margin: 5px;">

                              <div class="row" style="padding-left: 10px; padding-right: 10px;">

                              <div class="">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label for="select-to">An</label>
                                    <select id="to" class="contacts" placeholder="Empfänger auswählen..." autocomplete="off"></select> </div>
                                </div>
                              </div>

                              <div class="">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label for="cc">CC</label>
                                    <select id="cc" class="contacts" placeholder="Empfänger auswählen..." autocomplete="off"></select> </div>
                                </div>
                              </div>

                              <div class="">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label for="bcc">BCC</label>
                                    <input id="bcc" class="form-control" type="text" name="bcc" autocomplete="off"> </div>
                                </div>
                              </div>

                              <div class="">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="ct_head">Betreff</label>
                                        <input type="text" class="form-control" id="ct_head" name="ct_head" value="' .@$arrTemplate[0]['ct_head'] .'"> </div>
                                </div>
                              </div>

                              <div class="">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="ct_text">Text</label>
                                        <div id="editor"></div>
                                    </div>
                                </div>
                              </div>
                              
                              </div>

                              <div class="modal-footer">
                                <button id="modalEditBgMailSubmit" type="button" class="btn btn-primary margin-top-5 pull-right">E-Mail versenden</button>
                                <button type="button" class="btn btn-default btn-default margin-top-5 margin-right-10 pull-right" data-dismiss="modal">Close</button>
                              </div>
                               <!-- modal-body -->
                            </form>
                          </div>
                        </div>
                        <!-- End Modal -->  

';


$strJsFootScript.= '  <script src="js/script_neu.js"></script>' .chr(10);

$strJsFootCodeRun.= '

    var salutation = new Array();


    $("#editMail").on("click", function(e) {

      if ($("#ct_id_select").val() != "") {

        var data = $("#selectTemplate").serialize();

        data+= "&ac=renderMassage&beid=' .$intBeId .'";
        
        $.ajax({
          type: "POST",
          url:  "action/ajax.php",
          dataType: "json",
          data: data, //{ data, ac: "renderMassage", beid: ' .$intBeId .' },
          success: function(result) {

            $("#modalEditBgMail").modal("show");

            editor.value = result.ct_text.replace("{Salutation}", "<span class=\"salutToReplace\">{Salutation}</span>");

            $("#ct_head").val(result.ct_head);
        
          }
    
        });

      } else {

        $("#ModalDanger").modal("show");
        $("#ModalDanger #errMessage").html("Bitte wähle zuerst eine Vorlage aus.");

      }

      return false;

    });

    $("#modalEditBgMailSubmit").on("click", function(e) {
      
      $.ajax({
        type: "POST",
        url:  "action/ajax.php",
        dataType: "json",
        data: { ct_text: $("#editor").html(), ct_head: $("#ct_head").val(), to: $("#to").val(), cc: $("#cc").val(), bcc: $("#bcc").val(), ac: "sendMassage", beid: ' .$intBeId .', clid: ' .$_SESSION['id'] .' },
        success: function(result) {

          $("#modalEditBgMail").modal("hide");

          $("#ModalSuccess").modal("show");
          $("#ModalSuccess #successMessage").html("Die Nachricht wurde versendet.");
          
        }
  
      });

      return false;
  
    });
  
    var editor = new Jodit("#editor", {
      "spellcheck": false,
      "language": "de",
      "toolbarAdaptive": false,
      "showCharsCounter": false,
      "showWordsCounter": false,
      "showXPathInStatusbar": false,

      "height": 350,
    
      "askBeforePasteHTML": false,
      "askBeforePasteFromWord": false,
      "defaultActionOnPaste": "insert_only_text",
    
      "buttons": "source,|,bold,underline,italic,|,,ul,|,,|,|,align,undo,redo,indent,outdent,fullsize"
    
    });    

    ///*

    var REGEX_EMAIL = "([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@" +
    "(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)";
  
    $(\'#to\').selectize({
      persist: false,
      maxItems: null,
      valueField: \'email\',
      labelField: \'name\',
      searchField: [\'name\', \'email\'],
      options: [
  ';
  
  $arrPp = $objPp->arrGetPP($arrBgEvent[0]['be_meldestelle'], false);

  $strSqlContact3 = 'SELECT * FROM `Contact` WHERE `GroupId` = "' .$arrPp[0]['ac_cgid'] .'" AND `Ansprechpartner_fuer__c` LIKE "%Beitragsrückstand%" AND `Aktiv__c` = "true" AND `Email` != "" ORDER BY `LastName`';
  $arrSqlContact3 = MySQLStatic::Query($strSqlContact3);

  foreach ($arrSqlContact3 as $intCount => $arrContact) {
    $strContactSalut = $arrContact['Anrede_Brief_E_Mail__c'];
    
    if (strstr($arrContact['Anrede_Brief_E_Mail__c'], 'Herren') === false) {
      $strContactSalut.= ' ' .$arrContact['LastName'];
      $strContactName  = $arrContact['Salutation'] .' ' .$arrContact['Name'];
    } else {
      $strContactName = $arrContact['Name'];
    }

    $strJsFootCodeRun.= '
      {email: \'' .$arrContact['Email'] .'\', name: \'' .$strContactName .'\', salut: \'' .$strContactSalut .'\', info: \'' .str_replace(chr(10), '', $arrContact['Info__c']) .'\'},
  ';
    }
  
  /* } */
  
  $strJsFootCodeRun.= '
      ],
      render: {
        item: function(item, escape) {
          var cl = ""
          if (item.info != "") cl = "btninfo";
          return \'<div class="\' + cl + \'" title="\' + escape(item.info) + \'">\' +
            (item.name ? \'<span class="name">\' + escape(item.name) + \'</span>\' : \'\') +
            (item.email ? \'<span class="email">\' + escape(item.email) + \'</span>\' : \'\') +
            (item.salut ? \'<span class="salut">\' + escape(item.salut) + \'</span>\' : \'\') +
            (item.info ? \'<span class="info">\' + escape(item.info) + \'</span>\' : \'\') +
            \'</div>\';
        },
        option: function(item, escape) {
          var label_s = item.name || item.email;
          var caption = item.name ? item.email : null;
          var salut = item.salut;
          var info = item.info;
          var cl = ""
          if (item.info != "") cl = "btninfo";
          return \'<div class="\' + cl + \'" title="\' + escape(info) + \'">\' +
          \'<span class="label_s">\' + escape(label_s) + \'</span>\' +
          (caption ? \'<span class="caption">\' + escape(caption) + \'</span>\' : \'\') +
          \'<span class="salut">\' + escape(salut) + \'</span>\' +
          \'<span class="info">\' + escape(info) + \'</span>\' +
          \'</div>\';
        }
      },
      createFilter: function(input) {
        var match, regex;
  
        // email@address.com
        regex = new RegExp(\'^\' + REGEX_EMAIL + \'$\', \'i\');
        match = input.match(regex);
        if (match) return !this.options.hasOwnProperty(match[0]);
  
        // name <email@address.com>
        regex = new RegExp(\'^([^<]*)\<\' + REGEX_EMAIL + \'\>$\', \'i\');
        match = input.match(regex);
        if (match) return !this.options.hasOwnProperty(match[2]);
  
        return false;
      },
      create: function(input) {
        if ((new RegExp(\'^\' + REGEX_EMAIL + \'$\', \'i\')).test(input)) {
          return {email: input};
        }
        var match = input.match(new RegExp(\'^([^<]*)\<\' + REGEX_EMAIL + \'\>$\', \'i\'));
        if (match) {
          return {
            email : match[2],
            name  : $.trim(match[1])
          };
        }
        alert(\'Ungültige E-Mail-Adresse.\');
        return false;
      },
      onItemAdd: function (value, $item) {
        if ($item.context.childNodes.length > 1) {
          salutation.push($item.context.childNodes[2].innerText);
          if (salutation.length > 0) {
            $(".salutToReplace").html(salutation.join(",<br>") + ",");
          } else {
            $(".salutToReplace").html("{Salutation}");
          }
        }
        //console.log(salutation);
      },
      onItemRemove: function (value, $item) {
        if ($item.context.childNodes.length > 1) {
          var index = salutation.indexOf($item.context.childNodes[2].innerText);
          if (index > -1) {
            salutation.splice(index, 1);
            if (salutation.length > 0) {
              $(".salutToReplace").html(salutation.join(",<br>") + ",");
            } else {
              $(".salutToReplace").html("{Salutation}");
            }
          }
        }
        //console.log(salutation);
      }
    });
  
    $(\'#cc\').selectize({
      persist: false,
      maxItems: null,
      valueField: \'email\',
      labelField: \'name\',
      searchField: [\'name\', \'email\'],
      options: [
  ';
  
  if (count($arrSqlContact3) > 0) {
  
    foreach ($arrSqlContact3 as $intCount => $arrContact) {
      $strJsFootCodeRun.= '
        {email: \'' .$arrContact['Email'] .'\', name: \'' .$arrContact['Name'] .'\', salut: \'' .$arrContact['Anrede_Brief_E_Mail__c'] .'\', info: \'' .str_replace(chr(10), '', $arrContact['Info__c']) .'\'},
  ';
    }
  
  }
  
  $strJsFootCodeRun.= '
      ],
      render: {
        item: function(item, escape) {
          var cl = ""
          if (item.info != "") cl = "btninfo";
          return \'<div class="\' + cl + \'" title="\' + escape(item.info) + \'">\' +
            (item.name ? \'<span class="name">\' + escape(item.name) + \'</span>\' : \'\') +
            (item.email ? \'<span class="email">\' + escape(item.email) + \'</span>\' : \'\') +
            (item.salut ? \'<span class="salut">\' + escape(item.salut) + \'</span>\' : \'\') +
            (item.info ? \'<span class="info">\' + escape(item.info) + \'</span>\' : \'\') +
            \'</div>\';
        },
        option: function(item, escape) {
          var label_s = item.name || item.email;
          var caption = item.name ? item.email : null;
          var salut = item.salut;
          var info = item.info;
          var cl = ""
          if (item.info != "") cl = "btninfo";
          return \'<div class="\' + cl + \'" title="\' + escape(info) + \'">\' +
          \'<span class="label_s">\' + escape(label_s) + \'</span>\' +
          (caption ? \'<span class="caption">\' + escape(caption) + \'</span>\' : \'\') +
          \'<span class="salut">\' + escape(salut) + \'</span>\' +
          \'<span class="info">\' + escape(info) + \'</span>\' +
          \'</div>\';
        }
      },
      createFilter: function(input) {
        var match, regex;
  
        // email@address.com
        regex = new RegExp(\'^\' + REGEX_EMAIL + \'$\', \'i\');
        match = input.match(regex);
        if (match) return !this.options.hasOwnProperty(match[0]);
  
        // name <email@address.com>
        regex = new RegExp(\'^([^<]*)\<\' + REGEX_EMAIL + \'\>$\', \'i\');
        match = input.match(regex);
        if (match) return !this.options.hasOwnProperty(match[2]);
  
        return false;
      },
      create: function(input) {
        if ((new RegExp(\'^\' + REGEX_EMAIL + \'$\', \'i\')).test(input)) {
          return {email: input};
        }
        var match = input.match(new RegExp(\'^([^<]*)\<\' + REGEX_EMAIL + \'\>$\', \'i\'));
        if (match) {
          return {
            email : match[2],
            name  : $.trim(match[1])
          };
        }
        alert(\'Ungültige E-Mail-Adresse.\');
        return false;
      }
    });
  
    //*/

    $("#deldoc").click(function(){

      $.ajax({
        type: "POST",
        url:  "action/ajax.php",
        dataType: "text", 
        data: { ac: "bgDelDoc", beid: ' .$intBeId .' },
        success: function(result) {
          var jsonData = $.parseJSON(result);
          
          if (jsonData["Status"] == "OK") {
          
            $("#deldoc").parent().html("");
            
            //$("#re_" + jsonData["beid"]).parent().parent().hide();
            //$("#DocCount").html(parseInt($("#DocCount").html()) - 1);
          	//$("#Auskunft").modal("hide");
          	//location.reload();
                
          }
          
          if (jsonData["Status"] == "FAIL") {
  
            $("#ModalDanger").modal("show");
            $("#ModalDanger #errMessage").html(jsonData["Reason"]);
  
          }
  
        }
  
      });

      
    });
    
    $("#deldoc").bind("mouseover", function() {
        $(this).css("cursor", "pointer");
    });

    $("#modalEditBgMail").on("shown.bs.modal", function(e) {
      var $select = $("#to").selectize();
      var control = $select[0].selectize;
      control.clear();

      var $select = $("#cc").selectize();
      var control = $select[0].selectize;
      control.clear();

      $("#bcc").val("");

      salutation = new Array();
    });

';


?>