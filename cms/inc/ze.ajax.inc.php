<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

session_start();

require_once('../const.inc.php');

require_once('../../assets/classes/class.mysql.php');
require_once('../../assets/classes/class.database.php');

require_once('refactor.inc.php');

$intUser = $_SESSION['id'];

$arrHeader = array(
  'user: ' .$intUser,
  'Accept: application/json'
);

if (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'create') ) {

    $strId = $_REQUEST['id'];

    $strSql = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$strId .'"';
    $arrSql = MySQLStatic::Query($strSql);

    if (count($arrSql) > 0) {
        $strName = $arrSql[0]['Name'];

        $strName = str_replace('/', '-', $strName);
        $strName = str_replace('&', '-', $strName);

        if ($arrSql[0]['Liefert_Daten__c'] == 'true') {

            $strUrl = 'https://www.izs.de/cms/zertifikat/create/' .$strId .'.pdf?clid=' .$_REQUEST['clid'];

            $arrGet = [];
            $arrHeader = [];

            $arrRes = curl_get($strUrl, $arrGet, array(), $arrHeader);
            $arrRaw = json_decode($arrRes, true);

            $strLink = 'https://www.izs.de/cms/zertifikat/' .$strName .'/' .$strId .'.pdf';

            echo $arrRes .'|' .'<a href="' .$strLink .'">' .$strLink .'</a>';

        }

    }


} elseif (isset($_REQUEST['ac']) && ($_REQUEST['ac'] == 'revoke')) {

    $strSql1 = 'SELECT `pr_revoked` FROM `izs_zertifikat` WHERE `pr_id` = "' .$_REQUEST['id'] .'"';
    $arrSql1 = MySQLStatic::Query($strSql1);

    if ($arrSql1[0]['pr_revoked'] == 0) {
        $strSql = 'UPDATE `izs_zertifikat` SET `pr_revoked` = "1", `pr_revoked_by` = "' .$_REQUEST['clid'] .'", `pr_revoked_time` = NOW() WHERE `pr_id` = "' .$_REQUEST['id'] .'"';
    } else {
        $strSql = 'UPDATE `izs_zertifikat` SET `pr_revoked` = "0", `pr_revoked_by` = "0", `pr_revoked_time` = "0000-00-00 00:00:00" WHERE `pr_id` = "' .$_REQUEST['id'] .'"';
    }
    $arrSql = MySQLStatic::Update($strSql);

    if ((($_REQUEST['clid'] != '') && ($_REQUEST['clid'] != 0)) && ($arrSql1[0]['pr_revoked'] == 0)) {

        $strSql = 'SELECT `cl_first`, `cl_last` FROM `cms_login` WHERE `cl_id` = "' .$_REQUEST['clid'] .'"';
        $arrSql = MySQLStatic::Query($strSql);

        if (count($arrSql) > 0) {
            $strUserName = $arrSql[0]['cl_first'] .' ' .$arrSql[0]['cl_last'];
        }

    }

    if ($arrSql1[0]['pr_revoked'] == 0) {
        echo ($strUserName ?? '') .'|' .date('d.m.Y H:i:s');
    } else {
        echo '|';
    }

}


?>