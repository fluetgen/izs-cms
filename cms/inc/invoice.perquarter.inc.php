<?php

$strDebug = '';

$arrMonthName = array('', 'Jan', 'Feb', 'Mrz', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez');

$strSelect = '';
$strSql = 'SELECT DISTINCT(`is_date`) AS `is_date` FROM `import_stats` ORDER BY `is_date` DESC';
$arrAvailMonthList = MySQLStatic::Query($strSql);

$intMonthSum = count($arrAvailMonthList);
if ($intMonthSum > 0) {
  $strSelect.= '<form action="/cms/index_neu.php?ac=invoice&fu=perquarter" method="post"><label for="seldate" style="margin-right: 20px;">Abrechnungsmonat</label><select name="seldate" id="seldate">' .chr(10);
  foreach ($arrAvailMonthList as $intKey => $strMonth) {
    
    $intDate = strtotime($strMonth['is_date']);
    $intDate = date('U', mktime(0, 0, 0, date('n', $intDate) + 1, date('j', $intDate), date('Y', $intDate)));
    
    if (empty($_REQUEST['seldate']) && $intKey == 0) {
      $_REQUEST['seldate'] = date('Y-m', $intDate);
    }
    $strSelected = '';
    if (date('Y-m', $intDate) == @$_REQUEST['seldate']) {
      $strSelected = ' selected="selected"';
      $intMonth = date('m', $intDate);
      $intYear = date('Y', $intDate);
    }
    
    $strSelect.= '<option value="' .date('Y-m', $intDate) .'"' .$strSelected .'>' .date('Y-m', $intDate) .'</option>' .chr(10);
  }  
  $strSelect.= '</select></form>' .chr(10);
}

$intMonth1 = ($intMonth - 3 > 0) ? ($intMonth - 3) : (12 + $intMonth - 3); 
$intMonth2 = ($intMonth - 2 > 0) ? ($intMonth - 2) : (12 + $intMonth - 2); 
$intMonth3 = ($intMonth - 1 > 0) ? ($intMonth - 1) : (12 + $intMonth - 1); 


$strSql = 'SELECT `Group__c`.`Id` AS `GId`, `Group__c`.`Name` AS `GName`, `Betriebsnummer__c`, `Vertrag__c`.* FROM `Vertrag__c` INNER JOIN `Group__c` ON `Company_Group__c` = `Group__c`.`Id` WHERE `Aktiv__c` = "true" ORDER BY `GName`';
$arrGroupList = MySQLStatic::Query($strSql);
$intCountResult = count($arrGroupList);

$arrGroupListPerMonth = array();
if ($intCountResult > 0) {
  foreach ($arrGroupList as $intRow => $arrGroup) {
    $arrMonthList = explode(';', $arrGroup['Abzurechnen_in_Monat__c']);
    if (in_array($intMonth, $arrMonthList)) {
      $arrGroupListPerMonth[] = $arrGroup;
    }
  }
}

$arrGroupList = $arrGroupListPerMonth;

$arrEntList = array();
$strSql = 'SELECT * FROM `Account` WHERE `RecordTypeId` = "01230000001BvebAAC"';
$arrSql = MySQLStatic::Query($strSql);
if (count($arrSql) > 0) {
  foreach ($arrSql as $intKey => $arrEnt) {
    $arrEntList[$arrEnt['Id']] = $arrEnt['Name'];
  }
}

$arrTableHead = array(
  'Personaldienstleister',
  'Mitgliedsmodell',
  'Zahler',
  'Zahler ab',
  'Btnr',
  'V-Beginn',
  'Abrechnung',
  'angeliefert',
  'vereinbart',
  'Abweichung',
  'in %',
  'Preis/DS',
  'Summe',
  'Anmerkung',
  $arrMonthName[$intMonth1],
  $arrMonthName[$intMonth2],
  $arrMonthName[$intMonth3],
);

function arrGetDeliveredEvents ($strGrId = 0, $strMonth = '', $intMonthCount = 0, $intCountContract = 0) {

  global $strDebug;
  
  $arrReturn = array(
    'intCount' => 0,
    'intMissing' => ($intMonthCount + 1)
  );
  
  $intDate = strtotime($strMonth);
  $strMonthStop = date('Y-m', mktime(0, 0, 0, date('n', $intDate) - $intMonthCount - 1, date('j', $intDate), date('Y', $intDate))) .'-01';

  
  $arrSumReal = array();
  $strSql = 'SELECT `is_ilid` FROM `import_stats` WHERE `is_date` < "' .$strMonth .'" AND `is_date` >= "' .$strMonthStop .'" AND `is_grid` = "' .$strGrId .'" GROUP BY `is_ilid`';
  $arrFileList = MySQLStatic::Query($strSql);
  $intFileList = count($arrFileList);
  
  $arrFileRequest = array();
  if ($intFileList > 0) {
    foreach ($arrFileList as $intKey => $arrFile) {
      $arrFileRequest[] = $arrFile['is_ilid'];
    }
  } 
  
  $strFileRequest = implode(', ', $arrFileRequest);
  
  if ($strFileRequest != '') {
  
    $strSql = 'SELECT SUM(`is_datasets`) AS `sum`, `is_date`, MONTH( `is_date` ) AS `month` FROM `import_stats` WHERE `is_ilid` IN (' .$strFileRequest .') GROUP BY `is_date`';
    //$strDebug.= $strSql .chr(10);
    $arrDatasetCount = MySQLStatic::Query($strSql);
    $intDatasetCount = count($arrDatasetCount);
    
    $arrReturn['intMissing'] = ($intMonthCount + 1) - $intDatasetCount;
    //$arrReturn['arrDetail'] = $arrDatasetCount;
    
    foreach ($arrDatasetCount as $intKey => $arrMonth) {
      $arrReturn['intCount']+= $arrMonth['sum'];
      $arrReturn['arrDetail'][$arrMonth['month']] = $arrMonth['sum']; // ($arrMonth['sum'] - ($intCountContract * ($arrReturn['intMissing'] + 1)));
    }
  
  }

  /*
  if ($strGrId == 'a084S0000004iHSQAY') {
    echo $intCountContract;
    print_r($arrDatasetCount);
    print_r($arrReturn);
  }
  */
  
  return $arrReturn;
}

$arrList  = array();
$floatSum = 0.0;

foreach ($arrGroupList as $intRow => $arrGroup) {

  if ($arrGroup['GId'] == 'a083000000Rtl4fAAB') {
    //print_r($arrGroup);
  }
  
  $strSql = 'SELECT * FROM `Vertrag__c` WHERE `Company_Group__c` = "' .$arrGroup['GId'] .'" AND `Vertragsbeginn__c` < "' .$intYear .'-' .$intMonth .'-01" ORDER BY `Vertragsbeginn__c` DESC, `Aktiv__c` DESC';
  $arrVertrag = MySQLStatic::Query($strSql);
  $intCountResult = count($arrVertrag);
  if (($intCountResult > 1) && ($arrVertrag[0]['Vertragsbeginn__c'] == $arrVertrag[1]['Vertragsbeginn__c']) && ($arrVertrag[0]['Aktiv__c'] == 'true')) {
    $arrGroup = array_merge($arrGroup, $arrVertrag[0]);
  } elseif ($intCountResult >= 1) {
    $arrGroup = array_merge($arrGroup, $arrVertrag[0]);
  }

  if ($arrGroup['GId'] == 'a083000000Rtl4fAAB') {
    //print_r($arrVertrag[0]);
    //print_r($arrGroup);
    //die();
  }

  $strAddClass = '';
  
  preg_match("/(\d{1,2})/", $arrGroup['Abrechnungszeitraum__c'], $arrMonth);
  $intMonthCount = $arrMonth[0];
  
  $intCountContract = $arrGroup['Anzahl_Datens_tze_Quartal__c'];
  $arrCountReal = arrGetDeliveredEvents($arrGroup['Company_Group__c'], $intYear .'-' .$intMonth .'-01', $intMonthCount -1, round($intCountContract / 3));
  //$strDebug.= print_r($arrCountReal, true);
  $intCountReal = $arrCountReal['intCount'];
  
  $intDiff    = $intCountReal - round($intCountContract * $intMonthCount / 3);
  $intDiffRnd = $intCountReal - (ceil($intCountContract / 3) * $intMonthCount);
  if ($intCountContract != 0) { 
    $intPercent = round(($intDiffRnd / round($intCountContract)) * 100);
  } else {
    $intPercent = 0;
  }
  
  $strNote = '';
  if ($arrCountReal['intMissing'] > 0) {
    $strNote = '<i class="icon fa fa-warning" aria-hidden="true"></i> ' .$arrCountReal['intMissing'] .' Abrechnungsmonat fehlt';
  } 
  if ($arrCountReal['intMissing'] > 1) {
    $strNote = '<i class="icon fa fa-warning" aria-hidden="true"></i> ' .$arrCountReal['intMissing'] .' Abrechnungsmonate fehlen';
  }

  if (substr($arrGroup['Vertragsbeginn__c'], 0, -3) == $_REQUEST['seldate']) {
    //$strAddClass.= 'warning';

    $strSql = 'SELECT * FROM `izs_vertrag` WHERE `Id` = "' .$arrGroup['GId'] .'" AND `Vertragsbeginn__c` < "' .$arrGroup['Vertragsbeginn__c'] .'" ORDER BY `Name`';
    $arrGroupList = MySQLStatic::Query($strSql);
    $intCountResult = count($arrGroupList);
    if ($intCountResult >= 1) {
      if ($strNote != '') {
        $strNote = '<b>Achtung Vertragswechsel</b><br />';
      } else {
        $strNote.= '<b>Achtung Vertragswechsel</b>';
      }
      
    }

  }

  if ($intPercent > 0) {
    if ($intPercent < 10) $strAddClass.= 'warning';
    else $strAddClass.= 'danger';
  }

  $strSum = '';
  if ($intDiffRnd > 0) {
    $intSum = $arrGroup['Preis_pro_Datensatz_ber_Inklusivvolumen__c'] * $intDiff;
    $strSum = number_format($intSum, 2, ',', '') .' &euro;';

    $strMonthDiff = '';
    foreach ($arrCountReal['arrDetail'] as $intMonthNo => $intMonthDiff) {
      $strMonthDiff.= '_' .$intMonthNo .'_' .$intMonthDiff;
    }

    if ($arrCountReal['intMissing'] != 3) {
      $strSum.= ' <i class="fa fa-money svover" aria-hidden="true" rel="' .$arrMapping[$arrGroup['GId']] .'_' .$intDiff .'_' .$intCountReal .':' .$strMonthDiff .'"></i>';
    }

  } else {
    $intSum = 0;
  }

  $intContract   = round($intCountContract * $intMonthCount / 3);
  $intContractpM = ceil($intCountContract / 3) * $intMonthCount;

  $strOver = '';
  $strOverFrom = '';
  if ($arrGroup['BillingOver'] != '') {

    $strOver = $arrEntList[$arrGroup['BillingOver']];
    $strOverFrom = date('d.m.Y', strtotime($arrGroup['BillingOverFrom']));

  }
  
  $arrList[] = array(
    'ppayer'   => $arrGroup['GName'] .'<span class="hidden ' .$strAddClass .'"></span>',
    'model'    => $arrModelList[$arrGroup['GId']],
    'payer'    => $strOver,
    'payfrom'  => $strOverFrom,
    'btnr'     => $arrGroup['Betriebsnummer__c'],
    'since'    => strFormatDate($arrGroup['Vertragsbeginn__c'], 'd.m.Y', 'Y-m-d'),
    'invoice'  => $arrGroup['Abrechnungszeitraum__c'],
    'count'    => $intCountReal,
    'contract' => $intContract, 
    'diff'     => $intDiff, 
    'percent'  => $intPercent .'%',
    'price'    => number_format($arrGroup['Preis_pro_Datensatz_ber_Inklusivvolumen__c'], 2, ',', '.') .' &euro;',
    //'sum'      => '<span class="hidden">' .number_format($arrGroup['Preis_pro_Datensatz_ber_Inklusivvolumen__c'] * $intDiff, 2, '', '') .'</span><span style="white-space: nowrap; text-align: right; display: block;">' .$strSum .'</span>',
    'sum'      => '' .$strSum .'',
    'note'     => $strNote,
    'a' => @$arrCountReal['arrDetail'][$intMonth1],
    'b' => @$arrCountReal['arrDetail'][$intMonth2],
    'c' => @$arrCountReal['arrDetail'][$intMonth3]
  );

  if ($intDiff > 0) {
    $floatSum+= $intSum;
  }

}

$strTable = strCreateTable($arrList, $arrTableHead, '', false, true);

$strIncOutput.= ' 

    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><span id="DocCount">' .count($arrList) .'</span> Verträge gefunden</h3>
      </header>
      <div class="panel-body">
  
        <div class="row">
          <div class="col-lg-12">
          
';

$strIncOutput.= $strSelect;

$strIncOutput.= '<p>Maximal ' .number_format($floatSum, 2, ',', '.') .' EUR' .' können nachberechnet werden.</p>';

$strIncOutput.= $strTable;

if ($_SESSION['id'] == 3) {
  $strIncOutput.= $strDebug;
}

$strIncOutput.= ' 
    
          </div>  <!-- COL1 -->
    
        </div>  <!-- ROW -->
        
      </div>  <!-- PANELBODY -->
    </div>  <!-- PANEL -->
';

$strJsFootCodeRun.= '

$("span.warning").parent().parent().addClass("warning");
$("span.danger").parent().parent().addClass("danger");

$("#seldate").on("change", function (e) {
  this.form.submit();
});

$("td:nth-child(7)").css("text-align",  "right");
$("td:nth-child(7)").css("white-space", "nowrap");
$("td:nth-child(9)").css("white-space", "nowrap");
$("td:nth-child(10)").css("white-space", "nowrap");

$(".fa-money").on("click", function (e) {

  var ty, tys, obj, rel, part;

  obj = $(this);

  ty = 5;
  tys = "zusätzliche SV-Datensätze";

  part = $(this).attr("rel").split(":");
  rel = part[0].split("_");

  $.post("inc/invoice.ajax.php", { 
    ac: "createBill", id: rel[0], diff: rel[1], real: rel[2], month: ' .(int) $intMonth .', mcount: ' .$intMonthCount .', year: ' .$intYear .', type: ty, detail: part[1]
    }).done(function(data) {
      if (data == 1) {
        console.log(data);
        toastr.options = { "closeButton": true, "positionClass": "toast-top-full-width", "newestOnTop": true,   "showMethod": "fadeIn", "hideMethod": "fadeOut" }
        toastr.success(\'Die Rechnung "\' + tys + \'" wurde an easyBill gesendet.\');
        obj.hide();
      } else {
        toastr.options = { "closeButton": true, "positionClass": "toast-top-full-width", "newestOnTop": true,   "showMethod": "fadeIn", "hideMethod": "fadeOut" }
        toastr.error(\'Die Rechnung konnte nicht gesendet werden. Es sind nur 10 Zugriffe pro Minute möglich. Versuchen Sie es später noch einmal.\');
      }
    }
  );
  
});

';


?>