<?php

/*

importiert von
- /cms/eventimport.inc.php
- /cms/svcheck.inc.php

*/

require_once('../assets/classes/class.mysql.php');

function createTable($arrImport = array(), $arrWarning = array()) {
  
    global $boolHasHeader, $intCsvLines, $arrAc2Gr;
    
    $arrImpStat = array();
    $arrImpStatDetail = array();
    $arrAll = array();
    
    if (is_array($arrImport) && (count($arrImport) > 0)) {
      
      $strHtml = '';
      $strHtml.= '<table id="5">' .chr(10);
      $strHtml.= '  <thead>' .chr(10);
      $strHtml.= '    <tr>' .chr(10);
      $strHtml.= '      <th>Monat</th>' .chr(10);
      $strHtml.= '      <th>BNR PP</th>' .chr(10);
      $strHtml.= '      <th>Ch PP</th>' .chr(10);
      $strHtml.= '      <th>Name PP</th>' .chr(10);
      $strHtml.= '      <th>BNR KK</th>' .chr(10);
      $strHtml.= '      <th>Ch KK</th>' .chr(10);
      $strHtml.= '      <th>BNR IP</th>' .chr(10);
      $strHtml.= '      <th>Name IP</th>' .chr(10);
      $strHtml.= '      <th>Ch IP</th>' .chr(10);
      $strHtml.= '      <th>vorh.</th>' .chr(10);
      $strHtml.= '      <th>Import-Status</th>' .chr(10);
      $strHtml.= '      <th>Negativmerkmal</th>' .chr(10);
      $strHtml.= '    </tr>' .chr(10);
      $strHtml.= '  </thead>' .chr(10);
      $strHtml.= '  <tbody>' .chr(10);
      $strHtml.= '';
      
      $arrToImport = array();
      $arrProvider = array();
      
      $boolHasHeader = false;
      $boolIsDatev = false;
  
      $arrAllData = array();

      $arrMonthList = array();
    
      foreach ($arrImport as $intRowNumber => $strRow) {
  
        $arrLine = array();
  
        $arrDataLine = array();
        
        $boolOk = true;
        
        $strRow = array_values($strRow);
        
        if (preg_replace("/[^0-9a-zA-Z \-\_\.]/", "", $strRow[0]) == '') {
          continue;
        }
        
        if (($intRowNumber == 0) && (is_numeric($strRow[1]) == false)) {
          $boolHasHeader = true;

          if (strtolower(preg_replace("/[^0-9a-zA-Z \-\_\.]/", "", trim($strRow[5]))) == 'saldo') {
            $boolIsDatev = true;
          }
          continue;
        } 

        if (!$boolIsDatev) {
          die('Fehler! Dateiformat kann nicht verarbeitet werden.');
        }
        
        $arrMonth = cMonth(preg_replace("/[^0-9a-zA-Z \-\_\.]/", "", $strRow[0]));
        $strMonth = $arrMonth[2] .'-' .$arrMonth[1];
      
        $strStatMonth = $strMonth .'-01';

        $arrMonthList[] = $strStatMonth;
  
        if (!is_array($arrImpStatDetail[$strStatMonth])) {
          $arrImpStatDetail[$strStatMonth] = array(
            'is_result_ok' => 0,
            'is_result_error' => 0,
              'is_result_doublet_file' => 0,
              'is_result_doublet_system' => 0,
              'is_result_saldo_null' => 0,
              'is_result_saldo_no' => 0,
              'is_result_saldo_unknown' => 0
          );
        }
        
        if ($arrMonth[1][0] == 0) {
          $arrLine[1] = $arrMonth[1][1];
        } else {
          $arrLine[1] = $arrMonth[1];
        }
        $arrLine[2] = $arrMonth[2];
        
        $strBnrKk = cBnr(preg_replace("/[^0-9a-zA-Z \-\_\.]/", "", trim($strRow[1])));
        $strBnrPp = cBnr(preg_replace("/[^0-9a-zA-Z \-\_\.]/", "", trim($strRow[3])));

        $strReason = '';
        if ($boolIsDatev == true) {
          $strSaldo = preg_replace("/[^0-9a-zA-Z \-\_\.]/", "", strtolower(trim($strRow[5])));
          //$strSaldo = trim($strSaldo);
          if ($strSaldo == 'ja') {
            ;
          } elseif ($strSaldo == 'nein') {
            $boolOk = false;
            $strReason = '<span class="hidden">6</span><span class="red">Guthaben</span>';
            $arrImpStatDetail[$strStatMonth]['is_result_saldo_no']++;
            $arrDataLine[10] = 'Guthaben';
          } elseif ($strSaldo == 'null') {
            $boolOk = false;
            $strReason = '<span class="hidden">7</span><span class="red">Nullmeldung</span>';
            $arrImpStatDetail[$strStatMonth]['is_result_saldo_null']++;
            $arrDataLine[10] = 'Nullmeldung';
          } else {
            $boolOk = false;
            $strReason = '<span class="hidden">3</span><span class="red">NOT OK: Unbekanntes Saldo</span>';
            $arrImpStatDetail[$strStatMonth]['is_result_saldo_unknown']++;
            $arrDataLine[10] = 'Unbekanntes Saldo';
          }
        }
        
        //Check BNR PP
        $boolWrongPP = false;
        $intBnrExists = bnrExists($strBnrPp, 'PP');
        
        if ($intBnrExists != 1) {
          $boolImport = false;
          $boolOk = false;
          $boolWrongPP = true;
        }
        
        
        $arrLine[5] = $strBnrPp;
        
        $strKk    = getKk($strBnrKk);  
            
        $arrIp     = getNameBnr(cBnr($strKk), 'IP');
        $strNameIp = $arrIp[1];
        
        $arrLine[4] = $arrIp[0];
        
        $arrPp = getNameBnr($strBnrPp, 'PP');
        $strNamePp = $arrPp[1];
  
        $arrLine[3] = $strNamePp;
        
        $boolEventExists = eventExists(cBnr($strKk), $strBnrPp, $arrMonth);
        
        $intCsvLines++;
        
        $strHtml.= '    <tr class="larifari">' .chr(10);
        $strHtml.= '      <td>' .$strMonth .'</td>' .chr(10); 
        $strHtml.= '      <td>' .$strBnrPp .'</td>' .chr(10);
  
        $arrDataLine[0] = $strMonth;
        $arrDataLine[1] = $strBnrPp;
  
        $strAll = $strBnrPp .'_';
        
        $strGrId = $arrAc2Gr[$strBnrPp];
        
        if (isset($arrImpStat[$strGrId][$strStatMonth])) {
          $arrImpStat[$strGrId][$strStatMonth]++;
        } else {
          $arrImpStat[$strGrId][$strStatMonth] = 1;
        }
        
        $strHtml.= '      <td>';
        
        if (($strNamePp == '') || ($boolWrongPP == true)){
          $strHtml.= '<span class="red">NOT OK</span>';
          $boolOk = false;
          $arrDataLine[2] = 'NOT OK';
        } else {
          $strHtml.= 'OK';
          $arrDataLine[2] = 'OK';
        }
        
        $strHtml.= '</td>' .chr(10);
        $strHtml.= '      <td><a title="' .$strNamePp .'" href="index_neu.php?ac=contacts&grid=' .$strGrId .'" target="_blank">' .$strNamePp .'</a></td>' .chr(10);
        $strHtml.= '      <td>' .$strBnrKk .'</td>' .chr(10);
  
        $arrDataLine[3] = $strNamePp;
        $arrDataLine[4] = $strBnrKk;
  
        $strHtml.= '      <td>';
        
        if ($strKk != '00000000') {
          $strHtml.= 'OK';
          $arrDataLine[5] = 'OK';
        } else {
          $strHtml.= '<span class="red">NOT OK</span>';
          $boolOk = false;
          $arrDataLine[5] = 'NOT OK';
        }
        
        $strBnrIp = cBnr($strKk);
        $arrLine[6] = $strBnrIp;
  
        $strAll.= $strBnrIp;
        
        $strHtml.= '</td>' .chr(10);
        $strHtml.= '      <td>' .$strBnrIp .'</td>' .chr(10);
        $strHtml.= '      <td><a title="' .$strNameIp .'" href="index_neu.php?ac=contacts&bnr=' .$strBnrIp .'" target="_blank">' .$strNameIp .'</a>' .chr(10);
  
        $arrDataLine[6] = $strBnrIp;
        $arrDataLine[7] = $strNameIp;
  
        $strHtml.= '        <br />' .iconv('macintosh', 'UTF-8', $strRow[2]) .chr(10);
        
        $strHtml.= '      </td>' .chr(10);
        $strHtml.= '      <td>';
        
        if ($strNameIp != '') {
          $strHtml.= 'OK';
          $arrDataLine[8] = 'OK';
        } else {
          $strHtml.= '<span class="red">NOT OK</span>';
          $boolOk = false;
          $arrDataLine[8] = 'NOT OK';
        }
        
        $strHtml.= '</td>' .chr(10);
        $strHtml.= '      <td>';
  
        if (!$boolEventExists) {
          $strHtml.= 'NO';
          $arrDataLine[9] = 'NO';
        } else {
          $strHtml.= 'YES';
          $arrDataLine[9] = 'YES';
        }
        
        $strHtml.= '</td>' .chr(10);
        $strHtml.= '      <td>';
        
        $boolImport = false;
  
        if ($boolOk == true) {
          if ($boolEventExists) {
            $strHtml.= '<span class="hidden">10</span>NOT OK (Doublette)';
            $boolImport = false;
            $arrImpStatDetail[$strStatMonth]['is_result_doublet_system']++;
            $arrDataLine[10] = 'NOT OK (Doublette)';
          } elseif (in_array(array($strBnrIp, $strBnrPp), $arrProvider)) {
            $strHtml.= '<span class="hidden">9</span>OK (konsolidiert)';
            $boolImport = true;
            $arrLine[0] = '01230000001Ao73AAC';
            $arrLine[7] = 'abgelehnt / Dublette';
            $arrImpStatDetail[$strStatMonth]['is_result_doublet_file']++;
            $arrDataLine[10] = 'OK (konsolidiert)';
          } else {
            $strHtml.= '<span class="hidden">8</span>OK';
            $boolImport = true;
            $arrLine[0] = '01230000001Ao76AAC';
            $arrLine[7] = 'enquired';
            $arrImpStatDetail[$strStatMonth]['is_result_ok']++;
            $arrDataLine[10] = 'OK';
          }
        } else {
          if ($boolWrongPP == true) {
            if ($intBnrExists == 0) {
              $strHtml.= '<span class="hidden">1</span><span class="red">NOT OK: BNR PP ungültig!</span>';
              $arrDataLine[11] = $arrDataLine[10];
              $arrDataLine[10] = 'BNR PP ungültig!';
            } else {
              if ($intBnrExists == 2) {
                $strHtml.= '<span class="hidden">5</span><span class="red">NOT OK: Interne BNR (Office)</span>';
                $arrDataLine[11] = $arrDataLine[10];
                $arrDataLine[10] = 'Interne BNR (Office)';
              } else {
                $strHtml.= '<span class="hidden">4</span><span class="red">NOT OK: Inaktive BNR</span>';
                $arrDataLine[11] = $arrDataLine[10];
                $arrDataLine[10] = 'Inaktive BNR';
              }
            }
            $boolImport = false;
            $arrImpStatDetail[$strStatMonth]['is_result_error']++;
          } elseif ($strReason != '') {
            $strHtml.= $strReason;
            $boolImport = false;
          } else {
            $strHtml.= '<span class="hidden">2</span><span class="red">NOT OK: BNR IP ungültig!</span>';
            $boolImport = false;
            $arrImpStatDetail[$strStatMonth]['is_result_error']++;
            $arrDataLine[11] = $arrDataLine[10];
            $arrDataLine[10] = 'BNR IP ungültig!';
          }
        }
        
        $strHtml.= '</td>' .chr(10);
        $strHtml.= '      <td>{' .$strBnrPp .'_' .$strBnrIp .'_' .$strSaldo .'}</td>' .chr(10);
        $strHtml.= '    </tr>' .chr(10);
        
        if (($boolIsDatev == false) || ($strSaldo == 'ja')) {
          $arrProvider[] = array($strBnrIp, $strBnrPp);
        }
        if ($boolImport) {
          $arrToImport[] = $arrLine;
        }
  
        $arrAll[] = $strAll;
  
        $arrAllData[] = $arrDataLine;
        
      }
      
      $strHtml.= '  </tbody>' .chr(10);
      $strHtml.= '</table>' .chr(10);
      
      //print_r($arrImpStatDetail);
  
    } 
  
    return array($strHtml, $arrToImport, $arrImpStat, $arrImpStatDetail, $arrAll, $arrAllData, $arrMonthList);
  }
  
  function bnrExists($strBnrPp = '', $strType = 'PP') {
    $intReturn = 0;
    $strBnrPp = preg_replace("/[^0-9]/", "", $strBnrPp);
    if ($strBnrPp != '') {
      $strSql = 'SELECT `Id`, `SF42_use_data__c`, `SF42_Sub_Type__c` FROM `Account` WHERE  `RecordTypeId` = "01230000001Ao71AAC" AND `SF42_Comany_ID__c` = "' .$strBnrPp .'"';
      //echo $strSql .chr(10);
      $result = MySQLStatic::Query($strSql);
      //print_r($result);
      if (count($result) > 0) {
        if ($result[0]['SF42_use_data__c'] == 'true') {
          $intReturn = 1;
        } else {
          if ($result[0]['SF42_Sub_Type__c'] == 'Office') {
            $intReturn = 2;
          } else {
            $intReturn = 3;
          }
        }
      }
    }
    //echo 'RETURN: ' .$intReturn .chr(10) .chr(10);
    return $intReturn;
  } 
  
  
  function cMonth ($strMonth) {
    $arrMonth = explode('.', $strMonth);
    if (count($arrMonth) == 3) {
      if (strlen($arrMonth[2]) == 2) {
        $arrMonth[2] = '20' .$arrMonth[2];
      }
    }
    //$arrMonth[2] = 2014;
    return $arrMonth;
  }
  
  function cBnr ($strBnr) {
    return str_pad($strBnr, 8, '0', STR_PAD_LEFT);
  }
  
  function getNameBnr ($strBnr, $strType) {
    global $db;
  
    $strBnr = preg_replace("/[^0-9]/", "", $strBnr);
    
    if ($strType == 'IP') {
        $strFieldName = 'SF42_isInformationProvider__c';
    } elseif ($strType == 'PP') {
        $strFieldName = 'SF42_isPremiumPayer__c';
    } else {
      return '';
    }  
    //$strSql = 'SELECT `Name` FROM `Account` WHERE `SF42_Comany_ID__c` = %s AND `' .$strFieldName .'` = %s';
    $strSql = 'SELECT `Id`, `Name` FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$strBnr .'"';
    $result = MySQLStatic::Query($strSql);
    
    foreach ($result as $row) {
      return array($row['Id'], $row['Name']);
    }
  }
  
  function getKk ($strBnr) {
    global $db;
  
    $strBnr = preg_replace("/[^0-9]/", "", $strBnr);
    
    $strSql = 'SELECT `SF42_informationProvider__c`, `SF42_isInformationProvider__c` FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$strBnr .'"';
    $result = MySQLStatic::Query($strSql);
    
    foreach ($result as $row) {
      $strIp   = $row['SF42_informationProvider__c'];
      $strIsIp = $row['SF42_isInformationProvider__c'];
    }
    
    if ($strIsIp == 'true') {
      return $strBnr;
    }
    
    //echo 'IP: ' .$strIp .' | IS:' .$strIsIp; die();
  
    $strSql = 'SELECT `SF42_Comany_ID__c`, `SF42_isInformationProvider__c` FROM `Account` WHERE `Id` = "' .$strIp .'"';
    $result = MySQLStatic::Query($strSql);
    
    foreach ($result as $row) {
    
      //print_r($row); die();
  
      if ($row['SF42_isInformationProvider__c'] == 'true') {
        return $row['SF42_Comany_ID__c'];
      } else {
        //echo '<!-- rekursiv (' .$strIp .') -->' .chr(10);
        return getKk($row['SF42_Comany_ID__c']);
      }
    }
    
  }
  
  function eventExists ($strBnrIp, $strBnrPp, $arrMonth) {
    global $db;
  
    $strBnrIp = preg_replace("/[^0-9]/", "", $strBnrIp);
    $strBnrPp = preg_replace("/[^0-9]/", "", $strBnrPp);
    
    $strSql = 'SELECT `Id` FROM `SF42_IZSEvent__c` WHERE `Betriebsnummer_ZA__c` = "' .$strBnrPp .'" AND `Beitragsmonat__c` = "' .(int) $arrMonth[1] .'" AND `Beitragsjahr__c` = "' .$arrMonth[2] .'" AND `Betriebsnummer_IP__c` = "' .$strBnrIp .'"';
    $result = MySQLStatic::Query($strSql);
    
    foreach ($result as $row) {
      return true;
    }
    
    return false;
    
  }  

?>
