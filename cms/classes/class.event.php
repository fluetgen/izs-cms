<?php

if (!class_exists('MySQLStatic')) {
  require_once('../assets/classes/class.mysql.php');
}

class Event {
  
  private $strTableName = '`SF42_IZSEvent__c`';
  private $strFields = '';
  private $arrFields = array(
    "`SF42_IZSEvent__c`.`Id` AS `evid`",
    "`SF42_IZSEvent__c`.`IsDeleted` AS `ev_deleted`",
    "`SF42_IZSEvent__c`.`Name` AS `ev_name`",
    "`Betriebsnummer_ZA__c` AS `ev_ac_betriebsnummer`", //PP
    "`SF42_informationProvider__c` AS `ev_ip_acid`",
    "`Grund__c` AS `ev_grund`",
    "`Naechster_Meilenstein__c` AS `ev_meilenstein`",
    "`bis_am__c` AS `ev_bis`",
    "`Info__c` AS `ev_info`",
    "`Status_Klaerung__c` AS `ev_status`",
    "`AP_Klaerung` AS `ev_ap`",
    "`Tel_Klaerung` AS `ev_tel`",
    "`SF42_Month__c` AS `ev_month`",
    "`SF42_Year__c` AS `ev_year`",
    "`group_id` AS `ev_grid`",
    "`Rueckmeldung_am__c` AS `ev_rueck_am`",
    "`SF42_EventStatus__c` AS `ev_event_status`",
    "`SF42_DocumentUrl__c` AS `ev_doc`",
    "`Art_der_Rueckmeldung__c` AS `ev_rueckmeldung_art`"
  );
    
  function __construct () {
    
    $this->strFields = MySQLStatic::ImplodeFields($this->arrFields);
    
  }

  public function arrGetCatchAll () {
    
    $arrReturn = array();

    $strSql = 'SELECT * FROM  `izs_catchall`';
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);

    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }

    return $arrReturn;
  
  }  
  
  public function arrGetEvent ($strId = 0) {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    if ($strId != '') {
      
      $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' WHERE `Id` = "' .MySQLStatic::esc($strId) .'"';
      $arrResult = MySQLStatic::Query($strSql);
      $intCountResult = count($arrResult);
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }
    
    }
    
    return $arrReturn;


  }
 
  private function arrGetList ($strCondition = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' ' .$strCondition;
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }

  public function arrGetEventList ($arrFilter = array()) {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    $strCondition = '';

    $intCountFilter = count($arrFilter);

    if ($intCountFilter > 0) {
      
      $strCondition.= 'WHERE ';  
      foreach ($arrFilter as $intKey => $arrFilter) {
        if (isset($arrFilter['boolQuote']) && ($arrFilter['boolQuote'] === false)) {
          $strCondition.= '' .$arrFilter['strKey'] .' ' .$arrFilter['strType'] .' ' .$arrFilter['strValue'] .'';
        } else {
          $strCondition.= '`' .$arrFilter['strKey'] .'` ' .$arrFilter['strType'] .' "' .$arrFilter['strValue'] .'"';
        }
        if ($intKey < ($intCountFilter - 1)) {
          $strCondition.= ' AND ';
        }
        $strCondition.= ' ';
      }

    }

    $arrReturn = $this->arrGetList($strCondition);
    
    return $arrReturn;

  }
  
  public function arrGetEventListFromIp ($strIp = '', $strMonth = '', $strYear = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    //$strAccList = implode('", "', $arrAcc);
    
    $strCondition = ' WHERE `SF42_informationProvider__c` = "' .$strIp .'" ';
    
    if ($strMonth != '') {
      $strCondition.= 'AND `SF42_Month__c` = "' .$strMonth .'" '; 
    }
    
    if ($strYear != '') {
      $strCondition.= 'AND `SF42_Year__c` = "' .$strYear .'" '; 
    }
      
    $arrReturn = $this->arrGetList($strCondition);
    
    return $arrReturn;

  }

  public function arrGetEventListFromIpPp ($strIp = '', $strPp = '', $strMonth = '', $strYear = '') {
    
    global $arrDatabase;
    
    $objPp = new PremiumPayer;
    $strBtnr = $objPp->strGetBetriebsnummerFromId($strPp);
    
    $arrReturn = array();
    
    $strCondition = ' WHERE `SF42_informationProvider__c` = "' .$strIp .'" ';
    
    $strCondition.= ' AND `Betriebsnummer_ZA__c` = "' .$strBtnr .'" ';
    
    if ($strMonth != '') {
      $strCondition.= 'AND `SF42_Month__c` = "' .$strMonth .'" '; 
    }
    
    if ($strYear != '') {
      $strCondition.= 'AND `SF42_Year__c` = "' .$strYear .'" '; 
    }
      
    $arrReturn = $this->arrGetList($strCondition);
    
    return $arrReturn;

  }

  
  public function arrGetEventListFromIpPpInKlaerung ($strIp = '', $strPp = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    $objPp = new PremiumPayer;    
    
    $strCondition = ' WHERE `SF42_informationProvider__c` = "' .$strIp .'" ';
    $strCondition.= 'AND `Betriebsnummer_ZA__c` = "' .$objPp->strGetBetriebsnummerFromId($strPp) .'" '; 
    $strCondition.= 'AND `Status_Klaerung__c` = "in Klärung" '; 
    //$strCondition.= 'AND `Status_Klaerung__c` = "geschlossen / positiv" '; 
      
    $arrReturn = $this->arrGetList($strCondition);
    
    return $arrReturn;

  }
  
  public function arrGetListKlaerung ($strMonth = '', $strYear = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    $strCondition = ' WHERE `Id` != "" ';
    
    if ($strMonth != '') {
      $strCondition.= 'AND `SF42_Month__c` = "' .$strMonth .'" '; 
    }
    
    if ($strYear != '') {
      $strCondition.= 'AND `SF42_Year__c` = "' .$strYear .'" '; 
    }

    $strCondition.= 'AND `Status_Klaerung__c` = "in Klärung" '; 
    //$strCondition.= 'AND `Status_Klaerung__c` = "geschlossen / positiv" '; 
      
    $arrReturn = $this->arrGetList($strCondition);
    
    return $arrReturn;

  }
  
  public function boolEventIsNew ($strId = '', $strMonth = '', $strYear = '') {
    
    $boolReturn = false;
    
   	$strMonth = str_pad($strMonth, 2,'0', STR_PAD_LEFT);
    
    $arrEvent = $this->arrGetEvent($strId);
    
    $strCondition = ' WHERE `SF42_informationProvider__c` = "' .$arrEvent[0]['ev_ip_acid'] .'" ';
    $strCondition.= ' AND `Betriebsnummer_ZA__c` = "' .$arrEvent[0]['ev_ac_betriebsnummer'] .'" ';
    $strCondition.= ' AND STR_TO_DATE(CONCAT(`SF42_Year__c`, "-", `SF42_Month__c`), "%Y-%m") < "' .$strYear .'-' .$strMonth .'-00" ';
    
    $arrList = $this->arrGetList($strCondition);
    
    if (count($arrList) > 0) {
      $boolReturn = false;
    } else {
      $boolReturn = true;
    }
    
    return $boolReturn;
  
  }
  

}



?>