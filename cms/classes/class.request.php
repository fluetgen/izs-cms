<?php

require_once(APP_PATH .'assets/classes/class.mysql.php');
require_once(APP_PATH .'cms/classes/class.bg.php');

require_once(APP_PATH .'cms/classes/class.bgevent.php');
require_once(APP_PATH .'cms/classes/class.informationprovider.php');

class Request {
  
  private $strTableName = '';
  private $strFields = '';
  private $arrFields = array(
  );
  
  function __construct () {
    
    //$this->strFields = MySQLStatic::ImplodeFields($this->arrFields);
    
  }
  
  public function intSetRequest ($strId = '', $intYear = 0, $strMeldestelle = '') {

    //echo $strId; die();
    
    $arrReturn = array();

    $objIp = new InformationProvider;
    $objBgEvent = new BgEvent;

    $arrPpList = array();
    $arrDez = $objIp->arrDezentraleAnfrage($strId);

    $arrFields = array(
      "be_status_bearbeitung" => "angefragt", 
      "be_status_anfrage"     => "Anschreiben"
    );

    if (count($arrDez) > 0) {

        $strBg = $arrDez[0]['Information_Provider__c'];
        foreach ($arrDez as $intKey => $arrAnfragestelle) {
            $arrPpList[] = $arrAnfragestelle['Premium_Payer__c'];
        }

        $strSql = 'SELECT * FROM `izs_bg_event` WHERE `be_meldestelle` IN ("' .implode('", "', $arrPpList) .'") AND `be_status_bearbeitung` IN ("anzufragen", "angefragt", "erhalten - Klärungsfall") ';
        $arrReturn = MySQLStatic::Query($strSql);

        if (count($arrReturn) > 0) {
          foreach ($arrReturn as $intKey => $arrEvent) {
              $boolChange = $objBgEvent->boolChangeValueList($arrFields, $arrEvent['beid']);
          }
        }        

    } else {
    
      $strSql = 'SELECT * FROM `izs_bg_event` ';
      $strSql.= 'WHERE `be_bg` = "' .MySQLStatic::esc($strId) .'" AND `be_status_bearbeitung` IN ("anzufragen", "angefragt", "erhalten - Klärungsfall") ';
      if ($strMeldestelle != '') {
        $strSql.= 'AND `be_meldestelle` = "' .MySQLStatic::esc($strMeldestelle) .'"';
      }
      //echo $strSql; die();
      $arrReturn = MySQLStatic::Query($strSql);
      if (count($arrReturn) > 0) {
        foreach ($arrReturn as $intKey => $arrEvent) {
          $boolChange = $objBgEvent->boolChangeValueList($arrFields, $arrEvent['beid']);
        }  
      }
      
    }
    
    return $arrReturn;
    
  }

  
  public function boolWriteLog ($strId = '', $intYear = 0, $strBnr = '') {
    
    $boolReturn = false;
    
    if (($strId != '') && ($intYear != 0)) {
    
      $arrReq = $this->arrGetLog($strId, $intYear, $strBnr);
      
      if (count($arrReq) > 0) {
        $strSql = 'UPDATE `cms_req` SET `cr_created` = NOW() WHERE `cr_id` = "' .$arrReq[0]['cr_id'] .'"';
        if ($strBnr != '') {
          $strSql.= 'AND `cr_evid` = "' .$strBnr .'"';
        }
        MySQLStatic::Update($strSql);
      } else {
        $strSql = 'INSERT INTO `cms_req` (`cr_account_id`, `cr_month`, `cr_year`, `cr_created`, `cr_evid`) VALUES ("' .$strId .'", "", "' .$intYear .'", NOW(), "' .$strBnr .'")';
        MySQLStatic::Insert($strSql);
      }

      $boolReturn = true;
      
    }
    
    return $boolReturn;
    
  }

  public function arrGetLog ($strId = '', $intYear = 0, $strBnr = '') {
    
    $arrReturn = false;
    
    if (($strId != '') && ($intYear != 0)) {
    
      $strSql = 'SELECT * FROM `cms_req` WHERE `cr_account_id` = "' .$strId .'" AND `cr_year` = "' .$intYear .'" ';
      if ($strBnr != '') {
        $strSql.= 'AND `cr_evid`="' .$strBnr .'" ';
      }
      $arrReturn = MySQLStatic::Query($strSql);
      
    }
    
    return $arrReturn;
    
  }

}

?>