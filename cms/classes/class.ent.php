<?php

require_once(APP_PATH .'assets/classes/class.mysql.php');

class Ent {
  
  private $strTableName = '`izs_entleiher`';
  private $strFields = '';
  private $arrFields = array(
    "`izs_entleiher`.`Id` AS `ent_id`",
    "`Name` AS `ent_name`",
    "`Info_bei__c` AS `ent_info_when`",
    "`Info_Vertrag__c` AS `ent_info`",
	"`sl_db_aue`",
	"`sl_db_steuer`",
	"`sl_db_haftpflicht`",
	"`sl_db_register`",
	"`sl_db_tarif`",
	"`sl_db_zert`",
  "`Aktiv__c`",
  "`Kuendigung_wirksam_ab__c`"
  );

  /* SERVICELEVEL BD
        AÜ-Erlaubnis
        Bescheinigung in Steuersachen
        Betriebshaftpflicht
        Registerauszüge
        Tarifzugehörigkeit
        Zertifizierungen
  */
  
  function __construct () {
    
    $this->strFields = MySQLStatic::ImplodeFields($this->arrFields);
    
  }
  
  public function arrGetElement ($strId = '') {
    
    $arrReturn = array();
    
    if ($strId != '') {
      
      $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' WHERE `Id` = "' .MySQLStatic::esc($strId) .'"';
      $arrResult = MySQLStatic::Query($strSql);
      $intCountResult = count($arrResult);
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }
    
    }
    
    return $arrReturn;


  }

  
  private function arrGetList ($strCondition = '') {
    
    $arrReturn = array();
      
    $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' ' .$strCondition;
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }
  
  public function arrGetElementList ($strInfo = '', $boolAktiv = null) {
    
    $arrReturn = array();

    $strCondition = 'WHERE 1 ';
    if ($strInfo != '') {
        $strCondition.= 'AND `Info_bei__c` LIKE "%' .$strInfo .'%" ';
    }
    if ($boolAktiv != null) {
      if ($boolAktiv == true) {
        $strCondition.= 'AND `Aktiv__c` = "true" AND ((`Kuendigung_wirksam_ab__c` > CURDATE()) OR (`Kuendigung_wirksam_ab__c` = "0000-00-00")) ';
      } else {
        $strCondition.= 'AND `Aktiv__c` = "false" OR ((`Kuendigung_wirksam_ab__c` < CURDATE()) AND (`Kuendigung_wirksam_ab__c` > "0000-00-00")) ';
      }
      
    }
    
    $arrReturn = $this->arrGetList($strCondition);
    
    return $arrReturn;

  }
  
  public function arrGetRelation ($strId = '') {
    
    $arrReturn = array();

    $strSql = 'SELECT `Company_Group__c` AS `ver_id`, `Entleiher__c` AS `ent_id` FROM `Entleiher_CompGroup_Verbindung__c` WHERE 1 ';
    if (isset($strId) && ($strId != ''))  {
      $strSql.= 'AND `Entleiher__c` = "' .MySQLStatic::esc($strId) .'" ';
    }
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
    $arrReturn = $arrResult;
    }
    
    return $arrReturn;


  }

}



?>