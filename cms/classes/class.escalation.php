<?php

require_once(APP_PATH .'assets/classes/class.mysql.php');

class Escalation {
  
  public $arrEscalationList = array();

  function __construct () {
    
        $this->arrEscalationList = $this->getEscalationList();
    
  }
  
  public function getEscalationList ($strId = '', $strValue = 'ja') {
    
    $arrReturn = array();

    $strSql = 'SELECT * FROM `Group__c` WHERE `Eskalation__c` = "' .$strValue .'" ';
    if ($strId != '') {
       $strSql.= 'AND `Id` = "' .$strId .'"';
    }
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
        foreach ($arrResult as $intKey => $arrDataset) {
            $arrReturn[$arrDataset['Id']] = $arrDataset;
        }
    }
    
    return $arrReturn;

  }

}

?>