<?php

require_once(APP_PATH .'assets/classes/class.mysql.php');

class Additional {
  
  private $strTableName = '`izs_additional`';
  private $strFields = '';
  private $arrFields = array(
    "`adid`",
    "`ad_table`",
    "`ad_id`",
    "`ad_key`",
    "`ad_value`",
    "`ad_user`",
    "`ad_time`"
  );

  
  function __construct () {
    
    $this->strFields = MySQLStatic::ImplodeFields($this->arrFields);
    
  }
  
  public function arrGetElement ($strTable = '', $strId = '', $strKey = '') {//('Dokument_Dokumentation__c', $arrDocument['ddid'], 'received');
    
    $arrReturn = array();
    
    if (($strTable != '') && ($strId != '') && ($strKey != '')) {
      $strCondition = 'WHERE `ad_table` = "' .MySQLStatic::esc($strTable) .'" AND `ad_id` = "' .MySQLStatic::esc($strId);
      $strCondition.= '" AND `ad_key` = "' .MySQLStatic::esc($strKey) .'" ORDER BY `adid` DESC LIMIT 1';
    
      $arrReturn = $this->arrGetList($strCondition);
    }
    
    return $arrReturn;
    
  }
  
  private function arrGetList ($strCondition = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' ' .$strCondition;
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }
  
   public function boolInsert ($arrAdd = array()) {
    
    $boolReturn = false;
    
    if (count($arrAdd) > 0) {
      
      $arrElement = array();
      $arrElement['ad_table'] = '';
      $arrElement['ad_id'] = '';
      $arrElement['ad_key'] = '';
      $arrElement['ad_value'] = '';
      $arrElement['ad_user'] = $_SESSION['id'];
      $arrElement['ad_time'] = 'NOW()';
      
      $arrElement = array_merge($arrElement, $arrAdd);
      
      $boolReturn = $this->boolInsertEvent($arrElement);

    }
    
    return $boolReturn;

  }
  
  private function boolInsertEvent ($arrElement = array()) {
    
    $boolReturn = false;
    
    if (count($arrElement) > 0) {
        
      $strSql = 'INSERT INTO ' .$this->strTableName . ' (' .$this->strFields . ') VALUES (';
      $strSql.= 'NULL, "' .$arrElement['ad_table'] .'", ';
      $strSql.= '"' .$arrElement['ad_id'] .'", "' .$arrElement['ad_key'] .'", ';
      $strSql.= '"' .$arrElement['ad_value'] .'", "' .$arrElement['ad_user'] .'", ';
      $strSql.= '' .$arrElement['ad_time'] .')';
      $intResult = MySQLStatic::Insert($strSql);

      $boolReturn = true;
    
    }
    
    return $boolReturn;

  }
  
}

?>