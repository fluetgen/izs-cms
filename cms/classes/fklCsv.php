<?php
/**
 * Description
 * 
 * @copyright  Copyright (c) 2014 Frank K. Luetgen
 * @author     Frank K. Luetgen [fkl] <frank@luetgen.net>
 * 
 * @package    
 * @link       
 * 
 * @changes    1.0.0b    The initial implementation [fkl, 2014-01-27]
 */
class fklCsv {
  
  private $strDelimiter = ';';
  private $strEnclosure = '"';
  private $strPath      = '';
  
	
	public function __construct($strFile = '') {
	  
    if(!file_exists($strFile) || !is_readable($strFile))
        return FALSE;

	  $this->path = $strFile;
	  
    $objFile = new SplFileObject($strFile);
    $arrControl = $objFile->getCsvControl();
    
    if (is_array($arrControl) && (count($arrControl) > 0)) {
      //$this->strDelimiter = $arrControl[0];
      //$this->strEnclosure = $arrControl[1];
    }
	  
	}
	
  function isEmpty($array) {
    $my_not_empty = create_function('$v', 'return strlen($v) > 0;');
    return (count(array_filter($array, $my_not_empty)) == 0) ? 1 : 0;
  }
	
  public function to_array() {
  
      $header = array();
      $data = array();
      if (($handle = fopen($this->path, 'r')) !== FALSE)
      {
          while (($row = fgetcsv($handle, 1000, $this->strDelimiter)) !== FALSE)
          {
              if (!$this->isEmpty($row)) {
                
                /*
                if(!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
                */
                    $data[] = $row;
              }
          }
          fclose($handle);
      }
      return $data;
  }
  
  public function build_table($array){

    $html = '<table>';
    $html.= '<tr>';

    foreach($array[0] as $key=>$value) {
      $html .= '<th>' . $key . '</th>';
    }

    $html .= '</tr>';

    foreach( $array as $key=>$value) {
      $html .= '<tr>';
      foreach($value as $key2=>$value2) {
        $html .= '<td>' . $value2 . '</td>';
      }
      $html .= '</tr>';
    }

    $html.= '</table>';

    return $html;
  }
	
	public function getDelimiter() {
	  return $this->strDelimiter;
	}
	
	public function getEnclosure() {
	  return $this->strEnclosure;
	}
	
	
	public function __get($method) {
		return array($this, $method);		
	}
	
	
	public function __toString() {
		return '';
	}
		
}