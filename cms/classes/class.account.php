<?php

require_once(APP_PATH .'assets/classes/class.mysql.php');

class Account {
  
  private $strTableName = '`Account`';
  private $strFields = '';
  private $arrFields = array(
    "`Account`.`Id` AS `acid`",
    "`Account`.`IsDeleted` AS `ac_deleted`",
    "`Account`.`SF42_Comany_ID__c` AS `ac_betriebsnummer`",
    "`Name` AS `ac_name`"
  );
  
  function __construct () {
    
    $this->strFields = MySQLStatic::ImplodeFields($this->arrFields);
    
  }
  
  public function arrGetAccount ($strId = 0) {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    if ($strId != '') {
      
      $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' WHERE `Id` = "' .MySQLStatic::esc($strId) .'"';
      $arrResult = MySQLStatic::Query($strSql);
      $intCountResult = count($arrResult);
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }
    
    }
    
    return $arrReturn;


  }

  
  private function arrGetList ($strCondition = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' ' .$strCondition;
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }
  
  public function arrGetAccountList () {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $arrReturn = $this->arrGetList();
    
    return $arrReturn;

  }
  

}



?>