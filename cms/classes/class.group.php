<?php

require_once(APP_PATH .'assets/classes/class.mysql.php');

class Group {
  
  private $strTableName = '`Group__c`';
  private $strFields = '';
  private $arrFields = array(
    "`Group__c`.`Id` AS `grid`",
    "`Group__c`.`IsDeleted` AS `gr_deleted`",
    "`Group__c`.`Betriebsnummer__c` AS `gr_betriebsnummer`",
    "`Name` AS `gr_name`"
  );
  
  function __construct () {
    
    $this->strFields = MySQLStatic::ImplodeFields($this->arrFields);
    
  }
  
  public function arrGetGroup ($strId = 0) {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    if ($strId != '') {
      
      $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' WHERE `Id` = "' .MySQLStatic::esc($strId) .'"';
      $arrResult = MySQLStatic::Query($strSql);
      $intCountResult = count($arrResult);
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }
    
    }
    
    return $arrReturn;


  }

  
  private function arrGetList ($strCondition = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' ' .$strCondition;
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }
  
  public function arrGetGroupList () {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $arrReturn = $this->arrGetList();
    
    return $arrReturn;

  }
  

}



?>