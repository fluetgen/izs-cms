<?php

require_once(APP_PATH .'assets/classes/class.mysql.php');
require_once(APP_PATH .'cms/classes/class.bg.php');
require_once(APP_PATH .'cms/classes/class.group.php');

class Auth {
  
  private $strTableName = '`Vollmacht__c`';
  private $strFields = '';
  private $arrFields = array(
    "`Id` AS `ceid`",
    "`Company_Group__c` AS `ce_coid`",
    "`Link_zum_Dokument_pdf__c` AS `ce_link_pdf`",
    "`aktiviert__c` AS `ce_active`",
    "`Link_zum_Dokument_jpg__c` AS `ce_link_jpg`"
  );
  
  function __construct () {
    
    $this->strFields = MySQLStatic::ImplodeFields($this->arrFields);
    
  }
  
  public function arrGetAuth ($intId = 0) {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    if ($intId != 0) {
      
      $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' WHERE `Id` = "' .MySQLStatic::esc($intId) .'" ';
      $arrResult = MySQLStatic::Query($strSql);
      $intCountResult = count($arrResult);
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }
    
    }
    
    return $arrReturn;


  }

  
  private function arrGetList ($strCondition = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' ' .$strCondition;
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }
  
  public function arrGetAuthList () {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    $strCondition = '';  
    $arrReturn = $this->arrGetList($strCondition);
    
    return $arrReturn;

  }

  private function arrGetLinkLocal ($strId = 0) {
    
    $arrReturn = array();  
    
    if ($strId != 0) { //44296856
      
      $objPp = new PremiumPayer;
      $arrPp = $objPp->arrGetPP($strId, false);

      $strSql = 'SELECT * FROM `izs_file` WHERE `if_object` = "' .$arrPp[0]['ac_cgid'] .'" AND `if_type` = "Vollmacht" AND `if_show` = "true" ORDER BY `if_name` ASC';
      $arrSql = MySQLStatic::Query($strSql);
      $intSum = count($arrSql);

      if ($intSum > 0) {
        foreach ($arrSql as $intKey => $arrVollmachtRaw) {
          $arrReturn[] = CMS_PATH .'img/vollmacht/' .$arrVollmachtRaw['if_name'];
        }
      }
      
      /*
      $objGroup = new Group;
      $arrGroup = $objGroup->arrGetGroup($arrPp[0]['ac_cgid']);
      
      $strBetriebsnummer = $arrGroup[0]['gr_betriebsnummer'];
      
      $strLink_00 = CMS_PATH .'img/vollmacht/' .$strBetriebsnummer .'_Vollmacht.jpg';
      $strLink_01 = CMS_PATH .'img/vollmacht/' .$strBetriebsnummer .'_Vollmacht_01.jpg';
      
      if (file_exists($strLink_00) || file_exists($strLink_01)) {
        
        if (file_exists($strLink_01)) {
          for ($intFile = 1; $intFile < 20; $intFile++) {
            $strFileMore = CMS_PATH .'img/vollmacht/' .$strBetriebsnummer .'_Vollmacht_' .str_pad($intFile, 2, '0', STR_PAD_LEFT) .'.jpg';
            if (file_exists($strFileMore)) {
              $arrReturn[] = $strFileMore;
            } else {
              break;            
            }
          }                        
        } else {
          $arrReturn[] = $strLink_00;
        }
      
      }
      */

    
    }
    
    return $arrReturn;
    
  }

  private function arrGetLinkDb ($strId = '') {

    $arrReturn = array();  
    
    if ($strId != '') {
      
      $arrCert = $this->arrGetCert($strId);
      
      if (count($arrCert) > 0) {      
        foreach ($arrCert as $arrCertDeteil) {
          
          if (strstr($arrCertDeteil['Link_zum_Dokument_jpg__c'], '.doc') != false) {
            $arrCertDeteil['Link_zum_Dokument_jpg__c'] = 'http://www.izs-institut.de/Vollmacht_130513.jpg';
          }
        
          $strDest = TEMP_PATH .md5($arrCertDeteil['Link_zum_Dokument_jpg__c'] .'_' .$_SESSION['id']) .'.jpg';
          copy($arrCertDeteil['Link_zum_Dokument_jpg__c'], $strDest);
    
          $arrReturn[] = $strDest;
        }
      }
      
    }
    
    return $arrReturn;
    
  }

  public function arrGetAuthLink ($intId = 0) {

    $arrReturn = array();
    
    if ($intId != 0) {
      
      $arrReturn = $this->arrGetLinkLocal($intId);
      
    }
    
    return $arrReturn;
    
  }
  
  public function boolWriteLog ($strId = '', $intYear = 0, $strBnr = '') {
    
    $boolReturn = false;
    
    if (($strId != '') && ($intYear != 0)) {
    
      $arrCert = $this->arrGetLog($strId, $intYear, $strBnr);
      
      if (count($arrCert) > 0) {
        $strSql = 'UPDATE `cms_auth` SET `ca_created` = NOW() WHERE `ca_id` = "' .$arrCert[0]['ca_id'] .'" ';
        if ($strBnr != '') {
          $strSql.= 'AND `ca_evid` = "' .$strBnr .'"';
        }
        MySQLStatic::Update($strSql);
      } else {
        $strSql = 'INSERT INTO `cms_auth` (`ca_account_id`, `ca_month`, `ca_year`, `ca_created`, `ca_evid`) VALUES ("' .$strId .'", "", "' .$intYear .'", NOW(), "' .$strBnr .'")';
        MySQLStatic::Insert($strSql);
      }

      $boolReturn = true;
      
    }
    
    return $boolReturn;
    
  }

  public function arrGetLog ($strId = '', $intYear = 0, $strBnr = '') {
    
    $arrReturn = false;
    
    if (($strId != '') && ($intYear != 0)) {
    
      $strSql = 'SELECT * FROM `cms_auth` WHERE `ca_account_id` = "' .$strId .'" AND `ca_year` = "' .$intYear .'" ';
      if ($strBnr != '') {
        $strSql.= 'AND `ca_evid`="' .$strBnr .'" ';
      }
      $strSql.= 'ORDER BY `ca_created` DESC ';
      $arrReturn = MySQLStatic::Query($strSql);
      
    }
    
    return $arrReturn;
    
  }

}

?>