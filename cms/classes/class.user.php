<?php

require_once(APP_PATH .'assets/classes/class.mysql.php');

class User {
  
  private $strTableName = '`cms_login`';
  private $strFields = '';
  private $arrFields = array(
    "`cl_id`",
    "`cl_first`",
    "`cl_last`",
    "`cl_user`",
    "`cl_pass`",
    "`cl_mail`",
    "`cl_admin`",
    "`cl_deleted`"
  );
  
  function __construct () {
    
    $this->strFields = MySQLStatic::ImplodeFields($this->arrFields);
    
  }
  
  public function arrGetUser ($strId = 0) {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    if ($strId != '') {
      
      $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' WHERE `cl_id` = "' .MySQLStatic::esc($strId) .'"';
      $arrResult = MySQLStatic::Query($strSql);
      $intCountResult = count($arrResult);
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }
    
    }
    
    return $arrReturn;


  }

  
  private function arrGetList ($strCondition = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' ' .$strCondition;
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }
  
  public function arrGetUserList () {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $arrReturn = $this->arrGetList();
    
    return $arrReturn;

  }
  
  public function strGetUserName ($intId = 0) {
    
    $strReturn = '';
    
    $arrUser = $this->arrGetUser($intId);
      
    if ($arrUser[0]['cl_first'] != '') {
      $strReturn.= $arrUser[0]['cl_first'];
    }
    if ($arrUser[0]['cl_last'] != '') {
      if ($strReturn != '') {
        $strReturn.= ' ';
      }
      $strReturn.= $arrUser[0]['cl_last'];
    }
    
    return $strReturn;

  }
  

}



?>