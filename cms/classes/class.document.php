<?php

require_once(APP_PATH .'assets/classes/class.mysql.php');
require_once(APP_PATH .'assets/classes/class.database.php');

require_once(APP_PATH .'cms/classes/class.group.php');
require_once(APP_PATH .'cms/functions.inc.php');

class Document {
  
  private $strTableName = '`Dokument_Dokumentation__c`';
  private $strFields = '';
  private $arrFields = array(
    "`Dokument_Dokumentation__c`.`Id` AS `ddid`",
    "`Dokument_Dokumentation__c`.`IsDeleted` AS `dd_deleted`",
    "`Dokument_Dokumentation__c`.`Name` AS `dd_name`",
    "`Dokument_Dokumentation__c`.`RecordTypeId` AS `dd_rtype`",
    "`Company_Group__c` AS `dd_grid`",
    "`Link_zum_Dokument__c` AS `dd_link`",
    "`Status__c` AS `dd_status`",
    "`Dokumenten_Typ__c` AS `dd_dtype`",
    
    "`Zertifizierungsstelle__c` AS `dd_cert_center`",
    "`Verband__c` AS `dd_verband`",
    "`Zeitarbeitsfirma__c` AS `dd_cert_company`",
    "`Ausstellungsdatum__c` AS `dd_date`",
    "`Beginn__c` AS `dd_begin`",
    "`Ablauf__c` AS `dd_end`",
    "`Versicherer__c` AS `dd_versicherer`",
    
    /*
    "`X1_VS_Personen_Sach_Vermoegen__c` AS `dd_x1_person`",
    "`X1_Jahreshoechstentschaedigung__c` AS `dd_x1_jahres_hoechst`",
    "`X2_VS_Umwelt__c` AS `dd_x2_umwelt`",
    "`X2_Jahreshoechstentschaedigung__c` AS `dd_x2_jahres_hoechst`",
    "`X3_VS_Sach_in_Mio__c` AS `dd_x3_sach`",
    "`X4_VS_Vermoegen_in_Mio__c` AS `dd_x4_vermoegen`",
    "`X5_VS_Personen_in_Mio__c` AS `dd_x5_person`",
    "`X5_JHE_Personen_in_Mio__c` AS `dd_x5_jhe_person`",
    "`X3_JHE_Sach_in_Mio__c` AS `dd_jhe_sach`",
    "`X4_JHE_Vermoegen_in_Mio__c` AS `dd_jhe_vermoegen`",
    */

    "`Firmenname__c` AS `dd_company`",
    "`Strasse_Hausnummer__c` AS `dd_street`",
    "`Dokument_Dokumentation__c`.`PLZ__c` AS `dd_zip`",
    "`Dokument_Dokumentation__c`.`Ort__c` AS `dd_city`",
    "`Dokument_Dokumentation__c`.`Aktiv__c` AS `dd_active`",
    "`Dokument_Dokumentation__c`.`Info__c` AS `dd_info`",
    "`Letzte_Pruefung_am__c` AS `dd_check`",
    "`Update_Status__c` AS `dd_ustatus`",
    "`Info_sichtbar__c` AS `dd_ivisible`",
    
    "`Info_intern__c` AS `dd_info_intern`",
    
    "`erhalten__c` AS `dd_received`",
    "`Zeitarbeitsfirma__c` AS `dd_acid`"

  );
  
  private $arrDTypeUpdate = array (
    "01230000001DMTaAAO" =>	"Versicherungen",  	
    "01230000001DMTfAAO" =>	"Mitgliedschaften", 
    "01230000001DMTkAAO" =>	"Zertifizierungen",
    "0123A000001e7ChQAI" => "UBB Finanzamt",
    "01230000001DMTVAA4" => "AÜ Erlaubnis",
  );

/*
01230000001DMTaAAO
01230000001DMTfAAO
01230000001DMTkAAO
01230000001DMTQAA4
01230000001DMTVAA4
01230000001DS1FAAW
*/
  
  private $arrUpdateStatus = array (
    "",
    "Anfrage versandt",
    "1. Erinnerung",
    "2. Erinnerung",
    "3. Erinnerung",
    "Keine Reaktion",
    "Kein Ersatzdokument erforderlich",
    "Anfrage prüfen",
    "Anfrage zurückgestellt"
  );
  
  private $arrEscalation = array (0, 7, 4, 3, 2, 0);
  
  private $arrNextStatus = array (
    "Anfrage senden",
    "1. Erinnerung senden",
    "2. Erinnerung senden",
    "3. Erinnerung senden",
    "Ablegen",
    "Anfrage erneut senden",
    ""
  );

  private $intMaxUpdateStatus = 4;
  

  function __construct () {
    
    $this->strFields = MySQLStatic::ImplodeFields($this->arrFields);
    
  }
  
  public function arrGetDocument ($strId = 0) {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    if ($strId != '') {
      
      $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' WHERE `Id` = "' .MySQLStatic::esc($strId) .'"';
      $arrResult = MySQLStatic::Query($strSql);
      $intCountResult = count($arrResult);
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }
    
    }
    
    return $arrReturn;


  }

  private function arrGetList ($strCondition = '', $arrAddSelect = array()) {
    
    global $arrDatabase;
    
    $arrReturn = array();

    $strAddSelect = '';
    if (count($arrAddSelect) > 0) {
      foreach ($arrAddSelect as $strField => $strName) {
        $strAddSelect.= ', `' .$strField .'` AS `' .$strName .'`';
      }
    }
      
    $strSql = 'SELECT ' .$this->strFields .$strAddSelect .' FROM ' .$this->strTableName .' ' .$strCondition;

    if ($_SESSION['id'] == 3) {
      //echo $strSql;
    }
    
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }  

  public function arrGetDocumentList () {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $strCondition = '';      
    $arrReturn = $this->arrGetList($strCondition);
        
    return $arrReturn;

  }


  public function arrGetDocumentInformList () {
    
    global $arrDatabase;

    $strSelect = 'SELECT `ca_foreign_id` FROM `_cron_ajax` WHERE `ca_data` LIKE "%ddStatusClick%" AND `ca_status` = 1;';
    $arrSelect = MySQLStatic::Query($strSelect);

    $arrExclude = array();
    $strExclude = '';
    if (is_array($arrSelect) && count($arrSelect) > 0) {
      foreach ($arrSelect as $intKey => $arrId) {
        $arrExclude[] = $arrId['ca_foreign_id'];
      }
      $strExclude = '"' .implode('", "', $arrExclude) .'"';
    }
    
    $arrReturn = array();
    
    $strCondition = '';
    $strCondition.= 'INNER JOIN `Group__c` ';
    $strCondition.= 'ON ' .$this->strTableName .'.`Company_Group__c` = `Group__c`.`Id` ';

    $strCondition.= 'WHERE 1 ';
    $strCondition.= 'AND `Status__c` = "aktuell" ';
    $strCondition.= 'AND ((`Ablauf__c` > "0000-00-00") AND (`Ablauf__c` <= (NOW() - INTERVAL 4 WEEK))) ';
    if ($strExclude != '') {
      $strCondition.= 'AND `Dokument_Dokumentation__c`.`Id` NOT IN (' .$strExclude .') ';
    }
    $strCondition.= 'ORDER BY `Ablauf__c` DESC';

    $arrReturn = $this->arrGetList($strCondition, array('Status_Anbindung__c' => 'bd_status'));
        
    return $arrReturn;

  }

  public function arrGetDocumentUpdateList () {
    
    global $arrDatabase;

    $strSelect = 'SELECT `ca_foreign_id` FROM `_cron_ajax` WHERE `ca_data` LIKE "%ddStatusClick%" AND `ca_status` = 1;';
    $arrSelect = MySQLStatic::Query($strSelect);

    $arrExclude = array();
    $strExclude = '';
    if (is_array($arrSelect) && count($arrSelect) > 0) {
      foreach ($arrSelect as $intKey => $arrId) {
        $arrExclude[] = $arrId['ca_foreign_id'];
      }
      $strExclude = '"' .implode('", "', $arrExclude) .'"';
    }
    
    $arrReturn = array();
    
    $strCondition = '';
    $strCondition.= 'INNER JOIN `Group__c` ';
    $strCondition.= 'ON ' .$this->strTableName .'.`Company_Group__c` = `Group__c`.`Id` ';

    //$strCondition.= 'WHERE `RecordTypeId` IN (' .strArrayImplode($this->arrDTypeUpdate, '"', true) .') ';
    //$strCondition.= 'AND `Status__c` = "aktuell" AND `Ablauf__c` > "0000-00-00" ';

    $strCondition.= 'WHERE `Status__c` = "aktuell" AND `Ablauf__c` > "0000-00-00" ';
    $strCondition.= 'AND DATE_SUB(`Ablauf__c`, INTERVAL 14 DAY) <= CURDATE() ';
    $strCondition.= 'AND `Status__c` = "aktuell" ';
    $strCondition.= 'AND `Liefert_Daten__c` = "true" ';
    if ($strExclude != '') {
      $strCondition.= 'AND `Dokument_Dokumentation__c`.`Id` NOT IN (' .$strExclude .') ';
    } 
    $strCondition.= 'ORDER BY `Ablauf__c` DESC';
    $arrReturn = $this->arrGetList($strCondition, array('Status_Anbindung__c' => 'bd_status'));
        
    return $arrReturn;

  }
  
  public function arrGetDTypeUpdate () {

    $arrReturn = array();

    $strSql = 'SELECT `Id`, `Name`  FROM `RecordType` WHERE `SobjectType` = "Dokument_Dokumentation__c" AND `Id`!= "01230000001DS1FAAW" ORDER BY `Name`';
    $arrSql = MySQLStatic::Query($strSql);
    $intCountResult = count($arrSql);
    
    if ($intCountResult > 0) {
      foreach ($arrSql as $intCount => $arrType) {
        $arrReturn[$arrType['Id']] = $arrType['Name'];
      }
    }
    
    return $arrReturn;  
    
  }
  
  public function arrGetUpdateStatus () {
    $arrReturn = array();
    
    $arrReturn = $this->arrUpdateStatus;
    
    return $arrReturn;  
  }
  
  public function arrGetNextStatus () {
    $arrReturn = array();
    
    $arrReturn = $this->arrNextStatus;
    
    return $arrReturn;  
  }
  
  public function intGetMaxUpdateStatus () {
    $intReturn = array();
    
    $intReturn = $this->intMaxUpdateStatus;
    
    return $intReturn;  
  }

  public function arrGetDocumentStatus ($strId = 0) {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    if ($strId != '') {
      
      $strSql = 'SELECT * FROM `izs_basis_change` WHERE `bc_ddid` = "' .MySQLStatic::esc($strId) .'" AND `bc_type` != 7 ORDER BY `bcid` DESC';
      $arrResult = MySQLStatic::Query($strSql);
      $intCountResult = count($arrResult);
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }
    
    }
    
    return $arrReturn;


  }
  
  public function strGetNewUpdateStatus ($strStatus = '') {
    
    $strReturn = '';
    
    $intMaxUpdStatus = $this->intMaxUpdateStatus;
    $arrUpdateStatus = $this->arrUpdateStatus;
    
    $intKey = array_search($strStatus, $arrUpdateStatus);
    
    if ($intKey < $intMaxUpdStatus) {
      $strReturn = $arrUpdateStatus[$intKey + 1];
    } else {
      if ($intKey == 5) {
        $strReturn = $arrUpdateStatus[1];
      } elseif ($intKey >= 7) {
        $strReturn = '';
      } else {
        $strReturn = $arrUpdateStatus[$intMaxUpdStatus];
      }
    }
    
    return $strReturn;
    
  }
  
  public function strGetNewUpdateAction ($strStatus = '') {
    
    $strReturn = '';
  
    $intMaxUpdStatus = $this->intMaxUpdateStatus;
    $arrUpdateStatus = $this->arrUpdateStatus;
    $arrNextStatus   = $this->arrNextStatus;

    $intKey = array_search($strStatus, $arrUpdateStatus);

    if ($_SESSION['id'] == 3) {
      //echo $strStatus .' -> ' .$intKey;
    }

    if ($intKey <= $intMaxUpdStatus) {
      $strReturn = $arrNextStatus[$intKey];
    } else {
      if ($intKey == 5) {
        $strReturn = $arrNextStatus[0];
      } elseif ($intKey >= 7) {
        $strReturn = '';
      } else {
        $strReturn = $arrNextStatus[$intMaxUpdStatus - 2];
      }
    }
    
    return $strReturn;
    
  }
  
  public function boolUpdateUpdateStatusSf ($intDdId = '', $strUStatusNew = '', $intTime = '') {
    
    $boolReturn = false;
    
    /*
    $arrFields = array();
    $arrFields['Letzte_Pruefung_am__c'] = date('Y-m-d', $intTime);
    $arrFields['Update_Status__c'] = $strUStatusNew;
    $updateResult = SForceStatic::Update($arrFields, 'Dokument_Dokumentation__c', $intDdId);
    
    $arrReturn = json_decode(json_encode($updateResult), true);
    if ($arrReturn[0]['success'] == 1) {
      $updateResult = true;
    }
    */

    $updateResult = true;
    
    return $updateResult;
    
  }

  
  public function boolUpdateField ($intDdId = '', $strField = '', $strContent = '') {
    
    $updateResult = false;
    
    $strSql = 'UPDATE ' .$this->strTableName .' SET `' .$strField .'` = "' .$strContent .'" WHERE `Id` = "' .MySQLStatic::esc($intDdId) .'"';
    $intResult = MySQLStatic::Update($strSql);
    
    if ($intResult == 1) {
      $updateResult = true;
    }
    
    return $updateResult;
    
  }
  
  public function boolUpdateUpdateStatusDb ($intDdId = '', $strUStatusNew = '', $intTime = '') {
    
    $boolReturn = false;
    
    $intKey = array_search($strUStatusNew, $this->arrUpdateStatus);
    
    $strSql = 'UPDATE ' .$this->strTableName .' SET `Update_Status__c` = "' .$strUStatusNew .'" WHERE `Id` = "' .MySQLStatic::esc($intDdId) .'"';
    $intResult = MySQLStatic::Update($strSql);
    
    if (($intResult == 1) || ($intKey == $this->intMaxUpdateStatus)) {
      $updateResult = true;
    }
    
    return $updateResult;
    
  }

  public function boolArchiveDocumentSf ($intDdId = '') {
    
    $boolReturn = false;
    
    /*
    $arrFields = array();
    $arrFields['Status__c'] = 'aktiviert';
    $updateResult = SForceStatic::Update($arrFields, 'Dokument_Dokumentation__c', $intDdId);
    
    $arrReturn = json_decode(json_encode($updateResult), true);
    if ($arrReturn[0]['success'] == 1) {
      $updateResult = true;
    }
    */
    
    $updateResult = true;
    
    return $updateResult;
    
  }
  
  public function boolArchiveDocumentDb ($intDdId = '') {
    
    $boolReturn = false;
    
    $strSql = 'UPDATE ' .$this->strTableName .' SET `Status__c` = "aktiviert" WHERE `Id` = "' .MySQLStatic::esc($intDdId) .'"';
    $intResult = MySQLStatic::Update($strSql);
    
    if ($intResult == 1) {
      $boolReturn = true;
    }
    
    return $boolReturn;
    
  }
  
  public function arrGetRecipients ($strId = '', $strRecipientType = 'ver') {
    
    $arrReturn = array();
    
    $arrDocument = $this->arrGetDocument($strId);
    
    $objPp = new PremiumPayer;
    if ($strRecipientType == 'ver') {
      $arrPp = $objPp->arrGetAccountListFromGroup($arrDocument[0]['dd_grid']);
      $strGroupId = $arrDocument[0]['dd_grid'];
      $strFieldOfDuty = 'Basisdokumentation';
    } else {
      $arrPp = $objPp->arrGetEntleiherListFromGroup($arrDocument[0]['dd_grid']);
      $strGroupId = '';
      $strFieldOfDuty = 'Entleiherportal';
    }

    $objContact = new Contact;
    $arrReturn = $objContact->arrGetMailFromAccountList($arrPp, $strFieldOfDuty, $strGroupId);
    
    return $arrReturn;
  
  }
  
  public function strGetSubject ($strName = '', $strAStatus = '') {
    
    $strReturn = '';
    
    switch ($strAStatus) { 
      case 'Anfrage versandt': $strReturn = 'Basisdokumentation IZS: ' .$strName .' muss aktualisiert werden'; break;
      case '1. Erinnerung': $strReturn = '1. ERINNERUNG - Basisdokumentation IZS: ' .$strName .' muss aktualisiert werden'; break;
      case '2. Erinnerung': $strReturn = '2. ERINNERUNG - Basisdokumentation IZS: ' .$strName .' muss aktualisiert werden'; break;
      case '3. Erinnerung': $strReturn = 'LETZTMALIGE ERINNERUNG - Basisdokumentation IZS: ' .$strName .' muss aktualisiert werden'; break;

      case 'Info an Entleiher senden': $strReturn = 'IZS-Portal: Benachrichtigung über nicht fristgerechte Aktualisierung eines Dokuments der Basisdokumentation'; break;
      
      default: $strReturn = 'Basisdokumentation IZS: ' .$strName .' muss aktualisiert werden'; break;
    }
    
    return $strReturn;
  
  }
  
  public function intGetEscalation ($strAStatus = '') {
    
    $intReturn = -1;
    
    $intKey = array_search($strAStatus, $this->arrUpdateStatus);
    
    if ($intKey < $this->intMaxUpdateStatus) {
      $intReturn = $this->arrEscalation[$intKey];
    } else {
      $intReturn = $this->arrEscalation[$this->intMaxUpdateStatus];
    }
    
    return $intReturn;    
    
  }
  
  public function strGetMessage ($strId = '', $strAStatus = '', $intTime = 0) {
    
    $strReturn = '';
    
    $intEsc = $this->intGetEscalation($strAStatus);

    $arrDocument = $this->arrGetDocument($strId);
    $strRType = $this->arrDTypeUpdate[$arrDocument[0]['dd_rtype']];
    
    $strGroup = '';
    $strPp = '';
    if ($arrDocument[0]['dd_cert_company'] != '') {
      $objPp = new PremiumPayer;
      $arrPp = $objPp->arrGetAccount($arrDocument[0]['dd_cert_company']);
      $strPp = $arrPp[0]['ac_name'];
    }

    if ($strAStatus == 'Info an Entleiher senden') {

      $objGroup = new Group;
      $arrGroup = $objGroup->arrGetGroup($arrDocument[0]['dd_grid']);

      $strReturn.= '<p>Sehr geehrte Damen und Herren,</p>' .chr(10) .chr(10);

      $strReturn.= '<p>folgendes Dokument in der Basisdokumentation eines Ihrer Personaldienstleister ' .chr(10);
      $strReturn.= 'ist abgelaufen und wurde nicht fristgerecht aktualisiert.</p>' .chr(10) .chr(10);

      $strReturn.= '<p>Personaldienstleister: ' .$arrGroup[0]['gr_name'] .'<br />' .chr(10);
      $strReturn.= 'Typ: ' .$arrDocument[0]['dd_dtype'] .'<br />' .chr(10); // .' (<a href="' .$strDocLink .'">Dokument anzeigen</a>)<br />';
      
      if ($strRType == 'Zertifizierungen') {
        $strReturn.= 'Zertifizierungsstelle: ' .$arrDocument[0]['dd_cert_center'] .'<br />' .chr(10);
        if ($strPp != '') {
          $strReturn.= 'Für: ' .$strPp .'<br />' .chr(10);
        }
      } elseif ($strRType == 'Versicherungen') {
        $strReturn.= 'Versicherer: ' .$arrDocument[0]['dd_versicherer'] .'<br />' .chr(10);
      } elseif ($strRType == 'Mitgliedschaften') {
        $strReturn.= 'Verband: ' .$arrDocument[0]['dd_verband'] .'<br />' .chr(10);
        if ($strPp != '') {
          $strReturn.= 'Für: ' .$strPp .'<br />' .chr(10);
        }
      }
      $strReturn.= 'Ablauf: ' .strConvertDate($arrDocument[0]['dd_end'], 'Y-m-d', 'd.m.Y') .'</p>' .chr(10) .chr(10);


      $strReturn.= '<p>Weitere Informationen finden Sie in Ihrem Entleiher-Portal unter ' .chr(10);
      $strReturn.= '<a href="https://www.mein-verleiherportal.de/login.php">https://www.mein-verleiherportal.de/login.php</a>.</p>' .chr(10) .chr(10);

      $strReturn.= '<p><u>Wichtiger Hinweis:</u><br />' .chr(10);
      $strReturn.= 'Bitte beachten Sie, dass die Aktualisierung einzelner Dokumente u. U. einige Zeit in ' .chr(10);
      $strReturn.= 'Anspruch nehmen kann. IZS steht hierzu weiterhin im Austausch mit dem Personaldienstleister ' .chr(10);
      $strReturn.= 'und wird das Dokument am Portal aktualisieren, sobald ein Folge-Dokument vorliegt.</p>' .chr(10) .chr(10);

      $strReturn.= '<p>Mit besten Grüßen</p>' .chr(10) .chr(10);

    } else {
    
      $strReturn.= '<p>Sehr geehrte Damen und Herren,</p>' .chr(10) .chr(10);

      if ($strAStatus == '3. Erinnerung') {
        $strReturn.= '<p>folgendes Dokument in Ihrer Basisdokumentation auf dem IZS-Portal ist abgelaufen:</p>' .chr(10) .chr(10);
      } else {
        $strReturn.= '<p>folgendes Dokument in Ihrer Basisdokumentation auf dem IZS-Portal läuft in Kürze ab bzw. ist abgelaufen und muss nun aktualisiert werden:</p>' .chr(10) .chr(10);
      }
      
      if ((strstr($arrDocument[0]['dd_link'], 'http://') == false) && (strstr($arrDocument[0]['dd_link'], 'https://') == false)) {
        $strDocLink = 'http://' .$arrDocument[0]['dd_link'];
      } else {
        $strDocLink = $arrDocument[0]['dd_link'];
      }
      
      $strReturn.= '<p>Typ: ' .$arrDocument[0]['dd_dtype'] .'<br />' .chr(10); // .' (<a href="' .$strDocLink .'">Dokument anzeigen</a>)<br />';
      
      if ($strRType == 'Zertifizierungen') {
        $strReturn.= 'Bisherige Zertifizierungsstelle: ' .$arrDocument[0]['dd_cert_center'] .'<br />' .chr(10);
        if ($strPp != '') {
          $strReturn.= 'Für: ' .$strPp .'<br />' .chr(10);
        }
      } elseif ($strRType == 'Versicherungen') {
        $strReturn.= 'Bisheriger Versicherer: ' .$arrDocument[0]['dd_versicherer'] .'<br />' .chr(10);
      } elseif ($strRType == 'Mitgliedschaften') {
        $strReturn.= 'Bisheriger Verband: ' .$arrDocument[0]['dd_verband'] .'<br />' .chr(10);
        if ($strPp != '') {
          $strReturn.= 'Für: ' .$strPp .'<br />' .chr(10);
        }
      }
      $strReturn.= 'Ablauf: ' .strConvertDate($arrDocument[0]['dd_end'], 'Y-m-d', 'd.m.Y');
      if (($arrDocument[0]['dd_date'] != '0000-00-00') && ($arrDocument[0]['dd_date'] != '')) {
        $strReturn.= '<br />(Ausstellungsdatum: ' .strConvertDate($arrDocument[0]['dd_date'], 'Y-m-d', 'd.m.Y') .')' .chr(10);
      }
      $strReturn.= '</p>' .chr(10) .chr(10);
      
      if ($intTime != 0) {
        $strEscDate = strAddWorkingDays($intTime, $intEsc);
      } else {
        $strEscDate = strAddWorkingDays(date('U'), $intEsc);
      }
      
      if ($strAStatus == '3. Erinnerung') {

        $strReturn.= '<p>Trotz mehrfacher Erinnerungen haben wir bislang leider noch kein Aktualisierungsdokument erhalten.</p>' .chr(10) .chr(10);
        
        $strReturn.= '<p>Bitte beachten Sie, dass wir bei nicht rechtzeitiger Aktualisierung gemäß unseren AGB verpflichtet ' .chr(10);
        $strReturn.= 'sind, diesen Hinweis auf unserem Portal kenntlich zu machen.  Ferner werden die Ihrem Unternehmen ' .chr(10);
        $strReturn.= 'angeschlossenen Entleiher entsprechend informiert.</p>' .chr(10) .chr(10);

        $strReturn.= '<p>Da wir diese Eskalation gerne vermeiden möchten, bitten wir Sie, uns das Aktualisierungsdokument ' .chr(10);
        $strReturn.= 'daher nun kurzfristig bis ' .$strEscDate .' zukommen zu lassen.</p>' .chr(10) .chr(10);

        $strReturn.= '<p>Antworten Sie dazu einfach auf diese Email.</p>' .chr(10) .chr(10);
        $strReturn.= '<p>Mit besten Grüßen</p>' .chr(10) .chr(10);

      } else {

        $strReturn.= '<p>Bitte lassen Sie uns das Aktualisierungsdokument kurzfristig bis ' .$strEscDate .' zukommen.</p>' .chr(10) .chr(10);
        $strReturn.= '<p>Antworten Sie dazu einfach auf diese Email.</p>' .chr(10) .chr(10);
        $strReturn.= '<p>Mit besten Grüßen</p>' .chr(10) .chr(10);

      }


    }

    return $strReturn;
  
  }

}

?>