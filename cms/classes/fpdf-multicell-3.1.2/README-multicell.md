# Description
This FPDF addon class allows creation of an "Advanced Multicell" which uses as input a TAG based formatted string instead of a simple string.

See <https://www.interpid.eu/fpdf/multicell> for full details. 

 
# Installation
Just copy the project content to your web-server and call the example files (see `examples` folder)

# Support and Documentation

 - Examples: <https://tracker.interpid.eu/projects/pdf-addons/wiki/Fpdf_Multicell_Examples>
 - User Manual: <https://tracker.interpid.eu/projects/pdf-addons/wiki/Fpdf_Multicell_User_Manual>
 - Documentation Site: <https://tracker.interpid.eu/projects/pdf-addons/wiki>
 - Community forum: <https://tracker.interpid.eu/projects/pdf-addons/boards>
 - Issues, please report any issues here: <https://tracker.interpid.eu/projects/pdf-addons/issues>


# See also

- Product web page: <https://www.interpid.eu/fpdf/multicell>
- Other Interpid products: <https://www.interpid.eu/products>

# Software License Agreement
See `LICENSE` file provided with the source code
