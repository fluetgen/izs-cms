<?php

require_once(APP_PATH .'assets/classes/class.mysql.php');


class Bg {
  
  private $strRecordTypeId = '01230000001DKGtAAO'; //Berufsgenossenschaft
  private $strTableName = '`Account`';
  private $strFields = '';
  private $arrFields = array(
    "`Id` AS `acid`",
    "`IsDeleted` AS `ac_deleted`",
    "`Name` AS `ac_name`",
    "`Abteilung__c` AS `ac_req_abteilung`",
    "`Anrede_Anschreiben__c` AS `ac_req_salut`",
    "`Vorname__c` AS `ac_req_first`",
    "`Nachname__c` AS `ac_req_last`",
    "`Anfrage_Stra_e__c` AS `ac_req_street`",
    "`Anfrage_PLZ__c` AS `ac_req_zip`",
    "`Anfrage_Ort__c` AS `ac_req_city`",
    "`SF42_Comany_ID__c` AS `ac_betriebsnummer`", 
    "`Dezentrale_Anfrage__c` AS `ac_dezentral`", 
    "`SF42_Company_Group__c` AS `ac_cgid`",
    "`Originalvollmacht_ben_tigt__c` AS `ac_org`",
    "`SF42_Anfrageweg__c` AS `ac_anfrageweg`",
    "`Anfrage_E_Mail__c` AS `ac_mail`",
    "`Anfrage_zum__c` AS `ac_anfrage_zum`"
  );
  
  function __construct () {
    
    $this->strFields = MySQLStatic::ImplodeFields($this->arrFields);
    
  }

  
  public function arrGetAnfragestelle ($strId = 0) {
    
    $arrReturn = array();
    
    if ($strId != '') {
      
      $strSql = 'SELECT * FROM `Anfragestelle__c` WHERE `Id` = "' .MySQLStatic::esc($strId) .'" ';
      $arrResult = MySQLStatic::Query($strSql);
      $intCountResult = count($arrResult);
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }
    
    }
    
    return $arrReturn;


  }
  
  public function arrGetBg ($strId = 0) {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    if ($strId != '') {
      
      $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' WHERE `Id` = "' .MySQLStatic::esc($strId) .'" AND `RecordTypeId` = "' .$this->strRecordTypeId .'" ';
      $arrResult = MySQLStatic::Query($strSql);
      $intCountResult = count($arrResult);
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }
    
    }
    
    return $arrReturn;


  }

  
  private function arrGetList ($strCondition = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' ' .$strCondition;
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }

  public function arrGetBgView ($strId = '') {
    
    $arrReturn = array();
    
    $strSql = 'SELECT * FROM `izs_bg` ';
    if ($strId != '') {
      $strSql.= 'WHERE `Id` = "' .$strId .'" ';
    }
    $strSql.= 'ORDER BY `Name` ';

    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }

  public function arrGetBgList () {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    $strCondition = 'WHERE `RecordTypeId` = "' .$this->strRecordTypeId .'" ';  
    $arrReturn = $this->arrGetList($strCondition);
    
    return $arrReturn;

  }
  
  public function arrGetBgListFromCondition ($strCondition = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    $arrReturn = $this->arrGetList($strCondition);
    
    return $arrReturn;

  }
  
  public function arrGetMeldestellen ($strId = '', $strMeldestelle = '', $boolCheckDue = false, $boolCheckMeldestelle = true) {
    
    $arrReturn = array();
    
    $strCondition = 'WHERE 1 AND ';
    
    if ($boolCheckMeldestelle === true) {
      $strCondition.= '`Verbindung_Meldestelle_BG__c`.`Aktiv__c` = "true" AND ';
    }

    if ($strId != '') {
      $strCondition.= '`Berufsgenossenschaft__c` = "' .$strId .'" AND ';
    }
    if ($strMeldestelle != '') {
      $strCondition.= '`Account`.`Id` = "' .$strMeldestelle .'" ';
    } else {
      $strCondition.= '`Account`.`Ist_BG_Meldestelle__c` = "true" ';
    }

    if ($boolCheckDue === true) {
      $strCondition.= 'AND ((`Befristet_bis__c` = "0000-00-00") OR (`Befristet_bis__c` >= CURDATE())) ';
    }

    $strCondition.= 'ORDER BY `Account`.`Name` ';

    $strSql = 'SELECT `Berufsgenossenschaft__c` AS `acid`, `Meldestelle__c` AS `acid_meldestelle`, `Mitgliedsnummer_BG__c` AS `ac_bg_number`, ';
    $strSql.= '`SF42_Comany_ID__c` AS `ac_betriebsnummer`, `SF42_Company_Group__c` AS `ac_cgid`, ';
    $strSql.= '`Account`.`Name` AS `ac_name`, `Strasse_Meldestelle__c` AS `ac_street`, ';
    $strSql.= '`PLZ_Meldestelle__c` AS `ac_zip`, `Ort_Meldestelle__c` AS `ac_city` ';
    $strSql.= 'FROM `Verbindung_Meldestelle_BG__c` ';
    $strSql.= 'INNER JOIN `Account` ON `Verbindung_Meldestelle_BG__c`.`Meldestelle__c` = `Account`.`Id` ';
    $strSql.= $strCondition;
    
    //echo $strSql;

    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }  
  
  public function arrGetAllMeldestellen () {
    
    $arrReturn = array();
    
    $strSql = 'SELECT * FROM `Account` WHERE `Ist_BG_Meldestelle__c` = "true"';

    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }  

  public function boolConIsActive ($strMeldestelle = '', $strBg = '', $strBefristet = '') {
    
    $boolReturn = false;
    
    $strSql = 'SELECT * FROM `izs_bg_relation` INNER JOIN `izs_bg_meldestelle` ON `Meldestelle__c` = `izs_bg_meldestelle`.`Id` ';
        
    if ($strBg != '') { 
        $strSql.= 'WHERE `Berufsgenossenschaft__c` = "' .$strBg .'" AND `Meldestelle__c` ="' .$strMeldestelle .'" ';
        $strSql.= 'AND ((`Befristet_bis__c` > "' .$strBefristet .'") OR (`Befristet_bis__c` = "0000-00-00"))';
    }       
    
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $boolReturn = true;
    }

    return $boolReturn;

  }  
  
  public function arrGetBgFromMeldestelle ($strMeldestelle = '') {
    
    $arrReturn = array();

    $strSql = 'SELECT `Berufsgenossenschaft__c` AS `acid`, `Mitgliedsnummer_BG__c` AS `mglnr`, `Unternehmensnummer_BG__c` AS `untnr` FROM `Verbindung_Meldestelle_BG__c` ';
    $strSql.= 'WHERE `Meldestelle__c` = "' .MySQLStatic::esc($strMeldestelle) .'" AND `Aktiv__c` = "true" ';
    
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }  

}

?>