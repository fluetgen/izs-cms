<?php

if (!class_exists('MySQLStatic')) {
  require_once('../assets/classes/class.mysql.php');
}

class InformationProvider {
  
  private $strRecordTypeId = '01230000001Ao72AAC'; //Social Insurance Office
  private $strTableName = '`Account`';
  private $strFields = '';
  private $arrFields = array(
    "`Id` AS `acid`",
    "`IsDeleted` AS `ac_deleted`",
    "`Name` AS `ac_name`",
    "`SF42_Comany_ID__c` AS `ac_betriebsnummer`", 
    "`Dezentrale_Anfrage__c` AS `ac_dezentral`", 
    "`SF42_Company_Group__c` AS `ac_cgid`",
    "`Originalvollmacht_ben_tigt__c` AS `ac_org`"
  );
  
  function __construct () {
    
    $this->strFields = MySQLStatic::ImplodeFields($this->arrFields);
    
  }

  public function arrInformationProviderList () {
    
    $arrReturn = array();
      
    $strSql = 'SELECT * FROM `izs_informationprovider`';
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }  

  public function arrDezentraleAnfrage ($strId = '') {
    
    $arrReturn = array();
      
    $strSql = 'SELECT * FROM `izs_dezentrale_anfrage`';
    if ($strId != '') {
      $strSql.= 'WHERE `DezId` = "' .$strId .'"';
    }
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }  

  public function arrGetIP ($strId = 0) {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    if ($strId != '') {
      
      $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' WHERE `Id` = "' .MySQLStatic::esc($strId) .'" AND `RecordTypeId` = "' .$this->strRecordTypeId .'" AND `SF42_isInformationProvider__c` = "true"';
      $arrResult = MySQLStatic::Query($strSql);
      $intCountResult = count($arrResult);
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }
    
    }
    
    return $arrReturn;


  }

  
  private function arrGetList ($strCondition = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' ' .$strCondition;
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }
  
  public function arrGetIPList () {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    $strCondition = 'WHERE `RecordTypeId` = "' .$this->strRecordTypeId .'" AND `SF42_isInformationProvider__c` = "true"';  
    $arrReturn = $this->arrGetList($strCondition);
    
    return $arrReturn;

  }


  /*

  */
  
  public function arrGetNewVollmachtList ($strId = 0, $intMonth = 0, $intYear = 0) {
  
  	$arrReturn = array();
  	
  	$strMonth = str_pad($intMonth, 2,'0', STR_PAD_LEFT);
  	
    if ($strId != '') {

      $strSql2 = 'SELECT `Betriebsnummer_ZA__c` FROM  `SF42_IZSEvent__c` ';
      $strSql2.= 'INNER JOIN `Account` ON `Betriebsnummer_ZA__c` = `SF42_Comany_ID__c` ';
      $strSql2.= 'WHERE  `SF42_IZSEvent__c`.`SF42_informationProvider__c` =  "' .$strId .'" ';
      $strSql2.= 'AND `Account`.`Registriert_seit__c` = "' .$intYear .'-' .$strMonth .'-15" ';
      $strSql2.= 'AND `SF42_Year__c` = "' .$intYear .'" AND `SF42_Month__c` = "' .$intMonth .'" ';
      $strSql2.= 'GROUP BY `Betriebsnummer_ZA__c` ORDER BY `SF42_Premium_Payer__c` ';

      $arrResult2 = MySQLStatic::Query($strSql2);
      
      $arrAct = array();
      if (count($arrResult2) > 0) {
        foreach ($arrResult2 as $intCount => $intBtrNo) {
          $arrAct[] = $intBtrNo['Betriebsnummer_ZA__c'];
        }
      }

      $arrReturn = $arrAct;

    } else {

      $strSql2 = 'SELECT `SF42_Company_Group__c`, `SF42_Comany_ID__c` FROM `Account` WHERE `Registriert_seit__c` = "' .$intYear .'-' .$strMonth .'-15" ';

      $arrResult2 = MySQLStatic::Query($strSql2);
      
      $arrAct = array();
      if (count($arrResult2) > 0) {
        foreach ($arrResult2 as $intCount => $intBtrNo) {
          $arrAct[] = $intBtrNo['SF42_Company_Group__c'];
        }
      }

      $arrReturn = $arrAct;

    }
    
    return $arrReturn;
  	
  	
  }

}



?>