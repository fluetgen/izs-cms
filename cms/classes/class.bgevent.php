<?php

require_once(APP_PATH .'assets/classes/class.mysql.php');
require_once(APP_PATH .'cms/classes/class.bg.php');

require_once ('class.stream.php');

class BgEvent {
  
  private $strTableName = '`izs_bg_event`';
  private $strFields = '';
  private $arrFields = array(
    "`beid`", 
    "`be_name`", 
    "`be_meldestelle`", 
    "`be_bg`", 
    "`be_jahr`", 
    "`be_status_anfrage`", 
    "`be_ergebnis`", 
    "`be_auskunftsgeber`", 
    "`be_sperrvermerk`", 
    "`be_datum`", 
    "`be_befristet`", 
    "`be_link`",
    "`be_dokument_art`",  
    "`be_bemerkung`", 
    "`be_status_bearbeitung`", 
    "`be_sichtbar`", 
    "`be_info_intern`", 
    "`be_wiedervorlage`", 
    "`be_datum_rueckmeldung`", 
    "`be_klaerung_status`", 
    "`be_klaerung_grund`", 
    "`be_rueckstand`", 
    "`be_klaerung_schritt`", 
    "`be_klaerung_ap`", 
    "`be_klaerung_telefon`",
    "`be_name_meldestelle`",
    "`be_mitgliedsnummer`",
    "`be_unternehmensnummer`",
    "`be_name_bg`",
    "`be_verbindung`",
    "`be_stundung`",
    "`be_bearbeiter`",
    "`be_meldepflicht`",
    "`be_beitragsfaelligkeit`",
  );

  private $arrErgebnis = array(
    'OK', 
    'NOT OK', //,
    //'Keine Auskunft erhalten'
    'OK (mit RV)',
    'gestundet',
    'Hinweis beachten'
  );
  
  private $arrAuskunftsgeber = array(
    'Berufsgenossenschaft', 
    'Zeitarbeitsunternehmen'
  );

  private $arrStatusBearbeitung = array(
    'anzufragen', 
    'angefragt', 
    'Endkontrolle', 
    'veröffentlicht',
    'zurückgestellt von IZS'
  );
  
  private $arrSichtbarkeit = array (
    'sichtbar',
    'auf Anfrage',
    'nicht sichtbar'
  );

  private $arrKlaerungStatus = array (
    'in Klärung',
    'geschlossen - positiv',
    'geschlossen - negativ'
  );
  
  private $arrKlaerungGrund = array (
    'Auskunftsverweigerung',
    'Beitragsrückstand',
    'Beitragsrückstand + Stundung',
    'Beitragsrückstand + Ratenvereinbarung',
    'Bescheinigung so nicht veröffentlichenbar',
    'Kein Konto',
    'Keine Auskunft von BG',
    'Meldepflicht nicht erfüllt',
    'Ratenvereinbarung',
    'Sonstiges',
    'Stundung',
  );
  
  private $arrKlaerungSchritt = array (
    '1.1 Info an ZA: senden',
    '1.2 Info an ZA: Erinnerung 1',
    '1.3 Info an ZA: Erinnerung 2',
    '1.4 Info an ZA: Erinnerung 3',
    '2.1 Versenden: Anfrageformular an BG',
    '2.2 Versenden: Vollmacht an BG',
    '3.1 Anrufen: BG',
    '3.2 Anrufen: ZA',
    '4.1 Warten auf Rückmeldung: von BG',
    '4.2 Warten auf Rückmeldung: von ZA'
  );
  
  private $arrDokumentArt = array(
    'Dynamische Erzeugung',
    'Kontoauszug',
    'Unbedenklichkeitsbescheinigung (UBB)',
    'Sonstiges'
  );

  private $arrStundung = array (
    'Stundung',
    'Ratenzahlung',
    'Stundung oder Ratenzahlung'
  );

  private $arrMeldepflicht = array (
    'ja',
    'nein'
  );

  function __construct () {
    
    $this->strFields = MySQLStatic::ImplodeFields($this->arrFields);
    
  }
  
  public function boolChangeValueList ($arrChanges = array(), $intId = 0, $boolNotNull = false) {
    
    $boolReturn = false;
    
    if (is_array($arrChanges) && (count($arrChanges) > 0)) {
      
      foreach ($arrChanges as $strKey => $strValue) {

        if ($boolNotNull === true) {
          if (($strValue != '') && ($strValue != '0000-00-00')) {
            //echo "$strKey, $strValue, $intId" .chr(10);
            $this->boolChangeValue($strKey, $strValue, $intId);
          }
        } else {
          $this->boolChangeValue($strKey, $strValue, $intId);
        }
        
      }
            
      $boolReturn = true;
      
    }
    
    return $boolReturn;
    
  }

  public function getSgId ($intCmsId = 0) {

    $strReturn = '';

    $strSql = 'SELECT * FROM `izs_cms2sg` WHERE `cs_type` = "BG Event" AND `cs_cms_index` = "' .$intCmsId .'"';
    $arrSql = MySQLStatic::Query($strSql);

    if (count($arrSql) > 0) {

      $strReturn = $arrSql[0]['cs_sg_index'];

    }

    return $strReturn;

  }
  
  public function boolChangeValue ($strKey = '', $strValue = '', $intId = 0) {
    
    $boolReturn = false;
    
    if (($strKey != '') && ($intId != 0)) {
      
      $arrBgEvent = $this->arrGetBgEvent($intId);
      
      if ($strValue != $arrBgEvent[0][$strKey]) {
      
        $strSql = 'UPDATE ' .$this->strTableName . ' SET `' .MySQLStatic::esc($strKey) .'` = "' .MySQLStatic::esc($strValue) .'" WHERE `beid` = "' .MySQLStatic::esc($intId) .'"';
        //echo $strSql .chr(10);
        $intResult = MySQLStatic::Update($strSql);
        
        $objStream = new Stream('bgevent');
        $intStream = $objStream->intInsertStream($intId, $arrBgEvent[0]['be_name'], $strKey, $arrBgEvent[0][$strKey], $strValue);
      
      }
      
      $boolReturn = true;
      
    }
    
    return $boolReturn;
    
  }
  
  
  public function arrGetSelection ($strSelName = '') {
    $arrReturn = array();
    
    if ($strSelName == 'be_ergebnis') {
      $arrReturn = $this->arrErgebnis;
    }
    if ($strSelName == 'be_auskunftsgeber') {
      $arrReturn = $this->arrAuskunftsgeber;
    }
    if ($strSelName == 'be_status_bearbeitung') {
      $arrReturn = $this->arrStatusBearbeitung;
    }
    if ($strSelName == 'be_sichtbar') {
      $arrReturn = $this->arrSichtbarkeit;
    }
    if ($strSelName == 'be_klaerung_status') {
      $arrReturn = $this->arrKlaerungStatus;
    }
    if ($strSelName == 'be_klaerung_grund') {
      $arrReturn = $this->arrKlaerungGrund;
    }
    if ($strSelName == 'be_klaerung_schritt') {
      $arrReturn = $this->arrKlaerungSchritt;
    }
    if ($strSelName == 'be_dokument_art') {
      $arrReturn = $this->arrDokumentArt;
    }
    if ($strSelName == 'be_stundung') {
      $arrReturn = $this->arrStundung;
    }
    if ($strSelName == 'be_bearbeiter') {
      $arrReturn = $this->arrGetBearbeiter();
    }
    if ($strSelName == 'be_meldepflicht') {
      $arrReturn = $this->arrMeldepflicht;
    }

    return $arrReturn;  
  }

  private function arrGetBearbeiter () {

    $arrReturn = array();

    $strSql = 'SELECT `User`.`Id`, `User`.`Name`, `UserTask`.* FROM `UserTask` INNER JOIN `User` ON `User`.`Id` = `User_Id` WHERE `ut_ta_id` = 2 ORDER BY `User`.`Name` ';
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      foreach ($arrResult as $intKey => $arrUser) {
        $arrReturn[$arrUser['Id']] = $arrUser['Name'];
      }
    }

      return $arrReturn;

  }
  
  public function arrGetBgEvent ($intId = 0) {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    if ($intId != 0) {
      
      $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' WHERE `beid` = "' .MySQLStatic::esc($intId) .'" ';
      $arrResult = MySQLStatic::Query($strSql);
      $intCountResult = count($arrResult);
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }
    
    }
    
    return $arrReturn;


  }

  
  private function arrGetList ($strCondition = '', $boolAktiv = false) {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName;

    if ($boolAktiv === true) {
      $strSql.= ' INNER JOIN `Account` ON `be_meldestelle` = `Account`.`Id` INNER JOIN `Group__c` ON `SF42_Company_Group__c` = `Group__c`.`Id` ';
    }

    $strSql.= ' ' .$strCondition;

    if ($boolAktiv === true) {
      if ($strCondition != '') {
        $strSql.= ' AND `Liefert_Daten__c` = "true" ';
      } else {
        $strSql.= ' WHERE `Liefert_Daten__c` = "true" ';
      }
    }

    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);

    if (@$_SERVER['HTTP_FKLD'] == 'on') {
      //echo $strSql .chr(10);
    }
    
    if ($intCountResult > 0) {
        $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }
  
  public function arrGetBgEventList ($arrFilter = array(), $boolAktiv = false) {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    $strCondition = '';

    $intCountFilter = count($arrFilter);
    if ($intCountFilter > 0) {
      
      $strCondition.= 'WHERE ';  
      foreach ($arrFilter as $intKey => $arrFilter) {
        if (isset($arrFilter['boolQuote']) && ($arrFilter['boolQuote'] === false)) {
          $strCondition.= '' .$arrFilter['strKey'] .' ' .$arrFilter['strType'] .' ' .$arrFilter['strValue'] .'';
        } else {
          $strCondition.= '`' .$arrFilter['strKey'] .'` ' .$arrFilter['strType'] .' "' .$arrFilter['strValue'] .'"';
        }
        if ($intKey < ($intCountFilter - 1)) {
          $strCondition.= ' AND ';
        }
        $strCondition.= ' ';
      }
      
    }
    
if (@$_COOKIE['id'] == '3') {
  //echo $strCondition .chr(10);
}

    $arrReturn = $this->arrGetList($strCondition, $boolAktiv);
    
    return $arrReturn;

  }

  public function getEventListForRequest ($strId = '') {

    $strSql = 'SELECT * FROM `izs_bg_event` WHERE `be_status_bearbeitung` IN ("anzufragen", "angefragt", "erhalten - Klärungsfall") ';
    if ($strId != '') {
        $strSql.= 'AND `be_bg` = "' .$strId .'" ';
    }
    $arrResult = MySQLStatic::Query($strSql);

    return $arrResult;

  }
  
  public function arrGetOpenEventList ($intYear = 0, $intBbId = 0) {
    
    $arrReturn = array();  
    
    if ($intYear != 0) {

      $strSql = 'SELECT *, `Account`.`Name` AS `ac_name` FROM `izs_bg_event` ';
      $strSql.= 'INNER JOIN `Account` ON `be_meldestelle` = `Account`.`Id` ';
      $strSql.= 'INNER JOIN `Verbindung_Meldestelle_BG__c` ON ((`Meldestelle__c` = `be_meldestelle`) AND (`Berufsgenossenschaft__c` = `be_bg`)) ';
      //$strSql.= 'WHERE `be_jahr` = "' .$intYear .'" AND ';
      $strSql.= 'WHERE `Verbindung_Meldestelle_BG__c`.`Aktiv__c` = "true" AND `Befristet_bis__c` IN ';
      //$strSql.= '((YEAR(`be_befristet`) = "' .date('Y') .'") OR (`be_status_bearbeitung` IN ("anzufragen", "angefragt", "erhalten - Klärungsfall"))) ';
      $strSql.= '(`be_status_bearbeitung` IN ("anzufragen", "angefragt", "erhalten - Klärungsfall")) ';
      
      if ($intBbId != 0) {
        $strSql.= ' AND `be_bg` = "' .$intBbId .'" ';
      }

      $strSql.= 'ORDER BY `Account`.`Name`';
      
      //echo $strSql;
      
      $arrResult = MySQLStatic::Query($strSql);
      $intCountResult = count($arrResult);
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }

    }
    
    return $arrReturn;
    
  }

  public function boolInsertSingleEvent ($arrEventInsert = array()) {
    
    $boolReturn = false;
    
    if (count($arrEventInsert) > 0) {
      
      $arrEvent = array();
      $arrEvent['be_meldestelle'] = '';
      $arrEvent['be_bg'] = '';
      $arrEvent['be_jahr'] = '';
      $arrEvent['be_status_anfrage'] = '';
      $arrEvent['be_ergebnis'] = '';
      $arrEvent['be_auskunftsgeber'] = 'Berufsgenossenschaft';
      $arrEvent['be_sperrvermerk'] = 0;
      $arrEvent['be_datum'] = '';
      $arrEvent['be_befristet'] = '';
      $arrEvent['be_link'] = '';
      $arrEvent['be_dokument_art'] = 'Unbedenklichkeitsbescheinigung (UBB)';
      $arrEvent['be_bemerkung'] = '';
      $arrEvent['be_status_bearbeitung'] = 'anzufragen';
      $arrEvent['be_sichtbar'] = '';
      $arrEvent['be_info_intern'] = '';
      $arrEvent['be_wiedervorlage'] = '';
      $arrEvent['be_datum_rueckmeldung'] = '';
      $arrEvent['be_klaerung_status'] = '';
      $arrEvent['be_klaerung_grund'] = '';
      $arrEvent['be_rueckstand'] = '';
      $arrEvent['be_klaerung_schritt'] = '';
      $arrEvent['be_klaerung_ap'] = '';
      $arrEvent['be_klaerung_telefon'] = '';

      $arrEvent['be_name_meldestelle'] = '';
      $arrEvent['be_name_bg'] = '';
      $arrEvent['be_verbindung'] = '';
      $arrEvent['be_mitgliedsnummer'] = '';
      $arrEvent['be_unternehmensnummer'] = '';

      $arrEvent['be_stundung'] = '';
      $arrEvent['be_bearbeiter'] = '';
      $arrEvent['be_meldepflicht'] = '';
      $arrEvent['be_beitragsfaelligkeit'] = '';
      
      $arrEvent = array_merge($arrEvent, $arrEventInsert);

      //print_r($arrEvent); die();
      
      $boolReturn = $this->boolInsertEvent($arrEvent);

    }
    
    return $boolReturn;

  }
  
  private function boolInsertEvent ($arrEvent = array()) {
    
    $boolReturn = false;
    
    if (count($arrEvent) > 0) {

      $objBg = new Bg;
      $arrBgList = $objBg->arrGetBgList();
      
      if (count($arrBgList) > 0) {
        
        $strSql = 'SHOW TABLE STATUS LIKE "izs_bg_event"';
        $arrResult = MySQLStatic::Query($strSql);

        $intId   = $arrResult[0]['Auto_increment'];
        $strName = 'BG-' .str_pad($arrResult[0]['Auto_increment'], 6,'0', STR_PAD_LEFT);
      
        $strSql = 'INSERT INTO ' .$this->strTableName . ' (' .$this->strFields . ') VALUES (';
        $strSql.= '' .$intId .', "' .$strName .'", '; //BG-000048
        $strSql.= '"' .$arrEvent['be_meldestelle'] .'", "' .$arrEvent['be_bg'] .'", ';
        $strSql.= '"' .$arrEvent['be_jahr'] .'", "' .$arrEvent['be_status_anfrage'] .'", ';
        $strSql.= '"' .$arrEvent['be_ergebnis'] .'", "' .$arrEvent['be_auskunftsgeber'] .'", ';
        $strSql.= '"' .$arrEvent['be_sperrvermerk'] .'", "' .$arrEvent['be_datum'] .'", ';
        $strSql.= '"' .$arrEvent['be_befristet'] .'", "' .$arrEvent['be_link'] .'", "' .$arrEvent['be_dokument_art'] .'", ';
        $strSql.= '"' .$arrEvent['be_bemerkung'] .'", "' .$arrEvent['be_status_bearbeitung'] .'", ';
        $strSql.= '"' .$arrEvent['be_sichtbar'] .'", "' .$arrEvent['be_info_intern'] .'", ';
        $strSql.= '"' .$arrEvent['be_wiedervorlage'] .'", "' .$arrEvent['be_datum_rueckmeldung'] .'", ';
        $strSql.= '"' .$arrEvent['be_klaerung_status'] .'", "' .$arrEvent['be_klaerung_grund'] .'", ';
        $strSql.= '"' .$arrEvent['be_rueckstand'] .'", "' .$arrEvent['be_klaerung_schritt'] .'", ';
        $strSql.= '"' .$arrEvent['be_klaerung_ap'] .'", "' .$arrEvent['be_klaerung_telefon'] .'",';

        $strSql.= '"' .$arrEvent['be_name_meldestelle'] .'", "' .$arrEvent['be_mitgliedsnummer'] .'", ';
        $strSql.= '"' .$arrEvent['be_unternehmensnummer'] .'", "' .$arrEvent['be_name_bg'] .'", ';
        $strSql.= '"' .$arrEvent['be_verbindung'] .'", ';
        $strSql.= '"' .$arrEvent['be_stundung'] .'", "' .$arrEvent['be_bearbeiter'] .'", ';
        $strSql.= '"' .$arrEvent['be_meldepflicht'] .'", "' .$arrEvent['be_beitragsfaelligkeit'] .'") ';

        //echo $strSql; die();

        $intResult = MySQLStatic::Insert($strSql);

        $objStream = new Stream('bgevent');
        $intStream = $objStream->intInsertStream($intId, $strName, 'beid', '', $intId, 1);

        $boolReturn = $intResult;
      
      }

    }
    
    return $boolReturn;

  }
  
  private function boolSetAutoIncrement ($intAutoInc = 0) {
    
    $boolReturn = false;
    
    if ($intAutoInc != 0) {

      $strSql = 'ALTER TABLE ' .$this->strTableName . ' AUTO_INCREMENT = ' .MySQLStatic::esc($intAutoInc) .'';
      $arrResult = MySQLStatic::Query($strSql);
      $boolReturn = true;

    }   
      
    return $boolReturn;
  
  }
  
  
  public function boolDeleteEvent ($intYear = 0, $intId = 0) {
    
    $boolReturn = false;
    
    if ($intId != 0) {

      $arrBgEvent = $this->arrGetBgEvent($intId);
      
      $strSql = 'DELETE FROM ' .$this->strTableName . ' WHERE `beid` = "' .MySQLStatic::esc($intId) .'"';
      $arrResult = MySQLStatic::Query($strSql);

      $objStream = new Stream('bgevent');
      $intStream = $objStream->intInsertStream($intId, $arrBgEvent[0]['be_name'], 'beid', '', $intId, 5);

      $boolReturn = true;
   
    }   
      
    if ($intYear != 0) {
      
      $strSql = 'DELETE FROM ' .$this->strTableName . ' WHERE `be_jahr` = "' .MySQLStatic::esc($intYear) .'"';
      $arrResult = MySQLStatic::Query($strSql);
      
      $strSql = 'SELECT MAX(`beid`) AS `autoinc` FROM ' .$this->strTableName . '';
      $arrResult = MySQLStatic::Query($strSql);
      
      if ($arrResult[0]['autoinc'] == '') {
        $intAutoInc = 300;
      } else {
        $intAutoInc = $arrResult[0]['autoinc'] + 1;
      }
      
      $boolAutoInc = $this->boolSetAutoIncrement($intAutoInc);
      $boolReturn = true;
   
    }   
      
    return $boolReturn;

  }

  public function boolCreateEventsFromYear ($intYear = '') {
    
    $boolReturn = false;
    
    if ($intYear != '') {

      $objBg = new Bg;
      $arrBgList = $objBg->arrGetBgList();
      
      if (count($arrBgList) > 0) {
            
        //$boolDelete = $this->boolDeleteEvent($intYear);
      
        foreach ($arrBgList as $intKey => $arrBg) {
          
          $arrListMeldestelle = $objBg->arrGetMeldestellen($arrBg['acid']);
          
          //print_r($arrListMeldestelle);
          
          if (count($arrListMeldestelle) > 0) {
            
            foreach ($arrListMeldestelle as $intKey => $arrMeldestelle) {
              
              $arrEvent['be_meldestelle'] = $arrMeldestelle['acid_meldestelle'];
              $arrEvent['be_bg'] = $arrBg['acid'];
              $arrEvent['be_jahr'] = $intYear;
              $arrEvent['be_status_anfrage'] = '';
              $arrEvent['be_ergebnis'] = '';
              $arrEvent['be_auskunftsgeber'] = 'Berufsgenossenschaft';
              $arrEvent['be_sperrvermerk'] = 0;
              $arrEvent['be_datum'] = '';
              $arrEvent['be_befristet'] = '';
              $arrEvent['be_link'] = '';

              if (date('Y-m-d') < '2023-05-10') {
                $strDocArt = 'Unbedenklichkeitsbescheinigung (UBB)';
              } else {
                $strDocArt = 'Dynamische Erzeugung';
              }

              $arrEvent['be_dokument_art'] = $strDocArt;
              $arrEvent['be_bemerkung'] = '';
              $arrEvent['be_status_bearbeitung'] = 'anzufragen';
              $arrEvent['be_sichtbar'] = '';
              $arrEvent['be_info_intern'] = '';
              $arrEvent['be_wiedervorlage'] = '';
              $arrEvent['be_datum_rueckmeldung'] = '';
              $arrEvent['be_klaerung_status'] = '';
              $arrEvent['be_klaerung_grund'] = '';
              $arrEvent['be_rueckstand'] = '';
              $arrEvent['be_klaerung_schritt'] = '';
              $arrEvent['be_klaerung_ap'] = '';
              $arrEvent['be_klaerung_telefon'] = '';
              
              $this->boolInsertEvent($arrEvent);
              
              //print_r($arrEvent);

            }
            
          }
          
        }
        
        $boolReturn = true;
      
      }

    }
    
    return $boolReturn;

  }
  
  
  public function arrGetBgListEvent ($intYear = 0) {
    
    $arrReturn = array();  
    
    if ($intYear != 0) {
      
      $strCondition = 'WHERE `be_jahr` = "' .$intYear .'" AND ((`be_status_bearbeitung` IN ("anzufragen", "angefragt", "erhalten - Klärungsfall")) OR (YEAR(`be_befristet`) = "' .$intYear .'")) GROUP BY `be_bg`';
      $arrList = $this->arrGetList($strCondition);
      
      //print_r($arrList); die();
      
      if (count($arrList) > 0) {
        
        $arrAnfrageBg = array();
        foreach ($arrList as $intKey => $arrEvent) {
          $arrAnfrageBg[] = $arrEvent['be_bg'];
        }
        
        $objBg = new Bg;
        
        $strCondition = 'WHERE `Id` IN (' .strArrayImplode($arrAnfrageBg, '"') .') ORDER BY `Name`';
        $arrReturn = $objBg->arrGetBgListFromCondition($strCondition);
        
      }
    
    }
    
    return $arrReturn;
    
  }

  
  public function arrGetAnschreibenList ($intType = 0, $strWorkflow = '') {
    
    $arrReturn = array();  
    
    if ($intType != 0) {
      
      $strSql = 'SELECT `ct_id`, `ct_name` FROM `cms_text` WHERE `ct_type` = 3 ';
      if ($strWorkflow != '') {
        $strSql.= 'AND `ct_workflow` = "' .$strWorkflow .'" ';
      }
      $strSql.= 'ORDER BY `ct_name`';
      $arrResult = MySQLStatic::Query($strSql);
      $intCountResult = count($arrResult);      
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }
    
    }
    
    return $arrReturn;
  
  }
  
  
  
  public function arrGetAnschreiben ($intId = 0) {
    
    $arrReturn = array();  
    
    if ($intId != 0) {
      
      $strSql = 'SELECT `ct_id`, `ct_name`, `ct_head`, `ct_text` FROM `cms_text` WHERE `ct_id` = "' .$intId .'"';
      $arrResult = MySQLStatic::Query($strSql);
      $intCountResult = count($arrResult);      
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }
    
    }
    
    return $arrReturn;
  
  }
  
  public function strGetMitgliedsnummer ($strIdBg = '', $strIdMe = '') {
    
    $strReturn = '';
    
    if (($strIdBg != '') && ($strIdMe != '')) {
    
      $strSql = 'SELECT `Mitgliedsnummer_BG__c` FROM `Verbindung_Meldestelle_BG__c` WHERE `Meldestelle__c` = "' .MySQLStatic::esc($strIdMe) .'" AND ';
      $strSql.= '`Berufsgenossenschaft__c` = "' .MySQLStatic::esc($strIdBg) .'" AND `Verbindung_Meldestelle_BG__c`.`Aktiv__c` = "true"';
      $arrSql = MySQLStatic::Query($strSql);
      
      if (count($arrSql) > 0) {
        $strReturn = $arrSql[0]['Mitgliedsnummer_BG__c'];
      }
    
    }
    
    return $strReturn;
    
  }
  
  public function strGetUnternehmensnummer ($strIdBg = '', $strIdMe = '') {
    
    $strReturn = '';
    
    if (($strIdBg != '') && ($strIdMe != '')) {
    
      $strSql = 'SELECT `Unternehmensnummer_BG__c` FROM `Verbindung_Meldestelle_BG__c` WHERE `Meldestelle__c` = "' .MySQLStatic::esc($strIdMe) .'" AND ';
      $strSql.= '`Berufsgenossenschaft__c` = "' .MySQLStatic::esc($strIdBg) .'" AND `Verbindung_Meldestelle_BG__c`.`Aktiv__c` = "true"';
      $arrSql = MySQLStatic::Query($strSql);
      
      if (count($arrSql) > 0) {
        $strReturn = $arrSql[0]['Unternehmensnummer_BG__c'];
      }
    
    }
    
    return $strReturn;
    
  }

}

?>