<?php

require_once(APP_PATH .'assets/classes/class.mysql.php');
require_once(APP_PATH .'cms/classes/class.bg.php');

class SendRequest {
  
  private $strTableName = '';
  private $strFields = '';
  private $arrFields = array(
  );
  
  function __construct () {
    
    //$this->strFields = MySQLStatic::ImplodeFields($this->arrFields);
    
  }
  
  
  public function boolWriteLog ($strId = '', $intYear = '', $strBnr = '') {
    
    $boolReturn = false;
    
    if (($strId != '')) {
    
      $arrReq = $this->arrGetLog($strId, $intYear, $strBnr);
      
      if (count($arrReq) > 0) {
        $strSql = 'UPDATE `cms_sent` SET `cs_sent` = NOW() WHERE `cs_id` = "' .$arrReq[0]['cs_id'] .'"';
        MySQLStatic::Update($strSql);
      } else {
        $strSql = 'INSERT INTO `cms_sent` (`cs_account_id`, `cs_type`, `cs_month`, `cs_year`, `cs_sent`, `cs_evid`) VALUES ("' .$strId .'", "mail", "", "' .$intYear .'", NOW(), "' .$strBnr .'")';
        MySQLStatic::Insert($strSql);
      }

      $boolReturn = true;
      
    }
    
    return $boolReturn;
    
  }

  public function arrGetLog ($strId = '', $intYear = '', $strBnr = '') {
    
    $arrReturn = false;
    
    if ($strId != '') {
    
      $strSql = 'SELECT * FROM `cms_sent` WHERE `cs_account_id` = "' .$strId .'" AND `cs_year` = "' .$intYear .'" AND `cs_evid` = "' .$strBnr .'"';
      $arrReturn = MySQLStatic::Query($strSql);
      
    }
    
    return $arrReturn;
    
  }

}

?>