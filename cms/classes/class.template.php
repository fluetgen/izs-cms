<?php

require_once(APP_PATH .'assets/classes/class.mysql.php');

class Template {
  
  

  function __construct () {
    
        ;
    
  }
  
  private function getTemplate ($intId = 0) {
    
    $arrReturn = array();

    $strSql = 'SELECT `ct_head`, `ct_text` FROM `cms_text` WHERE `ct_id` = "' .$intId .'" ';
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
        $arrReturn = $arrResult[0];
    }
    
    return $arrReturn;

  }

  private function replacePlaceholder ($arrTemplate = array(), $arrPlaceholder = array()) {

    $arrReturn = array();

    $arrReturn = $arrTemplate;

    if ((count($arrTemplate) > 0) && (count($arrPlaceholder) > 0)) {

        foreach ($arrTemplate as $strKey => $strValue) {

            foreach ($arrPlaceholder as $strPlaceholder => $strReplacement) {

                $strValue = str_replace($strPlaceholder, $strReplacement, $strValue);

            }

            $arrReturn[$strKey] = $strValue;

        }

    }

    return $arrReturn;

  }


  public function getBgPlaceholder ($intEventId = 0) {

    $arrReturn = array();

    $strSql = 'SELECT * FROM `izs_bg_event` WHERE `beid` = "' .$intEventId .'"';
    $arrEvent = MySQLStatic::Query($strSql);

    $arrReturn['{Year}'] = $arrEvent[0]['be_jahr'];
    // {Month} {Sachbearbeiter KK} {Sachbearbeiter KK Telefon} {DOWNLOAD_VOLLMACHTEN} {DOWNLOAD_ANFRAGELISTE}

    $strSql = 'SELECT * FROM `Account` WHERE `Id` = "' .$arrEvent[0]['be_meldestelle'] .'"';
    $arrMeldestelle = MySQLStatic::Query($strSql);

    $strSql = 'SELECT * FROM `Account` WHERE `Id` = "' .$arrEvent[0]['be_bg'] .'"';
    $arrBg = MySQLStatic::Query($strSql);

    $arrReturn['{Beitragszahler}'] = $arrMeldestelle[0]['Name'];  
    $arrReturn['{BNR Beitragszahler}'] = $arrMeldestelle[0]['SF42_Comany_ID__c'];  
    $arrReturn['{BG}'] = $arrBg[0]['Name'];  

    $arrReturn['{Erledigung bis}'] = date('d.m.Y', strtotime($arrEvent[0]['be_wiedervorlage']));  
    $arrReturn['{Event Id}'] = $arrEvent[0]['be_name'];

    $arrReturn['{Mitgliedsnummer}'] = $arrEvent[0]['be_mitgliedsnummer'];
    $arrReturn['{Unternehmensnummer}'] = $arrEvent[0]['be_unternehmensnummer'];

    $strText['{Today}'] = date('d.m.Y');

    $arrReturn['{AP Klärung}'] = $arrEvent[0]['be_klaerung_ap'];  
    $arrReturn['{Tel Klärung}'] = $arrEvent[0]['be_klaerung_telefon'];  
    $arrReturn['{Grund}'] = $arrEvent[0]['be_klaerung_grund'];  

    if ($arrEvent[0]['be_beitragsfaelligkeit'] != '0000-00-00') {
      $strDate = date('m/Y', strtotime($arrEvent[0]['be_beitragsfaelligkeit']));
    } else {
      $strDate = '';
    }

    $arrReturn['{Faelligkeit}'] = $strDate;

    return $arrReturn;

  }  

  private function getSvPlaceholder ($intEventId = 0) {

    $arrReturn = array();

    /*
    $strSql = 'SELECT * FROM `SF42_IZSEvent__c` WHERE `evid` = "' .$_REQUEST['det'] .'"';
    $event  = MySQLStatic::Query($strSql);
  
    $strSql = 'SELECT * FROM `Account` WHERE `Id` = "' .$event[0]['SF42_informationProvider__c'] .'"';
    $Ip  = MySQLStatic::Query($strSql); // == KK
  
    $strSql = 'SELECT * FROM `Account` WHERE `SF42_Comany_ID__c` = "' .$event[0]['Betriebsnummer_ZA__c'] .'"';
    $Pp  = MySQLStatic::Query($strSql);
  
    $arrReturn['{Month}'] = $event[0]['SF42_Month__c'];  
    $arrReturn['{Year}'] = $event[0]['SF42_Year__c'];  
  
    $arrReturn['{Salutation}'] = $Ip[0]['Anrede_Anschreiben__c'] .' ' .$Ip[0]['Nachname__c'];  
    $arrReturn['{Beitragszahler}'] = $Pp[0]['Name'];  
    $arrReturn['{BNR Beitragszahler}'] = $Pp[0]['SF42_Comany_ID__c'];  
    $arrReturn['{Krankenkasse}'] = $Ip[0]['Name'];  
    $arrReturn['{Sachbearbeiter KK}'] = $Ip[0]['Anrede_Briefkopf__c'] .' ' .$Ip[0]['Nachname__c'];  
    $arrReturn['{Sachbearbeiter KK Telefon}'] = $Ip[0]['Phone'];  
    $arrReturn['{Erledigung bis}'] = $event[0]['bis_am__c'];  
    $arrReturn['{Event Id}'] = $event[0]['Name'];  

    $strText['{Salutation}'] = $strSalut, $strText);
    $strText['{Today}'] = date('d.m.Y'), $strText);
    $strText['{Month}'] = str_pad($arrPeriod[0], 2, "0", STR_PAD_LEFT), $strText);
    $strText['{Year}'] = $arrPeriod[1], $strText);

    $arrReturn['{AP Klärung}'] = $event[0]['AP_Klaerung'];  
    $arrReturn['{Tel Klärung}'] = $event[0]['Tel_Klaerung'];  
    $arrReturn['{Grund}'] = $event[0]['Grund__c'];  
    
    $strNameFiles = 'id=' .$arrAcc[0]['Id'] .'&y=' .$arrPeriod[1] .'&m=' .$arrPeriod[0];
    
    $strText['{DOWNLOAD_VOLLMACHTEN}'] = '<a href="http://www.izs-institut.de/cms/_download.php?t=a&' .$strNameFiles .'&evid=' .$_REQUEST['evid'] .'">Vollmachten_' .$arrAcc[0]['Name'] .'.pdf</a>', $strText);
    $strText['{DOWNLOAD_ANFRAGELISTE}'] = '<a href="http://www.izs-institut.de/cms/_download.php?t=r&' .$strNameFiles .'&evid=' .$_REQUEST['evid'] .'">Anfrageliste_' .$arrAcc[0]['Name'] .'.pdf</a>', $strText);
    */
  
    return $arrReturn;

  }  

  public function renderTemplate ($intId = 0, $intEventId = 0, $strType = '') {

    $arrReturn = array();

    $arrTemplate = $this->getTemplate($intId);

    if ($strType == 'BG') {

        $arrPlaceholder = $this->getBgPlaceholder($intEventId);

    } elseif ($strType == 'SV') {

        $arrPlaceholder = $this->getSvPlaceholder($intEventId);

    }

    $arrReturn = $this->replacePlaceholder($arrTemplate, $arrPlaceholder);

    return $arrReturn;

  }

}

?>