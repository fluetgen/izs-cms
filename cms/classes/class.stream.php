<?php

require_once(APP_PATH .'assets/classes/class.mysql.php');

class Stream {
  
  private $strStream = '';
  private $strTableName;
    
  private $arrTableName = array (
    'basic' => '`izs_basis_change`',
    'event' => '`izs_event_change`', 
    'bgevent' => '`izs_bgevent_change`'
  );
  
  private $arrTableId = array (
    'basic' => '`bc_ddid`',
    'event' => '`ev_id`',
    'bgevent' => '`ev_id`'
  );
  
  private $arrTableOrder = array (
    'basic' => '`bc_time`  DESC, `bc_type`',
    'event' => '`ev_time`',
    'bgevent' => '`ev_time`'
  );

  private $arrFields = array(
    'basic' => array(
      "`bcid`",
      "`bc_ddid`", 
      "`bc_clid`",
      "`bc_grid`", 
      "`bc_fild`", 
      "`bc_old`",
      "`bc_new`",
      "`bc_time`",
      "`bc_type`"
    ), 
    'event' => array(
      "`ec_id`",
      "`ev_id`",
      "`cl_id`",
      "`ev_name`",
      "`ec_fild`",
      "`ec_old`",
      "`ec_new`",
      "`ec_time`",
      "`ec_type`"
    ),
    'bgevent' => array(
      "`ec_id`",
      "`ev_id`",
      "`cl_id`",
      "`ev_name`",
      "`ec_fild`",
      "`ec_old`",
      "`ec_new`",
      "`ec_time`",
      "`ec_type`"
    )
  );
  
  function __construct ($strName = '') {
    
    $this->strStream = $strName;
    $this->strTableName = $this->arrTableName[$strName];
    $this->strTableId   = $this->arrTableId[$strName];
    $this->strTableOrder = $this->arrTableOrder[$strName];
    $this->strFields = MySQLStatic::ImplodeFields($this->arrFields[$strName]);
    
  }
  
  public function arrGetStream ($strId = 0) {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    if ($strId != '') {
      
      $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' WHERE ' .$this->strTableId .' = "' .MySQLStatic::esc($strId) .'"';
      $arrResult = MySQLStatic::Query($strSql);
      $intCountResult = count($arrResult);
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }
    
    }
    
    return $arrReturn;


  }

  private function arrGetList ($strCondition = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' ' .$strCondition;
    
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }  

  public function arrGetStreamList ($strId = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $strCondition = ' WHERE ' .$this->strTableId .' = "' .MySQLStatic::esc($strId) .'" ORDER BY ' .$this->strTableOrder .'';
    
    $arrReturn = $this->arrGetList($strCondition);
        
    return $arrReturn;

  }
  
  public function intInsertStream ($intId = '', $strIndex = '', $strField = '', $strOld = '', $strNew = '', $intType = 2) {
    
    $intReturn = 0;
    
    $strSql = 'INSERT INTO ' .$this->strTableName . ' (' .$this->strFields .') VALUES (NULL, ';
    $strSql.= '"' .MySQLStatic::esc($intId) .'", "' .MySQLStatic::esc($_SESSION['id']) .'", ';
    $strSql.= '"' .MySQLStatic::esc($strIndex) .'", "' .MySQLStatic::esc($strField) .'", ';
    $strSql.= '"' .MySQLStatic::esc($strOld) .'", "' .MySQLStatic::esc($strNew) .'", ';
    $strSql.= 'NOW(), "' .MySQLStatic::esc($intType) .'" ';
    $strSql.= ')';
    
    $intReturn = MySQLStatic::Insert($strSql);
    
    return $intReturn;
  
  }

}

?>