<?php

require_once(APP_PATH .'assets/classes/class.mysql.php');

class Contact {
  
  private $strTableName = '`Contact`';
  private $strFields = '';
  private $arrFields = array(
    "`Contact`.`Id` AS `coid`",
    "`Contact`.`IsDeleted` AS `co_deleted`",
    "`AccountId` AS `co_acid`",
    "`LastName` AS `co_last`",
    "`FirstName` AS `co_first`",
    "`Salutation` AS `co_salut`",
    "`RecordTypeId` AS `co_type`",
    "`MailingStreet` AS `co_street`",
    "`MailingCity` AS `co_city`",
    "`MailingState` AS `co_state`",
    "`MailingPostalCode` AS `co_zip`",
    "`MailingCountry` AS `co_country`",
    "`Phone` AS `co_phone`",
    "`OtherPhone` AS `co_phone_other`", 
    "`Fax` AS `co_fax`",
    "`Email` AS `co_mail`",
    "`Title` AS `co_title`",
    "`Funktion__c` AS `co_function`",
    "`Aktiv__c` AS `co_active`",
    "`Info__c` AS `co_info`",
    "`Zugehoerige_Anfragestelle__c` AS `co_anfrage`",
    "`Ansprechpartner_fuer__c` AS `co_ap`"
  );
  
  function __construct () {
    
    $this->strFields = MySQLStatic::ImplodeFields($this->arrFields);
    
  }
  
  public function arrGetContact ($strId = 0) {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    if ($strId != '') {
      
      $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' WHERE `Id` = "' .MySQLStatic::esc($strId) .'" AND `Aktiv__c` = "true"';
      $arrResult = MySQLStatic::Query($strSql);
      $intCountResult = count($arrResult);
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }
    
    }
    
    return $arrReturn;


  }

  
  private function arrGetList ($strCondition = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' ' .$strCondition;
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }
  
  public function arrGetContactList () {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $arrReturn = $this->arrGetList();
    
    return $arrReturn;

  }
  
  public function arrGetContactListFromIp ($strIp = '', $strAp = '', $boolPortal = false, $hasEmail = false) {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    $strCondition = ' WHERE `AccountId` = "' .MySQLStatic::esc($strIp) .'" AND `Aktiv__c` = "true" ';
    
    if ($strAp != '') {
      $strCondition.= 'AND `Ansprechpartner_fuer__c` LIKE "%' .MySQLStatic::esc($strAp) .'%" ';
    }
    if ($boolPortal) {
      $strCondition.= 'AND `Portal_User__c` = "Entleiher-Portal" ';
    }
    if ($hasEmail) {
      $strCondition.= 'AND `Email` != "" ';
    }
    
    $strCondition.= 'ORDER BY `LastName`';
      
    $arrReturn = $this->arrGetList($strCondition);
    
    return $arrReturn;

  }
  
  public function arrGetContactListFromIpPp ($strIp = '', $strPp = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' ';
    $strSql.= 'INNER JOIN `Kontakt_IP_zu_PP__c` ON `Kontakt_IP_zu_PP__c`.`Kontakt__c` = ' .$this->strTableName .'.`Id` ';
    $strSql.= 'WHERE `Premium_Payer__c` = "' .$strPp .'" AND `Information_Provider_del__c` = "' .$strIp .'" AND ' .$this->strTableName .'.`Aktiv__c` = "true" ';
    $strSql.= 'ORDER BY `LastName`';
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }

  
  public function arrGetIPPpFromContact ($strCo = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    $strSql = 'SELECT `Premium_Payer__c` AS `ppid`, `Information_Provider_del__c` AS `ipid` FROM `Kontakt_IP_zu_PP__c` ';
    $strSql.= 'WHERE `Kontakt__c` = "' .$strCo .'"'; // AND `Aktiv__c` = "true"';
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }
  
  public function arrGetContactFromAccountList ($arrAcc = array(), $strIp = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    $strAccList = implode('", "', $arrAcc);
    
    $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' ';
    $strSql.= 'INNER JOIN `Kontakt_IP_zu_PP__c` ON ' .$this->strTableName .'.`Id` = `Kontakt_IP_zu_PP__c`.`Kontakt__c` ';
    $strSql.= 'WHERE `Premium_Payer__c` IN ("' .$strAccList .'") AND `Kontakt_IP_zu_PP__c`.`Information_Provider_del__c` = "' .$strIp .'" AND ' .$this->strTableName .'.`Aktiv__c` = "true" ';
    $strSql.= 'GROUP BY ' .$this->strTableName .'.`Id` ORDER BY ' .$this->strTableName .'.`LastName`';
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }
  
  public function arrGetMailFromAccountList ($arrAcc = array(), $strAttribute = '', $strGrId = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    $strAccList = implode('", "', $arrAcc);
    
    $strSql = 'SELECT `Email` AS `co_mail` FROM ' .$this->strTableName .' ';
    $strSql.= 'WHERE (`AccountId` IN ("' .$strAccList .'") ';

    if ($strGrId != '' ) {
      $strSql.= 'OR (`GroupId` = "' .$strGrId .'") ';
    }    
    
    $strSql.= ') AND `Email` != "" AND `Aktiv__c` = "true" ';
    if ($strAttribute != '' ) {
      $strSql.= 'AND `Ansprechpartner_fuer__c` LIKE "%' .$strAttribute .'%" ';
    }

    //echo $strSql;

    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);

    if ($intCountResult > 0) {
      foreach ($arrResult as $intKey => $arrMail) {
        $arrReturn[] = $arrMail['co_mail'];
      }
    }
        
    return $arrReturn;

  }

}



?>