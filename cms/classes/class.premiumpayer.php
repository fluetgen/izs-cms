<?php

if (!class_exists('MySQLStatic')) {
  require_once(APP_PATH .'assets/classes/class.mysql.php');
}

class PremiumPayer {
  
  private $strRecordTypeId = ''; //
  private $strTableName = '`Account`';
  private $strFields = '';
  private $arrFields = array(
    "`Id` AS `acid`",
    "`RecordTypeId`",
    "`IsDeleted` AS `ac_deleted`",
    "`Name` AS `ac_name`",
    "`SF42_Company_Group__c` AS `ac_cgid`",
    "`SF42_Comany_ID__c` AS `ac_betriebsnummer`",
    
    "`BillingStreet` AS `ac_billing_street`", 
    "`BillingPostalCode` AS `ac_billing_zip`", 
    "`BillingCity` AS `ac_billing_city`"
  );
  
  function __construct () {
    
    $this->strFields = MySQLStatic::ImplodeFields($this->arrFields);
    
  }

  public function arrGetPremiumPayerList () {
    
    $arrReturn = array();
      
    $strSql = 'SELECT * FROM `izs_premiumpayer`';
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }  

  public function arrGetPremiumPayerGroupList () {
    
    $arrReturn = array();
        
    $strSql = 'SELECT * FROM `izs_premiumpayer_group`';
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }   

  public function arrGetPP ($strId = 0, $boolStrict = true) {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    if ($strId != '') {
      
      $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' WHERE `Id` = "' .MySQLStatic::esc($strId) .'"';
      if ($boolStrict === true) {
        $strSql.= ' AND `SF42_Comany_ID__c` != "" ';
      }
      $arrResult = MySQLStatic::Query($strSql);
     
      $intCountResult = count($arrResult);
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }
    
    }
    
    return $arrReturn;


  }
  
  
  public function arrGetAccount ($strId = 0) {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    if ($strId != '') {
      
      $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' WHERE `Id` = "' .MySQLStatic::esc($strId) .'" ';
      $arrResult = MySQLStatic::Query($strSql);
      $intCountResult = count($arrResult);
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }
    
    }
    
    return $arrReturn;


  }

  
  private function arrGetList ($strCondition = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' ' .$strCondition;
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }
  
  public function arrGetPPList () {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    $strCondition = 'WHERE `SF42_Comany_ID__c` != ""';  
    $arrReturn = $this->arrGetList();
    
    return $arrReturn;

  }
  
  
  public function arrGetPPFromGroup ($strId = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    $strCondition = 'WHERE `SF42_Comany_ID__c` != "" AND `SF42_Company_Group__c` = "' .$strId .'"';  
    $arrReturn = $this->arrGetList($strCondition);
    
    return $arrReturn;

  }
  
  public function arrGetPPFromBetriebsnummer ($strBetriebsnummer = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    $strCondition = 'WHERE `SF42_Comany_ID__c` = "' .$strBetriebsnummer .'"';  
    $arrReturn = $this->arrGetList($strCondition);
    
    return $arrReturn;

  }
  
  public function arrGetPPFromIdList ($arrIdList = array()) {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    if (is_array($arrIdList) && (count($arrIdList) > 0)) {
      $strCondition = 'WHERE `Id` IN ("' .implode('", "', $arrIdList) .'") ORDER BY `Name`';  
      $arrReturn = $this->arrGetList($strCondition);
    }
    
    return $arrReturn;

  }
  
  public function strGetIdFromBetriebsnummer ($strBetriebsnummer = '') {
  
    $strReturn = '';
    
    $arrAccount = $this->arrGetPPFromBetriebsnummer($strBetriebsnummer);
    $strReturn = $arrAccount[0]['acid'];
      
    return $strReturn;
  
  }
  
  public function strGetBetriebsnummerFromId ($strId = '') {
  
    $strReturn = '';
    
    $arrAccount = $this->arrGetPP($strId);
    $strReturn = $arrAccount[0]['ac_betriebsnummer'];
      
    return $strReturn;
  
  }

  public function arrGetAccountListFromGroup ($strId = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();

    $strSql = 'SELECT `Id` AS `acid` FROM ' .$this->strTableName .' ';
    $strSql.= 'WHERE `SF42_Company_Group__c` = "' .$strId .'"';
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      foreach ($arrResult as $intKey => $arrAcc) {
        $arrReturn[] = $arrAcc['acid'];
      }
    }
    
    return $arrReturn;

  }

  public function arrGetEntleiherListFromGroup ($strId = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();

    $strSql = 'SELECT `Entleiher__c` AS `acid` FROM `Entleiher_CompGroup_Verbindung__c` WHERE `Company_Group__c` = "' .$strId .'"';
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      foreach ($arrResult as $intKey => $arrAcc) {
        $arrReturn[] = $arrAcc['acid'];
      }
    }
    
    return $arrReturn;

  }

  public function arrGetUbbList () {
    
    $arrReturn = array();

    $strSql = 'SELECT * FROM `izs_ubb_senden` ORDER BY `Name`';
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }
 

}



?>