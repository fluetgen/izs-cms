<?php

require_once('../assets/classes/class.mysql.php');

class Anfragestelle {
  
  private $strTableName = '`Anfragestelle__c`';
  private $strFields = '';
  private $arrFields = array(
    "`Id` AS `asid`",
    "`IsDeleted` AS `as_deleted`",
    "`Name` AS `as_name`",
    "`CATCH_ALL__c` AS `as_catchall`",
    "`Anfrageweg__c` AS `as_`",
    "`Abteilung_Team__c` AS `as_unit`",
    "`Anrede__c` AS `as_salut`",
    "`Vorname__c` AS `as_first`",
    "`Name__c` AS `as_last`",
    "`Strasse_Hausnummer__c` AS `as_street`",
    "`PLZ__c` AS `as_zip`",
    "`Ort__c` AS `as_city`",
    "`Telefon_1__c` AS `as_phone`",
    "`Telefon_2__c` AS `as_phone2`",
    "`Fax__c` AS `as_fax`",
    "`E_Mail_Adressen__c` AS `as_mail`",
    "`Information_Provider__c` AS `as_acid`",
    "`Notiz__c` AS `as_note`",
    "`Name_Extern__c` AS `as_nameextern`",
    "`Abweichender_Anfrageprozess__c` AS `as_altrequest`"
  );

  
  function __construct () {
    
    $this->strFields = MySQLStatic::ImplodeFields($this->arrFields);
    
  }
  
  public function arrGetAnfragestelle ($strId = 0) {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    if ($strId != '') {
      
      $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' WHERE `Id` = "' .MySQLStatic::esc($strId) .'"';
      $arrResult = MySQLStatic::Query($strSql);
      $intCountResult = count($arrResult);
      
      if ($intCountResult > 0) {
        $arrReturn = $arrResult;
      }
    
    }
    
    return $arrReturn;


  }

  
  private function arrGetList ($strCondition = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $strSql = 'SELECT ' .$this->strFields . ' FROM ' .$this->strTableName .' ' .$strCondition;
    $arrResult = MySQLStatic::Query($strSql);
    $intCountResult = count($arrResult);
    
    if ($intCountResult > 0) {
      $arrReturn = $arrResult;
    }
    
    return $arrReturn;

  }
  
  public function arrGetAnfragestelleList () {
    
    global $arrDatabase;
    
    $arrReturn = array();
      
    $arrReturn = $this->arrGetList();
    
    return $arrReturn;

  }
  
  public function arrGetIPFromAnfragestelle ($strIp = '') {
    
    global $arrDatabase;
    
    $arrReturn = array();
    
    $strCondition = ' WHERE `Id` = "' .MySQLStatic::esc($strIp) .'"';
      
    $arrAnfragestelle = $this->arrGetList($strCondition);
    
    $objIp = new InformationProvider();
    $arrReturn = $objIp->arrGetIp($arrAnfragestelle[0]['as_acid']);
    
    return $arrReturn;

  }
  
  public function strGetAnfragestelleFromIpPp ($strIp = '', $strPp = '') {
    
    $strReturn = '';
    
    $strSql = 'SELECT `Anfragestelle__c` FROM `Dezentrale_Anfrage_KK__c` ';
    $strSql.= 'WHERE `Premium_Payer__c` = "' .$strPp .'" ';
    $strSql.= 'AND `Information_Provider__c` = "' .$strIp .'"';
    
    $arrResult = MySQLStatic::Query($strSql);
    
    if (count($arrResult) > 0) {
      $strReturn = $arrResult[0]['Anfragestelle__c'];
    }
    
    return $strReturn;
  
  }
  
  public function arrGetAnfragestelleFromEvent ($strId = '') {
    
    $arrReturn = array();
    
    $objEvent = new Event;
    $arrEvent = $objEvent->arrGetEvent($strId);
    
    $objIp = new InformationProvider;
    $arrIp = $objIp->arrGetIp($arrEvent[0]['ev_ip_acid']);
    
    $objPp = new PremiumPayer;
    $arrPp = $objPp->arrGetPPFromBetriebsnummer($arrEvent[0]['ev_ac_betriebsnummer']);
    
    if (count($arrIp) > 0) {
      if ($arrIp[0]['ac_dezentral'] == 'true') {
        
        $strAnfragestelle = $this->strGetAnfragestelleFromIpPp($arrIp[0]['acid'], $arrPp[0]['acid']);
        
        if ($strAnfragestelle != '') {
        
          $arrReturn = $this->arrGetAnfragestelle($strAnfragestelle);
        
        } else { //CATCH_ALL
          
          $strCondition = ' WHERE `Information_Provider__c` = "' .$arrIp[0]['acid'] .'" AND `CATCH_ALL__c` = "true"';
          $arrReturn = $this->arrGetList($strCondition);
          
        }
        
      } else {
        
        $arrReturn = $arrIp;
        $arrReturn[0]['as_name'] = $arrIp[0]['ac_name'];
        
      }
    }
    
    return $arrReturn;
    
  }


}



?>