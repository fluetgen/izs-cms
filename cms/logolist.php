<?php

session_start();

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  header('Location: /cms/index.php?ac=auth');
}

$strOutput = '';

$strOutput.= '<h1>Logo Liste</h1>' .Chr(10);

$strOutput.= '

<p><strong>ACHTUNG!</strong> Die Liste ist optimiert für E-Mail-Versand. Für für Powerpoint bitte <a href="https://izs-institut.box.com/s/7cf7z550bayeybgwjl58nv87h5q7f0rw" target="_blank">diese Vorlage (Referenzliste.pptx)</a> verwenden.</p>

<p>Link: <a href="http://api.izs-institut.de/apps/logo/logo_fix.php?shuffle=1" target="_blank">http://api.izs-institut.de/apps/logo/logo_fix.php?shuffle=1</a></p>

<p>Bei jedem Aufruf wird die Liste in einer anderen Reihenfolge zusammengesetzt. Wenn das "Bild" nicht gefällt, einfach neu laden. <br />
Für die Zusammenstellung werden immer 20 Logos (4 Reihen à 5 Logos) verwendet. Wenn weniger als 20 im Prio1-Ordner sind, wird entsprechend mit Prio2 aufgefüllt.</P

<p>Dann einen Screenshot machen und den INNERHALB des schwarzen Rahmens zuschneiden.</p>

';

?>