<?php

$strChartTemplate = "{
    type: 'line',
        data: {
            labels: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
            datasets: [{
                label: '',
                backgroundColor: 'rgb(98, 181, 99)',
                borderColor: 'rgb(98, 181, 99)',
                data: [*DATA*],
                fill: false,
                pointRadius: 0,
                borderWidth: 2
            }],
    },
    options: {
        layout: {
            padding: {
                left: 10,
                right: 10
            },
        },
        title: {
            display: false,
            text: '',
        },
        legend: {
            display: false
        },
        plugins: {
            datalabels: {
                display: true,
                align: 'top',
                padding: 5,
                backgroundColor: '',
                color: '#000000',
                borderRadius: 20,
            }
        },
        scales: {
            xAxes: [{
                ticks: {
                    fontColor: '#000000',
                    padding: 10,
                },
                gridLines: {
                    display: false
                },
                offset: true
            }],
            yAxes: [{
                ticks: {
                    suggestedMin: *MIN*,
                    suggestedMax: *MAX*,
                    fontColor: '#000000',
                    padding: 10,
                    stepSize: *STEPS*,
                },
                gridLines: {
                    drawBorder: false,
                    display: true
                }   
            }]
        }
    }
}";

?>