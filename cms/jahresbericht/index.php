<?php

ini_set('display_errors', '1');
error_reporting(E_ALL);

ini_set("memory_limit", "-1");
set_time_limit(0);

require_once('../const.inc.php');
require_once('../../assets/classes/class.mysql.php');
require_once('phpqrcode/classes/phpqrcode.class.php');

//https://www.izs.de/cms/event/Orizon%20GmbH/TUI%20BKK/2022/7/1042492.pdf

//print_r($_REQUEST);

$strRedirectUrl = @$_REQUEST['r'];

if (isset($strRedirectUrl) && ($strRedirectUrl != '')) {

    $arrPart = explode('/', $strRedirectUrl);

    //print_r($arrPart); die();

    if (count($arrPart) == 3) { // CREATE https://www.izs.de/cms/jahresbericht/a083000000LQSO0AAP/Orizon%20GmbH/2023.pdf

        $arrRequest = pathinfo($strRedirectUrl);

        if (isset($arrRequest['filename']) && ($arrRequest['filename'] != '') && ($arrRequest['filename'] <= date('Y'))) {

            $intBeginTime = microtime(true); 

            $intYear = $arrRequest['filename'];

            $strSql = 'SELECT *, UNIX_TIMESTAMP(`pr_time`) AS `pr_time_unix` FROM `izs_jahresbericht` WHERE `pr_group_id` = "' .$arrPart[0] .'" AND `pr_year` = "' .$arrRequest['filename'] .'"';
            $arrSql = MySQLStatic::Query($strSql);
            
            if (count($arrSql) == 0) {
                http_response_code(404);
                //include('my_404.php'); // provide your own HTML for the error page
                die();
            }

            $strReturnType  = 'string';
            $boolStandAlone = true;

            $strGroupIdJB = $arrPart[0];

            if (isset($_COOKIE['id']) && ($_COOKIE['id'] == 3)) {
                include ('create.jahresbericht.inc.php');
            } else {
                include ('create.jahresbericht.inc.php');
            }

            if (isset($_REQUEST['d']) && ($_REQUEST['d'] == 't')) {
                
                //echo $strOutput;
                $intDurationTime = microtime(true) - $intBeginTime;
                echo "gesamt: $intDurationTime Sek.";

            } else {

                $strFileName = 'Jahresbericht - ' .$arrRequest['filename'] .' - ' .$arrSql[0]['Name'] .'';

                if ((isset($_COOKIE['id'])) && ($_COOKIE['id'] == 3)) {
                    header ('Content-type:application/pdf');
                    header ('Content-Disposition:inline;filename="' .$strFileName .'.pdf";filename*=UTF-8\'\'' .rawurlencode($strFileName) .'.pdf'); //inline
                } else {
                    header ('Content-type:application/pdf');
                    header ('Content-Disposition:attachment;filename="' .$strFileName .'.pdf";filename*=UTF-8\'\'' .rawurlencode($strFileName) .'.pdf'); //inline
                }
                echo $strOutput;

            }



        } else {

            http_response_code(404);
            //include('my_404.php'); // provide your own HTML for the error page
            die();

        }

    }


} else {

    http_response_code(404);
    //include('my_404.php'); // provide your own HTML for the error page
    die();

}

?>