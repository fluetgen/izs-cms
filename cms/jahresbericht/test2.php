<?php

$strChart = "{
  type: 'line',
      data: {
          labels: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
          datasets: [{
              label: '',
              backgroundColor: 'rgb(98, 181, 99)',
              borderColor: 'rgb(98, 181, 99)',
              data: [93, 95, 98, 90, 88, 95, 97, 95, 93, 91, 96, 95],
              fill: false,
              pointRadius: 0,
              borderWidth: 2
          }],
  },
  options: {
      layout: {
          padding: {
              left: 10,
              right: 10
          },
      },
      title: {
          display: false,
          text: '',
      },
      legend: {
          display: false
      },
      plugins: {
          datalabels: {
              display: true,
              align: 'top',
              padding: 5,
              backgroundColor: 'rgb(255, 255, 255)',
              color: '#000000',
              borderRadius: 20,
          }
      },
      scales: {
          xAxes: [{
              ticks: {
                  fontColor: '#000000',
                  padding: 10,
              },
              gridLines: {
                  display: false
              },
              offset: true
          }],
          yAxes: [{
              ticks: {
                  suggestedMin: 68,
                  suggestedMax: 118,
                  fontColor: '#000000',
                  padding: 10,
                  stepSize: 20
              },
              gridLines: {
                  drawBorder: false,
                  display: true
              }   
          }]
      }
  }
}";

$chart_url = 'https://quickchart.io/chart?w=2337&h=605&v=2.9.4&c=' .urlencode($strChart);
// Lade das PNG-Diagramm herunter
file_put_contents("chart.png", file_get_contents($chart_url));

//echo "Das Diagramm wurde heruntergeladen als chart.png";

?>

<img src="chart.png" />