<?php

ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

if ($boolStandAlone === true) {

    require_once('../const.inc.php');
    require_once('../../assets/classes/class.mysql.php');

    require_once('../inc/refactor.inc.php');

    require_once ('../fpdf182/fpdf.php');
    require_once ('../fpdf182/class.pdfhtml_jb2.php');

}

require_once ('chart.inc.php');
//echo $strChartTemplate; die();

$intDurationTime = microtime(true) - $intBeginTime;
if ($_REQUEST['d'] == 't') echo " Include: $intDurationTime Sek." .chr(10);

// INPUT
$strGroupId = $arrRequesrt[0];
$intYear = $arrRequest['filename'];

function getKuendigung ($strId = '') {

    global $arrMonthList;

    $strKuendigung = '';
    $strSqlV1 = 'SELECT * FROM `Vertrag__c` WHERE `Company_Group__c` = "' .$strId .'" AND `Aktiv__c` = "true" AND `Kuendigung_wirksam_ab__c` != "0000-000-00"';
    $arrSqlV1 = MySQLStatic::Query($strSqlV1);
    if (count($arrSqlV1) > 0) {
        $strSqlV2 = 'SELECT * FROM `Vertrag__c` WHERE `Company_Group__c` = "' .$strId .'" AND `Vertragsbeginn__c` > "' .$arrSqlV1[0]['Vertragsbeginn__c'] .'"';
        $arrSqlV2 = MySQLStatic::Query($strSqlV2);
        if (count($arrSqlV2) == 0) {
            $intKuendigung = strtotime($arrSqlV1[0]['Kuendigung_wirksam_ab__c']);
            $strKuendigung = date('d', $intKuendigung) .'. ' .$arrMonthList[date('n', $intKuendigung)] .' ' .date('Y', $intKuendigung);
        }
    }

    return $strKuendigung;

}

function getKuendigungStatus ($strId = '') {

    $strSqlCompany = 'SELECT * FROM `Vertrag__c` WHERE `Company_Group__c` = "' .$strId .'" AND `Aktiv__c` = "true" AND ((`Kuendigung_wirksam_ab__c` > CURRENT_DATE()) OR (`Kuendigung_wirksam_ab__c` = "0000-000-00"))';
    $arrSqlCompany = MySQLStatic::Query($strSqlCompany);

    if (count($arrSqlCompany) > 0) {
        $boolRevokedCompletely = false;
    } else {
        $boolRevokedCompletely = true;
    }

    return $boolRevokedCompletely;

}


function createJahresbericht ($strGroupId = '', &$objPdf = null, $strReturnType = 'string') {

    global $intBeginTime, $arrPdfLinkListEvent, $intYear, $strChartTemplate, $arrMonthList, $strKuendigung;

    $arrMonth = array(
        1 => 'Januar',
        2 => 'Februar',
        3 => 'März',
        4 => 'April',
        5 => 'Mai',
        6 => 'Juni',
        7 => 'Juli',
        8 => 'August',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Dezember'
    );
    
    $arrMonthList = $arrMonth;

    if (!isset($strReturnType)) {
        $strReturnType = 'string';
    }

    // OUTPUT
    $strOutput = '';

    $intDurationTime = microtime(true) - $intBeginTime;
    if ($_REQUEST['d'] == 't') echo " Select: $intDurationTime Sek." .chr(10);

    //print_r($arrSql); die();

    if ($strGroupId != '') {

        $strSql = 'SELECT *, UNIX_TIMESTAMP(`pr_time`) AS `pr_time_unix` FROM `izs_jahresbericht` WHERE `pr_group_id` = "' .$strGroupId .'" AND `pr_year` = "' .$intYear .'"';
        $arrSql = MySQLStatic::Query($strSql);

        if (count($arrSql) == 0) {

            $strSqlG = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$strGroupId .'"';
            $arrSqlG = MySQLStatic::Query($strSqlG);

            $arrSql[0]['pr_time_unix'] = time();

            $arrData = array(
                'name' => $arrSqlG[0]['Name'],
                'year' => $intYear,
                'street' => $arrSqlG[0]['Strasse__c'],
                'zip' => $arrSqlG[0]['PLZ__c'],
                'city' => $arrSqlG[0]['Ort__c'],
                'bnr' => $arrSqlG[0]['Betriebsnummer__c'],
            );

            $strSqlU = 'INSERT INTO `izs_jahresbericht` (`pr_group_id`, `pr_data`, `pr_year`, `pr_time`) VALUES ("' .MySQLStatic::esc($strGroupId) .'", "' .MySQLStatic::esc(serialize($arrData)) .'", "' .$intYear .'", NOW())';
            $arrSqlU = MySQLStatic::Update($strSqlU);
            
        } else {

            $arrData = unserialize($arrSql[0]['pr_data']);

            $strSqlG = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$strGroupId .'"';
            $arrSqlG = MySQLStatic::Query($strSqlG);

        }

        $boolStandAlone = false;

        $arrPdfLinkList = array();

        if (!isset($objPdf)) {

            $boolStandAlone = true;
        }

        $strUnique = uniqid();

        $strHost = 'www';
        if (isset($_SERVER['SERVER_NAME']) && (strstr($_SERVER['SERVER_NAME'], 'test.') !== false)) {
            $strHost = 'test';
        }

        $strHash = 'https://' .$strHost .'.izs.de/cms/jahresbericht/' .$strUnique;

        $strUid  = md5(uniqid(rand(), true));

        if ($boolStandAlone === true) {
            $strPath = '../files/temp/' .$strUid .'.png';
        } else {
            $strPath = '../cms/files/temp/' .$strUid .'.png';
        }
        
        
        QRcode::png($strHash, $strPath);

        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " QR-Code: $intDurationTime Sek." .chr(10);

        $floatLeft = 10.8;
        $floatCol1 = 81.5;

        if (!isset($objPdf)) {

            $objPdf = new pdfHtml('P');
            $objPdf->AliasNbPages();

            $objPdf->AddFont('Roboto', '', 'Roboto-Regular.php');
            $objPdf->AddFont('Roboto', 'B', 'Roboto-Bold.php');
            $objPdf->AddFont('RobotoM', '', 'Roboto-Medium.php');
            
        } 

        /* ********************************* FIRST ********************************* */


        $objPdf->AddPage('P');
        $objPdf->SetAutoPageBreak(false);

        if ($boolStandAlone === false) {
            //$objPdf->SetLink($arrPdfLinkListEvent[$arrSql[0]['evid']], 0, -1);
        }

        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " Init PDF: $intDurationTime Sek." .chr(10);
        
        $objPdf->Image( __DIR__ .'/img/front.png', 0, 0, 210, 297, 'PNG');

        $objPdf->SetTextColor(62, 183, 88); //#3eb758
        $objPdf->SetFont('Roboto', 'B', 96);
        $objPdf->Text(21, 200, utf8_decode($arrData['year']));

        $objPdf->SetTextColor(255, 255, 255); //#ffffff
        $objPdf->SetFont('Roboto', 'B', 15);
        $objPdf->SetXY(21.5, 225);
        $objPdf->MultiCell(210 -43, 8, utf8_decode($arrData['name']), 0, 'L', false);

        $strTime = date('d', $arrSql[0]['pr_time_unix']) .'. ' .$arrMonth[date('n', $arrSql[0]['pr_time_unix'])] .' ' .date('Y', $arrSql[0]['pr_time_unix']) .'  ' .chr(127) .' ' .date('H:i:s', $arrSql[0]['pr_time_unix']) .' CET';

        $objPdf->SetX(21.5);
        $objPdf->SetFont('Roboto', '', 15);
        $objPdf->MultiCell(210 -43, 9, utf8_decode($strTime), 0, 'L', false);

        
        /* ********************************* INDEX ********************************* */

        $boolRevokedCompletely = getKuendigungStatus($strGroupId);

        $boolCertMissing = false;
        $strSqlProof2 = 'SELECT * FROM `izs_zertifikat` WHERE `pr_group_id` = "' .$strGroupId .'" AND `pr_revoked` = 0';
        $arrSqlProof2 = MySQLStatic::Query($strSqlProof2);
        if (count($arrSqlProof2) == 0) {
            $boolCertMissing = true;
        }

        $boolProtectMissing = false;
        $strSqlProof2 = 'SELECT * FROM `izs_subsidiaerprotect` WHERE `pr_group_id` = "' .$strGroupId .'" AND `pr_revoked` = 0';
        $arrSqlProof2 = MySQLStatic::Query($strSqlProof2);
        if (count($arrSqlProof2) == 0) {
            $boolProtectMissing = true;
        }

        //

        //Page margin
        $objPdf->SetMargins(21.5, 21.5);

        $objPdf->AddPage('P');
        $objPdf->SetAutoPageBreak(false);

        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " Init PDF: $intDurationTime Sek." .chr(10);
        
        $objPdf->Image( __DIR__ .'/img/first_1.png', 0, 0, 210, 297, 'PNG');

        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 18);
        $objPdf->Text(21.5, 185, utf8_decode('Inhaltsverzeichnis'));

        $intLine = 11.2;

        $objPdf->setY(192);
        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 12);

        $intLink = $objPdf->AddLink();
        $arrPdfLinkList['Zusammenfassung'] = $intLink;
        $objPdf->MultiCell(125, $intLine, utf8_decode('Zusammenfassung'), 0, 'L', false, $intLink);

        $objPdf->setXY(140, $objPdf->getY() + 3 - $intLine);
        $objPdf->SetTextColor(242,83,35); //#000000
        $objPdf->SetFont('Symbol', '', 12);
        $objPdf->WriteHTML('' .chr(174) .'');
        
        //$objPdf->Rect(21.5, $objPdf->getY() - 3, 125, 10, 'F');
        $objPdf->Link(21.5, $objPdf->getY() - 3, 125, 10, $intLink);

        $objPdf->SetDrawColor(236,236,236);
        $objPdf->SetLineWidth(0.4);
        $objPdf->Line(21.5, $objPdf->getY() + $intLine - 3, 146.5, $objPdf->getY() + $intLine - 3);

        //
        
        $objPdf->setY($objPdf->getY() - 3 + $intLine);
        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 12);

        $intLink = $objPdf->AddLink();
        $arrPdfLinkList['Krankenkassen'] = $intLink;
        $objPdf->MultiCell(170, $intLine, utf8_decode('Meldungen an Krankenkassen'), 0, 'L', false, $intLink);

        $objPdf->setXY(140, $objPdf->getY() + 3 - $intLine);
        $objPdf->SetTextColor(242,83,35); //#000000
        $objPdf->SetFont('Symbol', '', 12);
        $objPdf->WriteHTML('' .chr(174) .'');

        //$objPdf->Rect(21.5, $objPdf->getY() - 3, 125, 10, 'F');
        $objPdf->Link(21.5, $objPdf->getY() - 3, 125, 10, $intLink);

        $objPdf->SetDrawColor(236,236,236);
        $objPdf->SetLineWidth(0.4);
        $objPdf->Line(21.5, $objPdf->getY() + $intLine - 3, 146.5, $objPdf->getY() + $intLine - 3);

        //

        $objPdf->setY($objPdf->getY() - 3 + $intLine);
        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 12);

        $intLink = $objPdf->AddLink();
        $arrPdfLinkList['Berufsgenossenschaften'] = $intLink;
        $objPdf->MultiCell(170, $intLine, utf8_decode('Meldungen an Berufsgenossenschaften'), 0, 'L', false, $intLink);

        $objPdf->setXY(140, $objPdf->getY() + 3 - $intLine);
        $objPdf->SetTextColor(242,83,35); //#000000
        $objPdf->SetFont('Symbol', '', 12);
        $objPdf->WriteHTML('' .chr(174) .'');

        //$objPdf->Rect(21.5, $objPdf->getY() - 3, 125, 10, 'F');
        $objPdf->Link(21.5, $objPdf->getY() - 3, 125, 10, $intLink);

        $objPdf->SetDrawColor(236,236,236);
        $objPdf->SetLineWidth(0.4);
        $objPdf->Line(21.5, $objPdf->getY() + $intLine - 3, 146.5, $objPdf->getY() + $intLine - 3);

        //

        $objPdf->setY($objPdf->getY() - 3 + $intLine);
        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 12);

        $intLink = $objPdf->AddLink();
        $arrPdfLinkList['Erlaubnisse'] = $intLink;
        $objPdf->MultiCell(170, $intLine, utf8_decode('Erlaubnisse zur Arbeitnehmerüberlassung'), 0, 'L', false, $intLink);

        $objPdf->setXY(140, $objPdf->getY() + 3 - $intLine);
        $objPdf->SetTextColor(242,83,35); //#000000
        $objPdf->SetFont('Symbol', '', 12);
        $objPdf->WriteHTML('' .chr(174) .'');

        //$objPdf->Rect(21.5, $objPdf->getY() - 3, 125, 10, 'F');
        $objPdf->Link(21.5, $objPdf->getY() - 3, 125, 10, $intLink);

        if (($boolCertMissing === false) || ($boolProtectMissing === false)) {
            $objPdf->SetDrawColor(236,236,236);
            $objPdf->SetLineWidth(0.4);
            $objPdf->Line(21.5, $objPdf->getY() + $intLine - 3, 146.5, $objPdf->getY() + $intLine - 3);
        }

        //

        if (($boolRevokedCompletely === false) && ($boolCertMissing === false)) { // 

            $objPdf->setY($objPdf->getY() - 3 + $intLine);
            $objPdf->SetTextColor(0, 0, 0); //#000000
            $objPdf->SetFont('Roboto', 'B', 12);
    
            $intLink = $objPdf->AddLink();
            $arrPdfLinkList['Mitgliedschaft'] = $intLink;
            $objPdf->MultiCell(170, $intLine, utf8_decode('Zertifikat zur IZS-Mitgliedschaft'), 0, 'L', false, $intLink);

            $objPdf->setXY(140, $objPdf->getY() + 3 - $intLine);
            $objPdf->SetTextColor(242,83,35); //#000000
            $objPdf->SetFont('Symbol', '', 12);
            $objPdf->WriteHTML('' .chr(174) .'');
    
            //$objPdf->Rect(21.5, $objPdf->getY() - 3, 125, 10, 'F');
            $objPdf->Link(21.5, $objPdf->getY() - 3, 125, 10, $intLink);
    
            if ($boolProtectMissing === false) {
                $objPdf->SetDrawColor(236,236,236);
                $objPdf->SetLineWidth(0.4);
                $objPdf->Line(21.5, $objPdf->getY() + $intLine - 3, 146.5, $objPdf->getY() + $intLine - 3);
            }
    
        }

        //

        if (($boolRevokedCompletely === false) && ($boolProtectMissing === false)) { // 

            $objPdf->setY($objPdf->getY() - 3 + $intLine);
            $objPdf->SetTextColor(0, 0, 0); //#000000
            $objPdf->SetFont('Roboto', 'B', 12);

            $intLink = $objPdf->AddLink();
            $arrPdfLinkList['SubsidiärProtect'] = $intLink;
            $objPdf->MultiCell(170, $intLine, utf8_decode('Zertifikat zur Versicherbarkeit über SubsidiärProtect®'), 0, 'L', false, $intLink);


            $objPdf->setXY(140, $objPdf->getY() + 3 - $intLine);
            $objPdf->SetTextColor(242,83,35); //#000000
            $objPdf->SetFont('Symbol', '', 12);
            $objPdf->WriteHTML('' .chr(174) .'');
    
            //$objPdf->Rect(21.5, $objPdf->getY() - 3, 125, 10, 'F');
            $objPdf->Link(21.5, $objPdf->getY() - 3, 125, 10, $intLink);
            
            /*
            $objPdf->SetDrawColor(236,236,236);
            $objPdf->SetLineWidth(0.4);
            $objPdf->Line(21.5, $objPdf->getY() + $intLine - 3, 146.5, $objPdf->getY() + $intLine - 3);
            */

        }


        /* ********************************* OVERVIEW ********************************* */


        $objPdf->AddPage('P');
        $objPdf->SetAutoPageBreak(false);

        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " Init PDF: $intDurationTime Sek." .chr(10);
        
        $objPdf->Image( __DIR__ .'/img/follow.png', 0, 0, 210, 297, 'PNG');

        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 6.5);
        $objPdf->Text(15, 12, utf8_decode('Jahresbericht ' .$arrData['year']));
        $objPdf->SetFont('Roboto', '', 6.5);
        $objPdf->Text(37, 12, utf8_decode($arrData['name']));

        $objPdf->SetLink($arrPdfLinkList['Zusammenfassung'], 0, -1);

        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 24);
        $objPdf->Text(21.5, 62, utf8_decode('Zusammenfassung'));

        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 33);
        $objPdf->setY(106);

        $strCompanyName = $arrData['name'];
        if (strstr($strCompanyName, 'Consulting-Engineering-Service') != false) {
            $strCompanyName = str_replace('Consulting-Engineering-Service', 'Consulting-Engineering- Service', $strCompanyName);
        } elseif (strstr($strCompanyName, 'TIMEPARTNER-Unternehmensgruppe') !== false) {
            $strCompanyName = str_replace('TIMEPARTNER-Unternehmensgruppe', 'TIMEPARTNER- Unternehmensgruppe', $strCompanyName);
        }

        $floatHeight1 = $objPdf->GetMultiCellHeight(210 - 43, 15, utf8_decode($strCompanyName), 0, 'L'); // 15 per Line
        $objPdf->MultiCell(210 - 43, 15, utf8_decode($strCompanyName), 0, 'L', false);

        $intOffset1 = 0;
        if ($floatHeight1 > 30) {
            $intOffset1 = $floatHeight1 - 30;
        }

        if ($floatHeight1 <= 15) {
            $intOffset1 = - 15;
        }

        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 9);
        $objPdf->setY(148 + $intOffset1);
        $objPdf->MultiCell(210 - 43, 8, utf8_decode(str_replace(array("\n", "\r"), '', $arrData['street']) .', ' .$arrData['zip'] .' ' .$arrData['city']), 0, 'L', false);

        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', '', 9);
        $objPdf->setY(154 + $intOffset1);
        $objPdf->MultiCell(210 - 43, 8, utf8_decode('IZS-ID: ' .$arrData['bnr']), 0, 'L', false);
        $objPdf->MultiCell(210 - 43, 8, utf8_decode('Geprüft seit: ' .date('d.m.Y', strtotime($arrSqlG[0]['Registriert_bei_IZS_seit__c']))), 0, 'L', false);


        $objPdf->setY(183 + $intOffset1);

        $objPdf->SetAutoPageBreak(true, 30);

        //$objPdf->GetCellMargin(); die();
        $objPdf->SetCellMargin(1.5);

        $objPdf->createHighlight(
            array('body1' => array(
                'fillcolor' => '242,83,35',
                'textcolor' => '255,255,255',
                'rounded' => true,
                'height' => 5,
                'width' => 4
                )
            )
        );

        $objPdf->createHighlight(
            array('body2' => array(
                'fillcolor' => '242,83,35',
                'textcolor' => '255,255,255',
                'rounded' => true,
                'height' => 3,
                'width' => 2
                )
            )
        );

        $arrTheme['head1'] = array(
            'font_name' => 'Roboto', 
            'font_size' => '7', 
            'font_style' => 'B', 
            'fillcolor' => '236,236,236', 
            'textcolor' => '0,0,0', 
            'drawcolor' => '0,0,0', 
            'linewidth' => '0.4', 
            'linearea' => 'B',
            'height' => '10', 
            'align' => 'R', 
            'colset' => 'table1',
            'spacing' => '0'
        );

        $arrTheme['head2'] = array(
            'font_name' => 'Roboto', 
            'font_size' => '7', 
            'font_style' => 'B', 
            'fillcolor' => '236,236,236', 
            'textcolor' => '0,0,0', 
            'drawcolor' => '0,0,0', 
            'linewidth' => '0.2', 
            'linearea' => 'B',
            'height' => '8', 
            'align' => 'R', 
            'colset' => 'table2',
            'spacing' => '1'
        );

        $arrTheme['head3'] = $arrTheme['head2'];
        $arrTheme['head3']['linearea'] = 'BT';
        $arrTheme['head3']['height'] = '7'; 

        $arrTheme['head4'] = $arrTheme['head2'];
        $arrTheme['head4']['align'] = 'L';
        $arrTheme['head4']['colset'] = 'table3';

        $arrTheme['body1'] = array(
            'font_name' => 'Roboto', 
            'font_size' => '11', 
            'font_style' => '', 
            'fillcolor' => '255,255,255', 
            'textcolor' => '0,0,0', 
            'drawcolor' => '230,230,230', 
            'linewidth' => '0.4', 
            'linearea' => 'B',
            'height' => '11', 
            'align' => 'R', 
            'colset' => 'table1',
            'spacing' => '0',
        );

        $arrTheme['body2'] = array(
            'font_name' => 'Roboto', 
            'font_size' => '5.5', 
            'font_style' => '', 
            'fillcolor' => '255,255,255', 
            'textcolor' => '0,0,0', 
            'drawcolor' => '255,255,255', 
            'linewidth' => '0', 
            'linearea' => '',
            'height' => '4', 
            'align' => 'R', 
            'colset' => 'table2',
            'spacing' => '0.2',
        );

        $arrTheme['body2']['drawcolor'] = '236,236,236';
        $arrTheme['body2']['linewidth'] = '0.2';
        $arrTheme['body2']['linearea'] = 'B';

        $arrTheme['body4'] = $arrTheme['body2'];
        $arrTheme['body4']['align'] = 'L';
        $arrTheme['body4']['colset'] = 'table3';
        $arrTheme['body4']['drawcolor'] = '236,236,236';
        $arrTheme['body4']['linewidth'] = '0.2';
        $arrTheme['body4']['linearea'] = 'B';



        $objPdf->createTheme($arrTheme);

        $arrColSet['table1'] = array(53, 20, 94);
        $arrColSet['table2'] = array(35, 17, 8.5, 8.5, 8.5, 8.5, 8.5, 8.5, 8.5, 8.5, 8.5, 8.5, 8.5, 8.5, 13);
        $arrColSet['table3'] = array(57, 40, 13, 40, 17);
        $objPdf->createColSet($arrColSet);

        $columns = array();      
        
        // header col
        $col = array();
        $col[] = array('text' => utf8_decode('Geprüfte Meldungen ' .$arrData['year']), 'align' => 'L', 'theme' => 'head1');
        $col[] = array('text' => 'Gesamtanzahl','theme' => 'head1');
        $col[] = array('text' => utf8_decode('Nicht fristgerecht geklärte Beitragsrückstände'), 'theme' => 'head1');
        $columns[] = $col;

        $strSql = 'SELECT `Id`, `Name`, `SF42_Comany_ID__c` FROM `Account` WHERE `SF42_Comany_ID__c` != "" ORDER BY `Name`';
        $arrSql = MySQLStatic::Query($strSql);

        $arrBnrList = array();
        if (count($arrSql) > 0) {
            foreach ($arrSql as $intKey => $arrAccount) {
                $arrBnrList[$arrAccount['SF42_Comany_ID__c']] = $arrAccount['Name'];
            }
        }

        $strSql = 'SELECT `Betriebsnummer_ZA__c`, `SF42_Month__c`, `SF42_EventStatus__c` FROM `SF42_IZSEvent__c` WHERE `group_id` = "' .$strGroupId .'" AND `SF42_Year__c` = "' .$arrData['year'] .'" AND `SF42_EventStatus__c` IN ("OK", "not OK")';
        $arrSql = MySQLStatic::Query($strSql);

        $intCountAllSv = count($arrSql);

        $arrEmpty = array(
            1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0
        );

        $arrMonthSum = $arrEmpty;
        $arrBnrSum = array();

        $arrNotOkSvList = array();
        
        $intCountNotOkSv = 0;
        if (count($arrSql) > 0) {
            foreach ($arrSql as $intKey => $arrEvent) {

                $strKey = $arrBnrList[$arrEvent['Betriebsnummer_ZA__c']];

                if ($arrEvent['SF42_EventStatus__c'] == 'not OK') {
                    $intCountNotOkSv++;
                    $arrNotOkSvList[$strKey][$arrEvent['SF42_Month__c']] = true;
                }
                $arrMonthSum[$arrEvent['SF42_Month__c']]++;

                if (!isset($arrBnrSum[$strKey])) {
                    $arrBnrSum[$strKey] = array(
                        'month' => $arrEmpty,
                        'bnr'   => $arrEvent['Betriebsnummer_ZA__c']
                    );
                }
                $arrBnrSum[$strKey]['month'][$arrEvent['SF42_Month__c']]++;                
            }
            natsort($arrBnrSum);
        }

        //$strSql = 'SELECT `Account`.`Name` AS `be_name_meldestelle`, MONTH(`be_datum_rueckmeldung`) AS `SF42_Month__c`, `be_ergebnis`, `SF42_Comany_ID__c` FROM `izs_bg_event` INNER JOIN `Account` ON `be_meldestelle` = `Account`.`Id` WHERE `be_ergebnis` IN ("NOT OK", "OK", "OK (mit RV)") AND YEAR(`be_datum_rueckmeldung`) = "' .$arrData['year'] .'" AND `Account`.`SF42_Company_Group__c` = "' .$strGroupId .'"';
        $strSql = 'SELECT `Account`.`Name` AS `be_name_meldestelle`, MONTH(`be_beitragsfaelligkeit`) AS `SF42_Month__c`, `be_ergebnis`, `SF42_Comany_ID__c` FROM `izs_bg_event` INNER JOIN `Account` ON `be_meldestelle` = `Account`.`Id` WHERE `be_ergebnis` IN ("NOT OK", "OK", "OK (mit RV)") AND YEAR(`be_beitragsfaelligkeit`) = "' .$arrData['year'] .'" AND `Account`.`SF42_Company_Group__c` = "' .$strGroupId .'"';
        $arrSql = MySQLStatic::Query($strSql);

        $intCountAllBg = count($arrSql);
        
        $arrMonthSumBg = $arrEmpty;
        $arrBnrSumBg = array();

        $arrNotOkBgList = array();

        $intCountNotOkBg = 0;
        if (count($arrSql) > 0) {
            foreach ($arrSql as $intKey => $arrEvent) {

                $strKey = $arrEvent['be_name_meldestelle'];

                if ($arrEvent['be_ergebnis'] == 'NOT OK') {
                    $intCountNotOkBg++;
                    $arrNotOkBgList[$strKey][$arrEvent['SF42_Month__c']] = true;
                }

                $arrMonthSumBg[$arrEvent['SF42_Month__c']]++;

                if (!isset($arrBnrSumBg[$strKey])) {
                    $arrBnrSumBg[$strKey] = array(
                        'month' => $arrEmpty,
                        'bnr'   => $arrEvent['SF42_Comany_ID__c']
                    );
                }
                $arrBnrSumBg[$strKey]['month'][$arrEvent['SF42_Month__c']]++;          
            }
        }

        // data col
        $col = array();
        $col[] = array('text' => 'an Krankenkassen', 'align' => 'L', 'theme' => 'body1');
        $col[] = array('text' => number_format($intCountAllSv, 0, ',', '.'), 'theme' => 'body1');
        $col[] = array('text' => number_format($intCountNotOkSv, 0, ',', '.'), 'theme' => 'body1', 'highlight' => ($intCountNotOkSv > 0));
        $columns[] = $col;

        // data col
        $col = array();
        $col[] = array('text' => 'an Berufsgenossenschaften', 'align' => 'L', 'theme' => 'body1');
        $col[] = array('text' => number_format($intCountAllBg, 0, ',', '.'), 'theme' => 'body1');
        $col[] = array('text' => number_format($intCountNotOkBg, 0, ',', '.'), 'theme' => 'body1', 'highlight' => ($intCountNotOkBg > 0));
        $columns[] = $col;

        // Draw Table   
        $objPdf->WriteTable($columns);

        $floatY = $objPdf->GetY();
        $objPdf->SetFillColor(242, 83, 35);
        $objPdf->RoundedRect(21.5, $floatY + 12, 45, 10, 1, '1234', 'F');
   
        $objPdf->SetTextColor(255, 255, 255);
        $objPdf->SetXY(21.5 + 1.5, $floatY + 15.2);    

        $objPdf->SetFont('Roboto', '', 10);
        $objPdf->MultiCell(73, 4, utf8_decode('Zum Online-Konto'), 0, '');

        $objPdf->SetXY(58, $floatY + 15.2);    
        $objPdf->SetFont('Symbol', '', 10);
        $objPdf->WriteHTML('' .chr(174) .'');

        //$strUrl = 'izs-portal.netlify.app';
        $strUrl = 'portal.izs.de';
        $objPdf->Link(21.5, $floatY + 12, 45, 10, 'https://' .$strUrl .'/company_group/' .$strGroupId .'/stammdaten');
        /////

        $objPdf->SetAutoPageBreak(false);
        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetXY(185, 290);
        $objPdf->SetFont('Roboto', '', 6.5);
        $objPdf->Cell(20,0,'Seite '.$objPdf->PageNo().' von {nb}',0,0,'3');


        /* ***************************** SV ***************************** */

        $objPdf->AddPage('P');
        $objPdf->SetAutoPageBreak(false);
        $objPdf->Image( __DIR__ .'/img/follow.png', 0, 0, 210, 297, 'PNG');
        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 6.5);
        $objPdf->Text(15, 12, utf8_decode('Jahresbericht ' .$arrData['year']));
        $objPdf->SetFont('Roboto', '', 6.5);
        $objPdf->Text(37, 12, utf8_decode($arrData['name']));

        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetXY(185, 290);
        $objPdf->SetFont('Roboto', '', 6.5);
        $objPdf->Cell(20,0,'Seite '.$objPdf->PageNo().' von {nb}',0,0,'3');
        $objPdf->SetXY(21.5, 50);


        $objPdf->SetLink($arrPdfLinkList['Krankenkassen'], 0, -1);
        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 24);
        $objPdf->Text(21.5, 62, utf8_decode('Meldungen an'));
        $objPdf->Text(21.5, 72, utf8_decode('Krankenkassen'));

        // CONTENT
        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 9);
        $objPdf->setXY(20, 86);
        $objPdf->MultiCell(210 - 43, 7, utf8_decode('Gesamtanzahl der geprüften SV-Meldungen'), 0, 'L', false);

        $objPdf->SetDrawColor(223, 223, 223);
        $objPdf->SetLineWidth(0.2);
        $objPdf->Line(21.5, 97, 187, 97);

        $strUnique = uniqid();

        $intMax = max($arrMonthSum);
        $intMin = min($arrMonthSum);

        $intDiff = (int) ($intMax - $intMin);

        if ($intDiff < 6) { $intSteps = 1; }
        elseif ($intDiff < 11) { $intSteps = 2; }
        elseif ($intDiff < 26) { $intSteps = 5; }
        elseif ($intDiff < 51) { $intSteps = 10; }
        elseif ($intDiff < 101) { $intSteps = 20; }
        elseif ($intDiff < 251) { $intSteps = 50; }
        elseif ($intDiff < 501) { $intSteps = 100; }
        elseif ($intDiff > 501) { $intSteps = 200; }

        $intMin = $intMin - ($intSteps * 2);
        if ($intMin < 0) $intMin = 0;

        if ($intMax < 5) {
            $intMax = $intMax + (1 * $intSteps);
        } else {
            $intMax = $intMax + (2 * $intSteps);
        }

        $strChart = $strChartTemplate;
        $strChart = str_replace('*DATA*', implode(',', $arrMonthSum), $strChart);
        $strChart = str_replace('*MIN*', $intMin, $strChart);
        $strChart = str_replace('*MAX*', $intMax, $strChart);
        $strChart = str_replace('*STEPS*', $intSteps, $strChart);

        $chart_url = 'https://quickchart.io/chart?w=935&h=242&v=2.9.4&c=' .urlencode($strChart);
        file_put_contents($strUnique .'.png', file_get_contents($chart_url));

        $objPdf->Image( __DIR__ .'/' .$strUnique .'.png', 18.5, 103, 172, 44.5, 'PNG');

        if (file_exists($strUnique .'.png')) unlink($strUnique .'.png');

        $objPdf->SetDrawColor(44, 45, 46);
        $objPdf->SetLineWidth(0.2);
        $objPdf->Line(21.5, 155, 187, 155);

        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 7);
        $objPdf->setXY(20, 157);
        $objPdf->MultiCell(210 - 43, 6, utf8_decode('Summe: ' .number_format($intCountAllSv, 0, ',', '.')), 0, 'L', false);

        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 9);
        $objPdf->setXY(20, 184);
        $objPdf->MultiCell(210 - 43, 7, utf8_decode('Anzahl der geprüften SV-Meldungen pro Meldestelle'), 0, 'L', false);

        $objPdf->SetDrawColor(223, 223, 223);
        $objPdf->SetLineWidth(0.2);
        $objPdf->Line(21.5, 194, 187, 194);


        $objPdf->SetAutoPageBreak(true, 30);

        //       Jun Jul Aug Sep Okt Nov Dez Summe

        $objPdf->SetY(200);

        $columns = array();      
        
        // header col
        $col = array();
        $col[] = array('text' => utf8_decode('SV-Meldestelle'), 'align' => 'L', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Betriebsnr.'),'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Jan'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Feb'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Mär'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Apr'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Mai'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Jun'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Jul'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Aug'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Sep'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Okt'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Nov'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Dez'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Summe'), 'theme' => 'head2');
        $columns[] = $col;


        $arrBreak['table2'] = array(
            'head1' => utf8_decode('Jahresbericht ' .$arrData['year']),
            'head2' => utf8_decode($arrData['name']),
            'head3' => utf8_decode('Anzahl der geprüften SV-Meldungen pro Meldestelle (Fortsetzung)'),
            'image' => __DIR__ .'/img/follow.png',
            'coloms' => $columns
        );

        $objPdf->createBreak($arrBreak);

        $arrSumMonth = $arrEmpty;

        if (count($arrBnrSum) > 0) {

            foreach ($arrBnrSum as $strName => $arrDataset) {

                $col = array();
                $col[] = array('text' => utf8_decode($strName), 'align' => 'L', 'theme' => 'body2');
                $col[] = array('text' => $arrDataset['bnr'],'theme' => 'body2');

                //print_r($arrNotOkSvList); die();

                for ($i = 1; $i <= 12; $i++) {
                    
                    (isset($arrNotOkSvList[$strName][$i])) ? ($boolHighlight = true) : ($boolHighlight = false);
                    $col[] = array('text' => utf8_decode($arrDataset['month'][$i]), 'theme' => 'body2', 'highlight' => $boolHighlight);
                }
                
                $col[] = array('text' => utf8_decode(number_format(array_sum($arrDataset['month']), 0, ',', '.')), 'theme' => 'body2');
                $columns[] = $col;

            }

        }

        $objPdf->WriteTable($columns);

        $objPdf->SetFillColor(255, 255, 255);
        $objPdf->Rect($objPdf->getX(), $objPdf->getY()-0.7, 167, 2, 'F');

        $y=$objPdf->GetY();
        $objPdf->SetY($y+1);

        $columns = array();

        if ($objPdf->GetY() >= (267 - 7)) {
            $objPdf->SetAutoPageBreak(false, 10);
        }

        $col = array();
        $col[] = array('text' => utf8_decode('Summe'), 'align' => 'L', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode(''),'theme' => 'head3');
        $col[] = array('text' => utf8_decode(number_format($arrMonthSum[1], 0, ',', '.')), 'font_size' => '4.5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode(number_format($arrMonthSum[2], 0, ',', '.')), 'font_size' => '4.5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode(number_format($arrMonthSum[3], 0, ',', '.')), 'font_size' => '4.5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode(number_format($arrMonthSum[4], 0, ',', '.')), 'font_size' => '4.5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode(number_format($arrMonthSum[5], 0, ',', '.')), 'font_size' => '4.5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode(number_format($arrMonthSum[6], 0, ',', '.')), 'font_size' => '4.5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode(number_format($arrMonthSum[7], 0, ',', '.')), 'font_size' => '4.5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode(number_format($arrMonthSum[8], 0, ',', '.')), 'font_size' => '4.5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode(number_format($arrMonthSum[9], 0, ',', '.')), 'font_size' => '4.5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode(number_format($arrMonthSum[10], 0, ',', '.')), 'font_size' => '4.5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode(number_format($arrMonthSum[11], 0, ',', '.')), 'font_size' => '4.5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode(number_format($arrMonthSum[12], 0, ',', '.')), 'font_size' => '4.5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode(number_format($intCountAllSv, 0, ',', '.')), 'theme' => 'head3');
        $columns[] = $col;


        // Draw Table   
        $objPdf->WriteTable($columns);

        if (count($arrNotOkSvList) > 0) {

            $objPdf->SetFillColor(242,83,35);
            $objPdf->RoundedRect(21.5 + 1.5, $objPdf->getY() + 2.6, 1.5, 1.5, 1, '1234', 'F');

            $objPdf->SetTextColor(192,192,192); //#000000
            $objPdf->SetFont('Roboto', '', 5.5);
            $objPdf->setX($objPdf->getX() + 3);
            $objPdf->MultiCell(210 - 43, 7, utf8_decode('Es gab nicht fristgerecht geklärte Beitragsrückstände'), 0, 'L', false);

        }

        /* ***************************** BG  ***************************** */

        $objPdf->AddPage('P');
        $objPdf->SetAutoPageBreak(false);
        $objPdf->Image( __DIR__ .'/img/follow.png', 0, 0, 210, 297, 'PNG');
        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 6.5);
        $objPdf->Text(15, 12, utf8_decode('Jahresbericht ' .$arrData['year']));
        $objPdf->SetFont('Roboto', '', 6.5);
        $objPdf->Text(37, 12, utf8_decode($arrData['name']));

        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetXY(185, 290);
        $objPdf->SetFont('Roboto', '', 6.5);
        $objPdf->Cell(20,0,'Seite '.$objPdf->PageNo().' von {nb}',0,0,'3');
        $objPdf->SetXY(21.5, 50);


        $objPdf->SetLink($arrPdfLinkList['Berufsgenossenschaften'], 0, -1);
        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 24);
        $objPdf->Text(21.5, 62, utf8_decode('Meldungen an'));
        $objPdf->Text(21.5, 72, utf8_decode('Berufsgenossenschaften'));

        // CONTENT
        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 9);
        $objPdf->setXY(20, 86);
        $objPdf->MultiCell(210 - 43, 7, utf8_decode('Gesamtanzahl der geprüften BG-Meldungen'), 0, 'L', false);

        $objPdf->SetDrawColor(223, 223, 223);
        $objPdf->SetLineWidth(0.2);
        $objPdf->Line(21.5, 97, 187, 97);

        $strUnique = uniqid();

        $intMax = max($arrMonthSumBg);
        $intMin = min($arrMonthSumBg);

        $intDiff = (int) ($intMax - $intMin);

        if ($intDiff < 6) { $intSteps = 1; }
        elseif ($intDiff < 11) { $intSteps = 2; }
        elseif ($intDiff < 26) { $intSteps = 5; }
        elseif ($intDiff < 51) { $intSteps = 10; }
        elseif ($intDiff < 101) { $intSteps = 20; }
        elseif ($intDiff < 251) { $intSteps = 50; }
        elseif ($intDiff < 501) { $intSteps = 100; }
        elseif ($intDiff > 501) { $intSteps = 200; }

        $intMin = $intMin - ($intSteps * 2);
        if ($intMin < 0) $intMin = 0;

        if ($intMax < 5) {
            $intMax = $intMax + (1 * $intSteps);
        } else {
            $intMax = $intMax + (2 * $intSteps);
        }

        $strChart = $strChartTemplate;
        $strChart = str_replace('*DATA*', implode(',', $arrMonthSumBg), $strChart);
        $strChart = str_replace('*MIN*', $intMin, $strChart);
        $strChart = str_replace('*MAX*', $intMax, $strChart);
        $strChart = str_replace('*STEPS*', $intSteps, $strChart);

        $chart_url = 'https://quickchart.io/chart?w=935&h=242&v=2.9.4&c=' .urlencode($strChart);
        file_put_contents($strUnique .'.png', file_get_contents($chart_url));

        $objPdf->Image( __DIR__ .'/' .$strUnique .'.png', 18.5, 103, 172, 44.5, 'PNG');

        if (file_exists($strUnique .'.png')) unlink($strUnique .'.png');


        $objPdf->SetDrawColor(44, 45, 46);
        $objPdf->SetLineWidth(0.2);
        $objPdf->Line(21.5, 155, 187, 155);

        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 7);
        $objPdf->setXY(20, 157);
        $objPdf->MultiCell(210 - 43, 6, utf8_decode('Summe: ' .number_format($intCountAllBg, 0, ',', '.')), 0, 'L', false);

        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 9);
        $objPdf->setXY(20, 184);
        $objPdf->MultiCell(210 - 43, 7, utf8_decode('Anzahl der geprüften BG-Meldungen pro Meldestelle'), 0, 'L', false);

        $objPdf->SetDrawColor(223, 223, 223);
        $objPdf->SetLineWidth(0.2);
        $objPdf->Line(21.5, 194, 187, 194);


        $objPdf->SetAutoPageBreak(true, 30);

        //       Jun Jul Aug Sep Okt Nov Dez Summe

        $objPdf->SetY(200);

        $columns = array();      
        
        // header col
        $col = array();
        $col[] = array('text' => utf8_decode('BG-Meldestelle'), 'align' => 'L', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Betriebsnr.'),'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Jan'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Feb'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Mär'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Apr'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Mai'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Jun'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Jul'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Aug'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Sep'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Okt'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Nov'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Dez'), 'font_size' => '5', 'theme' => 'head2');
        $col[] = array('text' => utf8_decode('Summe'), 'theme' => 'head2');
        $columns[] = $col;


        $arrBreak['table2'] = array(
            'head1' => utf8_decode('Jahresbericht ' .$arrData['year']),
            'head2' => utf8_decode($arrData['name']),
            'head3' => utf8_decode('Anzahl der geprüften BG-Meldungen pro Meldestelle (Fortsetzung)'),
            'image' => __DIR__ .'/img/follow.png',
            'coloms' => $columns
        );

        $objPdf->createBreak($arrBreak);

        $arrSumMonth = $arrEmpty;

        if (count($arrBnrSumBg) > 0) {

            foreach ($arrBnrSumBg as $strName => $arrDataset) {

                $col = array();
                $col[] = array('text' => utf8_decode($strName), 'align' => 'L', 'theme' => 'body2');
                $col[] = array('text' => $arrData['bnr'],'theme' => 'body2');

                for ($i = 1; $i <= 12; $i++) {
                    
                    (isset($arrNotOkBgList[$strName][$i])) ? ($boolHighlight = true) : ($boolHighlight = false);
                    $col[] = array('text' => utf8_decode($arrDataset['month'][$i]), 'theme' => 'body2', 'highlight' => $boolHighlight);
                }

                $col[] = array('text' => utf8_decode(array_sum($arrDataset['month'])), 'theme' => 'body2');
                $columns[] = $col;

            }

        }

        $objPdf->WriteTable($columns);

        $objPdf->SetFillColor(255, 255, 255);
        $objPdf->Rect($objPdf->getX(), $objPdf->getY()-0.7, 167, 2, 'F');

        $y=$objPdf->GetY();
        $objPdf->SetY($y+1);

        $columns = array();

        if ($objPdf->GetY() >= (267 - 7)) {
            $objPdf->SetAutoPageBreak(false, 10);
        }

        $col = array();
        $col[] = array('text' => utf8_decode('Summe'), 'align' => 'L', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode(''),'theme' => 'head3');
        $col[] = array('text' => utf8_decode($arrMonthSumBg[1]), 'font_size' => '5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode($arrMonthSumBg[2]), 'font_size' => '5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode($arrMonthSumBg[3]), 'font_size' => '5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode($arrMonthSumBg[4]), 'font_size' => '5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode($arrMonthSumBg[5]), 'font_size' => '5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode($arrMonthSumBg[6]), 'font_size' => '5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode($arrMonthSumBg[7]), 'font_size' => '5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode($arrMonthSumBg[8]), 'font_size' => '5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode($arrMonthSumBg[9]), 'font_size' => '5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode($arrMonthSumBg[10]), 'font_size' => '5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode($arrMonthSumBg[11]), 'font_size' => '5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode($arrMonthSumBg[12]), 'font_size' => '5', 'theme' => 'head3');
        $col[] = array('text' => utf8_decode(number_format($intCountAllBg, 0, ',', '.')), 'theme' => 'head3');
        $columns[] = $col;


        // Draw Table   
        $objPdf->WriteTable($columns);

        if (count($arrNotOkBgList) > 0) {

            $objPdf->SetFillColor(242,83,35);
            $objPdf->RoundedRect(21.5 + 1.5, $objPdf->getY() + 2.6, 1.5, 1.5, 1, '1234', 'F');

            $objPdf->SetTextColor(192,192,192); //#000000
            $objPdf->SetFont('Roboto', '', 5.5);
            $objPdf->setX($objPdf->getX() + 3);
            $objPdf->MultiCell(210 - 43, 7, utf8_decode('Es gab nicht fristgerecht geklärte Beitragsrückstände'), 0, 'L', false);

        }

        /* ***************************** AÜ ***************************** */


        $objPdf->AddPage('P');
        $objPdf->SetAutoPageBreak(false);
        $objPdf->Image( __DIR__ .'/img/follow.png', 0, 0, 210, 297, 'PNG');
        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 6.5);
        $objPdf->Text(15, 12, utf8_decode('Jahresbericht ' .$arrData['year']));
        $objPdf->SetFont('Roboto', '', 6.5);
        $objPdf->Text(37, 12, utf8_decode($arrData['name']));

        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetXY(185, 290);
        $objPdf->SetFont('Roboto', '', 6.5);
        $objPdf->Cell(20,0,'Seite '.$objPdf->PageNo().' von {nb}',0,0,'3');
        $objPdf->SetXY(21.5, 50);


        $objPdf->SetLink($arrPdfLinkList['Erlaubnisse'], 0, -1);
        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 24);
        $objPdf->Text(21.5, 62, utf8_decode('Erlaubnisse zur'));
        $objPdf->Text(21.5, 72, utf8_decode('Arbeitnehmerüberlassung'));

        // CONTENT
        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', 'B', 9);
        $objPdf->setXY(20, 86);
        $objPdf->MultiCell(210 - 43, 7, utf8_decode('Stand: 31.12.' .$arrData['year']), 0, 'L', false);

        $objPdf->SetTextColor(0, 0, 0); //#000000
        $objPdf->SetFont('Roboto', '', 9);
        $objPdf->setXY(20, 93);
        $objPdf->MultiCell(210 - 43, 7, utf8_decode('Quelle: Webseite der Bundesagentur für Arbeit (BA)'), 0, 'L', false);

        $floatX = 143;
        $floatY = 77;

        $objPdf->SetFillColor(242, 83, 35);
        $objPdf->RoundedRect($floatX, $floatY + 12, 45, 10, 1, '1234', 'F');
   
        $objPdf->SetTextColor(255, 255, 255);
        $objPdf->SetXY($floatX + 1.5, $floatY + 15.2);    

        $objPdf->SetFont('Roboto', '', 10);
        $objPdf->MultiCell(73, 4, utf8_decode('Zur BA-Webseite'), 0, '');

        $objPdf->SetXY($floatX + 34, $floatY + 15.2);    
        $objPdf->SetFont('Symbol', '', 10);
        $objPdf->WriteHTML('' .chr(174) .'');

        //$strUrl = 'izs-portal.netlify.app';
        $strUrl = 'https://spitzenverbaende.arbeitsagentur.de/';
        $objPdf->Link($floatX, $floatY + 12, 45, 10, $strUrl);


        $objPdf->SetAutoPageBreak(true, 30);

        //       Jun Jul Aug Sep Okt Nov Dez Summe

        $objPdf->SetY(110);

        $strSql = 'SELECT * FROM `izs_aue_erlaubnis` WHERE `au_pp_group` = "' .$strGroupId .'" ORDER BY `au_name`, `au_zip`';
        $arrSql = MySQLStatic::Query($strSql);

        if (count($arrSql) > 0) {

            $columns = array();
            
            // header col
            $col = array();
            $col[] = array('text' => utf8_decode('Erlaubnisinhaber'), 'align' => 'L', 'theme' => 'head4');
            $col[] = array('text' => utf8_decode('Adresse'), 'align' => 'L','theme' => 'head4');
            $col[] = array('text' => utf8_decode('PLZ'), 'align' => 'L', 'theme' => 'head4');
            $col[] = array('text' => utf8_decode('Ort'), 'align' => 'L', 'theme' => 'head4');
            $col[] = array('text' => utf8_decode('Land'), 'align' => 'L', 'theme' => 'head4');

            $columns[] = $col;


            $arrBreak['table3'] = array(
                'head1' => utf8_decode('Jahresbericht ' .$arrData['year']),
                'head2' => utf8_decode($arrData['name']),
                'head3' => utf8_decode('Erlaubnisse zur Arbeitnehmerüberlassung (Fortsetzung)'),
                'image' => __DIR__ .'/img/follow.png',
                'coloms' => $columns
            );

            $objPdf->createBreak($arrBreak);

            ///*
            if (count($arrBnrSumBg) > 0) {

                foreach ($arrSql as $intKey => $arrDataset) {

                    if ($arrDataset['au_country'] == '') {
                        $arrDataset['au_country'] = 'Deutschland';
                    }

                    $col = array();
                    $col[] = array('text' => utf8_decode($arrDataset['au_name']), 'align' => 'L', 'theme' => 'body4');
                    $col[] = array('text' => utf8_decode($arrDataset['au_street']), 'align' => 'L','theme' => 'body4');
                    $col[] = array('text' => utf8_decode($arrDataset['au_zip']), 'align' => 'L', 'theme' => 'body4');
                    $col[] = array('text' => utf8_decode($arrDataset['au_city']), 'align' => 'L', 'theme' => 'body4');
                    $col[] = array('text' => utf8_decode($arrDataset['au_country']), 'align' => 'L', 'theme' => 'body4');
                    $columns[] = $col;

                }

            }
            //*/

            $objPdf->WriteTable($columns);

        } else {

            $objPdf->SetTextColor(0, 0, 0); //#000000
            $objPdf->SetFont('Roboto', 'B', 9);
            $objPdf->setX(20);
            $objPdf->MultiCell(210 - 43, 7, utf8_decode($arrSqlG[0]['RatingAUeHinweis__c']), 0, 'L', false); 

        }


        /* ***************************** MITGLIEDSCHAFT ***************************** */

        $objPdf->AddFont('Lato', '', 'Lato-Regular.php');
        $objPdf->AddFont('Lato', 'B', 'Lato-Bold.php');

        $objPdf->AddFont('OfficinaSerif', '', 'itcofficinaserifw04medium-webfont.php');
        $objPdf->AddFont('OfficinaSerif', 'B', 'itcofficinaserifw04medium-webfont.php');

        if ($boolCertMissing === false) {

            $boolStandAlone = false;

            //$intBeginTime, $arrPdfLinkListEvent, $arrSql, $boolCreate, $intCreateUser, $strKuendigung;

            $intBeginTime = 0;
            $arrPdfLinkListEvent[$arrSql[0]['evid']] = $arrPdfLinkList['Mitgliedschaft'];

            $boolCreate = false;
            $intCreateUser = 0;

            $strKuendigung = getKuendigung($strGroupId);

            require_once ('../zertifikat/create.zertifikat.cert.inc.php');


            $strSql = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$strGroupId .'"';
            $arrSql = MySQLStatic::Query($strSql);

            createCert(0, $objPdf, 'string', $arrSql);

            $boolStandAlone = true;

        }


        /* ***************************** SUBSIDIÄR ***************************** */

        if ($boolProtectMissing === false) {

            $boolStandAlone = false;

            //$intBeginTime, $arrPdfLinkListEvent, $arrSql, $boolCreate, $intCreateUser, $strKuendigung;
    
            $intBeginTime = 0;
            $arrPdfLinkListEvent[$arrSql[0]['evid']] = $arrPdfLinkList['SubsidiärProtect'];
    
            $boolCreate = false;
            $intCreateUser = 0;
    
            $strKuendigung = getKuendigung($strGroupId);
    
            require_once ('../protect/create.subsidiaerprotect.cert.inc.php');
    
    
            $strSql = 'SELECT * FROM `Group__c` WHERE `Id` = "' .$strGroupId .'"';
            $arrSql = MySQLStatic::Query($strSql);
    
            createCertSp(0, $objPdf, 'string', $arrSql);
    
            $boolStandAlone = true;
        
        }

        /* ***************************** BACK ***************************** */


        $objPdf->AddPage('P');
        $objPdf->SetAutoPageBreak(false);
        $objPdf->Image( __DIR__ .'/img/back.png', 0, 0, 210, 297, 'PNG');

        $objPdf->SetMargins(21.5, 21.5);

        $objPdf->SetTextColor(255, 255, 255);
        $objPdf->SetXY(21.5, 30);    

        $objPdf->SetFont('Roboto', '', 10);
        $objPdf->MultiCell(210 - 43, 7, utf8_decode('Grundlage für Prüfungen der Zahlungen an die Berufsgenossenschaften und Krankenkassen waren alle ordnungsgemäß und fristgerecht zum Beitragsmonat übermittelten Beitragsnachweisdaten der Unternehmensgruppe.'), 0, '');


        
        $objPdf->SetXY(21.5, 235);  

        $objPdf->SetFont('Roboto', 'B', 10);
        $objPdf->MultiCell(120, 7, utf8_decode('IZS Institut für Zahlungssicherheit GmbH'), 0, 'L', false);

        $objPdf->SetFont('Roboto', '', 10);
        $objPdf->MultiCell(120, 7, utf8_decode('Würmtalstraße 20a'), 0, 'L', false);
        $objPdf->MultiCell(120, 7, utf8_decode('81375 München'), 0, 'L', false);
        $objPdf->MultiCell(120, 7, utf8_decode('+49 (0) 89 122 237 77 0'), 0, 'L', false);

        $objPdf->setFillColor(2, 19, 37);
        $objPdf->MultiCell(120, 7, utf8_decode('info@izs-institut.de'), 0, 'L', 'mailto:info@izs-institut.de');
        $objPdf->MultiCell(120, 7, utf8_decode('www.izs.de'), 0, 'L', 'https://www.izs.de/');
        
        $intDurationTime = microtime(true) - $intBeginTime;
        if ($_REQUEST['d'] == 't') echo " Process PDF: $intDurationTime Sek." .chr(10);

        if (($strReturnType == 'string') && ($boolStandAlone === true)) {
            return $objPdf->Output('S'); //Output($strFileName, 'F')
        }

    }

}

if ($boolStandAlone === true) {

    $strOutput = createJahresbericht($strGroupIdJB);
}

?>