<?php

/*

// quickchart-php https://github.com/typpo/quickchart-php

require 'QuickChart.php';

$qc = new QuickChart(array(
  'width' => 500,
  'height' => 300,
  'version' => '2',
));

$config = <<<EOD
{
  type: 'line',
  data: {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label: '',
        backgroundColor: 'rgb(255, 99, 132)',
        borderColor: 'rgb(255, 99, 132)',
        data: [93, -29, -17, -8, 73, 98, 40],
        fill: false,
      }
    ],
  },
  options: {
    title: {
      display: false,
      text: 'Chart.js Line Chart',
    },
  },
}
EOD;

// Chart config can be set as a string or as a nested array
$qc->setConfig($config);

// Print the chart URL
//echo $qc->getUrl();

// Get the image
$image = $qc->toBinary();

// Or write it to a file
$qc->toFile('chart.png')

*/

?><?php

// Daten für das Liniendiagramm
$data = array(
    "labels" => ["Jan", "Feb", "Mar"],
    "datasets" => array(
        array(
            "label" => "Einnahmen",
            "data" => [200, 400, 600]
        ),
        array(
            "label" => "Ausgaben",
            "data" => [150, 300, 450]
        )
    )
);

// Konvertiere Daten in JSON
$data_json = json_encode($data);

// Erstelle die URL für das Diagramm
$chart_url = "https://chart.googleapis.com/chart?cht=lc&chs=400x300&chd=t:" . urlencode(implode(",", $data['datasets'][0]['data'])) . "|" . urlencode(implode(",", $data['datasets'][1]['data'])) . "&chxt=x,y&chxl=0:|" . implode("|", $data['labels']);

// Lade das PNG-Diagramm herunter
file_put_contents("chart.png", file_get_contents($chart_url));

//echo "Das Diagramm wurde heruntergeladen als chart.png";

?>

<img src="chart.png" />