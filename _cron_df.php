<?php

date_default_timezone_set('Europe/Berlin');

ini_set("memory_limit", "-1");
set_time_limit(0);

function formatBytes ($iBytes) {
    $aUnits = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB');
    $sResult = '0.00 B';
       
    if($iBytes > 0) {
        $fResult = log($iBytes) / log(1024);       
        $sResult = round(pow(1024, $fResult - ($fTmp = floor($fResult))), 2) .' ' .$aUnits[$fTmp];
    }
    return $sResult;
}

$intReportLimit = 1024 * 1024 * 1024 * 8; // 8GB
$intReportPercent = 10;

$strSubject = 'ACHTUNG! Zu wenig Speicherplatz: www.izs.de';

$strShell = 'df | grep /dev/md | awk \'{ print $6 "_" $2 "_" $3 "_" $4 "_" $5 }\'';

$strDf =  shell_exec($strShell);
$intValueCount = count(explode('_', $strShell));

$arrDfHd = explode(chr(10), $strDf);

foreach ($arrDfHd as $intKey => $strDfHd) {

    if ($strDfHd !== '') {

        $arrDf = explode('_', $strDfHd);

        $arrDf[1] = $arrDf[1] * 1024;
        $arrDf[2] = $arrDf[2] * 1024;        
        $arrDf[3] = $arrDf[3] * 1024;

        if ((count($arrDf) == $intValueCount) && ($arrDf[0] != '/boot')){

            $arrDf[4] = (int) substr($arrDf[4], 0, -1);
            
            $strError = '';

            if ($arrDf[3] < $intReportLimit) {
                $strError.= '' .$arrDf[0] .': ' .formatBytes($arrDf[3]) .' < ' .formatBytes($intReportLimit) .'';
            }

            if ((100 - $arrDf[4]) < $intReportPercent) {
                if ($strError != '') {
                    $strError.= chr(10);
                }
                $strError.= '' .$arrDf[0] .': ' .(100 - $arrDf[4]) .'% < ' .$intReportPercent .'%';
            }

            if ($strError != '') {
                mail('system@izs-institut.de', $strSubject, $strError);
            }

        }
    
    }

}

?>