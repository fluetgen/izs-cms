<?php

//redirector.inc.php

//if ($_SERVER['REMOTE_ADDR'] == '146.52.241.185') {
if (false) {
  
  $strRedirectPage = '';

  $strMethod  = $_SERVER['REQUEST_METHOD'];
  $arrRequest = explode('/', trim($_REQUEST['q'],'/'));
  $arrQuery   = $_REQUEST;
  
  echo chr(10) .'strMETHOD:  ' .$strMethod .chr(10);
  echo chr(10) .'arrREQUEST: ' .print_r($arrRequest, true);
  echo chr(10) .'arrQUERY:   ' .print_r($arrQuery, true);
  echo chr(10) .'REDIRECT:   ' .$strRedirectPage;
  
  exit;

}

//if ($_SERVER['REMOTE_ADDR'] == '62.245.254.82') {
if (true) {

  if (strstr(@$_SERVER['SERVER_NAME'], 'test') !== false) {
    $strServer = 'test';
  } else {
    $strServer = 'www';
  }
  
  $strHost   = 'https://' .$strServer .'.izs-institut.de/';
  $strPortal = 'portal.html';
  
  $strRedirectPage = '';

  $strMethod  = $_SERVER['REQUEST_METHOD'];
  $arrRequest = explode('/', trim($_REQUEST['q'],'/'));
  $arrQuery   = $_REQUEST;

  if ($arrRequest[0] == 'protect') {

    include('protect/index.php');
    exit;

  }
  
  if (isset($arrRequest[0])) {
    
    $strRequestPage = $arrRequest[0];

    if (($strRequestPage == 'portal-startseite.html') && ($_SERVER['HTTP_HOST'] == 'izs.de')) {
      $strRedirectPage = $strHost .$strPortal .'#!/company/a083000000IaKp3AAF/insurance/account/001300000109ounAAA'; //http://www.izs-institut.de/portal.html#!/company/a083000000IaKp3AAF/insurance/account/001300000109ounAAA
    }
    
    //PORTAL
    if ($strRequestPage == 'unternehmensinfo.html') {
      $strRedirectPage = $strHost .$strPortal .'#!/company/' .$arrQuery['companyid'];
    }
    
    if ($strRequestPage == 'auswahl-meldestellen.html') {
      $strRedirectPage = $strHost .$strPortal .'#!/company/' .$arrQuery['companyid'] .'/insurance';
    }
    
    if ($strRequestPage == 'kontoauszug.html') {
      $strRedirectPage = $strHost .$strPortal .'#!/company/' .$arrQuery['companyid'] .'/insurance/account/' .$arrQuery['accountid'];
      if (isset($arrQuery['selectedMonth']) && isset($arrQuery['selectedYear'])) {
        $strRedirectPage = $strHost .$strPortal .'#!/company/' .$arrQuery['companyid'] .'/insurance/account/' .$arrQuery['accountid'] .'?year=' .$arrQuery['selectedYear'] .'&month=' .$arrQuery['selectedMonth'];
      }
    }
    
    if ($strRequestPage == 'berufsgenossenschaften.html') {
      $strRedirectPage = $strHost .$strPortal .'#!/company/' .$arrQuery['companyid'] .'/liability';
    }
    
    if ($strRequestPage == 'aue-erlaubnis.html') {
      $strRedirectPage = $strHost .$strPortal .'#!/company/' .$arrQuery['companyid'] .'/lease';
    }
    
    if ($strRequestPage == 'kostenloser-infoservice.html') {
      $strRedirectPage = $strHost .$strPortal .'#!/company/' .$arrQuery['companyid'] .'/infoservice';
    }
    
    if ($strRequestPage == 'portal-home') {
      $strRedirectPage = $strHost .$strPortal;
    }
    
    //CONTENT
    if ($strRequestPage == 'zeitarbeitsfirma.html') {
      $strRedirectPage = $strHost .'/izs-fur-zeitarbeitsfirmen/';
    }
    
    if ($strRequestPage == 'kundenunternehmen.html') {
      $strRedirectPage = $strHost .'/izs-fur-entleiher/';
    }

    if ($strRequestPage == 'infoservice.html') {
      $strRedirectPage = $strHost .'/kontakt/';
    }

    if ($strRequestPage == 'unternehmen.html') {
      $strRedirectPage = $strHost;
    }
    
    if ($strRequestPage == 'agb.html') {
      $strRedirectPage = $strHost .'/agb/';
    }

    if ($strRequestPage == 'presse') {
      $strRedirectPage = $strHost .'/blog/';
    }
    
    if (($strRequestPage == 'angebotsanfrage') || ($strRequestPage == 'angebotsanfrage.html')) {
      $strRedirectPage = $strHost .'/angebot-anfordern/';
    }

    
    if ($strRequestPage == 'kostenloser-infoservice-rd.html') {
        $strRedirectPage = $strHost .$strPortal .'#!/company/a083000000IaKp3AAF/infoservice';
    }    

    if ($strRequestPage == 'kostenloser-infoservice-gulp.html') {
        $strRedirectPage = $strHost .$strPortal .'#!/company/a083000000RvE85AAF/infoservice';
    }
    
    //...
    
    
    //LANDING PAGES
    if (isset($arrRequest[1]) && ($arrRequest[1] == 'infoservice')) {

      if ($strRequestPage == 'timepartner') {
        $strRedirectPage = $strHost .$strPortal .'#!/company/a083000000UW4WuAAL/infoservice';
      }

      if ($strRequestPage == 'meteor') {
        $strRedirectPage = $strHost .$strPortal .'#!/company/a083000000IaKp0AAF/infoservice';
      }

      if ($strRequestPage == 'orizon') {
        $strRedirectPage = $strHost .$strPortal .'#!/company/a083000000LQSO0AAP/infoservice';
      }

      if ($strRequestPage == 'randstad') {
        $strRedirectPage = $strHost .$strPortal .'#!/company/a083000000IaKp3AAF/infoservice';
      }

    }

    
    if (isset($arrRequest[1]) && ($arrRequest[1] == 'krankenkassen')) {

      if ($strRequestPage == 'randstad') {
        $strRedirectPage = $strHost .$strPortal .'#!/company/a083000000IaKp3AAF/insurance/account/001300000109ounAAA';
      }

    }

    if (isset($arrRequest[1]) && ($arrRequest[1] == 'berufsgenossenschaften')) {

      if ($strRequestPage == 'randstad') {
        $strRedirectPage = $strHost .$strPortal .'#!/company/a083000000IaKp3AAF/liability';
      }

    }
    
    if (isset($arrRequest[1]) && ($arrRequest[1] == 'aue-erlaubnisse')) {

      if ($strRequestPage == 'randstad') {
        $strRedirectPage = $strHost .$strPortal .'#!/company/a083000000IaKp3AAF/lease';
      }

    }
    
  }

  if (isset($arrQuery['d']) && ($arrQuery['d'] == 1)) {
    
    echo chr(10) .'strMETHOD:  ' .$strMethod .chr(10);
    echo chr(10) .'arrREQUEST: ' .print_r($arrRequest, true);
    echo chr(10) .'arrQUERY:   ' .print_r($arrQuery, true);
    echo chr(10) .'REDIRECT:   ' .$strRedirectPage;
    
  } elseif ($strRedirectPage != '') {
    
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' .$strRedirectPage);
    header('Connection: close'); 
    exit;
    
  }  
  
  
}

?>
