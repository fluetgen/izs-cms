<?php

date_default_timezone_set('Europe/Berlin');

ini_set("memory_limit", "-1");
ini_set('default_socket_timeout', 900);
set_time_limit(0);

if (isset($_SERVER['SERVER_NAME']) && (strstr($_SERVER['SERVER_NAME'], 'test.') !== false)) {
  $strFileRoot = 'https://test.izs.de/cms/';
} else {
  $strFileRoot = 'https://www.izs.de/cms/';
}

$intLimitPerMinute = 500;
//$intLimitPerMinute = 6;

require_once('assets/classes/class.mysql.php');

$strSql = 'SELECT * FROM `_cron_running` WHERE `cr_running` = 1 AND `cr_name` = "create_request"';
$arrRow = MySQLStatic::Query($strSql);

if (count($arrRow) > 0) {

  echo 'Cron is running!';
  exit;

} else {

  $strSql = 'SELECT * FROM `_cron_url` WHERE `cu_status` = 1 ORDER BY `cu_id` ASC LIMIT ' .$intLimitPerMinute;
  $arrRow = MySQLStatic::Query($strSql);

  if (count($arrRow) > 0) {

    $strSql     = 'INSERT INTO `_cron_running` (`cr_id`, `cr_name`, `cr_running`, `cr_start`, `cr_stop`) VALUES (NULL, "create_request",  "1", NOW(), "")';
    $intStartId = MySQLStatic::Insert($strSql);

    foreach ($arrRow as $intKey => $arrUrl) {
      
      $strSql = 'UPDATE `_cron_url` SET `cu_status` = 4, `cu_time` = NOW() WHERE `cu_id` = "' .$arrUrl['cu_id'] .'"';
      $arrRow = MySQLStatic::Update($strSql);

      //unset($arrRow[$intKey]);
      
      $strCron = md5(rand(0, 999));
      $strHash = md5('Triple2013' .$strCron);

      $strUrl = '';

      if (strstr($arrUrl['cu_url'], 'http') === false) {
        $strUrl.= $strFileRoot;
      }
      
      $strUrl.= $arrUrl['cu_url']; 

      if (strstr($strUrl, '?') === false) {
        $strUrl.= '?';
      } else {
        $strUrl.= '&';
      }
      
      $strUrl.= 'cron=' .$strCron .'&hash=' .$strHash;

      echo $strUrl .chr(10);

      $strPage = file_get_contents($strUrl);

      unset($arrRow[$intKey]);
      
      $strSql = 'UPDATE `_cron_url` SET `cu_status` = 2, `cu_time` = NOW() WHERE `cu_id` = "' .$arrUrl['cu_id'] .'"';
      $arrRow = MySQLStatic::Update($strSql);

      //sleep(1);
      
    }

    $strSql     = 'UPDATE `_cron_running` SET `cr_running` = "0", `cr_stop` = NOW() WHERE `cr_id` ="' .$intStartId .'"';
    $intStartId = MySQLStatic::Insert($strSql);

  }

}

?>