<?PHP

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('assets/classes/class.mysql.php');
require_once('cms/inc/refactor.inc.php');

//find Chat
$arrHeader = array(
    'apikey: e9d99940-1aed-414a-b836-b929fe97d093',
    'user: 4',
    'Accept: application/json'
);


$strSql = 'SELECT `izs_bg_event`.`beid`, `izs_bg_event`.`be_name`, `izs_cms2sg`.`cs_sg_index` FROM `izs_bg_event` INNER JOIN `izs_cms2sg` ON `cs_cms_index` = `beid` WHERE `cs_type` = "BG Event" AND `be_status_bearbeitung` = "angefragt" ORDER BY `be_name` DESC';
$arrSql = MySQLStatic::Query($strSql);

if (count($arrSql) > 0) {

    foreach ($arrSql as $intKey => $arrDataset) {

        $arrGet = array();

        $arrRes = curl_get('http://api.izs-institut.de/api/sg/topic/' .$arrDataset['cs_sg_index'], $arrGet, array(), $arrHeader);
        $arrToppicRaw = json_decode($arrRes, true);

        $intEsc = mktime(12, 0, 0, 11, 23, 2022) * 1000;
        $intEscDate = (int) $intEsc; //($intEsc + 43200 + (86400 * 7)) * 1000; // 12:00 Uhr (+ 7 Tage)

        if (isset($arrToppicRaw['id'])) {

            $arrPart = explode(': ', $arrToppicRaw['title']);

            if (count($arrPart) > 1) {

                if ($arrPart[0] == 'anfordern') {

                    $arrToppicChange = array(
                        'title' => str_replace('anfordern:', 'Warten auf Eingang:', $arrToppicRaw['title']),
                        'due' => (int) $intEscDate          
                    );

                    //print_r($arrToppicChange); 

                    $arrRes = curl_post('http://api.izs-institut.de/api/sg/topic/' .$arrToppicRaw['id'], $arrToppicChange, array(), $arrHeader);
                    $arrTop = json_decode($arrRes, true);

                    //print_r($arrRes);

                    sleep(1);

                    echo 'Done: ' .$arrToppicRaw['title'] .chr(10);
                    //die();

                }

            } else {

                echo 'Nope: (Title)' .$arrToppicRaw['title'] .chr(10);

            }

        } else {

            echo 'Nope: (Chat)' .$arrDataset['cs_cms_index'] .' -> ' .$arrDataset['cs_sg_index'] .chr(10);
            die();

        }

    }

}

die();


/*

  for ($intStart = 3398; $intStart <= 3901; $intStart++) {

        $arrGet = array(
            'from' => 0,
            'q' => 'BG-00' .$intStart,
            'state' => 'open',
            'type' => 'topic'
        );

        $arrRes = curl_get('http://api.izs-institut.de/api/sg/browse/topic', $arrGet, array(), $arrHeader);
        $arrToppicList = json_decode($arrRes, true);

        if (isset($arrToppicList['items'][0]['id'])) {
            
            $arrRes = curl_post('http://api.izs-institut.de/api/sg/topic/' .$arrToppicList['items'][0]['id'] .'/flag/done', array(), array(), $arrHeader);
            $arrToppicUpdate = json_decode($arrRes, true);
    
            echo 'done: ' .$arrToppicList['items'][0]['id'] .chr(10);

        }


    }

*/

  /*
Anfrage-URL: 
Anfragemethode: POST
Statuscode: 200 
Remote-Adresse: 217

  */
die();

/*
$strSql = 'SELECT * FROM `Account` WHERE `RecordTypeId` = "01230000001Ao71AAC" AND `SF42_Sub_Type__c` = "Premium Payer" AND `SF42_isPremiumPayer__c` = "true" AND `Aktiv__c` = "true" AND `SF42_use_data__c` = "true"';
$arrSql = MySQLStatic::Query($strSql);

foreach ($arrSql as $intKey => $arrDataset) {

    if ($arrDataset['Registriert_seit__c'] == '0000-00-00') {

        //'12/2011'
        $strMonth = '12';
        $strYear  = '2011';

    } else {

        $arrPart = explode('-', $arrDataset['Registriert_seit__c']);
        $strMonth = $arrPart[1];
        $strYear  = $arrPart[0];

    } 

    $strUbbText = 'immer fristgerecht'; // 'überwiegend fristgerecht'

    $strSql = 'UPDATE `Account` SET `UbbFormulierung__c` = "' .$strUbbText .'", `UbbSeitMonat__c` = "' .$strMonth .'", `UbbSeitJahr__c` = "' .$strYear .'" WHERE `Id` = "' .$arrDataset['Id'] .'"';
    $intSql = MySQLStatic::Update($strSql);
    //echo $strSql .chr(10);


}
*/


for ($intCount = 1; $intCount <= 21; $intCount++) {

$intRandStartMinus = rand(0, 15 * 60);
$intRandStartPlus  = rand(0, 20 * 60);

$indMinus = rand(0, 1);

if ($indMinus == 1) {
    $intStart = mktime(9, 0, 0 - $intRandStartMinus, date('d'), date('m'), date('Y'));
} else {
    $intStart = mktime(9, 0, 0 + $intRandStartPlus, date('d'), date('m'), date('Y'));
}

$intBreakAdd = rand(0, 3);

$intStop = $intStart + (($intBreakAdd + 30) * 60) + (7 * 60 * 60) + rand(0, 25 * 60);

$intRandStartMinus = rand(0, 10 * 60);
$intRandStartPlus  = rand(0, 15 * 60);

$indMinus = rand(0, 1);

if ($indMinus == 1) {
    $intBreak = mktime(13, 0, 0 - $intRandStartMinus, date('d'), date('m'), date('Y'));
} else {
    $intBreak = mktime(13, 0, 0 + $intRandStartPlus, date('d'), date('m'), date('Y'));
}

echo date('Hi', $intStart);
echo  '    ' .chr(10);
echo date('Hi', $intStop);
echo  '    ' .chr(10);
echo date('Hi', $intBreak);
echo  '    ' .chr(10);
echo $intBreakAdd + 30;
echo  '    ' .chr(10);
echo date('H:i:s', mktime(0, 0, 0 + ($intStop - $intStart) - (($intBreakAdd + 30) * 60), date('d'), date('m'), date('Y')));

echo  '<br />' .chr(10);
echo  '<br />' .chr(10);

}

//INSERT INTO `izs_clock_entry` (`ce_id`, `ce_cu_id`, `ce_start`, `ce_stop`, `ce_break`, `ce_break_duration`, `ce_status`) VALUES ('', '', '', '', '', '', '');


?>
