<?php

require_once('../assets/classes/global.inc.php');
require_once('../assets/classes/class.mysql.php');

$strMethod = $_SERVER['REQUEST_METHOD'];

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
}

header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');    // cache for 1 day

if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, DELETE, PUT");         

if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

if ($strMethod == 'OPTIONS') {

    exit(0);

}

/*
Array
(
    [type] => content/created
    [entity] => Array
        (
            [id] => content-903421df-ff29-42c0-8e51-2b0161d6564c
            [target] => topic-1a0d53f7-979c-4ce8-ae3c-69a061d0da6d
            [author] => system@izs-institut.de
            [contenttype] => mail
            [title] => E-Mail
            [flags] => Array
                (
                    [0] => incoming
                )
            [text] => <p>Inhalt</p><p>--<br>Frank LÃ¼tgen</p><p>IZS - Institut fÃ¼r Zahlungssicherheit GmbH<br>WÃ¼rmtalstraÃŸe 20a<br>81375 MÃ¼nchen</p><p>Telefon: +49 (0) 89 â€“ 122 237 77 0<br>Fax: +49 (0) 89 â€“ 122 237 77 9<br>E-Mail: <a href="mailto:system@izs-institut.de">system@izs-institut.de</a></p><p>Eingetragen im Handelsregister MÃ¼nchen â–ª HRB 184 133 â–ª GeschÃ¤ftsfÃ¼hrer:<br>Christian Marchsreiter</p><p>Wir verarbeiten Ihre personenbezogenen Daten und mÃ¶chten Sie daher auf<br>unsere Datenschutzhinweise aufmerksam machen, welche Sie unter folgendem<br>Link finden: <a href="https://www.izs-institut.com/datenschutz">https://www.izs-institut.com/datenschutz</a></p><p>Disclaimer:<br>Der Inhalt dieser E-Mail ist vertraulich und ausschlieÃŸlich fÃ¼r den<br>bezeichneten Adressaten bestimmt. Wenn Sie nicht der vorgesehene<br>Adressat dieser E-Mail oder dessen Vertreter sein sollten, so beachten<br>Sie bitte, dass jede Form der Kenntnisnahme, VerÃ¶ffentlichung,<br>VervielfÃ¤ltigung od
 er Weitergabe des Inhalts dieser E-Mail unzulÃ¤ssig<br>ist. Wir bitten Sie, sich in diesem Fall mit dem Absender der E-Mail in<br>Verbindung zu setzen. Der Inhalt der E-Mail ist nur rechtsverbindlich,<br>wenn er unsererseits durch einen Brief oder ein Fax entsprechend<br>bestÃ¤tigt wird.</p>
            [from] => system@izs-institut.de
            [to] => Array()
            [cc] => Array()
            [bcc] => Array()
            [recipients] => Array()
            [messageid] => 0078fe59-1be8-20e8-2f61-fcbf5d85f230@izs-institut.de
            [threadid] => 6134991c-874b-4545-8de5-b7b2098c2a9d
            [attachments] => Array()
            [type] => content
            [created] => 1671621386770
            [updated] => 1671621386770
            [groups] => Array()
            [path] => Array
                (
                    [0] => context-4b7f91fe-ec4e-42ee-a359-6dd7af678f3d
                    [1] => topic-1a0d53f7-979c-4ce8-ae3c-69a061d0da6d
                )

            [version] => 1
        )

    [entityId] => content-903421df-ff29-42c0-8e51-2b0161d6564c
    [instance] => https://qm.smartgroups.io
    [date] => 1671621386832
)
*/

$fp = fopen('php://input', 'r');
$rawData = stream_get_contents($fp);
$arrPost = json_decode($rawData, true);

//mail('system@izs-institut.de', 'Testhook', print_r($arrPost, true));

///*

if (isset($arrPost) && (count($arrPost) > 0)) {


    if (isset($arrPost['entity']['contenttype']) && ($arrPost['entity']['contenttype'] == 'mail')) { // content is incomming mail?
        //  && isset($arrPost['entity']['flags']) && (in_array('incoming', $arrPost['entity']['flags']) !== false)

        $strSql = 'SELECT * FROM `izs_cms2sg` WHERE `cs_type` = "Basisdoku"';
        $arrSql = MySQLStatic::Query($strSql);
    
        $arrMonitorTopicList = array();
        if (count($arrSql) > 0) {
            foreach ($arrSql as $intKey => $arrRelation) {
                $arrMonitorTopicList[$arrRelation['cs_sg_index']] = $arrRelation['cs_id'];
            }
        }

        if (isset($arrPost['entity']['target']) && (isset($arrMonitorTopicList[$arrPost['entity']['target']]))) { // target is monitored?

            if ($arrPost['entity']['from'] != '') { // system mails have no "from"
                $strSql = 'UPDATE `izs_cms2sg` SET `cs_sg_update` = NOW() WHERE `cs_id` = "' .$arrMonitorTopicList[$arrPost['entity']['target']] .'"';
                $arrSql = MySQLStatic::Update($strSql);
            }

            $strSql = 'INSERT INTO `izs_sg_hook` (`sh_id`, `sh_type`, `sh_key`, `sh_time`, `sh_entity`) VALUES (NULL, "1", "' .$arrPost['entity']['target'] .'", NOW(), "' .MySQLStatic::esc(serialize($arrPost['entity'])) .'")';
            $intSql = MySQLStatic::Insert($strSql);

        }


    }


}

//*/


?>