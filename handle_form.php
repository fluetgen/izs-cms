<?php

header('Access-Control-Allow-Origin: *');

function curl_get ($strUrl, array $arrGet = NULL, array $arrOptions = array(), $arrHeader = array()) { 

    $arrDefault = array( 
        CURLOPT_URL => $strUrl. (strpos($strUrl, '?') === FALSE ? '?' : ''). http_build_query($arrGet), 
        CURLOPT_HEADER => 0, 
        CURLOPT_RETURNTRANSFER => true, 
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_TIMEOUT => 4
    ); 
    
    $resCurl = curl_init();
  
    if (count($arrHeader) > 0) {
        curl_setopt ($resCurl, CURLOPT_HTTPHEADER, $arrHeader); 
    }
    
    curl_setopt_array($resCurl, ($arrOptions + $arrDefault)); 
    if(!$mxdResult = curl_exec($resCurl)) { 
        trigger_error(curl_error($resCurl)); 
    } 
    curl_close($resCurl);
  
    return $mxdResult; 
} 

function curl_post ($strUrl, array $arrPost = NULL, array $arrOptions = array(), $arrHeader = array()) { 

    $arrDefault = array( 
        CURLOPT_POST => TRUE,
        CURLOPT_POSTFIELDS => json_encode($arrPost),
        CURLOPT_RETURNTRANSFER => TRUE, 
        CURLOPT_URL => $strUrl,
        CURLOPT_HEADER => TRUE,
        CURLOPT_SSL_VERIFYPEER => TRUE
    ); 
  
    $resCurl = curl_init(); 
  
    if (count($arrHeader) > 0) {
        curl_setopt ($resCurl, CURLOPT_HTTPHEADER, $arrHeader); 
    }
    
    curl_setopt_array($resCurl, ($arrOptions + $arrDefault)); 
    if(!$mxdResult = curl_exec($resCurl)) { 
        trigger_error(curl_error($resCurl)); 
    } 
    $arrInfo = curl_getinfo($resCurl);
    curl_close($resCurl);
  
    $strHeader = trim(substr($mxdResult, 0, $arrInfo['header_size']));
    $strBody = substr($mxdResult, $arrInfo['header_size']);
  
    return $strBody; 
} 

$strHeader = 'From: tilda@izs-institut.de';

$strMessage = '';

mail('fl@izs-institut.de', 'Test', print_r($_REQUEST, true));

if (isset($_REQUEST['Name-Unternehmen'])) { // NEW WP-PAGE

    //print_r($_REQUEST); die();

    if (isset($_REQUEST['Name-Unternehmen']) && !isset($_REQUEST['AnzahlSV-Meldestellen'])) {

        $_REQUEST['Name_Ihres_Unternehmens'] = $_REQUEST['Name-Unternehmen'];
        $_REQUEST['Art_des_Unternehmens']    = $_REQUEST['art-Unternehmen'];
        $_REQUEST['Name']                    = $_REQUEST['your-name'];
        $_REQUEST['last_name']               = $_REQUEST['Nachname'];
        $_REQUEST['function']                = $_REQUEST['Funktion'];
        $_REQUEST['phone']                   = $_REQUEST['Telefon'];
        $_REQUEST['e-mail']                  = $_REQUEST['your-email'];
        //Nachricht fällt weg s.u.

    }

    if (isset($_REQUEST['AnzahlSV-Meldestellen'])) {

        $_REQUEST['Name_Ihres_Unternehmens']  = $_REQUEST['Name-Unternehmen'];
        $_REQUEST['street']                   = $_REQUEST['Strasse-Hausnummer'];
        $_REQUEST['zip']                      = $_REQUEST['Postleitzahl'];
        $_REQUEST['city']                     = $_REQUEST['Ort'];
        $_REQUEST['Vorname']                  = $_REQUEST['your-name'];
        $_REQUEST['last_name']                = $_REQUEST['Nachname'];
        $_REQUEST['function']                 = $_REQUEST['Funktion'];
        $_REQUEST['phone']                    = $_REQUEST['Telefon'];
        $_REQUEST['e-mail']                   = $_REQUEST['your-email'];
        $_REQUEST['meldestelle']              = $_REQUEST['AnzahlSV-Meldestellen'];
        $_REQUEST['beitragsnachweise_anzahl'] = $_REQUEST['AnzahlSV-BeitragsnachweiseproMonat'];
        $_REQUEST['lohnbuchhaltungssoftware'] = $_REQUEST['Lohnbuchhaltungssoftware'];
        $_REQUEST['verband']                  = $_REQUEST['Verband'];
        $_REQUEST['entleiher']                = $_REQUEST['Entleiher-AktionscodefrSonderkonditionen'];
        //Nachricht fällt weg s.u.

    }

    if (isset($_REQUEST['AnzahlPersonaldienstleister'])) {
        
        $_REQUEST['Name_Ihres_Unternehmens']                                     = $_REQUEST['Name-Unternehmen'];
        $_REQUEST['Vorname']                                                     = $_REQUEST['your-name'];
        $_REQUEST['last_name']                                                   = $_REQUEST['Nachname'];
        $_REQUEST['function']                                                    = $_REQUEST['Funktion'];
        $_REQUEST['phone']                                                       = $_REQUEST['Telefon'];
        $_REQUEST['e-mail']                                                      = $_REQUEST['your-email'];
        $_REQUEST['Anzahl_Personaldienstleister_mit_denen_Sie_zusammenarbeiten'] = $_REQUEST['AnzahlPersonaldienstleister'];
        //Nachricht fällt weg s.u.

    }

}


/* --------------------- FORM PROCESSING --------------------- */

/*
<input type="hidden" name="form-processing" value="context=context-62e0fded-731d-4461-8d4f-5525df81da5f; chatlist=chatlist-4fb00ebb-c8a9-4e1c-b347-69666b063bb9; deliver=mail; assign=gordon.schenke+karoline.tomasini+jacqueline.broeking; follow=cm; due=0;subject=ENT "{Name_Ihres_Unternehmens}" > Anfrage über IZS-Website" />
*/

/* ----------------------------------------------------------- */

$arrUserListRaw = array(
    'anja.schuller' => array(
        'domain' => 'gmx.de',
        'sgid'   => 'user-bc3ab35f-0e93-449f-86a3-65e1d2f05c8c'
    ),
    'begumsultan.korkmaz' => array(
        'domain' => 'izs-institut.de',
        'sgid'   => 'user-4025846c-1a95-48b8-aa4c-e55eb79372f1'
    ),
    'beitragsnachweise' => array(
        'domain' => 'izs-institut.de',
        'sgid'   => 'user-a1dea35f-1362-4414-b3e8-8113f8ea76a1'
    ),
    'bianca.frass' => array(
        'domain' => 'izs-institut.de',
        'sgid'   => 'user-79ad6f94-c614-4820-903b-d59173c43527'
    ),
    'christiane.marchsreiter' => array(
        'domain' => 'taranis-capital.de',
        'sgid'   => 'user-de8056be-0207-4efc-94ed-f05ef9077d48'
    ),
    'christina.krachten' => array(
        'domain' => 'izs-institut.de',
        'sgid'   => 'user-9bc6ae5d-75d2-4fd0-b9be-e8ccea70b420'
    ),
    'cm' => array(
        'domain' => 'izs-institut.de',
        'sgid'   => 'user-50754418-3718-4de0-a072-c131f780ba47'
    ),
    'daniel.broeking' => array(
        'domain' => 'taranis-capital.de',
        'sgid'   => 'user-9f703f25-fed4-4cd4-b6ed-9ce0df6eba4c'
    ),
    'ecem.coskun' => array(
        'domain' => 'izs-institut.de',
        'sgid'   => 'user-6d9eaa1d-4935-4347-b145-97535f206fc9'
    ),
    'erwin.niessl' => array(
        'domain' => 'izs-institut.de',
        'sgid'   => 'user-91d98f18-33c9-4577-a1fa-196479ee58d3'
    ),
    'fl' => array(
        'domain' => 'izs-institut.de',
        'sgid'   => 'user-3c098ca3-2b00-4dad-bff5-a7d0619a77fc'
    ),
    'gordon.schenke' => array(
        'domain' => 'izs-institut.de',
        'sgid'   => 'user-79b0a631-3eed-47f4-8742-f4059d421ae7'
    ),
    'hannelore.mutzel' => array(
        'domain' => 'izs-institut.de',
        'sgid'   => 'user-2faeabbc-ad1d-445c-944b-e3cf7d88a7c4'
    ),
    'igor.kubitskyy' => array(
        'domain' => 'izs.onmicrosoft.com',
        'sgid'   => 'user-b3db7f92-f0be-4625-a548-449848cad645'
    ),
    'jacqueline.broeking' => array(
        'domain' => 'izs-institut.de',
        'sgid'   => 'user-b1de431c-61aa-469a-a961-85862320c74b'
    ),
    'karoline.tomasini' => array(
        'domain' => 'izs-institut.de',
        'sgid'   => 'user-1c4baa29-3e96-40b3-9225-7953d706bb2c'
    ),
    'kathrein.hetze' => array(
        'domain' => 'izs-institut.de',
        'sgid'   => 'user-93cf12e7-14ff-41bc-b0c2-20eaefe29bfc'
    ),
    'km' => array(
        'domain' => 'izs-institut.de',
        'sgid'   => 'user-431415ef-d9cd-4e76-8d6d-e17a06abcb92'
    ),
    'monika.albert' => array(
        'domain' => 'izs-institut.de',
        'sgid'   => 'user-ff103753-f6fa-427b-aa26-eb1ba2fe7aa0'
    ),
    'renate.brunner' => array(
        'domain' => 'izs-institut.de',
        'sgid'   => 'user-45c79a41-feae-4c86-976d-3acbdde01b5e'
    ),
    'serap.yildirim' => array(
        'domain' => 'izs-institut.de',
        'sgid'   => 'user-53c49896-2416-4e21-af4a-01b4a38cc348'
    ),
    'stefanie.feldmeier' => array(
        'domain' => 'izs-institut.de',
        'sgid'   => 'user-f42b6360-d5e0-4935-8135-039c79c0f881'
    ),
    'susanne.knoepfle' => array(
        'domain' => 'izs-institut.de',
        'sgid'   => 'user-365f46ec-cba0-4c28-bb41-2771f325c3d0'
    ),
);


if (isset($_REQUEST['form-processing'])) {

    $arrProcessingPart = preg_split('/;\s?/', $_REQUEST['form-processing']);

    $arrProcessing = [];
    foreach ($arrProcessingPart as $intKey => $strValue) {

        if ($strValue == '') continue; 

        $arrVar = preg_split('/=/', $strValue);
        if (count($arrVar) == 2) {
            $arrProcessing[$arrVar[0]] = $arrVar[1];
        }
        
    }

    // 
    if (isset($arrProcessing['context']) && ($arrProcessing['context'] != '')) { // Process as hub

        $arrPart = explode('/', $arrProcessing['context']);
        if (count($arrPart) > 0) {
            $arrProcessing['context'] = end($arrPart);
        }

        $strSubject = 'IZS Institut Anfrage - Angebotsanfrage';

        $arrHeader = array(
            'apikey: e9d99940-1aed-414a-b836-b929fe97d093',
            'user: ' .'4', // Mr. Bot
            'Accept: application/json'
        );


        $arrMenmber = array();
        $arrFollow = array();

        $strUrl = 'http://api.izs-institut.de/api/sg/topic';

        $arrPost = array(
            //'target' => $arrProcessing['context'],
            'type'   => 'topic',
            'topictype' => 'default',
            'category' => array(),
            'context' => $arrProcessing['context']
        );

        if (isset($arrProcessing['chatlist']) && ($arrProcessing['chatlist'] != '')) {
            $arrPost['chatlist'] = $arrProcessing['chatlist'];
        }

        if (isset($arrProcessing['subject']) && ($arrProcessing['subject'] != '')) {

            $arrPost['title'] = $arrProcessing['subject'];

            preg_match_all('/\{([^}]+)\}/U', $arrProcessing['subject'], $arrVarList);

            //print_r($arrVarList);
            //print_r($_REQUEST);

            if (count($arrVarList) == 2) {
                foreach ($arrVarList[1] as $intKey => $strVarName) {
                    if (isset($_REQUEST[$strVarName])) {
                        $arrPost['title'] = str_replace($arrVarList[0][$intKey], $_REQUEST[$strVarName], $arrPost['title']);
                    }
                }
            }

        }

        //if (isset($_REQUEST['id']) && ($_REQUEST['id'] == 3)) { echo $arrPost['title']; die(); }

        if (isset($arrProcessing['due']) && ($arrProcessing['due'] != '')) {
            $arrPost['due'] = strtotime('+ ' .$arrProcessing['due'] .' days') * 1000;
        }

        if (isset($arrProcessing['assign']) && ($arrProcessing['assign'] != '')) {
            $arrUserList = preg_split('/\s?\+\s?/', $arrProcessing['assign']);
            if (count($arrUserList) > 0) {
                foreach ($arrUserList as $intCount => $strUser) {
                    if ($strUser == '') continue;
                    if (isset($arrUserListRaw[$strUser])) $arrPost['member'][] = $arrUserListRaw[$strUser]['sgid'];
                }
            }
        }

        if (isset($arrProcessing['follow']) && ($arrProcessing['follow'] != '')) {
            $arrUserList = preg_split('/\s?\+\s?/', $arrProcessing['follow']);
            if (count($arrUserList) > 0) {
                foreach ($arrUserList as $intCount => $strUser) {
                    if ($strUser == '') continue;
                    if (isset($arrUserListRaw[$strUser])) $arrPost['follow'][] = $arrUserListRaw[$strUser]['sgid'];
                }
            }
        }

        //print_r($arrPost); die();

        $strMessage = '';
        $strMessage.= '<html><head></head><body>' .chr(10);

        $strMessage.= '<p>Eingang: ' .date('d.m.Y H:i:s') .'</p>' .chr(10);

        $strMessage.= '<table border="1" style="width: 500px;">' .chr(10);

        foreach ($_REQUEST as $strKey => $strValue) {

            if (in_array($strKey, array('form-processing', 'tranid', 'formid'))) continue;

            if (strstr($strValue, '@') !== false) {
                $strValue = '<a href="mailto:' .$strValue .'">' .$strValue .'</a>';
            } else if (strstr($strValue, 'http') !== false) {
                $strValue = '<a href="' .$strValue .'" target="_blank">' .$strValue .'</a>';
            }

            $strMessage.= '  <tr>' .chr(10);
            $strMessage.= '    <td style="padding: 5px;">' .str_replace('_', ' ', $strKey) .'</td>' .chr(10);
            $strMessage.= '    <td style="padding: 5px;">' .$strValue .'</td>' .chr(10);
            $strMessage.= '  </tr>' .chr(10);
        }

        $strMessage.= '</table>' .chr(10);
        $strMessage.= '</body></html>' .chr(10);

        mail('fl@izs-institut.de', $arrPost['title'], $strMessage, $strHeader); //


        $jsonPost = curl_post($strUrl, $arrPost, array(), $arrHeader);
        $arrTopic = json_decode($jsonPost, true);

        //print_r($arrTopic); 

        $strUrl  = $strUrl .'/' .$arrTopic['id'];
        $jsonGet = curl_get($strUrl, array(), array(), $arrHeader);
        $arrTopic = json_decode($jsonGet, true);

        $strHeader = 'From: form-processor@izs-institut.de' ."\r\n";
        $strHeader.= 'MIME-Version: 1.0' ."\r\n";
        $strHeader.= 'Content-Type: text/html; charset=UTF-8' ."\r\n";

        mail($arrTopic['metadata']['email'], $arrPost['title'], $strMessage, $strHeader); //

        echo '1';

        exit;
        //*/

    }

    die();


} else {

    //print_r($_REQUEST); die();

    /*
    STARTSEITE / KONTAKT:

    Versand an: info@izs-institut.de

    Format:
    Frank Lütgen <f.luetgen@gmx.de>

    Firma: Das ist nur ein Test 
    Benutzergruppe: Zeitarbeitsfirma
    Anrede: Herr
    Vorname: Frank
    Nachname: Lütgen
    Titel: Titel
    Telefonnummer: 08912345678
    Email: f.luetgen@gmx.de 
    Nachricht:
    Nachricht


    Array
    (
        [Name_Ihres_Unternehmens] => Test von der Startseite
        [Art_des_Unternehmens] => Entleiher
        [Name] => Frank
        [last_name] => LÃ¼tgen
        [function] => DVD
        [phone] => 08912345678
        [e-mail] => f.luetgen@gmx.de
        [Checkbox] => yes
        [tranid] => 2579521:1702403021
        [formid] => form199774532
    )

    STARTSEITE [formid] => form199774532
    KONTAKT [formid] => form199767071
    */

    if (!isset($_REQUEST['Anzahl_Personaldienstleister_mit_denen_Sie_zusammenarbeiten']) && !isset($_REQUEST['AnzahlPersonaldienstleister'])) {

        $strMailTo  = 'info@izs-institut.de';
        $strSubject = 'IZS Institut Anfrage - Kontaktform';
        
        $strMessage.= $_REQUEST['Name'] .' ' .$_REQUEST['last_name'] .' <' .$_REQUEST['e-mail'] .'>' .chr(10) .chr(10);
        $strMessage.= 'Firma: ' .$_REQUEST['Name_Ihres_Unternehmens'] .chr(10);
        $strMessage.= 'Benutzergruppe: ' .$_REQUEST['Art_des_Unternehmens'] .chr(10);
        //$strMessage.= 'Anrede: Herr' .chr(10); //Fällt weg
        $strMessage.= 'Vorname: ' .$_REQUEST['Name'] .chr(10);
        $strMessage.= 'Nachname: ' .$_REQUEST['last_name'] .chr(10);
        //$strMessage.= 'Titel: Titel' .chr(10); //Fällt weg
        $strMessage.= 'Funktion: ' .$_REQUEST['function'] .chr(10); //Neu
        $strMessage.= 'Telefonnummer: ' .$_REQUEST['phone'] .chr(10);
        $strMessage.= 'Email: ' .$_REQUEST['e-mail'] .chr(10);
        //$strMessage.= 'Nachricht:' .chr(10); //Fällt weg

        //NICHT VERSENDEN BEI "leistungen / izs für zeitarbeitsfirmen"
        if ($_REQUEST['verband'] == '') {
            @mail($strMailTo, $strSubject, $strMessage, $strHeader);
        }

    }

    /*
    ZEITARBEITSFIRMA

    Betreff: Neue Angebotsanfrage: Das ist nur ein Test (.de)

    Versand an: -> Chat

    Format:
    B.A.H. Personaldienste GmbH
    Albring 81
    78658 Zimmern ob Rottweil

    Herr
    Michael Hartung
    Leiter Operations

    Tel.: 0175-2872318
    E-Mail: m.hartung@bah-gruppe.de

    Anzahl Meldestellen: 1
    Anzahl Beitragsnachweise: 40

    Software: Perfidia
    Verband: BAP
    Entleiher (Sonderkonditionen): EuroQ

    Array
    (
        [Name_Ihres_Unternehmens] => Test von Zeitarbeitsfirma
        [street] => Schilfweg 10
        [zip] => 82178
        [city] => Puchheim
        [Vorname] => first_nFme
        [last_name] => LÃ¼tgen
        [function] => DVD
        [phone] => 08912345678
        [e-mail] => f.luetgen@gmx.de
        [meldestelle] => 20
        [beitragsnachweise_anzahl] => 100
        [lohnbuchhaltungssoftware] => Addison Lohn & Gehalt
        [verband] => Keine Verbandsmitgliedschaft
        [entleiher] => Siemans
        [Checkbox] => yes
        [tranid] => 2579521:1702408071
        [formid] => form199676404
    )
    */

    if (isset($_REQUEST['meldestelle'])) {

        $boolTest = true;


        $strMailTo  = 'fl@izs-institut.de';
        $strSubject = 'IZS Institut Anfrage - Angebotsanfrage';

        $arrHeader = array(
            'apikey: e9d99940-1aed-414a-b836-b929fe97d093',
            'user: ' .'3', // Mr. Bot
            'Accept: application/json'
        );


        // Create User List
        $arrRes = curl_get('http://api.izs-institut.de/api/sg/user', array(), array(), $arrHeader);
        $arrUserList = json_decode($arrRes, true);

        // Chat User
        $arrAssign = array(
            36 => 'Gordon',
            44 => 'Karoline', 
        );

        $arrMenmber = array();
        foreach ($arrAssign as $intId => $strName) {
            $arrMember[] = $arrUserList[$intId];
        }

        $arrFollow = array(
            2 => 'Kerstin',
            1 => 'Christian'
        );

        $arrFollower = array();
        foreach ($arrFollow as $intId => $strName) {
            $arrFollower[] = $arrUserList[$intId];
        }


        $strUrl = 'http://api.izs-institut.de/api/sg/topic';

        $arrPost = array(
            'target' => 'context-dc4c9a93-e9d0-4f0c-bfa2-555a081fccb2',
            'type'   => 'topic',
            'topictype' => 'default',
            'title' => 'Neue Angebotsanfrage: ' .$_REQUEST['Name_Ihres_Unternehmens'],
            'category' => array(),
            'due' => time() * 1000,
            'member' => $arrMember,
            'follow' => $arrFollower,
            'context' => 'context-dc4c9a93-e9d0-4f0c-bfa2-555a081fccb2'
        );

        //@mail($strMailTo, 'Post/topic', print_r($arrPost, true), $strHeader);

        $jsonPost = curl_post ($strUrl, $arrPost, array(), $arrHeader);
        $arrTopic = json_decode($jsonPost, true);

        $strUrl = 'http://api.izs-institut.de/api/sg/content';

        $strContent = '<p>' .$_REQUEST['Name_Ihres_Unternehmens'] .'<br />';
        $strContent.= $_REQUEST['street'] .'<br />';

        $strContent.= $_REQUEST['zip'] .' ' .$_REQUEST['city'] .'<br />';
        $strContent.= '<br />';
        //$strContent.= $_REQUEST['salutation'] .'<br />';
        $strContent.= $_REQUEST['Name'] .' ' .$_REQUEST['last_name'] .'<br />';
        if (isset($_REQUEST['function'])) $strContent.= $_REQUEST['function'] .'<br />';
        $strContent.= '<br />';
        $strContent.= 'Tel.: ' .$_REQUEST['phone'] .'<br />';
        $strContent.= 'E-Mail: ' .$_REQUEST['e-mail'] .'<br />';
        $strContent.= '<br />';
        $strContent.= 'Anzahl Meldestellen: ' .$_REQUEST['meldestelle'] .'<br />';
        $strContent.= 'Anzahl Beitragsnachweise: ' .$_REQUEST['beitragsnachweise_anzahl'] .'<br />';
        $strContent.= '<br />';
        $strContent.= 'Software: ' .$_REQUEST['lohnbuchhaltungssoftware'] .'<br />';
        $strContent.= 'Verband: ' .$_REQUEST['verband'];
        if (isset($_REQUEST['entleiher'])) $strContent.= '<br />' .'Entleiher (Sonderkonditionen): ' .$_REQUEST['entleiher'];

        $strContent.= '</p>';

        $arrPost = array(
            'author' => $arrUserList[3], //user-3c098ca3-2b00-4dad-bff5-a7d0619a77fc
            //'context' => 'context-dc4c9a93-e9d0-4f0c-bfa2-555a081fccb2', 
            //'topic' => $arrTopic['id'],
            'target' => $arrTopic['id'],
            'text' => $strContent,
            'type' => 'content',
            'contenttype' => 'message'
        );

        //@mail($strMailTo, 'Post/content', print_r($arrPost, true), $strHeader);

        sleep(1);

        $jsonPost = curl_post ($strUrl, $arrPost, array(), $arrHeader);

        //@mail($strMailTo, $strSubject, strip_tags(str_replace('<br />', chr(10), $strContent)), $headers);

    }

    /*
    ENTLEIHER

    Versand an: info@izs-institut.de

    Betreff: IZS Institut Anfrage - Entleiher

    Format:
    Frank [name] <f.luetgen@gmx.de>

    Name: [name]
    Vorname: Frank
    Funktion: Frank Lütgen
    Firma: Das ist nur ein Test (.de) 
    Telefon: 08912345678
    Email: f.luetgen@gmx.de 

    Anzahl Ihrer Personaldienster: 1-10





    --
    This e-mail was sent from a contact form on IZS Institut

    Array
    (
        [company_name] => Test von Entleiher
        [Vorname] => first_nFme
        [last_name] => LÃ¼tgen
        [function] => DVD
        [phone] => 08912345678
        [e-mail] => f.luetgen@gmx.de
        [Anzahl_Personaldienstleister_mit_denen_Sie_zusammenarbeiten] => 11-25
        [Checkbox] => yes
        [tranid] => 2579521:1702412851
        [formid] => form199688202
    )
    */

    if (isset($_REQUEST['Anzahl_Personaldienstleister_mit_denen_Sie_zusammenarbeiten'])) {

        $strMailTo  = 'info@izs-institut.de';
        $strSubject = 'IZS Institut Anfrage - Entleiher';
        
        $strMessage.= $_REQUEST['Name'] .' ' .$_REQUEST['last_name'] .' <' .$_REQUEST['e-mail'] .'>' .chr(10) .chr(10);
        $strMessage.= 'Name: ' .$_REQUEST['last_name'] .chr(10);
        $strMessage.= 'Vorname: ' .$_REQUEST['Name'] .chr(10);
        $strMessage.= 'Funktion: ' .$_REQUEST['function'] .chr(10); //Neu
        $strMessage.= 'Firma: ' .$_REQUEST['Name_Ihres_Unternehmens'] .chr(10);
        $strMessage.= 'Telefon: ' .$_REQUEST['phone'] .chr(10);
        $strMessage.= 'Email: ' .$_REQUEST['e-mail'] .chr(10) .chr(10);
        $strMessage.= 'Anzahl Ihrer Personaldienster: ' .$_REQUEST['Anzahl_Personaldienstleister_mit_denen_Sie_zusammenarbeiten'];
        //$strMessage.= 'Nachricht:' .chr(10); //Fällt weg

        @mail($strMailTo, $strSubject, $strMessage, $strHeader);

    }


    $headers = 'From: tilda@izs-institut.de';
    $message = print_r($_REQUEST,true);
    @mail('fl@izs-institut.de', 'Tilda TEST', $message, $headers);

    if (isset($_REQUEST['Name-Unternehmen'])) { // NEW WP-PAGE
        if (isset($_SERVER['HTTP_REFERER']) && (strstr($_SERVER['HTTP_REFERER'], 'wp.izs-institut') != '')) {
            header('Location: https://wp.izs-institut.de/vielen-dank/');
        } else {
            header('Location: https://www.izs-institut.de/vielen-dank/');
        }
    } else {
        echo 'ok';
    }

}


?>